﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.K2Objects
{
    public class CommonDTO
    {
        public string Remark { get; set; }
        public string SUBJECT { get; set; }
        public string ACCCODE { get; set; }
        public string PROJECTMANAGER { get; set; }
        public string PROJECTSPONSOR { get; set; }
        public string REQUESTBY { get; set; }
    }
}