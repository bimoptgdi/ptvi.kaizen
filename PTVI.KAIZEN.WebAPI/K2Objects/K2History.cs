﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.K2Objects
{
    public class K2History
    {
        public string ActivityName { get; set; }
        public string NtUserId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string StartDate { get; set; }
        public string FinishDate { get; set; }
        public string CreatedDate { get; set; }
        public string ActionResult { get; set; }
        public string Comment { get; set; }
    }
}