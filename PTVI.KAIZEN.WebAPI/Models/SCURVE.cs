//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PTVI.KAIZEN.WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SCURVE
    {
        public int OID { get; set; }
        public Nullable<int> ESTIMATIIONSHIFTID { get; set; }
        public Nullable<int> PROJECTID { get; set; }
        public string PROJECTNAME { get; set; }
        public string TASKUID { get; set; }
        public string TASKNAME { get; set; }
        public string PARENTTASKUID { get; set; }
        public Nullable<int> OUTLINELEVEL { get; set; }
        public string OUTLINEPOSITION { get; set; }
        public Nullable<bool> ISSUMMARY { get; set; }
        public Nullable<int> SHIFTNO { get; set; }
        public Nullable<int> PERCENTCOMPLETEBASELINE { get; set; }
        public Nullable<int> PERCENTCOMPLETEACTUAL { get; set; }
        public Nullable<int> BASELINEDURATION { get; set; }
        public Nullable<int> ACTUALDURATION { get; set; }
        public Nullable<double> REMAININGORIGINAL { get; set; }
        public Nullable<double> PERCENTCOMPLETEORIGINAL { get; set; }
        public Nullable<double> REMAININGOPTIMIS { get; set; }
        public Nullable<double> PERCENTCOMPLETEOPTIMIS { get; set; }
        public Nullable<double> REMAININGREALISTIC { get; set; }
        public Nullable<double> PERCENTCOMPLETEREALISTIC { get; set; }
    }
}
