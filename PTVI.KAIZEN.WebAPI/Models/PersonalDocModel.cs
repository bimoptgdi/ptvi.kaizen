﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.OSC.WebAPI.Models
{
    public class PersonalDocModel
    {
        public int OID { get; set; }
        public Nullable<int> PersonalID { get; set; }
        public string DocumentType { get; set; }
        public string DocDescription { get; set; }
        public Nullable<System.DateTime> DateIssue { get; set; }
        public Nullable<System.DateTime> DateExpired { get; set; }
        public string DocumentFile { get; set; }
        public string DocumentName { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}