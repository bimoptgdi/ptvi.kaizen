//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PTVI.KAIZEN.WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FILEINTEGRATIONLOADER
    {
        public int OID { get; set; }
        public Nullable<int> FILEINTEGRATIONID { get; set; }
        public string FILENAME { get; set; }
        public Nullable<System.DateTime> LOADERDATE { get; set; }
        public string LOADERBY { get; set; }
        public string REMARK { get; set; }
    
        public virtual FILEINTEGRATION FILEINTEGRATION { get; set; }
    }
}
