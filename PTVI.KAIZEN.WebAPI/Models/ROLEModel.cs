﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.OSC.WebAPI.Models
{
    public class ROLEModel
    {
        public int OID { get; set; }
        public string ROLENAME { get; set; }
        public string DESCRIPTION { get; set; }

        public List<MENUModel> MenuList { get; set; }
        public List<USERModel> UserList { get; set; }
        public List<ACCESSRIGHTModel> Accessright { get; set; }
    }
}