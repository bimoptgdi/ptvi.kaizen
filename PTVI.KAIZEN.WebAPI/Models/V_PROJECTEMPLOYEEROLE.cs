//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PTVI.iPROM.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_PROJECTEMPLOYEEROLE
    {
        public int OID { get; set; }
        public string PROJECTNO { get; set; }
        public string PROJECTDESCRIPTION { get; set; }
        public Nullable<System.DateTime> PLANSTARTDATE { get; set; }
        public Nullable<System.DateTime> PLANFINISHDATE { get; set; }
        public Nullable<System.DateTime> ACTUALSTARTDATE { get; set; }
        public Nullable<System.DateTime> ACTUALFINISHDATE { get; set; }
        public string EMPLOYEEID { get; set; }
        public string EMPLOYEENAME { get; set; }
        public Nullable<int> RoleLevel { get; set; }
        public string RoleName { get; set; }
    }
}
