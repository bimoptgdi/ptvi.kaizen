﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class technicalSupportInteractionObject
    {
        
        public int id { get; set; }
        public int userAssignmentId { get; set; }
        public string actionDescription { get; set; }
        public string result { get; set; }
        public double? completePercentage { get; set; }
        public DateTime? completedDate { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        
        public virtual userAssignmentObject userAssignment { get; set; }
       

    }
}
