﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class tenderObject
    {
        public tenderObject()
        {
            //this.projectDocuments = new HashSet<projectDocumentObject>();
            this.tenderEquipments = new HashSet<tenderEquipmentObject>();
            this.tenderInstallations = new HashSet<tenderInstallationObject>();
            this.tenderManPowers = new HashSet<tenderManPowerObject>();
            this.tenderMaterials = new HashSet<tenderMaterialObject>();
            this.tenderProgresses = new HashSet<tenderProgressObject>();
            this.tenderPunchlists = new HashSet<tenderPunchlistObject>();
        }

        public int id { get; set; }
        public string tenderNo { get; set; }
        public DateTime? tenderCreatedDate { get; set; }
        public string tenderCreatedBy { get; set; }
        public int? vendorSelectedId { get; set; }
        public string vendorSelectedCode { get; set; }
        public string vendorSelectedName { get; set; }
        public string contractNo { get; set; }
        public DateTime? contractCreatedDate { get; set; }
        public string contractCreatedBy { get; set; }
        public string pmoNo { get; set; }
        public DateTime? pmoCreatedDate { get; set; }
        public string pmoCreatedBy { get; set; }
        public string docNoSummary { get; set; }
        public bool isEnabledSelectVendor { get; set; }
        public string contractTrafic { get; set; }
        public DateTime? tenderInstallationsMaxDate { get; set; }
        public DateTime? tenderPunchlistsMaxDate { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        //public virtual ICollection<projectDocumentObject> projectDocuments { get; set; }
        public virtual ICollection<tenderEquipmentObject> tenderEquipments { get; set; }
        public virtual ICollection<tenderInstallationObject> tenderInstallations { get; set; }
        public virtual ICollection<tenderManPowerObject> tenderManPowers { get; set; }
        public virtual ICollection<tenderMaterialObject> tenderMaterials { get; set; }
        public virtual ICollection<tenderProgressObject> tenderProgresses { get; set; }
        public virtual ICollection<tenderPunchlistObject> tenderPunchlists { get; set; }
    }
}
