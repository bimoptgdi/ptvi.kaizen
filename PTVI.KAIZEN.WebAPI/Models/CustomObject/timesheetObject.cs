﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class timesheetObject
    {
        public int id { get; set; }
        public int? projectId { get; set; }
        public int? projectDocumentId { get; set; }
        public string overheadActivity { get; set; }
        public string description { get; set; }
        public int? spentHours { get; set; }
        public Nullable<int> weeklyId { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual projectObject project { get; set; }
        //public virtual projectDocumentObject projectDocument { get; set; }
        public virtual WEEKLY WEEKLY { get; set; }
    }
}
