using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterKeyDeliverableObject
    {
        public int id { get; set; }
        public Nullable<int> projectGroupCategoryId { get; set; }
        public string seq { get; set; }
        public string keyDeliverables { get; set; }
        public string remarks { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string projectGroupCategoryName { get; set; }

        public virtual masterProjectGroupCategoryObject masterProjectGroupCategory { get; set; }
    }
}
