﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Controllers;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class timesheetExtObject
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private KAIZENController ipc = new KAIZENController();
        public int id { get; set; }
        public int? projectId { get; set; }
        public int? projectDocumentId { get; set; }
        public int? technicalSupportId { get; set; }
        public int? technicalSupportUserActionId { get; set; }
        public string overheadActivity { get; set; }
        public double? weekHours { get; set; }
        public double? hours { get; set; }
        public Nullable<int> weeklyId { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public ICollection<projectObject> project { get
            {
                var project = db.PROJECTs.Where(x => x.OID== this.projectId).Select(y => new projectObject()
                {
                    id = y.OID,
                    projectNo = y.PROJECTNO,
                    projectDescription = y.PROJECTDESCRIPTION
                }).ToList();
                if(project.Count>0)
                {
                    this.projectNo = project[0].projectNo;
                    this.description = project[0].projectDescription;
                }
                return project;
            } set
            {
            }
        }
        public ICollection<projectDocumentObject> projectDocument {
            get
            {
                var projectDocument = db.PROJECTDOCUMENTs.Where(x => x.OID == this.projectDocumentId).Select(y => new projectDocumentObject()
                {
                    id = y.OID,
                    docType = y.DOCTYPE,
                    docTitle = y.DOCTITLE,
                    drawingTitle = y.DRAWINGTITLE,
                    planStartDate = y.PLANSTARTDATE!=null? y.PLANSTARTDATE : DateTime.MinValue,
                    actualStartDate = y.ACTUALSTARTDATE,
                    actualFinishDate = y.ACTUALFINISHDATE,
                    planFinishDate = y.PLANFINISHDATE != null ? y.PLANFINISHDATE : DateTime.MinValue,
                    status = y.STATUS == constants.STATUSIPROM_HOLD || y.STATUS == constants.STATUSIPROM_CANCELLED ? y.STATUS : !y.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !y.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED
                }).ToList();
                if (projectDocument.Count > 0)
                {
                    this.task = projectDocument[0].docTitle;
                    this.planDate = projectDocument[0].planStartDate.Value;
                    this.actualDate = projectDocument[0].actualStartDate;
                    var ph = projectDocument[0].planFinishDate.Value.Subtract(projectDocument[0].planStartDate.Value).TotalDays;
                    this.planHours = Convert.ToInt32(ph) * 8;
                    this.status = projectDocument[0].status;
                }

                return projectDocument;
            }
            set
            {
            }

        }
        public int planHours { get; set; }
        public technicalSupportInteractionObject technicalSupportInteractionObject
        {
            get {
                var result = db.TECHNICALSUPPORTUSERACTIONs.Find(this.technicalSupportUserActionId);
                if (result != null)
                {
                    this.task = result.ACTIONDESCRIPTION;
                } else if (!this.projectDocumentId.HasValue && !this.projectId.HasValue)
                        {
                    this.task = this.description;
                }

                return new technicalSupportInteractionObject();
                
            }
        }

        public ICollection<technicalSupportObject> technicalSupportObject { get
            {
                var technicalSupportObject = db.TECHNICALSUPPORTs.Where(x => x.OID == this.technicalSupportId).Select(y => new technicalSupportObject()
                {
                    id = y.OID,
                    tsNo = y.TSNO,
                    tsDesc = y.TSDESC,
                    status = y.STATUS
                }).ToList();
                if (technicalSupportObject.Count > 0)
                {
                    this.tsOid = technicalSupportObject[0].id;
                    this.status = technicalSupportObject[0].status;
                    if (projectDocument.Count == 0)
                    {
                        this.projectNo = technicalSupportObject[0].tsNo;
                        this.description = technicalSupportObject[0].tsDesc;
                        this.status = technicalSupportObject[0].status;
                    }
                }
                else
                {
                    if ((project.Count == 0) && (projectDocument.Count == 0)&& (technicalSupportObject.Count == 0))
                    {
                        this.descriptionOha = db.LOOKUPs.Where(x => x.TYPE == "overheadTimesheet" && x.VALUE == this.overheadActivity).Select(y => y.TEXT).FirstOrDefault();
                    }
                }
                return technicalSupportObject;
            }
            set
            { }
           }
        int tsOid;
        private double? _spentHours;
        public double? spentHours
        {
            get
            {
                double? spentHrs = 0;
                if (this.projectDocument.Count > 0)
                    spentHrs = db.TIMESHEETs.Where(x => x.WEEKLYID == weeklyId && x.CREATEDBY == createdBy && x.PROJECTDOCUMENTID == projectDocumentId).Sum(z => z.HOURS);
                else if (technicalSupportObject.Count > 0)
                {
                    spentHrs = db.TIMESHEETs.Where(x => x.WEEKLYID == weeklyId && x.CREATEDBY == createdBy && x.TECHNICALSUPPORTID == tsOid).Sum(z => z.HOURS);
                    if (spentHrs == null) spentHrs = this.hours;
                }
                else if ((project.Count == 0) && (projectDocument.Count == 0) && (technicalSupportObject.Count == 0)) 
                    spentHrs = weekHours;
                return spentHrs;
            }
            set {
                if (this.projectDocument.Count > 0)
                    _spentHours = db.TIMESHEETs.Where(x => x.WEEKLYID == weeklyId && x.CREATEDBY == createdBy && x.PROJECTDOCUMENTID == projectDocumentId).Sum(z => z.HOURS).GetValueOrDefault();
                else if (technicalSupportObject.Count > 0)
                {
                    _spentHours = db.TIMESHEETs.Where(x => x.WEEKLYID == weeklyId && x.CREATEDBY == createdBy && x.TECHNICALSUPPORTID == tsOid).Sum(z => z.HOURS);
                    if (_spentHours == null) _spentHours = this.hours;
                }
                else if ((project.Count == 0) && (projectDocument.Count == 0) && (technicalSupportObject.Count == 0)) { }
                    _spentHours = weekHours;
            }
        }
        public string projectNo { get; set; }
        public string description { get; set; }
        public string descriptionOha { get; set; }
        public string task { get; set; }
        public string status { get; set; }
        public DateTime planDate { get; set; }
        public DateTime? actualDate { get; set; }
        public virtual WEEKLY WEEKLY { get; set; }
    }
}
