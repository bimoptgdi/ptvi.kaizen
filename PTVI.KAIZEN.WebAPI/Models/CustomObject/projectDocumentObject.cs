﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class projectDocumentObject
    {
        public projectDocumentObject()
        {
            this.constructionServices = new HashSet<constructionServiceObject>();
            this.projectCloseOuts = new HashSet<projectCloseOutObject>();
            this.costReportElements = new HashSet<costReportElementObject>();
            this.materialComponents = new HashSet<materialComponentObject>();
        }

        public int id { get; set; }
        public int? projectId { get; set; }
        public int? keyDeliverablesId { get; set; }
        public string keyDeliverablesName { get; set; }
        public string docType { get; set; }
        public string docDesc { get; set; }
        public string docNo { get; set; }
        public string docTitle { get; set; }
        public string drawingNo { get; set; }
        public string drawingTitle { get; set; }
        public int? drawingCount { get; set; }
        public DateTime? cutOfDate { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planFinishDate { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualFinishDate { get; set; }
        public string discipline { get; set; }
        public string status { get; set; }
        public string statusText { get; set; }
        public string remark { get; set; }
        public string executor { get; set; }
        public string executorAssignment { get; set; }
        public double? progress { get; set; }
        public string PMName { get; set; }
        public string documentById { get; set; }
        public string documentByName { get; set; }
        public string documentByEmail { get; set; }
        public string documentTrafic { get; set; }
        public string documentStatusLabel { get; set; }
        public int? tenderId { get; set; }
        public int? revision { get; set; }
        public string sharedFolder { get; set; }
        public string pmoNo { get; set; }
        public string contractNo { get; set; }
        public string tenderNo { get; set; }
        public string position { get; set; }
        public string ewpDocType { get; set; }
        public double? weightHours { get; set; }
        public string docTitleDiscipline { get; set; }
        public double? participationValue { get; set; }
        public string remarksActualResults { get; set; }
        public string remarksDesignKpi { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string docTitleText { get; set; }
        public string assignmentType { get; set; }
        public string documentGroup { get; set; }
        public double? parameterValue { get; set; }

        public virtual projectObject project { get; set; }
        public virtual ICollection<constructionServiceObject> constructionServices { get; set; }
        public virtual ICollection<projectCloseOutObject> projectCloseOuts { get; set; }
        public virtual ICollection<costReportElementObject> costReportElements { get; set; }
        public virtual ICollection<materialComponentObject> materialComponents { get; set; }
        public virtual tenderObject tender { get; set; }
        public virtual IEnumerable<projectDocumentReferenceObject> projectDocumentReferences { get; set; }
        public virtual IEnumerable<projectDocumentWeightHoursObject> projectDocumentWeightHours { get; set; }
    }

}
