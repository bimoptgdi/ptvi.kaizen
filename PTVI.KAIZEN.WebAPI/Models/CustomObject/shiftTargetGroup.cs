﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class shiftTargetGroup
    {
        public Guid ID { get; set; }
        public string projectName { get; set; }
        public string groupName { get; set; }
        public string Executor { get; set; }
        public string Supervisor { get; set;}
        public string Planner { get; set; }
        public string ConstructionManager { get; set; }
        public DateTime Date { get; set; }
        public int? percentComplete { get; set; }
        public string outlineNumber { get; set; }
        public int outlineLevel { get; set; }

        public string shiftName { get; set; }

        public List<shiftTarget> shiftTargetList { get; set; }
    }
}