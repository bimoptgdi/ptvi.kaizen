﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class materialStatusObject
    {
        public int id { get; set; }
        public int? materialComponentId { get; set; }
        public string prNo { get; set; }
        public string prItemNo { get; set; }
        public string reservation { get; set; }
        public int? reservationItemNo { get; set; }
        public string codeLookup { get; set; }
        public string specialstock { get; set; }
        public int? reservationWithdrawalQuantity { get; set; }
        public string incotermsPo { get; set; }
        public string trafficMethod { get; set; }
        public string stoNo { get; set; }
        public string deliveryNo { get; set; }
        public string shipmentNo { get; set; }
        public DateTime? shipmentStartDate { get; set; }
        public DateTime? etaSiteDate { get; set; }
        public DateTime? shipmentEndDate { get; set; }
        public string shipmentRoute { get; set; }
        public string location { get; set; }
        public DateTime? actualDeliveryDate { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updateddAte { get; set; }
        public string updatedBy { get; set; }

        public virtual materialComponentObject materialComponent { get; set; }
    }
}
