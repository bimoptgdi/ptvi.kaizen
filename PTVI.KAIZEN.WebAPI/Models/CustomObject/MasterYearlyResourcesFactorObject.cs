﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterYearlyResourcesFactorObject
    {
        public masterYearlyResourcesFactorObject()
        {
            this.projects = new HashSet<projectObject>();
            this.technicalSupportEngineeringFactors = new HashSet<technicalSupportEngineeringFactorObject>();
        }

        public int id { get; set; }
        public int? year { get; set; }
        public string levelCode { get; set; }
        public string levelDescription { get; set; }
        public Nullable<double> minRange { get; set; }
        public Nullable<double> maxRange { get; set; }
        public Nullable<double> engineerDesignerFactor { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual ICollection<projectObject> projects { get; set; }
        public virtual ICollection<technicalSupportEngineeringFactorObject> technicalSupportEngineeringFactors { get; set; }
    }
}
