﻿using PTVI.KAIZEN.WebAPI.K2Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class BudgetApproval
    {
        public int OID { get; set; }
        public Nullable<int> PROJECTID { get; set; }
        public string PROJECTNO { get; set; }
        public string PROJECTDESCRIPTION { get; set; }
        public string PROJECTMANAGERID { get; set; }
        public string PROJECTMANAGERNAME { get; set; }
        public string PROJECTMANAGEREMAIL { get; set; }
        public Nullable<double> BUDGETAMOUNT { get; set; }
        public string BUDGETSTATUS { get; set; }

        public string FILENAME { get; set; }
        public string TYPE { get; set; }
        public string VALUE { get; set; }
        public string Remark { get; set; }
        public List<BudgetApproval> CATEGORYOFWORK { get; set; }
        public List<BudgetApproval> TYPEOFREQUESTEDWORK { get; set; }
        public List<BudgetApproval> DETAILOFWORK { get; set; }
        public List<BudgetApproval> PROJECTTYPE { get; set; }
        public List<USERASSIGNMENT> USERPARTICIPANTS { get; set; }
        public List<USERASSIGNMENT> USERDISTRIBUTIONS { get; set; }
        public List<SUPPORTINGDOCUMENT> SUPPORTINGDOCUMENTS { get; set; }
        //public List<K2WorklistItem> K2WorklistItems { get; set; }
        public int? WF_ID { get; set; }
        public List<K2History> K2Histories { get; set; }
        public BudgetApproval PROJECTBUDGETAPPROVAL { get; set; }
    }

}