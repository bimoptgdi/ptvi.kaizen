﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Controllers;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class materialComponentSimpleObject
    {
        public materialComponentSimpleObject()
        {
            this.materialPurchases = new HashSet<materialPurchaseObject>();
            this.materialPlans = new HashSet<materialPlanObject>();
            this.materialSchedules = new HashSet<materialScheduleObject>();
            this.materialStatus = new HashSet<materialStatusObject>();
            this.tenderMaterials = new HashSet<tenderMaterialObject>();
        }

        public int id { get; set; }
        public Nullable<int> projectId { get; set; }
        public Nullable<int> projectdocumentId { get; set; }
        public string projectNo { get; set; }
        public string ewpNo { get; set; }
        public string mrNo { get; set; }
        public string wbsNo { get; set; }
        public string network { get; set; }
        public string networkActivity { get; set; }
        public string lineItem { get; set; }
        public string material { get; set; }
        public string materialDesc { get; set; }
        public Nullable<int> orderQuantity { get; set; }
        public Nullable<int> withdrawnQuantity { get; set; }
        public Nullable<int> receivedQuantity { get; set; }
        public string uom { get; set; }
        public Nullable<System.DateTime> planOnsite { get; set; }
        public string engineer { get; set; }
        public string reservation { get; set; }
        public Nullable<int> reservationItemNo { get; set; }
        public string prNo { get; set; }
        public string prItemNo { get; set; }
        public string poNo { get; set; }
        public string poItemNo { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string createdBy { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual PROJECT project { get; set; }
        public virtual ICollection<materialPurchaseObject> materialPurchases { get; set; }
        public virtual PROJECTDOCUMENT projectDocument { get; set; }
        public virtual ICollection<materialPlanObject> materialPlans { get; set; }
        public virtual ICollection<materialScheduleObject> materialSchedules { get; set; }
        public virtual ICollection<materialStatusObject> materialStatus { get; set; }
        public virtual ICollection<tenderMaterialObject> tenderMaterials { get; set; }

    }
}
