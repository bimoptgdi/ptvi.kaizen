using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class requestOtherUserObject
    {
        public int id { get; set; }
        public int? projectId { get; set; }
        public string requestNo { get; set; }
        public DateTime? requestDate { get; set; }
        public string mailToId { get; set; }
        public string mailToName { get; set; }
        public string mailTo { get; set; }
        public string mailCcId { get; set; }
        public string mailCcName { get; set; }
        public string mailCc { get; set; }
        public string mailFromId { get; set; }
        public string mailFromName { get; set; }
        public string mailFrom { get; set; }
        public string mailSubject { get; set; }
        public string mailBody { get; set; }
        public DateTime? responseRequiredBy { get; set; }
        public string priority { get; set; }
        public string status { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string priorityText { get; set; }

        public virtual projectObject project { get; set; }
    }
}
