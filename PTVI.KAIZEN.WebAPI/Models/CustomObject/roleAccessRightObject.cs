﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class roleAccessRightObject
    {
        public int id { get; set; }
        public int roleID { get; set; }
        public int accessRightID { get; set; }
        public int isReadonly { get; set; }

        public virtual accessRightObject accessRight { get; set; }
        public virtual roleObject role { get; set; }
    }
}
