﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class materialPurchaseObject
    {
        public int id { get; set; }
        public int? materialComponentId { get; set; }
        public string prNo { get; set; }
        public string prItemNo { get; set; }
        public DateTime? prIssuedDate { get; set; }
        public DateTime? poRaisedDate { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual materialComponentObject materialComponent { get; set; }
    }
}
