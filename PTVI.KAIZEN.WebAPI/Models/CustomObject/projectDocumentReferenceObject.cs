﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class projectDocumentReferenceObject
    {
        public int id { get; set; }
        public int? projectDocRefId { get; set; }

        public virtual getProjectDocumentRefObject projectDocRef { get; set; }
    }

    public partial class getProjectDocumentRefObject
    {

        public int id { get; set; }
        public string docNo { get; set; }
        public string docTitle { get; set; }
        public string position { get; set; }
        public string ewpDocType { get; set; }

    }

}
