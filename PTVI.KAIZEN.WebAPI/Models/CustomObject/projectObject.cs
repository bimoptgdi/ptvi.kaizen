﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class projectObject
    {
        public projectObject()
        {
            this.costReportElements = new HashSet<costReportElementObject>();
            this.materialComponents = new HashSet<materialComponentObject>();
            this.otherDepartmentTasks = new HashSet<otherDepartmentTaskObject>();
            //this.projectDocuments = new HashSet<projectDocumentObject>();
            this.projectManagements = new HashSet<projectManagementObject>();
            this.timesheets = new HashSet<timesheetObject>();
            this.userAssignments = new HashSet<userAssignmentObject>();
        }

        public int id { get; set; }
        public string projectNo { get; set; }
        public string projectDescription { get; set; }
        public string ownerId { get; set; }
        public string ownerName { get; set; }
        public string ownerEmail { get; set; }
        public string sponsorId { get; set; }
        public string sponsorName { get; set; }
        public string sponsorEmail { get; set; }
        public object ownerSponsorName { get; set; }
        public string projectManagerId { get; set; }
        public string projectManagerName { get; set; }
        public string projectManagerEmail { get; set; }
        public string projectEngineerId { get; set; }
        public string projectEngineerName { get; set; }
        public string projectEngineerEmail { get; set; }
        public string maintenanceRepId { get; set; }
        public string maintenanceRepName { get; set; }
        public string maintenanceRepEmail { get; set; }
        public string operationRepId { get; set; }
        public string operationRepName { get; set; }
        public string operationRepEmail { get; set; }
        public int? areaInvolvedId { get; set; }
        public string projectCategory { get; set; }
        public string projectType { get; set; }
        public string pmoNo { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planFinishDate { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualFinishDate { get; set; }
        public double? totalCost { get; set; }
        public double? actualCost { get; set; }
        public double? commitment { get; set; }
        public double? forecast { get; set; }
        public string shareFolderDoc { get; set; }
        public string iptProjectId { get; set; }
        public string pmTrafic { get; set; }
        public DateTime? pmTraficDate { get; set; }
        public string esTrafic { get; set; }
        public DateTime? esTraficDate { get; set; }
        public List<PROJECTDOCUMENT> esTraficPic { get; set; }
        public string esTraficPicTextEwp { get; set; }
        public string esTraficPicTextDwg { get; set; }
        public string materialTrafic { get; set; }
        public IEnumerable<materialByEngineerObject> materialByEngineer { get; set; }
        public string materialTraficPic { get; set; }
        public DateTime? materialTraficDate { get; set; }
        public int? materialTraficCount { get; set; }
        public string consTrafic { get; set; }
        public DateTime? tenderInstallationsMaxDate { get; set; }
        public DateTime? tenderPunchlistsMaxDate { get; set; }
        public IEnumerable<PROJECTDOCUMENT> projectDocumentTD { get; set; }
        //public IEnumerable<tenderInstallationObject> tenderInstallations { get; set; }
        //public IEnumerable<tenderPunchlistObject> tenderPunchlists { get; set; }
        public DateTime? contractAdminDate { get; set; }
        public DateTime? consTraficDate { get; set; }
        public string consTraficPic { get; set; }
        public string handoverCloseOutTrafic { get; set; }
        public DateTime? handoverCloseOutTraficDate { get; set; }
        public string handoverCloseOutTraficRole { get; set; }
        public string handoverCloseOutTraficPic { get; set; }
        public string roleInProject { get; set; }
        public string positionName { get; set; }
        public string status { get; set; }
        public object projectManagementModuleChartMaxMinDate { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string ownerSponsorToString { get; set; }
        public string userAssignmentsToString { get; set; }
        public string PMPEToString { get; set; }
        public int? momewrid { get; set; }
        public virtual areaInvolvedObject areaInvolved { get; set; }
        public virtual ICollection<costReportElementObject> costReportElements { get; set; }
        public virtual ICollection<materialComponentObject> materialComponents { get; set; }
        public virtual IEnumerable<otherDepartmentTaskObject> otherDepartmentTasks { get; set; }
        //public virtual ICollection<projectDocumentObject> projectDocuments { get; set; }
        public virtual IEnumerable<projectManagementObject> projectManagements { get; set; }
        public virtual ICollection<timesheetObject> timesheets { get; set; }
        public virtual IEnumerable<userAssignmentObject> userAssignments { get; set; }
        public virtual IEnumerable<userAssignmentObject> uaCoPic { get; set; }
        public virtual IEnumerable<userAssignmentObject> otherPositions { get; set; }
    }
}
