﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class EWRMomDTO
    {
        public int OID { get; set; }
        public int EWRREQUESTID { get; set; }
        public string EWRNO { get; set; }
        public string BACKGROUND { get; set; }
        public string DATAREQUIRED { get; set; }
        public string PARTICIPANTS { get; set; }
        public string DISTRIBUTIONS { get; set; }
        public Nullable<System.DateTime> MOMDATE { get; set; }
        public Nullable<System.DateTime> MOMTIME { get; set; }
        public Nullable<System.DateTime> ACTIONTIMELINE { get; set; }
        public string PROJECTDELIVERABLE { get; set; }
        public string PROJECTMANAGER { get; set; }
        public string PROJECTMANAGERBN { get; set; }
        public string PROJECTMANAGEREMAIL { get; set; }
        public string PROJECTTITLE { get; set; }
        public string RECORDEDBY { get; set; }
        public string RECORDEDBYBN { get; set; }
        public string RECORDEDBYEMAIL { get; set; }
        public string SCOPEOFWORK { get; set; }
        public string SPONSORCP { get; set; }
        public string VENUE { get; set; }
        public List<MOMDetailDTO> PROJECTTYPE { get; set; }
        public string OPERATIONALCP { get; set; }
        public string OPERATIONALCPNAME { get; set; }
        public string OPERATIONALCPEMAIL { get; set; }
        public string OPERATIONALCPBN { get; set; }
        public string OPERATIONALCP2 { get; set; }
        public string OPERATIONALCP2NAME { get; set; }
        public string OPERATIONALCP2EMAIL { get; set; }
        public string OPERATIONALCP2BN { get; set; }
        public string MOMEWRNO { get; set; }
    }

    public class MOMDetailDTO
    {
        public int OID { get; set; }
        public int EWRREQUESTID { get; set; }
        public string TYPE { get; set; }
        public string VALUE { get; set; }
        public string DESCRIPTION { get; set; }
    }
}