﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class setupCloseOutChecklistObject
    {
        public int id { get; set; }
        public int? closeOutRoleId { get; set; }
        public int? seq { get; set; }
        public string checkListItem { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }        
        public string updatedBy { get; set; }
    }
}