﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Controllers;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class materialComponentObject
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private KAIZENController ipc = new KAIZENController();
        public materialComponentObject()
        {
            this.tenderMaterials = new HashSet<tenderMaterialObject>();
        }

        public int id { get; set; }
        public int? projectId { get; set; }
        public int? projectDocumentId { get; set; }
        public string projectNo { get; set; }
        public string ewpNo { get; set; }
        public string mrNo { get; set; }
        public string wbsNo { get; set; }
        public string network { get; set; }
        public string networkActivity { get; set; }
        public string lineItem { get; set; }
        public string material { get; set; }
        public string materialDesc { get; set; }
        public int? orderQuantity { get; set; }
        public int? withdrawnQuantity { get; set; }
        public int? receivedQuantity { get; set; }
        public string uom { get; set; }
        public string engineer { get; set; }
        public string reservation { get; set; }
        public int? reservationItemNo { get; set; }
        public string prNo { get; set; }
        public string pritemNo { get; set; }
        public string poNo { get; set; }
        public string poItemNo { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public virtual projectObject project { get; set; }
        public virtual ICollection<materialPurchaseObject> materialPurchases
        {
            get
            {
                var materialPurchases = db.MATERIALPURCHASEs.Where(x => x.MATERIALCOMPONENTID == this.id).Select(y => new materialPurchaseObject()
                {
                    id = y.OID,
                    poRaisedDate = y.PORAISEDDATE,
                    prIssuedDate = y.PRISSUEDDATE,
                    prNo = y.PRNO,
                    prItemNo = y.PRITEMNO
                }).ToList();
                if (materialPurchases.Count > 0)
                {
                    this.prIssuedDate = materialPurchases[0].prIssuedDate;
                    this.poRaisedDate = materialPurchases[0].poRaisedDate;
                    this.prMPNo = materialPurchases[0].prNo;
                    this.prItmNo = materialPurchases[0].prItemNo;
                }
                return materialPurchases;
            }
        }
        public DateTime? prIssuedDate { get; set; }
        public DateTime? poRaisedDate { get; set; }
        public string prMPNo { get; set; }
        public string prItmNo { get; set; }
        public virtual IEnumerable<materialPurchaseGroupObject> materialPurchaseGroup { get; set; }
        public string prGroupCode { get {
                var gc = materialPurchaseGroup != null ? materialPurchaseGroup.FirstOrDefault() : null;
                return gc != null ? gc.prGroupCode : ""; } set { } }
        public string prGroupDesc { get {
                var gd = materialPurchaseGroup != null ? materialPurchaseGroup.FirstOrDefault() : null;
                return gd != null?gd.prGroupDesc : ""; } set { } }
        public virtual ICollection<materialScheduleObject> materialSchedules { get
            {
                var materialSchedules = db.MATERIALSCHEDULEs.Where(x => x.MATERIALCOMPONENTID == this.id).Select(y => new materialScheduleObject()
                {
                    id = y.OID,
                    orderedQuantity = y.ORDEREDQUANTITY,
                    receivedQuantity = y.RECEIVEDQUANTITY
                }).ToList();
                if (materialSchedules.Count > 0)
                {
                    this.orderedQuantityMS = materialSchedules[0].orderedQuantity;
                    this.receivedQuantityMS = materialSchedules[0].receivedQuantity;
                }
                return materialSchedules;
            }
        }
        public double? orderedQuantityMS { get; set; }
        public double? receivedQuantityMS { get; set; }
        public virtual ICollection<materialStatusObject> materialStatus
        {
            get
            {
                var materialStatus = db.MATERIALSTATUS.Where(x => x.MATERIALCOMPONENTID == this.id).Select(y => new materialStatusObject()
                {
                    id = y.OID,
                    shipmentEndDate = y.SHIPMENTENDDATE,
                    etaSiteDate = y.ETASITEDATE,
                    actualDeliveryDate = y.ACTUALDELIVERYDATE,
                    trafficMethod = y.TRAFFICMETHOD,
                    location = y.LOCATION
                }).ToList();
                if (materialStatus.Count > 0)
                {
                    this.shipmentEndDate = materialStatus[0].shipmentEndDate;
                    this.etaSiteDateMSt = materialStatus[0].etaSiteDate;
                    this.shipmentEndDate = materialStatus[0].shipmentEndDate;
                    this.trafficMethodMSt = materialStatus[0].trafficMethod;
                    this.LocationMSt = materialStatus[0].location;
                }
                return materialStatus;
            }
        }
        public virtual ICollection<tenderMaterialObject> tenderMaterials { get; set; }
        public DateTime? shipmentEndDate { get; set; }
        public DateTime? etaSiteDateMSt { get; set; }
        public DateTime? actualDeliveryDateMSt { get; set; }
        public String trafficMethodMSt { get; set; }
        public String LocationMSt { get; set; }
        public virtual ICollection<materialPlanObject> setMaterialPlan{ get; set; }
        public virtual ICollection<materialPlanObject> materialPlan
        {
            get
            {
                var materialPlan = db.MATERIALPLANs.Where(x => x.MATERIALCOMPONENTID == this.id).Select(y => new materialPlanObject()
                {
                    id = y.OID,
                    prPlanDate = y.PRPLANDATE,
                    rfqDate = y.RFQDATE,
                    poPlanDate = y.POPLANDATE,
                    trafficofficername = y.TRAFFICOFFICERNAME
                }).ToList();
                if (materialPlan.Count > 0)
                {
                    //this.prPlanDate = materialPlan[0].prPlanDate;
                    this.rfqDate = materialPlan[0].rfqDate;
                    //this.poPlanDate = materialPlan[0].poPlanDate;
                    this.officerNameMp = materialPlan[0].trafficofficername;
                }
                return materialPlan;
            }
        }
        public virtual ICollection<materialByEngineerObject> materialByEngineer
        {
            get
            {
                var materialByEngineer = db.MATERIALBYENGINEERs.Where(x => x.MRNO == this.mrNo && x.PROJECTID==this.projectId).Select(y => new materialByEngineerObject()
                {
                    id = y.OID,
                    prPlanDate = y.PRPLANDATE,
                    poPlanDate = y.POPLANDATE,
                    onSitePlanDate = y.ONSITEPLANDATE
                }).ToList();
                if (materialByEngineer.Count > 0)
                {
                    this.prPlanDate = materialByEngineer[0].prPlanDate;
                    this.poPlanDate = materialByEngineer[0].poPlanDate;
                    this.planOnSite = materialByEngineer[0].onSitePlanDate;
                }
                return materialByEngineer;
            }
        }
        public DateTime? planOnSite { get; set; }
        public DateTime? prPlanDate { get; set; }
        public DateTime? rfqDate { get; set; }
        public DateTime? poPlanDate { get; set; }
        public string officerNameMp { get; set; }
        public string documentTraficPR
        {
            get
            {
                return prIssuedDate != null? constants.TRAFFICLIGHT_GREEN: prPlanDate != null ? ipc.traficStatus(prPlanDate.GetValueOrDefault(), prIssuedDate, "") : constants.TRAFFICLIGHT_GREY;
            }
        }
        public string documentTraficPO
        {
            get
            {
                return poRaisedDate != null ? constants.TRAFFICLIGHT_GREEN : poPlanDate != null ? ipc.traficStatus(poPlanDate.GetValueOrDefault(), poRaisedDate, "") : constants.TRAFFICLIGHT_GREY;
            }
        }
        public string documentTraficETASite
        {
            get
            {
                return etaSiteDateMSt != null ? constants.TRAFFICLIGHT_GREEN : shipmentEndDate != null ? ipc.traficStatus(shipmentEndDate.GetValueOrDefault(), etaSiteDateMSt, "") : constants.TRAFFICLIGHT_GREY;
            }
        }
        public string documentTraficOSS
        {
            get
            {
                return shipmentEndDate != null ? constants.TRAFFICLIGHT_GREEN : planOnSite != null ? ipc.traficStatus(planOnSite.GetValueOrDefault(), shipmentEndDate, "") : constants.TRAFFICLIGHT_GREY;
            }
        }

    }
}
