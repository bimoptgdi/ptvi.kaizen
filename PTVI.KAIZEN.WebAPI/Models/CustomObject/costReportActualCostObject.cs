﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class costReportActualCostObject
    {
        public int id { get; set; }
        public int? costReportElementId { get; set; }
        public string projectNo { get; set; }
        public string wbsNo { get; set; }
        public string network { get; set; }
        public string objectType { get; set; }
        public int? year { get; set; }
        public int? month { get; set; }
        public double? amount { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updateddate { get; set; }
        public string updatedby { get; set; }

        public virtual costReportElementObject costReportElement { get; set; }
    }
}
