﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class employeeDetails
    {
        public string BADGE_NUMBER { get; set; }
        public string NAME { get; set; }
        public DateTime DATE_OF_HIRE { get; set; }
        public string POINT_OF_HIRE { get; set; }
        public string DROP_POINT { get; set; }
        public string DEPARTMENT { get; set; }
        public string COST_CENTER { get; set; }
        public string BIRTH_DATE { get; set; }
        public DateTime DATE_OF_BIRTH { get; set; }
        public string GENDER { get; set; }
        public string NATIONALITY { get; set; }
        public string WORK_LOCATION { get; set; }
        public string MARITAL_STATUS { get; set; }
        public string EMPLOYEE_TYPE { get; set; }
        public string HOME_ADDRESS { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public string HOME_PHONE { get; set; }
        public string OFFICE_PHONE { get; set; }
        public string MOBILE_PHONE { get; set; }
        public string JAMSOSTEK { get; set; }
        public string LEVEL_GROUP { get; set; }
        public string PAY_GROUP_CODE { get; set; }
        public string PAY_GROUP { get; set; }
        public string POS_ID { get; set; }
        public string POSITION_TITLE { get; set; }
        public string SUPERIOR_ID { get; set; }
        public string SUPERIOR_NAME { get; set; }
        public string SUPERIOR_TITLE { get; set; }
        public string SUPERIOR_LEVEL { get; set; }
        public DateTime RETIREMENT_DATE { get; set; }
        public string PTKP { get; set; }
        public string PTKP_VALUE { get; set; }
        public string NPWP { get; set; }
        public string AWARD_CODE { get; set; }
        public string PATTERN { get; set; }
        public string EXPAT_IND { get; set; }
        public string COMPANYCODE { get; set; }
        public string NO_KK { get; set; }
        public string KTP { get; set; }
        public string USERNAME { get; set; }
    }
}