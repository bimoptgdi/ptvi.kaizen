﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class tenderEquipmentObject
    {
        public int id { get; set; }
        public int? tenderId { get; set; }
        public string equipment { get; set; }
        public int? quantity { get; set; }
        public string status { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdby { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual tenderObject tender { get; set; }
    }
}
