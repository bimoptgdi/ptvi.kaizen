﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class setupCloseOutDocumentObject
    {
        public int id { get; set; }
        public int? closeOutRoleId { get; set; }
        public string documentType { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string createdBy { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public string updatedBy { get; set; }


    }
}
