﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterPerformanceValueDCObject
    {
        public int id { get; set; }
        public Nullable<int> year { get; set; }
        public string referenceCode { get; set; }
        public string performanceValue { get; set; }
        public double? min { get; set; }
        public double? max { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
    }
}