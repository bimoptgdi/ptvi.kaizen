﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class shiftTargetPlan
    {
        public projectShiftObject projectShift { get; set; }
        public List<shiftTargetGroup> shiftTargetGroupList { get; set; }
    }
}