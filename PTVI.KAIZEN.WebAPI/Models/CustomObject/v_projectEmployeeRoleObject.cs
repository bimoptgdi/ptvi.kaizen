using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class v_projectEmployeeRoleObject
    {
        public int id { get; set; }
        public string projectno { get; set; }
        public string projectdescription { get; set; }
        public System.DateTime? planstartdate { get; set; }
        public System.DateTime? planfinishdate { get; set; }
        public System.DateTime? actualstartdate { get; set; }
        public System.DateTime? actualfinishdate { get; set; }
        public string employeeid { get; set; }
        public string employeename { get; set; }
        public int? rolelevel { get; set; }
        public string rolename { get; set; }
    }
}
