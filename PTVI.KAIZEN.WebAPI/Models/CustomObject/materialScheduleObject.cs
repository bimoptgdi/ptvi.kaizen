﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class materialScheduleObject
    {
        public int id { get; set; }
        public int? materialComponentId { get; set; }
        public string prNo { get; set; }
        public string prItemNo { get; set; }
        public DateTime? deliveryDate { get; set; }
        public double? orderedQuantity { get; set; }
        public string currency { get; set; }
        public double? unitPrice { get; set; }
        public string unitQuantity { get; set; }
        public double? totalPrice { get; set; }
        public double? deliveredQuantity { get; set; }
        public double? receivedQuantity { get; set; }
        public string buyer { get; set; }
        public string vendor { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual materialComponentObject materialComponent { get; set; }
    }
}
