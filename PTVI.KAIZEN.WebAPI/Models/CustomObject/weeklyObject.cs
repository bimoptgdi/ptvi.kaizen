﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class weeklyObject
    {
        public weeklyObject()
        {
            this.timesheets = new HashSet<timesheetObject>();
        }

        public int id { get; set; }
        public int? year { get; set; }
        public int? month { get; set; }
        public int? week { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
        public string createdBy { get; set; }
        public DateTime? createdDate { get; set; }
        public string updatedBy { get; set; }
        public DateTime? updatedDate { get; set; }

        public virtual ICollection<timesheetObject> timesheets { get; set; }
    }
}
