﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class GeneralParameterController : KAIZENController
    {
        // GET api/GeneralParameter
        public IEnumerable<GENERALPARAMETER> GetGENERALPARAMETERs()
        {
            return db.GENERALPARAMETERs.AsEnumerable();
        }

        // GET api/GeneralParameter/5
        public GENERALPARAMETER GetGENERALPARAMETER(int id)
        {
            GENERALPARAMETER generalparameter = db.GENERALPARAMETERs.Find(id);
            if (generalparameter == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return generalparameter;
        }

        // PUT api/GeneralParameter/5
        public HttpResponseMessage PutGENERALPARAMETER(int id, GENERALPARAMETER generalparameter)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != generalparameter.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            generalparameter.UPDATEDBY = GetCurrentNTUserId();
            generalparameter.UPDATEDDATE = DateTime.Now;
            db.Entry(generalparameter).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, generalparameter);
        }

        // POST api/GeneralParameter
        [HttpPost]
        [ActionName("PostGENERALPARAMETER")]
        public HttpResponseMessage PostGENERALPARAMETER(int param, GENERALPARAMETER generalparameter)
        {
            if (ModelState.IsValid)
            {
                generalparameter.CREATEDBY = GetCurrentNTUserId();
                generalparameter.CREATEDDATE = DateTime.Now;
                db.GENERALPARAMETERs.Add(generalparameter);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, generalparameter);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = generalparameter.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/GeneralParameter/5
        public HttpResponseMessage DeleteGENERALPARAMETER(int id)
        {
            GENERALPARAMETER generalparameter = db.GENERALPARAMETERs.Find(id);
            if (generalparameter == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.GENERALPARAMETERs.Remove(generalparameter);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, generalparameter);
        }

        #region CustomAction
        // api/generalparameter/FindByTitle/param
        [HttpGet]
        [ActionName("FindByTitle")]
        public IEnumerable<GENERALPARAMETER> FindByTitle(string param)
        {
            return db.GENERALPARAMETERs.Where(x => x.TITLE == param);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}