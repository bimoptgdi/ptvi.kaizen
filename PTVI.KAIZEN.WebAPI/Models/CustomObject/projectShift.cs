﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class projectShift
    {
        public int id { get; set; }
        public string projectName { get; set; }
        public string shift { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}