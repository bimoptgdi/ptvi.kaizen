﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class menuAccessRightObject
    {
        public int id { get; set; }
        public int menuID { get; set; }
        public int accessRightID { get; set; }
    
        public virtual accessRightObject accessRight { get; set; }
        public virtual menuObject menu { get; set; }
    }
}
