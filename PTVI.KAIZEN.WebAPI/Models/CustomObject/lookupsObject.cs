﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class lookupsObject
    {
        public int id { get; set; }
        public string type { get; set; }
        public string text { get; set; }
        public string value { get; set; }
        public string description { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }

    }
}
