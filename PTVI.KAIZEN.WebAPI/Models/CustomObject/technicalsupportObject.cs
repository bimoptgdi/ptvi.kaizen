﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class technicalSupportObject
    {
        public technicalSupportObject()
        {
            this.technicalSupportAreas = new HashSet<technicalSupportAreaObject>();
            this.userAssignments = new HashSet<userAssignmentObject>();
        }

        public int id { get; set; }
        public string tsNo { get; set; }
        public string tsDesc { get; set; }
        public DateTime? requestDate { get; set; }
        public string problemStatement { get; set; }
        public string sharedFolder { get; set; }
        public string status { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string requestBy;
        
        public virtual ICollection<technicalSupportAreaObject> technicalSupportAreas { get; set; }
        public virtual ICollection<userAssignmentObject> userAssignments { get; set; }
        public virtual ICollection<userAssignmentObject> engineerLists { get; set; }
        public virtual ICollection<userAssignmentObject> designerLists { get; set; }

    }
}
