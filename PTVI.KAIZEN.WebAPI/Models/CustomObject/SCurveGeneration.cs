﻿using PTVI.KAIZEN.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class SCurveGeneration
    {
        public int id { get; set; }
        public Nullable<int> projectId { get; set; }
        public Nullable<int> estimationShiftID { get; set; }

        public projectShiftObject projectShift 
        {
            get
            {
                ShiftController shController = new ShiftController();

                return shController.ProjectShiftByID(estimationShiftID.Value);
            }        
        }

        public Nullable<int> totalShift { get; set; }
        public Nullable<int> shiftNoInProgress { get; set; }
        public string status { get; set; }
        public string errorMessage { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> finishDate { get; set; }
    }
}