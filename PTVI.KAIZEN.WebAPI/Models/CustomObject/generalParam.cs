﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models
{
    public class GeneralParam
    {
        public String reason { get; set; }
        public int oid { get; set; }
        public String curr_role { get; set; }

        public ROLE role { get; set; }
        public List<USER> user { get; set; }
        public List<ACCESSRIGHT> accessright { get; set; }
        public List<ROLEACCESSRIGHT> roleaccessright { get; set; }

        public String item_name { get; set; }
    }
}