﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class technicalSupportAreaObject
    {
        public int id { get; set; }
        public int? technicalSupportId { get; set; }
        public int? areaInvolvedId { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual areaInvolvedObject areaInvolved { get; set; }
        public virtual technicalSupportObject technicalSupport { get; set; }
    }
}
