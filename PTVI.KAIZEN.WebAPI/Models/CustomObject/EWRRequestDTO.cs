﻿using PTVI.KAIZEN.WebAPI.K2Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class EWRRequestDTO
    {
        public int OID { get; set; }
        public string EWRNO { get; set; }
        public string SUBJECT { get; set; }
        public string AREA { get; set; }
        public Nullable<System.DateTime> DATEISSUED { get; set; }
        public string ACCCODE { get; set; }
        public string NETWORKNO { get; set; }
        public Nullable<decimal> BUDGETALLOC { get; set; }
        public Nullable<System.DateTime> DATECOMPLETION { get; set; }
        public string PROBLEMSTATEMENT { get; set; }
        public string OBJECTIVE { get; set; }
        public string ASSIGNEDPM { get; set; }
        public string PROJECTSPONSOR { get; set; }
        public string PROJECTSPONSORBN { get; set; }
        public string SPONSORLOCATION { get; set; }
        public string SPONSORPHONENO { get; set; }
        public string OPERATIONALCP { get; set; }
        public string PROJECTTITLE { get; set; }
        public string STATUS { get; set; }
        public string OPERATIONALCPEMAIL { get; set; }
        public string OPERATIONALCPBN { get; set; }
        public string OPERATIONALCP2 { get; set; }
        public string OPERATIONALCP2EMAIL { get; set; }
        public string OPERATIONALCP2BN { get; set; }
        public string SPONSOREMAIL { get; set; }
        public string REQUESTORBN { get; set; }
        public string REQUESTORNAME { get; set; }
        public string REQUESTOREMAIL { get; set; }
        public List<EWRRequestDetailDTO> CATEGORYOFWORK { get; set; }
        public List<EWRRequestDetailDTO> TYPEOFREQUESTEDWORK { get; set; }
        public List<EWRRequestDetailDTO> DETAILOFWORK { get; set; }
        public List<EWRRequestDetailDTO> PROJECTTYPE { get; set; }
        public List<USERASSIGNMENT> USERPARTICIPANTS { get; set; }
        public List<USERASSIGNMENT> USERDISTRIBUTIONS { get; set; }
        public List<SUPPORTINGDOCUMENT> SUPPORTINGDOCUMENTS { get; set; }
        public string Remark { get; set; }
        //public List<K2WorklistItem> K2WorklistItems { get; set; }
        public int? WF_ID { get; set; }
        public List<K2History> K2Histories { get; set; }
        public EWRMomDTO EWRMOM { get; set; }
    }

    public class EWRRequestDetailDTO
    {
        public int OID { get; set; }
        public int EWRREQUESTID { get; set; }
        public string TYPE { get; set; }
        public string VALUE { get; set; }
        public string DESCRIPTION { get; set; }
    }
}