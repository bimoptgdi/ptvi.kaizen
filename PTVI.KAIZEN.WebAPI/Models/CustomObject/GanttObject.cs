﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class GanttObject
    {
        public string originalId { get; set; }
        public string id { get; set; }

        public String taskOutlineNum { get; set; }
        public int taskOutlineLevel { get; set; }

        public bool expanded { get; set; }
        public Nullable<DateTime> end { get; set; }
        public int? orderId { get; set; }
        public string parentId { get; set; }

        public int? percentCompletePreviousShift { get; set; }
        public int? percentComplete { get; set;}
        public int? percentCompleteBaseline { get; set; }
        public Nullable<DateTime> start { get; set; }
        public bool summary { get; set; }
        public string title { get; set; }

        public string parentOriginalId {get;set;}

        public Nullable<DateTime> actualStart { get; set; }
        public Nullable<DateTime> actualEnd { get; set; }
        public double? duration { get; set; }
        public double? durationDay { get; set; }
        public string executor { get; set; }

        public double percentCompleteF
        {
            get
            {
                return (double)this.percentComplete / 100.0;
            }
        }

        public int? percentComplete100
        {
            get
            {
                return this.percentComplete;
            }
        }
    }
}