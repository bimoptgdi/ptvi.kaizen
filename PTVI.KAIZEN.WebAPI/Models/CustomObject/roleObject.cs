﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class roleObject
    {
        public roleObject()
        {
            this.roleAccessRights = new HashSet<roleAccessRightObject>();
            this.userRoles = new HashSet<userRoleObject>();
        }
    
        public int id { get; set; }
        public string roleName { get; set; }
        public string description { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }
    
        public virtual ICollection<roleAccessRightObject> roleAccessRights { get; set; }
        public virtual ICollection<userRoleObject> userRoles { get; set; }
    }
}
