using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.K2Objects;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class psdApprovalObject
    {
        public int id { get; set; }
        public int? projectId { get; set; }
        public string remarks { get; set; }
        public int? documentid { get; set; }
        public string fileNamePsd { get; set; }
        public string refDocId { get; set; }
        public string approvalStatus { get; set; }
        public string submittedBy { get; set; }
        public DateTime? submittedDate { get; set; }
        public DateTime? completionDate { get; set; }
        public int? wfId { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string k2CurrentActivityName { get; set; }

        public virtual projectObject project { get; set; }
        public List<K2History> K2Histories { get; set; }

    }
}
