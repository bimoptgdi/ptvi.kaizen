﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class menuObject
    {
        public menuObject()
        {
            this.menu1 = new HashSet<menuObject>();
            this.menuAccessRights = new HashSet<menuAccessRightObject>();
        }

        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int? parentID { get; set; }
        public string fileName { get; set; }
        public bool? isActive { get; set; }
        public int? seq { get; set; }
        public string image { get; set; }
        public bool? showTitle { get; set; }
        public string createdBy { get; set; }
        public DateTime? createdDate { get; set; }
        public string updatedBy { get; set; }
        public DateTime? updatedDate { get; set; }

        public virtual IEnumerable<menuObject> menu1 { get; set; }
        public virtual menuObject menu2 { get; set; }
        public virtual IEnumerable<menuAccessRightObject> menuAccessRights { get; set; }
    }
}
