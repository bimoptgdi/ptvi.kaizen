﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class constants
    {
        // Upload Excel Misc
        public const string MATERIAL_EXCESS = "Material Excess";


        //role from rolename in ROLE-rolename to SETUPCLOSEOUTROLE-description

        //INDIVIDUAL_REPORT_ENGINEER
        public const string INDIVIDUAL_REPORT_ENGINEER = "INDIVIDUAL_REPORT_ENGINEER";

        //submitted close out project
        public const string SUBMIT_CLOSEOUT = "SUBMIT_CLOSEOUT";

        //submitted close out projectType
        public const string OPERATING = "OPERATING";
        public const string CAPITAL = "CAPITAL";

        //rejected role by PM from setup closeout role
        public const string PM = "Project Manager";
        public const string LD_C_M_ES = "Lead Designer Mechanical Civil";
        public const string LD_E_I_ES = "Lead Designer Electrical Instrument";
        public const string MC = "Material Coordinator";
        public const string PC = "Project Controller";
        public const string DC = "Document Controller";

        public const string PMF = "PM Finalize";

        public const string REJECT_CLOSEOUT = "REJECT_CLOSEOUT";
        public const string APPROVE_CLOSEOUT = "APPROVE_CLOSEOUT";

        public const string DOCUMENT_REPOSITORY_CID = "DOCUMENT_REPOSITORY_CID";
        public const string DOCUMENT_EXP_NOTIF_RECIPIENT = "DOCUMENT_EXP_NOTIF_RECIPIENT";
        public const string LIBRARY_DOCUMENT_DIRECTORY = "LIBRARY_DOCUMENT_DIRECTORY";
        public const string EMPLOYEE_API_URL = "EMPLOYEE_API_URL";
        public const string EMPLOYEE_PHOTO_LOCATION = "EMPLOYEE_PHOTO_LOCATION";
        public const string COMMON_SERVICE_API_URL = "COMMONSERVICE_API_URL";
        public const string CODE_EMPLOYEE_EXTERNALQRY = "CODE_EMPLOYEE_EXTERNALQRY";
        public const string CODE_EMPLOYEE_ONLY_EXTERNALQRY = "CODE_EMPLOYEE_ONLY_EXTERNALQRY";
        public const string ADR_WEB_API_ODATA = "WEB_API_URL_ODATA";
        public const string CONTRACTWITHPM_API_URL = "CONTRACTWITHPM_API_URL";
        public const string MATERIAL = "MATERIAL";
        //ROLE
        public const string ROLEADMIN = "ADM";

        //General Parameter
        public const string STARTWEEKLY = "STARTWEEKLY";
        public const string DISABLED_CHECKLIST = "DISABLED_CHECKLIST";
        public const string DesignDrawingDefaultSpan = "DesignDrawingDefaultSpan";
        public const string DesignDrawingExDefaultSpan = "DesignDrawingExDefaultSpan";
        public const string cutofkeydeliverable = "cutofkeydeliverable";

        //Tender Progress
        public const string TENDERPROGRESS = "tenderProgress";


        //Traffic Light
        public const string TOTALDAYS_YELLOW = "TOTALDAYS_YELLOW";
        public const string TOTALDAYS_RED = "TOTALDAYS_RED";
        public const string TRAFFICLIGHT_GREEN = "Green";
        public const string TRAFFICLIGHT_YELLOW = "Yellow";
        public const string TRAFFICLIGHT_RED = "Red";
        public const string TRAFFICLIGHT_BLACK = "Black";
        public const string TRAFFICLIGHT_GREY = "Grey";

        //ASSIGNMENTTYPE (PROJECT DOCUMENT)
        public const string ASSIGNMENTTYPE = "assignmentType";
        public const string PROJECT_PROJECTMANAGER = "PPM";
        public const string PROJECT_PROJECTENGINEER = "PPE";
        public const string PROJECT_ENGINEER = "PE";
        public const string PROJECT_DESIGNER = "PD";
        public const string PORJECT_DESIGN = "DC";
        public const string PROJECT_PROJECT_CONTROLLER = "PC";
        public const string PROJECT_OFFICER = "PO";
        public const string PROJECT_MATERIAL_COORDINATOR = "MC";
        public const string TECHNICAL_SUPPORT_REQUESTER = "TSR";
        public const string TECHNICAL_SUPPORT_ENGINEER = "TSE";
        public const string TECHNICAL_SUPPORT_DESIGNER = "TSD";

        //ASSIGNMENTTYPE (PROJECT DOCUMENT)
        public const string DOCUMENTTYPE = "DocumentType";
        public const string DOCUMENTTYPE_ENGINEER = "EWP";
        public const string DOCUMENTTYPE_DESIGN = "DWG";
        public const string DOCUMENTTYPE_DESIGN_CONFORMITY = "DC";

        //ASSIGNMENTTYPE (EXECUTOR EWP)
        public const string EXECUTOREWPTYPE = "executorEWP";

        //PROJECTMANAGEMENT KEY
        public const string PROJECTMANAGEMENT_MANAGEKEYISSUE = "ManageKeyIssue";
        public const string PROJECTMANAGEMENT_SCOPE = "Scope";
        public const string PROJECTMANAGEMENT_BENEFIT = "Benefit";
        public const string PROJECTMANAGEMENT_ACCOMPLISHMENT = "Accomplishment";
        public const string PROJECTMANAGEMENT_DETAILS = "Details";
        public const string PROJECTMANAGEMENT_CONTRACTOR = "Contractor";
        public const string PROJECTMANAGEMENT_SAFETYPERFORMANCE = "SafetyPerformance";
        public const string PROJECTMANAGEMENT_CHANGEREQUEST = "ChangeRequest";
        public const string PROJECTMANAGEMENT_EHSRECORDS = "EhsRecords";

        //USER POSITION in PROJECT
        public const string USERPOSITION_OWNER = "owner";
        public const string USERPOSITION_OWNER_NAME = "Owner";
        public const string USERPOSITION_SPONSOR = "sponsor";
        public const string USERPOSITION_SPONSOR_NAME = "Sponsor";
        public const string USERPOSITION_PROJECTMANAGER = "projectManager";
        public const string USERPOSITION_PROJECTMANAGER_NAME = "Project Manager";
        public const string USERPOSITION_PROJECTENGINEER = "projectEngineer";
        public const string USERPOSITION_PROJECTENGINEER_NAME = "Project Engineer";
        public const string USERPOSITION_PROJECT_CONTROLLER = "PC";
        public const string USERPOSITION_PROJECT_CONTROLLER_NAME = "Project Controller";
        public const string USERPOSITION_MATERIAL_COORDINATOR = "MC";
        public const string USERPOSITION_MATERIAL_COORDINATOR_NAME = "Material Coordinator";
        public const string USERPOSITION_MAINTENANCEREP = "maintenanceRep";
        public const string USERPOSITION_MAINTENANCEREP_NAME = "Maintenance Rep";
        public const string USERPOSITION_OPERATIONREP = "operationRep";
        public const string USERPOSITION_OPERATIONREP_NAME = "Operation Rep";
        public const string USERPOSITION_ENGINEERDESIGNER = "engineerDesigner";
        public const string USERPOSITION_ENGINEERDESIGNER_NAME = "Engineer Designer";
        public const string USERPOSITION_ENGINEER = "engineer";
        public const string USERPOSITION_ENGINEER_NAME = "Engineer";
        public const string USERPOSITION_DESIGNER = "designer";
        public const string USERPOSITION_DESIGNER_NAME = "Designer";
        public const string USERPOSITION_DESIGN_CONFORMITY = "designConformity";
        public const string USERPOSITION_DESIGN_NAME = "Design Conformity";

        //Status Iprom
        public const string STATUSIPROM_COMPLETED = "S1";
        public const string STATUSIPROM_INPROGRESS = "S2";
        public const string STATUSIPROM_CANCELLED = "S3";
        public const string STATUSIPROM_DELAY = "S4";
        public const string STATUSIPROM_HOLD = "S5";
        public const string STATUSIPROM_NOTSTARTED = "S6";

        //Status Iprom Tech Support
        public const string STATUSIPROMCS_COMPLETED = "S1";
        public const string STATUSIPROMCS_INPROGRESS = "S2";
        public const string STATUSIPROMCS_CANCELLED = "S3";
        public const string STATUSIPROMCS_DELAY = "S4";
        public const string STATUSIPROMCS_HOLD = "S5";
        public const string STATUSIPROMCS_NOTSTARTED = "S6";

        //status CS
        public const string STATUSCS_CLOSED = "CLOSE";
        public const string STATUSCS_COMPLETED = "COMPLETED";
        public const string STATUSCS_CANCELLED = "CANCELLED";

        //status Executor Document Engineer
        public const string EXECUTOR_AUTO = "Auto";
        public const string EXECUTOR_NA = "NA";
        public const string EXECUTOR_CAS = "CAS";
        public const string EXECUTOR_CS = "CS";


        //status ExecutorEWP
        public const string EXECUTOR_EWP = "executorEWP";
        public const string EXECUTOR_EWP_AUTO = "AUTO";
        public const string EXECUTOR_EWP_CS = "CS";
        public const string EXECUTOR_EWP_CONTRACTOR = "CT";
        public const string EXECUTOR_EWP_CS_CONTRACTOR = "CSCT";

        //ipt progress report
        public const string IPT_PROGRESS_TODATE_SUMMARY = "IPT_PROGRESS_TODATE_SUMMARY";
        public const string IPT_PROGRESS_UPDOWN = "IPT_PROGRESS_UPDOWN";
        public const string IPT_FORECAST_BASELINEVSACTUAL = "IPT_FORECAST_BASELINEVSACTUAL";
        public const string IPT_SCURVE_ACTUAL_AND_FORECAST = "IPT_SCURVE_ACTUAL_AND_FORECAST";
        public const string IPT_FORECAST_OPTIMISVSPROPORSIONAL = "IPT_FORECAST_OPTIMISVSPROPORSIONAL";

        //ROLE iProM
        public const string RoleSystemAdministrator = "ADM";
        public const string RoleProjectManager = "PPM";
        public const string RoleProjectEngineer = "PPE";
        public const string RoleProjectSponsor = "PPS";
        public const string RoleProjectOwner = "PPO";
        public const string RoleMaintenanceReport = "PMR";
        public const string RoleOperationReport = "POR";
        public const string RoleEngineer = "PEN";
        public const string RoleDesigner = "PDG";
        public const string RoleDesignConfomity = "PDC";
        public const string RoleMaterialCoordinator = "PMC";
        public const string RoleProjectController = "PPC";
        public const string RoleGM_ES = "GMES";
        public const string RoleManager_E_I_ES = "MEES";
        public const string RoleManager_C_M_ES = "MCES";
        public const string RoleLeadDesigner_C_M_ES = "LDES";
        public const string RoleLeadDesigner_E_I_ES = "LDEI";
        public const string RoleLeadDesigner_DC = "DC";
        public const string RoleGM_MaintenanceUtilities = "GMMU";
        public const string RoleManager_C_M_MU = "MCMU";
        public const string RoleManager_Automation_MU = "MAMU";
        public const string RoleManager_Electrical_MU = "MEMU";
        public const string RoleBuyer = "ABY";
        public const string RoleManagerProjectServices = "AMP";
        public const string RoleConstructionServices = "ACS";
        public const string RoleContractAdmin = "ACA";
        public const string RoleOtherDeptUser = "OTH";

        //otherPositionProject
        public const string otherPositionProject = "otherPositionProject";

        // IPT
        public const string IPT_SHIFTTARGET_MAXTASKLEVEL = "IPT_SHIFTTARGET_MAXTASKLEVEL";

        // scurve generation
        public const string IPT_SCURVEGENERATION_STATUS_NEW = "NEW";
        public const string IPT_SCURVEGENERATION_STATUS_INPROGRESS = "INPROGRESS";
        public const string IPT_SCURVEGENERATION_STATUS_COMPLETED = "COMPLETED";
        public const string IPT_SCURVEGENERATION_STATUS_EMAILFAILED = "EMAIL_FAILED";
        public const string IPT_SCURVEGENERATION_STATUS_GENERATIONFAILED = "FAILED";
        public const string IPT_SCURVEGENERATION_STATUS_RESTART = "RESTART";
        public const string IPT_SCURVEGENERATION_STATUS_RETRY = "RETRY";

        // custom field parameter
        public const string IPT_CUSTOMFIELD_UID_EXECUTOR = "IPT_CUSTOMFIELD_UID_EXECUTOR";
        public const string IPT_CUSTOMFIELD_UID_SUPERVISOR = "IPT_CUSTOMFIELD_UID_SUPERVISOR";
        public const string IPT_CUSTOMFIELD_UID_INPLANNER = "IPT_CUSTOMFIELD_UID_INPLANNER";
        public const string IPT_CUSTOMFIELD_UID_CM = "IPT_CUSTOMFIELD_UID_CM";
        public const string IPT_CUSTOMFIELD_UID_PLAN_CALC_STARTDATE = "IPT_CUSTOMFIELD_UID_PLAN_CALC_STARTDATE";
        public const string IPT_CUSTOMFIELD_UID_PLAN_CALC_ENDDATE = "IPT_CUSTOMFIELD_UID_PLAN_CALC_ENDDATE";
        public const string IPT_CUSTOMFIELD_UID_PLAN_CALC_DURATION = "IPT_CUSTOMFIELD_UID_PLAN_CALC_DURATION";
        public const string IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_STARTDATE = "IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_STARTDATE";
        public const string IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_ENDDATE = "IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_ENDDATE";
        public const string IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_DURATION = "IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_DURATION";
        public const string IPT_CUSTOMFIELD_UID_REALIS_CALC_STARTDATE = "IPT_CUSTOMFIELD_UID_REALIS_CALC_STARTDATE";
        public const string IPT_CUSTOMFIELD_UID_REALIS_CALC_ENDDATE = "IPT_CUSTOMFIELD_UID_REALIS_CALC_ENDDATE";
        public const string IPT_CUSTOMFIELD_UID_REALIS_CALC_DURATION = "IPT_CUSTOMFIELD_UID_REALIS_CALC_DURATION";
        public const string IPT_CUSTOMFIELD_UID_TOTALDURATION = "IPT_CUSTOMFIELD_UID_TOTALDURATION";

        public const string EMAILREDACTION_SCURVEGENERATION_COMPLETED = "SCURVEGENERATION_COMPLETED";
        // END IPT

        //Traffic light change email code
        public const string EWPDocChangeYellow = "EWPDocChangeYellow";
        public const string EWPDocChangeRed = "EWPDocChangeRed";
        public const string CloseOutChangeYellow = "CloseOutChangeYellow";
        public const string CloseOutChangeRed = "CloseOutChangeRed";

        //email
        public const string ProjectInformation = "ProjectInformation";
        public const string engineerDesigner = "engineerDesigner";
        public const string projectService = "projectService";
        public const string technicalSupport = "technicalSupport";
        public const string engineerDesignerTS = "engineerDesignerTS";
        public const string otherDepartmentTask = "otherDepartmentTask";
        public const string posOwner = "OWNER";
        public const string posSponsor = "SPONSOR";
        public const string posProjectManager = "PROJECT MANAGER";
        public const string posProjectEngineer = "PROJECT ENGINEER";
        public const string posMaintenanceRep = "MAINTENANCE REP.";
        public const string posOperationRep = "OPERATION REP.";

        public const string male = "MALE";
        public const string female = "FEMALE";
        public const string prefixmale = "Mr.";
        public const string prefixfemaleSingle = "Ms.";
        public const string prefixfemaleMarried = "Mrs.";

        public const string requestListPriority = "requestListPriority";
        public const string married = "Married";

        public const string fileIntegrationToolTipDef = "fileIntegrationToolTipDef";

        public const string DEBUG_USER = "DEBUG_USER";
        public const string SCURVE_GENERATION_METHOD = "SCURVE_GENERATION_METHOD";
        public const string SCURVE_GENERATION_METHOD_ALWAYS_CALCULATE = "ALWAYS_CALCULATE";
        public const string SCURVE_GENERATION_METHOD_NORMAL = "NORMAL";

        public const string overheadTimesheet = "overheadTimesheet";
        public const string statusIprom = "statusIprom";
        public const string closeOutReason = "closeOutReason";

        public const string positionEngineer = "positionEngineer";
        public const string positionDesigner = "positiondesigner";
        public const string docTypeWithReport = "DTBasicDesign";

        public const string projectManager = "PM";
        public const int ManageDocEWP = 26;
        public const int ManageDesignDoc = 27;
        public const int PMModul = 20;
        public const string loaderCost = "loaderCost";
        // otherPositionProject
        public const string MES = "MES"; //Manager ES 
        public const string MCPM = "MCPM"; //Manager CPM 
        public const string MMSE = "MMSE"; //Manager MSE 
        public const string SEIES = "SEIES"; //Supervisor EI-ES 
        public const string SMCES = "SMCES"; //Supervisor MC-ES 
        public const string SPS = "SPS"; //Supervisor PS 
        public const string SMSER = "SMSER"; //Supervisor MSE Elect 
        public const string SMSEI = "SMSEI"; //Supervisor MSE Utilities Instrument
        public const string SMSEP = "SMSEP"; //Supervisor MSE Utilities PP 
        public const string POR = "POR"; //Operation Reps 
        public const string PMR = "PMR"; //Maintenance Reps 
        public const string REV = "REV"; //Reviewers 
        public const string OTHP = "OTHP"; //Others 
        public const string MOPS = "MOPS"; //Manager Operation 
        public const string SCH = "SCH"; //Scheduler 
        public const string PLN = "PLN"; //Planner 
        public const string MPS = "MPS"; //Manager Project Sponsor 
        public const string PPE = "PPE"; //Project Engineer 
        public const string AUDIT = "AUDIT"; //Audit By Project 

        //Lookup key
        public const string ewpDocType = "ewpDocType";
        public const string reasonCloseOutTypeDel = "DEL";

        //emailredactionkey
        public const string EmailPSDPFDChange = "EmailPSDPFDChange";

        // ewr
        public const string EWR_SUBMIT = "SUBMITTED";      
        public const string EWR_L2_APPROVED = "L2_APPROVED";
        public const string EWR_ENGINEER_SEC_APPROVED = "ENGINEER_SEC_APPROVED";
        public const string EWR_ENGINEER_L2_APPROVED = "ENGINEER_L2_APPROVED";
        public const string EWR_DRAFT = "DRAFT";

        // ewr user assignment type
        public const string EWR_PARTICIPANT = "EWRPARTICIPANT";
        public const string EWR_DISTRIBUTION = "EWRDISTRIBUTION";

        public const string DOC_EWR_TYPE = "EWR";
        public const string DOC_EWR_MOM = "EWRMOMATTCH";
        // K2
        public const string STATUS_APPROVE_K2 = "Approved";
        public const string STATUS_REJECT_K2 = "Rejected";
        public const string STATUS_REVISE_K2 = "Revised";

        public const string WORKLISTITEM_API_URL = "WORKLISTITEM_API_URL";

        public const string WORKINGTIME_5DAYS = "WKTM5";
        public const string WORKINGTIME_7DAYS = "WKTM7";

        public const string IPT_PROGRESSCONDITION_AHEAD = "AH";
        public const string IPT_PROGRESSCONDITION_DELAY = "DL";

        //type WeightHours Lookup
        public const string WeightHoursTypeEWP = "WeightHoursTypeEWP";
        public const string WeightHoursTypeDWGEWP = "WeightHoursTypeDWGEWP";
        public const string WeightHoursTypeDC = "WeightHoursTypeDC";
        public const string WeightHoursTypeDWGDC = "WeightHoursTypeDWGDC";

        //General Parameter Factored Drawing
        public const string FactoredDrawingMC = "FactoredDrawingMC";
        public const string FactoredDrawingEI = "FactoredDrawingEI";

        //LOOKUP Position Group Designer
        public const string positionGroupDesignerMC = "MC";
        public const string positionGroupDesignerEI = "EI";

        //Lookup finalResultAction
        public const string finalResultAction = "finalResultAction";
        public const string finalResultActionYes = "Y";
        public const string finalResultActionNotRequired = "NR";
        public const string finalResultActionNo = "N";

        //Maximum Target Forecast
        public const string MAX = "MAX";
        public const string FORECAST = "FORECAST";

        //SummaryTaskBaseline
        public const string SummaryTaskBaseline = "SummaryTaskBaseline";

        //DocServer
        public const string DocServer = "DocServer";
    }
}