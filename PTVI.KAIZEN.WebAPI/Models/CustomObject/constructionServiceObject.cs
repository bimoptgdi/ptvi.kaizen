﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class constructionServiceObject
    {
        public int id { get; set; }
        public int? projectId { get; set; }
        public int? projectDocumentId { get; set; }
        public string projectNo { get; set; }
        public string ewpNo { get; set; }
        public string ewpTitle { get; set; }
        public string projectManager { get; set; }
        public string engineer { get; set; }
        public string pmoNo { get; set; }
        public DateTime? rbd { get; set; }
        public string status { get; set; }
        public DateTime? planStartDate { get; set; }
        public int? estimationDays { get; set; }
        public DateTime? planFinishDate { get; set; }
        public DateTime? actualStartDate { get; set; }
        public int? actualDays { get; set; }
        public DateTime? actualFinishDate { get; set; }
        public double? estimationCost { get; set; }
        public double? actualCost { get; set; }
        public string trafficStatus { get; set; }
        public int? tenderId { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual executorConstructionServiceObject executor { get; set; }
    }
}
