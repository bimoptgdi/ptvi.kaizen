﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class tenderInstallationObject
    {
        public int id { get; set; }
        public int? tenderId { get; set; }
        public string task { get; set; }
        public double? weightPercentage { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planFinishDate { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualFinishDate { get; set; }
        public double? completePercentage { get; set; }
        public string issue { get; set; }
        public string issueStatus { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual tenderObject tender { get; set; }
    }
}
