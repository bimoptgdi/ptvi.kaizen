﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class otherDepartmentTaskObject
    {
        public int id { get; set; }
        public int? projectId { get; set; }
        public string projectNo { get; set; }
        public string projectDesc { get; set; }
        public string departmentId { get; set; }
        public string departmentName { get; set; }
        public string taskName { get; set; }
        public string picId { get; set; }
        public string picName { get; set; }
        public string picEmail { get; set; }
        public DateTime? planDate { get; set; }
        public DateTime? actualDate { get; set; }
        public string remark { get; set; }
        public string status { get; set; }
        public string trafic { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual projectObject project { get; set; }
    }
}
