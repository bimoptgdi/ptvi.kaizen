using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class projectDocumentComplexityObject
    {
        public int oid { get; set; }
        public int? projectDocumentId { get; set; }
        public int? masterComplexityDCId { get; set; }
        public string category { get; set; }
        public double? complexityValue { get; set; }
        public double? point { get; set; }
        public double? weight { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual masterDesignerComplexityObject masterDesignerComplexity { get; set; }
        //public virtual projectDocumentObject projectDocument { get; set; }
    }
}
