﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class materialByEngineerObject
    {
        public int id { get; set; }
        public string mrNo { get; set; }
        public string materialDesc { get; set; }
        public DateTime? prPlanDate { get; set; }
        public DateTime? prActualDate { get; set; }
        public DateTime? poPlanDate { get; set; }
        public DateTime? poActualDate { get; set; }
        public DateTime? onSitePlanDate { get; set; }
        public DateTime? onSiteActualDate { get; set; }
        public DateTime? rfqPlanDate { get; set; }
        public DateTime? rfqActualDate { get; set; }
        public int? revision { get; set; }
        public string engineer { get; set; }
        public string engineerId { get; set; }
        public string remark { get; set; }
        public DateTime? createddAte { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
    }
}
