﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class technicalSupportEngineeringFactorObject
    {
        public int id { get; set; }
        public int year { get; set; }
        public int? technicalSupportId { get; set; }
        public int? masterYearlyEngineeringHourId { get; set; }
        public string employeeId { get; set; }
        public string employeeName { get; set; }
        public string employeeEmail { get; set; }
        public string employeePosition { get; set; }
        public int masterYearlyFactorId { get; set; }
        public int engineeringDesignerId { get; set; }
        public int holidayPublic { get; set; }
        public int holidaysatsun { get; set; }
        public int holidayannual { get; set; }
        public int leaverr { get; set; }
        public int leaveext { get; set; }
        public int leaveOtherJan { get; set; }
        public int leaveOtherFeb { get; set; }
        public int leaveOtherMar { get; set; }
        public int leaveOtherApr { get; set; }
        public int leaveOtherMei { get; set; }
        public int leaveOtherJun { get; set; }
        public int leaveOtherJul { get; set; }
        public int leaveOtherAug { get; set; }
        public int leaveOtherSep { get; set; }
        public int leaveOtherOct { get; set; }
        public int leaveOtherNov { get; set; }
        public int leaveOtherDec { get; set; }

        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual masterYearlyEngineeringHoursObject masterYearlyEngineeringHour { get; set; }
        public virtual TECHNICALSUPPORTMASTER technicalSupportMaster { get; set; }
    }
}
