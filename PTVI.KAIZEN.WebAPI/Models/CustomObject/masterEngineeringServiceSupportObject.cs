﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class masterEngineeringServiceSupportObject
    {
        public masterEngineeringServiceSupportObject()
        {
            this.PROJECTs = new HashSet<PROJECT>();
            this.TECHNICALSUPPORTMASTERs = new HashSet<TECHNICALSUPPORTMASTER>();
        }


        public int id { get; set; }
        public int? year { get; set; }
        public int? month { get; set; }
        public string docType { get; set; }
        public Nullable<double> monthFactor { get; set; }
        public double minTarget { get; set; }
        public Nullable<double> target { get; set; }
        public Nullable<double> maxTarget { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string createdBy { get; set; }
        public Nullable<System.DateTime> updatedDate{ get; set; }
        public string updatedBy{ get; set; }
        public virtual ICollection<PROJECT> PROJECTs { get; set; }
        public virtual ICollection<TECHNICALSUPPORTMASTER> TECHNICALSUPPORTMASTERs { get; set; }
    }
}
