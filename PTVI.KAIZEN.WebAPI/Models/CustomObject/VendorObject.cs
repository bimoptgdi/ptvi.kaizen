﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class VendorObject
    {
        public string id { get; set; }
        public string vendor_Code { get; set; }
        public string vendor_Name { get; set; }
        public string vendor_Type { get; set; }
    }
}