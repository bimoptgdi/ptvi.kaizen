﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Controllers;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class contractorObject
    {
        public string BADGENO;
        public string EMPLOYEE_NAME;
        public DateTime? GIPVALIDUNTIL;
        public DateTime? MCU_DATE;
        public string STATUS;
        public string CONTRACTNUMBER;
        public string VENDORNAME;
        public string VENDORID;
        public contractObject contractData
        {
            get
            {
                ContractController contractControl = new ContractController();
                return contractControl.GetCONTRACT(CONTRACTNUMBER);
            }
        }
    }
}