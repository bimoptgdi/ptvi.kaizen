﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class employeeObject
    {
        public string employeeId { get; set; }
        public string full_Name { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public string dept_Code { get; set; }
        public string deptName { get; set; }
        public string positionId { get; set; }
    }
}