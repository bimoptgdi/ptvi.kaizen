using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class doaLoginObject
    {
        public string userNameOrigin { get; set; }
        public string userNameDelegated { get; set; }
        public DateTime? loginDate { get; set; }
    }
}
