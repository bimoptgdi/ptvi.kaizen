﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class TaskPercentComplete
    {
        public string taskId { get; set; }
        public Nullable<DateTime> actualStartDate { get; set; }
        public Nullable<DateTime> actualEndDate { get; set; }
        public int percentComplete { get; set; }
    }
}