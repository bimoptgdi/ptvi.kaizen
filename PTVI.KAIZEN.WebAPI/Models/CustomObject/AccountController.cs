﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.Data;
using System.Web;
using System.Security.Principal;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class AccountController : KAIZENController
    {

        string user = Properties.Settings.Default.LogonUser;
        string password = Properties.Settings.Default.LogonPassword;
        string domain = "inco";

        public DataTable GetAccount(string id)
        {
            return EmployeeData(id);
        }

        [HttpGet]
        [ActionName("getEmployeeByBN")]
        public employeeObject getEmployeeByBN(string param)
        {
            GENERALPARAMETER empDataUrl = db.GENERALPARAMETERs.FirstOrDefault(f => f.TITLE == constants.EMPLOYEE_API_URL);
            if (empDataUrl != null)
            {
                if (param == "IUSR" || param == "SYSTEM") param = "herus";

                //#if DEBUG
                //                if (param == "IUSR" || param == "SYSTEM") param = "herus";
                //#endif                
                string url = empDataUrl.VALUE.Replace("[bn]", param);
                //url += string.Format("?filter=(USERNAME contains %27{0}%27) or (EMPLOYEEID contains %27{0}%27)", param.Trim());
                //return GetListFromExternalData(url);
                var employee = GetJsonObjectFromWebApi(url);
                employeeObject dt = new employeeObject();
                if (employee != null)
                {
                    dt.full_Name = employee["FULL_NAME"];
                    dt.employeeId = employee["EMPLOYEEID"];
                    dt.email = employee["EMAIL"];
                    dt.dept_Code = employee["DEPT_CODE"];
                    dt.positionId = employee["POSITIONID"];
                }
                return dt;
            }
            return null;

        }

        [HttpGet]
        [ActionName("EmployeeData")]
        public DataTable EmployeeData(string param)
        {

            try
            {
                GENERALPARAMETER empDataUrl = db.GENERALPARAMETERs.FirstOrDefault(x => x.TITLE == constants.EMPLOYEE_API_URL);
                if (empDataUrl != null)
                {
                    string url = empDataUrl.VALUE.Replace("[bn]", param);
                    //url += string.Format("?filter=(USERNAME contains %27{0}%27) or (EMPLOYEEID contains %27{0}%27)", param.Trim());

                    //return GetListFromExternalData(url);
                    var employee = GetJsonObjectFromWebApi(url);
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[] {
                        new DataColumn("full_Name"), new DataColumn ("employeeId"), new DataColumn ("email")

                    });
                    var row = dt.NewRow();
                    row["full_Name"] = employee["FULL_NAME"];
                    row["employeeId"] = employee["EMPLOYEEID"];
                    row["email"] = employee["EMAIL"];
                    dt.Rows.Add(row);
                    dt.AcceptChanges();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                // failed to get general parameter
            }

            return null;

        }

        //[HttpGet]
        //[ActionName("PhotoByBN")]
        //public HttpResponseMessage PhotoByBN(string param)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();

        //    GENERALPARAMETER parameter = db.GENERALPARAMETERs.First(x => x.TITLE == constants.EMPLOYEE_PHOTO_LOCATION);
        //    string filePath = System.IO.Path.Combine(parameter.VALUE, string.Format("E{0}.jpg", param));

        //    IntPtr token;

        //    if (!NativeMethods.LogonUser(
        //        user,
        //        domain,
        //        password,
        //        NativeMethods.LogonType.NewCredentials,
        //        NativeMethods.LogonProvider.Default,
        //        out token))
        //    {
        //        //throw new Win32Exception();
        //    }

        //    try
        //    {
        //        IntPtr tokenDuplicate;

        //        if (!NativeMethods.DuplicateToken(
        //            token,
        //            NativeMethods.SecurityImpersonationLevel.Impersonation,
        //            out tokenDuplicate))
        //        {
        //            //throw new Win32Exception();
        //        }

        //        try
        //        {
        //            using (WindowsImpersonationContext impersonationContext =
        //                new WindowsIdentity(tokenDuplicate).Impersonate())
        //            {
        //                // Do stuff with your share here.
        //                using (System.IO.FileStream fs = System.IO.File.OpenRead(filePath))
        //                {
        //                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
        //                    ms.SetLength(fs.Length);
        //                    fs.Read(ms.GetBuffer(), 0, (int)fs.Length);

        //                    response.Content = new ByteArrayContent(ms.ToArray());
        //                    ms.Close();
        //                    ms.Dispose();
        //                }

        //                impersonationContext.Undo();
        //                //return;
        //            }
        //        }
        //        finally
        //        {
        //            if (tokenDuplicate != IntPtr.Zero)
        //            {
        //                if (!NativeMethods.CloseHandle(tokenDuplicate))
        //                {
        //                    // Uncomment if you need to know this case.
        //                    ////throw new Win32Exception();
        //                }
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        if (token != IntPtr.Zero)
        //        {
        //            if (!NativeMethods.CloseHandle(token))
        //            {
        //                // Uncomment if you need to know this case.
        //                ////throw new Win32Exception();
        //            }
        //        }
        //    }
        //    //using (System.Security.Principal.WindowsImpersonationContext ctx = System.Security.Principal.WindowsIdentity.Impersonate(userTokenptr))
        //    //{

        //    //}

        //    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpg");
        //    response.StatusCode = HttpStatusCode.OK;

        //    return response;
        //}

        //[HttpGet]
        //[ActionName("DownloadPhotoByBN")]
        //public string DownloadPhotoByBN(string param)
        //{
        //    string downloadedFilePath = "";

        //    GENERALPARAMETER parameter = db.GENERALPARAMETERs.First(x => x.TITLE == constants.EMPLOYEE_PHOTO_LOCATION);
        //    string filePath = System.IO.Path.Combine(parameter.VALUE, string.Format("E{0}.jpg", param));

        //    IntPtr token;

        //    if (!NativeMethods.LogonUser(
        //        user,
        //        domain,
        //        password,
        //        NativeMethods.LogonType.NewCredentials,
        //        NativeMethods.LogonProvider.Default,
        //        out token))
        //    {
        //        //throw new Win32Exception();
        //    }

        //    try
        //    {
        //        IntPtr tokenDuplicate;

        //        if (!NativeMethods.DuplicateToken(
        //            token,
        //            NativeMethods.SecurityImpersonationLevel.Impersonation,
        //            out tokenDuplicate))
        //        {
        //            //throw new Win32Exception();
        //        }

        //        try
        //        {
        //            using (WindowsImpersonationContext impersonationContext =
        //                new WindowsIdentity(tokenDuplicate).Impersonate())
        //            {
        //                // Do stuff with your share here.
        //                using (System.IO.FileStream fs = System.IO.File.OpenRead(filePath))
        //                {
        //                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
        //                    ms.SetLength(fs.Length);
        //                    fs.Read(ms.GetBuffer(), 0, (int)fs.Length);

        //                    string root = HttpContext.Current.Server.MapPath("~/App_Data");
        //                    string fileName = Guid.NewGuid().ToString() + "." + System.IO.Path.GetExtension(filePath);
        //                    downloadedFilePath = System.IO.Path.Combine(root, fileName);
        //                    System.IO.FileStream fsSave = new System.IO.FileStream(downloadedFilePath, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
        //                    byte[] imageData = ms.ToArray();
        //                    fsSave.Write(imageData, 0, imageData.Length);
        //                    fsSave.Flush();

        //                    //response.Content = new ByteArrayContent(ms.ToArray());
        //                    ms.Close();
        //                    ms.Dispose();
        //                    fsSave.Close();
        //                    fsSave.Dispose();
        //                }

        //                impersonationContext.Undo();
        //                //return;
        //            }
        //        }
        //        finally
        //        {
        //            if (tokenDuplicate != IntPtr.Zero)
        //            {
        //                if (!NativeMethods.CloseHandle(tokenDuplicate))
        //                {
        //                    // Uncomment if you need to know this case.
        //                    ////throw new Win32Exception();
        //                }
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        if (token != IntPtr.Zero)
        //        {
        //            if (!NativeMethods.CloseHandle(token))
        //            {
        //                // Uncomment if you need to know this case.
        //                ////throw new Win32Exception();
        //            }
        //        }
        //    }

        //    return downloadedFilePath;
        //}

        [HttpGet]
        [ActionName("FindAllEmployee")]
        public IHttpActionResult FindAllEmployee(string param)
        {
            try
            {
                //List<Dictionary<string, dynamic>> contractorData = new List<Dictionary<string, dynamic>>();
                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {
                    if (!String.IsNullOrEmpty(param))
                    {
                        IEnumerable<EMPLOYEE> result = GetListFromExternalData<EMPLOYEE>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_ADLIST_EX");
                        return Ok(result.Where(d => d.EMPLOYEEID != "-").AsQueryable<EMPLOYEE>());
                    }
                    else
                    {
                        return NotFound();
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

        }
    }
}
