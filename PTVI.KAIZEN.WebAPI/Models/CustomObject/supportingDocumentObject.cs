﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class supportingDocumentObject
    {
        public int id { get; set; }
        public int? typeID { get; set; }
        public string typeName { get; set; }
        public string fileName { get; set; }
        public string refDocID { get; set; }
        public int? sequenceNo { get; set; }
        public string uploadedBy { get; set; }
        public DateTime? uploadedDate { get; set; }
        public string updatedBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string typeDetail { get; set; }
    }
}
