﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class userRoleObject
    {
        public int id { get; set; }
        public int userID { get; set; }
        public int roleID { get; set; }
    
        public virtual roleObject role { get; set; }
        public virtual userObject user { get; set; }
    }
}
