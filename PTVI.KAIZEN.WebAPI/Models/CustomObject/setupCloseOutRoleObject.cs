﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class setupCloseOutRoleObject
    {
        public setupCloseOutRoleObject()
        {
            //this.closeOutProgresses = new HashSet<closeOutProgressObject>();
            this.setupCloseOutCheckLists = new HashSet<setupCloseOutChecklistObject>();
            this.setupCloseOutDocumentObjects = new HashSet<setupCloseOutDocumentObject>();
        }

        public int id { get; set; }
        public string description { get; set; }
        public string processName { get; set; }
        public int? seq { get; set; }
        public bool? isRole { get; set; }
        public string roleName { get; set; }
        public string assignmentType { get; set; }
        public bool? isActive { get; set; }
        public System.DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public System.DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        //public virtual ICollection<closeOutProgressObject> closeOutProgresses { get; set; }
        public virtual ICollection<setupCloseOutChecklistObject> setupCloseOutCheckLists { get; set; }
        public virtual ICollection<setupCloseOutDocumentObject> setupCloseOutDocumentObjects { get; set; }
    }
}
