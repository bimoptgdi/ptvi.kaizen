﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class tenderMaterialObject
    {
        public int id { get; set; }
        public int? tenderId { get; set; }
        public int? materialComponentId { get; set; }
        public string mrNo { get; set; }
        public string poNo { get; set; }
        public string poItemNo { get; set; }
        public string reservation { get; set; }
        public int? reservationItemNo { get; set; }
        public string materialDesc { get; set; }
        public string status { get; set; }
        public string handover { get; set; }
        public string mirNo { get; set; }
        public int? usedQuantity { get; set; }
        public int? remainingQuantity { get; set; }
        public string remainingLocation { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual tenderObject tender { get; set; }
        public virtual materialComponentObject materialComponent { get; set; }

    }
}
