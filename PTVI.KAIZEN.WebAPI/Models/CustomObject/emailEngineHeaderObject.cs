﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class emailEngineHeaderObject
    {
        public int KEY { get; set; }
        public string BADGENO { get; set; }
        public string BADGENOTO { get; set; }
        public string EMAIL { get; set; }
        public string RECEIVER { get; set; }
        public List<string> FIELDLIST { get; set; }

    }
}