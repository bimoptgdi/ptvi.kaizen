﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterYearlyTargetScoreObject
    {
        public int id { get; set; }
        public Nullable<int> year { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public double? value { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
    }
}