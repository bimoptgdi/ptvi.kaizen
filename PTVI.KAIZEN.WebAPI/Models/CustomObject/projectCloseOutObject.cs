﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Controllers;
namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class projectCloseOutObject

    {
        public int id { get; set; }
        public Nullable<int> projectId { get; set; }
        public string projectNo { get; set; }
        public string projectTitle { get; set; }
        public string reasonClosureType { get; set; }
        public string comments { get; set; }
        public int curCopId { get; set; } // current close out progresses id (OID CLOSEOUTPROGRESS)
        public int curRoleId { get; set; } // current role from default / choosen user login role (OID SETUPCLOSEOUTROLE)
        public Nullable<bool> isApproved { get; set; }
        public Nullable<System.DateTime> approvedDate { get; set; }
        public string approvedBy { get; set; }
        public Nullable<System.DateTime> rejectedDate { get; set; }
        public string rejectedBy { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string createdBy { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string remarkRejectCLoseOut { get; set; }

        public string userLoginBadgeNo { get; set; }
        public string userLoginEmail { get; set; }
        public string userLoginName { get; set; }
        public bool loginerIsInCurrProj { get; set; }
        public bool loginerIsInCurrTask { get; set; }
        public IEnumerable<CloseOutController.objRole> rolesLoginer { get; set; }
        public IEnumerable<CloseOutController.objRole> rolesNotSubmitted { get; set; }
        public IEnumerable<CloseOutController.objRole> rolesLoginerValid { get; set; }
        public CloseOutController.objRole roleLoginerDefault { get; set; }
        public virtual IEnumerable<closeOutProgressObject> closeOutProgresses { get; set; }
        public virtual projectObject project { get; set; }
        //public IEnumerable<CloseOutController.coDoc> notDeletedCoDocs { get; set; }
    }
}