﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class userAssignmentObject
    {
        public userAssignmentObject()
        {
            this.technicalSupportInteraction = new HashSet<technicalSupportInteractionObject>();
            
        }

        public int id { get; set; }
        public int? projectId { get; set; }
        public int? technicalSupportId { get; set; }
        public string assignmentType { get; set; }
        public string employeeId { get; set; }
        public string employeeName { get; set; }
        public string employeeEmail { get; set; }
        public string employeePosition { get; set; }
        public string engineerDesigner { get; set; }
        public string toTechnicalSupportInteraction { get; set; }
        public string result { get; set; }
        public DateTime? joinedDate { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string actionDescription { get; set; }
        
        public double? completePercentage { get; set; }
        public DateTime? completedDate { get; set; }
        


        public virtual projectObject project { get; set; }
        public virtual technicalSupportObject technicalsupport { get; set; }
        public virtual ICollection<technicalSupportInteractionObject> technicalSupportInteraction { get; set;}
    }
}
