using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class generalParameterObject
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string value { get; set; }
        public string dataType { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }
    }
}
