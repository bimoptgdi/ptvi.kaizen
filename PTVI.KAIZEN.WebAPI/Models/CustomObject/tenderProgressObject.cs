using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class tenderProgressObject
    {
        public int id { get; set; }
        public int? tenderId { get; set; }
        public string processType { get; set; }
        public string processText { get; set; }
        public string processBy { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planFinishDate { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualFinishDate { get; set; }
        public string status { get; set; }
        public string trafficStatus { get; set; }
        public string contractAward { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual tenderObject tender { get; set; }
    }
}
