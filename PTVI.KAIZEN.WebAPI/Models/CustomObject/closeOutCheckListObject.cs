﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class closeOutCheckListObject
    {
        public int id { get; set; }
        public Nullable<int> closeOutProgressId { get; set; }
        public Nullable<int> closeOutCheckListId { get; set; }
        public Nullable<int> closeOutCheckListIdSeq { get; set; }
        public string closeOutCheckListText { get; set; }
        public Nullable<bool> isChecked { get; set; }
        
        
        
        public bool isDisabled
        {
            get {
                /*
                iPROMEntities db = new iPROMEntities();
                GENERALPARAMETER gpChkl = db.GENERALPARAMETERs.Where(x=>x.TITLE == constants.DISABLED_CHECKLIST && x.ISACTIVE == true).FirstOrDefault();
                if (gpChkl != null)
                {
                    string[] arrChkl = gpChkl.VALUE.Split('|');
                    for (var i = 0; i < arrChkl.Length; i++)
                    {
                        if (closeOutCheckListText == arrChkl[i])
                        {
                            return true;
                        }
                        //return closeOutCheckListText == "Complete COM" || closeOutCheckListText == "Complete Capitalized";
                    }
                }                
                return false;*/
                return false;
            }
        }
        public virtual setupCloseOutChecklistObject setupCloseOutCheckList { get; set; }
        //public virtual closeOutProgressObject closeOutProgress { get; set; }
    }
}
