﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class emailRedactionObject
    {
        public int id { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        
    }
}
