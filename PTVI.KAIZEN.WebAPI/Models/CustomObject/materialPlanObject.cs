﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class materialPlanObject
    {
        public int id { get; set; }
        public int? materialComponentId { get; set; }
        public DateTime? prPlanDate { get; set; }
        public string prPlanBy { get; set; }
        public DateTime? rfqDate { get; set; }
        public string rfqBy { get; set; }
        public DateTime? poPlanDate { get; set; }
        public string poPlanBy { get; set; }
        public string trafficofficerid { get; set; }
        public string trafficofficername { get; set; }
        public string trafficofficeremail { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual materialComponentObject materialComponent { get; set; }
    }
}
