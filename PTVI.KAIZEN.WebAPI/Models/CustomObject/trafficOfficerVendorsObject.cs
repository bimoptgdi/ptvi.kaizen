using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class trafficOfficerVendorsObject
    {
        public int id { get; set; }
        public string trafficOfficer { get; set; }
        public string vendor { get; set; }
        public bool? isActive { get; set; }
        public System.DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public System.DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

    }
}
