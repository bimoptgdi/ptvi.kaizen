using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class projectDocumentFinalResultObject
    {
        public int oid { get; set; }
        public int? projectDocumentId { get; set; }
        public int? masterFinalResultId { get; set; }
        public string masterFinalResultDesc { get; set; }
        public double? defaultValuePercentage { get; set; }
        public string finalResultValue { get; set; }
        public string finalResultText { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual masterDCFinalResultObject masterFinalResult { get; set; }
        //public virtual projectDocumentObject projectDocument { get; set; }
        public virtual IEnumerable<lookupsObject> lookupFinalResultValues { get; set; }
}
}
