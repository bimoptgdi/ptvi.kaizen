﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class projectShiftObject
    {
        public int id { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string shift { get; set; }
        public bool shiftTargetHasGenerated { get; set; }
        public bool actualPercentCompleteHasUpdated { get; set; }

        public string text
        {
            get
            {
                return string.Format("{0}: {1} - {2}", shift.PadLeft(2, '0'), startDate.ToString("dd MMM yyyy  HH:mm:ss"), endDate.ToString("dd MMM yyyy  HH:mm:ss"));
            }
        }
    }
}