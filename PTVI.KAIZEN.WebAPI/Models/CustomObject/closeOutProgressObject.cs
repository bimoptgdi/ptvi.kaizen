﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class closeOutProgressObject
    {
        public closeOutProgressObject()
        {
            this.setupCloseOutDocuments = new HashSet<setupCloseOutDocumentObject>();
            this.closeOutCheckLists = new HashSet<closeOutCheckListObject>();
            this.closeOutDocuments = new HashSet<closeOutDocumentObject>();
        }

        public int id { get; set; }
        public Nullable<int> projectCloseOutId { get; set; }
        public Nullable<int> closeOutRoleId { get; set; }
        public Nullable<int> closeOutRoleSeq { get; set; }
        public Nullable<int> closeOutSeqApproval { get; set; }
        public string closeOutRoleText { get; set; }
        public string closeOutRolePICName { get; set; }
        public string closeOutRolePICEmail { get; set; }
        public string closeOutRolePICID { get; set; }
        public Nullable<System.DateTime> planDate { get; set; }
        public Nullable<System.DateTime> submitedDate { get; set; }
        public string submitedBy { get; set; }

        public virtual IEnumerable<setupCloseOutDocumentObject> setupCloseOutDocuments { get; set; }
        public virtual IEnumerable<closeOutCheckListObject> closeOutCheckLists { get; set; }
        public virtual IEnumerable<closeOutDocumentObject> closeOutDocuments { get; set; }
        public virtual setupCloseOutRoleObject setupCloseOutRole { get; set; }
        public virtual projectCloseOutObject projectCloseOut { get; set; }
    }
}
