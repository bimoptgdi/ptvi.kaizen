﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class keyDeliverablesProgressObject
    {
        public int id { get; set; }
        public int? keyDeliverablesId { get; set; }
        public int? year { get; set; }
        public int? month { get; set; }
        public double? planPercentage { get; set; }
        public double? actualPercentage { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual keyDeliverableObject keyDeliverable { get; set; }
    }
}
