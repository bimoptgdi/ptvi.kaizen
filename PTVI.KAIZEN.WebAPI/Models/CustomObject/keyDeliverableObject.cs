﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class keyDeliverableObject
    {
        public keyDeliverableObject()
        {
            this.keyDeliverablesProgresses = new HashSet<keyDeliverablesProgressObject>();
        }

        public int id { get; set; }
        public int? projectManagementId { get; set; }
        public int? projectId { get; set; }
        public string name { get; set; }
        public double? weightPercentage { get; set; }
        public DateTime? planStartDate { get; set; }
        public DateTime? planFinishDate { get; set; }
        public int? revision { get; set; }
        public DateTime? actualStartDate { get; set; }
        public DateTime? actualFinishDate { get; set; }
        public string status { get; set; }
        public string remark { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string seq { get; set; }
        public string nameText { get; set; }
        public virtual projectManagementObject projectManagement { get; set; }
        public virtual ICollection<keyDeliverablesProgressObject> keyDeliverablesProgresses { get; set; }
    }
}
