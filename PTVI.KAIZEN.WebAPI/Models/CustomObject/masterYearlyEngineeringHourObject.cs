﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class masterYearlyEngineeringHoursObject
    {
        public masterYearlyEngineeringHoursObject()
        {
            this.PROJECTs = new HashSet<PROJECT>();
            this.TECHNICALSUPPORTMASTERs = new HashSet<TECHNICALSUPPORTMASTER>();
        }


        public int id { get; set; }
        public int? year { get; set; }
        public string employeeId { get; set; }
        public string employeeName { get; set; }
        public string employeeEmail { get; set; }
        public string employeePosition { get; set; }
        public Nullable<System.DateTime> dateOfLastPromotion { get; set; }
        public int? masterYearlyFactorId { get; set; }
        public Nullable<double> engineerDesignerFactor { get; set; }
        public int? holidayPublic { get; set; }
        public int? holidaySatsun { get; set; }
        public int? leaveAnnual { get; set; }
        public int? leaverr { get; set; }
        public int? leaveExt { get; set; }
        public int? leaveOtherJan { get; set; }
        public int? leaveOtherFeb { get; set; }
        public int? leaveOtherMar { get; set; }
        public int? leaveOtherApr { get; set; }
        public int? leaveOtherMay { get; set; }
        public int? leaveOtherJun { get; set; }
        public int? leaveOtherJul { get; set; }
        public int? leaveOtherAug { get; set; }
        public int? leaveOtherSep { get; set; }
        public int? leaveOtherOct { get; set; }
        public int? leaveOtherNov { get; set; }
        public int? leaveOtherDec { get; set; }
        public Nullable<double> totNonWorkingDay { get; set; }
        public Nullable<double> availableHours { get; set; }
        public Nullable<double> baselineHours { get; set; }
        public Nullable<double> serviceYear { get; set; }

        public Nullable<System.DateTime> createdDate { get; set; }
        public string createdBy { get; set; }
        public Nullable<System.DateTime> updatedDate{ get; set; }

        public string updatedBy{ get; set; }

        public virtual ICollection<PROJECT> PROJECTs { get; set; }
        public virtual ICollection<TECHNICALSUPPORTMASTER> TECHNICALSUPPORTMASTERs { get; set; }
    }
}
