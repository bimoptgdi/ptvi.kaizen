﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class contractObject
    {
        public string VendorID { get; set; }
        public string ContractNo { get; set; }
        public string ContractStartDate { get; set; }
        public string ContractEndDate { get; set; }
        public string ContractDescription { get; set; }
        public string ContractType { get; set; }
        public string Sponsor_Name { get; set; }
        public string DeptCode { get; set; }
        public string ContractProjectManager { get; set; }
    }
}