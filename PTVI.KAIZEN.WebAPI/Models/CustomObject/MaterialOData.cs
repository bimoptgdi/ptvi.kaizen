﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class MaterialOData
    {
        //[JsonProperty("odata.type")]
        public string MATERIAL_CODE { get; set; }
        public string STORAGE_LOC { get; set; }
        public string MATERIAL_DESCRIPTION { get; set; }
        public string MATERIAL_LONG_TEXT_01 { get; set; }
        public string MANUFACTURER { get; set; }
        public string PART_NUMBER { get; set; }
        public string MRP_CONTROLLER { get; set; }
        public string STAT_CODE { get; set; }
    }
}