﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class projectDocumentWeightHoursObject
    {
        public int oid { get; set; }
        public int? projectDocumentId { get; set; }
        public int? masterWeightHoursId { get; set; }
        public string masterWeightHoursDesc { get; set; }
        public string estimateHours { get; set; }
        public double? actualHours { get; set; }
        public int? seq { get; set; }
        public int? parentId { get; set; }
        public bool? isEditable { get; set; }
        public bool? isParentGroup { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        //public virtual projectDocumentObject projectDocument { get; set; }
    }
}
