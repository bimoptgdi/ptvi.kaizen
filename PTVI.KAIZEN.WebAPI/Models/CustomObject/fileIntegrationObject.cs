using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class fileIntegrationObject
    {
         
        public int id { get; set; }
        public string purposeType { get; set; }
        public string purposeFile { get; set; }
        public string fileName { get; set; }
        public string shareFolder { get; set; }
        public string toolTipDef { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string createdBy { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public string updatedBy { get; set; }
    }
}
