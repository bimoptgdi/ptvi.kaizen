﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterDesignerComplexityObject
    {
        public int id { get; set; }
        public Nullable<int> year { get; set; }
        public string category { get; set; }
        public double? weight { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
    }
}