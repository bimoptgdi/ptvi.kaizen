﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class areaInvolvedObject
    {
        public areaInvolvedObject()
        {
            this.projects = new HashSet<projectObject>();
            this.technicalSupportAreas = new HashSet<technicalSupportAreaObject>();
        }

        public int id { get; set; }
        public string type { get; set; }
        public string areaName { get; set; }
        public string areaCode { get; set; }
        public string areaDesc { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual ICollection<projectObject> projects { get; set; }
        public virtual ICollection<technicalSupportAreaObject> technicalSupportAreas { get; set; }
    }
}
