﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class accessRightObject
    {
        public accessRightObject()
        {
            this.menuAccessRights = new HashSet<menuAccessRightObject>();
            this.roleAccessRights = new HashSet<roleAccessRightObject>();
        }
    
        public int id { get; set; }
        public string description { get; set; }
    
        public virtual ICollection<menuAccessRightObject> menuAccessRights { get; set; }
        public virtual ICollection<roleAccessRightObject> roleAccessRights { get; set; }
    }
}
