﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class userObject
    {
        public userObject()
        {
            this.userRoles = new HashSet<userRoleObject>();
        }
    
        public int id { get; set; }
        public string ntUserID { get; set; }
        public string badgeNo { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }
    
        public virtual ICollection<userRoleObject> userRoles { get; set; }
    }
}
