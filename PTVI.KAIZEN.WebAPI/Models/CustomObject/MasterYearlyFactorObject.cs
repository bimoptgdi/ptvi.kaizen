﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class masterYearlyFactorObject
    {
        public masterYearlyFactorObject()
        {
            this.PROJECTs = new HashSet<PROJECT>();
            this.TECHNICALSUPPORTMASTERs = new HashSet<TECHNICALSUPPORTMASTER>();
        }


        public int id { get; set; }
        public Nullable<int> year { get; set; }
        public Nullable<int> levelNo { get; set; }
        public string engineerLevelCode { get; set; }
        public Nullable<double> service1 { get; set; }
        public Nullable<double> service2 { get; set; }
        public Nullable<double> service3 { get; set; }
        public Nullable<double> service4 { get; set; }
        public Nullable<double> service5 { get; set; }
        public Nullable<System.DateTime> CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public Nullable<System.DateTime> UPDATEDDATE { get; set; }
        public string UPDATEDBY { get; set; }

        public virtual ICollection<PROJECT> PROJECTs { get; set; }
        public virtual ICollection<TECHNICALSUPPORTMASTER> TECHNICALSUPPORTMASTERs { get; set; }
    }
}
