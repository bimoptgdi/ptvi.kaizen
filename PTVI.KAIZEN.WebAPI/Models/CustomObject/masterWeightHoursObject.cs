﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterWeightHoursObject
    {
        public int id { get; set; }
        public string description { get; set; }
        public string estimateHours { get; set; }
        public string weightHoursType { get; set; }
        public int? seq { get; set; }
        public int? pId { get; set; }
        public bool? isEditable { get; set; }
        public bool? isParentGroup { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string weightHoursTypeText { get; set; }
    }
}