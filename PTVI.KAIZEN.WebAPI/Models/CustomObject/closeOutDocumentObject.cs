﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class closeOutDocumentObject
    {
        public int id { get; set; }
        public Nullable<int> closeoutProgressId { get; set; }
        public Nullable<int> closeoutDocumentId { get; set; }
        public Nullable<int> supportingDocumentId { get; set; }
        public string refDocId { get; set; }
        public string fileType { get; set; }
        public string fileName { get; set; }
        public string description { get; set; }
        public int? fileSeq { get; set; }


        public virtual setupCloseOutDocumentObject setupCloseOutDocDocument { get; set; }
        //public virtual closeOutProgressObject closeOutProgress { get; set; }
        public virtual supportingDocumentObject supportingDocument { get; set; }
    }
}
