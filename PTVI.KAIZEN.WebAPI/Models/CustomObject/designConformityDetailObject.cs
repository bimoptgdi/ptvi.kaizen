using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class designConformityDetailObject
    {
        public int id { get; set; }
        public int? projectDocumentId { get; set; }
        public int? projectDocumentIdRef { get; set; }

        //public virtual projectDocumentObject designConformity { get; set; }
        //public virtual projectDocumentObject projectDocumentRef { get; set; }
    }
}
