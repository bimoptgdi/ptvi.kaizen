﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class ForecastSpendingDTO
    {
        public int OID { get; set; }
        public Nullable<int> PROJECTID { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<System.DateTime> DUEDATE { get; set; }
        public Nullable<decimal> FORECASTVALUE { get; set; }
    }
}