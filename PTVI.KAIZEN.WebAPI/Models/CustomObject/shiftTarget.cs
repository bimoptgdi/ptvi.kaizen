﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class shiftTarget
    {        
        public Guid taskID { get; set; }

        public string projectName { get; set; }


        public String taskOutlineNum{ get; set; }
        public int taskOutlineLevel { get; set; }

        //public Guid startDateForCalcCFId { get; set; }
        //public Guid endDateForCalcCFid { get; set; }
        //public Guid durationCalcCFid { get; set; }
        public Nullable<int> percentCompletePlan { get; set; }
        public Nullable<int> percentCompletePrevious { get; set; }
        public Nullable<int> percentCompleteActual { get; set; }
        public Nullable<DateTime> actualStart { get; set; }
        public Nullable<DateTime> actualFinish { get; set; }
        public Nullable<DateTime> start { get; set; }
        public Nullable<DateTime> finish { get; set; }
        public string Executor { get; set; }
        public string Supervisor { get; set; }
        public string Planner { get; set; }
        public string ConstructionManager { get; set; }

        public Guid groupTaskId { get; set; }

        public List<string> titleList { get; set; }

        public string taskName
        {
            get
            {
                var ttl = "";
                for (var i = titleList.Count - 1; i >= 0; i--)
                    ttl += (ttl == "" ? "" : " - ") + titleList[i];

                return ttl;
            }
        }

    }
}