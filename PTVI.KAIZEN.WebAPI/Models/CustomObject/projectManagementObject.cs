﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class projectManagementObject
    {
        public projectManagementObject()
        {
            this.keyDeliverables = new HashSet<keyDeliverableObject>();
        }

        public int id { get; set; }
        public int? projectId { get; set; }
        public string manageKeyIssue { get; set; }
        public string safetyPerfomance { get; set; }
        public string scope { get; set; }
        public string changeRequest { get; set; }
        public string ehsRecords { get; set; }
        public string benefit { get; set; }
        public string accomplishment { get; set; }
        public string details { get; set; }
        public string contractor { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual IEnumerable<keyDeliverableObject> keyDeliverables { get; set; }
        public virtual projectObject project { get; set; }
    }
}
