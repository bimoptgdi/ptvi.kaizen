﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class masterDesignerDraftingObject
    {
        public int id { get; set; }
        public string designerType { get; set; }
        public int? typeOfDrawing { get; set; }
        public double? estimateHours { get; set; }
        public string text { get; set; }
        public bool? isActive { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }
        public string designerTypeText { get; set; }


    }
}