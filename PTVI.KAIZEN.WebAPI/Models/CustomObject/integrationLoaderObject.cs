using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class integrationLoaderObject
    {
        public int id { get; set; }
        public int? fileIntegrationId { get; set; }
        public string purposeFile { get; set; }
        public string fileName { get; set; }
        public Nullable<System.DateTime> loaderDate { get; set; }
        public string loaderBy { get; set; }
        public string remark { get; set; }
    }
}
