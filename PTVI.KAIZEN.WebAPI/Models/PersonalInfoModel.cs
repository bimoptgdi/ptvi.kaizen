﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.OSC.WebAPI.Models
{
    public class PersonalInfoModel
    {
        public int OID { get; set; }
        public string ContractNo { get; set; }
        public string RequestType { get; set; }
        public Nullable<System.DateTime> ExpiredDate { get; set; }
        public Nullable<bool> ExpatStatus { get; set; }
        public string BadgeNo { get; set; }
        public string PrevBadgeNoContractor { get; set; }
        public string PevContractor { get; set; }
        public string PrevBadgeNoVale { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public string FullNameKTP { get; set; }
        public string NickName { get; set; }
        public string KTPState { get; set; }
        public string KTPAddress { get; set; }
        public string KTPProvince { get; set; }
        public string KTPCity { get; set; }
        public string KTPDistrict { get; set; }
        public Nullable<int> KTPZipCode { get; set; }
        public string CurrState { get; set; }
        public string CurrAddress { get; set; }
        public string CurrProvince { get; set; }
        public string CurrCity { get; set; }
        public string CurrDistrict { get; set; }
        public Nullable<int> CurrZipCode { get; set; }
        public string Phone { get; set; }
        public string BirthPlace { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string LastEducation { get; set; }
        public string OtherInformation { get; set; }
        public string UploadPhoto { get; set; }
        public string EmploymentType { get; set; }
        public Nullable<System.DateTime> ContractDurationFrom { get; set; }
        public Nullable<System.DateTime> ContractDurationTo { get; set; }
        public Nullable<System.DateTime> PersonContractDurationFrom { get; set; }
        public Nullable<System.DateTime> PersonContractDurationTo { get; set; }
        public string ContractDuration { get; set; }
        public string SalaryClasification { get; set; }
        public Nullable<bool> AKADL { get; set; }
        public string Position { get; set; }
        public string PositionId { get; set; }
        public string ContractorType { get; set; }
        public string LocalDistrict { get; set; }
        public string NameOfSubcont { get; set; }
        public Nullable<bool> PersonelStatus { get; set; }
        public string RequestStatus { get; set; }
        public string BadgeStatus { get; set; }
        public Nullable<System.DateTime> BadgePrintDate { get; set; }
        public string Remark { get; set; }
        public string Experiences { get; set; }
        public string FacilityAccess { get; set; }
        public int Age { get; set; }
        public Nullable<int> WfId { get; set; }
        public string Provider { get; set; }
        public string ProjectTitle { get; set; }
        public string ProjectManager { get; set; }        
        public string ProjectManagerName { get; set; }
        public string ProjectManagerEmail { get; set; }
        public string ProjectManagerNTAcc { get; set; }
        public string ContractType { get; set; }
        public string Region { get; set; }
        public Nullable<bool> NameSuspend { get; set; }
        public Nullable<bool> NikSuspend { get; set; }
        public Nullable<bool> TlSuspend { get; set; }
        public Nullable<System.DateTime> DateMCU { get; set; }
        public Nullable<System.DateTime> ExpiredDateMCU { get; set; }
        public string MCUResult { get; set; }
        public string MCUFileName { get; set; }
        public string MCUUniqueName { get; set; }
        public string ActivityName { get; set; }
        public string APROVALREMARK { get; set; }
        public string VendorName { get; set; }
        public string CreatedBy { get; set; }       
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> GIPExpiredDate { get; set; }
        public Nullable<System.DateTime> GIPScheduleDate { get; set; }
        public Nullable<System.DateTime> ACTIVITYUPDATEDDATE { get; set; }
        public List<ExperiencesModel> ExperiencesModel { get; set; }
        public List<CourseModel> CourseModel { get; set; }
        public List<EducationModel> EducationModel { get; set; }
        public List<PersonalDocModel> PersonalDocModel { get; set; }
        public List<CompetencyModel> CompetencyModel { get; set; }
        public List<McuModel> McuModel { get; set; }
        public List<PERSONALOTHERINFOObj> OtherInfoModel { get; set; }
        public List<K2WorklistItem> K2WorklistItems { get; set; }
    }

    public class ExperiencesModel
    {
        public string VALUE { get; set; }
    }

    public class CourseModel
    {
        public string VALUE { get; set; }
    }
    public class EducationModel
    {
        public string VALUE { get; set; }
    }
}