//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PTVI.KAIZEN.WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MATERIALPLAN
    {
        public int OID { get; set; }
        public Nullable<int> MATERIALCOMPONENTID { get; set; }
        public Nullable<System.DateTime> PRPLANDATE { get; set; }
        public string PRPLANBY { get; set; }
        public Nullable<System.DateTime> RFQDATE { get; set; }
        public string RFQBY { get; set; }
        public Nullable<System.DateTime> POPLANDATE { get; set; }
        public string POPLANBY { get; set; }
        public string TRAFFICOFFICERID { get; set; }
        public string TRAFFICOFFICERNAME { get; set; }
        public string TRAFFICOFFICEREMAIL { get; set; }
        public Nullable<System.DateTime> CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public Nullable<System.DateTime> UPDATEDDATE { get; set; }
        public string UPDATEDBY { get; set; }
    
        public virtual MATERIALCOMPONENT MATERIALCOMPONENT { get; set; }
    }
}
