﻿var _roleOwner = "owner";
var _roleSponsor = "sponsor";
var _roleProjectManager = "projectManager";
var _roleProjectEngineer = "projectEngineer";
var _roleMaintenanceRep = "maintenanceRep";
var _roleOperationRep = "operationRep";
var _roleEngineerDesigner = "engineerDesigner";
var _roleEngineer = "engineer";
var _roleDesigner = "designer";

function uniqueArray(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}
function _showDialogMessage(title, content, url) {
    //console.log(dlgMessage);//.parent().find('.k-window-titlebar,.k-window-actions').css('backgroundColor', '#fe2712');
    dlgMessage.data("kendoDialog").content(content);
    dlgMessage.data("kendoDialog").title(title);
    dlgMessage.data("kendoDialog").open();

    if (url) {
        _redirectUrl = url;
    }

}

function common_toTitleCase(str) {
    if (str) {
        str = str.toLowerCase();
        return str.replace(/(?:^|\s)\w/g, function (match) {
            return match.toUpperCase();
        });
    } else {
        return str;
    }

}

function common_getDepartmentNameByCode(url, deptCode) {
    if (!deptCode)
        return "";

    var deptName = deptCode; // set default department name to department code
    $.ajax({
        url: url + "user/FindListDepartment/" + deptCode,
        type: "GET",
        async: false,
        success: function (result) {
            if (result) {
                if (result.length > 0) {
                    deptName = result[0].TABLE_DESC;
                }

            }

        }
    });
    return deptName;
}

function getDiscipline(disc) {
    var discipline = "";
    if (disc) {
        $.ajax({
            url: _webApiUrl + "lookup/FindByTypeAndValue/discipline|" + disc,
            type: "GET",
            async: false,
            success: function (result) {
                if (result.length > 0)
                    discipline = $.trim(result[0].text);
            },
            error: function (error) {
            }
        });
    }
    return discipline;
}

function common_getListDepartment(url) {
    
    var listOfDepartment = [];
   

    $.ajax({
        url: url + "user/FindListDepartment/all",
        type: "GET",
        async: false,
        success: function (result) {
            $.each(result, function (key, value) {

                value.TABLE_DESC = common_toTitleCase(value.TABLE_DESC);
            });

            
            listOfDepartment = result;
        }
    });
    
    return listOfDepartment;

}

function common_toJSON(paramData) {
    return JSON.parse(JSON.stringify(paramData));
}

    
var common_value_getPositionData = {};

function common_getAllPosition() {
    var rowDataDept = {};
    $.ajax({
        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_POSITION",
        type: "GET",
        async: false,
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                common_value_getPositionData[result[i].POSITION_ID] = result[i].POS_TITLE;
            }
        },
        error: function (error) {
        }
    });
}

function common_getPositionName(postCode) {
    if (postCode == null || postCode == "")
        return "";

    var name = postCode;
    if (common_value_getPositionData[postCode] != null)
        name = common_value_getPositionData[postCode];
    else name = "OTHERS";

    return name;
}

var common_value_getStatusIprom = {};

function common_getAllStatusIprom() {
    var rowDataDept = {};
    $.ajax({
        url: _webApiUrl + "lookup/findbytype/" + _statusIprom,
        type: "GET",
        async: false,
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                common_value_getStatusIprom[result[i].value] = result[i].text;
            }
        },
        error: function (error) {
        }
    });
}

function common_getStatusIpromDesc(value) {
    if (value == null || value == "")
        return "";

    var name = value;
    if (common_value_getStatusIprom[value] != null)
        name = common_value_getStatusIprom[value];
    else name = "OTHERS";

    return name;
}


function common_getTrafficImageUrl(value) {
    var result = "trafficlight-green.png";
    if (value == "Yellow")
        result = "trafficlight-yellow.png";
    else if (value == "Red")
        result = "trafficlight-red.png";
    else if (value == "Grey")
        result = "trafficlight-grey.png";
    return result;
}

function common_getTrafficHint(value) {
    var result = _trafficGreenHint;
    if (value == "Yellow")
        result = _trafficYellowHint;
    else if (value == "Red")
        result = _trafficRedHint;
    else if (value == "Grey")
        result = _trafficGreyHint;
    return result;
}

function common_getPositionByPositionID(param) {
    var positionDesc = "";
    $.ajax({
        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_POSNAME_BYID|@posID=" + param,
        type: "GET",
        async: false,
        success: function (result) {
            if (result.length > 0)
                positionDesc = $.trim(result[0].POS_TITLE);
        },
        error: function (error) {
        }
    });
    return positionDesc;
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}