﻿function common_numericTextBoxEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            max: 100,
            step: 1
        });
}

function common_textAreaEditor(container, options) {
    $('<textarea data-bind="value: ' + options.field + '" cols="20" rows="4" style="height: 60px; width: 100%;" maxlength="2000"></textarea>')
        .appendTo(container);
}
 
function common_dropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataTextField: "CategoryName",
            dataValueField: "CategoryID",
            dataSource: {
                type: "odata",
                transport: {
                    read: "//demos.telerik.com/kendo-ui/service/Northwind.svc/Categories"
                }
            }
        });
}