﻿// gatepass_request model
var common_projectModel = kendo.data.Model.define({
    id: "id",
    fields: {
         projectNo : { type: "string" },
         projectDescription : { type: "string" },
         projectType: { type: "string" },
         ownerId: { type: "string" },
         ownerName : { type: "string" },
         ownerEmail : { type: "string" },
         sponsorId : { type: "string" },
         sponsorName : { type: "string" },
         sponsorEmail : { type: "string" },
         projectManagerId : { type: "string" },
         projectManagerName : { type: "string" },
         projectManagerEmail : { type: "string" },
         projectEngineerId : { type: "string" },
         projectEngineerName : { type: "string" },
         projectEngineerEmail : { type: "string" },
         maintenanceRepId : { type: "string" },
         maintenanceRepName : { type: "string" },
         maintenanceRepEmail : { type: "string" },
         operationRepId : { type: "string" },
         operationRepName : { type: "string" },
         operationRepEmail : { type: "string" },
         areaInvolvedId : { type: "string" },
         projectType : { type: "string" },
         pmoNo : { type: "string" },
         planStartDate : { type: "date" },
         planFinishDate : { type: "date" },
         actualStartDate : { type: "date" },
         actualFinishDate : { type: "date" },
         totalCost : { type: "number" },
         actualCost: { type: "number" },
         commitment : { type: "number" },
         shareFolderDoc: { type: "string" },
         pmTrafic: { type: "number" },
         esTrafic: { type: "number" },
         materialTrafic: { type: "number" },
         consTrafic: { type: "number" },
         handoverCloseOutTrafic: { type: "number" },
         momewrid: { type: "number" }
    }
});

var common_projectDocumentModel = kendo.data.Model.define({
    id: "id",
    fields: {
        projectId: { type: "number" },
        docType: { type: "string" },
        docDesc: { type: "string" },
        docNo: { type: "string" },
        docTitle: { type: "string" },
        drawingNo: { type: "string" },
        planStartDate: { type: "date" },
        planFinishDate: { type: "date" },
        actualStartDate: { type: "date" },
        actualFinishDate: { type: "date" },
        discipline: { type: "string" },
        status: { type: "string" },
        remark: { type: "string" },
        executor: { type: "string" },
        executorAssignment: { type: "string" },
        progress: { type: "number" },
        documentById: { type: "string" },
        documentByName: { type: "string" },
        documentByEmail: { type: "string" },
        documentTrafic: { type: "number" }
    }
});

var common_projectManagementModel = kendo.data.Model.define({
    id: "id",
    fields: {
        projectId: { type: "number" },
        manageKeyIssue: { type: "string" },
        scope: { type: "string" },
        benefit: { type: "string" },
        accomplishment: { type: "string" },
        details: { type: "string" },
        contractor: { type: "string" }
    }
});

var common_keyDeliverableModel = kendo.data.Model.define({
    id: "id",
    fields: {
        projectManagementId: { type: "number" },
        projectId: { type: "number" },
        name: { type: "string" },
        weightPercentage: { type: "number" },
        planStartDate: { type: "date" },
        planFinishDate: { type: "date" },
        revision: { type: "number" },
        actualStartDate: { type: "date" },
        actualFinishDate: { type: "date" },
        status: { type: "string" },
        remark: { type: "string" }
    }
});

var common_ewrRequest = kendo.data.Model.define({
    id: "id",
    fields: {
        oid: { type: "number" },
        ewrNo: { type: "string" },
        subject: { type: "string" },
        area: { type: "string" },
        dateIssued: { type: "date" },
        accCode: { type: "string" },
        networkNo: { type: "string" },
        budgetAlloc: { type: "number" },
        dateCompletion: { type: "date" },
        problemStatement: { type: "string" },
        objective: { type: "string" },
        assignedPM: { type: "string" },
        projectSponsor: { type: "string" },
        projectSponsorBN: { type: "string" },
        sponsorLocation: { type: "string" },
        sponsorPhoneNo: { type: "string" },
        operationalCP: { type: "string" },
        projectTitle: { type: "string" },
        operationalCPEmail: { type: "string" },
        operationalCPBN: { type: "string" },
        operationalCP2: { type: "string" },
        operationalCP2Email: { type: "string" },
        operationalCP2BN: { type: "string" },
        sponsorEmail: { type: "string" },
        status: { type: "string" },
        categoryOfWork: { type: "json" },
        typeOfRequestedWork: { type: "json" },
        detailOfWork: { type: "json" }

    }
});

var common_ewrRequestDetail = kendo.data.Model.define({
    id: "id",
    fields: {
        ewrRequestId: { type: "number" },
        type: { type: "string" },
        value: { type: "string" },
        description: { type: "string" }
    }
});

var common_ewrMom = kendo.data.Model.define({
    id: "id",
    fields: {
        ewrRequestId: { type: "number" },
        participants: { type: "json" },
        momDate: { type: "date" },
        venue: { type: "string" },
        recordedBy: { type: "string" },
        background: { type: "string" },
        scopeOfWork: { type: "string" },
        projectType: { type: "json" },
        projectTitle: { type: "string" },
        projectManager: { type: "string" },
        projectDeliverable: { type: "string" },
        actionTimeLine: { type: "date" },
        sponsorCP: { type: "string" },
        dataRequired: { type: "string" }
    }
});