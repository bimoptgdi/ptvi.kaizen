/// <autosync enabled="true" />
/// <reference path="bootstrap.bundle.js" />
/// <reference path="bootstrap.js" />
/// <reference path="common/common_function.js" />
/// <reference path="common/common_grid_editor_component.js" />
/// <reference path="common/common_model.js" />
/// <reference path="common/common_template.js" />
/// <reference path="common/sc_technicalsupport.js" />
/// <reference path="esm/popper.js" />
/// <reference path="esm/popper-utils.js" />
/// <reference path="jquery.unobtrusive-ajax.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="jquery-3.3.1.js" />
/// <reference path="jquery-3.3.1.slim.js" />
/// <reference path="jquery-ui-1.12.1.js" />
/// <reference path="kendo/2016.3.914/angular.min.js" />
/// <reference path="kendo/2016.3.914/jquery.min.js" />
/// <reference path="kendo/2016.3.914/jszip.min.js" />
/// <reference path="kendo/2016.3.914/kendo.all.min.js" />
/// <reference path="kendo/2016.3.914/kendo.angular.min.js" />
/// <reference path="kendo/2016.3.914/kendo.angular2.min.js" />
/// <reference path="kendo/2016.3.914/kendo.autocomplete.min.js" />
/// <reference path="kendo/2016.3.914/kendo.binder.min.js" />
/// <reference path="kendo/2016.3.914/kendo.button.min.js" />
/// <reference path="kendo/2016.3.914/kendo.calendar.min.js" />
/// <reference path="kendo/2016.3.914/kendo.color.min.js" />
/// <reference path="kendo/2016.3.914/kendo.colorpicker.min.js" />
/// <reference path="kendo/2016.3.914/kendo.columnmenu.min.js" />
/// <reference path="kendo/2016.3.914/kendo.columnsorter.min.js" />
/// <reference path="kendo/2016.3.914/kendo.combobox.min.js" />
/// <reference path="kendo/2016.3.914/kendo.core.min.js" />
/// <reference path="kendo/2016.3.914/kendo.data.min.js" />
/// <reference path="kendo/2016.3.914/kendo.data.odata.min.js" />
/// <reference path="kendo/2016.3.914/kendo.data.signalr.min.js" />
/// <reference path="kendo/2016.3.914/kendo.data.xml.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.barcode.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.chart.funnel.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.chart.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.chart.polar.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.core.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.diagram.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.gauge.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.map.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.mobile.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.qrcode.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.sparkline.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.stock.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.themes.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dataviz.treemap.min.js" />
/// <reference path="kendo/2016.3.914/kendo.datepicker.min.js" />
/// <reference path="kendo/2016.3.914/kendo.datetimepicker.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dialog.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dom.min.js" />
/// <reference path="kendo/2016.3.914/kendo.draganddrop.min.js" />
/// <reference path="kendo/2016.3.914/kendo.drawing.min.js" />
/// <reference path="kendo/2016.3.914/kendo.dropdownlist.min.js" />
/// <reference path="kendo/2016.3.914/kendo.editable.min.js" />
/// <reference path="kendo/2016.3.914/kendo.editor.min.js" />
/// <reference path="kendo/2016.3.914/kendo.excel.min.js" />
/// <reference path="kendo/2016.3.914/kendo.filebrowser.min.js" />
/// <reference path="kendo/2016.3.914/kendo.filtercell.min.js" />
/// <reference path="kendo/2016.3.914/kendo.filtermenu.min.js" />
/// <reference path="kendo/2016.3.914/kendo.fx.min.js" />
/// <reference path="kendo/2016.3.914/kendo.gantt.list.min.js" />
/// <reference path="kendo/2016.3.914/kendo.gantt.min.js" />
/// <reference path="kendo/2016.3.914/kendo.gantt.timeline.min.js" />
/// <reference path="kendo/2016.3.914/kendo.grid.min.js" />
/// <reference path="kendo/2016.3.914/kendo.groupable.min.js" />
/// <reference path="kendo/2016.3.914/kendo.imagebrowser.min.js" />
/// <reference path="kendo/2016.3.914/kendo.list.min.js" />
/// <reference path="kendo/2016.3.914/kendo.listview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.maskedtextbox.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mediaplayer.min.js" />
/// <reference path="kendo/2016.3.914/kendo.menu.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.actionsheet.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.application.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.button.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.buttongroup.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.collapsible.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.drawer.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.listview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.loader.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.modalview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.navbar.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.pane.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.popover.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.scroller.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.scrollview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.shim.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.splitview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.switch.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.tabstrip.min.js" />
/// <reference path="kendo/2016.3.914/kendo.mobile.view.min.js" />
/// <reference path="kendo/2016.3.914/kendo.multiselect.min.js" />
/// <reference path="kendo/2016.3.914/kendo.notification.min.js" />
/// <reference path="kendo/2016.3.914/kendo.numerictextbox.min.js" />
/// <reference path="kendo/2016.3.914/kendo.ooxml.min.js" />
/// <reference path="kendo/2016.3.914/kendo.pager.min.js" />
/// <reference path="kendo/2016.3.914/kendo.panelbar.min.js" />
/// <reference path="kendo/2016.3.914/kendo.pdf.min.js" />
/// <reference path="kendo/2016.3.914/kendo.pivot.configurator.min.js" />
/// <reference path="kendo/2016.3.914/kendo.pivot.fieldmenu.min.js" />
/// <reference path="kendo/2016.3.914/kendo.pivotgrid.min.js" />
/// <reference path="kendo/2016.3.914/kendo.popup.min.js" />
/// <reference path="kendo/2016.3.914/kendo.progressbar.min.js" />
/// <reference path="kendo/2016.3.914/kendo.reorderable.min.js" />
/// <reference path="kendo/2016.3.914/kendo.resizable.min.js" />
/// <reference path="kendo/2016.3.914/kendo.responsivepanel.min.js" />
/// <reference path="kendo/2016.3.914/kendo.router.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.agendaview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.dayview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.monthview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.recurrence.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.timelineview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.scheduler.view.min.js" />
/// <reference path="kendo/2016.3.914/kendo.selectable.min.js" />
/// <reference path="kendo/2016.3.914/kendo.slider.min.js" />
/// <reference path="kendo/2016.3.914/kendo.sortable.min.js" />
/// <reference path="kendo/2016.3.914/kendo.splitter.min.js" />
/// <reference path="kendo/2016.3.914/kendo.spreadsheet.min.js" />
/// <reference path="kendo/2016.3.914/kendo.tabstrip.min.js" />
/// <reference path="kendo/2016.3.914/kendo.timepicker.min.js" />
/// <reference path="kendo/2016.3.914/kendo.timezones.min.js" />
/// <reference path="kendo/2016.3.914/kendo.toolbar.min.js" />
/// <reference path="kendo/2016.3.914/kendo.tooltip.min.js" />
/// <reference path="kendo/2016.3.914/kendo.touch.min.js" />
/// <reference path="kendo/2016.3.914/kendo.treelist.min.js" />
/// <reference path="kendo/2016.3.914/kendo.treeview.draganddrop.min.js" />
/// <reference path="kendo/2016.3.914/kendo.treeview.min.js" />
/// <reference path="kendo/2016.3.914/kendo.upload.min.js" />
/// <reference path="kendo/2016.3.914/kendo.userevents.min.js" />
/// <reference path="kendo/2016.3.914/kendo.validator.min.js" />
/// <reference path="kendo/2016.3.914/kendo.view.min.js" />
/// <reference path="kendo/2016.3.914/kendo.virtuallist.min.js" />
/// <reference path="kendo/2016.3.914/kendo.web.min.js" />
/// <reference path="kendo/2016.3.914/kendo.webcomponents.min.js" />
/// <reference path="kendo/2016.3.914/kendo.window.min.js" />
/// <reference path="kendo/2016.3.914/pako_deflate.min.js" />
/// <reference path="kendo/2017.1.118/angular.min.js" />
/// <reference path="kendo/2017.1.118/jquery.min.js" />
/// <reference path="kendo/2017.1.118/jszip.min.js" />
/// <reference path="kendo/2017.1.118/kendo.all.min.js" />
/// <reference path="kendo/2017.1.118/kendo.angular.min.js" />
/// <reference path="kendo/2017.1.118/kendo.autocomplete.min.js" />
/// <reference path="kendo/2017.1.118/kendo.binder.min.js" />
/// <reference path="kendo/2017.1.118/kendo.button.min.js" />
/// <reference path="kendo/2017.1.118/kendo.calendar.min.js" />
/// <reference path="kendo/2017.1.118/kendo.color.min.js" />
/// <reference path="kendo/2017.1.118/kendo.colorpicker.min.js" />
/// <reference path="kendo/2017.1.118/kendo.columnmenu.min.js" />
/// <reference path="kendo/2017.1.118/kendo.columnsorter.min.js" />
/// <reference path="kendo/2017.1.118/kendo.combobox.min.js" />
/// <reference path="kendo/2017.1.118/kendo.core.min.js" />
/// <reference path="kendo/2017.1.118/kendo.data.min.js" />
/// <reference path="kendo/2017.1.118/kendo.data.odata.min.js" />
/// <reference path="kendo/2017.1.118/kendo.data.signalr.min.js" />
/// <reference path="kendo/2017.1.118/kendo.data.xml.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.barcode.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.chart.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.core.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.diagram.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.gauge.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.map.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.mobile.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.qrcode.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.sparkline.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.stock.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.themes.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dataviz.treemap.min.js" />
/// <reference path="kendo/2017.1.118/kendo.datepicker.min.js" />
/// <reference path="kendo/2017.1.118/kendo.datetimepicker.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dialog.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dom.min.js" />
/// <reference path="kendo/2017.1.118/kendo.draganddrop.min.js" />
/// <reference path="kendo/2017.1.118/kendo.drawing.min.js" />
/// <reference path="kendo/2017.1.118/kendo.dropdownlist.min.js" />
/// <reference path="kendo/2017.1.118/kendo.editable.min.js" />
/// <reference path="kendo/2017.1.118/kendo.editor.min.js" />
/// <reference path="kendo/2017.1.118/kendo.excel.min.js" />
/// <reference path="kendo/2017.1.118/kendo.filebrowser.min.js" />
/// <reference path="kendo/2017.1.118/kendo.filtercell.min.js" />
/// <reference path="kendo/2017.1.118/kendo.filtermenu.min.js" />
/// <reference path="kendo/2017.1.118/kendo.fx.min.js" />
/// <reference path="kendo/2017.1.118/kendo.gantt.list.min.js" />
/// <reference path="kendo/2017.1.118/kendo.gantt.min.js" />
/// <reference path="kendo/2017.1.118/kendo.gantt.timeline.min.js" />
/// <reference path="kendo/2017.1.118/kendo.grid.min.js" />
/// <reference path="kendo/2017.1.118/kendo.groupable.min.js" />
/// <reference path="kendo/2017.1.118/kendo.imagebrowser.min.js" />
/// <reference path="kendo/2017.1.118/kendo.list.min.js" />
/// <reference path="kendo/2017.1.118/kendo.listview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.maskedtextbox.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mediaplayer.min.js" />
/// <reference path="kendo/2017.1.118/kendo.menu.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.actionsheet.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.application.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.button.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.buttongroup.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.collapsible.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.drawer.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.listview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.loader.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.modalview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.navbar.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.pane.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.popover.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.scroller.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.scrollview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.shim.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.splitview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.switch.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.tabstrip.min.js" />
/// <reference path="kendo/2017.1.118/kendo.mobile.view.min.js" />
/// <reference path="kendo/2017.1.118/kendo.multiselect.min.js" />
/// <reference path="kendo/2017.1.118/kendo.notification.min.js" />
/// <reference path="kendo/2017.1.118/kendo.numerictextbox.min.js" />
/// <reference path="kendo/2017.1.118/kendo.ooxml.min.js" />
/// <reference path="kendo/2017.1.118/kendo.pager.min.js" />
/// <reference path="kendo/2017.1.118/kendo.panelbar.min.js" />
/// <reference path="kendo/2017.1.118/kendo.pdf.min.js" />
/// <reference path="kendo/2017.1.118/kendo.pivot.configurator.min.js" />
/// <reference path="kendo/2017.1.118/kendo.pivot.fieldmenu.min.js" />
/// <reference path="kendo/2017.1.118/kendo.pivotgrid.min.js" />
/// <reference path="kendo/2017.1.118/kendo.popup.min.js" />
/// <reference path="kendo/2017.1.118/kendo.progressbar.min.js" />
/// <reference path="kendo/2017.1.118/kendo.reorderable.min.js" />
/// <reference path="kendo/2017.1.118/kendo.resizable.min.js" />
/// <reference path="kendo/2017.1.118/kendo.responsivepanel.min.js" />
/// <reference path="kendo/2017.1.118/kendo.router.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.agendaview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.dayview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.monthview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.recurrence.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.timelineview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.scheduler.view.min.js" />
/// <reference path="kendo/2017.1.118/kendo.selectable.min.js" />
/// <reference path="kendo/2017.1.118/kendo.slider.min.js" />
/// <reference path="kendo/2017.1.118/kendo.sortable.min.js" />
/// <reference path="kendo/2017.1.118/kendo.splitter.min.js" />
/// <reference path="kendo/2017.1.118/kendo.spreadsheet.min.js" />
/// <reference path="kendo/2017.1.118/kendo.tabstrip.min.js" />
/// <reference path="kendo/2017.1.118/kendo.timepicker.min.js" />
/// <reference path="kendo/2017.1.118/kendo.timezones.min.js" />
/// <reference path="kendo/2017.1.118/kendo.toolbar.min.js" />
/// <reference path="kendo/2017.1.118/kendo.tooltip.min.js" />
/// <reference path="kendo/2017.1.118/kendo.touch.min.js" />
/// <reference path="kendo/2017.1.118/kendo.treelist.min.js" />
/// <reference path="kendo/2017.1.118/kendo.treeview.draganddrop.min.js" />
/// <reference path="kendo/2017.1.118/kendo.treeview.min.js" />
/// <reference path="kendo/2017.1.118/kendo.upload.min.js" />
/// <reference path="kendo/2017.1.118/kendo.userevents.min.js" />
/// <reference path="kendo/2017.1.118/kendo.validator.min.js" />
/// <reference path="kendo/2017.1.118/kendo.view.min.js" />
/// <reference path="kendo/2017.1.118/kendo.virtuallist.min.js" />
/// <reference path="kendo/2017.1.118/kendo.web.min.js" />
/// <reference path="kendo/2017.1.118/kendo.window.min.js" />
/// <reference path="kendo/2017.1.118/pako_deflate.min.js" />
/// <reference path="kendo/2018.3.911/angular.min.js" />
/// <reference path="kendo/2018.3.911/jszip.min.js" />
/// <reference path="kendo/2018.3.911/kendo.all.min.js" />
/// <reference path="kendo/2018.3.911/kendo.angular.min.js" />
/// <reference path="kendo/2018.3.911/kendo.autocomplete.min.js" />
/// <reference path="kendo/2018.3.911/kendo.binder.min.js" />
/// <reference path="kendo/2018.3.911/kendo.button.min.js" />
/// <reference path="kendo/2018.3.911/kendo.buttongroup.min.js" />
/// <reference path="kendo/2018.3.911/kendo.calendar.min.js" />
/// <reference path="kendo/2018.3.911/kendo.chat.min.js" />
/// <reference path="kendo/2018.3.911/kendo.color.min.js" />
/// <reference path="kendo/2018.3.911/kendo.colorpicker.min.js" />
/// <reference path="kendo/2018.3.911/kendo.columnmenu.min.js" />
/// <reference path="kendo/2018.3.911/kendo.columnsorter.min.js" />
/// <reference path="kendo/2018.3.911/kendo.combobox.min.js" />
/// <reference path="kendo/2018.3.911/kendo.core.min.js" />
/// <reference path="kendo/2018.3.911/kendo.data.min.js" />
/// <reference path="kendo/2018.3.911/kendo.data.odata.min.js" />
/// <reference path="kendo/2018.3.911/kendo.data.signalr.min.js" />
/// <reference path="kendo/2018.3.911/kendo.data.xml.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.barcode.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.chart.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.core.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.diagram.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.gauge.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.map.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.mobile.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.qrcode.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.sparkline.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.stock.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.themes.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dataviz.treemap.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dateinput.min.js" />
/// <reference path="kendo/2018.3.911/kendo.datepicker.min.js" />
/// <reference path="kendo/2018.3.911/kendo.datetimepicker.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dialog.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dom.min.js" />
/// <reference path="kendo/2018.3.911/kendo.draganddrop.min.js" />
/// <reference path="kendo/2018.3.911/kendo.drawing.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dropdownlist.min.js" />
/// <reference path="kendo/2018.3.911/kendo.dropdowntree.min.js" />
/// <reference path="kendo/2018.3.911/kendo.editable.min.js" />
/// <reference path="kendo/2018.3.911/kendo.editor.min.js" />
/// <reference path="kendo/2018.3.911/kendo.excel.min.js" />
/// <reference path="kendo/2018.3.911/kendo.filebrowser.min.js" />
/// <reference path="kendo/2018.3.911/kendo.filtercell.min.js" />
/// <reference path="kendo/2018.3.911/kendo.filtermenu.min.js" />
/// <reference path="kendo/2018.3.911/kendo.fx.min.js" />
/// <reference path="kendo/2018.3.911/kendo.gantt.list.min.js" />
/// <reference path="kendo/2018.3.911/kendo.gantt.min.js" />
/// <reference path="kendo/2018.3.911/kendo.gantt.timeline.min.js" />
/// <reference path="kendo/2018.3.911/kendo.grid.min.js" />
/// <reference path="kendo/2018.3.911/kendo.groupable.min.js" />
/// <reference path="kendo/2018.3.911/kendo.imagebrowser.min.js" />
/// <reference path="kendo/2018.3.911/kendo.list.min.js" />
/// <reference path="kendo/2018.3.911/kendo.listbox.min.js" />
/// <reference path="kendo/2018.3.911/kendo.listview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.maskedtextbox.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mediaplayer.min.js" />
/// <reference path="kendo/2018.3.911/kendo.menu.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.actionsheet.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.application.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.button.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.buttongroup.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.collapsible.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.drawer.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.listview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.loader.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.modalview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.navbar.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.pane.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.popover.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.scroller.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.scrollview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.shim.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.splitview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.switch.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.tabstrip.min.js" />
/// <reference path="kendo/2018.3.911/kendo.mobile.view.min.js" />
/// <reference path="kendo/2018.3.911/kendo.multicolumncombobox.min.js" />
/// <reference path="kendo/2018.3.911/kendo.multiselect.min.js" />
/// <reference path="kendo/2018.3.911/kendo.notification.min.js" />
/// <reference path="kendo/2018.3.911/kendo.numerictextbox.min.js" />
/// <reference path="kendo/2018.3.911/kendo.ooxml.min.js" />
/// <reference path="kendo/2018.3.911/kendo.pager.min.js" />
/// <reference path="kendo/2018.3.911/kendo.panelbar.min.js" />
/// <reference path="kendo/2018.3.911/kendo.pdf.min.js" />
/// <reference path="kendo/2018.3.911/kendo.pivot.configurator.min.js" />
/// <reference path="kendo/2018.3.911/kendo.pivot.fieldmenu.min.js" />
/// <reference path="kendo/2018.3.911/kendo.pivotgrid.min.js" />
/// <reference path="kendo/2018.3.911/kendo.popup.min.js" />
/// <reference path="kendo/2018.3.911/kendo.progressbar.min.js" />
/// <reference path="kendo/2018.3.911/kendo.reorderable.min.js" />
/// <reference path="kendo/2018.3.911/kendo.resizable.min.js" />
/// <reference path="kendo/2018.3.911/kendo.responsivepanel.min.js" />
/// <reference path="kendo/2018.3.911/kendo.router.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.agendaview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.dayview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.monthview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.recurrence.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.timelineview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.scheduler.view.min.js" />
/// <reference path="kendo/2018.3.911/kendo.selectable.min.js" />
/// <reference path="kendo/2018.3.911/kendo.slider.min.js" />
/// <reference path="kendo/2018.3.911/kendo.sortable.min.js" />
/// <reference path="kendo/2018.3.911/kendo.splitter.min.js" />
/// <reference path="kendo/2018.3.911/kendo.spreadsheet.min.js" />
/// <reference path="kendo/2018.3.911/kendo.tabstrip.min.js" />
/// <reference path="kendo/2018.3.911/kendo.timepicker.min.js" />
/// <reference path="kendo/2018.3.911/kendo.timezones.min.js" />
/// <reference path="kendo/2018.3.911/kendo.toolbar.min.js" />
/// <reference path="kendo/2018.3.911/kendo.tooltip.min.js" />
/// <reference path="kendo/2018.3.911/kendo.touch.min.js" />
/// <reference path="kendo/2018.3.911/kendo.treelist.min.js" />
/// <reference path="kendo/2018.3.911/kendo.treeview.draganddrop.min.js" />
/// <reference path="kendo/2018.3.911/kendo.treeview.min.js" />
/// <reference path="kendo/2018.3.911/kendo.upload.min.js" />
/// <reference path="kendo/2018.3.911/kendo.userevents.min.js" />
/// <reference path="kendo/2018.3.911/kendo.validator.min.js" />
/// <reference path="kendo/2018.3.911/kendo.view.min.js" />
/// <reference path="kendo/2018.3.911/kendo.virtuallist.min.js" />
/// <reference path="kendo/2018.3.911/kendo.web.min.js" />
/// <reference path="kendo/2018.3.911/kendo.webcomponents.js" />
/// <reference path="kendo/2018.3.911/kendo.window.min.js" />
/// <reference path="kendo/2018.3.911/pako_deflate.min.js" />
/// <reference path="knockout-3.4.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="popper.js" />
/// <reference path="popper-utils.js" />
/// <reference path="respond.matchmedia.addlistener.js" />
/// <reference path="respond.min.js" />
/// <reference path="umd/popper.js" />
/// <reference path="umd/popper-utils.js" />
