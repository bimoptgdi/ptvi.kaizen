﻿using PTVI.KAIZEN.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class BASEController : ApiController
    {
        public HttpResponseMessage Options()
        {
            var resp = Request.CreateResponse(HttpStatusCode.OK);
            resp.Content = new StringContent(string.Empty);
            resp.Content.Headers.Add("Allow", "GET,POST,PUT,OPTIONS,DELETE");
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        protected VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        protected bool _debugMode = Convert.ToBoolean(global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_MODE);
        public List<NTUserIDName> ListNTUserIDName = new List<NTUserIDName>();

        protected string GetCurrentNTUserId()
        {
            if (global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.IsDebug)
                return "Herus";


            string ntUserId = "";

            System.Security.Principal.IIdentity identity = (Request.Properties["MS_HttpContext"] as System.Web.HttpContextWrapper).Request.LogonUserIdentity;
            string userName = identity.Name;

            string[] identityName = userName.Split('\\');
            if (identityName.Length > 1)
            {
                ntUserId = identityName[1];
            }
            else
            {
                ntUserId = userName;
            }

            return ntUserId;
        }


        /// <summary>
        /// Find Name in temporary List of NTUserId : Name Object
        ///  insert if not found
        /// </summary>
        /// <param name="NTUserId"></param>
        /// <returns></returns>
        //public NTUserIDName findInListNTUserIDName(string NTUserId)
        //{
        //    AccountController = new AccountController();
        //    var m = ListNTUserIDName.Find(x => x.ntUserId == NTUserId);
        //    if (m == null)
        //    {
        //        var accCtl = accController.EmployeeData(NTUserId);
        //        NTUserIDName ntName = new NTUserIDName();
        //        if (accCtl != null)
        //            ntName.name = accCtl.FULL_NAME;
        //        else
        //            ntName.name = "";
        //        ntName.ntUserId = NTUserId;
        //        ListNTUserIDName.Add(ntName);
        //        return ntName;
        //    }
        //    else
        //        return m;
        //}


        public Dictionary<string, dynamic> GetJsonObjectFromWebApi(string url)
        {
            Uri uri = new Uri(url);

            WebRequest webRequest = WebRequest.Create(uri);
            webRequest.UseDefaultCredentials = true;

            using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
            {
                StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                string json = reader.ReadToEnd();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                //var output = serializer.Deserialize<Dictionary<string, dynamic>>(json);
                var output = serializer.Deserialize<Dictionary<string, dynamic>>(json);

                return output;
            }

            //return null;
        }
        public DataTable GetListFromExternalData(string apiUrl)
        {
            // initiate web api client 
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            HttpClient client = new HttpClient(handler);
            List<USERNONEMPLOYEE> list = new List<USERNONEMPLOYEE>();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var serializer = new JavaScriptSerializer();

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;

            if (response.IsSuccessStatusCode)
            {
                list = response.Content.ReadAsAsync<List<USERNONEMPLOYEE>>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsStringAsync().Result;
                throw new ApplicationException(exception);
            }

            DataTable result = new DataTable();
            result.Columns.Add("EMPLOYEEID");
            result.Columns.Add("FULL_NAME");
            result.Columns.Add("USERNAME");
            result.Columns.Add("EMAIL");
            result.Columns.Add("DEPT_CODE");

            foreach (USERNONEMPLOYEE emp in list)
            //{
            //    DataRow row = result.NewRow();
            //    row["EMPLOYEEID"] = emp.EMPLOYEEID;
            //    row["FULL_NAME"] = emp.FULL_NAME;
            //    row["USERNAME"] = emp.USERNAME;
            //    row["EMAIL"] = emp.EMAIL;
            //    row["DEPT_CODE"] = emp.DEPT_CODE;

            //    result.Rows.Add(row);

            //}

            result.AcceptChanges();

            return result;
        }

        protected DataTable getDataByQuery(string qry, System.Data.SqlClient.SqlConnection connection)
        {
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = new System.Data.SqlClient.SqlCommand(qry, connection);

            DataTable dt = new DataTable();
            adapter.Fill(dt);

            return dt;
        }

        public List<T> GetListFromExternalData<T>(string apiUrl)
        {
            // initiate web api client 
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            HttpClient client = new HttpClient(handler);
            List<T> list = new List<T>();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var serializer = new JavaScriptSerializer();

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;

            if (response.IsSuccessStatusCode)
            {
                list = response.Content.ReadAsAsync<List<T>>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsStringAsync().Result;
                throw new ApplicationException(exception);
            }

            return list;
        }

        public List<Dictionary<string, dynamic>> GetJsonDataFromWebApi(string url)
        {
            try
            {
                List<Dictionary<string, dynamic>> output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }
                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //public OData GetJsonDataFromWebApiOData(string url)
        //{
        //    try
        //    {
        //        OData output;

        //        Uri uri = new Uri(url);
        //        WebRequest webRequest = WebRequest.Create(uri);
        //        webRequest.UseDefaultCredentials = true;

        //        using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
        //        {
        //            StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
        //            string json = reader.ReadToEnd();

        //            JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            //output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);

        //            output = serializer.Deserialize<OData>(json);


        //        }

        //        return output;
        //    }
        //    catch (WebException ex)
        //    {
        //        String errorText = "";
        //        WebResponse errorResponse = ex.Response;
        //        using (Stream responseStream = errorResponse.GetResponseStream())
        //        {
        //            StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
        //            errorText = reader.ReadToEnd();


        //        }
        //        throw new WebException(errorText);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.ToString());
        //    }
        //}

        public class FileData
        {
            public string ContentType { get; set; }
            public string Base64 { get; set; }
            public string FileName { get; set; }
        }

        // POST api/adr/ExportToExcelProxy
        [HttpPost]
        [ActionName("ExportToExcelProxy")]
        public HttpResponseMessage ExportToExcelProxy(string param, [FromBody]FileData file)
        {
            var data = Convert.FromBase64String(file.Base64);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(data))
            };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = file.FileName
            };

            return result;
        }

        public bool IsUserContractor(string userID)
        {
            if (userID.IndexOf("C") == 0)
            {
                return true;
            }
            return false;
        }
        public void WriteLog(string p)
        {
            //string logPath = global::PTVI.OSC.WebAPI.Properties.Settings.Default.LogPath;
            //change to appdata
            string logPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/log/");
            using (StreamWriter writer = new StreamWriter(new FileStream(logPath + "Log.txt", FileMode.Append)))
            {
                writer.WriteLine(DatetimeNow() + ": " + p);
            }
        }

        protected string TemporaryUploadLocation
        {
            get
            {
                //return global::PTVI.OSC.WebAPI.Properties.Settings.Default.AttachmentTempPath;
                // change to appdata 
                return System.Web.HttpContext.Current.Server.MapPath("~/SupportingDocuments/temp/");
            }

        }

        protected string EprocCommonServiceUrl
        {
            get
            {
                return WebConfigurationManager.AppSettings["EProcCommonServiceUrl"];
            }
        }

        protected string PermanentUploadLocation
        {
            get
            {
                //return global::PTVI.OSC.WebAPI.Properties.Settings.Default.AttachmentPath;
                // change to appdata 
                return System.Web.HttpContext.Current.Server.MapPath("~/SupportingDocuments/doc/");
            }

        }

        //protected string AttachmentLink
        //{
        //    get
        //    {
        //        return global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.AttachmentLink;
        //    }

        //}

        public String uploadDocumentToDocRepo(string uri, string fileName, int oid, string uploadedBy)
        {
            String result = "";
            //GENERALPARAMETER parameter = db.GENERALPARAMETERs.Find(Constants.DOCUMENT_REPOSITORY_CID);
            //if (parameter == null)
                throw new WebException("Configuration for document repository cid not found. Please contact administrator.");

            //DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

            //string parent = parameter.VALUE;
            System.IO.FileStream openStream = new System.IO.FileStream(uri, System.IO.FileMode.Open);
            System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(openStream);
            byte[] data = binaryReader.ReadBytes(Convert.ToInt32(openStream.Length));
            binaryReader.Close();

            int recId = oid;

            //service.Credentials = new System.Net.NetworkCredential("aramdhani", "vale20141", "inco");
            //result = service.UploadFile(data, fileName, parent, recId, uploadedBy);

            return result;
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        protected DateTime GetTodayUtc()
        {
            return DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
        }

        protected string GetDataUri(string fileName)
        {
            var uri = "";

            try
            {
                if (fileName.Contains("."))
                {
                    uri = "http://osc.valeindonesia.co.id/webapi/api/upload/GetImage/" + fileName;
                }
                else
                {
                    uri = "http://osc.valeindonesia.co.id/webapi/api/upload/GetImage/" + fileName + ".jpg";
                }
            }
            catch (Exception)
            {
                throw;
            }

            //try
            //{
            //    var bytes = File.ReadAllBytes(url);
            //    var b64String = Convert.ToBase64String(bytes);
            //    var uri = "data:image/" + Path.GetExtension(url).Replace(".", "") + ";base64," + b64String;
            //    return uri;
            //}
            //catch
            //{
            //    return "";
            //}

            return uri;
        }

        protected DateTime VerifyDateUtc(DateTime input)
        {
            if (input.Kind == DateTimeKind.Utc)
                return input.ToLocalTime();
            else
                return input;
        }

        protected enum PersonalInfoLogType { Approved, Deactivate, Suspend }

        protected bool WritePersonalInfoLog(int personalInfoID, PersonalInfoLogType type, DateTime? suspendFromDate, DateTime? suspendToDate)
        {
            VALE_KAIZENEntities db = new VALE_KAIZENEntities();

            PERSONALINFO pInfo = db.PERSONALINFOes.Find(personalInfoID);
            if (pInfo != null)
            {
                var contractDesc = "";
                CONTRACTLIST contractList = db.CONTRACTLISTs.Find(pInfo.ContractNo);
                if (contractList != null)
                    contractDesc = string.Format("{0} - {1}", contractList.ContractNo, contractList.ProjectTitle);
                else
                    contractDesc = contractList.ContractNo;

                var remarks = "";
                switch (type)
                {
                    case PersonalInfoLogType.Approved:
                        remarks = string.Format("Badge Approved: BN {0} is approved under contract no {1}", pInfo.BadgeNo, contractDesc);
                        break;
                    case PersonalInfoLogType.Deactivate:
                        remarks = string.Format("Badge Deactivate: BN {0} is deactivate under contract no {1}", pInfo.BadgeNo, contractDesc);
                        break;
                    case PersonalInfoLogType.Suspend:
                        remarks = string.Format("Input Suspend: BN {0} is suspended from {1} to {2}", pInfo.BadgeNo, suspendFromDate.HasValue ? suspendFromDate.Value.ToString("dd MMM yyyy") : "NA", suspendToDate.HasValue ? suspendToDate.Value.ToString("dd MMM yyyy") : "NA");
                        break;
                }

                PERSONALLOG personalLog = new PERSONALLOG()
                {
                    BadgeNo = pInfo.BadgeNo,
                    CreatedBy = GetCurrentNTUserId(),
                    CreatedDate = GetTodayUtc(),
                    LogDateTime = GetTodayUtc(),
                    PersonalID = personalInfoID,
                    Remarks = remarks
                };

                db.PERSONALLOGs.Add(personalLog);
                db.SaveChanges();

                return true;
            }

            return false;


        }

        protected string WebAppUrl
        {
            get
            {
                return WebConfigurationManager.AppSettings["WebAppUrl"];
            }
        }

        protected DateTime DatetimeNow()
        {
            var witaNow = DateTime.Now;
            try
            {
                //witaNow = DateTime.UtcNow.AddHours(global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.TimeOffSet);
            }
            catch
            {
                witaNow = DateTime.UtcNow.AddHours(8);
            }

            return witaNow;
        }

    }
}
