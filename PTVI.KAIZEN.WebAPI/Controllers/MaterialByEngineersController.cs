﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.IO;
using Excel;
using System.Text.RegularExpressions;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MaterialByEngineersController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accController = new AccountController();

        // GET: api/MaterialByEngineers
        public IQueryable<materialByEngineerObject> GetMATERIALBYENGINEERs()
        {
            return db.MATERIALBYENGINEERs.Select(s => new materialByEngineerObject
            {
                id = s.OID,
                mrNo = s.MRNO,
                materialDesc = s.MATERIALDESC,
                prPlanDate = s.PRPLANDATE,
                prActualDate = s.PRACTUALDATE,
                poPlanDate = s.POPLANDATE,
                poActualDate = s.POACTUALDATE,
                onSitePlanDate = s.ONSITEPLANDATE,
                onSiteActualDate = s.ONSITEACTUALDATE,
                rfqPlanDate = s.RFQPLANDATE,
                rfqActualDate = s.RFQACTUALDATE,
                revision = s.REVISION//,
                //engineer = string.IsNullOrEmpty(s.CREATEDBY) ? "-" : accController.getEmployeeByBN(s.CREATEDBY).full_Name
            });
        }

        // GET: api/MaterialByEngineers/5
        [ResponseType(typeof(MATERIALBYENGINEER))]
        public IHttpActionResult GetMATERIALBYENGINEER(int id)
        {
            MATERIALBYENGINEER mATERIALBYENGINEER = db.MATERIALBYENGINEERs.Find(id);
            if (mATERIALBYENGINEER == null)
            {
                return NotFound();
            }

            return Ok(mATERIALBYENGINEER);
        }

        // PUT: api/MaterialByEngineers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMATERIALBYENGINEER(int id, MATERIALBYENGINEER mATERIALBYENGINEER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mATERIALBYENGINEER.OID)
            {
                return BadRequest();
            }

            db.Entry(mATERIALBYENGINEER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MATERIALBYENGINEERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MaterialByEngineers
        [ResponseType(typeof(MATERIALBYENGINEER))]
        public IHttpActionResult PostMATERIALBYENGINEER(MATERIALBYENGINEER mATERIALBYENGINEER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MATERIALBYENGINEERs.Add(mATERIALBYENGINEER);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mATERIALBYENGINEER.OID }, mATERIALBYENGINEER);
        }

        // DELETE: api/MaterialByEngineers/5
        [ResponseType(typeof(MATERIALBYENGINEER))]
        public IHttpActionResult DeleteMATERIALBYENGINEER(int id)
        {
            MATERIALBYENGINEER mATERIALBYENGINEER = db.MATERIALBYENGINEERs.Find(id);
            if (mATERIALBYENGINEER == null)
            {
                return NotFound();
            }

            db.MATERIALBYENGINEERs.Remove(mATERIALBYENGINEER);
            db.SaveChanges();

            return Ok(mATERIALBYENGINEER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        [ActionName("byProject")]
        public IEnumerable<materialByEngineerObject> byProject(int param)
        {
            return db.MATERIALBYENGINEERs.Where(w => w.PROJECTID == param).ToList().Select(s => new materialByEngineerObject
            {
                id = s.OID,
                mrNo = s.MRNO,
                materialDesc = s.MATERIALDESC,
                prPlanDate = s.PRPLANDATE,
                prActualDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PRISSUEDDATE),
                poPlanDate = s.POPLANDATE,
                poActualDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PORAISEDDATE),
                onSitePlanDate = s.ONSITEPLANDATE,
                remark = s.REMARK,
                onSiteActualDate = db.MATERIALSTATUS.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.SHIPMENTENDDATE),
                rfqPlanDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PRISSUEDDATE).HasValue ? db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PRISSUEDDATE).Value.AddDays(14) : (DateTime?)null,
                rfqActualDate = (DateTime?)null,
                revision = s.REVISION,
                engineer = string.IsNullOrEmpty(s.CREATEDBY) ? "-" : accController.getEmployeeByBN(s.CREATEDBY).full_Name,
                engineerId = string.IsNullOrEmpty(s.CREATEDBY) ? "-" : accController.getEmployeeByBN(s.CREATEDBY).employeeId
            }).OrderBy(o => !o.prActualDate.HasValue ? o.prPlanDate : !o.poActualDate.HasValue ? o.poPlanDate : !o.onSiteActualDate.HasValue ? o.onSitePlanDate : new DateTime(2999, 1, 1));
        }

        [HttpPost]
        [ActionName("AddMR")]
        public IHttpActionResult AddMR(int param, materialByEngineerObject materialByEngineerParam)
        {
            try
            {
                if (db.MATERIALBYENGINEERs.Where(x => x.MRNO == materialByEngineerParam.mrNo && x.PROJECTID == param).Count() == 0)
                {
                    MATERIALBYENGINEER materialByEngineer = new MATERIALBYENGINEER();
                    materialByEngineer.OID = 0;
                    materialByEngineer.PROJECTID = param;
                    materialByEngineer.MRNO = materialByEngineerParam.mrNo;
                    materialByEngineer.MATERIALDESC = materialByEngineerParam.materialDesc;
                    materialByEngineer.PRPLANDATE = materialByEngineerParam.prPlanDate;
                    materialByEngineer.POPLANDATE = materialByEngineerParam.poPlanDate;
                    materialByEngineer.ONSITEPLANDATE = materialByEngineerParam.onSitePlanDate;
                    materialByEngineer.REVISION = 0;
                    materialByEngineer.CREATEDBY = materialByEngineerParam.createdBy;
                    materialByEngineer.REMARK = materialByEngineerParam.remark;
                    materialByEngineer.CREATEDDATE = DateTime.Now;

                    db.MATERIALBYENGINEERs.Add(materialByEngineer);
                    db.SaveChanges();

                    materialByEngineerParam.id = materialByEngineer.OID;
                    materialByEngineerParam.engineer = string.IsNullOrEmpty(materialByEngineer.CREATEDBY) ? "-" : accController.getEmployeeByBN(materialByEngineer.CREATEDBY).full_Name;
                    return Ok(materialByEngineerParam);
                } else if (db.MATERIALBYENGINEERs.Where(x => x.MRNO == materialByEngineerParam.mrNo && x.PROJECTID == param).Count() > 0)
                {
                    return BadRequest("Failed: MR No. Exist");
                } else
                {
                    return BadRequest("Failed: Data not saved");
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
        }


        [HttpPut]
        [ActionName("UpdateMR")]
        public IHttpActionResult UpdateMR(int param, materialByEngineerObject materialByEngineerParam)
        {
            try
            {
                MATERIALBYENGINEER materialByEngineer = db.MATERIALBYENGINEERs.Find(param);
                materialByEngineer.MRNO = materialByEngineerParam.mrNo;
                materialByEngineer.MATERIALDESC = materialByEngineerParam.materialDesc;
                if (materialByEngineer.PRPLANDATE != materialByEngineerParam.prPlanDate ||
                        materialByEngineer.POPLANDATE != materialByEngineerParam.poPlanDate ||
                        materialByEngineer.ONSITEPLANDATE != materialByEngineerParam.onSitePlanDate)
                {
                    materialByEngineer.REVISION = materialByEngineer.REVISION + 1;
                }
                materialByEngineer.PRPLANDATE = materialByEngineerParam.prPlanDate;
                materialByEngineer.POPLANDATE = materialByEngineerParam.poPlanDate;
                materialByEngineer.ONSITEPLANDATE = materialByEngineerParam.onSitePlanDate;
                materialByEngineer.UPDATEDBY = materialByEngineerParam.updatedBy;
                materialByEngineer.REMARK = materialByEngineerParam.remark;
                materialByEngineer.UPDATEDDATE = DateTime.Now;

                db.Entry(materialByEngineer).State = EntityState.Modified;
                db.SaveChanges();


                return Ok(materialByEngineerParam);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
        }


        [HttpPost]
        [ActionName("Upload")]
        public System.Threading.Tasks.Task<HttpResponseMessage> Upload(string param)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();

            int count = 0;
            string listFailed = null;
            string listFailedDetail = null;

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    foreach (var stream in provider.Contents)
                    {
                        // Process The File
                        var type = Path.GetExtension(stream.Headers.ContentDisposition.FileName.Replace("\"", ""));

                        var fs = new MemoryStream(stream.ReadAsByteArrayAsync().Result);

                        var splitParam = param.Split('|');
                        int oid = int.Parse(splitParam[0]);
                        string updateBy = splitParam[1];

                        var returnVal = ProcessUploadMRListFile(fs, type, oid, updateBy).Split('|');
                        count += int.Parse(returnVal[0]);

                        if (!string.IsNullOrEmpty(listFailed))
                        {
                            listFailed += ", " + returnVal[1];
                        }
                        else
                        {
                            listFailed += returnVal[1];
                        }

                        if (!string.IsNullOrEmpty(listFailedDetail))
                        {
                            listFailedDetail += "; " + returnVal[2];
                        }
                        else
                        {
                            listFailedDetail += returnVal[2];
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
                var aaa = listFailed.Split(',').Count();

                return Request.CreateResponse(HttpStatusCode.OK, new { success = count, Failed = (listFailed.Split(',').Count() - 1), listFailed = listFailed, listFailedDetail = listFailedDetail });
            });

            return task;
        }

        private string ProcessUploadMRListFile(Stream stream, string fileType, int oid, string uploadedBy)
        {
            IExcelDataReader excelReader;
            if (Path.GetExtension(fileType) == ".xls")
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            else
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            excelReader.IsFirstRowAsColumnNames = true;

            DataSet ds = excelReader.AsDataSet(true);

            int saveCount = 0;
            var dt = ds.Tables[0];
            string listErrorRow = null;
            string listDetailErrorRow = null;
            int nowYear = DateTime.UtcNow.Year;
            List<MATERIALBYENGINEER> newListMaterial = new List<MATERIALBYENGINEER>();
            foreach (DataRow row in dt.Rows)
            {
                if (string.IsNullOrEmpty(row[1].ToString()))
                {
                    continue;
                }

                DateTime fromDateValue;
                string inpDetailErrorRow = null;

                var cekReturn = true;

                DateTime? prPlanDate = null;
                if (!DateTime.TryParse(row[3].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadMRListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadMRListFileMessage(inpDetailErrorRow, row[0].ToString(), "PR Plan Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    prPlanDate = fromDateValue;
                }

                DateTime? poPlanDate = null;
                if (!DateTime.TryParse(row[4].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadMRListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadMRListFileMessage(inpDetailErrorRow, row[0].ToString(), "PO Plan Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    poPlanDate = fromDateValue;
                }

                DateTime? onSitePlanDate = null;
                if (!DateTime.TryParse(row[5].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadMRListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadMRListFileMessage(inpDetailErrorRow, row[0].ToString(), "On site plan Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    onSitePlanDate = fromDateValue;
                }

                var mrNo = row[1].ToString();
                var mrDesc = row[2].ToString();

                MATERIALBYENGINEER material = db.MATERIALBYENGINEERs.Where(w => w.PROJECTID == oid && w.MRNO == mrNo).FirstOrDefault();

                if (material == null)
                {
                    material = new MATERIALBYENGINEER();
                    material.CREATEDDATE = DateTime.Now;
                    material.CREATEDBY = uploadedBy;
                    material.PROJECTID = oid;
                    material.REVISION = 0;
                }
                else
                {
                    material.UPDATEDDATE = DateTime.Now;
                    material.UPDATEDBY = uploadedBy;

                    if (material.PRPLANDATE != prPlanDate || material.POPLANDATE != poPlanDate || material.ONSITEPLANDATE != onSitePlanDate)
                        material.REVISION = material.REVISION + 1;
                }

                material.MRNO = mrNo;
                material.MATERIALDESC = mrDesc;
                material.PRPLANDATE = prPlanDate;
                material.POPLANDATE = poPlanDate;
                material.ONSITEPLANDATE = onSitePlanDate;

                if (material.OID > 0)
                {
                    db.Entry(material).State = EntityState.Modified;
                }
                else
                {
                    newListMaterial.Add(material);
                }
                ++saveCount;
            }

            db.MATERIALBYENGINEERs.AddRange(newListMaterial.GroupBy(g => g.MRNO).Select(s => new MATERIALBYENGINEER
            {
                CREATEDDATE = s.Max(m => m.CREATEDDATE),
                CREATEDBY = s.Max(m => m.CREATEDBY),
                UPDATEDDATE = s.Max(m => m.UPDATEDDATE),
                UPDATEDBY = s.Max(m => m.UPDATEDBY),
                PROJECTID = s.Max(m => m.PROJECTID),
                REVISION = s.Max(m => m.REVISION),
                MRNO = s.Key,
                MATERIALDESC = s.Max(m => m.MATERIALDESC),
                PRPLANDATE = s.Max(m => m.PRPLANDATE),
                POPLANDATE = s.Max(m => m.POPLANDATE),
                ONSITEPLANDATE = s.Max(m => m.ONSITEPLANDATE)

            }));
            db.SaveChanges();
            listErrorRow = saveCount.ToString() + "|" + listErrorRow + "|" + listDetailErrorRow;
            return listErrorRow;
        }

        private string errorProcessUploadMRListFile(string listErrorRow, string numberOfError)
        {
            if (!string.IsNullOrEmpty(listErrorRow))
            {
                listErrorRow += "," + numberOfError;
            }
            else
            {
                listErrorRow += numberOfError;
            }

            return listErrorRow;
        }

        private string errorProcessUploadMRListFileMessage(string inpDetailErrorRow, string row, string Message)
        {
            if (!string.IsNullOrEmpty(inpDetailErrorRow))
            {
                inpDetailErrorRow += ", ";
            }
            else
            {
                inpDetailErrorRow = "row " + row + ": ";
            }

            return inpDetailErrorRow += Message;
        }


        private bool MATERIALBYENGINEERExists(int id)
        {
            return db.MATERIALBYENGINEERs.Count(e => e.OID == id) > 0;
        }
    }
}