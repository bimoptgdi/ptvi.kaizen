﻿using Microsoft.ProjectServer.Client;
//using net.sf.mpxj;
//using net.sf.mpxj.MpxjUtilities;
//using net.sf.mpxj.reader;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTVI.KAIZEN.WebAPI.PSI_ProjectWS;
using PTVI.KAIZEN.WebAPI.PSI_QueueWS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class IPTProjectController : KAIZENController
    {
        [HttpGet]
        [ActionName("SysInfo")]
        public object SysInfo(string param)
        {
            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            var publishedProjects = prjContext.LoadQuery(prjContext.Projects.Where(x => x.Name == param));
            prjContext.ExecuteQuery();

            if (publishedProjects.Count() > 0)
            {
                var p = publishedProjects.First();

                prjContext.Load(p.Calendar);
                prjContext.ExecuteQuery();

                return new
                {
                    guid = p.Id,
                    calendar = p.Calendar.Name
                };
            }

            return null;
        }

        [HttpGet]
        [ActionName("generateSCurve")]
        public List<SCURVE> generateSCurve(int startProjectShiftID)
        {
            SCurveGeneratorController scurveGeneratorController = new SCurveGeneratorController();
            
            return scurveGeneratorController.generateSCurve(startProjectShiftID, null, false);
        }

        [HttpGet]
        [ActionName("ProjectTaskByShift")]
        public List<GanttObject> ProjectTaskByShift(int projectShiftID)
        {
            List<GanttObject> ganttObjects = new List<GanttObject>();

            var projectTasksByShift = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID);
            var outlineLevels = projectTasksByShift.Select(x => x.OUTLINELEVEL).Distinct().ToList();

            foreach (var outlineLevel in outlineLevels)
            {
                var customId = 1;
                var tasksInLevel = projectTasksByShift.Where(x => x.OUTLINELEVEL == outlineLevel);

                foreach (var task in tasksInLevel)
                {
                    GanttObject parent = null;
                    if (!string.IsNullOrEmpty(task.PARENTTASKUID))
                    {
                        try
                        {
                            parent = ganttObjects.Where(x => x.originalId == task.PARENTTASKUID).FirstOrDefault();
                        }
                        catch { }
                    }

                    ganttObjects.Add(new GanttObject()
                    {
                        originalId = task.TASKUID,
                        id = task.TASKUID,
                        taskOutlineNum = task.OUTLINEPOSITION,
                        expanded = (outlineLevel < outlineLevels[outlineLevels.Count - 1]),
                        title = string.Format("{0} - {1}", task.OUTLINEPOSITION, task.TASKNAME),
                        start = task.BASELINESTART.HasValue ? task.BASELINESTART : task.STARTDATE,
                        end = task.BASELINEFINISH.HasValue ? task.BASELINEFINISH : task.FINISHDATE,
                        percentCompletePreviousShift = task.PERCENTCOMPLETEACTUALBEFORESHIFT.HasValue ? task.PERCENTCOMPLETEACTUALBEFORESHIFT : 0,
                        percentComplete = task.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue ? task.PERCENTCOMPLETEACTUALAFTERSHIFT : (task.PERCENTCOMPLETEACTUALBEFORESHIFT.HasValue ? task.PERCENTCOMPLETEACTUALBEFORESHIFT : 0),
                        percentCompleteBaseline = task.PERCENTCOMPLETEBASELINE.HasValue ? task.PERCENTCOMPLETEBASELINE: 0,
                        summary = task.ISSUMMARY.Value,
                        orderId = customId,
                        parentId = parent == null ? null : parent.id,
                        parentOriginalId = task.PARENTTASKUID,
                        actualStart = task.ACTUALSTART,
                        actualEnd = task.ACTUALFINISH,
                        executor = task.CONTRACTOR,
                        taskOutlineLevel = task.OUTLINELEVEL.Value
                    });

                    customId++;
                }
                
            }

            return ganttObjects.Where(x => x.parentId == null || x.parentId != x.id).OrderBy(x=>x.executor).ThenBy(x=>x.taskOutlineNum).ToList();
        }

        [HttpGet]
        [ActionName("ShiftTarget")]
        public IEnumerable<shiftTarget> ShiftTarget(int param)
        {
            var maxTaskLevel = shiftTargetMaxTaskLevel;

            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(param);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + param);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == param).ToList();

            var groupList = new List<shiftTargetGroup>();
            var levelMaxRows = tasks.Where(x => x.OUTLINELEVEL == maxTaskLevel).ToList();
            while (levelMaxRows.Count == 0)
            {
                maxTaskLevel--;
                levelMaxRows = tasks.Where(x => x.OUTLINELEVEL == maxTaskLevel).ToList();
            }

            List<shiftTarget> result = new List<shiftTarget>();
            foreach (var row in tasks)
            {
                if (!row.ISSUMMARY.Value && tasks.Count(x => x.PARENTTASKUID == row.TASKUID) == 0)
                {
                    shiftTarget st = new shiftTarget()
                    {
                        percentCompletePrevious = row.PERCENTCOMPLETEACTUALBEFORESHIFT,
                        actualStart = row.ACTUALSTART,
                        actualFinish = row.ACTUALFINISH,
                        start = row.BASELINESTART,
                        finish = row.BASELINEFINISH,
                        taskID = new Guid(row.TASKUID),
                        taskOutlineNum = row.OUTLINEPOSITION,
                        taskOutlineLevel = row.OUTLINELEVEL.Value,
                        percentCompletePlan = row.PERCENTCOMPLETEBASELINE,
                        percentCompleteActual = row.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue ? row.PERCENTCOMPLETEACTUALAFTERSHIFT : row.PERCENTCOMPLETEACTUALBEFORESHIFT,
                        Executor = row.CONTRACTOR,
                        titleList = new List<string>()
                    };

                    populateShiftTarget(row, st, maxTaskLevel, tasks);

                    result.Add(st);
                }
            }


            return result.OrderBy(x => x.Executor).ThenBy(x=>x.projectName).ThenBy(x => x.taskOutlineNum);            
        }

        private void populateShiftTarget(PROJECTTASKBYSHIFT task, shiftTarget shiftTarget, int outlineLevelMax, List<PROJECTTASKBYSHIFT> tasks)
        {
            if (task.OUTLINELEVEL >= outlineLevelMax)
            {
                shiftTarget.titleList.Add(task.TASKNAME);

                if (task.OUTLINELEVEL == outlineLevelMax)
                {
                    shiftTarget.groupTaskId = new Guid(task.TASKUID);
                    shiftTarget.projectName = task.TASKNAME;
                }

                var parentTaskRow = tasks.Where(x => x.TASKUID == task.PARENTTASKUID).FirstOrDefault();

                if (parentTaskRow != null)
                    populateShiftTarget(parentTaskRow, shiftTarget, outlineLevelMax, tasks);
            }
        }

        private void populateGroupList(List<shiftTargetGroup> groupList, List<shiftTarget> shiftTargetList, ref List<shiftTargetGroup> groupNewList)
        {
            var shiftCount = shiftTargetList.Select(e => new { e.groupTaskId, e.Executor })
                            .Distinct().ToList();
            foreach (var shift in shiftCount)
            {
                var selectedGroup = groupList.Where(x => x.ID == shift.groupTaskId).FirstOrDefault();
                shiftTargetGroup stg = new shiftTargetGroup()
                {
                    ID = selectedGroup.ID,
                    projectName = selectedGroup.projectName,
                    groupName = selectedGroup.groupName,
                    Executor = shift.Executor,
                    Date = selectedGroup.Date,
                    percentComplete = selectedGroup.percentComplete,
                    outlineNumber = selectedGroup.outlineNumber,
                    shiftName = selectedGroup.shiftName
                };
                groupNewList.Add(stg);
            }

        }
    }
}
