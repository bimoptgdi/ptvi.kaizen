﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PTVI.iPROM.WebApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<LOOKUP>("LOOKUPsOData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class LOOKUPsODataController : ODataController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: odata/LOOKUPsOData
        [EnableQuery]
        public IQueryable<LOOKUP> GetLOOKUPsOData()
        {
            return db.LOOKUPs;
        }

        // GET: odata/LOOKUPsOData(5)
        [EnableQuery]
        public SingleResult<LOOKUP> GetLOOKUP([FromODataUri] int key)
        {
            return SingleResult.Create(db.LOOKUPs.Where(lOOKUP => lOOKUP.OID == key));
        }

        // PUT: odata/LOOKUPsOData(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<LOOKUP> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LOOKUP lOOKUP = await db.LOOKUPs.FindAsync(key);
            if (lOOKUP == null)
            {
                return NotFound();
            }

            patch.Put(lOOKUP);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LOOKUPExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(lOOKUP);
        }

        // POST: odata/LOOKUPsOData
        public async Task<IHttpActionResult> Post(LOOKUP lOOKUP)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LOOKUPs.Add(lOOKUP);
            await db.SaveChangesAsync();

            return Created(lOOKUP);
        }

        // PATCH: odata/LOOKUPsOData(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<LOOKUP> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LOOKUP lOOKUP = await db.LOOKUPs.FindAsync(key);
            if (lOOKUP == null)
            {
                return NotFound();
            }

            patch.Patch(lOOKUP);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LOOKUPExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(lOOKUP);
        }

        // DELETE: odata/LOOKUPsOData(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            LOOKUP lOOKUP = await db.LOOKUPs.FindAsync(key);
            if (lOOKUP == null)
            {
                return NotFound();
            }

            db.LOOKUPs.Remove(lOOKUP);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LOOKUPExists(int key)
        {
            return db.LOOKUPs.Count(e => e.OID == key) > 0;
        }
    }
}
