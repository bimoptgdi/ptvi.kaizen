﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using Microsoft.ProjectServer.Client;
using PTVI.KAIZEN.WebAPI.PSI_ProjectWS;
using PTVI.KAIZEN.WebAPI.PSI_QueueWS;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class SCurveGeneratorController: KAIZENController
    {
        [HttpGet]
        [ActionName("NewList")]
        public HttpResponseMessage NewList(int param)
        {
            var newList = db.SCURVEGENERATIONs
                            .Where(x => x.STATUS == constants.IPT_SCURVEGENERATION_STATUS_NEW || 
                                x.STATUS == constants.IPT_SCURVEGENERATION_STATUS_RESTART ||
                                x.STATUS == constants.IPT_SCURVEGENERATION_STATUS_RETRY)
                            .Select(x=> new {
                                id = x.OID,
                                projectId = x.PROJECTID,
                                estimationShiftID = x.ESTIMATIONSHIFTID
                            });

            return Request.CreateResponse(HttpStatusCode.OK, newList);
        }

        public SCurveGeneration Get(int id) {
            return db.SCURVEGENERATIONs.Where(x => x.OID == id)
                .Select(x => new SCurveGeneration()
                {
                    id = x.OID,
                    projectId = x.PROJECTID,
                    estimationShiftID = x.ESTIMATIONSHIFTID,
                    shiftNoInProgress = x.CURRENTPROCESSEDSHIFT,
                    totalShift = x.TOTALSHIFT,
                    startDate = x.PROCESSSTARTDATE,
                    finishDate = x.PROCESSFINISHDATE,
                    status = x.STATUS
                }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("SetStatus")]
        public HttpResponseMessage SetStatus(int id, string status)
        {
            var scurveGen = db.SCURVEGENERATIONs.Find(id);

            scurveGen.STATUS = status;
            scurveGen.PROCESSSTARTDATE = null;
            scurveGen.PROCESSFINISHDATE = null;

            db.Entry(scurveGen).State = System.Data.Entity.EntityState.Modified;

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, scurveGen);
        }

        [HttpGet]
        [ActionName("Register")]
        public HttpResponseMessage Register(int param)
        {
            var projectShift = db.PROJECTSHIFTs.Find(param);

            if (db.SCURVEGENERATIONs.Count(x => x.PROJECTID == projectShift.PROJECTID &&
                                        x.ESTIMATIONSHIFTID == projectShift.OID) > 0)
            {
                throw new ApplicationException("SCurve for this shift has been generated.");
            }

            SCURVEGENERATION sg = new SCURVEGENERATION()
            {
                PROJECTID = projectShift.PROJECTID,
                ESTIMATIONSHIFTID = projectShift.OID,
                TOTALSHIFT = db.PROJECTSHIFTs.Count(x=>x.PROJECTID == projectShift.PROJECTID),
                CURRENTPROCESSEDSHIFT = 0,
                STATUS = constants.IPT_SCURVEGENERATION_STATUS_NEW,                
                CREATEDBY = GetCurrentNTUserId(),
                CREATEDDATE = DateTime.Now
            };

            db.SCURVEGENERATIONs.Add(sg);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.Created, Get(sg.OID));
        }

        [HttpGet]
        [ActionName("ByProjectID")]
        public IEnumerable<SCurveGeneration> ByProjectID(int param)
        {
            return db.SCURVEGENERATIONs.Where(x => x.PROJECTID == param)
                .OrderBy(x=>x.PROCESSSTARTDATE)
                .Select(x => new SCurveGeneration()
                {
                    id = x.OID,
                    projectId = x.PROJECTID,
                    estimationShiftID = x.ESTIMATIONSHIFTID,
                    shiftNoInProgress = x.CURRENTPROCESSEDSHIFT,
                    totalShift = x.TOTALSHIFT,
                    startDate = x.PROCESSSTARTDATE,
                    finishDate = x.PROCESSFINISHDATE,
                    status = x.STATUS,
                    errorMessage = x.ERRORMESSAGE
                }).AsEnumerable();
        }

        [HttpGet]
        [ActionName("Start")]
        public HttpResponseMessage Start(int param) // scurve generation id
        {
            var scurveGeneration = db.SCURVEGENERATIONs.Find(param);
            var project = db.PROJECTs.Find(scurveGeneration.PROJECTID);
             
            // check if there already scurve generation process of project
            if (db.SCURVEGENERATIONs.Count(x=>x.PROJECTID == scurveGeneration.PROJECTID && x.STATUS == constants.IPT_SCURVEGENERATION_STATUS_INPROGRESS) > 0)
                throw new ApplicationException(
                    string.Format("Project with id {0} - {1} has already has scurve generation process running. There only one scurve generation process is allowed for each project.",
                                        project.OID, project.PROJECTDESCRIPTION));

            var isRestart = scurveGeneration.STATUS == constants.IPT_SCURVEGENERATION_STATUS_RESTART;
            // set in progress state to scurvegeneration data
            scurveGeneration.STATUS = constants.IPT_SCURVEGENERATION_STATUS_INPROGRESS;
            scurveGeneration.ERRORMESSAGE = "";
            scurveGeneration.PROCESSSTARTDATE = DateTime.Now; 
                
            db.Entry(scurveGeneration).State = System.Data.Entity.EntityState.Modified;

            db.SaveChanges();

            // start scurve generation
            try
            {
                var scurves = generateSCurve(scurveGeneration.ESTIMATIONSHIFTID.Value, scurveGeneration, isRestart);

                // send email
                EmailController emailController = new EmailController();
                var sendEmailSuccess = emailController.SendSCurveGenerationFinishedEmail(param);

                // SAVE LOG
                if (scurveGeneration != null)
                {
                    scurveGeneration.PROCESSFINISHDATE = DateTime.Now;

                    if (sendEmailSuccess)
                        scurveGeneration.STATUS = constants.IPT_SCURVEGENERATION_STATUS_COMPLETED;
                    else
                        scurveGeneration.STATUS = constants.IPT_SCURVEGENERATION_STATUS_EMAILFAILED;

                    db.Entry(scurveGeneration).State = System.Data.Entity.EntityState.Modified;

                    db.SaveChanges();
                }


            }
            catch (Exception exc)
            {
                // update scurve generation state
                scurveGeneration.STATUS = constants.IPT_SCURVEGENERATION_STATUS_GENERATIONFAILED;
                scurveGeneration.ERRORMESSAGE = exc.ToString();

                db.Entry(scurveGeneration).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK, scurveGeneration);
        }

       

        [HttpGet]
        [ActionName("generateSCurve")]
        public List<SCURVE> generateSCurve(int startProjectShiftID, SCURVEGENERATION scurveGeneration, bool restart)
        {
            //var ALWAYS_CALCULATE = false;

            VALE_KAIZENEntities db = new VALE_KAIZENEntities();
            var paramProjectShift = db.PROJECTSHIFTs.Find(startProjectShiftID);
            if (paramProjectShift == null)
                throw new ApplicationException("Project shift not found for id " + startProjectShiftID);

            // remove all generated scurve previously if restart is true
            if (restart)
            {
                db.Database.ExecuteSqlCommand("DELETE FROM [SCURVE] WHERE ESTIMATIIONSHIFTID = " + startProjectShiftID);
            }

            // get data from actual progress of previous shift
            //var previousAndExistingTaskProgress = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID <= startProjectShiftID && x.PROJECTID == projectShift.PROJECTID).ToList();
            var scurveData = new List<SCURVE>();
            var projectShiftsInProject = paramProjectShift.PROJECT.PROJECTSHIFTs.OrderBy(x => x.SHIFTID).ToList();
            var scurveReferences = new Dictionary<string, SCURVE>();

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);
            var cfRealisticStartDateUid = new Guid(cfCalcRealisticDurationStartUid);
            var cfRealisticEndDateUid = new Guid(cfCalcRealisticDurationEndDateUid);

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            var prjName = paramProjectShift.PROJECT.PROJECTDESCRIPTION;
            var projectUid = new Guid(paramProjectShift.PROJECT.IPTPROJECTID);

            CheckProjectState(projectUid);

            Guid sessionUid = Guid.NewGuid();

            try
            {
                for (var i = 0; i < projectShiftsInProject.Count; i++)
                {
                    sessionUid = Guid.NewGuid();
                    ProjectWS.CheckOutProject(projectUid, sessionUid, "Prepare S Curve for project " + prjName);

                    var projectShift = projectShiftsInProject[i];

                    // scurve generation process
                    // find in projecttaskbyshift, if exist and has updated (actual pc), store the value to scurve
                    var projectTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShift.OID && x.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue).ToList();

                    if (projectTasks.Count > 0 && projectShift.SHIFTID <= paramProjectShift.SHIFTID)
                    {
                        PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                        var projectName = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow).PROJ_NAME;

                        var shiftTargetDate = projectShift.ENDDATE;

                        // update baseline for current shift
                        if (projectShift.OID == paramProjectShift.OID)
                        {
                            // update projecttaskby shift
                            foreach(var pt in projectTasks)
                            {
                                var psTask = projectDs.Task.Select("TASK_UID='" + pt.TASKUID + "'").FirstOrDefault();
                                var msBsLineTask = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShift.OID && x.TASKUID == pt.TASKUID).FirstOrDefault();
                                if (psTask != null)
                                {
                                    var r = psTask as ProjectDataSet.TaskRow;
                                    if (!r.IsTB_DURNull())
                                        pt.BASELINEDURATION = r.TB_DUR;
                                    if (!r.IsTB_STARTNull())
                                        pt.BASELINESTART = r.TB_START;
                                    if (!r.IsTB_FINISHNull())
                                        pt.BASELINEFINISH = r.TB_FINISH;
                                    if (!r.IsTASK_DURNull())
                                    {
                                        pt.ESTDURATION = r.TASK_DUR;
                                        if (msBsLineTask != null)
                                            msBsLineTask.ESTIMATEDURATION = pt.ESTDURATION;
                                    }

                                    if (!r.IsTASK_START_DATENull())
                                        pt.STARTDATE = r.TASK_START_DATE;                                    
                                    if (!r.IsTASK_FINISH_DATENull())
                                        pt.FINISHDATE = r.TASK_FINISH_DATE;

                                    if (!r.IsTASK_FREE_SLACKNull())
                                    {
                                        pt.FREESLACK = r.TASK_FREE_SLACK;
                                        if (msBsLineTask != null)
                                            msBsLineTask.FREESLACK = pt.FREESLACK;
                                    }

                                    if (!r.IsTASK_IS_SUMMARYNull())
                                        pt.ISSUMMARY = r.TASK_IS_SUMMARY;
                                    if (!r.IsTASK_OUTLINE_LEVELNull())
                                        pt.OUTLINELEVEL = r.TASK_OUTLINE_LEVEL;
                                    if (!r.IsTASK_OUTLINE_NUMNull())
                                        pt.OUTLINEPOSITION = r.TASK_OUTLINE_NUM;
                                    if (!r.IsTASK_PARENT_UIDNull())
                                        pt.PARENTTASKUID = r.TASK_PARENT_UID.ToString();
                                    if (!r.IsTASK_NAMENull())
                                        pt.TASKNAME = r.TASK_NAME;
                                    if (!r.IsTASK_TOTAL_SLACKNull())
                                    {
                                        pt.TOTALSLACK = r.TASK_TOTAL_SLACK;
                                        if (msBsLineTask != null)
                                            msBsLineTask.TOTALSLACK = pt.TOTALSLACK;
                                    }

                                    db.Entry(pt).State = System.Data.Entity.EntityState.Modified;
                                    if (msBsLineTask != null)
                                        db.Entry(msBsLineTask).State = System.Data.Entity.EntityState.Modified;
                                }
                            }

                            // update masterbaselinetaskbyshift
                        }

                        //var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        // reset all leave baseline var
                        var allLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0");
                        foreach (var leave in allLeaves)
                        {
                            var t = leave as ProjectDataSet.TaskRow;

                            // set start and end equal date to make it value 0
                            var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                            if (filterStartDateRows.Length > 0)
                            {
                                filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }

                            var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                            if (filterEndDateRows.Length > 0)
                            {
                                filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                        }

                        //var tasks = projectDs.Task.Select("TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        var isAhead = projectShift.PROGRESSCONDITION == constants.IPT_PROGRESSCONDITION_AHEAD;
                        var includeLeaves = isAhead ? projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'") :
                                                projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");

                        foreach (var localTask in includeLeaves)
                        {
                            //var taskRow = projectDs.Task.Select("TASK_UID = '" + localTask.TASKUID + "'")[0] as PSI_ProjectWS.ProjectDataSet.TaskRow;
                            var taskRow = localTask as ProjectDataSet.TaskRow;

                            SCURVE prevSCurveData = null;
                            if (scurveReferences.ContainsKey(taskRow.TASK_UID.ToString()))
                                prevSCurveData = scurveReferences[taskRow.TASK_UID.ToString()];

                            var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                            if (filterStartDateRows.Length > 0)
                            {
                                filterStartDateRows[0].DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;
                                newStartDateCFRow.DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                            }


                            var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                            if (filterEndDateRows.Length > 0)
                            {
                                filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value; // use to calculate baseline duration
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                                endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);

                            }                            
                        }

                        if (projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                        {
                            // publish project to appy calculation
                            Guid jobUid = Guid.NewGuid();

                            var changedDs = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                            ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs, false);

                            WaitForQueue(q, jobUid);

                            jobUid = Guid.NewGuid();
                            ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Checking of S Curve for project " + prjName);

                            WaitForQueue(q, jobUid);
                        }

                        var ENTITY_TYPE_CUSTOMFIELD = 64;

                        PSI_ProjectWS.ProjectDataSet projectDs3 =
                                ProjectWS.ReadProjectEntities(projectUid, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                        var cfTotalDurationUID = cfTotalDuration;
                        var cfPlanDurationUid = cfCalcPlanDurationUid;
                        var cfRealisticDurationUid = cfCalcRealisticDuration;

                        //var rows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        //var rows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        foreach (var projecttask in projectTasks) // for existing shift -> shift yg sudah lewat
                        {
                            var taskDur = 0.0;
                            var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + projecttask.TASKUID + "' and " +
                            "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                            if (totalDurationRows.Length > 0)
                            {
                                try
                                {
                                    taskDur = (double)totalDurationRows[0].NUM_VALUE;
                                }
                                catch { }
                            }

                            var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + projecttask.TASKUID + "' and " +
                                        "MD_PROP_UID = '" + cfPlanDurationUid + "'");

                            var commonDurFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + projecttask.TASKUID + "' and " +
                                        "MD_PROP_UID = '" + cfRealisticDurationUid + "'");
                            if (shiftWorkDurationRows.Length > 0)
                            {
                                var shiftWorkDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                                var percentComplete = (shiftWorkDurationMinute / taskDur) * 100.00;

                                if (percentComplete > 100.0)
                                    percentComplete = 100.0;


                                var sCurve = new SCURVE()
                                {
                                    ACTUALDURATION = projecttask.ACTUALDURATION,
                                    BASELINEDURATION = projecttask.BASELINEDURATION,
                                    ESTIMATIIONSHIFTID = paramProjectShift.OID,
                                    ISSUMMARY = projecttask.ISSUMMARY,
                                    OUTLINELEVEL = projecttask.OUTLINELEVEL,
                                    OUTLINEPOSITION = projecttask.OUTLINEPOSITION,
                                    PARENTTASKUID = projecttask.PARENTTASKUID,
                                    PERCENTCOMPLETEACTUAL = projecttask.PERCENTCOMPLETEACTUALAFTERSHIFT,
                                    PERCENTCOMPLETEBASELINE = /*projecttask.PERCENTCOMPLETEBASELINE*/ Double.IsNaN(percentComplete) ? -1 : Convert.ToInt32(percentComplete),
                                    PERCENTCOMPLETEOPTIMIS = projecttask.PERCENTCOMPLETEOPTIMIS,
                                    PERCENTCOMPLETEORIGINAL = projecttask.PERCENTCOMPLETEORIGINAL,
                                    PERCENTCOMPLETEREALISTIC = projecttask.PERCENTCOMPLETEREALISTIS,
                                    PROJECTID = projectShift.PROJECTID,
                                    PROJECTNAME = projectShift.PROJECT.PROJECTDESCRIPTION,
                                    REMAININGOPTIMIS = projecttask.REMAININGOPTIMISTIC,
                                    REMAININGORIGINAL = projecttask.REMAININGORIGINAL,
                                    REMAININGREALISTIC = projecttask.REMAININGREALISTIC,
                                    SHIFTNO = projectShift.SHIFTID,
                                    TASKNAME = projecttask.TASKNAME,
                                    TASKUID = projecttask.TASKUID
                                };

                                if (!scurveReferences.ContainsKey(projecttask.TASKUID))
                                    scurveReferences.Add(projecttask.TASKUID, sCurve);
                                else
                                    scurveReferences[projecttask.TASKUID] = sCurve;

                                if (db.SCURVEs.Count(x => x.ESTIMATIIONSHIFTID == startProjectShiftID && x.SHIFTNO == projectShift.SHIFTID && x.TASKUID == sCurve.TASKUID) == 0)
                                {
                                    db.SCURVEs.Add(sCurve);
                                }
                            }
                        }
                    }
                    else
                    {
                        // if current shift already generated then process do not save
                        if (db.SCURVEs.Count(x => x.ESTIMATIIONSHIFTID == startProjectShiftID && x.SHIFTNO == projectShift.SHIFTID) > 0)
                        {
                            continue;
                        }

                        PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                        var projectName = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow).PROJ_NAME;

                        var shiftTargetDate = projectShift.ENDDATE;

                        //var excludeLeavesBaseline = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        var allLeavesBaseline = projectDs.Task.Select("TASK_IS_SUMMARY = 0");
                        foreach (var leave in allLeavesBaseline)
                        {
                            var t = leave as ProjectDataSet.TaskRow;

                            // set start and end equal date to make it value 0
                            var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                            if (filterStartDateRows.Length > 0)
                            {
                                filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }

                            var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                            if (filterEndDateRows.Length > 0)
                            {
                                filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                        }

                        var isAhead = projectShift.PROGRESSCONDITION == constants.IPT_PROGRESSCONDITION_AHEAD;
                        var includeLeavesBaseline = isAhead ? projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'"):
                            projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        foreach (ProjectDataSet.TaskRow taskRow in includeLeavesBaseline)
                        {
                            //var taskRow = projectDs.Task.Select("TASK_UID = '" + localTask.TASKUID + "'")[0] as ProjectDataSet.TaskRow;
                            var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                            "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                            if (filterStartDateRows.Length > 0)
                            {
                                filterStartDateRows[0].DATE_VALUE = taskRow.TB_START;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                                newStartDateCFRow.DATE_VALUE = taskRow.TB_START;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                            }


                            var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                            if (filterEndDateRows.Length > 0)
                            {
                                filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value; // use to calculate baseline duration

                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                                endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);

                            }
                        }

                        //var excludeLeavesOther = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        var excludeAllLeavesOther = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0");
                        foreach (var leave in excludeAllLeavesOther)
                        {
                            var t = leave as ProjectDataSet.TaskRow;
                            var filterOptimisticStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfCalcOptimisticDurationStartDate + "'");
                            if (filterOptimisticStartDateRows.Length > 0)
                            {
                                filterOptimisticStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOptimisticStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOptimisticStartDateCFRow.PROJ_UID = t.PROJ_UID;
                                newOptimisticStartDateCFRow.TASK_UID = t.TASK_UID;
                                newOptimisticStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOptimisticStartDateCFRow.MD_PROP_UID = new Guid(cfCalcOptimisticDurationStartDate);

                                newOptimisticStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOptimisticStartDateCFRow);
                            }

                            var filterOptimisticEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfCalcOptimisticDurationEndDate + "'");

                            if (filterOptimisticEndDateRows.Length > 0)
                            {
                                filterOptimisticEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow optimisticEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                optimisticEndDateCFRow.PROJ_UID = t.PROJ_UID;
                                optimisticEndDateCFRow.TASK_UID = t.TASK_UID;
                                optimisticEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                optimisticEndDateCFRow.MD_PROP_UID = new Guid(cfCalcOptimisticDurationEndDate);

                                optimisticEndDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(optimisticEndDateCFRow);

                            }

                            var filterRealisticStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfCalcRealisticDurationStartUid + "'");
                            if (filterRealisticStartDateRows.Length > 0)
                            {
                                filterRealisticStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newRealisticStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newRealisticStartDateCFRow.PROJ_UID = t.PROJ_UID;
                                newRealisticStartDateCFRow.TASK_UID = t.TASK_UID;
                                newRealisticStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newRealisticStartDateCFRow.MD_PROP_UID = new Guid(cfCalcRealisticDurationStartUid);

                                newRealisticStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newRealisticStartDateCFRow);
                            }

                            var filterRealisticEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfCalcRealisticDurationEndDateUid + "'");

                            if (filterRealisticEndDateRows.Length > 0)
                            {
                                filterRealisticEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newRealisticEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newRealisticEndDateCFRow.PROJ_UID = t.PROJ_UID;
                                newRealisticEndDateCFRow.TASK_UID = t.TASK_UID;
                                newRealisticEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newRealisticEndDateCFRow.MD_PROP_UID = new Guid(cfCalcRealisticDurationEndDateUid);

                                newRealisticEndDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newRealisticEndDateCFRow);

                            }

                            // original
                            var filterOriginalStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfCalcOriginalDurationStartUid + "'");
                            if (filterOriginalStartDateRows.Length > 0)
                            {
                                filterOriginalStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOriginalStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOriginalStartDateCFRow.PROJ_UID = t.PROJ_UID;
                                newOriginalStartDateCFRow.TASK_UID = t.TASK_UID;
                                newOriginalStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOriginalStartDateCFRow.MD_PROP_UID = new Guid(cfCalcOriginalDurationStartUid);

                                newOriginalStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOriginalStartDateCFRow);
                            }

                            var filterOriginalEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfCalcOriginalDurationEndDateUid + "'");

                            if (filterOriginalEndDateRows.Length > 0)
                            {
                                filterOriginalEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOriginalEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOriginalEndDateCFRow.PROJ_UID = t.PROJ_UID;
                                newOriginalEndDateCFRow.TASK_UID = t.TASK_UID;
                                newOriginalEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOriginalEndDateCFRow.MD_PROP_UID = new Guid(cfCalcOriginalDurationEndDateUid);

                                newOriginalEndDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOriginalEndDateCFRow);

                            }
                        }


                        //var tasks = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        //var tasks = projectDs.Task.Select("TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");

                        //var includeLeavesOther = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        
                        var includeLeavesOther = isAhead ? projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'") :
                            projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        foreach (ProjectDataSet.TaskRow taskRow in includeLeavesOther)
                        {
                            //var taskRow = projectDs.Task.Select("TASK_UID = '" + task.TASKUID + "'")[0] as PSI_ProjectWS.ProjectDataSet.TaskRow;

                            SCURVE prevSCurveData = null;
                            if (scurveReferences.ContainsKey(taskRow.TASK_UID.ToString()))
                                prevSCurveData = scurveReferences[taskRow.TASK_UID.ToString()];
                          
                            var startDate = taskRow.TASK_START_DATE;
                            if (prevSCurveData != null)
                            {
                                var nextShift = db.PROJECTSHIFTs.Where(x => x.PROJECTID == prevSCurveData.PROJECTID && x.SHIFTID > prevSCurveData.SHIFTNO).OrderBy(x => x.SHIFTID).FirstOrDefault();

                                //if (nextShift != null && nextShift.STARTDATE.Value >= taskRow.TASK_START_DATE)
                                //    startDate = nextShift.STARTDATE.Value;
                                if (!isAhead)
                                {
                                    if (nextShift != null && nextShift.STARTDATE.Value >= taskRow.TB_START)
                                        startDate = nextShift.STARTDATE.Value;
                                }
                                else
                                {
                                    if (nextShift != null && nextShift.STARTDATE.Value >= taskRow.TASK_START_DATE)
                                        startDate = nextShift.STARTDATE.Value;
                                }
                            }


                            var filterOptimisticStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfCalcOptimisticDurationStartDate + "'");
                            if (filterOptimisticStartDateRows.Length > 0)
                            {
                                filterOptimisticStartDateRows[0].DATE_VALUE = startDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOptimisticStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOptimisticStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newOptimisticStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newOptimisticStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOptimisticStartDateCFRow.MD_PROP_UID = new Guid(cfCalcOptimisticDurationStartDate);

                                newOptimisticStartDateCFRow.DATE_VALUE = startDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOptimisticStartDateCFRow);
                            }

                            var filterOptimisticEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfCalcOptimisticDurationEndDate + "'");

                            var optimisticEndDate = shiftTargetDate.Value;
                            string tUid = taskRow.TASK_UID.ToString();
                            var localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.TASKUID == tUid && x.PROJECTID == paramProjectShift.PROJECTID).OrderBy(x => x.ESTIMATIONSHIFTID).ToList().LastOrDefault();
                            
                            if (localTask == null)
                            {
                                var optimisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                        "MD_PROP_UID = '" + cfOptimisticFinishEach + "'");

                                var optimisticEndDateTmp = optimisticFinishfilteredRows.Length > 0 ? optimisticFinishfilteredRows[0].DATE_VALUE : taskRow.TASK_FINISH_DATE;
                                if (optimisticEndDate > optimisticEndDateTmp)
                                    optimisticEndDate = optimisticEndDateTmp;
                            }
                            else
                            {
                                if (optimisticEndDate > localTask.OPTIMISTICFINISH)
                                    optimisticEndDate = localTask.OPTIMISTICFINISH.Value;
                            }

                            if (filterOptimisticEndDateRows.Length > 0)
                            {
                                filterOptimisticEndDateRows[0].DATE_VALUE = optimisticEndDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOptimisticEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOptimisticEndDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newOptimisticEndDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newOptimisticEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOptimisticEndDateCFRow.MD_PROP_UID = new Guid(cfCalcOptimisticDurationEndDate);

                                newOptimisticEndDateCFRow.DATE_VALUE = optimisticEndDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOptimisticEndDateCFRow);

                            }

                            // realistic 
                            var filterRealisticStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfCalcRealisticDurationStartUid + "'");
                            if (filterRealisticStartDateRows.Length > 0)
                            {
                                filterRealisticStartDateRows[0].DATE_VALUE = startDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newRealisticStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newRealisticStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newRealisticStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newRealisticStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newRealisticStartDateCFRow.MD_PROP_UID = new Guid(cfCalcRealisticDurationStartUid);

                                newRealisticStartDateCFRow.DATE_VALUE = startDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newRealisticStartDateCFRow);
                            }

                            var filterRealisticEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfCalcRealisticDurationEndDateUid + "'");

                            var realisticEndDate = shiftTargetDate.Value;

                            if (localTask == null)
                            {
                                var realisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticFinishEach + "'");

                                var realisticEndDateTmp = realisticFinishfilteredRows.Length > 0 ? realisticFinishfilteredRows[0].DATE_VALUE : taskRow.TASK_FINISH_DATE;
                                if (realisticEndDate > realisticEndDateTmp)
                                    realisticEndDate = realisticEndDateTmp;
                            }
                            else
                            {
                                if (realisticEndDate > localTask.REALISTICFINISH)
                                    realisticEndDate = localTask.REALISTICFINISH.Value;
                            }

                            if (filterRealisticEndDateRows.Length > 0)
                            {
                                filterRealisticEndDateRows[0].DATE_VALUE = realisticEndDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newRealisticEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newRealisticEndDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newRealisticEndDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newRealisticEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newRealisticEndDateCFRow.MD_PROP_UID = new Guid(cfCalcRealisticDurationEndDateUid);

                                newRealisticEndDateCFRow.DATE_VALUE = realisticEndDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newRealisticEndDateCFRow);

                            }

                            // original
                            var filterOriginalStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfCalcOriginalDurationStartUid + "'");
                            if (filterOriginalStartDateRows.Length > 0)
                            {
                                filterOriginalStartDateRows[0].DATE_VALUE = startDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOriginalStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOriginalStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newOriginalStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newOriginalStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOriginalStartDateCFRow.MD_PROP_UID = new Guid(cfCalcOriginalDurationStartUid);

                                newOriginalStartDateCFRow.DATE_VALUE = startDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOriginalStartDateCFRow);
                            }

                            var filterOriginalEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfCalcOriginalDurationEndDateUid + "'");
                            var originalEndDate = shiftTargetDate.Value;

                            if (localTask == null)
                            {
                                var originalFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfOriginalFinishEach + "'");

                                var originalEndDateTmp = originalFinishfilteredRows.Length > 0 ? originalFinishfilteredRows[0].DATE_VALUE : taskRow.TASK_FINISH_DATE;
                                if (originalEndDate > originalEndDateTmp)
                                    originalEndDate = originalEndDateTmp;
                            }
                            else
                            {
                                if (originalEndDate > localTask.ORIGINALFINISH)
                                    originalEndDate = localTask.ORIGINALFINISH.Value;
                            }

                            if (filterOriginalEndDateRows.Length > 0)
                            {
                                filterOriginalEndDateRows[0].DATE_VALUE = originalEndDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newOriginalEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newOriginalEndDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newOriginalEndDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newOriginalEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newOriginalEndDateCFRow.MD_PROP_UID = new Guid(cfCalcOriginalDurationEndDateUid);

                                newOriginalEndDateCFRow.DATE_VALUE = originalEndDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newOriginalEndDateCFRow);

                            }
                        }


                        if (projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                        {
                            // publish project to appy calculation
                            Guid jobUid = Guid.NewGuid();

                            var changedDs = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                            ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs , false);

                            WaitForQueue(q, jobUid);

                            jobUid = Guid.NewGuid();
                            ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Checking of S Curve for project " + prjName);

                            WaitForQueue(q, jobUid);
                        }

                        var ENTITY_TYPE_CUSTOMFIELD = 64;

                        PSI_ProjectWS.ProjectDataSet projectDs3 =
                                ProjectWS.ReadProjectEntities(projectUid, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);


                        var cfTotalDurationUID = cfTotalDuration;
                        var cfPlanDurationUid = cfCalcPlanDurationUid;
                        var cfRealisticDurationSumUid = cfCalcRealisticDurationSum;
                        var cfOptimisticDurationSumUid = cfCalcOptimisticDurationSum;
                        var cfOriginalDurationSumUid = cfCalcOriginalDurationSum;

                        // get calculation result
                        //var rows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        var rows = isAhead ? projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'"):
                            projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        foreach (ProjectDataSet.TaskRow t in rows)
                        {
                            //var t = projectDs.Task.Select("TASK_UID = '" + task.TASKUID + "'")[0] as ProjectDataSet.TaskRow;

                            SCURVE prevSCurveData = null;
                            if (scurveReferences.ContainsKey(t.TASK_UID.ToString()))
                                prevSCurveData = scurveReferences[t.TASK_UID.ToString()];

                            var taskDur = 0.0;
                            var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                            if (totalDurationRows.Length > 0)
                            {
                                try
                                {
                                    taskDur = (double)totalDurationRows[0].NUM_VALUE;
                                }
                                catch { }
                            }

                            var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                        "MD_PROP_UID = '" + cfPlanDurationUid + "'");

                            var optimisticDurFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                        "MD_PROP_UID = '" + cfOptimisticDurationSumUid + "'");

                            var optimisticRemainingFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                            "MD_PROP_UID = '" + cfCalcRemainingOptimistic + "'");

                            var optimisticEachRemainingFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                            "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");

                            var realisticDurFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                    "MD_PROP_UID = '" + cfRealisticDurationSumUid + "'");

                            var realisticRemainingFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                            "MD_PROP_UID = '" + cfRemainingRealistic + "'");

                            var realisticEachRemainingFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");

                            var originalDurFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                            "MD_PROP_UID = '" + cfOriginalDurationSumUid + "'");

                            var originalRemainingFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                        "MD_PROP_UID = '" + cfCalcRemainingOriginal + "'");


                            if (shiftWorkDurationRows.Length > 0 && optimisticDurFilteredRows.Length > 0 && realisticDurFilteredRows.Length > 0 && originalDurFilteredRows.Length > 0)
                            {
                                var shiftWorkDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                                var percentComplete = (shiftWorkDurationMinute / taskDur) * 100.00;

                                var optimisDurationInShift10 = (double)optimisticDurFilteredRows[0].NUM_VALUE;
                                var realistisDurationInShift10 = (double)realisticDurFilteredRows[0].NUM_VALUE;
                                var originalDurationInShift10 = (double)originalDurFilteredRows[0].NUM_VALUE;

                                var remainingOptimistic = (double)optimisticRemainingFilteredRows[0].NUM_VALUE;
                                //if ((double)optimisticEachRemainingFilteredRows[0].NUM_VALUE > remainingOptimistic)
                                //    remainingOptimistic = (double)optimisticEachRemainingFilteredRows[0].NUM_VALUE;

                                var remainingOriginal = (double)originalRemainingFilteredRows[0].NUM_VALUE;
                                //var remainingOriginal = t.TASK_REM_DUR;
                                var remainingRealistic = (double)realisticRemainingFilteredRows[0].NUM_VALUE;
                                //if ((double)realisticEachRemainingFilteredRows[0].NUM_VALUE > remainingRealistic)
                                //    remainingRealistic = (double)realisticEachRemainingFilteredRows[0].NUM_VALUE;

                                if (percentComplete > 100.0)
                                    percentComplete = 100.0;

                                if (prevSCurveData != null)
                                {
                                    double? percentCompleteOriginal = 0;
                                    double? percentCompleteOptimis = 0;
                                    double? percentCompleteRealistic = 0;

                                    if (!t.IsTASK_PCT_COMPNull() && t.TASK_PCT_COMP >= 100)
                                    {
                                        percentCompleteOriginal = 100;
                                        percentCompleteOptimis = 100;
                                        percentCompleteRealistic = 100;
                                    }
                                    else
                                    {
                                        percentCompleteOriginal = prevSCurveData == null ? 0 : (prevSCurveData.PERCENTCOMPLETEACTUAL + (remainingOriginal == 0 ? 0 : ((originalDurationInShift10 / remainingOriginal) * (100.0 - prevSCurveData.PERCENTCOMPLETEACTUAL))));
                                        percentCompleteOptimis = prevSCurveData == null ? 0 : (prevSCurveData.PERCENTCOMPLETEACTUAL + (remainingOptimistic == 0 ? 0 : ((optimisDurationInShift10 / remainingOptimistic) * (100.0 - prevSCurveData.PERCENTCOMPLETEACTUAL))));
                                        percentCompleteRealistic = prevSCurveData == null ? 0 : (prevSCurveData.PERCENTCOMPLETEACTUAL + (remainingRealistic == 0 ? 0 : ((realistisDurationInShift10 / remainingRealistic) * (100.0 - prevSCurveData.PERCENTCOMPLETEACTUAL))));
                                    }

                                    var scurve = new SCURVE()
                                    {
                                        ESTIMATIIONSHIFTID = startProjectShiftID,
                                        ISSUMMARY = t.TASK_IS_SUMMARY,
                                        OUTLINELEVEL = t.TASK_OUTLINE_LEVEL,
                                        OUTLINEPOSITION = t.TASK_OUTLINE_NUM,
                                        PARENTTASKUID = t.TASK_PARENT_UID.ToString(),
                                        PERCENTCOMPLETEACTUAL = null,
                                        PERCENTCOMPLETEBASELINE = Double.IsNaN(percentComplete) ? -1 : Convert.ToInt32(percentComplete),
                                        PERCENTCOMPLETEOPTIMIS = percentCompleteOptimis < 0 ? 0 : (percentCompleteOptimis > 100 || prevSCurveData.REMAININGOPTIMIS == 0 ? 100 : percentCompleteOptimis),
                                        PERCENTCOMPLETEORIGINAL = percentCompleteOriginal < 0 ? 0 : (percentCompleteOriginal > 100 || prevSCurveData.REMAININGORIGINAL == 0 ? 100 : percentCompleteOriginal),
                                        PERCENTCOMPLETEREALISTIC = percentCompleteRealistic < 0 ? 0 : (percentCompleteRealistic > 100 || prevSCurveData.REMAININGREALISTIC == 0 ? 100 : percentCompleteRealistic),
                                        PROJECTID = projectShift.PROJECTID,
                                        PROJECTNAME = projectShift.PROJECT.PROJECTDESCRIPTION,
                                        SHIFTNO = projectShift.SHIFTID,
                                        TASKNAME = t.TASK_NAME,
                                        TASKUID = t.TASK_UID.ToString()
                                    };

                                    scurveData.Add(scurve);    

                                    db.SCURVEs.Add(scurve);
                                }
                            }
                        }
                    }

                    if (scurveGeneration != null)
                    {
                        // do logging
                        if (i == 0)
                        {
                            scurveGeneration.TOTALSHIFT = projectShiftsInProject.Max(x => x.SHIFTID);
                        }

                        scurveGeneration.CURRENTPROCESSEDSHIFT = projectShift.SHIFTID;

                        db.Entry(scurveGeneration).State = System.Data.Entity.EntityState.Modified;

                        db.SaveChanges();
                    }

                    //if (projectShift.SHIFTID == 23)
                    //    break;
                }

            }
            catch (Exception exc)
            {
                Guid jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Checkin for error handling of calculation value f S Curve for project " + prjName);

                WaitForQueue(q, jobUid);

                throw;
            }

            return scurveData;
        }

        static private void WaitForQueue(QueueSystem q, Guid jobId)
        {
            PSI_QueueWS.JobState jobState;
            const int QUEUE_WAIT_TIME = 2; // two seconds
            bool jobDone = false;
            string xmlError = string.Empty;
            int wait = 0;

            // Wait for the project to get through the queue.
            // Get the estimated wait time in seconds.
            wait = q.GetJobWaitTime(jobId);

            // Wait for it.
            System.Threading.Thread.Sleep(wait * 1000);
            // Wait until it is finished.

            do
            {
                // Get the job state.
                jobState = q.GetJobCompletionState(jobId, out xmlError);

                if (jobState == PSI_QueueWS.JobState.Success)
                {
                    jobDone = true;
                }
                else
                {
                    if (jobState == PSI_QueueWS.JobState.Unknown
                    || jobState == PSI_QueueWS.JobState.Failed
                    || jobState == PSI_QueueWS.JobState.FailedNotBlocking
                    || jobState == PSI_QueueWS.JobState.CorrelationBlocked
                    || jobState == PSI_QueueWS.JobState.Canceled)
                    {
                        // If the job failed, error out.
                        throw (new ApplicationException("Queue request " + jobState + " for Job ID " + jobId + ".\r\n" + xmlError));
                    }
                    else
                    {
                        Console.WriteLine("Job State: " + jobState + " for Job ID: " + jobId);
                        System.Threading.Thread.Sleep(QUEUE_WAIT_TIME * 1000);
                    }
                }
            }
            while (!jobDone);
        }

        /*[HttpGet]
        [ActionName("testgenerateSCurve")]
        public List<SCURVE> testgenerateSCurve(int param)
        {
            iPROMEntities db = new iPROMEntities();

            int startProjectShiftID = param;
            var paramProjectShift = db.PROJECTSHIFTs.Find(startProjectShiftID);
            if (paramProjectShift == null)
                throw new ApplicationException("Project shift not found for id " + startProjectShiftID);

            // get data from actual progress of previous shift
            //var previousAndExistingTaskProgress = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID <= startProjectShiftID && x.PROJECTID == projectShift.PROJECTID).ToList();
            var scurveData = new List<SCURVE>();
            var projectShiftsInProject = paramProjectShift.PROJECT.PROJECTSHIFTs.OrderBy(x => x.SHIFTID).ToList();
            var scurveReferences = new Dictionary<string, SCURVE>();

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);
            var cfRealisticStartDateUid = new Guid(cfCalcRealisticDurationStartUid);
            var cfRealisticEndDateUid = new Guid(cfCalcRealisticDurationEndDateUid);

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            var prjName = paramProjectShift.PROJECT.PROJECTDESCRIPTION;
            var projectUid = new Guid(paramProjectShift.PROJECT.IPTPROJECTID);

            Guid sessionUid = Guid.NewGuid();
            try
            {
                for (var i = 0; i < projectShiftsInProject.Count; i++)
                {
                    var projectShift = projectShiftsInProject[i];

                    // scurve generation process
                    // find in projecttaskbyshift, if exist and has updated (actual pc), store the value to scurve
                    var projectTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShift.OID && x.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue).ToList();

                    if (projectTasks.Count > 0 && projectShift.SHIFTID <= paramProjectShift.SHIFTID)
                    {
                        foreach (var projecttask in projectTasks)
                        {
                            var sCurve = new SCURVE()
                            {
                                ACTUALDURATION = projecttask.ACTUALDURATION,
                                BASELINEDURATION = projecttask.BASELINEDURATION,
                                ESTIMATIIONSHIFTID = paramProjectShift.OID,
                                ISSUMMARY = projecttask.ISSUMMARY,
                                OUTLINELEVEL = projecttask.OUTLINELEVEL,
                                OUTLINEPOSITION = projecttask.OUTLINEPOSITION,
                                PARENTTASKUID = projecttask.PARENTTASKUID,
                                PERCENTCOMPLETEACTUAL = projecttask.PERCENTCOMPLETEACTUALAFTERSHIFT,
                                PERCENTCOMPLETEBASELINE = projecttask.PERCENTCOMPLETEBASELINE,
                                PERCENTCOMPLETEOPTIMIS = projecttask.PERCENTCOMPLETEOPTIMIS,
                                PERCENTCOMPLETEORIGINAL = projecttask.PERCENTCOMPLETEORIGINAL,
                                PERCENTCOMPLETEREALISTIC = projecttask.PERCENTCOMPLETEREALISTIS,
                                PROJECTID = projectShift.PROJECTID,
                                PROJECTNAME = projectShift.PROJECT.PROJECTDESCRIPTION,
                                REMAININGOPTIMIS = projecttask.REMAININGOPTIMISTIC,
                                REMAININGORIGINAL = projecttask.REMAININGORIGINAL,
                                REMAININGREALISTIC = projecttask.REMAININGREALISTIC,
                                SHIFTNO = projectShift.SHIFTID,
                                TASKNAME = projecttask.TASKNAME,
                                TASKUID = projecttask.TASKUID
                            };

                            if (!scurveReferences.ContainsKey(projecttask.TASKUID))
                                scurveReferences.Add(projecttask.TASKUID, sCurve);
                            else
                                scurveReferences[projecttask.TASKUID] = sCurve;

                            scurveData.Add(sCurve);
                        }
                    }
                    else
                    {
                        sessionUid = Guid.NewGuid();
                        ProjectWS.CheckOutProject(projectUid, sessionUid, "Prepare S Curve for project " + prjName);

                        PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                        var projectName = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow).PROJ_NAME;

                        var shiftTargetDate = projectShift.ENDDATE;

                        //var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'");
                        foreach (var leave in excludeLeaves)
                        {
                            var t = leave as ProjectDataSet.TaskRow;

                            // set start and end equal date to make it value 0
                            var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                            if (filterStartDateRows.Length > 0)
                            {
                                filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }

                            var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                            if (filterEndDateRows.Length > 0)
                            {
                                filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                        }


                        //var tasks = projectDs.Task.Select("TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        var tasks = projectDs.Task.Select("TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");

                        foreach (var row in tasks)
                        {
                            var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;

                            //var prevSCurvesForCurrentTask = scurveData.Where(x => x.TASKUID == taskRow.TASK_UID.ToString() && x.SHIFTNO < projectShift.SHIFTID).OrderByDescending(x => x.SHIFTNO).ToList();

                            SCURVE prevSCurveData = null;
                            if (scurveReferences.ContainsKey(taskRow.TASK_UID.ToString()))
                                prevSCurveData = scurveReferences[taskRow.TASK_UID.ToString()];

                            if (taskRow.TASK_IS_SUMMARY == false)
                            {
                                var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                            "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                                if (filterStartDateRows.Length > 0)
                                {
                                    //filterStartDateRows[0].DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START; // use this to calculate baseline duration
                                    filterStartDateRows[0].DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                                    //projectDs.TaskCustomFields.BeginLoadData();
                                    //projectDs.TaskCustomFields.LoadDataRow(filterStartDateRows[0].ItemArray, false);
                                    //projectDs.TaskCustomFields.EndLoadData();
                                }
                                else
                                {
                                    PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                    newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                    newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                    newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                    newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                                    //newStartDateCFRow.DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START;
                                    newStartDateCFRow.DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                                    projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                                    //projectDs.TaskCustomFields.LoadDataRow(newStartDateCFRow.ItemArray, false);
                                }


                                var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                            "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                                if (filterEndDateRows.Length > 0)
                                {
                                    filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value; // use to calculate baseline duration

                                }
                                else
                                {
                                    PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                    endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                    endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                    endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                    endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                                    endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                    projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);

                                }
                            }

                            var startDate = taskRow.TASK_START_DATE;
                            if (prevSCurveData != null)
                            {
                                var nextShift = db.PROJECTSHIFTs.Where(x => x.PROJECTID == prevSCurveData.PROJECTID && x.SHIFTID > prevSCurveData.SHIFTNO).OrderBy(x => x.SHIFTID).FirstOrDefault();

                                if (nextShift != null)
                                    startDate = nextShift.STARTDATE.Value;
                            }
                            var filterCommonStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticStartDateUid + "'");
                            if (filterCommonStartDateRows.Length > 0)
                            {
                                filterCommonStartDateRows[0].DATE_VALUE = startDate;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newCommonStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                newCommonStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                newCommonStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                                newCommonStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                newCommonStartDateCFRow.MD_PROP_UID = cfRealisticStartDateUid;

                                newCommonStartDateCFRow.DATE_VALUE = startDate;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newCommonStartDateCFRow);
                            }

                            var filterCommonEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                        "MD_PROP_UID = '" + cfRealisticEndDateUid + "'");

                            if (filterCommonEndDateRows.Length > 0)
                            {
                                filterCommonEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                            }
                            else
                            {
                                PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow commonEndDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                                commonEndDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                                commonEndDateCFRow.TASK_UID = taskRow.TASK_UID;
                                commonEndDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                                commonEndDateCFRow.MD_PROP_UID = cfRealisticEndDateUid;

                                commonEndDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                                projectDs.TaskCustomFields.AddTaskCustomFieldsRow(commonEndDateCFRow);

                            }
                        }


                        if (projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                        {
                            // publish project to appy calculation
                            Guid jobUid = Guid.NewGuid();

                            var changedDs = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                            ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs, false);

                            WaitForQueue(q, jobUid);

                            Guid jobUid2 = Guid.NewGuid();
                            ProjectWS.QueueCheckInProject(jobUid2, projectUid, false, sessionUid, "Checking of S Curve for project " + prjName);

                            WaitForQueue(q, jobUid2);
                        }
                        //else
                        //{

                        //    Guid jobUid = Guid.NewGuid();
                        //    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Publish (no changes) of S Curve for project " + prjName + " Shift " + projectShift.SHIFTID);

                        //    PSI_QueueWS.QueueSystem q = new QueueSystem();
                        //    q.Credentials = credential;

                        //    WaitForQueue(q, jobUid);
                        //}

                        //PSI_ProjectWS.Project ProjectWS2 = new PSI_ProjectWS.Project();
                        //ProjectWS2.Credentials = pwaCredential;

                        //Guid sessionUid2 = Guid.NewGuid();
                        //ProjectWS2.CheckOutProject(projectUid, sessionUid2, "Do calculation value of S Curve for project " + prjName + " Shift " + projectShift.SHIFTID);
                        var ENTITY_TYPE_CUSTOMFIELD = 64;
                        //PSI_ProjectWS.ProjectDataSet projectDs2 =
                        //        ProjectWS.ReadProjectEntities(projectUid, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.PublishedStore);

                        PSI_ProjectWS.ProjectDataSet projectDs3 =
                                ProjectWS.ReadProjectEntities(projectUid, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);


                        var cfTotalDurationUID = cfTotalDuration;
                        var cfPlanDurationUid = cfCalcPlanDurationUid;
                        var cfRealisticDurationUid = cfCalcRealisticDuration;

                        // update percentCompleteBaseline for all task and its parents
                        //var rows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        var rows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                        foreach (var task in rows)
                        {

                            var t = task as ProjectDataSet.TaskRow;
                            SCURVE prevSCurveData = null;
                            if (scurveReferences.ContainsKey(t.TASK_UID.ToString()))
                                prevSCurveData = scurveReferences[t.TASK_UID.ToString()];


                            //var taskDur = (double)(t.IsTASK_DURNull() ? t.TB_DUR : t.TASK_DUR);
                            var taskDur = 0.0;
                            var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                            if (totalDurationRows.Length > 0)
                            {
                                try
                                {
                                    taskDur = (double)totalDurationRows[0].NUM_VALUE;
                                }
                                catch { }
                            }

                            var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                        "MD_PROP_UID = '" + cfPlanDurationUid + "'");

                            var commonDurFilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                        "MD_PROP_UID = '" + cfRealisticDurationUid + "'");
                            if (shiftWorkDurationRows.Length > 0 && commonDurFilteredRows.Length > 0)
                            {
                                var shiftWorkDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                                var percentComplete = (shiftWorkDurationMinute / taskDur) * 100.00;

                                var durationInShift10 = (double)commonDurFilteredRows[0].NUM_VALUE * 10.0;
                                //var durationInShift10 = 0.0;
                                if (percentComplete > 100.0)
                                    percentComplete = 100.0;

                                if (prevSCurveData != null)
                                {
                                    //var baselineDuration = prevSCurveData.REMAININGORIGINAL;
                                    //var remainingOriginal = baselineDuration == 0 ? 0 : baselineDuration - durationInShift10;
                                    //var percentCompleteOriginal = remainingOriginal == 0 ? 100.0 : (prevSCurveData.PERCENTCOMPLETEORIGINAL + ((100.0 - prevSCurveData.PERCENTCOMPLETEORIGINAL) * (durationInShift10 / baselineDuration)));
                                    var percentCompleteOriginal = prevSCurveData == null ? 0 : (prevSCurveData.PERCENTCOMPLETEACTUAL + ((durationInShift10 / prevSCurveData.REMAININGORIGINAL) * (100.0 - prevSCurveData.PERCENTCOMPLETEACTUAL)));
                                    //var remainingOptimis = ((100.0 - prevSCurveData.PERCENTCOMPLETEOPTIMIS) / 100.0) * prevSCurveData.REMAININGOPTIMIS;
                                    //var percentCompleteOptimis = remainingOptimis == 0 ? 100.0 : (prevSCurveData.PERCENTCOMPLETEOPTIMIS + ((100.0 - prevSCurveData.PERCENTCOMPLETEOPTIMIS) * (durationInShift10 / remainingOptimis)));
                                    var percentCompleteOptimis = prevSCurveData == null ? 0 : (prevSCurveData.PERCENTCOMPLETEACTUAL + ((durationInShift10 / prevSCurveData.REMAININGOPTIMIS) * (100.0 - prevSCurveData.PERCENTCOMPLETEACTUAL)));
                                    //var remainingRealistic = ((100.0 - prevSCurveData.PERCENTCOMPLETEREALISTIC) / 100.0) * prevSCurveData.REMAININGREALISTIC;
                                    //var percentCompleteRealistic = remainingRealistic == 0 ? 100.0 : (prevSCurveData.REMAININGREALISTIC + ((100.0 - prevSCurveData.PERCENTCOMPLETEREALISTIC) * (durationInShift10 / remainingRealistic)));
                                    var percentCompleteRealistic = prevSCurveData == null ? 0 : (prevSCurveData.PERCENTCOMPLETEACTUAL + ((durationInShift10 / prevSCurveData.REMAININGREALISTIC) * (100.0 - prevSCurveData.PERCENTCOMPLETEACTUAL)));

                                    var sCurve = new SCURVE()
                                    {
                                        //ACTUALDURATION = t.TASK_ACT_DUR,
                                        //BASELINEDURATION = Convert.ToInt32(baselineDuration),
                                        ESTIMATIIONSHIFTID = startProjectShiftID,
                                        ISSUMMARY = t.TASK_IS_SUMMARY,
                                        OUTLINELEVEL = t.TASK_OUTLINE_LEVEL,
                                        OUTLINEPOSITION = t.TASK_OUTLINE_NUM,
                                        PARENTTASKUID = t.TASK_PARENT_UID.ToString(),
                                        PERCENTCOMPLETEACTUAL = null,
                                        //PERCENTCOMPLETEACTUAL = t.IsTASK_PCT_COMPNull() ? null : new int?(t.TASK_PCT_COMP),
                                        PERCENTCOMPLETEBASELINE = Double.IsNaN(percentComplete) ? -1 : Convert.ToInt32(percentComplete),
                                        PERCENTCOMPLETEOPTIMIS = percentCompleteOptimis < 0 ? 0 : (percentCompleteOptimis > 100 || prevSCurveData.REMAININGOPTIMIS == 0 ? 100 : percentCompleteOptimis),
                                        PERCENTCOMPLETEORIGINAL = percentCompleteOriginal < 0 ? 0 : (percentCompleteOriginal > 100 || prevSCurveData.REMAININGORIGINAL == 0 ? 100 : percentCompleteOriginal),
                                        PERCENTCOMPLETEREALISTIC = percentCompleteRealistic < 0 ? 0 : (percentCompleteRealistic > 100 || prevSCurveData.REMAININGREALISTIC == 0 ? 100 : percentCompleteRealistic),
                                        PROJECTID = projectShift.PROJECTID,
                                        PROJECTNAME = projectShift.PROJECT.PROJECTDESCRIPTION,
                                        //REMAININGOPTIMIS = remainingOptimis < 0 ? 0 : remainingOptimis,
                                        //REMAININGORIGINAL = remainingOriginal < 0 ? 0 : remainingOriginal,
                                        //REMAININGREALISTIC = remainingRealistic < 0 ? 0 : remainingRealistic,
                                        SHIFTNO = projectShift.SHIFTID,
                                        TASKNAME = t.TASK_NAME,
                                        TASKUID = t.TASK_UID.ToString()
                                    };

                                    //if (!scurveReferences.ContainsKey(t.TASK_UID.ToString()))
                                    //    scurveReferences.Add(t.TASK_UID.ToString(), sCurve);
                                    //else
                                    //    scurveReferences[t.TASK_UID.ToString()] = sCurve;


                                    scurveData.Add(sCurve);
                                }
                            }
                        }

                        //Guid jobUid2 = Guid.NewGuid();
                        //ProjectWS.QueueCheckInProject(jobUid2, projectUid, false, sessionUid2, "Complete calculation value of S Curve for project " + prjName + " Shift " + projectShift.SHIFTID);

                        //PSI_QueueWS.QueueSystem q2 = new QueueSystem();
                        //q2.Credentials = credential;

                        //WaitForQueue(q2, jobUid2);
                    }
                }
            }
            catch (Exception exc)
            {
                Guid jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Checkin for error handling of calculation value f S Curve for project " + prjName);

                WaitForQueue(q, jobUid);

                throw exc;
            }

            // save to database
            //if (scurveData.Count > 0)
            //{
            //    foreach (var scurve in scurveData)
            //        db.SCURVEs.Add(scurve);

            //    db.SaveChanges();
            //}

            return scurveData;
        }*/

    }
}