﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MasterEngineeringServiceSupportController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/MasterEngineeringServiceSupport
        public IQueryable<masterEngineeringServiceSupportObject> GetMasterEngineeringServiceSupport()
        {
            return db.MASTERENGINEERINGSERVICESUPPORTs.Select(s => new masterEngineeringServiceSupportObject
            {
                id = s.OID,
                year = s.YEAR,
                month = s.MONTH,
                docType = s.DOCTYPE,
                monthFactor = s.MONTHFACTOR,
                minTarget = s.MINTARGET,
                target = s.TARGET,
                maxTarget = s.MAXTARGET,
            });
        }

        // GET: api/MasterEngineeringServiceSupport
        [ResponseType(typeof(masterEngineeringServiceSupportObject))]
        public IHttpActionResult GetMASTERENGINEERINGSERVICESUPPORT(int id)
        {
            MASTERENGINEERINGSERVICESUPPORT mASTERENGINEERINGSERVICESUPPORT = db.MASTERENGINEERINGSERVICESUPPORTs.Find(id);
            if (mASTERENGINEERINGSERVICESUPPORT == null)
            {
                return NotFound();
            }

            return Ok(mASTERENGINEERINGSERVICESUPPORT);
        }

        // PUT: api/MasterEngineeringServiceSupport/5
        [ResponseType(typeof(masterEngineeringServiceSupportObject))]
        public IHttpActionResult PutMASTERENGINEERINGSERVICESUPPORT(int id, masterEngineeringServiceSupportObject mASTERENGINEERINGSERVICESUPPORT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERENGINEERINGSERVICESUPPORT.id)
            {
                return BadRequest();
            }

            db.Entry(mASTERENGINEERINGSERVICESUPPORT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERENGINEERINGSERVICESUPPORTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MasterEngineeringServiceSupport
        [ResponseType(typeof(masterEngineeringServiceSupportObject))]
        public IHttpActionResult PostMasterEngineeringServiceSupport(MASTERENGINEERINGSERVICESUPPORT MASTERENGINEERINGSERVICESUPPORT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MASTERENGINEERINGSERVICESUPPORTs.Add(MASTERENGINEERINGSERVICESUPPORT);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { OID = MASTERENGINEERINGSERVICESUPPORT.OID }, MASTERENGINEERINGSERVICESUPPORT);
        }


        // DELETE: api/MasterEngineeringServiceSupport/5
        [ResponseType(typeof(masterEngineeringServiceSupportObject))]
        public IHttpActionResult DeleteMASTERENGINEERINGSERVICESUPPORT(int id)
        {
            MASTERENGINEERINGSERVICESUPPORT mASTERENGINEERINGSERVICESUPPORT = db.MASTERENGINEERINGSERVICESUPPORTs.Find(id);
            if (mASTERENGINEERINGSERVICESUPPORT == null)
            {
                return NotFound();
            }

            db.MASTERENGINEERINGSERVICESUPPORTs.Remove(mASTERENGINEERINGSERVICESUPPORT);
            db.SaveChanges();

            return Ok(mASTERENGINEERINGSERVICESUPPORT);
        }

        [HttpPut]
        [ActionName("updateMasterEngineeringServiceSupport")]
        public IHttpActionResult updateMasterEngineeringServiceSupport(int param, masterEngineeringServiceSupportObject mASTERENGINEERINGSERVICESUPPORTParam)
        {
          if (!ModelState.IsValid)
           {
                return BadRequest();
            }

            if (param != mASTERENGINEERINGSERVICESUPPORTParam.id)
            {
               return BadRequest();
            }

            try
           {
                MASTERENGINEERINGSERVICESUPPORT update = db.MASTERENGINEERINGSERVICESUPPORTs.Find(param);
                update.OID = mASTERENGINEERINGSERVICESUPPORTParam.id;
               update.YEAR = mASTERENGINEERINGSERVICESUPPORTParam.year;
               update.MONTH = mASTERENGINEERINGSERVICESUPPORTParam.month;
               update.DOCTYPE = mASTERENGINEERINGSERVICESUPPORTParam.docType;
              update.MONTHFACTOR = mASTERENGINEERINGSERVICESUPPORTParam.monthFactor;
                update.MINTARGET = mASTERENGINEERINGSERVICESUPPORTParam.minTarget;
                update.TARGET = mASTERENGINEERINGSERVICESUPPORTParam.target;
                update.MAXTARGET = mASTERENGINEERINGSERVICESUPPORTParam.maxTarget;
                update.UPDATEDDATE = DateTime.Now;
              update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

           }
           catch (Exception ex)
           {
               WriteLog(ex.ToString());

               throw;
           }
           return Ok(mASTERENGINEERINGSERVICESUPPORTParam);
        }


        [HttpGet]
        [ActionName("getMaxMinYearly")]
        public object getMaxMinYearly(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERENGINEERINGSERVICESUPPORTs.Min(w => w.YEAR);
            var max = db.MASTERENGINEERINGSERVICESUPPORTs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }

        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterEngineeringServiceSupportObject> getByYear(int param)
        {
            return db.MASTERENGINEERINGSERVICESUPPORTs.Where(w => w.YEAR == param).Select(s => new masterEngineeringServiceSupportObject
            {
                id = s.OID,
                year = s.YEAR,
                month = s.MONTH,
                docType = s.DOCTYPE,
                monthFactor = s.MONTHFACTOR,
                minTarget = s.MINTARGET,
                target = s.TARGET,
                maxTarget = s.MAXTARGET,
            });
        }

        //[HttpGet]
        //[ActionName("getAllEmployeeMasterEngineeringServiceSupport")]
        //public object getAllEmployeeMasterEngineeringServiceSupport(string param)
        //{
        //    return db.MASTERENGINEERINGSERVICESUPPORTs.Select(s => new
        //    {
        //        badge = s.EMPLOYEEID,
        //        name = s.EMPLOYEENAME
        //    }).Distinct();
        //}

        [HttpPost]
        [ActionName("addMasterEngineeringServiceSupport")]
        public IHttpActionResult addMasterEngineeringServiceSupport(string param, masterEngineeringServiceSupportObject masterEngineeringServiceSupportParam)
        {
            MASTERENGINEERINGSERVICESUPPORT add = new MASTERENGINEERINGSERVICESUPPORT();
            add.OID = masterEngineeringServiceSupportParam.id;
            add.YEAR = masterEngineeringServiceSupportParam.year;
            add.MONTH = masterEngineeringServiceSupportParam.month;
            add.DOCTYPE = masterEngineeringServiceSupportParam.docType;
            add.MONTHFACTOR = masterEngineeringServiceSupportParam.monthFactor;
            add.MINTARGET = masterEngineeringServiceSupportParam.minTarget;
            add.TARGET = masterEngineeringServiceSupportParam.target;
            add.MAXTARGET = masterEngineeringServiceSupportParam.maxTarget;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERENGINEERINGSERVICESUPPORTs.Add(add);
            db.SaveChanges();

            masterEngineeringServiceSupportParam.id = add.OID;
            return Ok(masterEngineeringServiceSupportParam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MASTERENGINEERINGSERVICESUPPORTExists(int id)
        {
            return db.MASTERENGINEERINGSERVICESUPPORTs.Count(e => e.OID == id) > 0;
        }
    }
}