﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using SourceCode.Workflow.Client;
using System.Data.Entity.Validation;
using System.IO;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MOMEWRsController : KAIZENController
    {
        // GET: api/MOMEWRs
        public IQueryable<MOMEWR> GetMOMEWRs()
        {
            return db.MOMEWRs;
        }

        [HttpGet]
        [ActionName("getMOMEWRList")]
        public IQueryable<object> getMOMEWRList(int param)
        {
            
            var m = db.MOMEWRs.Where(w => (w.PROJECTs.Count() == 0 && w.MOMEWRNO != null) ||(w.PROJECTs.Where(w1 => w1.OID == param).Count()>0)).Select(s => new {
                id = s.OID,
                ewrTitle = s.MOMEWRNO + " - "+ s.PROJECTTITLE
            });
            return m;
        }
        // GET: api/MOMEWRs/5
        [ResponseType(typeof(MOMEWR))]
        public IHttpActionResult GetMOMEWR(int id)
        {
            MOMEWR mOMEWR = db.MOMEWRs.Find(id);
            if (mOMEWR == null)
            {
                return NotFound();
            }

            return Ok(mOMEWR);
        }

        // PUT: api/MOMEWRs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMOMEWR(int id, MOMEWR mOMEWR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mOMEWR.OID)
            {
                return BadRequest();
            }

            db.Entry(mOMEWR).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MOMEWRExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MOMEWRs
        [ResponseType(typeof(MOMEWR))]
        public IHttpActionResult PostMOMEWR(MOMEWR mOMEWR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MOMEWRs.Add(mOMEWR);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mOMEWR.OID }, mOMEWR);
        }

        // DELETE: api/MOMEWRs/5
        [ResponseType(typeof(MOMEWR))]
        public IHttpActionResult DeleteMOMEWR(int id)
        {
            MOMEWR mOMEWR = db.MOMEWRs.Find(id);
            if (mOMEWR == null)
            {
                return NotFound();
            }

            db.MOMEWRs.Remove(mOMEWR);
            db.SaveChanges();

            return Ok(mOMEWR);
        }

        [HttpGet]
        [ActionName("FindMomEwrByRequest")]
        public IHttpActionResult FindMomEwrByRequest(int param)
        {
            return Ok(db.MOMEWRs.FirstOrDefault(d => d.EWRREQUESTID == param));
        }
        [HttpPost]
        [ActionName("SubmitMOMRequest")]
        public IHttpActionResult SubmitMOMRequest(int param, [FromBody]EWRMomDTO reqData)
        {
            List<String> emailAdresses = new List<string>();
            if (reqData != null)
            {
                try
                {
                    MOMEWR momEWR = new MOMEWR();
                    momEWR.ACTIONTIMELINE = reqData.ACTIONTIMELINE;
                    momEWR.EWRNO = reqData.EWRNO;
                    momEWR.BACKGROUND = reqData.BACKGROUND;
                    momEWR.DATAREQUIRED = reqData.DATAREQUIRED;
                    //momEWR.DISTRIBUTION = reqData.PARTICIPANTS;
                    momEWR.EWRREQUESTID = reqData.EWRREQUESTID;
                    momEWR.MOMDATE = reqData.MOMDATE;
                    momEWR.MOMTIME = reqData.MOMTIME;
                    momEWR.PROJECTDELIVERABLE = reqData.PROJECTDELIVERABLE;
                    momEWR.PROJECTMANAGER = reqData.PROJECTMANAGER;
                    momEWR.PROJECTMANAGERBN = reqData.PROJECTMANAGERBN;
                    momEWR.PROJECTMANAGEREMAIL = reqData.PROJECTMANAGEREMAIL;
                    momEWR.PROJECTTITLE = reqData.PROJECTTITLE;
                    momEWR.RECORDEDBY = reqData.RECORDEDBY;
                    momEWR.RECORDEDBYBN = reqData.RECORDEDBYBN;
                    momEWR.RECORDEDBYEMAIL = reqData.RECORDEDBYEMAIL;
                    momEWR.SCOPEOFWORK = reqData.SCOPEOFWORK;
                    momEWR.SPONSORCP = reqData.SPONSORCP;
                    momEWR.VENUE = reqData.VENUE;
                    momEWR.CREATEDBY = GetCurrentNTUserId();
                    momEWR.CREATEDDATE = DateTime.Now;

                    momEWR.MOMEWRNO = GenerateMoMEWRNumber();

                    db.MOMEWRs.Add(momEWR);

                    if (reqData.PROJECTTYPE != null)
                    {
                        foreach (MOMDetailDTO dto in reqData.PROJECTTYPE)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = GetCurrentNTUserId();
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.MOMEWR = momEWR;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (!String.IsNullOrEmpty(reqData.PARTICIPANTS))
                    {
                        foreach (string participant in reqData.PARTICIPANTS.Split(','))
                        {
                            USERASSIGNMENT userAssign = new USERASSIGNMENT();
                            userAssign.EWRID = reqData.EWRREQUESTID;

                            userAssign.EMPLOYEEID = participant.Split('-')[0];
                            userAssign.EMPLOYEENAME = participant.Split('-')[1];
                            userAssign.EMPLOYEEEMAIL = participant.Split('-')[2];
                            userAssign.ASSIGNMENTTYPE = constants.EWR_PARTICIPANT;

                            userAssign.CREATEDBY = GetCurrentNTUserId();
                            userAssign.CREATEDDATE = DateTime.Now;

                            db.USERASSIGNMENTs.Add(userAssign);

                            // collect participant email
                            if (!String.IsNullOrEmpty(userAssign.EMPLOYEEEMAIL) && !emailAdresses.Contains(userAssign.EMPLOYEEEMAIL))
                            {
                                emailAdresses.Add(userAssign.EMPLOYEEEMAIL);
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(reqData.DISTRIBUTIONS))
                    {
                        foreach (string distribution in reqData.DISTRIBUTIONS.Split(','))
                        {
                            USERASSIGNMENT userAssign = new USERASSIGNMENT();
                            userAssign.EWRID = reqData.EWRREQUESTID;

                            userAssign.EMPLOYEEID = distribution.Split('-')[0];
                            userAssign.EMPLOYEENAME = distribution.Split('-')[1];
                            userAssign.EMPLOYEEEMAIL = distribution.Split('-')[2];
                            userAssign.ASSIGNMENTTYPE = constants.EWR_DISTRIBUTION;

                            userAssign.CREATEDBY = GetCurrentNTUserId();
                            userAssign.CREATEDDATE = DateTime.Now;

                            db.USERASSIGNMENTs.Add(userAssign);

                            // collect distribution email
                            if (!String.IsNullOrEmpty(userAssign.EMPLOYEEEMAIL) && !emailAdresses.Contains(userAssign.EMPLOYEEEMAIL))
                            {
                                emailAdresses.Add(userAssign.EMPLOYEEEMAIL);
                            }
                        }
                    }

                    db.SaveChanges();

                    // update momewrnumber in general parameter
                    IncrementMoMEWRNumber();

                    EWRREQUEST ewr = db.EWRREQUESTs.Find(reqData.EWRREQUESTID);
                    if (ewr != null)
                    {
                        //ewr.STATUS = GetNextApprovalStatus(ewr.STATUS);

                        ewr.OPERATIONALCP = reqData.OPERATIONALCPNAME;
                        ewr.OPERATIONALCPEMAIL = reqData.OPERATIONALCPEMAIL;
                        ewr.OPERATIONALCPBN = reqData.OPERATIONALCPBN;
                        
                        ewr.OPERATIONALCP2 = reqData.OPERATIONALCP2NAME;
                        ewr.OPERATIONALCP2EMAIL = reqData.OPERATIONALCP2EMAIL;
                        ewr.OPERATIONALCP2BN = reqData.OPERATIONALCP2BN;

                        db.Entry(ewr).State = EntityState.Modified;

                        db.SaveChanges();

                        // collect PM email
                        if (!String.IsNullOrEmpty(reqData.PROJECTMANAGEREMAIL) && !emailAdresses.Contains(reqData.PROJECTMANAGEREMAIL))
                        {
                            emailAdresses.Add(reqData.PROJECTMANAGEREMAIL);
                        }
                        // collect Record by email
                        if (!String.IsNullOrEmpty(reqData.RECORDEDBYEMAIL) && !emailAdresses.Contains(reqData.RECORDEDBYEMAIL))
                        {
                            emailAdresses.Add(reqData.RECORDEDBYEMAIL);
                        }
                        // collect Project Sponsor email
                        if (!String.IsNullOrEmpty(ewr.SPONSOREMAIL) && !emailAdresses.Contains(ewr.SPONSOREMAIL))
                        {
                            emailAdresses.Add(ewr.SPONSOREMAIL);
                        }
                        // collect Operational Contact Person email
                        if (!String.IsNullOrEmpty(ewr.OPERATIONALCPEMAIL) && !emailAdresses.Contains(ewr.OPERATIONALCPEMAIL))
                        {
                            emailAdresses.Add(ewr.OPERATIONALCPEMAIL);
                        }
                        // collect Operational Contact Person 2 email
                        if (!String.IsNullOrEmpty(ewr.OPERATIONALCP2EMAIL) && !emailAdresses.Contains(ewr.OPERATIONALCP2EMAIL))
                        {
                            emailAdresses.Add(ewr.OPERATIONALCP2EMAIL);
                        }
                        // collect Requestor email
                        if (!String.IsNullOrEmpty(ewr.REQUESTOREMAIL) && !emailAdresses.Contains(ewr.REQUESTOREMAIL))
                        {
                            emailAdresses.Add(ewr.REQUESTOREMAIL);
                        }
                        // call K2
                        K2ManagementController k2Management = new K2ManagementController();
                        k2Management.Connect();

                        K2ConnectController k2Connect = new K2ConnectController();

                        try
                        {
                            string nextApprover = getNextK2Approver("");

                            List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                            EWRRequestDTO ewrDTO = new EWRRequestDTO();
                            ewrDTO.WF_ID = ewr.WF_ID; //set wf id nya
                            k2Object.Add(ewrDTO);

                            k2Connect.populateFlightWorkListItems(k2Object);

                            string sn = ewrDTO.K2WorklistItems.
                                        Where(d => d.procId == ewr.WF_ID &&
                                        d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                        Select(d => d.serialNo).FirstOrDefault();
                            
                            k2Connect.ExecuteWorklistAction(sn, constants.STATUS_APPROVE_K2, "", null);

                            // Send Email to all related person
                            EmailController emailController = new EmailController();
                            DateTime momDate = (DateTime)reqData.MOMDATE;

                            emailController.SendMomEmail(ewr.OID, momDate.ToString("dd MMM yyyy") + " " + momDate.ToString("hh:mm:ss"), string.Join("; ", emailAdresses.ToArray()), "");


                        }
                        catch (DbEntityValidationException exc)
                        {
                            var errorMessage = "";
                            foreach (var error in exc.EntityValidationErrors)
                            {
                                foreach (var validationError in error.ValidationErrors)
                                    errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                            }
                            WriteLog(errorMessage);
                            throw new ApplicationException(errorMessage);

                        }
                        finally
                        {
                            k2Management.Close();
                            k2Connect.Disconnect();
                            
                        }

                    }

                    return Ok();

                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

            }
            return BadRequest("No data found");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MOMEWRExists(int id)
        {
            return db.MOMEWRs.Count(e => e.OID == id) > 0;
        }
        
        [HttpGet]
        [ActionName("TestEmailMOMSubmit")]
        public IHttpActionResult TestEmailMOMSubmit(int param)
        {
            string webappurl = global::PTVI.KAIZEN.WebAPIProperties.Settings.Default.IPROM_WebApp_BaseUrl;
            
            webappurl = webappurl.Replace("/", "|") + "|TemplateEWRDetailForm.aspx?EWRID=" + param;

            string webapiurl = "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20";
            string ellipseurl = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.WebApiUrlEllipse;
            string pdfurl = ellipseurl + webapiurl + Uri.EscapeDataString(webappurl) + "/";
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            Uri uri = new Uri(pdfurl);

            string fileName = "EWR-" + param + ".pdf";
            string filePath = Path.Combine(root, fileName);

            using (var client = new WebClient())
            {
                client.DownloadFile(pdfurl, filePath);

                UploadController upload = new UploadController();
                upload.UploadFileInAppData(fileName, param, "EWRMOM", "EWR Mom PDF", "herus");

                return Ok();
            }

        }
        [HttpGet]
        [ActionName("TestMoMEWRNumber")]
        public IHttpActionResult TestMoMEWRNumber(string param)
        {
            return Ok(GenerateMoMEWRNumber());
        }
        public string GenerateMoMEWRNumber()
        {
            string ewrFormat = "MOMEWR";
            string ewrFormatNo = "00000";

            string ewrMoMNo = GetMoMEWRNumber();

            ewrFormatNo = ewrFormatNo + ewrMoMNo.Split('|')[1];

            string ewrNo = ewrFormatNo.Substring(ewrFormatNo.Length - 5, ewrFormatNo.Length - 1);
            return ewrFormat + ewrMoMNo.Split('|')[0] + ewrNo;
        }

        [HttpGet]
        [ActionName("incrementmomewrno")]
        public IHttpActionResult AddMoMEwrNo()
        {
            IncrementMoMEWRNumber();
            return Ok();
        }
        public void IncrementMoMEWRNumber()
        {
            GENERALPARAMETER genParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == "MOMEWRNO");
            if (genParam != null)
            {
                string year = genParam.VALUE.Split('|')[0];
                int incNo = int.Parse(genParam.VALUE.Split('|')[1]);
                genParam.VALUE = year + "|" + (incNo + 1).ToString();
                db.Entry(genParam).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public string GetMoMEWRNumber()
        {
            string ewrNumber = "";
            DateTime currDate = DateTime.Now;
            GENERALPARAMETER genParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == "MOMEWRNO");
            if (genParam != null)
            {
                string paramEWRNo = genParam.VALUE.Split('|')[1];
                string paramEWRYear = genParam.VALUE.Split('|')[0];
                if (paramEWRYear != currDate.Year.ToString())
                {
                    ewrNumber = currDate.Year.ToString() + "|1";

                    genParam.VALUE = ewrNumber;
                    db.Entry(genParam).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    ewrNumber = genParam.VALUE;
                }
            }
            else
            {
                GENERALPARAMETER momEWRNo = new GENERALPARAMETER();
                momEWRNo.TITLE = "MOMEWRNO";
                momEWRNo.DATATYPE = "number";
                momEWRNo.DESCRIPTION = "MOM EWR Number";
                momEWRNo.ISACTIVE = true;
                momEWRNo.VALUE = currDate.Year.ToString() + "|1";
                momEWRNo.CREATEDBY = "system";
                momEWRNo.CREATEDDATE = DateTime.Now;

                db.GENERALPARAMETERs.Add(momEWRNo);
                db.SaveChanges();

                ewrNumber = currDate.Year.ToString() + "|1"; ;
            }
            return ewrNumber;
        }
    }
}