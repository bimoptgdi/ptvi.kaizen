﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterKeyDeliverablesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/masterKeyDeliverables
        public IQueryable<masterKeyDeliverableObject> GetMASTERKEYDELIVERABLEs()
        {
            return db.MASTERKEYDELIVERABLES.Select(s => new masterKeyDeliverableObject
            {
                id = s.OID,
                projectGroupCategoryName = s.MASTERPROJECTGROUPCATEGORY.NAME,
                projectGroupCategoryId = s.PROJECTGROUPCATEGORYID,
                seq = s.SEQ,
                keyDeliverables = s.KEYDELIVERABLES,
                remarks = s.REMARKS 
           });
        }

        // GET: api/masterKeyDeliverables/5
        [ResponseType(typeof(MASTERKEYDELIVERABLE))]
        public IHttpActionResult GetMASTERKEYDELIVERABLE(int id)
        {
            MASTERKEYDELIVERABLE mASTERKEYDELIVERABLE = db.MASTERKEYDELIVERABLES.Find(id);
            if (mASTERKEYDELIVERABLE == null)
            {
                return NotFound();
            }

            return Ok(mASTERKEYDELIVERABLE);
        }

        // PUT: api/masterKeyDeliverables/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMASTERKEYDELIVERABLE(int id, MASTERKEYDELIVERABLE mASTERKEYDELIVERABLE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERKEYDELIVERABLE.OID)
            {
                return BadRequest();
            }

            db.Entry(mASTERKEYDELIVERABLE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERKEYDELIVERABLEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/masterKeyDeliverables
        [ResponseType(typeof(MASTERKEYDELIVERABLE))]
        public IHttpActionResult PostMASTERKEYDELIVERABLE(MASTERKEYDELIVERABLE mASTERKEYDELIVERABLE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MASTERKEYDELIVERABLES.Add(mASTERKEYDELIVERABLE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mASTERKEYDELIVERABLE.OID }, mASTERKEYDELIVERABLE);
        }

        // DELETE: api/masterKeyDeliverables/5
        [ResponseType(typeof(MASTERKEYDELIVERABLE))]
        public IHttpActionResult DeleteMASTERKEYDELIVERABLE(int id)
        {
            MASTERKEYDELIVERABLE mASTERKEYDELIVERABLE = db.MASTERKEYDELIVERABLES.Find(id);
            if (mASTERKEYDELIVERABLE == null)
            {
                return NotFound();
            }

            db.MASTERKEYDELIVERABLES.Remove(mASTERKEYDELIVERABLE);
            db.SaveChanges();

            return Ok(mASTERKEYDELIVERABLE);
        }
        [HttpPut]
        [ActionName("updateMasterKey")]

        public IHttpActionResult updateMasterKey (int param, masterKeyDeliverableObject masterKeyParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != masterKeyParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERKEYDELIVERABLE update = db.MASTERKEYDELIVERABLES.Find(param);
                update.PROJECTGROUPCATEGORYID = masterKeyParam.projectGroupCategoryId;
                update.SEQ = masterKeyParam.seq;
                update.KEYDELIVERABLES = masterKeyParam.keyDeliverables;
                update.REMARKS = masterKeyParam.remarks;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(masterKeyParam);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        [ActionName("addMasterKey")]

        public IHttpActionResult addMasterKey(string param, masterKeyDeliverableObject masterKeyParam)
        {
            MASTERKEYDELIVERABLE add = new MASTERKEYDELIVERABLE();
            add.PROJECTGROUPCATEGORYID = masterKeyParam.projectGroupCategoryId;
            add.SEQ = masterKeyParam.seq;
            add.KEYDELIVERABLES = masterKeyParam.keyDeliverables;
            add.REMARKS = masterKeyParam.remarks;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERKEYDELIVERABLES.Add(add);
            db.SaveChanges();

            masterKeyParam.id = add.OID;
            masterKeyParam.projectGroupCategoryName = db.MASTERPROJECTGROUPCATEGORies.Find(masterKeyParam.projectGroupCategoryId).NAME;
            return Ok(masterKeyParam);


        }

        [HttpGet]
        [ActionName("getMasterKeyList")]
        public IQueryable<masterProjectGroupCategoryObject> getMasterKeyList(string param)
        {
            var masterKeyList = db.MASTERPROJECTGROUPCATEGORies.Select(s => new masterProjectGroupCategoryObject
            {
                id = s.OID,
                name = s.NAME
            });
            return masterKeyList;
        }

        //[HttpGet]
        //[ActionName("getMasterKeyInActive")]

        //public IHttpActionResult getMasterKeyInActive(int param)
        //{
        //    var masterKey = db.MASTERKEYDELIVERABLES.Where(x => x.PROJECTGROUPCATEGORYID == 1).Select(s => new masterKeyDeliverableObject
        //    {
        //        id = s.OID,
        //        PROJECTGROUPCATEGORYID = s.PROJECTGROUPCATEGORYID
        //    });
        //    return Ok(masterKey);
        //}

        private bool MASTERKEYDELIVERABLEExists(int id)
        {
            return db.MASTERKEYDELIVERABLES.Count(e => e.OID == id) > 0;
        }

        // api/masterKeyDeliverables/FindByKeyDeliverables/param
        [HttpGet]
        [ActionName("FindByKeyDeliverables")]
        public IEnumerable<masterKeyDeliverableObject> FindByKeyDeliverables(int param)
        {
            var listKD = db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param).Select(s => s.NAME);

            return db.MASTERKEYDELIVERABLES.Where(w => ! listKD.Contains(w.KEYDELIVERABLES) && w.MASTERPROJECTGROUPCATEGORY.CODE == db.PROJECTs.FirstOrDefault(p => p.OID == param).PROJECTTYPE).Select(s => new masterKeyDeliverableObject 
            {
                id = s.OID,
                keyDeliverables = s.KEYDELIVERABLES,
            });
        }
    }
}