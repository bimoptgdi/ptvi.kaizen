﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectDocumentComplexitiesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/projectDocumentComplexities
        public IQueryable<projectDocumentComplexityObject> GetPROJECTDOCUMENTCOMPLEXITies()
        {
            //return db.PROJECTDOCUMENTCOMPLEXITies;
            return null;
        }

        // GET: api/projectDocumentComplexities/5
        [ResponseType(typeof(PROJECTDOCUMENTCOMPLEXITY))]
        public IHttpActionResult GetPROJECTDOCUMENTCOMPLEXITY(int id)
        {
            PROJECTDOCUMENTCOMPLEXITY pROJECTDOCUMENTCOMPLEXITY = db.PROJECTDOCUMENTCOMPLEXITies.Find(id);
            if (pROJECTDOCUMENTCOMPLEXITY == null)
            {
                return NotFound();
            }
            projectDocumentComplexityObject result = new projectDocumentComplexityObject();
            result.oid = pROJECTDOCUMENTCOMPLEXITY.OID;
            result.projectDocumentId = pROJECTDOCUMENTCOMPLEXITY.PROJECTDOCUMENTID;
            result.masterComplexityDCId = pROJECTDOCUMENTCOMPLEXITY.MASTERCOMPLEXITYDCID;
            result.category = pROJECTDOCUMENTCOMPLEXITY.MASTERCOMPLEXITYDC.CATEGORY;
            result.complexityValue = pROJECTDOCUMENTCOMPLEXITY.COMPLEXITYVALUE;
            result.point = pROJECTDOCUMENTCOMPLEXITY.MASTERCOMPLEXITYDC.POINT;
            result.weight = pROJECTDOCUMENTCOMPLEXITY.MASTERCOMPLEXITYDC.WEIGHT;
            result.createdDate = pROJECTDOCUMENTCOMPLEXITY.CREATEDDATE;
            result.createdBy = pROJECTDOCUMENTCOMPLEXITY.CREATEDBY;
            result.updatedDate = pROJECTDOCUMENTCOMPLEXITY.UPDATEDDATE;
            result.updatedBy = pROJECTDOCUMENTCOMPLEXITY.UPDATEDBY;
            return Ok(result);
        }

        // PUT: api/projectDocumentComplexities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROJECTDOCUMENTCOMPLEXITY(int id, PROJECTDOCUMENTCOMPLEXITY pROJECTDOCUMENTCOMPLEXITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTDOCUMENTCOMPLEXITY.OID)
            {
                return BadRequest();
            }

            db.Entry(pROJECTDOCUMENTCOMPLEXITY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTCOMPLEXITYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/projectDocumentComplexities
        [ResponseType(typeof(PROJECTDOCUMENTCOMPLEXITY))]
        public IHttpActionResult PostPROJECTDOCUMENTCOMPLEXITY(PROJECTDOCUMENTCOMPLEXITY pROJECTDOCUMENTCOMPLEXITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROJECTDOCUMENTCOMPLEXITies.Add(pROJECTDOCUMENTCOMPLEXITY);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pROJECTDOCUMENTCOMPLEXITY.OID }, pROJECTDOCUMENTCOMPLEXITY);
        }

        // DELETE: api/projectDocumentComplexities/5
        [ResponseType(typeof(PROJECTDOCUMENTCOMPLEXITY))]
        public IHttpActionResult DeletePROJECTDOCUMENTCOMPLEXITY(int id)
        {
            PROJECTDOCUMENTCOMPLEXITY pROJECTDOCUMENTCOMPLEXITY = db.PROJECTDOCUMENTCOMPLEXITies.Find(id);
            if (pROJECTDOCUMENTCOMPLEXITY == null)
            {
                return NotFound();
            }

            db.PROJECTDOCUMENTCOMPLEXITies.Remove(pROJECTDOCUMENTCOMPLEXITY);
            db.SaveChanges();

            return Ok(pROJECTDOCUMENTCOMPLEXITY);
        }

        [HttpGet]
        [ActionName("getComplexityFromProjectDocId")]
        public IEnumerable<projectDocumentComplexityObject> getComplexityFromProjectDocId(int param)
        {
            var getProjDocument = db.PROJECTDOCUMENTs.Find(param);
            var getComplexityType = db.PROJECTDOCUMENTCOMPLEXITies.Where(w => w.PROJECTDOCUMENTID == param).Select(s => new projectDocumentComplexityObject
            {
                oid = s.OID,
                projectDocumentId = s.PROJECTDOCUMENTID,
                masterComplexityDCId = s.MASTERCOMPLEXITYDCID,
                category = s.MASTERCOMPLEXITYDCID.HasValue ? s.MASTERCOMPLEXITYDC.CATEGORY : null,
                complexityValue = s.COMPLEXITYVALUE,
                point = s.MASTERCOMPLEXITYDCID.HasValue ? s.MASTERCOMPLEXITYDC.POINT : null,
                weight = s.MASTERCOMPLEXITYDCID.HasValue ? s.MASTERCOMPLEXITYDC.WEIGHT : null
            });

            if (db.MASTERCOMPLEXITYDCs.Where(w => w.YEAR == getProjDocument.PLANFINISHDATE.Value.Year).Count() == 0)
            {
                int? maxYear = db.MASTERCOMPLEXITYDCs.Max(m => m.YEAR);
                if (maxYear.HasValue)
                {
                    var defaultValue = db.MASTERCOMPLEXITYDCs.Where(w => w.YEAR == maxYear).ToList().Select(s => new MASTERCOMPLEXITYDC {
                        YEAR = getProjDocument.PLANFINISHDATE.Value.Year,
                        CATEGORY = s.CATEGORY,
                        POINT = s.POINT,
                        WEIGHT = s.WEIGHT,
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = GetCurrentNTUserId()
                    });
                    db.MASTERCOMPLEXITYDCs.AddRange(defaultValue);

                    db.SaveChanges();
                }
            }

            var masterComplexityType = db.MASTERCOMPLEXITYDCs.Where(w => w.YEAR == getProjDocument.PLANFINISHDATE.Value.Year && !getComplexityType.Select(s => s.masterComplexityDCId).Contains(w.OID)).Select(s => new projectDocumentComplexityObject
            {
                oid = 0,
                projectDocumentId = param,
                masterComplexityDCId = s.OID,
                category = s.CATEGORY,
                complexityValue = 0,
                point = s.POINT,
                weight = s.WEIGHT
            });

            return getComplexityType.Union(masterComplexityType).OrderBy(o => o.category).Select(s => new projectDocumentComplexityObject
            {
                oid = s.oid,
                projectDocumentId = s.projectDocumentId,
                masterComplexityDCId = s.masterComplexityDCId,
                category = s.category,
                complexityValue = s.complexityValue,
                point = s.point,
                weight = s.weight
            });
        }

        [HttpPut]
        [ActionName("updateByMasterComplexity")]
        public IHttpActionResult updateByMasterComplexity(int param, projectDocumentComplexityObject paramComplexity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROJECTDOCUMENTCOMPLEXITY complexity = new PROJECTDOCUMENTCOMPLEXITY();
            if (param > 0)
            {
                if (paramComplexity.oid > 0)
                {
                    complexity = db.PROJECTDOCUMENTCOMPLEXITies.Find(paramComplexity.oid);
                    complexity.COMPLEXITYVALUE = paramComplexity.complexityValue;
                    complexity.UPDATEDBY = GetCurrentNTUserId();
                    complexity.UPDATEDDATE = DateTime.Now;
                    db.Entry(complexity).State = EntityState.Modified;
                }
                else
                {
                    complexity.PROJECTDOCUMENTID = paramComplexity.projectDocumentId;
                    complexity.MASTERCOMPLEXITYDCID = paramComplexity.masterComplexityDCId;
                    complexity.COMPLEXITYVALUE = paramComplexity.complexityValue;
                    complexity.CREATEDBY = GetCurrentNTUserId();
                    complexity.CREATEDDATE = DateTime.Now;
                    db.Entry(complexity).State = EntityState.Added;
                }
            }
            else
            {
                return BadRequest();
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTCOMPLEXITYExists(complexity.OID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            paramComplexity.masterDesignerComplexity = null;
            paramComplexity.projectDocument = null;
            paramComplexity.oid = complexity.OID;
            return Ok(paramComplexity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTDOCUMENTCOMPLEXITYExists(int id)
        {
            return db.PROJECTDOCUMENTCOMPLEXITies.Count(e => e.OID == id) > 0;
        }
    }
}