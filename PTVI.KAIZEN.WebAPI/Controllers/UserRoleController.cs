﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class UserRoleController : KAIZENController
    {
        // GET api/UserRole
        public IEnumerable<USERROLE> GetUSERROLEs()
        {
            var userroles = db.USERROLEs.Include(u => u.ROLE).Include(u => u.USER);
            return userroles.AsEnumerable();
        }

        // GET api/UserRole/5
        public USERROLE GetUSERROLE(int id)
        {
            USERROLE userrole = db.USERROLEs.Find(id);
            if (userrole == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return userrole;
        }

        // PUT api/UserRole/5
        public HttpResponseMessage PutUSERROLE(int id, USERROLE userrole)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != userrole.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(userrole).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/UserRole
        public HttpResponseMessage PostUSERROLE(USERROLE userrole)
        {
            if (ModelState.IsValid)
            {
                db.USERROLEs.Add(userrole);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, userrole);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = userrole.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/UserRole/5
        public HttpResponseMessage DeleteUSERROLE(int id)
        {
            USERROLE userrole = db.USERROLEs.Find(id);
            if (userrole == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.USERROLEs.Remove(userrole);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, userrole);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}