﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterDCFinalResultController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/masterDCFinalResult
        public IQueryable<masterDCFinalResultObject> GetMASTERFINALRESULT()
        {
            return db.MASTERFINALRESULTs.Select(s => new masterDCFinalResultObject
            {
                id = s.OID,
                year = s.YEAR,
                description = s.DESCRIPTION,
                resultValue = s.VALUE,
            });
        }

        // GET api/masterDCFinalResult/getByYear/param
        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterDCFinalResultObject> getByYear(int param)
        {
            return db.MASTERFINALRESULTs.Where(x => x.YEAR == param).Select(s => new masterDCFinalResultObject
            {
                id = s.OID,
                year = s.YEAR,
                description = s.DESCRIPTION,
                resultValue = s.VALUE,
            });
        }
        // POST api/masterDCFinalResult/addFinalResult
        [HttpPost]
        [ActionName("addFinalResult")]
        public IHttpActionResult addFinalResult(string param, masterDCFinalResultObject finalResultParam)
        {
            MASTERFINALRESULT add = new MASTERFINALRESULT();
            add.YEAR = finalResultParam.year;
            add.DESCRIPTION = finalResultParam.description;
            add.VALUE = finalResultParam.resultValue;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERFINALRESULTs.Add(add);
            db.SaveChanges();

            finalResultParam.id = add.OID;
            return Ok(finalResultParam);
        }

        // PUT api/updateDCFinalResult/addFinalResult
        [HttpPut]
        [ActionName("updateFinalResult")]

        public IHttpActionResult updateFinalResult (int param, masterDCFinalResultObject finalResultParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != finalResultParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERFINALRESULT update = db.MASTERFINALRESULTs.Find(param);
                update.YEAR = finalResultParam.year;
                update.DESCRIPTION = finalResultParam.description;
                update.VALUE = finalResultParam.resultValue;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(finalResultParam);
        }

        [HttpGet]
        [ActionName("getMaxMinFinalResult")]
        public object getMaxMinFinalResult(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERFINALRESULTs.Min(w => w.YEAR);
            var max = db.MASTERFINALRESULTs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }
        private bool MASTERFINALRESULTExists(int id)
        {
            return db.MASTERFINALRESULTs.Count(e => e.OID == id) > 0;
        }
        // DELETE api/<controller>/5
        public void Delete(int id)
        {

        }
    }
}