﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using Microsoft.Data.OData;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PTVI.iPROM.WebApi.Models.CustomObject;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<EMPLOYEE>("EmployeeOData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EmployeeODataController : ODataController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        private KAIZENController controller = new KAIZENController();

        [EnableQuery]
        public IQueryable<EMPLOYEE> Get()
        {
            //String confString = HttpContext.Current.Request.ApplicationPath.ToString();
            //Configuration conf = WebConfigurationManager.OpenWebConfiguration(confString);
            //ScriptingJsonSerializationSection section = (ScriptingJsonSerializationSection)conf.GetSection("system.web.extensions/scripting/webServices/jsonSerialization");
            //section.MaxJsonLength = 6553600;
            //conf.Save();
            // validate the query.
            try
            {
                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {   
                    List<EMPLOYEE> result = controller.GetListFromExternalData<EMPLOYEE>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_VALE_EMPLOYEES"); // untuk production/development server

                    return result.Where(d => d.EMPLOYEEID != "-").AsQueryable<EMPLOYEE>();
                }
            }
            catch (ODataException ex)
            {
                return null;
            }

            // return Ok<IEnumerable<EMPLOYEE>>(eMPLOYEEs);
            return null;

        }
    }
}
