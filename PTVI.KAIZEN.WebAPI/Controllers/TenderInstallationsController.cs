﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TenderInstallationsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TenderInstallations
        public IQueryable<TENDERINSTALLATION> GetTENDERINSTALLATIONs()
        {
            return db.TENDERINSTALLATIONs;
        }

        // GET: api/TenderInstallations/5
        [ResponseType(typeof(TENDERINSTALLATION))]
        public IHttpActionResult GetTENDERINSTALLATION(int id)
        {
            TENDERINSTALLATION tENDERINSTALLATION = db.TENDERINSTALLATIONs.Find(id);
            if (tENDERINSTALLATION == null)
            {
                return NotFound();
            }

            return Ok(tENDERINSTALLATION);
        }

        // PUT: api/TenderInstallations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDERINSTALLATION(int id, TENDERINSTALLATION tENDERINSTALLATION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDERINSTALLATION.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDERINSTALLATION).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDERINSTALLATIONExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TenderInstallations
        [ResponseType(typeof(TENDERINSTALLATION))]
        public IHttpActionResult PostTENDERINSTALLATION(TENDERINSTALLATION tENDERINSTALLATION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDERINSTALLATIONs.Add(tENDERINSTALLATION);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDERINSTALLATION.OID }, tENDERINSTALLATION);
        }

        // DELETE: api/TenderInstallations/5
        [ResponseType(typeof(TENDERINSTALLATION))]
        public IHttpActionResult DeleteTENDERINSTALLATION(int id)
        {
            TENDERINSTALLATION tENDERINSTALLATION = db.TENDERINSTALLATIONs.Find(id);
            if (tENDERINSTALLATION == null)
            {
                return NotFound();
            }

            db.TENDERINSTALLATIONs.Remove(tENDERINSTALLATION);
            db.SaveChanges();

            return Ok(tENDERINSTALLATION);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TENDERINSTALLATIONExists(int id)
        {
            return db.TENDERINSTALLATIONs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("TenderInstallationById")]
        public tenderInstallationObject TenderInstallationById(int param)
        {
            return db.TENDERINSTALLATIONs.Where(w => w.OID == param).Select(s => new tenderInstallationObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                task = s.TASK,
                weightPercentage = s.WEIGHTPERCENTAGE,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                completePercentage = s.COMPLETEPERCENTAGE,
                issue = s.ISSUE,
                issueStatus = s.ISSUESTATUS
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("TenderInstallationsByTenderId")]
        public IEnumerable<tenderInstallationObject> TenderInstallationsByTenderId(int param)
        {
            return db.TENDERINSTALLATIONs.Where(w => w.TENDERID == param).Select(s => new tenderInstallationObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                task = s.TASK,
                weightPercentage = s.WEIGHTPERCENTAGE,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                completePercentage = s.COMPLETEPERCENTAGE,
                issue = s.ISSUE,
                issueStatus = s.ISSUESTATUS
            });
        }

        [HttpPut]
        [ActionName("TenderInstallationSubmit")]
        public IHttpActionResult TenderInstallationSubmit(int param, tenderInstallationObject installationParam)
        {
            TENDERINSTALLATION installation = new TENDERINSTALLATION();
            if (installationParam.id > 0)
            {
                installation = db.TENDERINSTALLATIONs.Find(installationParam.id);
                installation.UPDATEDBY = GetCurrentNTUserId();
                installation.UPDATEDDATE = DateTime.Now;
            }
            else
            {
                installation.CREATEDBY = GetCurrentNTUserId();
                installation.CREATEDDATE = DateTime.Now;
                installation.TENDERID = param;
            }
            installation.TASK = installationParam.task;
            installation.WEIGHTPERCENTAGE = installationParam.weightPercentage;
            installation.PLANSTARTDATE = installationParam.planStartDate;
            installation.PLANFINISHDATE = installationParam.planFinishDate;
            installation.ACTUALSTARTDATE = installationParam.actualStartDate;
            installation.ACTUALFINISHDATE = installationParam.actualFinishDate;
            installation.COMPLETEPERCENTAGE = installationParam.completePercentage;
            installation.ISSUE = installationParam.issue;
            installation.ISSUESTATUS = installationParam.issueStatus;

            if (installationParam.id > 0)
                db.Entry(installation).State = EntityState.Modified;
            else
                db.Entry(installation).State = EntityState.Added;
            db.SaveChanges();

            return Ok(TenderInstallationById(installation.OID));
        }
    }
}