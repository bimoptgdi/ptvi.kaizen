﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Controllers;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTGDI.Email;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class OtherDepartmentTasksController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accController = new AccountController();

        // GET: api/OtherDepartmentTasks
        public IQueryable<otherDepartmentTaskObject> GetOTHERDEPARTMENTTASKs()
        {
            return db.OTHERDEPARTMENTTASKs.Select(s => new otherDepartmentTaskObject {
                id = s.OID,
                projectId = s.PROJECTID,
                departmentId = s.DEPARTMENTID,
                departmentName = s.DEPARTMENTNAME,
                taskName = s.TASKNAME,
                picId = s.PICID,
                picName = s.PICNAME,
                picEmail = s.PICEMAIL,
                planDate = s.PLANDATE,
                actualDate = s.ACTUALDATE,
                remark = s.REMARK,
                status = s.STATUS
            });
        }

        // GET: api/OtherDepartmentTasks/5
        [ResponseType(typeof(otherDepartmentTaskObject))]
        public IHttpActionResult GetOTHERDEPARTMENTTASK(int id)
        {
            OTHERDEPARTMENTTASK oTHERDEPARTMENTTASK = db.OTHERDEPARTMENTTASKs.Find(id);
            if (oTHERDEPARTMENTTASK == null)
            {
                return NotFound();
            }

            otherDepartmentTaskObject result = new otherDepartmentTaskObject
            {
                id = oTHERDEPARTMENTTASK.OID,
                projectId = oTHERDEPARTMENTTASK.PROJECTID,
                departmentId = oTHERDEPARTMENTTASK.DEPARTMENTID,
                departmentName = oTHERDEPARTMENTTASK.DEPARTMENTNAME,
                taskName = oTHERDEPARTMENTTASK.TASKNAME,
                picId = oTHERDEPARTMENTTASK.PICID,
                picName = oTHERDEPARTMENTTASK.PICNAME,
                picEmail = oTHERDEPARTMENTTASK.PICEMAIL,
                planDate = oTHERDEPARTMENTTASK.PLANDATE,
                actualDate = oTHERDEPARTMENTTASK.ACTUALDATE,
                remark = oTHERDEPARTMENTTASK.REMARK,
                status = oTHERDEPARTMENTTASK.STATUS
            };
            return Ok(result);
        }

        // PUT: api/OtherDepartmentTasks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOTHERDEPARTMENTTASK(int id, OTHERDEPARTMENTTASK oTHERDEPARTMENTTASK)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oTHERDEPARTMENTTASK.OID)
            {
                return BadRequest();
            }

            db.Entry(oTHERDEPARTMENTTASK).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OTHERDEPARTMENTTASKExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OtherDepartmentTasks
        [ResponseType(typeof(OTHERDEPARTMENTTASK))]
        public IHttpActionResult PostOTHERDEPARTMENTTASK(OTHERDEPARTMENTTASK oTHERDEPARTMENTTASK)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.OTHERDEPARTMENTTASKs.Add(oTHERDEPARTMENTTASK);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = oTHERDEPARTMENTTASK.OID }, oTHERDEPARTMENTTASK);
        }

        // DELETE: api/OtherDepartmentTasks/5
        [ResponseType(typeof(OTHERDEPARTMENTTASK))]
        public IHttpActionResult DeleteOTHERDEPARTMENTTASK(int id)
        {
            OTHERDEPARTMENTTASK oTHERDEPARTMENTTASK = db.OTHERDEPARTMENTTASKs.Find(id);
            if (oTHERDEPARTMENTTASK == null)
            {
                return NotFound();
            }

            db.OTHERDEPARTMENTTASKs.Remove(oTHERDEPARTMENTTASK);
            db.SaveChanges();

            return Ok(oTHERDEPARTMENTTASK);
        }

        [HttpGet]
        [ActionName("listOtheDeptTaskByProjectId")]
        public IEnumerable<otherDepartmentTaskObject> listOtheDeptTaskByProjectId(int param)
        {
            return db.OTHERDEPARTMENTTASKs.Where(w => w.PROJECTID == param).ToList().Select(s => new otherDepartmentTaskObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                departmentId = s.DEPARTMENTID,
                departmentName = s.DEPARTMENTNAME,
                taskName = s.TASKNAME,
                picId = s.PICID,
                picName = s.PICNAME,
                picEmail = s.PICEMAIL,
                planDate = s.PLANDATE,
                actualDate = s.ACTUALDATE,
                remark = s.REMARK,
                status = s.STATUS,
                trafic = s.PLANDATE.HasValue ? traficStatus(s.PLANDATE.Value, s.ACTUALDATE, null) : constants.TRAFFICLIGHT_GREY
            }).OrderBy(o => !o.actualDate.HasValue ? o.planDate : o.actualDate);
        }

        [HttpGet]
        [ActionName("listOtheDeptTaskByBadgeNo")]
        public IEnumerable<otherDepartmentTaskObject> listOtheDeptTaskByBadgeNo(string param)
        {
            return db.OTHERDEPARTMENTTASKs.Where(w => w.PICID == param).ToList().Select(s => new otherDepartmentTaskObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                projectNo = s.PROJECT.PROJECTNO,
                projectDesc = s.PROJECT.PROJECTDESCRIPTION,
                departmentId = s.DEPARTMENTID,
                departmentName = s.DEPARTMENTNAME,
                taskName = s.TASKNAME,
                picId = s.PICID,
                picName = s.PICNAME,
                picEmail = s.PICEMAIL,
                planDate = s.PLANDATE,
                actualDate = s.ACTUALDATE,
                remark = s.REMARK,
                status = s.STATUS,
                trafic = s.PLANDATE.HasValue ? traficStatus(s.PLANDATE.Value, s.ACTUALDATE, null) : constants.TRAFFICLIGHT_GREY
            });
        }

        [HttpGet]
        [ActionName("otheDeptTaskById")]
        public otherDepartmentTaskObject otheDeptTaskById(int id)
        {
            OTHERDEPARTMENTTASK oTHERDEPARTMENTTASK = db.OTHERDEPARTMENTTASKs.Find(id);
            if (oTHERDEPARTMENTTASK == null)
            {
                return new otherDepartmentTaskObject();
            }

            otherDepartmentTaskObject result = new otherDepartmentTaskObject
            {
                id = oTHERDEPARTMENTTASK.OID,
                projectId = oTHERDEPARTMENTTASK.PROJECTID,
                departmentId = oTHERDEPARTMENTTASK.DEPARTMENTID,
                departmentName = oTHERDEPARTMENTTASK.DEPARTMENTNAME,
                taskName = oTHERDEPARTMENTTASK.TASKNAME,
                picId = oTHERDEPARTMENTTASK.PICID,
                picName = oTHERDEPARTMENTTASK.PICNAME,
                picEmail = oTHERDEPARTMENTTASK.PICEMAIL,
                planDate = oTHERDEPARTMENTTASK.PLANDATE,
                actualDate = oTHERDEPARTMENTTASK.ACTUALDATE,
                remark = oTHERDEPARTMENTTASK.REMARK,
                status = oTHERDEPARTMENTTASK.STATUS,
                trafic = oTHERDEPARTMENTTASK.STATUS == constants.STATUSIPROM_COMPLETED ? constants.TRAFFICLIGHT_GREEN : oTHERDEPARTMENTTASK.PLANDATE.HasValue ? traficStatus(oTHERDEPARTMENTTASK.PLANDATE.Value, oTHERDEPARTMENTTASK.ACTUALDATE, null) : constants.TRAFFICLIGHT_GREY
            };
            return result;
        }

        [HttpPost]
        [ActionName("deptTaskAdd")]
        public IHttpActionResult deptTaskAdd(string param, otherDepartmentTaskObject otherDepartmentTaskParam)
        {
            EmailSender emailSender = new EmailSender();
            OTHERDEPARTMENTTASK otherDeptTask = new OTHERDEPARTMENTTASK();
            otherDeptTask.PROJECTID = otherDepartmentTaskParam.projectId;
            otherDeptTask.DEPARTMENTID = otherDepartmentTaskParam.departmentId;
            otherDeptTask.DEPARTMENTNAME = otherDepartmentTaskParam.departmentName;
            otherDeptTask.TASKNAME = otherDepartmentTaskParam.taskName;
            otherDeptTask.PICID = otherDepartmentTaskParam.picId;
            otherDeptTask.PICNAME = otherDepartmentTaskParam.picName;
            otherDeptTask.PICEMAIL = otherDepartmentTaskParam.picEmail;
            otherDeptTask.PLANDATE = otherDepartmentTaskParam.planDate;
            otherDeptTask.ACTUALDATE = otherDepartmentTaskParam.actualDate;
            otherDeptTask.REMARK = otherDepartmentTaskParam.remark;
            otherDeptTask.STATUS = otherDepartmentTaskParam.status;
            otherDeptTask.CREATEDDATE = DateTime.Now;
            otherDeptTask.CREATEDBY = GetCurrentNTUserId();

            db.Entry(otherDeptTask).State = EntityState.Added;
            db.SaveChanges();

            //send email
            var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.otherDepartmentTask).FirstOrDefault();
            var projectRequest = db.PROJECTs.Where(x => x.OID == otherDeptTask.PROJECTID).FirstOrDefault();
            var mailList = new List<usersToEmail>() {
                        new usersToEmail(){ userName = projectRequest.OWNERNAME, email =  "", position = constants.posOwner },
                        new usersToEmail(){userName = projectRequest.SPONSORNAME, email = "", position = constants.posSponsor  },
                        new usersToEmail(){userName = projectRequest.PROJECTMANAGERNAME, email = "", position = constants.posProjectManager  } ,
                        new usersToEmail(){userName = projectRequest.PROJECTENGINEERNAME, email = "", position = constants.posProjectEngineer  },
                        new usersToEmail(){userName = projectRequest.MAINTENANCEREPNAME, email = "", position = constants.posMaintenanceRep  },
                        new usersToEmail(){userName = projectRequest.OPERATIONREPNAME, email = "", position = constants.posOperationRep  }
                    };
            var ua = db.USERASSIGNMENTs.Where(x => (x.PROJECTID == otherDeptTask.PROJECTID && x.ISACTIVE == true));
            foreach (var u in ua)
            {
                var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = "", position = posName != null ? posName.TEXT : "" });
            }

            string RecipientEmail = otherDeptTask.PICEMAIL;
            string userList = "<table>";
            foreach (var l in mailList)
            {
                userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
            }
            userList = userList + "</table>";

            string RecipientName = string.Join(";", mailList.Select(x => x.userName).ToArray());
            string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
            var uc = new UserController();
            var login = GetCurrentNTUserId();
#if DEBUG
            if (login == "IUSR") login = "herus";
#endif
            var logininfo = uc.FindLoginInfoByNtUserID(login).FirstOrDefault();
            var RecipientCC = "";
            if (logininfo != null)
                RecipientCC = logininfo.GetType().GetProperty("EMAIL").GetValue(logininfo,null).ToString();
            var FromEmail = "testing@ptgdi.com";
            var subject = mail.SUBJECT;
            var body = mail.BODY;
            var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
            var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + otherDeptTask.PICID).FirstOrDefault();
            string prefixGender = "";
            if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                prefixGender = constants.prefixmale;
            else
                if (employeeDetail.MARITAL_STATUS == constants.married)
                prefixGender = constants.prefixfemaleMarried;
            else
                prefixGender = constants.prefixfemaleSingle;
            body = body.Replace("[userName]", prefixGender + " " + otherDeptTask.PICNAME);
            body = body.Replace("[projectNo]", projectRequest.PROJECTNO);
            body = body.Replace("[projectDesc]", projectRequest.PROJECTDESCRIPTION);
            body = body.Replace("[department]", otherDepartmentTaskParam.departmentName);
            //body = body.Replace("[planStartDate]", projectRequest.PLANSTARTDATE.Value.ToString("dd MMMM yyyy"));
            //body = body.Replace("[planFinishDate]", projectRequest.PLANFINISHDATE.Value.ToString("dd MMMM yyyy"));
            //body = body.Replace("[listuser]", userList);
            body = body.Replace("[task]", otherDeptTask.TASKNAME);
            body = body.Replace("[planDate]", otherDeptTask.PLANDATE.Value.ToString("dd MMMM yyyy"));
            body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/MainProjectForm.aspx?projectId=" + projectRequest.OID);
            body = body.Replace("[assign]", accController.getEmployeeByBN(projectRequest.CREATEDBY).full_Name);
            body = body.Replace("\r\n", "");
            body = body.Replace("\\", "");
            emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);

            return Ok(otheDeptTaskById(otherDeptTask.OID));
        }

        [HttpPut]
        [ActionName("deptTaskUpdate")]
        public IHttpActionResult deptTaskUpdate(int param, otherDepartmentTaskObject otherDepartmentTaskParam)
        {
            OTHERDEPARTMENTTASK otherDeptTask = db.OTHERDEPARTMENTTASKs.Find(param);
            otherDeptTask.PROJECTID = otherDepartmentTaskParam.projectId;
            otherDeptTask.DEPARTMENTID = otherDepartmentTaskParam.departmentId;
            otherDeptTask.DEPARTMENTNAME = otherDepartmentTaskParam.departmentName;
            otherDeptTask.TASKNAME = otherDepartmentTaskParam.taskName;
            otherDeptTask.PICID = otherDepartmentTaskParam.picId;
            otherDeptTask.PICNAME = otherDepartmentTaskParam.picName;
            otherDeptTask.PICEMAIL = otherDepartmentTaskParam.picEmail;
            otherDeptTask.PLANDATE = otherDepartmentTaskParam.planDate;
            otherDeptTask.ACTUALDATE = otherDepartmentTaskParam.actualDate;
            otherDeptTask.REMARK = otherDepartmentTaskParam.remark;
            otherDeptTask.STATUS = otherDepartmentTaskParam.status;
            otherDeptTask.UPDATEDDATE = DateTime.Now;
            otherDeptTask.UPDATEDBY = GetCurrentNTUserId();

            db.Entry(otherDeptTask).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(otheDeptTaskById(otherDeptTask.OID));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OTHERDEPARTMENTTASKExists(int id)
        {
            return db.OTHERDEPARTMENTTASKs.Count(e => e.OID == id) > 0;
        }
    }
}