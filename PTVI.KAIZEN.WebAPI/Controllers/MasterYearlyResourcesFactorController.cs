﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MasterYearlyResourcesFactorController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/MasterYearlyResourcesFactor
        public IQueryable<masterYearlyResourcesFactorObject> GetMasterYearlyResourcesFactor()
        {
            return db.MASTERYEARLYRESOURCESFACTORs.Select(s => new masterYearlyResourcesFactorObject
            {
                id = s.OID,
                year = s.YEAR,
                levelCode = s.LEVELCODE,
                levelDescription = s.LEVELDESCRIPTION,
                minRange = s.MINRANGE,
                maxRange = s.MAXRANGE,
                engineerDesignerFactor = s.ENGINEERDESIGNERFACTOR,
            });
        }

        [HttpGet]
        [ActionName("bylevelCode")]
        public IQueryable<masterYearlyResourcesFactorObject> bylevelCode(string param)
        {
            if(param=="0")
                return db.MASTERYEARLYRESOURCESFACTORs.Select(s => new masterYearlyResourcesFactorObject
                {
                    id = s.OID,
                    year = s.YEAR,
                    levelCode = s.LEVELCODE,
                    levelDescription = s.LEVELDESCRIPTION,
                    minRange = s.MINRANGE,
                    maxRange = s.MAXRANGE,
                    engineerDesignerFactor = s.ENGINEERDESIGNERFACTOR,
                });
            else
            return db.MASTERYEARLYRESOURCESFACTORs.Where(w => w.LEVELCODE== param).Select(s => new masterYearlyResourcesFactorObject
            {
                id = s.OID,
                year = s.YEAR,
                levelCode = s.LEVELCODE,
                levelDescription = s.LEVELDESCRIPTION,
                minRange = s.MINRANGE,
                maxRange = s.MAXRANGE,
                engineerDesignerFactor = s.ENGINEERDESIGNERFACTOR,
            });
        }

        [HttpGet]
        [ActionName("GetDistinctMasterYearlyResourcesFactorCode")]
        public IQueryable<object> GetDistinctMasterYearlyResourcesFactorCode(int param)
        {
            return db.MASTERYEARLYRESOURCESFACTORs.Where(w => w.YEAR==param).Select(s => new 
            {
                levelCode = s.LEVELCODE,
                levelDescription = s.LEVELDESCRIPTION,
            }).Distinct();
        }

        // GET: api/MasterYearlyResourcesFactor
        [ResponseType(typeof(masterYearlyResourcesFactorObject))]
        public IHttpActionResult GetMASTERYEARLYRESOURCESFACTOR(int id)
        {
            MASTERYEARLYRESOURCESFACTOR mASTERYEARLYRESOURCESFACTOR = db.MASTERYEARLYRESOURCESFACTORs.Find(id);
            if (mASTERYEARLYRESOURCESFACTOR == null)
            {
                return NotFound();
            }

            return Ok(mASTERYEARLYRESOURCESFACTOR);
        }

        // PUT: api/MasterYearlyResourcesFactor/5
        [ResponseType(typeof(masterYearlyResourcesFactorObject))]
        public IHttpActionResult PutMASTERYEARLYRESOURCESFACTORs(int id, masterYearlyResourcesFactorObject mASTERYEARLYRESOURCESFACTOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERYEARLYRESOURCESFACTOR.id)
            {
                return BadRequest();
            }

            db.Entry(mASTERYEARLYRESOURCESFACTOR).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERYEARLYRESOURCESFACTORExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MasterYearlyResourcesFactor/5
        [ResponseType(typeof(masterYearlyResourcesFactorObject))]
        public IHttpActionResult PostMasterYearlyResourcesFactor(MASTERYEARLYRESOURCESFACTOR MASTERYEARLYRESOURCESFACTOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MASTERYEARLYRESOURCESFACTORs.Add(MASTERYEARLYRESOURCESFACTOR);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { OID = MASTERYEARLYRESOURCESFACTOR.OID }, MASTERYEARLYRESOURCESFACTOR);
        }


        // DELETE: api/MasterYearlyResourcesFactor/5
        [ResponseType(typeof(masterYearlyResourcesFactorObject))]
        public IHttpActionResult DeleteMASTERYEARLYRESOURCESFACTOR(int id)
        {
            MASTERYEARLYRESOURCESFACTOR mASTERYEARLYRESOURCESFACTOR = db.MASTERYEARLYRESOURCESFACTORs.Find(id);
            if (mASTERYEARLYRESOURCESFACTOR == null)
            {
                return NotFound();
            }

            db.MASTERYEARLYRESOURCESFACTORs.Remove(mASTERYEARLYRESOURCESFACTOR);
            db.SaveChanges();

            return Ok(mASTERYEARLYRESOURCESFACTOR);
        }

        [HttpPut]
        [ActionName("updateMasterYearlyResourcesFactor")]
        public IHttpActionResult updateMasterYearlyResourcesFactor(int param, masterYearlyResourcesFactorObject mASTERYEARLYRESOURCESFACTORParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != mASTERYEARLYRESOURCESFACTORParam.id)
            {
                return BadRequest();
            }

            try
            {
                MASTERYEARLYRESOURCESFACTOR update = db.MASTERYEARLYRESOURCESFACTORs.Find(param);
                update.OID = mASTERYEARLYRESOURCESFACTORParam.id;
                update.YEAR = mASTERYEARLYRESOURCESFACTORParam.year;
                update.LEVELCODE = mASTERYEARLYRESOURCESFACTORParam.levelCode;
                update.LEVELDESCRIPTION = mASTERYEARLYRESOURCESFACTORParam.levelDescription;
                update.MINRANGE = mASTERYEARLYRESOURCESFACTORParam.minRange;
                update.MAXRANGE = mASTERYEARLYRESOURCESFACTORParam.maxRange;
                update.ENGINEERDESIGNERFACTOR = mASTERYEARLYRESOURCESFACTORParam.engineerDesignerFactor;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(mASTERYEARLYRESOURCESFACTORParam);
        }

        [HttpGet]
        [ActionName("getMaxMinYearly")]
        public object getMaxMinYearly(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERYEARLYRESOURCESFACTORs.Min(w => w.YEAR);
            var max = db.MASTERYEARLYRESOURCESFACTORs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }

        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterYearlyResourcesFactorObject> getByYear(int param)
        {
            return db.MASTERYEARLYRESOURCESFACTORs.Where(w => w.YEAR == param).Select(s => new masterYearlyResourcesFactorObject
            {
                id = s.OID,
                year = s.YEAR,
                levelCode = s.LEVELCODE,
                levelDescription = s.LEVELDESCRIPTION,
                minRange = s.MINRANGE,
                maxRange = s.MAXRANGE,
                engineerDesignerFactor = s.ENGINEERDESIGNERFACTOR,
            });
        }

        [HttpPost]
        [ActionName("addMasterYearlyResourcesFactor")]
        public IHttpActionResult addMasterYearlyResourcesFactor(string param, masterYearlyResourcesFactorObject masterYearlyResourcesFactorParam)
        {
            MASTERYEARLYRESOURCESFACTOR add = new MASTERYEARLYRESOURCESFACTOR();
            add.YEAR = masterYearlyResourcesFactorParam.year;
            add.LEVELCODE = masterYearlyResourcesFactorParam.levelCode;
            add.LEVELDESCRIPTION = masterYearlyResourcesFactorParam.levelDescription;
            add.MINRANGE = masterYearlyResourcesFactorParam.minRange;
            add.MAXRANGE = masterYearlyResourcesFactorParam.maxRange;
            add.ENGINEERDESIGNERFACTOR = masterYearlyResourcesFactorParam.engineerDesignerFactor;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERYEARLYRESOURCESFACTORs.Add(add);
            db.SaveChanges();

            masterYearlyResourcesFactorParam.id = add.OID;
            return Ok(masterYearlyResourcesFactorParam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MASTERYEARLYRESOURCESFACTORExists(int id)
        {
            return db.MASTERYEARLYRESOURCESFACTORs.Count(e => e.OID == id) > 0;
        }
    }
}