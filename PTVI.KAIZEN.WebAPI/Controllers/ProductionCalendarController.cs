﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProductionCalendarController : KAIZENController
    {

        [HttpGet]
        [ActionName("getCurrentWeek")]
        // To get current week
        public object getCurrentWeek(string param)
        {
            DateTime date = DateTime.Now;
            List<object> week = new List<object>();
            week.Add(GetWeekFromDate(date.ToString("yyyyMMdd")));
            return week;
        }

        [HttpGet]
        [ActionName("findAllYear")]
        public object findAllYear(string param)
        {
            List<Dictionary<string, int>> listYear = new List<Dictionary<string, int>>();

            var generalParam = db.GENERALPARAMETERs.Where(x => x.TITLE == "YEAR_DURATION").ToList();
            if (generalParam.Count > 0)
            {
                DateTime today = DateTime.Now;
                int durationYearFrom = today.Year - int.Parse(generalParam[0].VALUE);


                for (int i = today.Year; i > durationYearFrom; i--)
                {
                    Dictionary<string, int> year = new Dictionary<string, int>();
                    year.Add("PERIODYEAR", i);

                    listYear.Add(year);
                }
            }


            return listYear;

        }

        [HttpGet]
        [ActionName("getAllWeekInYear")]
        public object getAllWeekInYear(int param)
        {
            List<Dictionary<string, int>> listWeek = new List<Dictionary<string, int>>();
            int totalWeekInYear = 0;


            var generalParam = db.GENERALPARAMETERs.Where(x => x.TITLE == "BASE_YEAR").ToList();
            if (generalParam.Count > 0)
            {

                int baseYear = int.Parse(generalParam[0].VALUE);

                // per 6 tahun dari 2004, jumlah week adalah 53
                // selain itu 52
                int formulaForTotalWeek = (param - baseYear) % 6;
                if (formulaForTotalWeek == 0)
                {
                    totalWeekInYear = 53;
                }
                else
                {
                    totalWeekInYear = 52;
                }

                //for (int i = totalWeekInYear; i >= 1 ; i--)
                for (int i = 1; i <= totalWeekInYear; i++)
                {
                    Dictionary<string, int> week = new Dictionary<string, int>();
                    week.Add("PERIODWEEK", i);

                    listWeek.Add(week);
                }
            }


            //var result = db.PRODUCTIONCALENDARs.Where(x => x.PERIODYEAR == param).Select(x => new { x.PERIODWEEK });
            return listWeek;
        }

        [HttpGet]
        [ActionName("getNextWeekInYear")]
        public object getNextWeekInYear(string param)
        {
            string[] val = param.Split('|');
            int year = int.Parse(val[0]);
            int prevWeek = int.Parse(val[1]);

            List<Dictionary<string, int>> listWeek = new List<Dictionary<string, int>>();
            int totalWeekInYear = 0;


            var generalParam = db.GENERALPARAMETERs.Where(x => x.TITLE == "BASE_YEAR").ToList();
            if (generalParam.Count > 0)
            {

                int baseYear = int.Parse(generalParam[0].VALUE);

                // per 6 tahun dari 2004, jumlah week adalah 53
                // selain itu 52
                int formulaForTotalWeek = (year - baseYear) % 6;
                if (formulaForTotalWeek == 0)
                {
                    totalWeekInYear = 53;
                }
                else
                {
                    totalWeekInYear = 52;
                }

                //for (int i = totalWeekInYear; i >= prevWeek; i--)
                for (int i = prevWeek; i <= totalWeekInYear; i++)
                {
                    Dictionary<string, int> week = new Dictionary<string, int>();
                    week.Add("PERIODWEEK", i);

                    listWeek.Add(week);
                }
            }


            //var result = db.PRODUCTIONCALENDARs.Where(x => x.PERIODYEAR == param).Select(x => new { x.PERIODWEEK });
            return listWeek;
        }

        //[EdmFunction("PTVI.MP.WebAPI.Models.Store", "BEGINWEEK")]
        //public DateTime? PCBeginWeek(int? TheYear, int? WeekNumber)
        //{
        //    throw new NotSupportedException("Direct calls are not supported.");
        //}

        [HttpGet]
        [ActionName("GetDateOfTheWeek")]
        //This method return start date and end date of the selected year and week. example: GetDateOfTheWeek(year|week)
        public object GetDateOfTheWeek(string param)
        {
            var criterias = param.Split('|');
            if (criterias.Length == 0)
            {
                throw new ApplicationException("This action need at least one parameter to be supplied.");
            }

            DateTime start = (DateTime)ExecuteFunctionByParam("BEGINWEEK|" + param);
            DateTime end = (DateTime)ExecuteFunctionByParam("ENDWEEK|" + param);

            start = start.AddDays(1);

            var res = new
            {
                startDate = start.ToString("yyyyMMdd"),
                endDate = end.ToString("yyyyMMdd")
            };

            return res;
        }

        [HttpGet]
        [ActionName("GetWeekFromDate")]
        // To get week from date selected. Example: GetWeekFromDate(20131217) -> format date = yyyyMMdd
        public object GetWeekFromDate(string param)
        {
            //DateTime dt = DateTime.Parse(param);

            int resweek = (int)ExecuteFunctionByParam("WEEKOFDATE|'" + param + "'");

            var res = new { week = resweek };
            return res;
        }

        [HttpGet]
        [ActionName("GetFirstDayOfYear")]
        // To get week from date selected. Example: GetFirstDayOfYear(2013) -> format date = yyyy
        public object GetFirstDayOfYear(string param)
        {
            //DateTime dt = DateTime.Parse(param);

            DateTime dt = (DateTime)ExecuteFunctionByParam("FIRSTDAYOFYEAR|'" + param + "'");
            dt = dt.AddDays(1);

            var res = new { date = dt.ToString("yyyyMMdd") };
            return res;
        }

        [HttpGet]
        [ActionName("GetWeekFromMonth")]
        public object GetWeekFromMonth(string param)
        {
            string[] parSplit = param.Split('|');
            int year = int.Parse(parSplit[0]);
            int month = int.Parse(parSplit[1]);

            DateTime date = DateTime.MinValue;

            string code = string.Empty;
            var baseYearParam = db.GENERALPARAMETERs.Where(x => x.TITLE == "BASE_YEAR").ToList();
            if (baseYearParam.Count > 0)
            {
                int baseYear = int.Parse(baseYearParam[0].VALUE);
                // per 6 tahun dari 2004, jumlah week adalah 53
                // selain itu 52
                int formulaForTotalWeek = (year - baseYear) % 6;
                if (formulaForTotalWeek == 0)
                    code = "WEEK_DURATION_53";
                else
                    code = "WEEK_DURATION_52";
            }

            int startWeekVal = 1;
            int endWeekVal = 0;
            var weekPattern = db.GENERALPARAMETERs.Where(x => x.TITLE == code).ToList();
            if (weekPattern.Count > 0)
            {
                string pattern = weekPattern[0].VALUE;
                string[] val = pattern.Split(',');
                for (int i = 0; i < month - 1; i++)
                {
                    startWeekVal += int.Parse(val[i]);
                }

                int lengthWeek = int.Parse(val[month - 1]); //knp dikurangi 1 karena index mulai dari 0 dan month mulai dari 1
                endWeekVal = startWeekVal + lengthWeek - 1;
            }

            var result = new { startWeek = startWeekVal, endWeek = endWeekVal };
            return result;
        }

        [HttpGet]
        [ActionName("GetWeekFromMonthResetValue")]
        public object GetWeekFromMonthResetValue(string param)
        {
            string[] parSplit = param.Split('|');
            int year = int.Parse(parSplit[0]);
            int month = int.Parse(parSplit[1]);

            DateTime date = DateTime.MinValue;

            string code = string.Empty;
            var baseYearParam = db.GENERALPARAMETERs.Where(x => x.TITLE == "BASE_YEAR").ToList();
            if (baseYearParam.Count > 0)
            {
                int baseYear = int.Parse(baseYearParam[0].VALUE);
                // per 6 tahun dari 2004, jumlah week adalah 53
                // selain itu 52
                int formulaForTotalWeek = (year - baseYear) % 6;
                if (formulaForTotalWeek == 0)
                    code = "WEEK_DURATION_53";
                else
                    code = "WEEK_DURATION_52";
            }

            int startWeekVal = 1;
            int endWeekVal = 0;
            var weekPattern = db.GENERALPARAMETERs.Where(x => x.TITLE == code).ToList();
            if (weekPattern.Count > 0)
            {
                string pattern = weekPattern[0].VALUE;
                string[] val = pattern.Split(',');
                for (int i = 0; i < month - 1; i++)
                {
                    startWeekVal += int.Parse(val[i]);
                }

                int lengthWeek = int.Parse(val[month - 1]); //knp dikurangi 1 karena index mulai dari 0 dan month mulai dari 1
                endWeekVal = startWeekVal + lengthWeek - 1;
            }

            // reset value week dari 1
            endWeekVal = (endWeekVal - startWeekVal + 1);
            startWeekVal = 1;

            var result = new { startWeek = startWeekVal, endWeek = endWeekVal };
            return result;
        }

        [HttpGet]
        [ActionName("ExecuteFunctionByParam")]
        private object ExecuteFunctionByParam(string param)
        {
            // param == qrycode|par1name=par1value|par2name=par2value
            var criterias = param.Split('|');
            if (criterias.Length == 0)
                throw new ApplicationException("This action need at least one parameter to be supplied.");

            string functionName = criterias[0];

            String fParam = String.Empty;
            for (int i = 1; i < criterias.Length; i++)
            {
                if (fParam != "")
                {
                    fParam += ",";
                }

                fParam += criterias[i];
            }

            string qry = "select dbo." + functionName + "(" + fParam + ")";
            // execute query and store to datatable
            string connectionString = db.Database.Connection.ConnectionString;
            //System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(qry, connectionString);

            var conn = db.Database.Connection;
            var dc = conn.CreateCommand();
            dc.CommandText = qry;
            dc.Connection.Open();
            dc.CommandType = CommandType.Text;
            //dc.Parameters.AddWithValue("@TheYear", 2013);
            object res = dc.ExecuteScalar();

            conn.Close();

            return res;
        }
    }
}
