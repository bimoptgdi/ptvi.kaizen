﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using Microsoft.Data.OData;
using System.Configuration;
using System.Web;
using System.Web.Configuration;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class VendorsController : ODataController
    {
        private KAIZENController db = new KAIZENController();

        private VALE_KAIZENEntities controller = new VALE_KAIZENEntities();

        [EnableQuery]
        public IQueryable<VendorObject> Get()
        {
            try
            {
                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {
                    List<VendorObject> result = controller.GetListFromExternalData<VendorObject>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_VENDORS"); // untuk production/development server

                    return result.Where(d => d.vendor_Code != "-").AsQueryable<VendorObject>();
                }
            }
            catch (ODataException ex)
            {
                return null;
            }

            return null;

        }
    }
}
