﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class RoleController : KAIZENController
    {
        // GET api/Role
        public IEnumerable<ROLE> GetROLEs()
        {
            IEnumerable<ROLE> listRole = db.ROLEs;
            foreach (ROLE role in listRole)
            {
                IEnumerable<USERROLE> listUserRole = role.USERROLEs;
                foreach (USERROLE userRole in listUserRole)
                {
                    USER user = userRole.USER;
                    var temp1 = user.USERROLEs;
                    user.USERROLEs = null;
                }

                IEnumerable<ROLEACCESSRIGHT> listRoleAccessRight = role.ROLEACCESSRIGHTs;
                foreach (ROLEACCESSRIGHT roleAccessRight in listRoleAccessRight)
                {
                    ACCESSRIGHT accessRight = roleAccessRight.ACCESSRIGHT;
                    var temp2 = accessRight.ROLEACCESSRIGHTs;
                    accessRight.ROLEACCESSRIGHTs = null;

                    var temp3 = roleAccessRight.ROLE;
                    roleAccessRight.ROLE = null;
                    ACCESSRIGHT accessRight2 = roleAccessRight.ACCESSRIGHT;
                    if (accessRight2 != null)
                    {
                        var temp4 = accessRight2.ROLEACCESSRIGHTs;
                        accessRight2.ROLEACCESSRIGHTs = null;
                        IEnumerable<MENUACCESSRIGHT> listMenuAccessRight = accessRight2.MENUACCESSRIGHTs;
                        foreach (MENUACCESSRIGHT menuAccessRight in listMenuAccessRight)
                        {
                            var temp5 = menuAccessRight.ACCESSRIGHT;
                            menuAccessRight.ACCESSRIGHT = null;

                        }
                    }
                }
            }
            return listRole.AsEnumerable();
        }

        // GET api/Role/5
        public ROLE GetROLE(int id)
        {
            ROLE role = db.ROLEs.Find(id);
            if (role == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            IEnumerable<USERROLE> listUserRole = role.USERROLEs;
            foreach (USERROLE userRole in listUserRole)
            {
                USER user = userRole.USER;
                var temp1 = user.USERROLEs;
                user.USERROLEs = null;
            }

            IEnumerable<ROLEACCESSRIGHT> listRoleAccessRight = role.ROLEACCESSRIGHTs;
            foreach (ROLEACCESSRIGHT roleAccessRight in listRoleAccessRight)
            {
                ACCESSRIGHT accessRight = roleAccessRight.ACCESSRIGHT;
                var temp2 = accessRight.ROLEACCESSRIGHTs;
                accessRight.ROLEACCESSRIGHTs = null;
            }
            return role;
        }

        // PUT api/Role/5
        public HttpResponseMessage PutROLE(int id, ROLE role)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            //}

            if (id != role.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            if (id == role.OID)
            {
                var existing = db.ROLEs.Find(role.OID);
                existing.DESCRIPTION = role.DESCRIPTION;
                existing.UPDATEDBY = GetCurrentNTUserId();
                existing.UPDATEDDATE = DateTime.Now;

                db.Entry(existing).State = EntityState.Modified;

                foreach (USERROLE USERROLE in role.USERROLEs)
                {
                    if (USERROLE.OID == 0)
                    {
                        USERROLE newUserRole = new USERROLE();
                        newUserRole.ROLEID = role.OID;

                        USER user = db.USERs.FirstOrDefault(d => d.BADGENO == USERROLE.USER.BADGENO);
                        if (user != null)
                        {
                            newUserRole.USERID = user.OID;
                            newUserRole.USER = null;
                        }
                        else
                        {
                            USER tempUser = USERROLE.USER;
                            tempUser.ISACTIVE = true;
                            tempUser.CREATEDDATE = DateTime.Now;
                            tempUser.UPDATEDDATE = DateTime.Now;
                            tempUser.CREATEDBY = GetCurrentNTUserId();

                            newUserRole.USER = USERROLE.USER;
                        }                                            

                        db.USERROLEs.Add(newUserRole);
                    }
                }

                foreach (ROLEACCESSRIGHT role_access in role.ROLEACCESSRIGHTs)
                {
                    if (role_access.OID == 0)
                    {
                        ROLEACCESSRIGHT newRoleAccess = new ROLEACCESSRIGHT();
                        newRoleAccess.ROLEID = role.OID;
                        newRoleAccess.ACCESSRIGHTID = role_access.ACCESSRIGHT.OID;
                        newRoleAccess.ACCESSRIGHT = null;

                        db.ROLEACCESSRIGHTs.Add(newRoleAccess);
                    }                   
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Role
        public HttpResponseMessage PostROLE(ROLE role)
        {
            //if (ModelState.IsValid)
            //{
            role.CREATEDDATE = DateTime.Now;
            role.UPDATEDDATE = DateTime.Now;
            role.CREATEDBY = GetCurrentNTUserId();

            foreach (USERROLE USERROLE in role.USERROLEs)
            {
                //cek  apakah user udah terdaftar di tabel
                USER user = db.USERs.FirstOrDefault(d => d.BADGENO == USERROLE.USER.BADGENO);
                if (user != null)
                {
                    USERROLE.USERID = user.OID;
                    USERROLE.USER = null;
                }
                else
                {
                    USER tempUser = USERROLE.USER;
                    tempUser.ISACTIVE = true;
                    tempUser.CREATEDDATE = DateTime.Now;
                    tempUser.UPDATEDDATE = DateTime.Now;
                    tempUser.CREATEDBY = GetCurrentNTUserId();
                }
            }

            foreach (ROLEACCESSRIGHT role_access in role.ROLEACCESSRIGHTs)
            {
                role_access.ACCESSRIGHTID = role_access.ACCESSRIGHT.OID;
                role_access.ACCESSRIGHT = null;
            }

            db.ROLEs.Add(role);
            db.SaveChanges();

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, role);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = role.OID }));
            return response;
            //}
            //else
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            //}
        }

        // DELETE api/Role/5
        public HttpResponseMessage DeleteROLE(int id)
        {
            ROLE role = db.ROLEs.Find(id);
            if (role == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (role.USERROLEs.Count() > 0)
            {
                for (int i = role.USERROLEs.Count() - 1; i > -1; i--)
                {
                    int userid = role.USERROLEs.ElementAt(i).USERID;
                    db.USERROLEs.Remove(role.USERROLEs.ElementAt(i));

                    //cek apakah user masih digunakan di tabel USERROLEs
                    IQueryable<USERROLE> listMenu = db.USERROLEs.Where(d => d.USERID == userid);
                    if (listMenu.Count() == 1)
                    {
                        //del user
                        USER delUser = db.USERs.Find(userid);
                        db.USERs.Remove(delUser);
                    }

                }
            }

            if (role.ROLEACCESSRIGHTs.Count() > 0)
            {
                for (int i = role.ROLEACCESSRIGHTs.Count() - 1; i > -1; i--)
                {
                    db.ROLEACCESSRIGHTs.Remove(role.ROLEACCESSRIGHTs.ElementAt(i));
                }
            }

            db.ROLEs.Remove(role);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, role);
        }

        [HttpGet]
        [ActionName("GetUser")]
        public IEnumerable<USERROLE> GetUser(int param)
        {
            ROLE role = db.ROLEs.Find(param);
            IEnumerable<USERROLE> listUserRole = role.USERROLEs;
            foreach (USERROLE userRole in listUserRole)
            {
                USER user = userRole.USER;
                var temp = user.USERROLEs;
                user.USERROLEs = null;

                IEnumerable<ROLEACCESSRIGHT> listRoleAccessRight = userRole.ROLE.ROLEACCESSRIGHTs;
                foreach (ROLEACCESSRIGHT roleAccessRight in listRoleAccessRight)
                {
                    var temp3 = roleAccessRight.ROLE;
                    roleAccessRight.ROLE = null;
                    ACCESSRIGHT accessRight = roleAccessRight.ACCESSRIGHT;
                    if (accessRight != null)
                    {
                        var temp4 = accessRight.ROLEACCESSRIGHTs;
//                        accessRight.ROLEACCESSRIGHTs = null;
                        IEnumerable<ROLEACCESSRIGHT> listMenuAccessRight = accessRight.ROLEACCESSRIGHTs;
                        foreach (ROLEACCESSRIGHT menuAccessRight in listMenuAccessRight)
                        {
                            var temp5 = menuAccessRight.ACCESSRIGHT;
                            menuAccessRight.ACCESSRIGHT = null;

                        }
                    }
                }
                //var temp3 = userRole.ROLE.ROLEACCESSRIGHTs;
                //userRole.ROLE.ROLEACCESSRIGHTs = null;

                ROLE role2 = userRole.ROLE;
                var temp2 = role2.USERROLEs;
                role2.USERROLEs = null;
            }

            return listUserRole.AsEnumerable();
        }

        [HttpGet]
        [ActionName("GetAccessRight")]
        public IEnumerable<ROLEACCESSRIGHT> GetAccessRight(int param)
        {
            ROLE role = db.ROLEs.Find(param);
            IEnumerable<ROLEACCESSRIGHT> listRoleAccessRight = role.ROLEACCESSRIGHTs;
            foreach (ROLEACCESSRIGHT roleAccessRight in listRoleAccessRight)
            {
                var temp3 = roleAccessRight.ROLE;
                roleAccessRight.ROLE = null;
                ACCESSRIGHT accessRight = roleAccessRight.ACCESSRIGHT;
                if (accessRight != null)
                {
                    var temp4 = accessRight.ROLEACCESSRIGHTs;
                    accessRight.ROLEACCESSRIGHTs = null;
                    var temp5 = accessRight.MENUACCESSRIGHTs;
                    accessRight.MENUACCESSRIGHTs = null;
                    
                }
            }
            return role.ROLEACCESSRIGHTs.AsEnumerable();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        [ActionName("UpdateROLE")]
        public HttpResponseMessage UpdateROLE(int param, [FromBody] GeneralParam generalParam)
        {
            ROLE role = generalParam.role;
            List<USER> listUser = generalParam.user;
            //List<ACCESSRIGHT> listAccessRight = generalParam.accessright;
            List<ROLEACCESSRIGHT> listAccessRight = generalParam.roleaccessright;

            if (param != role.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            else
            {
                //save edit role
                var existing = db.ROLEs.Find(role.OID);
                existing.DESCRIPTION = role.DESCRIPTION;
                existing.ROLETYPE = role.ROLETYPE;
                existing.UPDATEDBY = GetCurrentNTUserId();
                existing.UPDATEDDATE = DateTime.Now;

                db.Entry(existing).State = EntityState.Modified;

                if (listUser != null)
                {
                    foreach (USER user in listUser)
                    {
                        //USER existingUser = db.USERs.Find(user.OID);
                        USER existingUser = db.USERs.FirstOrDefault(d => d.BADGENO == user.BADGENO);
                        if (existingUser != null) // user exist
                        {
                            // jika sudah ada di tabel user, save edit
                            existingUser.BADGENO = user.BADGENO;
                            existingUser.EMAIL = user.EMAIL;
                            existingUser.NTUSERID = user.NTUSERID;
                            existingUser.USERNAME = user.USERNAME;
                            existingUser.UPDATEDBY = GetCurrentNTUserId();
                            existingUser.UPDATEDDATE = DateTime.Now;

                            db.Entry(existingUser).State = EntityState.Modified;

                            // cek Role pada user tsb, jika role tsb belum ada, save.
                            USERROLE userRole = existingUser.USERROLEs.FirstOrDefault(d => d.ROLEID == role.OID);
                            if (userRole == null)
                            {
                                USERROLE newUserRole = new USERROLE();
                                newUserRole.ROLEID = role.OID;
                                newUserRole.USERID = existingUser.OID;
                                newUserRole.USER = existingUser;

                                db.USERROLEs.Add(newUserRole);
                            }
                        }
                        else
                        {
                            // jika belum ada di tabel user, save tabel user
                            user.CREATEDBY = GetCurrentNTUserId();
                            user.CREATEDDATE = DateTime.Now;
                            user.UPDATEDBY = GetCurrentNTUserId();
                            user.UPDATEDDATE = DateTime.Now;
                            user.ISACTIVE = true;

                            db.USERs.Add(user);

                            // pasti belum ada di tabel userrole, save tabel userrole
                            var uRole = user.USERROLEs.Where(d => d.ROLEID == role.OID);
                            if (uRole.Count() == 0)
                            {
                                USERROLE userRole = new USERROLE();
                                userRole.ROLEID = role.OID;
                                userRole.USERID = user.OID;
                                userRole.USER = user;
                                db.USERROLEs.Add(userRole);
                            }

                        }
                    }
                }

                if (listAccessRight != null)
                {
                    foreach (ROLEACCESSRIGHT accessRight in listAccessRight)
                    {
                        ACCESSRIGHT existingAccessRight = db.ACCESSRIGHTs.Find(accessRight.ACCESSRIGHTID);
                        if (existingAccessRight != null)
                        {
                            ROLEACCESSRIGHT roleAccess = existingAccessRight.ROLEACCESSRIGHTs.FirstOrDefault(d => d.ROLEID == accessRight.ROLEID);
                            if (roleAccess == null)
                            {
                                //save new ROLEACCESSRIGHT
                                ROLEACCESSRIGHT newRoleAccess = new ROLEACCESSRIGHT();
                                newRoleAccess.ROLEID = role.OID;
                                newRoleAccess.ACCESSRIGHTID = existingAccessRight.OID;
                                newRoleAccess.ACCESSRIGHT = existingAccessRight;
                                newRoleAccess.ISREADONLY = true;
                                db.ROLEACCESSRIGHTs.Add(newRoleAccess);
                            }else
                            {
                                if ((roleAccess.ISREADONLY != accessRight.ISREADONLY) && (roleAccess.OID==accessRight.OID))
                                {
                                    roleAccess.ISREADONLY = accessRight.ISREADONLY;
                                    db.Entry(roleAccess).State = EntityState.Modified;
                                }
                            }
                                
                        }
                    }
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("FindRoleByBadgeNo")]
        public ROLE FindRoleByBadgeNo(string param)
        {

            ROLE role = db.ROLEs.FirstOrDefault(d => d.USERROLEs.Any(e => e.USER.BADGENO == param));
            if (role != null)
            {
                var temp1 = role.USERROLEs;
                role.USERROLEs = null;
                var temp2 = role.ROLEACCESSRIGHTs;
                role.ROLEACCESSRIGHTs = null;

                return role;
            }

            return null;
        }
        [HttpGet]
        [ActionName("FindRolesByBadgeNo")]
        public object FindRolesByBadgeNo (string param)
        {
            var userId = db.USERs.Where(x => x.BADGENO == param).Select(y=>y.OID).FirstOrDefault();
            var roleIds = db.USERROLEs.Where(x => x.USERID == userId).Select(y=>y.ROLEID).ToList();
            List<string> roles = new List<string>();
            foreach (int id in roleIds) {
                roles.Add(db.ROLEs.Find(id).ROLENAME);
            }
            return roles;
        }
    }

    
}