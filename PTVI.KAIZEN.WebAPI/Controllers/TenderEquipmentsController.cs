﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TenderEquipmentsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TenderEquipments
        public IQueryable<TENDEREQUIPMENT> GetTENDEREQUIPMENTs()
        {
            return db.TENDEREQUIPMENTs;
        }

        // GET: api/TenderEquipments/5
        [ResponseType(typeof(TENDEREQUIPMENT))]
        public IHttpActionResult GetTENDEREQUIPMENT(int id)
        {
            TENDEREQUIPMENT tENDEREQUIPMENT = db.TENDEREQUIPMENTs.Find(id);
            if (tENDEREQUIPMENT == null)
            {
                return NotFound();
            }

            return Ok(tENDEREQUIPMENT);
        }

        // PUT: api/TenderEquipments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDEREQUIPMENT(int id, TENDEREQUIPMENT tENDEREQUIPMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDEREQUIPMENT.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDEREQUIPMENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDEREQUIPMENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TenderEquipments
        [ResponseType(typeof(TENDEREQUIPMENT))]
        public IHttpActionResult PostTENDEREQUIPMENT(TENDEREQUIPMENT tENDEREQUIPMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDEREQUIPMENTs.Add(tENDEREQUIPMENT);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDEREQUIPMENT.OID }, tENDEREQUIPMENT);
        }

        // DELETE: api/TenderEquipments/5
        [ResponseType(typeof(TENDEREQUIPMENT))]
        public IHttpActionResult DeleteTENDEREQUIPMENT(int id)
        {
            TENDEREQUIPMENT tENDEREQUIPMENT = db.TENDEREQUIPMENTs.Find(id);
            if (tENDEREQUIPMENT == null)
            {
                return NotFound();
            }

            db.TENDEREQUIPMENTs.Remove(tENDEREQUIPMENT);
            db.SaveChanges();

            return Ok(tENDEREQUIPMENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TENDEREQUIPMENTExists(int id)
        {
            return db.TENDEREQUIPMENTs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("TenderEquipmentById")]
        public tenderEquipmentObject TenderEquipmentById(int param)
        {
            return db.TENDEREQUIPMENTs.Where(w => w.OID == param).Select(s => new tenderEquipmentObject {
                id = s.OID,
                tenderId = s.TENDERID,
                equipment = s.EQUIPMENT,
                quantity = s.QUANTITY,
                status = s.STATUS
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("TenderEquipmentsByTenderId")]
        public IEnumerable<tenderEquipmentObject> TenderEquipmentsByTenderId(int param)
        {
            return db.TENDEREQUIPMENTs.Where(w => w.TENDERID == param).Select(s => new tenderEquipmentObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                equipment = s.EQUIPMENT,
                quantity = s.QUANTITY,
                status = s.STATUS
            });
        }

        [HttpPut]
        [ActionName("TenderEquipmentSubmit")]
        public IHttpActionResult TenderEquipmentSubmit(int param, tenderEquipmentObject equipmentParam)
        {
            TENDEREQUIPMENT equipment = new TENDEREQUIPMENT();
            if (equipmentParam.id > 0)
            {
                equipment = db.TENDEREQUIPMENTs.Find(equipmentParam.id);
                equipment.UPDATEDBY = GetCurrentNTUserId();
                equipment.UPDATEDDATE = DateTime.Now;
            } else
            {
                equipment.CREATEDBY = GetCurrentNTUserId();
                equipment.CREATEDDATE = DateTime.Now;
                equipment.TENDERID = param;
            }
            equipment.EQUIPMENT = equipmentParam.equipment;
            equipment.QUANTITY = equipmentParam.quantity;
            equipment.STATUS = equipmentParam.status;

            if (equipmentParam.id > 0)
                db.Entry(equipment).State = EntityState.Modified;
            else
                db.Entry(equipment).State = EntityState.Added;
            db.SaveChanges();

            return Ok(TenderEquipmentById(equipment.OID));
        }
    }
}