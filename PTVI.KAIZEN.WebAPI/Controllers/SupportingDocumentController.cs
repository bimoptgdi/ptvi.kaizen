﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class SupportingdocumentController : KAIZENController
    {
        

        // GET: api/supportingdocument
        public IQueryable<SUPPORTINGDOCUMENT> GetSUPPORTINGDOCUMENTs()
        {
            return db.SUPPORTINGDOCUMENTs;
        }

        // GET: api/supportingdocument/5
        public SUPPORTINGDOCUMENT GetSUPPORTINGDOCUMENT(int id)
        {
            SUPPORTINGDOCUMENT supportingdocument = db.SUPPORTINGDOCUMENTs.Find(id);
            if (supportingdocument == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return supportingdocument;
        }

        // PUT: api/supportingdocument/5
        public HttpResponseMessage PutSUPPORTINGDOCUMENT(int id, SUPPORTINGDOCUMENT supportingDocument)
        {
            if (!ModelState.IsValid && id != supportingDocument.OID)
            {
                db.Entry(supportingDocument).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!supportingdocumentExists(id))
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }



            return Request.CreateResponse(HttpStatusCode.OK, supportingDocument);
        }

        // POST: api/supportingdocument
        public HttpResponseMessage PostSUPPORTINGDOCUMENT(SUPPORTINGDOCUMENT supportingDocument)
        {
            if (ModelState.IsValid)
            {
                db.SUPPORTINGDOCUMENTs.Add(supportingDocument);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, supportingDocument);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = supportingDocument.OID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/supportingdocument/5
        public HttpResponseMessage DeleteSUPPORTINGDOCUMENT(int id)
        {
            SUPPORTINGDOCUMENT supportingDocument = db.SUPPORTINGDOCUMENTs.Find(id);
            if (supportingDocument == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.SUPPORTINGDOCUMENTs.Remove(supportingDocument);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, supportingDocument);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool supportingdocumentExists(int id)
        {
            return db.SUPPORTINGDOCUMENTs.Count(e => e.OID == id) > 0;
        }

        // ex. param = MENTORING|6  (TYPE|REQUESTID)
        // this method will return refdocid|filename
        [HttpGet]
        [ActionName("findSuppDocByType")]
        public object findSuppDocByType(String param)
        {
            String result = String.Empty;
            if (param.Split('|').Count() > 1)
            {
                string typeName = param.Split('|')[0];
                int typeID;
                if (int.TryParse(param.Split('|')[1], out typeID))
                {
                    string strSuppDoc = "";

                    IEnumerable<SUPPORTINGDOCUMENT> listSuppDoc =
                        db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName && x.TYPEID == typeID);
                    foreach (SUPPORTINGDOCUMENT suppDoc in listSuppDoc)
                    {
                        strSuppDoc += suppDoc.REFDOCID + "|" + suppDoc.FILENAME + ",";
                    }
                    // buang koma terakhir
                    if (strSuppDoc != "")
                    {
                        strSuppDoc = strSuppDoc.Remove(strSuppDoc.Count() - 1, 1);
                    }
                    result = strSuppDoc;
                }
            }
            var v = new { SUPPDOC = result };
            return v;
        }

        // ex. param = DS|6 (TYPE|REQUESTID)
        [HttpGet]
        [ActionName("findSuppDocByTypeNameAndID")]
        public IEnumerable<SUPPORTINGDOCUMENT> findSuppDocByTypeNameAndID(String param)
        {
            if (param.Split('|').Count() > 1)
            {
                string typeName = param.Split('|')[0];
                int typeID;
                if (int.TryParse(param.Split('|')[1], out typeID))
                {
                    IEnumerable<SUPPORTINGDOCUMENT> result =
                        db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName && x.TYPEID == typeID);
                    return result;
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest));
                }
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
        }

        // param: Supp Doc OID
        [HttpDelete]
        [ActionName("removeSupportingDocumentById")]
        public HttpResponseMessage removeSupportingDocumentById(string param)
        {
            int selectedSuppDocOID;
            if (int.TryParse(param, out selectedSuppDocOID))
            {
                //delete doc in share folder
                string result = string.Empty; //response == "OK" || response == "FILE IS NOT FOUND"
                List<GENERALPARAMETER> parameter = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DOCUMENT_REPOSITORY_CID).ToList();
                if (parameter.Count == 0)
                    throw new WebException("Configuration for document repository cid not found. Please contact administrator.");

                string parent = parameter[0].VALUE;

                try
                {
                    DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();
                    //delete supporting document
                    SUPPORTINGDOCUMENT supportingDocument = db.SUPPORTINGDOCUMENTs.Find(selectedSuppDocOID);
                    if (supportingDocument != null)
                    {
                        // delete file di share folder
                        // jika berhasil, delete juga di tabel supporting document
                        result = service.RemoveFile(int.Parse(supportingDocument.REFDOCID), supportingDocument.FILENAME, parent, supportingDocument.OID).ToUpper();
                        if (result == "OK" || result == "FILE IS NOT FOUND")
                        {
                            // delete di table supporting document
                            DeleteSUPPORTINGDOCUMENT(selectedSuppDocOID);

                            return Request.CreateResponse(HttpStatusCode.OK, supportingDocument);
                        }
                        else
                        {
                            // tidak jadi delete di tabel supporting document
                        }
                    }
                    else
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // param is what dev action name and its oid
        // ex. param = MENTORING|6
        [HttpDelete]
        [ActionName("removeAllsupportingdocumentByDevAction")]
        public bool removeAllsupportingdocumentByDevAction(string param)
        {
            bool canDelete = true;

            if (param.Split('|').Count() > 1)
            {
                string typeName = param.Split('|')[0];
                int typeID;
                if (int.TryParse(param.Split('|')[1], out typeID))
                {
                    //delete doc in share folder
                    string result = string.Empty; //response == "OK" || response == "FILE IS NOT FOUND"
                    List<GENERALPARAMETER> parameter = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DOCUMENT_REPOSITORY_CID).ToList();
                    if (parameter.Count == 0)
                        throw new WebException("Configuration for document repository cid not found. Please contact administrator.");

                    string parent = parameter[0].VALUE;

                    try
                    {
                        DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();
                        //delete supporting document
                        IEnumerable<SUPPORTINGDOCUMENT> listSuppDoc =
                            db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName && x.TYPEID == typeID);
                        foreach (SUPPORTINGDOCUMENT suppDoc in listSuppDoc)
                        {
                            result = service.RemoveFile(int.Parse(suppDoc.REFDOCID), suppDoc.FILENAME, parent, suppDoc.OID).ToUpper();
                            if (result == "OK" || result == "FILE IS NOT FOUND")
                            {
                                // delete di table supporting document
                                db.SUPPORTINGDOCUMENTs.Remove(suppDoc);
                            }
                            else
                            {
                                // jika ada yang gagal delete maka di flag agar tidak delete table coaching
                                canDelete = false;
                            }
                        }
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest));
                }
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return canDelete;
        }
        
    }
}