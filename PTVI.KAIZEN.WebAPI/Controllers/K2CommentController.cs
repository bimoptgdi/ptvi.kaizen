﻿using SourceCode.SmartObjects.Client;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class K2CommentController : KAIZENController
    {
        public List<CommentObject> CommentsByProcID(string param)
        {
            // TODO: Replace these placeholder values with values for your environment 
            string _user = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserName;
            string _domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserDomain;
            string _serverName = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ServerName;
            string _password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserPassword;
            uint _port = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2MgmtPort;
            string _securityLabel = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2SecurityLabel;

            SourceCode.SmartObjects.Client.SmartObjectClientServer serverName = new SmartObjectClientServer();
            SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();

            // build a connection string 
            connectionString.Authenticate = true;
            connectionString.Host = _serverName;
            connectionString.Integrated = true;
            connectionString.IsPrimaryLogin = true;
            connectionString.Port = _port;
            connectionString.UserID = _user;
            connectionString.WindowsDomain = _domain;
            connectionString.Password = _password;
            connectionString.SecurityLabelName = _securityLabel;
            serverName.CreateConnection();
            serverName.Connection.Open(connectionString.ToString());

            //var wfCommentSetting = db2.general_parameter.Find(Constants.COMMENT_SMARTOBJECT_NAME);
            var wfCommentSetting = "Workflow_Comment_2;Get_List;Process_Instance_ID";
            var commentSettings = wfCommentSetting.Split(';');
            var smartObjectName = commentSettings[0];
            var methodName = commentSettings[1];
            var keyPropertyName = commentSettings[2];

            // get a handle to the "Employee" SmartObject 
            SmartObject smartObject = serverName.GetSmartObject(smartObjectName);

            // specify which method will be called 
            smartObject.MethodToExecute = methodName;

            smartObject.Properties[keyPropertyName].Value = param;

            // call the method 
            SmartObjectList smoList = serverName.ExecuteList(smartObject);

            List<CommentObject> results = new List<CommentObject>();
            foreach (SmartObject smo in smoList.SmartObjectsList)
            {
                CommentObject commentObj = new CommentObject()
                {
                    Comment = smo.Properties["Comment"].Value,
                    CreatedBy = smo.Properties["Created_By"].Value,
                    CreatedDate = Convert.ToDateTime(smo.Properties["Created_Date"].Value, CultureInfo.InvariantCulture)
                };
                results.Add(commentObj);
            }

            return results;
        }

        [HttpGet]
        [ActionName("FindCommentsByProcID")]
        public IHttpActionResult FindCommentsByProcID(string param)
        {
            return Ok(CommentsByProcID(param));
        }
    }
}

public class CommentObject
{
    public String Comment { get; set; }
    public Nullable<System.DateTime> CreatedDate { get; set; }
    public String CreatedBy { get; set; }
}
