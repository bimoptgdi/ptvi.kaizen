﻿using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTVI.KAIZEN.WebAPI.PSI_ProjectWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ShiftController : KAIZENController
    {
        [HttpGet]
        [ActionName("masterShiftByProjectId")]
        public IQueryable<shiftObject> masterShiftByProjectId(int param)
        {
            var result = db.SHIFTs.Where(x => x.PROJECTID == param).Select(x => new shiftObject()
            {
                id = x.OID,
                shiftName = x.SHIFTNAME,
                startTime = x.STARTTIME,
                endTime = x.ENDTIME,
                createdDate = x.CREATEDDATE,
                createdBy = x.CREATEDBY,
                updatedDate = x.UPDATEDDATE,
                updatedBy = x.UPDATEDBY,
                projectID = x.PROJECTID
            });

            return result;
        }

        [HttpPost]
        [ActionName("PostmasterShift")]
        public HttpResponseMessage PostmasterShift(int param, shiftObject mSObj)
        {
            if (ModelState.IsValid)
            {
                var shift = new SHIFT()
                {
                    SHIFTNAME = mSObj.shiftName,
                    STARTTIME = mSObj.startTime,
                    ENDTIME = mSObj.endTime,
                    CREATEDDATE = DateTime.Now,
                    CREATEDBY = mSObj.createdBy,
                    UPDATEDDATE = DateTime.Now,
                    UPDATEDBY = mSObj.updatedBy,
                    PROJECTID = mSObj.projectID,
                };
                db.SHIFTs.Add(shift);
                db.SaveChanges();
                mSObj.id = shift.OID;

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, mSObj);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = mSObj.id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [ActionName("PutmasterShift")]
        public HttpResponseMessage PutmasterShift(int param, shiftObject mSObj)
        {
            if (ModelState.IsValid)
            {
                var shift = db.SHIFTs.Find(param);
                if (shift != null)
                {
                    
                    shift.SHIFTNAME = mSObj.shiftName;
                    shift.STARTTIME = mSObj.startTime;
                    shift.ENDTIME = mSObj.endTime;
                    shift.UPDATEDDATE = DateTime.Now;
                    shift.UPDATEDBY = mSObj.updatedBy;
                }

                db.Entry(shift).State = EntityState.Modified;

                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mSObj);

                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        //[HttpPut]
        //[ActionName("PutUserAssignmentObject")]
        //public HttpResponseMessage PutUserAssignmentObject(int param, shiftObject mSObj)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }

        //    if (param != mSObj.id)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);
        //    }

        //    var shiftObj = db.SHIFTs.Where(x => x.OID == mSObj.id).FirstOrDefault();
        //    shiftObj.SHIFTNAME = mSObj.shiftName;
        //    shiftObj.STARTTIME = mSObj.startTime;
        //    shiftObj.ENDTIME = mSObj.endTime;
        //    shiftObj.UPDATEDDATE = DateTime.Now;
        //    shiftObj.UPDATEDBY = mSObj.updatedBy;
        //    shiftObj.PROJECTID = mSObj.projectID;
        //    db.Entry(shiftObj).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, mSObj);
        //}

        [HttpDelete]
        [ActionName("DeletemasterShift")]
        public HttpResponseMessage DeletemasterShift(int param)
        {
            SHIFT sHIFT = db.SHIFTs.Find(param);
            if (sHIFT == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Shift Data Not Found");
            }

            db.SHIFTs.Remove(sHIFT);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, sHIFT);
        }

        [HttpDelete]
        [ActionName("removeProjectShift")]
        public HttpResponseMessage removeProjectShift(int param)
        {            
            PROJECTSHIFT ps = db.PROJECTSHIFTs.Find(param);
            if (ps == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Project Shift Data Not Found");
            }

            if (db.PROJECTTASKBYSHIFTs.Count(x=>x.ESTIMATIONSHIFTID == param && x.PROJECTID == ps.PROJECTID && x.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue) > 0)
            {
                throw new ApplicationException("Shift Target of this shift has been generated and already has value.");
            }

            // delete captured task of this shift
            db.Database.ExecuteSqlCommand("DELETE FROM [PROJECTTASKBYSHIFT] WHERE ESTIMATIONSHIFTID = " + param);           
            db.Database.ExecuteSqlCommand("DELETE FROM [MASTERTASKBASELINEBYSHIFT] WHERE ESTIMATIONSHIFTID = " + param);

            db.PROJECTSHIFTs.Remove(ps);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, ps);
        }

        [HttpGet]
        [ActionName("activeProjectShifts")]
        public IQueryable<projectShiftObject> activeProjectShifts(int param)
        {
            var result = db.PROJECTSHIFTs.Where(x => x.PROJECTID == param).OrderBy(x=>x.SHIFTID).Select(x => new projectShiftObject()
            {
                id = x.OID,
                shift = x.SHIFTID.ToString(),
                startDate = x.STARTDATE.Value,
                endDate = x.ENDDATE.Value,
                shiftTargetHasGenerated = db.PROJECTTASKBYSHIFTs.Count(y=>y.ESTIMATIONSHIFTID == x.OID && y.PERCENTCOMPLETEBASELINE.HasValue) > 0,
                actualPercentCompleteHasUpdated = db.PROJECTTASKBYSHIFTs.Count(y => y.ESTIMATIONSHIFTID == x.OID && y.PERCENTCOMPLETEBASELINE.HasValue && y.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue) > 0
            });

            return result;
        }

        [HttpGet]
        [ActionName("generateProjectShift")]
        public IQueryable<projectShiftObject> generateProjectShift(int projectID, string workingTimeType, string trackingMode)
        {
            var projectData = db.PROJECTs.Find(projectID);
            if (projectData == null)
            {
                throw new ApplicationException("Project not found.");
            }

            if (!string.IsNullOrEmpty(workingTimeType))
            {
                projectData.WORKINGTIMETYPE = workingTimeType;
                projectData.IPTTRACKINGMODE = trackingMode;
                projectData.UPDATEDBY = GetCurrentNTUserId();
                projectData.UPDATEDDATE = DateTime.Now;

                db.SaveChanges();
            }

            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectData.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

            var zeroLevelTask = projectDs.Task.Select("TASK_OUTLINE_LEVEL = 0").FirstOrDefault() as ProjectDataSet.TaskRow;

            var startDate = zeroLevelTask.IsTB_STARTNull() ? zeroLevelTask.TASK_START_DATE: zeroLevelTask.TB_START;
            var finishDate = zeroLevelTask.IsTB_FINISHNull() ? zeroLevelTask.TASK_FINISH_DATE: zeroLevelTask.TB_FINISH;

            var curDate = startDate;
            var weekStartDate = startDate;
            var shiftID = 1;
            var weekCounter = 0;
            var trackingModeValue = int.Parse(trackingMode.Split('_')[1]);

            while (curDate < finishDate)
            {
                if (trackingModeValue == 0)
                {
                    // daily
                    if (workingTimeType == constants.WORKINGTIME_5DAYS &&
                    (curDate.DayOfWeek == DayOfWeek.Saturday || curDate.DayOfWeek == DayOfWeek.Sunday))
                    {
                        curDate = curDate.AddDays(1);
                        continue;
                    }

                    if (curDate > finishDate)
                        curDate = finishDate;

                    foreach (var masterShift in projectData.SHIFTs)
                    {
                        db.PROJECTSHIFTs.Add(new Models.PROJECTSHIFT()
                        {
                            PROJECTID = projectData.OID,
                            SHIFTID = shiftID,
                            STARTDATE = curDate.Date.Add(masterShift.STARTTIME.Value),
                            ENDDATE = masterShift.ENDTIME.Value <= masterShift.STARTTIME.Value ? curDate.Date.AddDays(1).Add(masterShift.ENDTIME.Value) : curDate.Date.Add(masterShift.ENDTIME.Value),
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        });

                        shiftID++;
                    }

                    curDate = curDate.AddDays(1);
                }
                else
                {
                    // weekly                    
                    while(weekCounter < trackingModeValue)
                    {   
                        if (weekCounter == 0 && curDate.DayOfWeek == DayOfWeek.Monday)
                            weekStartDate = curDate;

                        if (workingTimeType == constants.WORKINGTIME_5DAYS && curDate.AddDays(1).DayOfWeek == DayOfWeek.Saturday)
                        {
                            weekCounter++;
                            if (weekCounter < trackingModeValue)
                                curDate = curDate.AddDays(1);
                        }
                        else if (workingTimeType == constants.WORKINGTIME_7DAYS && curDate.AddDays(1).DayOfWeek == DayOfWeek.Monday)
                        {
                            weekCounter++;
                            if (weekCounter < trackingModeValue)
                                curDate = curDate.AddDays(1);
                        }
                        else
                            curDate = curDate.AddDays(1);
                    }

                    if (curDate > finishDate)
                        curDate = finishDate;

                    foreach (var masterShift in projectData.SHIFTs)
                    {
                        db.PROJECTSHIFTs.Add(new Models.PROJECTSHIFT()
                        {
                            PROJECTID = projectData.OID,
                            SHIFTID = shiftID,
                            STARTDATE = weekStartDate.Date.Add(masterShift.STARTTIME.Value),
                            ENDDATE = masterShift.ENDTIME.Value <= masterShift.STARTTIME.Value ? curDate.Date.AddDays(1).Add(masterShift.ENDTIME.Value) : curDate.Date.Add(masterShift.ENDTIME.Value),
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        });

                        shiftID++;
                    }

                    weekCounter = 0;
                    curDate = curDate.AddDays(1);
                }
                
            }

            db.SaveChanges();

            return db.PROJECTSHIFTs.Where(x => x.PROJECTID == projectID).Select(x => new projectShiftObject()
            {
                id = x.OID,
                shift = x.SHIFTID.ToString(),
                startDate = x.STARTDATE.Value,
                endDate = x.ENDDATE.Value
            });
        }

        [HttpGet]
        [ActionName("synchronizeProjectShift")]
        public IQueryable<projectShiftObject> synchronizeProjectShift(int param)
        {
            var projectData = db.PROJECTs.Find(param);
            if (projectData == null)
            {
                throw new ApplicationException("Project not found.");
            }

            //if (db.PROJECTTASKBYSHIFTs.Count(x=>x.PROJECTID == param && x.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue) > 0)
            if (db.PROJECTTASKBYSHIFTs.Count(x => x.PROJECTID == param) > 0)
            {
                throw new ApplicationException("Cannot continue synchronizing project shift because shift target of this project has been generated.");
            }

            db.Database.ExecuteSqlCommand("DELETE FROM [PROJECTSHIFT] WHERE PROJECTID = " + param);

            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectData.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

            var zeroLevelTask = projectDs.Task.Select("TASK_OUTLINE_LEVEL = 0").FirstOrDefault() as ProjectDataSet.TaskRow;

            var startDate = zeroLevelTask.IsTB_STARTNull() ? zeroLevelTask.TASK_START_DATE : zeroLevelTask.TB_START;
            var finishDate = zeroLevelTask.IsTB_FINISHNull() ? zeroLevelTask.TASK_FINISH_DATE : zeroLevelTask.TB_FINISH;

            var curDate = startDate;
            var shiftID = 1;
            
            var weekStartDate = startDate;
            var weekCounter = 0;
            var trackingModeValue = string.IsNullOrEmpty(projectData.IPTTRACKINGMODE) ? 0: int.Parse(projectData.IPTTRACKINGMODE.Split('_')[1]);
            while (curDate < finishDate)
            {
                if (trackingModeValue == 0)
                {
                    // daily
                    if (projectData.WORKINGTIMETYPE == constants.WORKINGTIME_5DAYS &&
                    (curDate.DayOfWeek == DayOfWeek.Saturday || curDate.DayOfWeek == DayOfWeek.Sunday))
                    {
                        curDate = curDate.AddDays(1);
                        continue;
                    }

                    if (curDate > finishDate)
                        curDate = finishDate;

                    foreach (var masterShift in projectData.SHIFTs)
                    {
                        db.PROJECTSHIFTs.Add(new Models.PROJECTSHIFT()
                        {
                            PROJECTID = projectData.OID,
                            SHIFTID = shiftID,
                            STARTDATE = curDate.Date.Add(masterShift.STARTTIME.Value),
                            ENDDATE = masterShift.ENDTIME.Value <= masterShift.STARTTIME.Value ? curDate.Date.AddDays(1).Add(masterShift.ENDTIME.Value) : curDate.Date.Add(masterShift.ENDTIME.Value),
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        });

                        shiftID++;
                    }

                    curDate = curDate.AddDays(1);
                }
                else
                {
                    // weekly                    
                    while (weekCounter < trackingModeValue)
                    {
                        if (weekCounter == 0 && curDate.DayOfWeek == DayOfWeek.Monday)
                            weekStartDate = curDate;

                        if (projectData.WORKINGTIMETYPE == constants.WORKINGTIME_5DAYS && curDate.AddDays(1).DayOfWeek == DayOfWeek.Saturday)
                        {
                            weekCounter++;
                            if (weekCounter < trackingModeValue)
                                curDate = curDate.AddDays(1);
                        }
                        else if (projectData.WORKINGTIMETYPE == constants.WORKINGTIME_7DAYS && curDate.AddDays(1).DayOfWeek == DayOfWeek.Monday)
                        {
                            weekCounter++;
                            if (weekCounter < trackingModeValue)
                                curDate = curDate.AddDays(1);
                        }
                        else
                            curDate = curDate.AddDays(1);
                    }

                    if (curDate > finishDate)
                        curDate = finishDate;

                    foreach (var masterShift in projectData.SHIFTs)
                    {
                        db.PROJECTSHIFTs.Add(new Models.PROJECTSHIFT()
                        {
                            PROJECTID = projectData.OID,
                            SHIFTID = shiftID,
                            STARTDATE = weekStartDate.Date.Add(masterShift.STARTTIME.Value),
                            ENDDATE = masterShift.ENDTIME.Value <= masterShift.STARTTIME.Value ? curDate.Date.AddDays(1).Add(masterShift.ENDTIME.Value) : curDate.Date.Add(masterShift.ENDTIME.Value),
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        });

                        shiftID++;
                    }

                    weekCounter = 0;
                    curDate = curDate.AddDays(1);
                }

            }

            db.SaveChanges();

            return db.PROJECTSHIFTs.Where(x => x.PROJECTID == param).Select(x => new projectShiftObject()
            {
                id = x.OID,
                shift = x.SHIFTID.ToString(),
                startDate = x.STARTDATE.Value,
                endDate = x.ENDDATE.Value
            });
        }

        [HttpGet]
        [ActionName("ProjectShiftByID")]
        public projectShiftObject ProjectShiftByID(int param)
        {
            return db.PROJECTSHIFTs.Where(x => x.OID == param).Select(x => new projectShiftObject()
            {
                id = x.OID,
                shift = x.SHIFTID.ToString(),
                startDate = x.STARTDATE.Value,
                endDate = x.ENDDATE.Value,
                shiftTargetHasGenerated = db.PROJECTTASKBYSHIFTs.Count(y=>y.ESTIMATIONSHIFTID == x.OID && y.PERCENTCOMPLETEBASELINE.HasValue) > 0
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("AddShifts")]
        public IQueryable<projectShiftObject> AddShifts(int projectID, int add)
        {
            var startDate = DateTime.MinValue;
            var endDate = DateTime.MinValue;
            var shiftID = 0;
            var weekCounter = 0;

            var projectData = db.PROJECTs.Find(projectID);
            if (projectData == null)
            {
                throw new ApplicationException("Project not found.");
            }

            var latestProjectShift = db.PROJECTSHIFTs.Where(x => x.PROJECTID == projectID).OrderByDescending(x=>x.SHIFTID).FirstOrDefault();

            if (latestProjectShift == null)
            {
                var credential = pwaCredential;
                PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
                ProjectWS.Credentials = credential;

                PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(new Guid(projectData.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

                var zeroLevelTask = projectDs.Task.Select("TASK_OUTLINE_LEVEL = 0").FirstOrDefault() as ProjectDataSet.TaskRow;

                startDate = zeroLevelTask.IsTB_STARTNull() ? zeroLevelTask.TASK_START_DATE.Date: zeroLevelTask.TB_START.Date;

                shiftID = 1;
            }
            else
            {
                shiftID = latestProjectShift.SHIFTID.Value + 1;
                startDate = latestProjectShift.STARTDATE.Value.Date.AddDays(1);
            }

            var count = 0;
            var weekStartDate = startDate;
            var trackingModeValue = string.IsNullOrEmpty(projectData.IPTTRACKINGMODE) ? 0: int.Parse(projectData.IPTTRACKINGMODE.Split('_')[1]);
            while (count < add)
            {
                if (trackingModeValue == 0)
                {
                    // daily
                    if (projectData.WORKINGTIMETYPE == constants.WORKINGTIME_5DAYS &&
                    (startDate.DayOfWeek == DayOfWeek.Saturday || startDate.DayOfWeek == DayOfWeek.Sunday))
                    {
                        startDate = startDate.AddDays(1);
                        continue;
                    }

                    foreach (var masterShift in projectData.SHIFTs)
                    {
                        db.PROJECTSHIFTs.Add(new Models.PROJECTSHIFT()
                        {
                            PROJECTID = projectData.OID,
                            SHIFTID = shiftID,
                            STARTDATE = startDate.Date.Add(masterShift.STARTTIME.Value),
                            ENDDATE = masterShift.ENDTIME.Value <= masterShift.STARTTIME.Value ? startDate.Date.AddDays(1).Add(masterShift.ENDTIME.Value) : startDate.Date.Add(masterShift.ENDTIME.Value),
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        });

                        shiftID++;
                    }

                    startDate = startDate.AddDays(1);
                    count++;
                }
                else
                {
                    // weekly                    
                    while (weekCounter < trackingModeValue)
                    {
                        if (weekCounter == 0 && startDate.DayOfWeek == DayOfWeek.Monday)
                            weekStartDate = startDate;

                        if (projectData.WORKINGTIMETYPE == constants.WORKINGTIME_5DAYS && startDate.AddDays(1).DayOfWeek == DayOfWeek.Saturday)
                        {
                            weekCounter++;
                            if (weekCounter < trackingModeValue)
                                startDate = startDate.AddDays(1);
                        }
                        else if (projectData.WORKINGTIMETYPE == constants.WORKINGTIME_7DAYS && startDate.AddDays(1).DayOfWeek == DayOfWeek.Monday)
                        {
                            weekCounter++;
                            if (weekCounter < trackingModeValue)
                                startDate = startDate.AddDays(1);
                        }
                        else
                            startDate = startDate.AddDays(1);
                    }
                 

                    foreach (var masterShift in projectData.SHIFTs)
                    {
                        db.PROJECTSHIFTs.Add(new Models.PROJECTSHIFT()
                        {
                            PROJECTID = projectData.OID,
                            SHIFTID = shiftID,
                            STARTDATE = weekStartDate.Date.Add(masterShift.STARTTIME.Value),
                            ENDDATE = masterShift.ENDTIME.Value <= masterShift.STARTTIME.Value ? startDate.Date.AddDays(1).Add(masterShift.ENDTIME.Value) : startDate.Date.Add(masterShift.ENDTIME.Value),
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        });

                        shiftID++;
                    }

                    weekCounter = 0;
                    startDate = startDate.AddDays(1);
                    count++;
                }

            }

            db.SaveChanges();

            return db.PROJECTSHIFTs.Where(x => x.PROJECTID == projectID).Select(x => new projectShiftObject()
            {
                id = x.OID,
                shift = x.SHIFTID.ToString(),
                startDate = x.STARTDATE.Value,
                endDate = x.ENDDATE.Value
            });
        }

        [HttpGet]
        [ActionName("shiftworkingtimetypebyprojectid")]
        public HttpResponseMessage shiftworkingtimetypebyprojectid(int param)
        {
            var project = db.PROJECTs.Find(param);
            if (project != null)
                return Request.CreateResponse(HttpStatusCode.OK, project.WORKINGTIMETYPE + "|" + project.IPTTRACKINGMODE);

            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
    }
}
