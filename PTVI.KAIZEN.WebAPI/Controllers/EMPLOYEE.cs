﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public class EMPLOYEE
    {
        public string EMPLOYEEID { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string FULL_NAME { get; set; }
        public string USERNAME { get; set; }
        public string EMAIL { get; set; }
        public string DEPT_CODE { get; set; }
        public string POSITIONID { get; set; }
        public string SAPNAME { get; set; }
        public string DROP_POINT_DESC { get; set; }
        public string LEVEL_INFO { get; set; }
    }
}