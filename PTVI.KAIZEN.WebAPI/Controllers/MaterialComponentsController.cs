﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{

    public class MaterialComponentsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: MaterialComponents
        public IQueryable<materialComponentObject> Get()
        {
            IQueryable<materialComponentObject> uAObj;
            uAObj = db.MATERIALCOMPONENTs.Select(o => new materialComponentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                engineer = o.ENGINEER,
                ewpNo = o.EWPNO,
                lineItem = o.LINEITEM,
                material = o.MATERIAL,
                materialDesc = o.MATERIALDESC,
                mrNo= o.MRNO,
                network = o.NETWORK,
                networkActivity = o.NETWORKACTIVITY,
                orderQuantity = o.ORDERQUANTITY,
                //planOnSite = o.PLANONSITE,
                poItemNo = o.POITEMNO,
                poNo = o.PONO,
                pritemNo = o.PRITEMNO,
                prNo = o.PRNO,
                projectDocumentId = o.PROJECTDOCUMENTID,
                projectNo = o.PROJECTNO,
                receivedQuantity = o.RECEIVEDQUANTITY,
                reservation = o.RESERVATION,
                reservationItemNo = o.RESERVATIONITEMNO,
                uom = o.UOM,
                wbsNo = o.WBSNO,
                withdrawnQuantity = o.WITHDRAWNQUANTITY,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
            });

            return uAObj;
        }

        [HttpGet]
        [ActionName("pickMaterialComponentByProjectId")]
        public IQueryable<materialComponentSimpleObject> pickMaterialComponentByProjectId(int param)
        {
            return db.MATERIALCOMPONENTs.Where(x => x.PROJECTID == param).Select(o => new materialComponentSimpleObject()
            {
                id = o.OID,
                material = o.MATERIAL,
                materialDesc = o.MATERIALDESC,
                mrNo = o.MRNO,
                prNo = o.PRNO,
                prItemNo = o.PRITEMNO,
                poNo = o.PONO,
                poItemNo = o.POITEMNO,
                reservation = o.RESERVATION,
                reservationItemNo = o.RESERVATIONITEMNO
            });
            
        }

        [HttpGet]
        [ActionName("getLastUpdate")]
        public DateTime? getLastUpdate(int param)
        {
            var cDate = db.MATERIALCOMPONENTs.Where(x => x.PROJECTID == param).Max(x => x.CREATEDDATE);
            var uDate = db.MATERIALCOMPONENTs.Where(x => x.PROJECTID == param).Max(x => x.UPDATEDDATE);
            if (uDate > cDate)
                return uDate;
            else
                return cDate;
        }

        [HttpGet]
        [ActionName("GetByPrID")]
        public IEnumerable<materialComponentObject> GetByPrID(int param)
        {
            IEnumerable<materialComponentObject> uAObj;
            uAObj = db.MATERIALCOMPONENTs.Where(x => x.PROJECTID == param).Select(o => new materialComponentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                engineer = o.ENGINEER,
                ewpNo = o.EWPNO,
                lineItem = o.LINEITEM,
                material = o.MATERIAL,
                materialDesc = o.MATERIALDESC,
                mrNo = o.MRNO,
                network = o.NETWORK,
                networkActivity = o.NETWORKACTIVITY,
                orderQuantity = o.ORDERQUANTITY,
                //planOnSite = o.PLANONSITE,
                poItemNo = o.POITEMNO,
                poNo = o.PONO,
                pritemNo = o.PRITEMNO,
                prNo = o.PRNO,
                projectDocumentId = o.PROJECTDOCUMENTID,
                projectNo = o.PROJECTNO,
                receivedQuantity = o.RECEIVEDQUANTITY,
                reservation = o.RESERVATION,
                reservationItemNo = o.RESERVATIONITEMNO,
                uom = o.UOM,
                wbsNo = o.WBSNO,
                withdrawnQuantity = o.WITHDRAWNQUANTITY,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY,
                materialPurchaseGroup = o.MATERIALPURCHASEGROUPs.Select(x => new materialPurchaseGroupObject()
                {
                    id = x.OID,
                    prGroupCode = x.PRGROUPCODE,
                    prGroupDesc = x.PRGROUPDESC
                })
            }).ToList().OrderBy(o => !o.prIssuedDate.HasValue ? o.prPlanDate : !o.poRaisedDate.HasValue ? o.poPlanDate : !o.shipmentEndDate.HasValue ? o.planOnSite : new DateTime(2999, 1, 1));

            return uAObj;
        }

        [HttpGet]
        [ActionName("GetByPrIDforPS")]
        public IEnumerable<materialComponentObject> GetByPrIDforPS(int param)
        {
            IEnumerable<materialComponentObject> uAObj;
            uAObj = db.MATERIALCOMPONENTs.Where(x => x.PROJECTID == param).Select(o => new materialComponentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                engineer = o.ENGINEER,
                ewpNo = o.EWPNO,
                lineItem = o.LINEITEM,
                material = o.MATERIAL,
                materialDesc = o.MATERIALDESC,
                mrNo = o.MRNO,
                network = o.NETWORK,
                networkActivity = o.NETWORKACTIVITY,
                orderQuantity = o.ORDERQUANTITY,
                //planOnSite = o.PLANONSITE,
                poItemNo = o.POITEMNO,
                poNo = o.PONO,
                pritemNo = o.PRITEMNO,
                prNo = o.PRNO,
                projectDocumentId = o.PROJECTDOCUMENTID,
                projectNo = o.PROJECTNO,
                receivedQuantity = o.RECEIVEDQUANTITY,
                reservation = o.RESERVATION,
                reservationItemNo = o.RESERVATIONITEMNO,
                uom = o.UOM,
                wbsNo = o.WBSNO,
                withdrawnQuantity = o.WITHDRAWNQUANTITY,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY,
                materialPurchaseGroup = o.MATERIALPURCHASEGROUPs.Select(x => new materialPurchaseGroupObject()
                {
                    id = x.OID,
                    prGroupCode = x.PRGROUPCODE,
                    prGroupDesc = x.PRGROUPDESC
                })
            }).ToList().OrderBy(o => !o.prIssuedDate.HasValue ? o.prPlanDate : new DateTime(2999, 1, 1));

            return uAObj;
        }

        [HttpGet]
        [ActionName("GetByPrIDforBuyer")]
        public IEnumerable<materialComponentObject> GetByPrIDforBuyer(int param)
        {
            IEnumerable<materialComponentObject> uAObj;
            uAObj = db.MATERIALCOMPONENTs.Where(x => x.PROJECTID == param).Select(o => new materialComponentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                engineer = o.ENGINEER,
                ewpNo = o.EWPNO,
                lineItem = o.LINEITEM,
                material = o.MATERIAL,
                materialDesc = o.MATERIALDESC,
                mrNo = o.MRNO,
                network = o.NETWORK,
                networkActivity = o.NETWORKACTIVITY,
                orderQuantity = o.ORDERQUANTITY,
                //planOnSite = o.PLANONSITE,
                poItemNo = o.POITEMNO,
                poNo = o.PONO,
                pritemNo = o.PRITEMNO,
                prNo = o.PRNO,
                projectDocumentId = o.PROJECTDOCUMENTID,
                projectNo = o.PROJECTNO,
                receivedQuantity = o.RECEIVEDQUANTITY,
                reservation = o.RESERVATION,
                reservationItemNo = o.RESERVATIONITEMNO,
                uom = o.UOM,
                wbsNo = o.WBSNO,
                withdrawnQuantity = o.WITHDRAWNQUANTITY,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY,
                materialPurchaseGroup = o.MATERIALPURCHASEGROUPs.Select(x => new materialPurchaseGroupObject()
                {
                    id = x.OID,
                    prGroupCode = x.PRGROUPCODE,
                    prGroupDesc = x.PRGROUPDESC
                })
            }).ToList().OrderBy(o => !o.prIssuedDate.HasValue ? o.prPlanDate : !o.poRaisedDate.HasValue ? o.poPlanDate : !o.shipmentEndDate.HasValue ? o.etaSiteDateMSt : new DateTime(2999, 1, 1));

            return uAObj;
        }

        [HttpPut]
        [ActionName("updateEngMaterial")]
        public HttpResponseMessage updateEngMaterial(int param, materialComponentObject[] models)
        {
//            var n = db.MATERIALCOMPONENTs.Join(db.MATERIALPLANs, z => z.PONO, y => y.OID,  new { });
            foreach (materialComponentObject obj in models)
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var mco = db.MATERIALCOMPONENTs.Where(x => x.OID == obj.id).FirstOrDefault();
                mco.UPDATEDBY = obj.createdBy;
                mco.UPDATEDDATE = DateTime.Now;
                db.Entry(mco).State = EntityState.Modified;
                var mplan = db.MATERIALPLANs.Where(x => x.MATERIALCOMPONENTID == obj.id).FirstOrDefault();
                if (mplan != null)
                {
                    if (obj.setMaterialPlan.FirstOrDefault().poPlanDate != null)
                    {
                        mplan.POPLANDATE = obj.setMaterialPlan.FirstOrDefault().poPlanDate;
                        mplan.POPLANBY = obj.setMaterialPlan.FirstOrDefault().poPlanBy;
                    }
                    if (obj.setMaterialPlan.FirstOrDefault().rfqDate != null)
                    {
                        mplan.RFQDATE = obj.setMaterialPlan.FirstOrDefault().rfqDate;
                        mplan.RFQBY = obj.setMaterialPlan.FirstOrDefault().rfqBy;
                    }
                    if (obj.setMaterialPlan.FirstOrDefault().prPlanDate != null)
                    {
                        mplan.PRPLANDATE = obj.setMaterialPlan.FirstOrDefault().prPlanDate;
                        mplan.PRPLANBY = obj.setMaterialPlan.FirstOrDefault().prPlanBy;
                    }
                    mplan.TRAFFICOFFICERID = obj.setMaterialPlan.FirstOrDefault().trafficofficerid;
                    mplan.TRAFFICOFFICERNAME = obj.setMaterialPlan.FirstOrDefault().trafficofficername;
                    mplan.TRAFFICOFFICEREMAIL = obj.setMaterialPlan.FirstOrDefault().trafficofficeremail;
                    mplan.UPDATEDDATE = DateTime.Now;
                    mplan.UPDATEDBY = obj.setMaterialPlan.FirstOrDefault().updatedBy;
                    db.Entry(mplan).State = EntityState.Modified;
                }
                else
                {
                    var matPlan = new MATERIALPLAN();
                    if (obj.setMaterialPlan.FirstOrDefault().poPlanDate != null)
                    {
                        matPlan.POPLANDATE = obj.setMaterialPlan.FirstOrDefault().poPlanDate;
                        matPlan.POPLANBY = obj.setMaterialPlan.FirstOrDefault().poPlanBy;
                    }
                    if (obj.setMaterialPlan.FirstOrDefault().rfqDate != null)
                    {
                        matPlan.RFQDATE = obj.setMaterialPlan.FirstOrDefault().rfqDate;
                        matPlan.RFQBY = obj.setMaterialPlan.FirstOrDefault().rfqBy;
                    }
                    if (obj.setMaterialPlan.FirstOrDefault().prPlanDate != null)
                    {
                        matPlan.PRPLANDATE = obj.setMaterialPlan.FirstOrDefault().prPlanDate;
                        matPlan.PRPLANBY = obj.setMaterialPlan.FirstOrDefault().prPlanBy;
                    }
                    matPlan.TRAFFICOFFICERID = obj.setMaterialPlan.FirstOrDefault().trafficofficerid;
                    matPlan.TRAFFICOFFICERNAME = obj.setMaterialPlan.FirstOrDefault().trafficofficername;
                    matPlan.TRAFFICOFFICEREMAIL = obj.setMaterialPlan.FirstOrDefault().trafficofficeremail;
                    matPlan.MATERIALCOMPONENTID = mco.OID;
                    matPlan.UPDATEDDATE = DateTime.Now;
                    matPlan.UPDATEDBY = obj.setMaterialPlan.FirstOrDefault().updatedBy;
                    matPlan.CREATEDDATE = DateTime.Now;
                    matPlan.CREATEDBY = obj.setMaterialPlan.FirstOrDefault().createdBy;
                    db.Entry(matPlan).State = EntityState.Added;
                }

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

            }
            return Request.CreateResponse(HttpStatusCode.OK, models);

        }
    }
}
