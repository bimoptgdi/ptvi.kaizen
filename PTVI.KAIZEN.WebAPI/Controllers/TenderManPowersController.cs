﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TenderManPowersController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TenderManPowers
        public IQueryable<TENDERMANPOWER> GetTENDERMANPOWERs()
        {
            return db.TENDERMANPOWERs;
        }

        // GET: api/TenderManPowers/5
        [ResponseType(typeof(TENDERMANPOWER))]
        public IHttpActionResult GetTENDERMANPOWER(int id)
        {
            TENDERMANPOWER tENDERMANPOWER = db.TENDERMANPOWERs.Find(id);
            if (tENDERMANPOWER == null)
            {
                return NotFound();
            }

            return Ok(tENDERMANPOWER);
        }

        // PUT: api/TenderManPowers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDERMANPOWER(int id, TENDERMANPOWER tENDERMANPOWER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDERMANPOWER.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDERMANPOWER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDERMANPOWERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TenderManPowers
        [ResponseType(typeof(TENDERMANPOWER))]
        public IHttpActionResult PostTENDERMANPOWER(TENDERMANPOWER tENDERMANPOWER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDERMANPOWERs.Add(tENDERMANPOWER);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDERMANPOWER.OID }, tENDERMANPOWER);
        }

        // DELETE: api/TenderManPowers/5
        [ResponseType(typeof(TENDERMANPOWER))]
        public IHttpActionResult DeleteTENDERMANPOWER(int id)
        {
            TENDERMANPOWER tENDERMANPOWER = db.TENDERMANPOWERs.Find(id);
            if (tENDERMANPOWER == null)
            {
                return NotFound();
            }

            db.TENDERMANPOWERs.Remove(tENDERMANPOWER);
            db.SaveChanges();

            return Ok(tENDERMANPOWER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TENDERMANPOWERExists(int id)
        {
            return db.TENDERMANPOWERs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("TenderManPowerById")]
        public tenderManPowerObject TenderManPowerById(int param)
        {
            return db.TENDERMANPOWERs.Where(w => w.OID == param).Select(s => new tenderManPowerObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                position = s.POSITION,
                quantity = s.QUANTITY,
                qualification = s.QUALIFICATION,
                license = s.LICENSE,
                status = s.STATUS
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("TenderManPowersByTenderId")]
        public IEnumerable<tenderManPowerObject> TenderManPowersByTenderId(int param)
        {
            return db.TENDERMANPOWERs.Where(w => w.TENDERID == param).Select(s => new tenderManPowerObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                position = s.POSITION,
                quantity = s.QUANTITY,
                qualification = s.QUALIFICATION,
                license = s.LICENSE,
                status = s.STATUS
            });
        }

        [HttpPut]
        [ActionName("TenderManPowerSubmit")]
        public IHttpActionResult TenderManPowerSubmit(int param, tenderManPowerObject manPowerParam)
        {
            TENDERMANPOWER manPower = new TENDERMANPOWER();
            if (manPowerParam.id > 0)
            {
                manPower = db.TENDERMANPOWERs.Find(manPowerParam.id);
                manPower.UPDATEDBY = GetCurrentNTUserId();
                manPower.UPDATEDDATE = DateTime.Now;
            }
            else
            {
                manPower.CREATEDBY = GetCurrentNTUserId();
                manPower.CREATEDDATE = DateTime.Now;
                manPower.TENDERID = param;
            }
            manPower.POSITION = manPowerParam.position;
            manPower.QUANTITY = manPowerParam.quantity;
            manPower.QUALIFICATION = manPowerParam.qualification;
            manPower.LICENSE = manPowerParam.license;
            manPower.STATUS = manPowerParam.status;

            if (manPowerParam.id > 0)
                db.Entry(manPower).State = EntityState.Modified;
            else
                db.Entry(manPower).State = EntityState.Added;
            db.SaveChanges();

            return Ok(TenderManPowerById(manPower.OID));
        }
    }
}