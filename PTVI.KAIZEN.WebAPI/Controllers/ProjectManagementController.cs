﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.IO;
using Excel;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectManagementController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/ProjectManagement
        public IQueryable<PROJECTMANAGEMENT> GetPROJECTMANAGEMENTs()
        {
            return db.PROJECTMANAGEMENTs;
        }

        // GET: api/ProjectManagement/5
        [ResponseType(typeof(PROJECTMANAGEMENT))]
        public IHttpActionResult GetPROJECTMANAGEMENT(int id)
        {
            PROJECTMANAGEMENT pROJECTMANAGEMENT = db.PROJECTMANAGEMENTs.Find(id);
            if (pROJECTMANAGEMENT == null)
            {
                return NotFound();
            }

            return Ok(pROJECTMANAGEMENT);
        }

        // PUT: api/ProjectManagement/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROJECTMANAGEMENT(int id, PROJECTMANAGEMENT pROJECTMANAGEMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTMANAGEMENT.OID)
            {
                return BadRequest();
            }

            db.Entry(pROJECTMANAGEMENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTMANAGEMENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjectManagement
        [ResponseType(typeof(PROJECTMANAGEMENT))]
        public IHttpActionResult PostPROJECTMANAGEMENT(PROJECTMANAGEMENT pROJECTMANAGEMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROJECTMANAGEMENTs.Add(pROJECTMANAGEMENT);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pROJECTMANAGEMENT.OID }, pROJECTMANAGEMENT);
        }

        // DELETE: api/ProjectManagement/5
        [ResponseType(typeof(PROJECTMANAGEMENT))]
        public IHttpActionResult DeletePROJECTMANAGEMENT(int id)
        {
            PROJECTMANAGEMENT pROJECTMANAGEMENT = db.PROJECTMANAGEMENTs.Find(id);
            if (pROJECTMANAGEMENT == null)
            {
                return NotFound();
            }

            db.PROJECTMANAGEMENTs.Remove(pROJECTMANAGEMENT);
            db.SaveChanges();

            return Ok(pROJECTMANAGEMENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTMANAGEMENTExists(int id)
        {
            return db.PROJECTMANAGEMENTs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("PMById")]
        public projectManagementObject PMById(int param)
        {
            projectManagementObject projectManagement =
                db.PROJECTMANAGEMENTs.Where(w => w.OID == param).
                Select(s => new projectManagementObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    manageKeyIssue = s.MANAGEKEYISSUE,
                    safetyPerfomance = s.SAFETYPERFOMANCE,
                    scope = s.SCOPE,
                    changeRequest = s.CHANGEREQUEST,
                    benefit = s.BENEFIT,
                    accomplishment = s.ACCOMPLISHMENT,
                    details = s.DETAILS,
                    ehsRecords = s.EHSRECORDS,
                    contractor = s.CONTRACTOR
                }).FirstOrDefault();
            return projectManagement;
        }

        [HttpGet]
        [ActionName("PMByProjectId")]
        public projectManagementObject PMByProjectId(int param)
        {
            var cekData = db.PROJECTMANAGEMENTs.Where(w => w.PROJECTID == param);
            if (cekData.Count() == 0)
            {
                PROJECTMANAGEMENT insertPM = new PROJECTMANAGEMENT();
                insertPM.PROJECTID = param;
                insertPM.CREATEDBY = GetCurrentNTUserId();
                insertPM.CREATEDDATE = DateTime.Now;
                db.PROJECTMANAGEMENTs.Add(insertPM);
                db.SaveChanges();
            }

            projectManagementObject projectManagement =
                db.PROJECTMANAGEMENTs.Where(w => w.PROJECTID == param).
                Select(s => new projectManagementObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    manageKeyIssue = s.MANAGEKEYISSUE,
                    safetyPerfomance = s.SAFETYPERFOMANCE,
                    scope = s.SCOPE,
                    changeRequest = s.CHANGEREQUEST,
                    benefit = s.BENEFIT,
                    accomplishment = s.ACCOMPLISHMENT,
                    details = s.DETAILS,
                    ehsRecords = s.EHSRECORDS,
                    contractor = s.CONTRACTOR
                    //,
                    //keyDeliverables = s.KEYDELIVERABLES.Select(k => new keyDeliverableObject()
                    //{
                    //    id = k.OID,
                    //    projectManagementId = k.PROJECTMANAGEMENTID,
                    //    name = k.NAME,
                    //    weightPercentage = k.WEIGHTPERCENTAGE,
                    //    planStartddate = k.PLANSTARTDATE,
                    //    planFinishdDate = k.PLANFINISHDATE,
                    //    actualStartdDate = k.ACTUALSTARTDATE,
                    //    actualFinishdDate = k.ACTUALFINISHDATE,
                    //    status = k.STATUS,
                    //    remark = k.REMARK
                    //}).ToList()
                }).FirstOrDefault();
            return projectManagement;
        }

        [HttpPost]
        [ActionName("UpdateManagementModule")]
        public IHttpActionResult UpdateManagementModule(string param, projectManagementObject projectManagementParam)
        {
            try
            {
                PROJECTMANAGEMENT projectManagement = new PROJECTMANAGEMENT();
                if (projectManagementParam.id > 0)
                {
                    projectManagement = db.PROJECTMANAGEMENTs.Find(projectManagementParam.id);
                    projectManagement.UPDATEDBY = GetCurrentNTUserId();
                    projectManagement.UPDATEDDATE = DateTime.Now;
                }
                else
                {

                    projectManagement.CREATEDBY = GetCurrentNTUserId();
                    projectManagement.CREATEDDATE = DateTime.Now;
                }

                if (param == constants.PROJECTMANAGEMENT_MANAGEKEYISSUE)
                    projectManagement.MANAGEKEYISSUE = projectManagementParam.manageKeyIssue;
                else if (param == constants.PROJECTMANAGEMENT_SAFETYPERFORMANCE)
                    projectManagement.SAFETYPERFOMANCE = projectManagementParam.safetyPerfomance;
                else if (param == constants.PROJECTMANAGEMENT_SCOPE)
                    projectManagement.SCOPE = projectManagementParam.scope;
                else if (param == constants.PROJECTMANAGEMENT_CHANGEREQUEST)
                    projectManagement.CHANGEREQUEST = projectManagementParam.changeRequest;
                else if (param == constants.PROJECTMANAGEMENT_BENEFIT)
                    projectManagement.BENEFIT = projectManagementParam.benefit;
                else if (param == constants.PROJECTMANAGEMENT_ACCOMPLISHMENT)
                    projectManagement.ACCOMPLISHMENT = projectManagementParam.accomplishment;
                else if (param == constants.PROJECTMANAGEMENT_DETAILS)
                    projectManagement.DETAILS = projectManagementParam.details;
                else if (param == constants.PROJECTMANAGEMENT_EHSRECORDS)
                    projectManagement.EHSRECORDS = projectManagementParam.ehsRecords;
                else if (param == constants.PROJECTMANAGEMENT_CONTRACTOR)
                    projectManagement.CONTRACTOR = projectManagementParam.contractor;
                else if (param == "all")
                {
                    projectManagement.MANAGEKEYISSUE = projectManagementParam.manageKeyIssue;
                    projectManagement.SAFETYPERFOMANCE = projectManagementParam.safetyPerfomance;
                    projectManagement.SCOPE = projectManagementParam.scope;
                    projectManagement.CHANGEREQUEST = projectManagementParam.changeRequest;
                    projectManagement.BENEFIT = projectManagementParam.benefit;
                    projectManagement.ACCOMPLISHMENT = projectManagementParam.accomplishment;
                    projectManagement.DETAILS = projectManagementParam.details;
                    projectManagement.EHSRECORDS = projectManagementParam.ehsRecords;
                    projectManagement.CONTRACTOR = projectManagementParam.contractor;
                }

                if (projectManagementParam.id > 0)
                {
                    projectManagement = db.PROJECTMANAGEMENTs.Find(projectManagementParam.id);
                    projectManagement.UPDATEDBY = GetCurrentNTUserId();
                    projectManagement.UPDATEDDATE = DateTime.Now;
                    db.Entry(projectManagement).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    db.PROJECTMANAGEMENTs.Add(projectManagement);
                    db.SaveChanges();
                }

                return Ok(PMById(projectManagement.OID));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                throw;
            }
        }

        [HttpPost]
        [ActionName("Upload")]
        public System.Threading.Tasks.Task<HttpResponseMessage> Upload(string param)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();

            int count = 0;
            string listFailed = null;
            string listFailedDetail = null;

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    foreach (var stream in provider.Contents)
                    {
                        // Process The File
                        var type = Path.GetExtension(stream.Headers.ContentDisposition.FileName.Replace("\"", ""));

                        var fs = new MemoryStream(stream.ReadAsByteArrayAsync().Result);

                        var splitParam = param.Split('|');
                        int oid = int.Parse(splitParam[0]);
                        string updateBy = splitParam[1];

                        var returnVal = ProcessUploadKeyListFile(fs, type, oid, updateBy).Split('|');
                        count += int.Parse(returnVal[0]);

                        if (!string.IsNullOrEmpty(listFailed))
                        {
                            listFailed += ", " + returnVal[1];
                        }
                        else
                        {
                            listFailed += returnVal[1];
                        }

                        if (!string.IsNullOrEmpty(listFailedDetail))
                        {
                            listFailedDetail += "; " + returnVal[2];
                        }
                        else
                        {
                            listFailedDetail += returnVal[2];
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
                var aaa = listFailed.Split(',').Count();

                return Request.CreateResponse(HttpStatusCode.OK, new { success = count, Failed = (listFailed.Split(',').Count() - 1), listFailed = listFailed, listFailedDetail = listFailedDetail });
            });

            return task;
        }

        private string ProcessUploadKeyListFile(Stream stream, string fileType, int oid, string uploadedBy)
        {
            IExcelDataReader excelReader;
            if (Path.GetExtension(fileType) == ".xls")
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            else
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            excelReader.IsFirstRowAsColumnNames = true;

            DataSet ds = excelReader.AsDataSet(true);
            PROJECTMANAGEMENT projectManagement = db.PROJECTMANAGEMENTs.Where(w => w.PROJECTID == oid).FirstOrDefault();
            if (projectManagement == null) return "Project Id Not Found";
            int saveCount = 0;
            var dt = ds.Tables[0];
            string listErrorRow = null;
            string listDetailErrorRow = null;
            int nowYear = DateTime.UtcNow.Year;
            List<KEYDELIVERABLE> newListKeyDeliverable = new List<KEYDELIVERABLE>();
            foreach (DataRow row in dt.Rows)
            {
                if (string.IsNullOrEmpty(row["Key Deliverables"].ToString()))
                {
                    continue;
                }

                DateTime fromDateValue;
                int fromIntValue;
                string inpDetailErrorRow = null;

                var cekReturn = true;

                DateTime? planStartDate = null;
                if (!DateTime.TryParse(row["Plan Start Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPMListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPMListFileMessage(inpDetailErrorRow, row[0].ToString(), "Plan Start Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    planStartDate = fromDateValue;
                }

                DateTime? planFinishDate = null;
                if (!DateTime.TryParse(row["Plan Finish Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPMListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPMListFileMessage(inpDetailErrorRow, row[0].ToString(), "Plan Finish Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    planFinishDate = fromDateValue;
                }

                DateTime? actualStartDate = null;
                if (!DateTime.TryParse(row["Actual Start Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPMListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPMListFileMessage(inpDetailErrorRow, row[0].ToString(), "Actual Start Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    actualStartDate = fromDateValue;
                }

                DateTime? actualFinishDate = null;
                if (!DateTime.TryParse(row["Actual Finish Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPMListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPMListFileMessage(inpDetailErrorRow, row[0].ToString(), "Actual Finish Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    actualFinishDate = fromDateValue;
                }

                int? weight = null;
                if (!int.TryParse(row["Weight"].ToString(), out fromIntValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPMListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPMListFileMessage(inpDetailErrorRow, row[0].ToString(), "Weight is not valid");
                    cekReturn = false;
                }
                else
                {
                    weight = fromIntValue;
                }


                var name = row["Key Deliverables"].ToString();
                var statusCode = row["StatusCode"].ToString();
                var remark = row["Remark"].ToString();

                KEYDELIVERABLE keyDeliverable = db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == oid && w.NAME == name).FirstOrDefault();

                if (keyDeliverable == null)
                {
                    keyDeliverable = new KEYDELIVERABLE();
                    keyDeliverable.CREATEDDATE = DateTime.Now;
                    keyDeliverable.CREATEDBY = uploadedBy;
                    keyDeliverable.PROJECTMANAGEMENTID = projectManagement.OID;
                    keyDeliverable.REVISION = 0;
                }
                else
                {
                    keyDeliverable.UPDATEDDATE = DateTime.Now;
                    keyDeliverable.UPDATEDBY = uploadedBy;

                    if (keyDeliverable.PLANSTARTDATE != planStartDate || keyDeliverable.PLANFINISHDATE != planFinishDate)
                        keyDeliverable.REVISION = keyDeliverable.REVISION + 1;
                }

                keyDeliverable.NAME = name;
                keyDeliverable.WEIGHTPERCENTAGE = weight;
                keyDeliverable.PLANSTARTDATE = planStartDate;
                keyDeliverable.PLANFINISHDATE = planFinishDate;
                keyDeliverable.ACTUALSTARTDATE = actualStartDate;
                keyDeliverable.ACTUALFINISHDATE = actualFinishDate;
                keyDeliverable.STATUS = statusCode;
                keyDeliverable.REMARK = remark;

                if (keyDeliverable.OID > 0)
                {
                    db.Entry(keyDeliverable).State = EntityState.Modified;
                }
                else
                {
                    List<keyDeliverablesProgressObject> listChilds = percentageMonthPogressByStartEnd_Date(keyDeliverable.PLANSTARTDATE.Value, keyDeliverable.PLANFINISHDATE.Value);

                    foreach (keyDeliverablesProgressObject child in listChilds)
                    {
                        KEYDELIVERABLESPROGRESS deliverablesProgress = new KEYDELIVERABLESPROGRESS();
                        deliverablesProgress.YEAR = child.year;
                        deliverablesProgress.MONTH = child.month;
                        deliverablesProgress.PLANPERCENTAGE = child.planPercentage;
                        deliverablesProgress.CREATEDBY = GetCurrentNTUserId();
                        deliverablesProgress.CREATEDDATE = DateTime.Now;

                        keyDeliverable.KEYDELIVERABLESPROGRESSes.Add(deliverablesProgress);
                    }

                    newListKeyDeliverable.Add(keyDeliverable);
                }
                ++saveCount;
            }

            db.KEYDELIVERABLES.AddRange(newListKeyDeliverable.GroupBy(g => g.NAME).Select(s => new KEYDELIVERABLE
            {
                CREATEDDATE = s.Max(m => m.CREATEDDATE),
                CREATEDBY = s.Max(m => m.CREATEDBY),
                UPDATEDDATE = s.Max(m => m.UPDATEDDATE),
                UPDATEDBY = s.Max(m => m.UPDATEDBY),
                PROJECTMANAGEMENTID = s.Max(m => m.PROJECTMANAGEMENTID),
                REVISION = s.Max(m => m.REVISION),
                NAME = s.Max(m => m.NAME),
                WEIGHTPERCENTAGE = s.Max(m => m.WEIGHTPERCENTAGE),
                PLANSTARTDATE = s.Max(m => m.PLANSTARTDATE),
                PLANFINISHDATE = s.Max(m => m.PLANFINISHDATE),
                ACTUALSTARTDATE = s.Max(m => m.ACTUALSTARTDATE),
                ACTUALFINISHDATE = s.Max(m => m.ACTUALFINISHDATE),
                STATUS = s.Max(m => m.STATUS),
                REMARK = s.Max(m => m.REMARK),
                KEYDELIVERABLESPROGRESSes = s.Max(m => m.KEYDELIVERABLESPROGRESSes)
            }));
            db.SaveChanges();
            listErrorRow = saveCount.ToString() + "|" + listErrorRow + "|" + listDetailErrorRow;
            return listErrorRow;
        }

        private string errorProcessUploadPMListFile(string listErrorRow, string numberOfError)
        {
            if (!string.IsNullOrEmpty(listErrorRow))
            {
                listErrorRow += "," + numberOfError;
            }
            else
            {
                listErrorRow += numberOfError;
            }

            return listErrorRow;
        }

        private string errorProcessUploadPMListFileMessage(string inpDetailErrorRow, string row, string Message)
        {
            if (!string.IsNullOrEmpty(inpDetailErrorRow))
            {
                inpDetailErrorRow += ", ";
            }
            else
            {
                inpDetailErrorRow = "row " + row + ": ";
            }

            return inpDetailErrorRow += Message;
        }
    }
}