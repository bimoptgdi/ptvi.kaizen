﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;  
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.IO;
using Excel;
using System.Text.RegularExpressions;
using System.Text;


namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class UploadExcelController : KAIZENController
    {
        public class objFiles
        {
            public int id { get; set; }
            public string sourceFile { get; set; }
            public string destinationFile { get; set; }
        }
        public class objTbl
        {
            public string TableName { get; set; }
            public string Columns { get; set; }
        }

        [HttpGet]
        [ActionName("ProcessCostLoader")]
        public object ProcessCostLoaderX(string param)
        {
            var ntUser = GetCurrentNTUserId();
            List<dynamic> result = new List<dynamic>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "COST").ToList();
            string fileName = "";
            string purposeFile = "";
            

            var iCostReportElementEdit = db.COSTREPORTELEMENTs;
            IList<COSTREPORTELEMENT> iCostReportElementAdd = new List<COSTREPORTELEMENT>();

            var iCostReportNetworkEdit = db.COSTREPORTNETWORKs;
            IList<COSTREPORTNETWORK> iCostReportNetworkAdd = new List<COSTREPORTNETWORK>();

            var iCostReportBudgetEdit = db.COSTREPORTBUDGETs;
            IList<COSTREPORTBUDGET> iCostReportBudgetAdd = new List<COSTREPORTBUDGET>();

            var iCostReportActualCostEdit = db.COSTREPORTACTUALCOSTs;
            IList<COSTREPORTACTUALCOST> iCostReportActualCostAdd = new List<COSTREPORTACTUALCOST>();

            var iCostReportActualCostTempEdit = db.COSTREPORTACTUALCOSTTEMPs;
            IList<COSTREPORTACTUALCOSTTEMP> iCostReportActualCostTempAdd = new List<COSTREPORTACTUALCOSTTEMP>();

            var iCostReportCommitmentEdit = db.COSTREPORTCOMMITMENTs;
            IList<COSTREPORTCOMMITMENT> iCostReportCommitmentAdd = new List<COSTREPORTCOMMITMENT>();

            IList<FILEINTEGRATIONLOADER> iListfIntLoader = new List<FILEINTEGRATIONLOADER>();
            string filename = "";
            var ok = 1;
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                //remark detail as input for integration loader remark
                List<string> failedDetail = new List<string>();
                filename = fileInt.SHAREFOLDER + fileInt.FILENAME;
                var shareFolder = fileInt.SHAREFOLDER;
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                if (!Directory.Exists(shareFolder))
                {
                    //log sharefolder is empty     
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                    fIntLoader.REMARK = "Sharefolder is not exist";

                    db.Entry(fIntLoader).State = EntityState.Added;

                    failedDetail.Add(fIntLoader.REMARK);
                    result.Add(new { filename = filename, failed_Detail = failedDetail });
                    ok = 0;
                    continue;
                }
                else
                {
                    if (!File.Exists(filename))
                    {
                        //log file is not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "File is not exist";
                        fIntLoader.LOADERBY = GetCurrentNTUserId();
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;

                        failedDetail.Add(fIntLoader.REMARK);
                        result.Add(new { filename = filename, failed_Detail = failedDetail });
                        ok = 0;
                        continue;

                    }
                }
            }
            if (ok == 0)
                return result;
            else
                result.Clear();

            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                var shareFolder = fileInt.SHAREFOLDER;
                fileName = shareFolder + fileInt.FILENAME;
                purposeFile = fileInt.PURPOSEFILE;
                List<string> failedDetail = new List<string>();
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();

                if (string.IsNullOrEmpty(shareFolder))
                {
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = ntUser;
                    fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | Sharefolder does not exist";

                    db.Entry(fIntLoader).State = EntityState.Added;

                    failedDetail.Add(fIntLoader.REMARK);
                    result.Add(new { file_name = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = failedDetail });

                    continue;
                }
                else
                {
                    if (!File.Exists(fileName))
                    {
                        //log file does not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | File does not exist";
                        fIntLoader.LOADERBY = ntUser;
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;

                        failedDetail.Add(fIntLoader.REMARK);
                        result.Add(new { file_name = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = failedDetail });

                        continue;
                    }
                    else
                    {
                        int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                        string localhost = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.InstanceName; //instance name sqlserver
                        string idUser = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idUser; //User sqlserver
                        string idCatalog = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idCatalog; //catalog sqlserver
                        string idPassword = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idPassword; //password sqlserver
                        string excelConnStr03 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0 Xml;HDR=YES;IMEX=1\"";
                        string excelConnStr07 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";
                        //string destConnString = @"Password=Formula1; Persist Security Info=True; User ID=sa; Initial Catalog=VALE_iPROM; Data Source=" + localhost;
                        string destConnString = @"Password="+ idPassword + "; Persist Security Info=True; User ID=" + idUser + "; Initial Catalog=" + idCatalog + "; Data Source=" + localhost;

                        //add new file integration loader
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        string connStr = "";
                        
                        //"HDR=Yes;" indicates that the first row contains columnnames, not data. "HDR=No;" indicates the opposite.
                        //IMEX are:
                        //0 is Export mode
                        //1 is Import mode
                        //2 is Linked mode(full update capabilities)
                        db.Entry(fIntLoader).State = EntityState.Added;
                        db.SaveChanges();
                        if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                            connStr = excelConnStr07;
                        else if (fileInt.FILENAME.ToUpper().Contains(".XLS"))
                            connStr = excelConnStr03;
                        else
                        {
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;
                            fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | This file doesn't have xlsx or xls extension";
                            db.Entry(fIntLoader).State = EntityState.Modified;
                            db.SaveChanges();
                            failedDetail.Add(fIntLoader.REMARK);
                            result.Add(new { file_name = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = failedDetail });
                            continue;
                        }

                        if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                        {
                            using (OleDbConnection conn = new OleDbConnection(connStr))
                            {
                                try
                                {
                                    if (conn.State == ConnectionState.Closed)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Project definition], [Level], [WBS element], [Name] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader != null)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        List<int> idxDelRow = new List<int>();

                                        var remark = "";
                                        
                                        List<string> pkProjectDefinition = new List<string>();
                                        List<string> pkLevel = new List<string>();
                                        List<string> negativeLevel = new List<string>();
                                        List<string> wrongFormatLevel = new List<string>();
                                        List<string> pkWbsNo = new List<string>();

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            //primary keys ProjectNo, WBSLevel, WBSNo

                                            // ProjectNo
                                            if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkProjectDefinition.Add(i.ToString());
                                                continue;
                                            }

                                            // WBSLevel
                                            int WBSLEVEL;
                                            if (int.TryParse(row["Level"].ToString().Trim(), out WBSLEVEL) || string.IsNullOrWhiteSpace(row["Level"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Level"].ToString()))
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    pkLevel.Add(i.ToString());
                                                    continue;
                                                }
                                                //if (WBSLEVEL < 0)
                                                //{
                                                //    negativeLevel.Add(i.ToString());
                                                //}
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatLevel.Add(i.ToString());
                                                continue;
                                            }

                                            // WBSNo
                                            if (string.IsNullOrWhiteSpace(row["WBS element"].ToString()))
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                pkWbsNo.Add(i.ToString());
                                                continue;
                                            }

                                            if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                            {
                                                row["Project definition"] = null;
                                                table.AcceptChanges();
                                            }

                                            if (string.IsNullOrWhiteSpace(row["Name"].ToString()))
                                            {
                                                row["Name"] = null;
                                                table.AcceptChanges();
                                            }

                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRow.Count; j++)
                                        {
                                            table.Rows[idxDelRow[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.COSTREPORTELEMENTTEMP";

                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                            bulkCopy.ColumnMappings.Add("Level", "WBSLEVEL");
                                            bulkCopy.ColumnMappings.Add("WBS element", "WBSNO");
                                            bulkCopy.ColumnMappings.Add("Name", "DESCRIPTION");

                                            bulkCopy.WriteToServer(table);

                                            fIntLoader.LOADERBY = ntUser;
                                            fIntLoader.LOADERDATE = DateTime.Now;
                                            fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                            db.Entry(fIntLoader).State = EntityState.Modified;
                                            db.SaveChanges();

                                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount });
                                        }                                        
                                    }                                    
                                }
                                catch (Exception e)
                                {
                                    throw;
                                }
                                finally
                                {
                                    //string sql = @"EXEC MOVECOSTREPORTELEMENT " + fIntLoader.FILEINTEGRATIONID.ToString();
                                    //db.Database.ExecuteSqlCommand(sql);
                                    db.MOVECOSTREPORTELEMENT(fIntLoader.OID);

                                    if (conn.State == ConnectionState.Open)
                                    {
                                        conn.Close();
                                    }
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "NETWORK")
                        {
                            using (OleDbConnection conn = new OleDbConnection(connStr))
                            {
                                try
                                {
                                    if (conn.State == ConnectionState.Closed)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Project definition], [WBS element], [Network], [Text] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader != null)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        List<int> idxDelRow = new List<int>();

                                        var remark = "";
                                        
                                        List<string> pkProjectNo = new List<string>();
                                        List<string> pkWbsNo = new List<string>();

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            // primary keys ProjectNo, WBSNo

                                            // ProjectNo
                                            if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                pkProjectNo.Add(i.ToString());
                                                continue;
                                            }

                                            // WbsNo
                                            if (string.IsNullOrWhiteSpace(row["WBS element"].ToString()))
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                pkWbsNo.Add(i.ToString());
                                                continue;
                                            }
                                            
                                            if (string.IsNullOrWhiteSpace(row["Network"].ToString())){
                                                row["Network"] = null;
                                                table.AcceptChanges();
                                            }

                                            if (string.IsNullOrWhiteSpace(row["Text"].ToString())) {
                                                row["Text"] = null;
                                                table.AcceptChanges();
                                            }
                                            
                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRow.Count; j++)
                                        {
                                            table.Rows[idxDelRow[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.COSTREPORTNETWORKTEMP";

                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                            bulkCopy.ColumnMappings.Add("WBS element", "WBSNO");
                                            bulkCopy.ColumnMappings.Add("Network", "NETWORK");
                                            bulkCopy.ColumnMappings.Add("Text", "DESCRIPTION");

                                            bulkCopy.WriteToServer(table);

                                            fIntLoader.LOADERBY = ntUser;
                                            fIntLoader.LOADERDATE = DateTime.Now;                                           
                                            fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                            db.Entry(fIntLoader).State = EntityState.Modified;
                                            db.SaveChanges();

                                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount });
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    throw;
                                }
                                finally
                                {
                                    //string sql = @"EXEC MOVECOSTREPORTNETWORK " + fIntLoader.FILEINTEGRATIONID.ToString();
                                    //db.Database.ExecuteSqlCommand(sql);
                                    db.MOVECOSTREPORTNETWORK(fIntLoader.OID);
                                    if (conn.State == ConnectionState.Open)
                                    {
                                        conn.Close();
                                    }
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "BUDGET")
                        {
                            using (OleDbConnection conn = new OleDbConnection(connStr))
                            {
                                try
                                {
                                    if (conn.State == ConnectionState.Closed)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Fiscal Year], [Value TranCurr] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader != null)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        List<int> idxDelRow = new List<int>();

                                        var remark = "";

                                        List<string> pkProjectDefinition = new List<string>();
                                        List<string> pkWbsNo = new List<string>();
                                        List<string> pkYear = new List<string>();
                                        List<string> invalidFiscalYear = new List<string>();
                                        List<string> wrongFormatFiscalYear = new List<string>();
                                        List<string> negativeValueTranCurr = new List<string>();
                                        List<string> wrongFormatValueTranCurr = new List<string>();

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            // Primary keys ProjectNo, WBSNo, YEAR

                                            // ProjectNo
                                            if (string.IsNullOrWhiteSpace(row["Project Definition"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkProjectDefinition.Add(i.ToString());
                                                continue;
                                            }

                                            // WbsNo
                                            if (string.IsNullOrWhiteSpace(row["WBS Element"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkWbsNo.Add(i.ToString());
                                                continue;
                                            }

                                            // Year
                                            int YEAR;
                                            if (int.TryParse(row["Fiscal Year"].ToString().Trim(), out YEAR) || string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    pkYear.Add(i.ToString());
                                                    continue;
                                                }

                                                if (YEAR <= 0)
                                                {
                                                    ++failedCount;
                                                    idxDelRow.Add(i);
                                                    invalidFiscalYear.Add(i.ToString());
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatFiscalYear.Add(i.ToString());
                                                continue;
                                            }


                                            float AMOUNT;
                                            if (float.TryParse(row["Value TranCurr"].ToString().Trim(), out AMOUNT) || string.IsNullOrWhiteSpace(row["Value TranCurr"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Value TranCurr"].ToString()))
                                                {
                                                    row["Value TranCurr"] = null;
                                                    table.AcceptChanges();
                                                }

                                                //if (AMOUNT < 0)
                                                //{
                                                //    negativeValueTranCurr.Add(i.ToString());
                                                //}
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatValueTranCurr.Add(i.ToString());
                                                continue;
                                            }                                                           

                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRow.Count; j++)
                                        {
                                            table.Rows[idxDelRow[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();
                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.COSTREPORTBUDGETTEMP";

                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                            bulkCopy.ColumnMappings.Add("WBS Element", "WBSNO");
                                            bulkCopy.ColumnMappings.Add("Fiscal Year", "YEAR");
                                            bulkCopy.ColumnMappings.Add("Value TranCurr", "AMOUNT");

                                            bulkCopy.WriteToServer(table);

                                            fIntLoader.LOADERBY = ntUser;
                                            fIntLoader.LOADERDATE = DateTime.Now;                                                                                       
                                            fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                            db.Entry(fIntLoader).State = EntityState.Modified;
                                            db.SaveChanges();

                                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount });
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    throw;
                                }
                                finally
                                {
                                    //string sql = @"EXEC MOVECOSTREPORTBUDGET " + fIntLoader.FILEINTEGRATIONID.ToString();
                                    //db.Database.ExecuteSqlCommand(sql);
                                    db.MOVECOSTREPORTBUDGET(fIntLoader.OID);

                                    if (conn.State == ConnectionState.Open)
                                    {
                                        conn.Close();
                                    }
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUALCIDB" || fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUALSIDB")
                        {
                            //OleDbTransaction transac = null;                            
                            using (OleDbConnection conn = new OleDbConnection(connStr))
                            {

                                try
                                {
                                    //using (System.Transactions.TransactionScope transaction = new System.Transactions.TransactionScope())
                                    //{
                                    if (conn.State == ConnectionState.Closed)
                                    {
                                        conn.Open();
                                    }
                                    //transac = conn.BeginTransaction();
                                    OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                                    //com.Transaction = transac;
                                    OleDbDataReader reader = com.ExecuteReader();

                                    if (reader != null)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        List<int> idxDelRow = new List<int>();

                                        var remark = "";
                                       
                                        List<string> pkProjectNo = new List<string>();
                                        List<string> pkWbsNo = new List<string>();
                                        List<string> pkYear = new List<string>();
                                        List<string> invalidFiscalYear = new List<string>();
                                        List<string> wrongFormatFiscalYear = new List<string>();
                                        List<string> pkMonth = new List<string>();
                                        List<string> negativePeriod = new List<string>();
                                        List<string> wrongFormatPeriod = new List<string>();
                                        List<string> negativeObjCrcy = new List<string>();
                                        List<string> wrongFormatObjCrcy = new List<string>();

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;
                                            
                                            // Primary keys ProjectNo, WBSNo, YEAR, Month

                                            // PrjectNo
                                            if (string.IsNullOrWhiteSpace(row["Project Definition"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkProjectNo.Add(i.ToString());
                                                continue;
                                            }

                                            // WbsNo
                                            if (string.IsNullOrWhiteSpace(row["WBS Element"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkWbsNo.Add(i.ToString());
                                                continue;
                                            }

                                            // Year
                                            int year;
                                            if (int.TryParse(row["Fiscal Year"].ToString().Trim(), out year) || string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                                {                                                    
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    pkYear.Add(i.ToString());
                                                    continue;
                                                }

                                                if (year <= 0)
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    invalidFiscalYear.Add(i.ToString());
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatFiscalYear.Add(i.ToString());
                                                continue;
                                            }

                                            // Month
                                            int month;
                                            if (int.TryParse(row["Period"].ToString().Trim(), out month) || string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Period"].ToString())) {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    pkMonth.Add(i.ToString());
                                                    continue;
                                                }                                                    

                                                if (year <= 0)
                                                {                                                    
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    negativePeriod.Add(i.ToString());
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatPeriod.Add(i.ToString());
                                                continue;
                                            }

                                            float AMOUNT;
                                            if (float.TryParse(row["Value in Obj# Crcy"].ToString().Trim(), out AMOUNT) || string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                                {
                                                    row["Value in Obj# Crcy"] = null;
                                                    table.AcceptChanges();
                                                }                                                    

                                                //if (AMOUNT < 0)
                                                //    negativeObjCrcy.Add(i.ToString());
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatObjCrcy.Add(i.ToString());
                                                continue;
                                            }        

                                            if (string.IsNullOrWhiteSpace(row["Object"].ToString()))
                                            {
                                                row["Object"] = null;
                                                table.AcceptChanges();
                                            }

                                            if (string.IsNullOrWhiteSpace(row["Object type"].ToString()))
                                            {
                                                row["Object type"] = null;
                                                table.AcceptChanges();
                                            }
                                            
                                            ++succeedCount;

                                        }

                                        for (int j = 0; j < idxDelRow.Count; j++)
                                        {
                                            table.Rows[idxDelRow[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.COSTREPORTACTUALCOSTTEMP";

                                            // Write from the source to the destination
                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Project Definition", "PROJECTNO");
                                            bulkCopy.ColumnMappings.Add("WBS Element", "WBSNO");
                                            bulkCopy.ColumnMappings.Add("Object", "NETWORK");
                                            bulkCopy.ColumnMappings.Add("Object type", "OBJECTTYPE");
                                            bulkCopy.ColumnMappings.Add("Fiscal Year", "YEAR");
                                            bulkCopy.ColumnMappings.Add("Period", "MONTH");
                                            bulkCopy.ColumnMappings.Add("Value in Obj# Crcy", "AMOUNT");

                                            bulkCopy.WriteToServer(table);

                                            fIntLoader.LOADERBY = ntUser;
                                            fIntLoader.LOADERDATE = DateTime.Now;                                           
                                            fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                            db.Entry(fIntLoader).State = EntityState.Modified;
                                            db.SaveChanges();

                                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount });
                                        }                                        
                                    }
                                    //transaction.Complete();
                                    //transac.Commit();
                                    //}
                                }
                                catch (Exception e)
                                {
                                    //if (transac != null)
                                    //    transac.Rollback();
                                    throw;
                                }
                                finally
                                {
                                    //string sql = @"EXEC MOVECOSTREPORTACTUALCOST " + fIntLoader.FILEINTEGRATIONID.ToString();
                                    //db.Database.ExecuteSqlCommand(sql);
                                    db.MOVECOSTREPORTACTUALCOST(fIntLoader.OID);

                                    if (conn.State == ConnectionState.Open)
                                    {
                                        conn.Close();
                                    }
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "COMMITMENT")
                        {
                            using (OleDbConnection conn = new OleDbConnection(connStr))
                            {
                                try
                                {
                                    if (conn.State == ConnectionState.Closed)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader != null)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        List<int> idxDelRow = new List<int>();

                                        var remark = "";
                                        
                                        List<string> pkProjectNo = new List<string>();
                                        List<string> pkWbsNo = new List<string>();
                                        List<string> pkYear = new List<string>();
                                        List<string> invalidFiscalYear = new List<string>();
                                        List<string> wrongFormatFiscalYear = new List<string>();
                                        List<string> pkMonth = new List<string>();
                                        List<string> invalidPeriod = new List<string>();
                                        List<string> wrongFormatPeriod = new List<string>();
                                        List<string> negativeValueInObjCrcy = new List<string>();
                                        List<string> wrongFormatValueInObjCrcy = new List<string>();

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            // Primary keys ProjectNo, WBSNo, YEAR, Month

                                            // ProjectNo
                                            if (string.IsNullOrWhiteSpace(row["Project Definition"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkProjectNo.Add(i.ToString());
                                                continue;
                                            }

                                            // WbsNo
                                            if (string.IsNullOrWhiteSpace(row["WBS Element"].ToString()))
                                            {
                                                idxDelRow.Add(i);
                                                ++failedCount;
                                                pkWbsNo.Add(i.ToString());
                                                continue;
                                            }

                                            //year
                                            int year;
                                            if (int.TryParse(row["Fiscal Year"].ToString().Trim(), out year) || string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    pkYear.Add(i.ToString());
                                                    continue;
                                                }

                                                if (year <= 0)
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    invalidFiscalYear.Add(i.ToString());
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatFiscalYear.Add(i.ToString());
                                                continue;
                                            }

                                            // Month     
                                            int month;
                                            if (int.TryParse(row["Period"].ToString().Trim(), out month) || string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    pkMonth.Add(i.ToString());
                                                    continue;
                                                }

                                                if (month <= 0)
                                                {
                                                    idxDelRow.Add(i);
                                                    ++failedCount;
                                                    invalidPeriod.Add(i.ToString());
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatPeriod.Add(i.ToString());
                                                continue;
                                            }

                                            float amount;
                                            if (float.TryParse(row["Value in Obj# Crcy"].ToString().Trim(), out amount) || string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                                {
                                                    row["Value in Obj# Crcy"] = null;
                                                    table.AcceptChanges();
                                                }

                                                //if (amount < 0)
                                                //{
                                                //    negativeValueInObjCrcy.Add(i.ToString());
                                                //}
                                            }
                                            else
                                            {
                                                ++failedCount;
                                                idxDelRow.Add(i);
                                                wrongFormatValueInObjCrcy.Add(i.ToString());
                                                continue;
                                            }

                                            if (string.IsNullOrWhiteSpace(row["Object"].ToString()))
                                            {
                                                row["Object"] = null;
                                                table.AcceptChanges();
                                            }

                                            if (string.IsNullOrWhiteSpace(row["Object type"].ToString()))
                                            {
                                                row["Object type"] = null;
                                                table.AcceptChanges();
                                            }

                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRow.Count; j++)
                                        {
                                            table.Rows[idxDelRow[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();
                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.COSTREPORTCOMMITMENTTEMP";

                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Project Definition", "PROJECTNO");
                                            bulkCopy.ColumnMappings.Add("WBS Element", "WBSNO");
                                            bulkCopy.ColumnMappings.Add("Object", "NETWORK");
                                            bulkCopy.ColumnMappings.Add("Object type", "OBJECTTYPE");
                                            bulkCopy.ColumnMappings.Add("Fiscal Year", "YEAR");
                                            bulkCopy.ColumnMappings.Add("Period", "MONTH");
                                            bulkCopy.ColumnMappings.Add("Value in Obj# Crcy", "AMOUNT");

                                            bulkCopy.WriteToServer(table);

                                            fIntLoader.LOADERBY = ntUser;
                                            fIntLoader.LOADERDATE = DateTime.Now;
                                            fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                            db.Entry(fIntLoader).State = EntityState.Modified;
                                            db.SaveChanges();

                                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount });
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    throw;
                                }
                                finally
                                {
                                    //string sql = @"EXEC MOVECOSTREPORTCOMMITMENT " + fIntLoader.FILEINTEGRATIONID.ToString();
                                    //db.Database.ExecuteSqlCommand(sql);
                                    db.MOVECOSTREPORTCOMMITMENT(fIntLoader.OID);
                                    if (conn.State == ConnectionState.Open)
                                    {
                                        conn.Close();
                                    }
                                }
                            }
                        }
                        else continue;

                        //string sql = @"EXEC MOVECOSTREPORTACTUALCOST, 'fIntLoader.FILEINTEGRATIONID'";
                        //db.Database.ExecuteSqlCommand(sql);
                    }
                }

                string sourceFile = fileName;
                string destinationFile = fileInt.SHAREFOLDER + "backup\\" + fileInt.FILENAME;
                if (result[0].failed_count == 0)
                {
                    if (File.Exists(destinationFile))
                        File.Delete(destinationFile);
                    File.Move(sourceFile, destinationFile);
                }
            }
            return result;
        }



        [HttpGet]
        [ActionName("ProcessCostLoaderCheckFirst")]
        public object ProcessCostLoader(string param)
        {
            var ntUser = GetCurrentNTUserId();
            List<dynamic> result = new List<dynamic>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "COST").ToList();
            string fileName = "";
            string purposeFile = "";


            var iCostReportElementEdit = db.COSTREPORTELEMENTs;
            IList<COSTREPORTELEMENT> iCostReportElementAdd = new List<COSTREPORTELEMENT>();

            var iCostReportNetworkEdit = db.COSTREPORTNETWORKs;
            IList<COSTREPORTNETWORK> iCostReportNetworkAdd = new List<COSTREPORTNETWORK>();

            var iCostReportBudgetEdit = db.COSTREPORTBUDGETs;
            IList<COSTREPORTBUDGET> iCostReportBudgetAdd = new List<COSTREPORTBUDGET>();

            var iCostReportActualCostEdit = db.COSTREPORTACTUALCOSTs;
            IList<COSTREPORTACTUALCOST> iCostReportActualCostAdd = new List<COSTREPORTACTUALCOST>();

            var iCostReportActualCostTempEdit = db.COSTREPORTACTUALCOSTTEMPs;
            IList<COSTREPORTACTUALCOSTTEMP> iCostReportActualCostTempAdd = new List<COSTREPORTACTUALCOSTTEMP>();

            var iCostReportCommitmentEdit = db.COSTREPORTCOMMITMENTs;
            IList<COSTREPORTCOMMITMENT> iCostReportCommitmentAdd = new List<COSTREPORTCOMMITMENT>();

            IList<FILEINTEGRATIONLOADER> iListfIntLoader = new List<FILEINTEGRATIONLOADER>();

            bool isDoLoad = true;
            List<objFiles> lFilesMoved = new List<objFiles>();

            int idx = 0;

            string excelConnStr03 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0 Xml;HDR=YES;IMEX=1\"";
            string excelConnStr07 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";
            string connStr = "";

            FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();

            //All files must exist
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {               

                string backupFolder = fileInt.SHAREFOLDER + "backup\\";
                if (!Directory.Exists(backupFolder))
                    Directory.CreateDirectory(backupFolder);

                lFilesMoved.Add(new objFiles { id = idx + 1, sourceFile = fileInt.SHAREFOLDER + fileInt.FILENAME, destinationFile = backupFolder + fileInt.FILENAME });
                ++idx;

                if (File.Exists(fileInt.SHAREFOLDER + fileInt.FILENAME))
                {
                    isDoLoad = isDoLoad && true;
                }
                else {
                    isDoLoad = isDoLoad && false;
                    result.Add(new
                    {
                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        failed_detail = "File does not exist."
                    });
                }
            }

            if (!isDoLoad)
            { // files does not exist
                return result;
            }
            else
            {
                //all required columns must exist

                foreach (FILEINTEGRATION fileInt in listFileIntegration)
                {
                    fileName = fileInt.SHAREFOLDER + fileInt.FILENAME;
                    var shareFolder = fileInt.SHAREFOLDER;

                    if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                    {
                        List<string> columnNames = new List<string>() {
                            "Project definition","Level","WBS element", "Name"
                        };
                        string ext = Path.GetExtension(fileName);
                        if (Path.GetExtension(fileName).ToLower() == ".xlsx" || Path.GetExtension(fileName).ToLower() == "xlsx")
                            connStr = string.Format(excelConnStr07, fileName);
                        else if (Path.GetExtension(fileName).ToLower() == ".xls" || Path.GetExtension(fileName).ToLower() == "xls")
                            connStr = string.Format(excelConnStr03,fileName);
                        else
                        {
                            result.Add(new
                            {
                                filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                failed_detail = "File extension is not valid."
                            });
                            return result;
                        }
                                                
                        using (OleDbConnection conn = new OleDbConnection(connStr))
                        {
                            try
                            {
                                if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                {
                                    conn.Open();
                                }
                                OleDbCommand comAll = new OleDbCommand("SELECT * FROM [Sheet1$]", conn);
                                OleDbDataReader readerAll = comAll.ExecuteReader();
                                List<string> columns = readerAll.GetSchemaTable().Rows
                                                                        .Cast<DataRow>()
                                                                        .Select(r => (string)r["ColumnName"])
                                                                        .ToList();
                                bool isNotFoundColumn = columnNames.Except(columns).Any();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }

                    else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "NETWORK")
                    {

                    }
                    else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "BUDGET")
                    {

                    }
                    else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUALCIDB" || fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUALSIDB")
                    {

                    }
                    else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "COMMITMENT")
                    {

                    }
                    else continue;
                }

                foreach (FILEINTEGRATION fileInt in listFileIntegration)
                {
                    // do sqlbulk
                }
            }
            return result;
        }


        [HttpGet]
        [ActionName("ProcessLoaderMaterialX")]
        public object ProcessLoaderMaterialX(string param)
        {
            var ntUser = GetCurrentNTUserId();
            List<object> result = new List<object>();
            List<object> listFilIntIds = new List<object>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "MATERIAL").ToList();
            string fileName = "";
            string purposeFile = "";

            string localhost = global::PTVI.iPROM.WebApi.Properties.Settings.Default.InstanceName; //instance name sqlserver
            string idUser = global::PTVI.iPROM.WebApi.Properties.Settings.Default.idUser; //User sqlserver
            string idCatalog = global::PTVI.iPROM.WebApi.Properties.Settings.Default.idCatalog; //catalog sqlserver
            string idPassword = global::PTVI.iPROM.WebApi.Properties.Settings.Default.idPassword; //password sqlserver
            
            string destConnString = @"Password=" + idPassword + "; Persist Security Info=True; User ID=" + idUser + "; Initial Catalog=" + idCatalog + "; Data Source=" + localhost;

            FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();

            //edit
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                var shareFolder = fileInt.SHAREFOLDER;
                fileName = shareFolder + fileInt.FILENAME;
                purposeFile = fileInt.PURPOSEFILE;

                string excelConnStr03 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0 Xml;HDR=YES;IMEX=1\"";
                string excelConnStr07 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";

                if (!Directory.Exists(shareFolder))
                {
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = ntUser;
                    fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | Sharefolder does not exist";
                    db.Entry(fIntLoader).State = EntityState.Added;
                    result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = fIntLoader.REMARK });
                    continue;
                }
                else
                {
                    if (!File.Exists(fileName))
                    {
                        //log file does not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | File does not exist";
                        fIntLoader.LOADERBY = ntUser;
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;
                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = fIntLoader.REMARK });
                        continue;
                    }
                    else
                    {
                        try
                        {
                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            string connStr = "";

                            db.Entry(fIntLoader).State = EntityState.Added;
                            db.SaveChanges();

                            if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                                connStr = excelConnStr07;
                            else if (fileInt.FILENAME.ToUpper().Contains(".XLS"))
                                connStr = excelConnStr03;
                            else
                            {
                                fIntLoader.LOADERBY = ntUser;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | This file doesn't have xlsx or xls extension.";
                                db.Entry(fIntLoader).State = EntityState.Modified;
                                db.SaveChanges();
                                result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = fIntLoader.REMARK });
                                continue;
                            }

                            using (OleDbConnection conn = new OleDbConnection(connStr))
                            {
                                if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                {
                                    conn.Open();
                                }

                                if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                                {
                                    OleDbCommand com = new OleDbCommand(@"Select [Project definition], [WBS element], [Network], [Activity], [BOM item], " +
                                                        "[Material], [Material text], [Item text line 1], [Requirement quantity], [Quantity withdrawn], " +
                                                        "[Quantity received], [Base unit of measure], [Requirements date], [Goods recipient]," +
                                                        "[Reservation], [Item], [Purchase requisition], [Requisition item], [Purchase Order], [Purchase Order Item]" +
                                                        "from [Sheet1$]", conn);

                                    using (OleDbDataReader reader = com.ExecuteReader())
                                    {
                                        int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                        StringBuilder errorRows = new StringBuilder();
                                        List<int> idxDelRows = new List<int>();

                                        if (reader.HasRows)
                                        {
                                            DataTable table = new DataTable();
                                            var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                            col1.DefaultValue = fIntLoader.OID;
                                            var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                            col2.DefaultValue = DateTime.Now;
                                            var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                            col3.DefaultValue = ntUser;
                                            var col4 = table.Columns.Add("MATERIALDESC", typeof(string));
                                            col4.DefaultValue = DBNull.Value;

                                            table.AcceptChanges();
                                            table.Load(reader);

                                            int i = 0;
                                            foreach (DataRow row in table.Rows)
                                            {
                                                i++;

                                                //4.Primary key table MaterialComponent: ProjectID, WBSNo, Network, NetworkActivity, LineItem

                                                // pk - ProjectID
                                                if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk - WBSNo
                                                if (string.IsNullOrWhiteSpace(row["WBS element"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk - Network
                                                if (string.IsNullOrWhiteSpace(row["Network"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk - NetworkActivity
                                                if (string.IsNullOrWhiteSpace(row["Activity"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk - LineItem
                                                if (string.IsNullOrWhiteSpace(row["BOM item"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // non pk
                                                if (string.IsNullOrWhiteSpace(row["Material"].ToString()))
                                                {
                                                    row["Material"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Material text"].ToString()))
                                                {
                                                    if (string.IsNullOrWhiteSpace(row["Item text line 1"].ToString()))
                                                    {
                                                        col4.DefaultValue = DBNull.Value;
                                                        table.AcceptChanges();
                                                    }
                                                    else
                                                    {
                                                        col4.DefaultValue = row["Item text line 1"].ToString();
                                                        table.AcceptChanges();
                                                    }
                                                }
                                                else {
                                                    col4.DefaultValue = row["Material text"].ToString();
                                                    table.AcceptChanges();
                                                }

                                                int ORDERQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Requirement quantity"].ToString()))
                                                {
                                                    row["Requirement quantity"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!int.TryParse(row["Requirement quantity"].ToString().Trim(), out ORDERQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                int WIDTHDRAWNQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Quantity withdrawn"].ToString()))
                                                {
                                                    row["Quantity withdrawn"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!int.TryParse(row["Quantity withdrawn"].ToString().Trim(), out WIDTHDRAWNQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                int RECEIVEDQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Quantity received"].ToString()))
                                                {
                                                    row["Quantity received"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!int.TryParse(row["Quantity received"].ToString().Trim(), out RECEIVEDQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Base unit of measure"].ToString()))
                                                {
                                                    row["Base unit of measure"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                DateTime PLANONSITE;
                                                if (string.IsNullOrWhiteSpace(row["Requirements date"].ToString()))
                                                {
                                                    row["Requirements date"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Requirements date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out PLANONSITE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Goods recipient"].ToString()))
                                                {
                                                    row["Goods recipient"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Reservation"].ToString()))
                                                {
                                                    row["Reservation"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                int RESERVATIONITEMNO;
                                                if (string.IsNullOrWhiteSpace(row["Item"].ToString()))
                                                {
                                                    row["Item"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!int.TryParse(row["Item"].ToString().Trim(), out RESERVATIONITEMNO))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Purchase requisition"].ToString()))
                                                {
                                                    row["Purchase requisition"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Requisition item"].ToString()))
                                                {
                                                    row["Requisition item"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Purchase Order"].ToString()))
                                                {
                                                    row["Purchase Order"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                if (string.IsNullOrWhiteSpace(row["Purchase Order Item"].ToString()))
                                                {
                                                    row["Purchase Order Item"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                ++succeedCount;
                                            }

                                            for (int j = 0; j < idxDelRows.Count; j++)
                                            {
                                                table.Rows[idxDelRows[j] - 1].Delete();
                                            }
                                            table.AcceptChanges();

                                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                            {
                                                bulkCopy.DestinationTableName = "dbo.MATERIALCOMPONENTTEMP";

                                                bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                                bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                                bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                                bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                                bulkCopy.ColumnMappings.Add("WBS element", "WBSNO");
                                                bulkCopy.ColumnMappings.Add("Network", "NETWORK");
                                                bulkCopy.ColumnMappings.Add("Activity", "NETWORKACTIVITY");
                                                bulkCopy.ColumnMappings.Add("BOM item", "LINEITEM");
                                                bulkCopy.ColumnMappings.Add("Material", "MATERIAL");
                                                bulkCopy.ColumnMappings.Add("MATERIALDESC", "MATERIALDESC");
                                                bulkCopy.ColumnMappings.Add("Requirement quantity", "ORDERQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Quantity withdrawn", "WITHDRAWNQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Quantity received", "RECEIVEDQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Base unit of measure", "UOM");
                                                bulkCopy.ColumnMappings.Add("Requirements date", "PLANONSITE");
                                                bulkCopy.ColumnMappings.Add("Goods recipient", "ENGINEER");
                                                bulkCopy.ColumnMappings.Add("Reservation", "RESERVATION");
                                                bulkCopy.ColumnMappings.Add("Item", "RESERVATIONITEMNO");
                                                bulkCopy.ColumnMappings.Add("Purchase requisition", "PRNO");
                                                bulkCopy.ColumnMappings.Add("Requisition item", "PRITEMNO");
                                                bulkCopy.ColumnMappings.Add("Purchase Order", "PONO");
                                                bulkCopy.ColumnMappings.Add("Purchase Order Item", "POITEMNO");

                                                bulkCopy.WriteToServer(table);
                                            }
                                        }
                                        fIntLoader.LOADERBY = ntUser;
                                        fIntLoader.LOADERDATE = DateTime.Now;
                                        fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                        db.Entry(fIntLoader).State = EntityState.Modified;
                                        db.SaveChanges();
                                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                        listFilIntIds.Add(new objFInt { id = fileInt.OID, purposeFile = fileInt.PURPOSEFILE });
                                    }
                                }
                                else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PR")
                                {
                                    OleDbCommand com = new OleDbCommand(@"Select [Purchase Requisition], [Item of Requisition], [Requisition Date], [Purchase Order Date], [Req# Tracking Number] from [Sheet1$]", conn);

                                    using (OleDbDataReader reader = com.ExecuteReader())
                                    {
                                        int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                        StringBuilder errorRows = new StringBuilder();
                                        List<int> idxDelRows = new List<int>();

                                        if (reader.HasRows)
                                        {
                                            DataTable table = new DataTable();
                                            var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                            col1.DefaultValue = fIntLoader.OID;
                                            var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                            col2.DefaultValue = DateTime.Now;
                                            var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                            col3.DefaultValue = ntUser;
                                            table.AcceptChanges();
                                            table.Load(reader);

                                            int i = 0;
                                            foreach (DataRow row in table.Rows)
                                            {
                                                i++;

                                                //5. Primary key table MaterialPurchase: PRNo, PRItemNo

                                                // pk PRNo
                                                if (string.IsNullOrWhiteSpace(row["Purchase Requisition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk PRItemNo
                                                if (string.IsNullOrWhiteSpace(row["Item of Requisition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                //non pk
                                                DateTime PRISSUEDDATE;
                                                if (string.IsNullOrWhiteSpace(row["Requisition Date"].ToString()))
                                                {
                                                    row["Requirements date"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Requisition Date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out PRISSUEDDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                DateTime PORAISEDDATE;
                                                if (string.IsNullOrWhiteSpace(row["Purchase Order Date"].ToString()))
                                                {
                                                    row["Purchase Order Date"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Purchase Order Date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out PORAISEDDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }
                                                var xxx = row["Req# Tracking Number"].ToString();
                                                if (string.IsNullOrWhiteSpace(row["Req# Tracking Number"].ToString()))
                                                {
                                                    row["Req# Tracking Number"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                ++succeedCount;
                                            }

                                            for (int j = 0; j < idxDelRows.Count; j++)
                                            {
                                                table.Rows[idxDelRows[j] - 1].Delete();
                                            }
                                            table.AcceptChanges();

                                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                            {
                                                bulkCopy.DestinationTableName = "dbo.MATERIALPURCHASETEMP";

                                                bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                                bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                                bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                                bulkCopy.ColumnMappings.Add("Purchase Requisition", "PRNO");
                                                bulkCopy.ColumnMappings.Add("Item of Requisition", "PRITEMNO");
                                                bulkCopy.ColumnMappings.Add("Requisition Date", "PRISSUEDDATE");
                                                bulkCopy.ColumnMappings.Add("Purchase Order Date", "PORAISEDDATE");
                                                bulkCopy.ColumnMappings.Add("Req# Tracking Number", "TRACKINGNO");

                                                bulkCopy.WriteToServer(table);
                                            }
                                        }
                                        fIntLoader.LOADERBY = ntUser;
                                        fIntLoader.LOADERDATE = DateTime.Now;
                                        fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                        db.Entry(fIntLoader).State = EntityState.Modified;
                                        db.SaveChanges();

                                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                        listFilIntIds.Add( new objFInt { id = fileInt.OID, purposeFile = fileInt.PURPOSEFILE });
                                    }
                                }
                                else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PO")
                                {
                                    OleDbCommand com = new OleDbCommand(@"Select [Purchase Requisition], [Item of Requisition], [Delivery Date], [Order Quantity], [Currency], [Net price],
                                                            [Order Price Unit], [Net Order Value], [Qty Delivered], [Quantity Received], [Purchasing Group], [Vendor/supplying plant] from [Sheet1$]", conn);

                                    using (OleDbDataReader reader = com.ExecuteReader())
                                    {
                                        int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                        StringBuilder errorRows = new StringBuilder();
                                        List<int> idxDelRows = new List<int>();

                                        if (reader.HasRows)
                                        {
                                            DataTable table = new DataTable();
                                            var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                            col1.DefaultValue = fIntLoader.OID;
                                            var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                            col2.DefaultValue = DateTime.Now;
                                            var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                            col3.DefaultValue = ntUser;
                                            table.AcceptChanges();
                                            table.Load(reader);

                                            int i = 0;
                                            foreach (DataRow row in table.Rows)
                                            {
                                                i++;

                                                //6.Primary key table MaterialSchedule: PRNo, PRItemNo

                                                // pk PRNo
                                                if (string.IsNullOrWhiteSpace(row["Purchase Requisition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk PRItemNo
                                                if (string.IsNullOrWhiteSpace(row["Item of Requisition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // non pk

                                                DateTime DELIVERYDATE;
                                                if (string.IsNullOrWhiteSpace(row["Delivery Date"].ToString()))
                                                {
                                                    row["Delivery Date"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Delivery Date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DELIVERYDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                float ORDEREDQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Order Quantity"].ToString()))
                                                {
                                                    row["Order Quantity"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!float.TryParse(row["Order Quantity"].ToString().Trim(), out ORDEREDQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Currency"].ToString()))
                                                {
                                                    row["Currency"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                float UNITPRICE;
                                                if (string.IsNullOrWhiteSpace(row["Net price"].ToString()))
                                                {
                                                    row["Net price"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!float.TryParse(row["Net price"].ToString().Trim(), out UNITPRICE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Order Price Unit"].ToString()))
                                                {
                                                    row["Order Price Unit"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                float TOTALPRICE;
                                                if (string.IsNullOrWhiteSpace(row["Net Order Value"].ToString()))
                                                {
                                                    row["Net Order Value"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!float.TryParse(row["Net Order Value"].ToString().Trim(), out TOTALPRICE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                float DELIVEREDQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Qty Delivered"].ToString()))
                                                {
                                                    row["Qty Delivered"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!float.TryParse(row["Qty Delivered"].ToString().Trim(), out DELIVEREDQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                float RECEIVEDQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Quantity Received"].ToString()))
                                                {
                                                    row["Quantity Received"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!float.TryParse(row["Quantity Received"].ToString().Trim(), out RECEIVEDQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Purchasing Group"].ToString()))
                                                {
                                                    row["Purchasing Group"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                var yyy = row["Vendor/supplying plant"].ToString();

                                                if (string.IsNullOrWhiteSpace(row["Vendor/supplying plant"].ToString()))
                                                {
                                                    row["Vendor/supplying plant"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                ++succeedCount;
                                            }

                                            for (int j = 0; j < idxDelRows.Count; j++)
                                            {
                                                table.Rows[idxDelRows[j] - 1].Delete();
                                            }
                                            table.AcceptChanges();
                                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                            {
                                                bulkCopy.DestinationTableName = "dbo.MATERIALSCHEDULETEMP";

                                                bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                                bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                                bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                                bulkCopy.ColumnMappings.Add("Purchase Requisition", "PRNO");
                                                bulkCopy.ColumnMappings.Add("Item of Requisition", "PRITEMNO");
                                                bulkCopy.ColumnMappings.Add("Delivery Date", "DELIVERYDATE");
                                                bulkCopy.ColumnMappings.Add("Order Quantity", "ORDEREDQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Currency", "CURRENCY");
                                                bulkCopy.ColumnMappings.Add("Net price", "UNITPRICE");
                                                bulkCopy.ColumnMappings.Add("Order Price Unit", "UNITQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Net Order Value", "TOTALPRICE");
                                                bulkCopy.ColumnMappings.Add("Qty Delivered", "DELIVEREDQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Quantity Received", "RECEIVEDQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Purchasing Group", "BUYER");
                                                bulkCopy.ColumnMappings.Add("Vendor/supplying plant", "VENDOR");

                                                bulkCopy.WriteToServer(table);
                                            }
                                        }
                                        fIntLoader.LOADERBY = ntUser;
                                        fIntLoader.LOADERDATE = DateTime.Now;
                                        fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                        db.Entry(fIntLoader).State = EntityState.Modified;
                                        db.SaveChanges();

                                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                        listFilIntIds.Add(new objFInt { id = fileInt.OID, purposeFile = fileInt.PURPOSEFILE });
                                    }
                                }
                                else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "DELIVERY")
                                {
                                    OleDbCommand com = new OleDbCommand(@"Select [PR], [Request Item PR], [Reservation], [Request Item Reservation], [Code Lookup],
                                                            [Special Stock], [Reservation Withdrawal Qty], [Incoterms PO (Part 1)], [Incoterms PO (Part 2)], 
                                                            [STO Number], [Delivery No#], [Shipment No#], [Shipment Start], [PO Delivery Date], [Shipment End], 
                                                            [Shipment Route], [Unloading Point], [Latest Goods Receipt Date] from [All_Projects$]", conn);

                                    using (OleDbDataReader reader = com.ExecuteReader())
                                    {
                                        int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                        StringBuilder errorRows = new StringBuilder();
                                        List<int> idxDelRows = new List<int>();

                                        if (reader.HasRows)
                                        {
                                            DataTable table = new DataTable();
                                            var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                            col1.DefaultValue = fIntLoader.OID;
                                            var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                            col2.DefaultValue = DateTime.Now;
                                            var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                            col3.DefaultValue = ntUser;
                                            table.AcceptChanges();
                                            table.Load(reader);

                                            int i = 0;
                                            foreach (DataRow row in table.Rows)
                                            {
                                                i++;

                                                // 7.	Primary key table MaterialStatus: PRNo, PRItemNo

                                                // pk PRNo
                                                if (string.IsNullOrWhiteSpace(row["PR"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk PRItemNo
                                                if (string.IsNullOrWhiteSpace(row["Request Item PR"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // non pk
                                                if (string.IsNullOrWhiteSpace(row["Reservation"].ToString()))
                                                {
                                                    row["Reservation"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                int RESERVATIONITEMNO;
                                                if (string.IsNullOrWhiteSpace(row["Request Item Reservation"].ToString()))
                                                {
                                                    row["Request Item Reservation"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!int.TryParse(row["Request Item Reservation"].ToString().Trim(), out RESERVATIONITEMNO))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Code Lookup"].ToString()))
                                                {
                                                    row["Code Lookup"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Special Stock"].ToString()))
                                                {
                                                    row["Special Stock"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                int RESERVATIONWITHDRAWALQUANTITY;
                                                if (string.IsNullOrWhiteSpace(row["Reservation Withdrawal Qty"].ToString()))
                                                {
                                                    row["Reservation Withdrawal Qty"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!int.TryParse(row["Reservation Withdrawal Qty"].ToString().Trim(), out RESERVATIONWITHDRAWALQUANTITY))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Incoterms PO (Part 1)"].ToString()))
                                                {
                                                    row["Incoterms PO (Part 1)"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Incoterms PO (Part 2)"].ToString()))
                                                {
                                                    row["Incoterms PO (Part 2)"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                if (string.IsNullOrWhiteSpace(row["STO Number"].ToString()))
                                                {
                                                    row["STO Number"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                if (string.IsNullOrWhiteSpace(row["Delivery No#"].ToString()))
                                                {
                                                    row["Delivery No#"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                if (string.IsNullOrWhiteSpace(row["Shipment No#"].ToString()))
                                                {
                                                    row["Shipment No#"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                DateTime SHIPMENTSTARTDATE;
                                                if (string.IsNullOrWhiteSpace(row["Shipment Start"].ToString()))
                                                {
                                                    row["Shipment Start"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Shipment Start"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SHIPMENTSTARTDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                    else {
                                                        row["Shipment Start"] = row["Shipment Start"].ToString().Replace(".", "/");
                                                        table.AcceptChanges();
                                                    }
                                                }
                                                DateTime ETASITEDATE;
                                                if (string.IsNullOrWhiteSpace(row["PO Delivery Date"].ToString()))
                                                {
                                                    row["PO Delivery Date"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["PO Delivery Date"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ETASITEDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                    else {
                                                        row["PO Delivery Date"] = row["PO Delivery Date"].ToString().Replace(".", "/");
                                                        table.AcceptChanges();
                                                    }
                                                }
                                                DateTime SHIPMENTENDDATE;
                                                if (string.IsNullOrWhiteSpace(row["Shipment End"].ToString()))
                                                {
                                                    row["Shipment End"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Shipment End"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SHIPMENTENDDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                    else {
                                                        row["Shipment End"] = row["Shipment End"].ToString().Replace(".", "/");
                                                        table.AcceptChanges();
                                                    }
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Shipment Route"].ToString()))
                                                {
                                                    row["Shipment Route"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                if (string.IsNullOrWhiteSpace(row["Unloading Point"].ToString()))
                                                {
                                                    row["Unloading Point"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                DateTime ACTUALDELIVERYDATE;
                                                if (string.IsNullOrWhiteSpace(row["Latest Goods Receipt Date"].ToString()))
                                                {
                                                    row["Latest Goods Receipt Date"] = DBNull.Value; table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    if (!DateTime.TryParseExact(row["Latest Goods Receipt Date"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ACTUALDELIVERYDATE))
                                                    {
                                                        ++failedCount;
                                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                        idxDelRows.Add(i);
                                                        continue;
                                                    }
                                                    else {
                                                        row["Latest Goods Receipt Date"] = row["Latest Goods Receipt Date"].ToString().Replace(".", "/");
                                                        table.AcceptChanges();
                                                    }
                                                }
                                                ++succeedCount;
                                            }

                                            for (int j = 0; j < idxDelRows.Count; j++)
                                            {
                                                table.Rows[idxDelRows[j] - 1].Delete();
                                            }
                                            table.AcceptChanges();

                                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                            {
                                                bulkCopy.DestinationTableName = "dbo.MATERIALSTATUSTEMP";

                                                bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                                bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                                bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                                bulkCopy.ColumnMappings.Add("PR", "PRNO");
                                                bulkCopy.ColumnMappings.Add("Request Item PR", "PRITEMNO");
                                                bulkCopy.ColumnMappings.Add("Reservation", "RESERVATION");
                                                bulkCopy.ColumnMappings.Add("Request Item Reservation", "RESERVATIONITEMNO");
                                                bulkCopy.ColumnMappings.Add("Code Lookup", "CODELOOKUP");
                                                bulkCopy.ColumnMappings.Add("Special Stock", "SPECIALSTOCK");
                                                bulkCopy.ColumnMappings.Add("Reservation Withdrawal Qty", "RESERVATIONWITHDRAWALQUANTITY");
                                                bulkCopy.ColumnMappings.Add("Incoterms PO (Part 1)", "INCOTERMSPO");
                                                bulkCopy.ColumnMappings.Add("Incoterms PO (Part 2)", "TRAFFICMETHOD");
                                                bulkCopy.ColumnMappings.Add("STO Number", "STONO");
                                                bulkCopy.ColumnMappings.Add("Delivery No#", "DELIVERYNO");
                                                bulkCopy.ColumnMappings.Add("Shipment No#", "SHIPMENTNO");
                                                bulkCopy.ColumnMappings.Add("Shipment Start", "SHIPMENTSTARTDATE");
                                                bulkCopy.ColumnMappings.Add("PO Delivery Date", "ETASITEDATE");
                                                bulkCopy.ColumnMappings.Add("Shipment End", "SHIPMENTENDDATE");
                                                bulkCopy.ColumnMappings.Add("Shipment Route", "SHIPMENTROUTE");
                                                bulkCopy.ColumnMappings.Add("Unloading Point", "LOCATION");
                                                bulkCopy.ColumnMappings.Add("Latest Goods Receipt Date", "ACTUALDELIVERYDATE");

                                                bulkCopy.WriteToServer(table);
                                            }
                                        }
                                        fIntLoader.LOADERBY = ntUser;
                                        fIntLoader.LOADERDATE = DateTime.Now;
                                        fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                        db.Entry(fIntLoader).State = EntityState.Modified;
                                        db.SaveChanges();
                                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    }
                                }
                                else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PRGROUP")
                                {
                                    OleDbCommand com = new OleDbCommand(@"Select [Purchase Requisition], [Item of Requisition], [Purchasing Group], [Purchasing group Description] from [Sheet1$]", conn);
                                    using (OleDbDataReader reader = com.ExecuteReader())
                                    {
                                        int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                        StringBuilder errorRows = new StringBuilder();
                                        List<int> idxDelRows = new List<int>();

                                        if (reader.HasRows)
                                        {
                                            DataTable table = new DataTable();
                                            var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                            col1.DefaultValue = fIntLoader.OID;
                                            var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                            col2.DefaultValue = DateTime.Now;
                                            var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                            col3.DefaultValue = ntUser;
                                            table.AcceptChanges();
                                            table.Load(reader);

                                            int i = 0;
                                            foreach (DataRow row in table.Rows)
                                            {
                                                i++;

                                                // 7.	Primary key table MaterialStatus: PRNo, PRItemNo

                                                // pk PRNo
                                                if (string.IsNullOrWhiteSpace(row["Purchase Requisition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // pk PRItemNo
                                                if (string.IsNullOrWhiteSpace(row["Item of Requisition"].ToString()))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }

                                                // non pk
                                                if (string.IsNullOrWhiteSpace(row["Purchasing Group"].ToString()))
                                                {
                                                    row["Purchasing Group"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                if (string.IsNullOrWhiteSpace(row["Purchasing group Description"].ToString()))
                                                {
                                                    row["Purchasing group Description"] = DBNull.Value; table.AcceptChanges();
                                                }

                                                ++succeedCount;
                                            }

                                            for (int j = 0; j < idxDelRows.Count; j++)
                                            {
                                                table.Rows[idxDelRows[j] - 1].Delete();
                                            }
                                            table.AcceptChanges();

                                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                            {
                                                bulkCopy.DestinationTableName = "dbo.MATERIALPURCHASEGROUPTEMP";

                                                bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                                bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                                bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                                bulkCopy.ColumnMappings.Add("Purchase Requisition", "PRNO");
                                                bulkCopy.ColumnMappings.Add("Item of Requisition", "PRITEMNO");
                                                bulkCopy.ColumnMappings.Add("Purchasing Group", "PRGROUPCODE");
                                                bulkCopy.ColumnMappings.Add("Purchasing group Description", "PRGROUPDESC");

                                                bulkCopy.WriteToServer(table);
                                            }
                                        }
                                        fIntLoader.LOADERBY = ntUser;
                                        fIntLoader.LOADERDATE = DateTime.Now;
                                        fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                        db.Entry(fIntLoader).State = EntityState.Modified;
                                        db.SaveChanges();

                                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    }
                                }
                            }
                        }
                        catch (Exception e) {
                            listFilIntIds = null;
                            throw e;
                        }
                        finally
                        {
                            if(listFilIntIds.Count > 0)
                            {
                                foreach (objFInt x in listFilIntIds)
                                {
                                    switch (x.purposeFile.ToUpper())
                                    {
                                        case "HEADER":
                                            db.MOVEMATERIALCOMPONENT(x.id);
                                            break;
                                        case "PR":
                                            db.MOVEMATERIALPURCHASETEMP(x.id);
                                            break;
                                        case "PO":
                                            db.MOVEMATERIALSCHEDULETEMP(x.id);
                                            break;
                                        case "DELIVERY":
                                            db.MOVEMATERIALSTATUSTEMP(x.id);
                                            break;
                                        case "PRGROUP":
                                            db.MOVEMATERIALPURCHASEGROUPTEMP(x.id);
                                            break;
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
            return result;
        }

        public class objFInt
        {
            public int id { get; set; }
            public string purposeFile { get; set; }
        }

        [HttpGet]
        [ActionName("ProcessLoaderMaterial")]
        public object ProcessLoaderMaterial(string param)
        {
            var ntUser = GetCurrentNTUserId();
            List<object> result = new List<object>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "MATERIAL").ToList();
            string fileName = "";
            string purposeFile = "";
            
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {                
                var shareFolder = fileInt.SHAREFOLDER;
                fileName = shareFolder + fileInt.FILENAME;
                purposeFile = fileInt.PURPOSEFILE;
                
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                
                if (!Directory.Exists(shareFolder))
                {
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = ntUser;
                    fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | Sharefolder does not exist";

                    db.Entry(fIntLoader).State = EntityState.Added;

                    //failedDetail.Add(fIntLoader.REMARK);
                    result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = fIntLoader.REMARK });

                    continue;
                }
                else
                {
                    if (!File.Exists(fileName))
                    {
                        //log file does not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | File does not exist";
                        fIntLoader.LOADERBY = ntUser;
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;
                        
                        result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = fIntLoader.REMARK });

                        continue;
                    }
                    else
                    {                        
                        string localhost = global::PTVI.iPROM.WebApi.Properties.Settings.Default.InstanceName; //instance name sqlserver
                        string idUser = global::PTVI.iPROM.WebApi.Properties.Settings.Default.idUser; //User sqlserver
                        string idCatalog = global::PTVI.iPROM.WebApi.Properties.Settings.Default.idCatalog; //catalog sqlserver
                        string idPassword = global::PTVI.iPROM.WebApi.Properties.Settings.Default.idPassword; //password sqlserver
                        string excelConnStr03 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0 Xml;HDR=YES;IMEX=1\"";
                        string excelConnStr07 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";
                        string destConnString = @"Password=" + idPassword + "; Persist Security Info=True; User ID=" + idUser + "; Initial Catalog=" + idCatalog + "; Data Source=" + localhost;

                        //add new file integration loader
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;

                        string connStr = "";

                        //"HDR=Yes;" indicates that the first row contains columnnames, not data. "HDR=No;" indicates the opposite.
                        //IMEX are:
                        //0 is Export mode
                        //1 is Import mode
                        //2 is Linked mode(full update capabilities)
                        db.Entry(fIntLoader).State = EntityState.Added;
                        db.SaveChanges();
                        if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                            connStr = excelConnStr07;
                        else if (fileInt.FILENAME.ToUpper().Contains(".XLS"))
                            connStr = excelConnStr03;
                        else
                        {
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;
                            fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | This file doesn't have xlsx or xls extension.";
                            db.Entry(fIntLoader).State = EntityState.Modified;
                            db.SaveChanges();
                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = fIntLoader.REMARK });
                            continue;
                        }

                        if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                        {
                            try
                            {
                                int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                StringBuilder errorRows = new StringBuilder();
                                List<int> idxDelRows = new List<int>();

                                using (OleDbConnection conn = new OleDbConnection(connStr))
                                {
                                    if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Project definition], [WBS element], [Network], [Activity], [BOM item], " +
                                                        "[Material], [Material text], [Item text line 1], [Requirement quantity], [Quantity withdrawn], " +
                                                        "[Quantity received], [Base unit of measure], [Requirements date], [Goods recipient]," +
                                                        "[Reservation], [Item], [Purchase requisition], [Requisition item], [Purchase Order], [Purchase Order Item]" +
                                                        "from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        var col4 = table.Columns.Add("MATERIALDESC", typeof(string));
                                        col4.DefaultValue = DBNull.Value;

                                        table.AcceptChanges();
                                        table.Load(reader);

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++; 

                                            //4.Primary key table MaterialComponent: ProjectID, WBSNo, Network, NetworkActivity, LineItem

                                            //// pk - ProjectID
                                            //if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            //// pk - WBSNo
                                            //if (string.IsNullOrWhiteSpace(row["WBS element"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            //// pk - Network
                                            //if (string.IsNullOrWhiteSpace(row["Network"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            //// pk - NetworkActivity
                                            //if (string.IsNullOrWhiteSpace(row["Activity"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            //// pk - LineItem
                                            //if (string.IsNullOrWhiteSpace(row["BOM item"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // non pk
                                            //if (string.IsNullOrWhiteSpace(row["Material"].ToString()))
                                            //{
                                            //    row["Material"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            if (string.IsNullOrWhiteSpace(row["Material text"].ToString()))
                                            {
                                                if (string.IsNullOrWhiteSpace(row["Item text line 1"].ToString()))
                                                {
                                                    col4.DefaultValue = DBNull.Value;
                                                    table.AcceptChanges();
                                                }
                                                else
                                                {
                                                    col4.DefaultValue = row["Item text line 1"].ToString();
                                                    table.AcceptChanges();
                                                }
                                            }
                                            else {
                                                col4.DefaultValue = row["Material text"].ToString();
                                                table.AcceptChanges();
                                            }

                                            int ORDERQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Requirement quantity"].ToString()))
                                            {
                                                row["Requirement quantity"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!int.TryParse(row["Requirement quantity"].ToString().Trim(), out ORDERQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            int WIDTHDRAWNQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Quantity withdrawn"].ToString()))
                                            {
                                                row["Quantity withdrawn"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!int.TryParse(row["Quantity withdrawn"].ToString().Trim(), out WIDTHDRAWNQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            int RECEIVEDQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Quantity received"].ToString()))
                                            {
                                                row["Quantity received"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!int.TryParse(row["Quantity received"].ToString().Trim(), out RECEIVEDQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Base unit of measure"].ToString()))
                                            //{
                                            //    row["Base unit of measure"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            DateTime PLANONSITE;
                                            if (string.IsNullOrWhiteSpace(row["Requirements date"].ToString()))
                                            {
                                                row["Requirements date"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Requirements date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out PLANONSITE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Goods recipient"].ToString()))
                                            //{
                                            //    row["Goods recipient"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            //if (string.IsNullOrWhiteSpace(row["Reservation"].ToString()))
                                            //{
                                            //    row["Reservation"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            int RESERVATIONITEMNO;
                                            if (string.IsNullOrWhiteSpace(row["Item"].ToString()))
                                            {
                                                row["Item"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!int.TryParse(row["Item"].ToString().Trim(), out RESERVATIONITEMNO))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Purchase requisition"].ToString()))
                                            //{
                                            //    row["Purchase requisition"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            //if (string.IsNullOrWhiteSpace(row["Requisition item"].ToString()))
                                            //{
                                            //    row["Requisition item"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            //if (string.IsNullOrWhiteSpace(row["Purchase Order"].ToString()))
                                            //{
                                            //    row["Purchase Order"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            //if (string.IsNullOrWhiteSpace(row["Purchase Order Item"].ToString()))
                                            //{
                                            //    row["Purchase Order Item"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRows.Count; j++)
                                        {
                                            table.Rows[idxDelRows[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.MATERIALCOMPONENTTEMP";
                                            //bulkCopy.BulkCopyTimeout = 0;
                                            //bulkCopy.BatchSize = table.Rows.Count;
                                            //bulkCopy.BatchSize = 50;

                                            // Write from the source to the destination.
                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                            bulkCopy.ColumnMappings.Add("WBS element", "WBSNO");
                                            bulkCopy.ColumnMappings.Add("Network", "NETWORK");
                                            bulkCopy.ColumnMappings.Add("Activity", "NETWORKACTIVITY");
                                            bulkCopy.ColumnMappings.Add("BOM item", "LINEITEM");
                                            bulkCopy.ColumnMappings.Add("Material", "MATERIAL");
                                            bulkCopy.ColumnMappings.Add("MATERIALDESC", "MATERIALDESC");
                                            bulkCopy.ColumnMappings.Add("Requirement quantity", "ORDERQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Quantity withdrawn", "WITHDRAWNQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Quantity received", "RECEIVEDQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Base unit of measure", "UOM");
                                            bulkCopy.ColumnMappings.Add("Requirements date", "PLANONSITE");
                                            bulkCopy.ColumnMappings.Add("Goods recipient", "ENGINEER");
                                            bulkCopy.ColumnMappings.Add("Reservation", "RESERVATION");
                                            bulkCopy.ColumnMappings.Add("Item", "RESERVATIONITEMNO");
                                            bulkCopy.ColumnMappings.Add("Purchase requisition", "PRNO");
                                            bulkCopy.ColumnMappings.Add("Requisition item", "PRITEMNO");
                                            bulkCopy.ColumnMappings.Add("Purchase Order", "PONO");
                                            bulkCopy.ColumnMappings.Add("Purchase Order Item", "POITEMNO");

                                            //bulkCopy.WriteToServer(table);
                                            //conn.Close();
                                            //conn.Dispose();
                                            //bulkCopy.Close();

                                            //if (bulkCopy != null)
                                            //{
                                            //    ((IDisposable)bulkCopy).Dispose();
                                            //}
                                            //table.Dispose();
                                            //table = null;
                                        }                                        
                                    }                                    
                                }
                                fIntLoader.LOADERBY = ntUser;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                db.Entry(fIntLoader).State = EntityState.Modified;
                                db.SaveChanges();
                                result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                //db.MOVEMATERIALCOMPONENT(fIntLoader.OID);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally {
                                //db.MOVEMATERIALCOMPONENT(fIntLoader.OID);
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PR")
                        {
                            try
                            {
                                int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                StringBuilder errorRows = new StringBuilder();
                                List<int> idxDelRows = new List<int>();

                                using (OleDbConnection conn = new OleDbConnection(connStr))
                                {
                                    if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Purchase Requisition], [Item of Requisition], [Requisition Date], [Purchase Order Date], [Req# Tracking Number] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            //5. Primary key table MaterialPurchase: PRNo, PRItemNo

                                            //// pk PRNo
                                            //if (string.IsNullOrWhiteSpace(row["Purchase Requisition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            //// pk PRItemNo
                                            //if (string.IsNullOrWhiteSpace(row["Item of Requisition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            //non pk
                                            DateTime PRISSUEDDATE;
                                            if (string.IsNullOrWhiteSpace(row["Requisition Date"].ToString()))
                                            {
                                                row["Requirements date"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Requisition Date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out PRISSUEDDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            DateTime PORAISEDDATE;
                                            if (string.IsNullOrWhiteSpace(row["Purchase Order Date"].ToString()))
                                            {
                                                row["Purchase Order Date"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Purchase Order Date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out PORAISEDDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Req# Tracking Number"].ToString()))
                                            //{
                                            //    row["Req# Tracking Number"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRows.Count; j++)
                                        {
                                            table.Rows[idxDelRows[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.MATERIALPURCHASETEMP";
                                            //bulkCopy.BulkCopyTimeout = 0;
                                            //bulkCopy.BatchSize = table.Rows.Count;
                                            //bulkCopy.BatchSize = 50;

                                            // Write from the source to the destination.
                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Purchase Requisition", "PRNO");
                                            bulkCopy.ColumnMappings.Add("Item of Requisition", "PRITEMNO");
                                            bulkCopy.ColumnMappings.Add("Requisition Date", "PRISSUEDDATE");
                                            bulkCopy.ColumnMappings.Add("Purchase Order Date", "PORAISEDDATE");
                                            bulkCopy.ColumnMappings.Add("Req# Tracking Number", "TRACKINGNO");
                                            
                                            bulkCopy.WriteToServer(table);
                                            //conn.Close();
                                            //conn.Dispose();
                                            //bulkCopy.Close();

                                            //if (bulkCopy != null)
                                            //{
                                            //    ((IDisposable)bulkCopy).Dispose();
                                            //}
                                            //table.Dispose();
                                            //table = null;                                            
                                        }                                        
                                    }                                    
                                }
                                fIntLoader.LOADERBY = ntUser;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                db.Entry(fIntLoader).State = EntityState.Modified;
                                db.SaveChanges();

                                result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                //db.MOVEMATERIALPURCHASETEMP(fIntLoader.OID);
                            }
                            catch (Exception ex) {
                                throw ex;
                            }
                            finally {
                                //db.MOVEMATERIALPURCHASETEMP(fIntLoader.OID);
                            }
                            
                        }
                        
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PO")
                        {
                            try
                            {
                                int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                                StringBuilder errorRows = new StringBuilder();
                                List<int> idxDelRows = new List<int>();

                                using (OleDbConnection conn = new OleDbConnection(connStr))
                                {
                                    if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                    {
                                        conn.Open();
                                    }
                                    OleDbCommand com = new OleDbCommand(@"Select [Purchase Requisition], [Item of Requisition], [Delivery Date], [Order Quantity], [Currency], [Net price],
                                                        [Order Price Unit], [Net Order Value], [Qty Delivered], [Quantity Received], [Purchasing Group], [Vendor/supplying plant] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            //6.Primary key table MaterialSchedule: PRNo, PRItemNo

                                            // pk PRNo
                                            //if (string.IsNullOrWhiteSpace(row["Purchase Requisition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // pk PRItemNo
                                            //if (string.IsNullOrWhiteSpace(row["Item of Requisition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // non pk

                                            DateTime DELIVERYDATE;
                                            if (string.IsNullOrWhiteSpace(row["Delivery Date"].ToString()))
                                            {
                                                row["Delivery Date"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Delivery Date"].ToString(), "dd/MM/yyyy HH.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DELIVERYDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            float ORDEREDQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Order Quantity"].ToString()))
                                            {
                                                row["Order Quantity"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!float.TryParse(row["Order Quantity"].ToString().Trim(), out ORDEREDQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Currency"].ToString()))
                                            //{
                                            //    row["Currency"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            float UNITPRICE;
                                            if (string.IsNullOrWhiteSpace(row["Net price"].ToString()))
                                            {
                                                row["Net price"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!float.TryParse(row["Net price"].ToString().Trim(), out UNITPRICE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Order Price Unit"].ToString()))
                                            //{
                                            //    row["Order Price Unit"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            float TOTALPRICE;
                                            if (string.IsNullOrWhiteSpace(row["Net Order Value"].ToString()))
                                            {
                                                row["Net Order Value"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!float.TryParse(row["Net Order Value"].ToString().Trim(), out TOTALPRICE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            float DELIVEREDQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Qty Delivered"].ToString()))
                                            {
                                                row["Qty Delivered"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!float.TryParse(row["Qty Delivered"].ToString().Trim(), out DELIVEREDQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            float RECEIVEDQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Quantity Received"].ToString()))
                                            {
                                                row["Quantity Received"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!float.TryParse(row["Quantity Received"].ToString().Trim(), out RECEIVEDQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Purchasing Group"].ToString()))
                                            //{
                                            //    row["Purchasing Group"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            

                                            //if (string.IsNullOrWhiteSpace(row["Vendor/supplying plant"].ToString()))
                                            //{
                                            //    row["Vendor/supplying plant"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRows.Count; j++)
                                        {
                                            table.Rows[idxDelRows[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();
                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.MATERIALSCHEDULETEMP";
                                            //bulkCopy.BulkCopyTimeout = 0;
                                            //bulkCopy.BatchSize = table.Rows.Count;
                                            //bulkCopy.BatchSize = 50;

                                            // Write from the source to the destination.
                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Purchase Requisition", "PRNO");
                                            bulkCopy.ColumnMappings.Add("Item of Requisition", "PRITEMNO");
                                            bulkCopy.ColumnMappings.Add("Delivery Date", "DELIVERYDATE");
                                            bulkCopy.ColumnMappings.Add("Order Quantity", "ORDEREDQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Currency", "CURRENCY");
                                            bulkCopy.ColumnMappings.Add("Net price", "UNITPRICE");
                                            bulkCopy.ColumnMappings.Add("Order Price Unit", "UNITQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Net Order Value", "TOTALPRICE");
                                            bulkCopy.ColumnMappings.Add("Qty Delivered", "DELIVEREDQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Quantity Received", "RECEIVEDQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Purchasing Group", "BUYER");
                                            bulkCopy.ColumnMappings.Add("Vendor/supplying plant", "VENDOR");

                                            bulkCopy.WriteToServer(table);
                                            //conn.Close();
                                            //conn.Dispose();
                                            //bulkCopy.Close();

                                            //if (bulkCopy != null)
                                            //{
                                            //    ((IDisposable)bulkCopy).Dispose();
                                            //}
                                            //table.Dispose();
                                            //table = null;
                                        }                                        
                                    }                                    
                                }
                                fIntLoader.LOADERBY = ntUser;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                db.Entry(fIntLoader).State = EntityState.Modified;
                                db.SaveChanges();

                                result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                //db.MOVEMATERIALSCHEDULETEMP(fIntLoader.OID);
                            }
                            catch (Exception ex) {
                                throw ex;
                            }
                            finally {
                                //db.MOVEMATERIALSCHEDULETEMP(fIntLoader.OID);
                            }
                            
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "DELIVERY")
                        {
                            try
                            {
                                int succeedCount = 0, failedCount = 0;
                                StringBuilder errorRows = new StringBuilder();
                                List<int> idxDelRows = new List<int>();

                                using (OleDbConnection conn = new OleDbConnection(connStr))
                                {
                                    if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                    {
                                        conn.Open();
                                    }

                                    OleDbCommand com = new OleDbCommand(@"Select [PR], [Request Item PR], [Reservation], [Request Item Reservation], [Code Lookup],
                                                        [Special Stock], [Reservation Withdrawal Qty], [Incoterms PO (Part 1)], [Incoterms PO (Part 2)], 
                                                        [STO Number], [Delivery No#], [Shipment No#], [Shipment Start], [PO Delivery Date], [Shipment End], 
                                                        [Shipment Route], [Unloading Point], [Latest Goods Receipt Date] from [All_Projects$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();

                                    if (reader.HasRows)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            // 7.	Primary key table MaterialStatus: PRNo, PRItemNo

                                            // pk PRNo
                                            //if (string.IsNullOrWhiteSpace(row["PR"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // pk PRItemNo
                                            //if (string.IsNullOrWhiteSpace(row["Request Item PR"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // non pk
                                            //if (string.IsNullOrWhiteSpace(row["Reservation"].ToString()))
                                            //{
                                            //    row["Reservation"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            int RESERVATIONITEMNO;
                                            if (string.IsNullOrWhiteSpace(row["Request Item Reservation"].ToString()))
                                            {
                                                row["Request Item Reservation"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!int.TryParse(row["Request Item Reservation"].ToString().Trim(), out RESERVATIONITEMNO))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Code Lookup"].ToString()))
                                            //{
                                            //    row["Code Lookup"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            //if (string.IsNullOrWhiteSpace(row["Special Stock"].ToString()))
                                            //{
                                            //    row["Special Stock"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            int RESERVATIONWITHDRAWALQUANTITY;
                                            if (string.IsNullOrWhiteSpace(row["Reservation Withdrawal Qty"].ToString()))
                                            {
                                                row["Reservation Withdrawal Qty"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!int.TryParse(row["Reservation Withdrawal Qty"].ToString().Trim(), out RESERVATIONWITHDRAWALQUANTITY))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Incoterms PO (Part 1)"].ToString()))
                                            //{
                                            //    row["Incoterms PO (Part 1)"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            //if (string.IsNullOrWhiteSpace(row["Incoterms PO (Part 2)"].ToString()))
                                            //{
                                            //    row["Incoterms PO (Part 2)"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            //if (string.IsNullOrWhiteSpace(row["STO Number"].ToString()))
                                            //{
                                            //    row["STO Number"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            //if (string.IsNullOrWhiteSpace(row["Delivery No#"].ToString()))
                                            //{
                                            //    row["Delivery No#"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            //if (string.IsNullOrWhiteSpace(row["Shipment No#"].ToString()))
                                            //{
                                            //    row["Shipment No#"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            DateTime SHIPMENTSTARTDATE;
                                            if (string.IsNullOrWhiteSpace(row["Shipment Start"].ToString()))
                                            {
                                                row["Shipment Start"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Shipment Start"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SHIPMENTSTARTDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                                else {
                                                    row["Shipment Start"] = row["Shipment Start"].ToString().Replace(".", "/");
                                                    table.AcceptChanges();
                                                }
                                            }
                                            DateTime ETASITEDATE;
                                            if (string.IsNullOrWhiteSpace(row["PO Delivery Date"].ToString()))
                                            {
                                                row["PO Delivery Date"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["PO Delivery Date"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ETASITEDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                                else {
                                                    row["PO Delivery Date"] = row["PO Delivery Date"].ToString().Replace(".", "/");
                                                    table.AcceptChanges();
                                                }
                                            }
                                            DateTime SHIPMENTENDDATE;
                                            if (string.IsNullOrWhiteSpace(row["Shipment End"].ToString()))
                                            {
                                                row["Shipment End"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Shipment End"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out SHIPMENTENDDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                                else {
                                                    row["Shipment End"] = row["Shipment End"].ToString().Replace(".", "/");
                                                    table.AcceptChanges();
                                                }
                                            }

                                            //if (string.IsNullOrWhiteSpace(row["Shipment Route"].ToString()))
                                            //{
                                            //    row["Shipment Route"] = DBNull.Value; table.AcceptChanges();
                                            //}
                                            //if (string.IsNullOrWhiteSpace(row["Unloading Point"].ToString()))
                                            //{
                                            //    row["Unloading Point"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            DateTime ACTUALDELIVERYDATE;
                                            if (string.IsNullOrWhiteSpace(row["Latest Goods Receipt Date"].ToString()))
                                            {
                                                row["Latest Goods Receipt Date"] = DBNull.Value; table.AcceptChanges();
                                            }
                                            else
                                            {
                                                if (!DateTime.TryParseExact(row["Latest Goods Receipt Date"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ACTUALDELIVERYDATE))
                                                {
                                                    ++failedCount;
                                                    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                                    idxDelRows.Add(i);
                                                    continue;
                                                }
                                                else {
                                                    row["Latest Goods Receipt Date"] = row["Latest Goods Receipt Date"].ToString().Replace(".", "/");
                                                    table.AcceptChanges();
                                                }
                                            }
                                            ++succeedCount;
                                        }

                                        for (int j = 0; j < idxDelRows.Count; j++)
                                        {
                                            table.Rows[idxDelRows[j] - 1].Delete();
                                        }
                                        table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {                                            
                                            bulkCopy.DestinationTableName = "dbo.MATERIALSTATUSTEMP";

                                            //bulkCopy.BulkCopyTimeout = 0;
                                            //bulkCopy.BatchSize = table.Rows.Count;
                                            //bulkCopy.BatchSize = 50;

                                            // Write from the source to the destination
                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("PR", "PRNO");
                                            bulkCopy.ColumnMappings.Add("Request Item PR", "PRITEMNO");
                                            bulkCopy.ColumnMappings.Add("Reservation", "RESERVATION");
                                            bulkCopy.ColumnMappings.Add("Request Item Reservation", "RESERVATIONITEMNO");
                                            bulkCopy.ColumnMappings.Add("Code Lookup", "CODELOOKUP");
                                            bulkCopy.ColumnMappings.Add("Special Stock", "SPECIALSTOCK");
                                            bulkCopy.ColumnMappings.Add("Reservation Withdrawal Qty", "RESERVATIONWITHDRAWALQUANTITY");
                                            bulkCopy.ColumnMappings.Add("Incoterms PO (Part 1)", "INCOTERMSPO");
                                            bulkCopy.ColumnMappings.Add("Incoterms PO (Part 2)", "TRAFFICMETHOD");
                                            bulkCopy.ColumnMappings.Add("STO Number", "STONO");
                                            bulkCopy.ColumnMappings.Add("Delivery No#", "DELIVERYNO");
                                            bulkCopy.ColumnMappings.Add("Shipment No#", "SHIPMENTNO");
                                            bulkCopy.ColumnMappings.Add("Shipment Start", "SHIPMENTSTARTDATE");
                                            bulkCopy.ColumnMappings.Add("PO Delivery Date", "ETASITEDATE");
                                            bulkCopy.ColumnMappings.Add("Shipment End", "SHIPMENTENDDATE");
                                            bulkCopy.ColumnMappings.Add("Shipment Route", "SHIPMENTROUTE");
                                            bulkCopy.ColumnMappings.Add("Unloading Point", "LOCATION");
                                            bulkCopy.ColumnMappings.Add("Latest Goods Receipt Date", "ACTUALDELIVERYDATE");

                                            bulkCopy.WriteToServer(table);
                                            //conn.Close();
                                            //conn.Dispose();
                                            //bulkCopy.Close();

                                            //if (bulkCopy != null)
                                            //{
                                            //    ((IDisposable)bulkCopy).Dispose();
                                            //}
                                            //table.Dispose();
                                            //table = null;

                                        }                                        
                                    }                                   
                                }
                                fIntLoader.LOADERBY = ntUser;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                db.Entry(fIntLoader).State = EntityState.Modified;
                                db.SaveChanges();
                                result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                //db.MOVEMATERIALSTATUSTEMP(fIntLoader.OID);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally {
                                //db.MOVEMATERIALSTATUSTEMP(fIntLoader.OID);
                            }
                            
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PRGROUP")
                        {
                            try
                            {
                                int succeedCount = 0, failedCount = 0;
                                StringBuilder errorRows = new StringBuilder();
                                List<int> idxDelRows = new List<int>();

                                using (OleDbConnection conn = new OleDbConnection(connStr))
                                {
                                    

                                    if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                                    {
                                        conn.Open();
                                    }

                                    //OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                                    OleDbCommand com = new OleDbCommand(@"Select [Purchase Requisition], [Item of Requisition], [Purchasing Group], [Purchasing group Description] from [Sheet1$]", conn);
                                    OleDbDataReader reader = com.ExecuteReader();

                                    if (reader.HasRows)
                                    {
                                        DataTable table = new DataTable();
                                        var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                        col1.DefaultValue = fIntLoader.OID;
                                        var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                        col2.DefaultValue = DateTime.Now;
                                        var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                        col3.DefaultValue = ntUser;
                                        table.AcceptChanges();
                                        table.Load(reader);

                                        int i = 0;
                                        foreach (DataRow row in table.Rows)
                                        {
                                            i++;

                                            // 7.	Primary key table MaterialStatus: PRNo, PRItemNo

                                            // pk PRNo
                                            //if (string.IsNullOrWhiteSpace(row["Purchase Requisition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // pk PRItemNo
                                            //if (string.IsNullOrWhiteSpace(row["Item of Requisition"].ToString()))
                                            //{
                                            //    ++failedCount;
                                            //    errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            //    idxDelRows.Add(i);
                                            //    continue;
                                            //}

                                            // non pk
                                            //if (string.IsNullOrWhiteSpace(row["Purchasing Group"].ToString()))
                                            //{
                                            //    row["Purchasing Group"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            //if (string.IsNullOrWhiteSpace(row["Purchasing group Description"].ToString()))
                                            //{
                                            //    row["Purchasing group Description"] = DBNull.Value; table.AcceptChanges();
                                            //}

                                            ++succeedCount;
                                        }

                                        //for (int j = 0; j < idxDelRows.Count; j++)
                                        //{
                                        //    table.Rows[idxDelRows[j] - 1].Delete();
                                        //}
                                        //table.AcceptChanges();

                                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                        {
                                            bulkCopy.DestinationTableName = "dbo.MATERIALPURCHASEGROUPTEMP";

                                            //bulkCopy.BulkCopyTimeout = 0;
                                            //bulkCopy.BatchSize = table.Rows.Count;
                                            //bulkCopy.BatchSize = 50;

                                            // Write from the source to the destination
                                            bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                            bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                            bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                            bulkCopy.ColumnMappings.Add("Purchase Requisition", "PRNO");
                                            bulkCopy.ColumnMappings.Add("Item of Requisition", "PRITEMNO");
                                            bulkCopy.ColumnMappings.Add("Purchasing Group", "PRGROUPCODE");
                                            bulkCopy.ColumnMappings.Add("Purchasing group Description", "PRGROUPDESC");

                                            bulkCopy.WriteToServer(table);
                                            //conn.Close();
                                            //conn.Dispose();
                                            //bulkCopy.Close();

                                            //if (bulkCopy != null)
                                            //{
                                            //    ((IDisposable)bulkCopy).Dispose();
                                            //}
                                            //table.Dispose();
                                            //table = null;
                                        }                                        
                                    }                                    
                                }
                                fIntLoader.LOADERBY = ntUser;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                db.Entry(fIntLoader).State = EntityState.Modified;
                                db.SaveChanges();

                                result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                //db.MOVEMATERIALPURCHASEGROUPTEMP(fIntLoader.OID);
                            }
                            catch (Exception ex) {
                                throw ex;
                            }
                            finally {
                                //db.MOVEMATERIALPURCHASEGROUPTEMP(fIntLoader.OID);
                            }                            
                        }
                        else continue;
                    }
                }
            }
            return result;
        }


        [HttpGet]
        [ActionName("ProcessLoaderCs")]
        public object ProcessLoaderCs(string param)
        {
            var ntUser = GetCurrentNTUserId();
            List<object> result = new List<object>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "CS").ToList();
            string fileName = "";
            string purposeFile = "";

            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                var shareFolder = fileInt.SHAREFOLDER;
                fileName = shareFolder + fileInt.FILENAME;
                purposeFile = fileInt.PURPOSEFILE;
                List<string> failedDetail = new List<string>();
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
            }

            return result;
        }


    }
}