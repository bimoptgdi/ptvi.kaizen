﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Data.SqlTypes;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class taskEmail: emailEngineHeaderObject
    {
        public new List<taskToEmailList> FIELDLIST { get; set; }

    }
    public class taskToEmailList
    {
        public string KEY { get; set; }
        public string BADGENO { get; set; }
        public string BADGENOTO { get; set; }
        public string FULLNAME { get; set; }
        public string RECEIVER { get; set; }
        public string EMAIL { get; set; }
        public string EMAILCC { get; set; }
        public string DUEDATE { get; set; }
        public double DAYSTODUE { get; set; }
        public string TASK { get; set; }
        public string STATUS { get; set; }
        public string TIPE { get; set; }
        public string PIC { get; set; }
        public string PROJECTNO { get; set; }
        public string PROJECTDESC { get; set; }
        public List<string> FIELDLIST
        {
            get
            {
                return new List<string>
                {
                    "PROJECTNO",
                    "PROJECTDESC",
                    "TASK",
                    "TIPE",
                    "DUEDATE",
                    "DAYSTODUE",
                    "STATUS",
                };
            }
        }

    }
    public class myTaskData
    {
        public string taskType { get; set; }
        public DateTime actualStartDate { get; set; }
        public DateTime actualFinishDate { get; set; }
        public string status { get; set; }
        public string remark { get; set; }
    }

    public class MyTaskController : KAIZENController
    {
        //private iPROMEntities db = new iPROMEntities();

        private object putMyTaskKD(int id, myTaskData taskData)
        {
            var kd = db.KEYDELIVERABLES.Find(id);
            if (taskData.actualStartDate != DateTime.MinValue) kd.ACTUALSTARTDATE = taskData.actualStartDate;
            if (taskData.actualFinishDate != DateTime.MinValue) kd.ACTUALFINISHDATE = taskData.actualFinishDate;
            kd.UPDATEDBY = GetCurrentNTUserId();
            kd.UPDATEDDATE = DateTime.Now;
            kd.REMARK = taskData.remark;
            kd.STATUS = taskData.status;
            if (taskData.status != null) kd.STATUS = taskData.status;
            db.Entry(kd).State = EntityState.Modified;
            db.SaveChanges();
            var obj = getMyTaskKD(id);
            return obj;
        }
        private object putMyTaskPD(int id, myTaskData taskData)
        {
            var kd = db.PROJECTDOCUMENTs.Find(id);
            if (taskData.actualStartDate != DateTime.MinValue) kd.ACTUALSTARTDATE = taskData.actualStartDate;
            if (taskData.actualFinishDate != DateTime.MinValue) kd.ACTUALFINISHDATE = taskData.actualFinishDate;
            kd.UPDATEDBY = GetCurrentNTUserId();
            kd.UPDATEDDATE = DateTime.Now;
            kd.REMARK = taskData.remark;
            kd.STATUS = taskData.status;
            db.Entry(kd).State = EntityState.Modified;
            db.SaveChanges();
            var obj = getMyTaskPD(id);
            return obj;
        }
        private object putMyTaskCO(int id, myTaskData taskData)
        {
            var kd = db.CLOSEOUTPROGRESSes.Find(id);
            kd.SUBMITEDDATE = taskData.actualFinishDate;
            kd.SUBMITEDBY = GetCurrentNTUserId();
            db.Entry(kd).State = EntityState.Modified;
            db.SaveChanges();
            var obj = getMyTaskCO(id);
            return obj;
        }
        private object getMyTaskKD(int id)
        {
            Object obj = db.KEYDELIVERABLES.Where(w => w.OID == id).
                Select(s => new 
                {
                    id = s.OID,
                    projectName = s.PROJECTMANAGEMENT.PROJECT.PROJECTDESCRIPTION,
                    projectManagementId = s.PROJECTMANAGEMENTID,
                    projectId = s.PROJECTMANAGEMENT.PROJECTID,
                    name = s.NAME,
                    weightPercentage = s.WEIGHTPERCENTAGE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    status = s.STATUS,
                    statusName = s.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(ds => ds.TYPE == constants.statusIprom &&
                     ds.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (s.ACTUALSTARTDATE != null) ?
                            db.LOOKUPs.Where(ds => ds.TYPE == constants.statusIprom && ds.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                    remark = s.REMARK,
                    revision = s.REVISION
                }).FirstOrDefault();

            return obj;
        }

        private object getMyTaskPD(int id)
        {
            Object obj = db.PROJECTDOCUMENTs.Where(w => w.OID == id).
                Select(s => new
                {
                    id = s.OID,
                    projectName = s.PROJECT.PROJECTDESCRIPTION,
                    docType = s.DOCTYPE,
                    docNo = s.DOCNO,
                    title = s.DOCTITLE,
                    docByName = s.DOCUMENTBYNAME,
                    docById = s.DOCUMENTBYID,
                    docByEmail = s.DOCUMENTBYEMAIL,
                    drawingCount = s.DRAWINGCOUNT,
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    discipline = s.DISCIPLINE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    planStartDate = s.PLANSTARTDATE,
                    cutOfDate = s.CUTOFDATE,
                    progress = s.PROGRESS,
                    remark = s.REMARK,
                    revision = s.REVISION,
                    sharedFolder = s.SHAREDFOLDER,
                    status = s.STATUS,
                    statusName = s.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(ds => ds.TYPE == constants.statusIprom &&
                     ds.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (s.PLANSTARTDATE != null && s.ACTUALSTARTDATE != null) ?
                     db.LOOKUPs.Where(ds => ds.TYPE == constants.statusIprom && ds.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                }).FirstOrDefault();

            return obj;
        }

        private object getMyTaskCO(int id)
        {
            Object obj = db.CLOSEOUTPROGRESSes.Where(w => w.OID == id).
                Select(s => new
                {
                    id = s.OID,
                    projectName = s.PROJECTCLOSEOUT.PROJECT.PROJECTDESCRIPTION,
                    roleName = s.SETUPCLOSEOUTROLE.DESCRIPTION,
                    planFinishDate = s.PLANDATE,
                    actualFinishDate = s.SUBMITEDDATE,
                    status = s.PROJECTCLOSEOUT.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == s.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                            constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS,
                    statusName = db.LOOKUPs.Where(ds => ds.TYPE == constants.statusIprom && ds.VALUE ==
                        (s.PROJECTCLOSEOUT.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == s.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                        constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS)).FirstOrDefault().TEXT,
                }).FirstOrDefault();

            return obj;
        }

        [HttpPut]
        [ActionName("putTaskData")]
        public object putTaskData(int param, myTaskData taskData)
        {
            var kd = new KeyDeliverablesController();
            var obj = new object();
            string taskType = taskData.taskType;
            switch (taskType)
            {
                case "KeyDeliverable":
                    obj = putMyTaskKD(param, taskData);
                    break;
                case "EWP":
                    obj = putMyTaskPD(param, taskData);
                    break;
                case "DWG":
                    obj = putMyTaskPD(param, taskData);
                    break;
                case "DC":
                    obj = putMyTaskPD(param, taskData);
                    break;
                case "CloseOutTask":
                    obj = putMyTaskCO(param, taskData);
                    break;
            }

            return obj;
        }


        [HttpGet]
        [ActionName("getTaskData")]
        public object getTaskData(int param, string taskType)
        {
            var kd = new KeyDeliverablesController();
            var obj = new object();
            switch (taskType)
            {   
                case "KeyDeliverable":
                    obj = getMyTaskKD(param);
                    break;
                case "EWP":
                    obj = getMyTaskPD(param);
                    break;
                case "DWG":
                    obj = getMyTaskPD(param);
                    break;
                case "DC":
                    obj = getMyTaskPD(param);
                    break;
                case "CloseOutTask":
                    obj = getMyTaskCO(param);
                    break;
            }

            return obj;
        }

        public List<object> IsReadonlyProjectToOnList(int param, string badgeNo, int accessrights)
        {
            var usc = new ProjectController();
            var isReadOnly = usc.IsReadonlyProjectTo(param, badgeNo, accessrights);
            List<object> result = new List<object>();


            result.Add(new { result = isReadOnly });

            return result;
        }

        [HttpGet]
        [ActionName("getMyTaskToEmail")]
        public IEnumerable<taskToEmailList> getMyTaskToEmail(string param)
        {
            var usc = new ProjectController();

            var pj = db.PROJECTs;

            var pp = pj.Select(p => new {
                id = p.OID,
                projectNo = p.PROJECTNO,
                ownerId = p.OWNERID,
                sponsorId = p.SPONSORID,
                projectManagerId = p.PROJECTMANAGERID,
                projectEngineerId = p.PROJECTENGINEERID,
                maintenanceRepId = p.MAINTENANCEREPID,
                operationRepId = p.OPERATIONREPID,
                userAssignment = p.USERASSIGNMENTs.Select(ua => new {
                    employeeId = ua.EMPLOYEEID,
                    employeeName = ua.EMPLOYEENAME,
                }),
                projectDocument = p.PROJECTDOCUMENTs.Where(pdA =>
                pdA.STATUS != constants.STATUSIPROM_HOLD && pdA.STATUS != constants.STATUSIPROM_CANCELLED
                && pdA.ACTUALFINISHDATE == null
                ).Select(pd => new {
                    id = pd.OID,
                    pId = p.OID,
                    pNo = p.PROJECTNO,
                    tipe = pd.DOCTYPE,
                    tipeName = pd.DOCTYPE == "EWP" ? "EWP Compliance" : pd.DOCTYPE == "DWG" ? "Drawing" : "Design Conformity",
                    desc = pd.DOCNO,
                    name = pd.DOCTITLE,
                    pfd = pd.PLANFINISHDATE,
                    psd = pd.PLANSTARTDATE,
                    afd = pd.ACTUALFINISHDATE,
                    asd = pd.ACTUALSTARTDATE,
                    pic = pd.DOCUMENTBYNAME,
                    picId = pd.DOCUMENTBYID,
                    picEmail = pd.DOCUMENTBYEMAIL,
                    picEmailCC = p.PROJECTMANAGEREMAIL,
                    pmName = p.PROJECTMANAGERNAME,
                    pmEmail = p.PROJECTMANAGEREMAIL,
                    pmId = p.PROJECTMANAGERID,
                    status = pd.STATUS,
                    statusName = pd.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom &&
                     s.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (pd.PLANSTARTDATE != null && pd.ACTUALSTARTDATE != null) ?
                     db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                    updatedBy = pd.UPDATEDBY,
                    updatedByName = pd.UPDATEDBY != null ? db.USERs.Where(u => u.NTUSERID == pd.UPDATEDBY).FirstOrDefault().USERNAME : "",
                    updatedDate = pd.UPDATEDDATE,
                }).OrderBy(o => o.psd).ThenBy(o => o.pfd),
                pM = p.PROJECTMANAGEMENTs.Where(k => k.KEYDELIVERABLES.Count() > 0).Select(pm => new {
                    pId = pm.PROJECTID,
                    cb = pm.CREATEDBY,
                    ub = pm.UPDATEDBY,
                    kd = pm.KEYDELIVERABLES.Where(pmId =>
                        pmId.STATUS != constants.STATUSIPROM_HOLD && pmId.STATUS != constants.STATUSIPROM_CANCELLED
                        && pmId.ACTUALFINISHDATE == null
                      ).Select(kd => new {
                          id = kd.OID,
                          pId = p.OID,
                          pNo = p.PROJECTNO,
                          tipe = "Key Deliverable",
                          tipeName = "Key Deliverable",
                          desc = kd.NAME,
                          name = kd.NAME,
                          pfd = kd.PLANFINISHDATE,
                          psd = kd.PLANSTARTDATE,
                          afd = kd.ACTUALFINISHDATE,
                          asd = kd.ACTUALSTARTDATE,
                          pic = p.PROJECTMANAGERNAME,
                          picId = p.PROJECTMANAGERID,
                          picEmail = p.PROJECTMANAGEREMAIL,
                          picEmailCC = p.PROJECTMANAGEREMAIL,
                          pmName = p.PROJECTMANAGERNAME,
                          pmEmail = p.PROJECTMANAGEREMAIL,
                          pmId = p.PROJECTMANAGERID,
                          status = kd.STATUS,
                          statusName = kd.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom &&
                           s.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (kd.ACTUALSTARTDATE != null) ?
                            db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                          updatedBy = kd.UPDATEDBY,
                          updatedByName = kd.UPDATEDBY != null ? db.USERs.Where(u => u.NTUSERID == kd.UPDATEDBY).FirstOrDefault().USERNAME : "",
                          updatedDate = kd.UPDATEDDATE,
                      }).OrderBy(o => o.psd).ThenBy(o => o.pfd)
                }),
                cOut = p.PROJECTCLOSEOUTs.Where(pCO => pCO.CLOSEOUTPROGRESSes.Count > 0).Select(pCO => new {
                    cOPg = pCO.CLOSEOUTPROGRESSes.Where(cOP => cOP.SUBMITEDDATE == null).Select(cOP => new {
                        id = cOP.OID,
                        pId = p.OID,
                        pNo = p.PROJECTNO,
                        tipe = "Close Out Task",
                        tipeName = "Close Out Task",
                        desc = cOP.SETUPCLOSEOUTROLE.DESCRIPTION,
                        name = "",
                        pfd = cOP.PLANDATE,
                        psd = (DateTime?)null,
                        afd = cOP.SUBMITEDDATE,
                        asd = pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.SUBMITEDDATE == null && cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1 && cOP.CLOSEOUTROLEID > 1).FirstOrDefault().SUBMITEDDATE,
                        pic = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                            (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                p.PROJECTMANAGERNAME :
                                db.ROLEs.Where(r => r.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && r.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.USERNAME,
                        picId = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                            (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                p.PROJECTMANAGERID :
                                db.ROLEs.Where(r => r.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && r.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.BADGENO,
                        picEmail = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                            (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                p.PROJECTMANAGEREMAIL :
                                db.ROLEs.Where(r => r.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && r.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.EMAIL,
                        picEmailCC = p.PROJECTMANAGEREMAIL,
                        picUa = p.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                        && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                ).Select(s => s.EMPLOYEENAME).ToList(),
                        picUaId = p.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                        && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                ).Select(s => s.EMPLOYEEID).ToList(),
                        picUaEmail = p.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                        && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                ).Select(s => s.EMPLOYEEEMAIL).ToList(),
                        picUaEmailCC = p.PROJECTMANAGEREMAIL,
                        pmName = p.PROJECTMANAGERNAME,
                        pmEmail = p.PROJECTMANAGEREMAIL,
                        pmId = p.PROJECTMANAGERID,
                        status = pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                            constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS,
                        statusName = db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE ==
                        (pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                            constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS)).FirstOrDefault().TEXT,
                        coRole = cOP.SETUPCLOSEOUTROLE.ISROLE.Value ? cOP.SETUPCLOSEOUTROLE.ROLENAME : cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE,
                        updatedBy = cOP.SUBMITEDBY,
                        updatedByName = cOP.SUBMITEDBY != null ? db.USERs.Where(u => u.NTUSERID == cOP.SUBMITEDBY).FirstOrDefault().USERNAME : "",
                        updatedDate = cOP.SUBMITEDDATE,
                    }).OrderBy(o => o.id)
                })
            });

            var kD = new List<object>();
            var ewp = new List<object>();
            var dwg = new List<object>();
            var dc = new List<object>();
            var coTask = new List<object>();
            foreach (var p in pp)
            {
                foreach (var pm in p.pM)
                {
                    foreach (var k in pm.kd)
                    {
                        bool editable = false;
                        //var edit = IsReadonlyProjectToOnList(p.id, param, constants.PMModul);
                        //if (edit != null)
                        //{
                        //    editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                        //}
                        editable = false;
                        var o = new
                        {
                            afd = k.afd,
                            asd = k.asd,
                            id = k.id,
                            desc = k.desc,
                            pfd = k.pfd,
                            pic = k.pic,
                            picId = k.picId,
                            picEmail = k.picEmail,
                            picEmailCC = k.picEmailCC,
                            pmId = k.pmId,
                            pmName = k.pmName,
                            pmEmail = k.pmEmail,
                            pNo = k.pNo,
                            pnoName = k.pNo + (k.name != "" ? " - " + k.name : ""),
                            psd = k.psd,
                            status = k.status,
                            statusName = k.statusName,
                            tipe = k.tipe,
                            tipeName = k.tipeName,
                            editable = editable,
                            coRole = "",
                            updatedBy = k.updatedBy,
                            updatedDate = k.updatedDate,
                        };
                        kD.Add(o);
                    }
                }

                foreach (var ep in p.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_ENGINEER /*&& (pd.picId == param || p.projectManagerId == param)*/)))
                {
                    bool editable = false;
                    //var edit = IsReadonlyProjectToOnList(p.id, param, constants.ManageDocEWP);
                    //if (edit != null)
                    //{
                    //    editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                    //}
                    editable = false;
                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        picEmail = ep.picEmail,
                        picEmailCC = ep.picEmailCC,
                        pmId = ep.pmId,
                        pmName = ep.pmName,
                        pmEmail = ep.pmEmail,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedDate = ep.updatedDate,
                    };
                    ewp.Add(o);
                }

                foreach (var ep in p.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_DESIGN)))
                {
                    bool editable = false;
                    //var edit = IsReadonlyProjectToOnList(p.id, param, constants.ManageDesignDoc);
                    //if (edit != null)
                    //{
                    //    editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                    //}

                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        picEmail = ep.picEmail,
                        picEmailCC = ep.picEmailCC,
                        pmName = ep.pmName,
                        pmEmail = ep.pmEmail,
                        pmId = ep.pmId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedDate = ep.updatedDate,
                    };
                    dwg.Add(o);
                }

                foreach (var ep in p.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)))
                {
                    bool editable = false;
                    //var edit = IsReadonlyProjectToOnList(p.id, param, constants.ManageDesignDoc);
                    //if (edit != null)
                    //{
                    //    editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                    //}

                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        picEmail = ep.picEmail,
                        picEmailCC = ep.picEmailCC,
                        pmName = ep.pmName,
                        pmEmail = ep.pmEmail,
                        pmId = ep.pmId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedDate = ep.updatedDate,
                    };
                    dc.Add(o);
                }

               // var ec = new EmailController();
//                var paramname = ec.FindNameAndEmailByBadgeNo(param).Split('|')[0];
                foreach (var co in p.cOut)
                {
                    foreach (var copg in co.cOPg.Where(c => c.asd != null))
                    {
                        if (copg.pic == "#ua#")
                        {
                            var pic = String.Join(",", copg.picUa);
                            var picId = String.Join(",", copg.picUaId);
                            var picEmail = String.Join(",", copg.picUaEmail);
                            var picEmailCC = String.Join(",", copg.picUaEmailCC);
                            var o = new
                            {
                                afd = copg.afd,
                                asd = copg.asd,
                                id = copg.id,
                                name = copg.name,
                                desc = copg.desc,
                                pfd = copg.pfd,
                                pic = pic,
                                picId = picId,
                                picEmail = picEmail,
                                picEmailCC = picEmailCC,
                                pmName = copg.pmName,
                                pmEmail = copg.pmEmail,
                                pmId = copg.pmId,
                                pNo = copg.pNo,
                                pnoName = copg.pNo + (copg.name != "" ? " - " + copg.name : ""),
                                psd = copg.psd,
                                status = copg.status,
                                statusName = copg.statusName,
                                tipe = copg.tipe,
                                tipeName = copg.tipeName,
                                coRole = copg.coRole,
                                updatedBy = copg.updatedBy,
                                updatedDate = copg.updatedDate,
                            };
                            if (copg.pic != null)
                                coTask.Add(o);
                        }
                        else
                        {
                            if (copg.pic != null )
                                coTask.Add(copg);
                        }

                    }
                }
            }

            var t = new List<object>();
            t.Add(new { myTask = kD.Union(ewp).Union(dwg).Union(coTask) });
            var m = kD.Union(ewp).Union(dwg).Union(dc).Union(coTask);
            var listField = m.Where(p => ((DateTime?)p.GetType().GetProperty("psd").GetValue(p, null) <= DateTime.Now.AddDays(14))).
                Select(e => new taskToEmailList()
                {
                    KEY = (string)e.GetType().GetProperty("id").GetValue(e, null).ToString(),
                    BADGENO = (string)e.GetType().GetProperty("picId").GetValue(e, null),
                    BADGENOTO = (string)e.GetType().GetProperty("picId").GetValue(e, null) + " | " + (string)e.GetType().GetProperty("pmId").GetValue(e, null),
                    DAYSTODUE = Math.Floor(((DateTime?)e.GetType().GetProperty("psd").GetValue(e, null)).Value.Subtract(DateTime.Now).TotalDays),
                    DUEDATE = ((string)e.GetType().GetProperty("psd").GetValue(e, null)) != null ? ((DateTime?)e.GetType().GetProperty("psd").GetValue(e, null)).Value.Date.ToString("dd-MMM-yyyy") : ((DateTime?)e.GetType().GetProperty("psd").GetValue(e, null)).Value.Date.ToString("dd-MMM-yyyy"),
                    FULLNAME = (string)e.GetType().GetProperty("pic").GetValue(e, null),
                    RECEIVER = (string)e.GetType().GetProperty("pic").GetValue(e, null),
                    EMAIL = (string)e.GetType().GetProperty("picEmail").GetValue(e, null),
                    EMAILCC = (string)e.GetType().GetProperty("picEmailCC").GetValue(e, null),
                    STATUS = (string)e.GetType().GetProperty("statusName").GetValue(e, null),
                    TASK = (string)e.GetType().GetProperty("pnoName").GetValue(e, null),
                    TIPE = (string)e.GetType().GetProperty("tipeName").GetValue(e, null),
                    PROJECTNO = (string)e.GetType().GetProperty("pNo").GetValue(e, null),
                    PROJECTDESC= (string)e.GetType().GetProperty("desc").GetValue(e, null),

                });
            //var m2 = ewp.Union(dwg).Union(dc);
            //var listField2 = m2.Where(p => ((DateTime?)p.GetType().GetProperty("psd").GetValue(p, null) <= DateTime.Now.AddDays(14))).
            //    Select(e => new taskToEmailList()
            //    {
            //        KEY = (string)e.GetType().GetProperty("id").GetValue(e, null).ToString(),
            //        BADGENO = (string)e.GetType().GetProperty("picId").GetValue(e, null),
            //        BADGENOTO = (string)e.GetType().GetProperty("picId").GetValue(e, null) + " | " + (string)e.GetType().GetProperty("pNo").GetValue(e, null),
            //        DAYSTODUE = Math.Floor(((DateTime?)e.GetType().GetProperty("psd").GetValue(e, null)).Value.Subtract(DateTime.Now).TotalDays),
            //        DUEDATE = (DateTime?)e.GetType().GetProperty("psd").GetValue(e, null),
            //        FULLNAME = (string)e.GetType().GetProperty("pic").GetValue(e, null),
            //        RECEIVER = (string)e.GetType().GetProperty("pic").GetValue(e, null),
            //        EMAIL = (string)e.GetType().GetProperty("pmEmail").GetValue(e, null),
            //        EMAILCC = "",
            //        STATUS = (string)e.GetType().GetProperty("statusName").GetValue(e, null),
            //        TASK = (string)e.GetType().GetProperty("pnoName").GetValue(e, null),
            //        TIPE = (string)e.GetType().GetProperty("tipeName").GetValue(e, null),
            //    });
            return listField;
        }

        [HttpGet]
        [ActionName("getMyTask")]
        public IEnumerable<object> getMyTask(string param)
        {
            var usc = new ProjectController();
//            var role = usc.getRoleInProject(param);
            int cekRoleTypeAllProject = db.USERROLEs.Count(w => w.USER.BADGENO == param && w.ROLE.ROLETYPE == "0");
            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);
            var pj = db.PROJECTs.Where(w => w.OWNERID == param || w.SPONSORID == param ||
                  w.PROJECTMANAGERID == param || w.PROJECTENGINEERID == param ||
                  w.MAINTENANCEREPID == param || w.OPERATIONREPID == param ||
                  w.USERASSIGNMENTs.Where(u => u.EMPLOYEEID == param && (u.ISACTIVE == true || otherPos.Select(so => so.VALUE).Contains(u.ASSIGNMENTTYPE))).Count() > 0);

            //get Deliverable
            //var deliverable = db.KEYDELIVERABLES.Where(d=>d.PROJECTMANAGEMENTID == db.PROJECTs.);
            //get EWP

            //get DesConf

            //get Des Drawing

            //get Task Close Out
            var pp = pj.Select(p => new {
                id = p.OID,
                projectNo = p.PROJECTNO,
                ownerId = p.OWNERID,
                sponsorId = p.SPONSORID,
                projectManagerId = p.PROJECTMANAGERID,
                projectEngineerId = p.PROJECTENGINEERID,
                maintenanceRepId = p.MAINTENANCEREPID,
                operationRepId = p.OPERATIONREPID,
                userAssignment = p.USERASSIGNMENTs.Select(ua => new {
                    employeeId = ua.EMPLOYEEID,
                    employeeName = ua.EMPLOYEENAME,
                }),
                projectDocument = p.PROJECTDOCUMENTs.Where(pdA => 
                pdA.STATUS != constants.STATUSIPROM_HOLD && pdA.STATUS != constants.STATUSIPROM_CANCELLED
                && pdA.ACTUALFINISHDATE == null
                ).Select(pd => new {
                    id = pd.OID,
                    pId = p.OID,
                    pNo = p.PROJECTNO,
                    tipe = pd.DOCTYPE,
                    tipeName = pd.DOCTYPE == "EWP" ? "EWP Compliance": pd.DOCTYPE == "DWG" ? "Drawing":"Design Conformity",
                    desc = pd.DOCNO,
                    name = pd.DOCTITLE,
                    pfd = pd.PLANFINISHDATE,
                    psd = pd.PLANSTARTDATE,
                    afd = pd.ACTUALFINISHDATE,
                    asd = pd.ACTUALSTARTDATE,
                    pic = pd.DOCUMENTBYNAME,
                    picId = pd.DOCUMENTBYID,
                    status = pd.STATUS,
                    statusName = pd.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom &&
                     s.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (pd.PLANSTARTDATE != null && pd.ACTUALSTARTDATE != null)?
                     db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                    updatedBy = pd.UPDATEDBY,
                    updatedByName = pd.UPDATEDBY != null ? db.USERs.Where(u => u.NTUSERID == pd.UPDATEDBY).FirstOrDefault().USERNAME : "",
                    updatedDate = pd.UPDATEDDATE,
                }).OrderBy(o => o.psd).ThenBy(o => o.pfd),
                pM = p.PROJECTMANAGEMENTs.Where(k => k.KEYDELIVERABLES.Count() > 0).Select(pm => new {
                    pId = pm.PROJECTID,
                    cb = pm.CREATEDBY,
                    ub = pm.UPDATEDBY,
                    kd = pm.KEYDELIVERABLES.Where(pmId => p.PROJECTMANAGERID == param &&
                        pmId.STATUS != constants.STATUSIPROM_HOLD && pmId.STATUS != constants.STATUSIPROM_CANCELLED
                        && pmId.ACTUALFINISHDATE == null
                      ).Select(kd => new {
                          id = kd.OID,
                          pId = p.OID,
                          pNo = p.PROJECTNO,
                          tipe = "Key Deliverable",
                          tipeName = "Key Deliverable",
                          desc = kd.NAME,
                          name = "",
                          pfd = kd.PLANFINISHDATE,
                          psd = kd.PLANSTARTDATE,
                          afd = kd.ACTUALFINISHDATE,
                          asd = kd.ACTUALSTARTDATE,
                          pic = p.PROJECTMANAGERNAME,
                          picId = p.PROJECTMANAGERID,
                          status = kd.STATUS,
                          statusName = kd.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom &&
                           s.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (kd.ACTUALSTARTDATE != null) ?
                            db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                          updatedBy = kd.UPDATEDBY,
                          updatedByName = kd.UPDATEDBY!=null? db.USERs.Where(u => u.NTUSERID == kd.UPDATEDBY).FirstOrDefault().USERNAME : "",
                          updatedDate = kd.UPDATEDDATE,
                      }).OrderBy(o => o.psd).ThenBy(o => o.pfd)
                }),
                cOut = p.PROJECTCLOSEOUTs.Where(pCO => pCO.CLOSEOUTPROGRESSes.Count > 0).Select(pCO => new {
                    cOPg = pCO.CLOSEOUTPROGRESSes.Where(cOP => cOP.SUBMITEDDATE == null).Select(cOP => new {
                        id = cOP.OID,
                        pId = p.OID,
                        pNo = p.PROJECTNO,
                        tipe = "Close Out Task",
                        tipeName = "Close Out Task",
                        desc = cOP.SETUPCLOSEOUTROLE.DESCRIPTION,
                        name = "",
                        pfd = cOP.PLANDATE,
                        psd = (DateTime?)null,
                        afd = cOP.SUBMITEDDATE,
                        asd = pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.SUBMITEDDATE == null && cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1 && cOP.CLOSEOUTROLEID > 1).FirstOrDefault().SUBMITEDDATE,
                        pic = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                            (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                p.PROJECTMANAGERNAME :
                                db.ROLEs.Where(r => r.ROLENAME==cOP.SETUPCLOSEOUTROLE.ROLENAME && r.USERROLEs.Count>0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.USERNAME,
                        picId = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                            (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                p.PROJECTMANAGERID :
                                db.ROLEs.Where(r => r.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && r.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.BADGENO,
                        picUa = p.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                        && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                ).Select(s => s.EMPLOYEENAME).ToList(),
                        picUaId = p.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                        && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                ).Select(s => s.EMPLOYEEID).ToList(),
                        status = pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null?
                            constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS,
                        statusName = db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == 
                        (pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                            constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS)).FirstOrDefault().TEXT,
                        coRole = cOP.SETUPCLOSEOUTROLE.ISROLE.Value ? cOP.SETUPCLOSEOUTROLE.ROLENAME : cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE,
                        updatedBy = cOP.SUBMITEDBY,
                        updatedByName = cOP.SUBMITEDBY != null ? db.USERs.Where(u => u.NTUSERID == cOP.SUBMITEDBY).FirstOrDefault().USERNAME : "",
                        updatedDate = cOP.SUBMITEDDATE,
                    }).OrderBy(o => o.id)
                })
            });

            var kD = new List<object>();
            var ewp = new List<object>();
            var dwg = new List<object>();
            var coTask = new List<object>();
            foreach (var p in pp)
            {
                foreach (var pm in p.pM)
                {
                    foreach (var k in pm.kd)
                    {
                        bool editable = false;
                        var edit = IsReadonlyProjectToOnList(p.id, param, constants.PMModul);
                        if (edit != null)
                        {
                            editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                        }
                        var o = new
                        {
                            afd = k.afd,
                            asd = k.asd,
                            id = k.id,
                            desc = k.desc,
                            pfd = k.pfd,
                            pic = k.pic,
                            picId = k.picId,
                            pNo = k.pNo,
                            pnoName = k.pNo + (k.name!=""?" - " + k.name:""),
                            psd = k.psd,
                            status = k.status,
                            statusName = k.statusName,
                            tipe = k.tipe,
                            tipeName = k.tipeName,
                            editable = editable,
                            coRole = "",
                            updatedBy = k.updatedBy,
                            updatedByName = k.updatedByName,
                            updatedDate = k.updatedDate,
                        };
                        kD.Add(o);
                    }
                }

                foreach (var ep in p.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_ENGINEER && (pd.picId == param || p.projectManagerId == param))))
                {
                    bool editable = false;
                    var edit = IsReadonlyProjectToOnList(p.id, param, constants.ManageDocEWP);
                    if (edit != null)
                    {
                        editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                    }
                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedByName = ep.updatedByName,
                        updatedDate = ep.updatedDate,
                    };
                    ewp.Add(o);
                }

                foreach (var ep in p.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && (pd.picId == param || p.projectManagerId == param))))
                {
                    bool editable = false;
                    var edit = IsReadonlyProjectToOnList(p.id, param, constants.ManageDocEWP);
                    if (edit != null)
                    {
                        editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                    }
                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedByName = ep.updatedByName,
                        updatedDate = ep.updatedDate,
                    };
                    ewp.Add(o);
                }

                foreach (var ep in p.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_DESIGN &&
                (pd.picId == param || p.projectManagerId == param ||
                (usc.getRoleInProject(p.id, param).FirstOrDefault().ROLENAME == constants.RoleLeadDesigner_C_M_ES ||
                usc.getRoleInProject(p.id, param).FirstOrDefault().ROLENAME == constants.RoleLeadDesigner_E_I_ES)))))
                {
                    bool editable = false;
                    var edit = IsReadonlyProjectToOnList(p.id, param,constants.ManageDesignDoc);
                    if (edit != null)
                    {
                        editable = !Convert.ToBoolean(edit[0].GetType().GetProperty("result").GetValue(edit[0], null));
                    }
                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedByName = ep.updatedByName,
                        updatedDate = ep.updatedDate,
                    };
                    dwg.Add(o);
                }

                var ec = new EmailController();
                var paramname = ec.FindNameAndEmailByBadgeNo(param).Split('|')[0];
                foreach (var co in p.cOut)
                {
                    foreach (var copg in co.cOPg.Where(c => c.asd != null))
                    {
                        if (copg.pic == "#ua#")
                        {
                            var pic = String.Join(",", copg.picUa);
                            var picId = String.Join(",", copg.picUaId);
                            var o = new
                            {
                                afd = copg.afd,
                                asd = copg.asd,
                                id = copg.id,
                                name = copg.name,
                                desc = copg.desc,
                                pfd = copg.pfd,
                                pic = pic,
                                picId = picId,
                                pNo = copg.pNo,
                                pnoName = copg.pNo + (copg.name != "" ? " - " + copg.name : ""),
                                psd = copg.psd,
                                status = copg.status,
                                statusName = copg.statusName,
                                tipe = copg.tipe,
                                tipeName = copg.tipeName,
                                coRole = copg.coRole,
                                updatedBy = copg.updatedBy,
                                updatedByName = copg.updatedByName,
                                updatedDate = copg.updatedDate,
                            };
                            if ((copg.pic != null && o.pic.Contains(paramname)) || p.projectManagerId == param)
                                coTask.Add(o);
                        }
                        else
                        {
                            if ((copg.pic != null && copg.pic.Contains(paramname)) || p.projectManagerId==param)
                                coTask.Add(copg);
                        }

                    }
                }
            }

            var t = new List<object>();
            t.Add(new { myTask = kD.Union(ewp).Union(dwg).Union(coTask) });
            var m = kD.Union(ewp).Union(dwg).Union(coTask);
            return m;
        }
    }
}
