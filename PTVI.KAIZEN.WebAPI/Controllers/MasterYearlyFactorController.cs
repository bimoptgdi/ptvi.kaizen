﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MasterYearlyFactorController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/MasterYearlyFactor
        public IQueryable<masterYearlyFactorObject> GetMasterYearlyFactor()
        {
            return db.MASTERYEARLYFACTORs.Select(s => new masterYearlyFactorObject
            {
                id = s.OID,
                year = s.YEAR,
                levelNo = s.LEVELNO,
                engineerLevelCode = s.ENGINEERLEVELCODE,
                service1 = s.SERVICE1,
                service2 = s.SERVICE2,
                service3 = s.SERVICE3,
                service4 = s.SERVICE4,
                service5 = s.SERVICE5
            });
        }

        // GET: api/MasterYearlyFactor/5
        [ResponseType(typeof(masterYearlyFactorObject))]
        public IHttpActionResult GetMASTERYEARLYFACTOR(int id)
        {
            MASTERYEARLYFACTOR mASTERYEARLYFACTOR = db.MASTERYEARLYFACTORs.Find(id);
            if (mASTERYEARLYFACTOR == null)
            {
                return NotFound();
            }

            return Ok(mASTERYEARLYFACTOR);
        }

        // PUT: api/MasterYearlyFactor/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMASTERYEARLYFACTOR(int id, masterYearlyFactorObject mASTERYEARLYFACTOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERYEARLYFACTOR.id)
            {
                return BadRequest();
            }

            db.Entry(mASTERYEARLYFACTOR).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERYEARLYFACTORExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MasterYearlyFactor
        [ResponseType(typeof(masterYearlyFactorObject))]
        public IHttpActionResult PostMasterYearlyFactor(MASTERYEARLYFACTOR MASTERYEARLYFACTOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MASTERYEARLYFACTORs.Add(MASTERYEARLYFACTOR);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { OID = MASTERYEARLYFACTOR.OID }, MASTERYEARLYFACTOR);
        }


        // DELETE: api/MasterYearlyFactor/5
        [ResponseType(typeof(masterYearlyFactorObject))]
        public IHttpActionResult DeleteMASTERYEARLYFACTOR(int id)
        {
            MASTERYEARLYFACTOR mASTERYEARLYFACTOR = db.MASTERYEARLYFACTORs.Find(id);
            if (mASTERYEARLYFACTOR == null)
            {
                return NotFound();
            }

            db.MASTERYEARLYFACTORs.Remove(mASTERYEARLYFACTOR);
            db.SaveChanges();

            return Ok(mASTERYEARLYFACTOR);
        }

        [HttpPut]
        [ActionName("updateMasterYearlyFactor")]
        public IHttpActionResult updateMasterYearlyFactor(int param, masterYearlyFactorObject mASTERYEARLYFACTORParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != mASTERYEARLYFACTORParam.id)
            {
                return BadRequest();
            }

            try
            {
                MASTERYEARLYFACTOR update = db.MASTERYEARLYFACTORs.Find(param);
                update.OID = mASTERYEARLYFACTORParam.id;
                update.YEAR = mASTERYEARLYFACTORParam.year;
                update.LEVELNO = mASTERYEARLYFACTORParam.levelNo;
                update.ENGINEERLEVELCODE = mASTERYEARLYFACTORParam.engineerLevelCode;
                update.SERVICE1 = mASTERYEARLYFACTORParam.service1;
                update.SERVICE2 = mASTERYEARLYFACTORParam.service2;
                update.SERVICE3 = mASTERYEARLYFACTORParam.service3;
                update.SERVICE4 = mASTERYEARLYFACTORParam.service4;
                update.SERVICE5 = mASTERYEARLYFACTORParam.service5;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(mASTERYEARLYFACTORParam);
        }

        [HttpGet]
        [ActionName("getMinumumYearly")]
        public int? getMinumumYearly(int param)
        {
            return db.MASTERYEARLYENGINEERINGHOURS.Min(w => w.YEAR);
        }

        [HttpPost]
        [ActionName("addMasterYearlyFactor")]
        public IHttpActionResult addMasterYearlyFactor(string param, masterYearlyFactorObject masterYearlyFactorParam)
        {
            MASTERYEARLYFACTOR add = new MASTERYEARLYFACTOR();
            add.OID = masterYearlyFactorParam.id;
            add.YEAR = masterYearlyFactorParam.year;
            add.LEVELNO = masterYearlyFactorParam.levelNo;
            add.ENGINEERLEVELCODE = masterYearlyFactorParam.engineerLevelCode;
            add.SERVICE1 = masterYearlyFactorParam.service1;
            add.SERVICE2 = masterYearlyFactorParam.service2;
            add.SERVICE3 = masterYearlyFactorParam.service3;
            add.SERVICE4 = masterYearlyFactorParam.service4;
            add.SERVICE5 = masterYearlyFactorParam.service5;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERYEARLYFACTORs.Add(add);
            db.SaveChanges();

            masterYearlyFactorParam.id = add.OID;
            return Ok(masterYearlyFactorParam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MASTERYEARLYFACTORExists(int id)
        {
            return db.MASTERYEARLYFACTORs.Count(e => e.OID == id) > 0;
        }
    }
}