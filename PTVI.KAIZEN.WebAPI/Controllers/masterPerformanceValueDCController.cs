﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterPerformanceValueDCController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/masterPerformanceValueEWP/getByYearDC/param (filter by year)
        [HttpGet]
        [ActionName("getByYearDC")]
        public IQueryable<masterPerformanceValueDCObject> getByYearDC(int param)
        {
            return db.MASTERCOMPLIANCEPERFORMANCEs.Where(w => w.YEAR == param && w.REFERENCECODE == "DC").Select(s => new masterPerformanceValueDCObject
            {
                id = s.OID,
                year = s.YEAR,
                referenceCode = s.REFERENCECODE,
                performanceValue = s.PERFORMANCEVALUE,
                min = s.MINRANGE,
                max = s.MAXRANGE
            });
        }

        // GET api/masterPerformanceValueDC/getMaxMinPerformanceValue/1
        [HttpGet]
        [ActionName("getMaxMinPerformanceValueDC")]
        public object getMaxMinPerformanceValueDC(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERCOMPLIANCEPERFORMANCEs.Min(w => w.YEAR);
            var max = db.MASTERCOMPLIANCEPERFORMANCEs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }
        // GET api/masterPerformanceValueEWP/cekDuplicateMinMax/ (for min max)
        [HttpGet]
        [ActionName("cekDuplicateMinMaxDC")]
        public Boolean cekDuplicateMinMaxDC(float param, float maxParam, int yearParam, string referenceParam, int objectId)
        {
            var duplicateMinMax = db.MASTERCOMPLIANCEPERFORMANCEs.Where(w => (w.YEAR == yearParam && w.REFERENCECODE == "DC" && w.OID != objectId) && ((param < w.MINRANGE && maxParam < w.MAXRANGE) || (param > w.MINRANGE && maxParam > w.MAXRANGE)));
            if (duplicateMinMax.Count() == db.MASTERCOMPLIANCEPERFORMANCEs.Where(w => w.YEAR == yearParam && w.REFERENCECODE == referenceParam && w.OID != objectId).Count())
            {
                return false;
            }
            else
                return true;
        }

        // POST api/masterPerformanceValueDC/addPerformanceValue/1
        [HttpPost]
        [ActionName("addPerformanceValueDC")]
        public IHttpActionResult addPerformanceValueDC(string param, masterPerformanceValueDCObject performanceValueParamDC)
        {
            MASTERCOMPLIANCEPERFORMANCE add = new MASTERCOMPLIANCEPERFORMANCE();
            add.MINRANGE = performanceValueParamDC.min;
            add.MAXRANGE = performanceValueParamDC.max;
            add.YEAR = performanceValueParamDC.year;
            add.REFERENCECODE = performanceValueParamDC.referenceCode;
            add.PERFORMANCEVALUE = performanceValueParamDC.performanceValue;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERCOMPLIANCEPERFORMANCEs.Add(add);
            db.SaveChanges();

            performanceValueParamDC.id = add.OID;
            return Ok(performanceValueParamDC);
        }

        // PUT api/masterPerformanceValueDC/updatePerformanceValue/
        [HttpPut]
        [ActionName("updatePerformanceValueDC")]
        public IHttpActionResult updatePerformanceValueDC(int param, masterPerformanceValueDCObject performanceValueParamDC)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != performanceValueParamDC.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERCOMPLIANCEPERFORMANCE update = db.MASTERCOMPLIANCEPERFORMANCEs.Find(param);
                update.YEAR = performanceValueParamDC.year;
                update.REFERENCECODE = performanceValueParamDC.referenceCode;
                update.PERFORMANCEVALUE = performanceValueParamDC.performanceValue;
                update.MINRANGE = performanceValueParamDC.min;
                update.MAXRANGE = performanceValueParamDC.max;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(performanceValueParamDC);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}