﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterYearlyTargetScoreController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/masterYearlyTargetScore
        public IQueryable<masterYearlyTargetScoreObject> GetMASTERYEARLYTARGETSCOREs()
        {
            return db.MASTERYEARLYTARGETSCOREs.Select(s => new masterYearlyTargetScoreObject
            {
                id = s.OID,
                year = s.YEAR,
                description = s.DESCRIPTION,
                value = s.VALUE,
            });
        }
        // GET: api/masterYearlyTargetScore/getByYear/2018 (filter by year)

        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterYearlyTargetScoreObject> getByYear(int param)
        {
            return db.MASTERYEARLYTARGETSCOREs.Where(x => x.YEAR == param).Select(s => new masterYearlyTargetScoreObject
            {
                id = s.OID,
                year = s.YEAR,
                code = s.CODE,
                description = s.DESCRIPTION,
                value = s.VALUE,
            });
        }

        // GET: api/masterYearlyTargetScore/5
        [ResponseType(typeof(MASTERYEARLYTARGETSCORE))]
        public IHttpActionResult GetMASTERYEARLYTARGETSCORE(int id)
        {
            MASTERYEARLYTARGETSCORE mASTERYEARLYTARGETSCORE = db.MASTERYEARLYTARGETSCOREs.Find(id);
            if (mASTERYEARLYTARGETSCORE == null)
            {
                return NotFound();
            }

            return Ok(mASTERYEARLYTARGETSCORE);
        }

        // PUT: api/masterYearlyTargetScore/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMASTERYEARLYTARGETSCORE(int id, MASTERYEARLYTARGETSCORE mASTERYEARLYTARGETSCORE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERYEARLYTARGETSCORE.OID)
            {
                return BadRequest();
            }

            db.Entry(mASTERYEARLYTARGETSCORE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERYEARLYTARGETSCOREExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/masterYearlyTargetScore
        [ResponseType(typeof(MASTERYEARLYTARGETSCORE))]
        public IHttpActionResult PostMASTERYEARLYTARGETSCORE(MASTERYEARLYTARGETSCORE mASTERYEARLYTARGETSCORE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MASTERYEARLYTARGETSCOREs.Add(mASTERYEARLYTARGETSCORE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mASTERYEARLYTARGETSCORE.OID }, mASTERYEARLYTARGETSCORE);
        }

        // DELETE: api/masterYearlyTargetScore/5
        [ResponseType(typeof(MASTERYEARLYTARGETSCORE))]
        public IHttpActionResult DeleteMASTERYEARLYTARGETSCORE(int id)
        {
            MASTERYEARLYTARGETSCORE mASTERYEARLYTARGETSCORE = db.MASTERYEARLYTARGETSCOREs.Find(id);
            if (mASTERYEARLYTARGETSCORE == null)
            {
                return NotFound();
            }

            db.MASTERYEARLYTARGETSCOREs.Remove(mASTERYEARLYTARGETSCORE);
            db.SaveChanges();

            return Ok(mASTERYEARLYTARGETSCORE);
        }

        [HttpPut]
        [ActionName("updateYearlyTarget")]

        public IHttpActionResult updateYearlyTarget(int param, masterYearlyTargetScoreObject yearlyTargetParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != yearlyTargetParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERYEARLYTARGETSCORE update = db.MASTERYEARLYTARGETSCOREs.Find(param);
                update.YEAR = yearlyTargetParam.year;
                update.DESCRIPTION = yearlyTargetParam.description;
                update.VALUE = yearlyTargetParam.value;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(yearlyTargetParam);
            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        [ActionName("addYearlyTarget")]

        public IHttpActionResult addYearlyTarget(string param, masterYearlyTargetScoreObject yearlyTargetParam)
        {
            MASTERYEARLYTARGETSCORE add = new MASTERYEARLYTARGETSCORE();
            add.YEAR = yearlyTargetParam.year;
            add.DESCRIPTION = yearlyTargetParam.description;
            add.VALUE = yearlyTargetParam.value;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERYEARLYTARGETSCOREs.Add(add);
            db.SaveChanges();

            yearlyTargetParam.id = add.OID;
            return Ok(yearlyTargetParam);
        }

        [HttpGet]
        [ActionName("getMaxMinYearlyTargetSc")]
        public object getMaxMinYearlyTargetSc(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERYEARLYTARGETSCOREs.Min(w => w.YEAR);
            var max = db.MASTERYEARLYTARGETSCOREs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }

        private bool MASTERYEARLYTARGETSCOREExists(int id)
        {
            return db.MASTERYEARLYTARGETSCOREs.Count(e => e.OID == id) > 0;
        }
    }
}