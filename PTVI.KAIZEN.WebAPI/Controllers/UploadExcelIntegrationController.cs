﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.IO;
using Excel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class UploadExcelIntegrationController : KAIZENController
    {
        public class objFiles
        {
            public int id { get; set; }
            public string sourceFile { get; set; }
            public string destinationFile { get; set; }
        }


        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();


        // GET api/Lookup
        public IEnumerable<LOOKUP> GetLOOKUPs()
        {
            return db.LOOKUPs.AsEnumerable();
        }

        [HttpGet]
        [ActionName("ProcessLoaderCs1")]
        public object ProcessLoaderCs1(string param)
        {
            List<dynamic> result = new List<dynamic>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSEFILE == "CS" && x.PURPOSETYPE == "CS");
            string filename = "";

            var listConstServEdit = db.CONSTRUCTIONSERVICEs;
            IList<CONSTRUCTIONSERVICE> listConstServAdd = new List<CONSTRUCTIONSERVICE>();

            IList<FILEINTEGRATIONLOADER> iListfIntLoader = new List<FILEINTEGRATIONLOADER>();
            var ok = 1;
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                //remark detail as input for integration loader remark
                List<string> failedDetail = new List<string>();
                filename = fileInt.SHAREFOLDER + fileInt.FILENAME;
                var shareFolder = fileInt.SHAREFOLDER;
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                if (string.IsNullOrEmpty(shareFolder))
                {
                    //log sharefolder is empty     
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                    fIntLoader.REMARK = "Sharefolder is not exist";

                    db.Entry(fIntLoader).State = EntityState.Added;

                    failedDetail.Add(fIntLoader.REMARK);
                    result.Add(new { filename = filename, failed_detail = failedDetail });
                    ok = 0;
                    continue;
                }
                else
                {
                    if (!File.Exists(filename) == true)
                    {
                        //log file is not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "File is not exist";
                        fIntLoader.LOADERBY = GetCurrentNTUserId();
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;

                        failedDetail.Add(fIntLoader.REMARK);
                        result.Add(new { filename = filename, failed_detail = failedDetail });
                        ok = 0;
                        continue;

                    }
                }
            }
            if (ok == 0)
                return result;
            else
                result.Clear();

            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                //remark detail as input for integration loader remark
                List<string> failedDetail = new List<string>();

                filename = fileInt.SHAREFOLDER + fileInt.FILENAME;
                var shareFolder = fileInt.SHAREFOLDER;
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                if (string.IsNullOrEmpty(shareFolder))
                {
                    //log sharefolder is empty     
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                    fIntLoader.REMARK = "Sharefolder is not exist";

                    db.Entry(fIntLoader).State = EntityState.Added;

                    failedDetail.Add(fIntLoader.REMARK);
                    result.Add(new { filename = filename, failed_detail = failedDetail });

                    continue;
                }
                else
                {
                    if (!File.Exists(filename))
                    {
                        //log file is not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "File is not exist";
                        fIntLoader.LOADERBY = GetCurrentNTUserId();
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;

                        failedDetail.Add(fIntLoader.REMARK);
                        result.Add(new { filename = filename, failed_detail = failedDetail });

                        continue;

                    }
                    else
                    {
                        int succeedCount = 0, failedCount = 0;
                        IExcelDataReader excelReader;
                        byte[] data = File.ReadAllBytes(filename);
                        MemoryStream stream = new MemoryStream(data);
                        if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        else
                            excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                        excelReader.IsFirstRowAsColumnNames = true;

                        DataSet ds = excelReader.AsDataSet(true);
                        var dt = ds.Tables[0];

                        foreach (DataRow row in dt.Rows)
                        {
                            int seqNo;
                            if (!int.TryParse(row[0].ToString(), out seqNo))
                            {
                                continue;
                            }

                            var ewpEwr = row[1].ToString().Split('-');

                            var pmono = row[2].ToString().Trim();

                            if (ewpEwr.Length < 2 || string.IsNullOrEmpty(pmono))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@EWP/EWR is empty or WO No is empty");
                                continue;
                            }

                            string projectNo = row[1].ToString().Substring(0, ewpEwr[0].Length).Trim();
                            //string ewpNo = row[1].ToString().Substring(ewpEwr[0].Length + 1, row[1].ToString().Length - ewpEwr[0].Length - 1).Trim();
                            string ewpNo = row[1].ToString();



                            CONSTRUCTIONSERVICE conServ = listConstServEdit.Where(w => w.PROJECTNO == projectNo && w.EWPNO == ewpNo && w.PMONO == pmono).FirstOrDefault();
                            PROJECT project = db.PROJECTs.Where(w => w.PROJECTNO == projectNo).FirstOrDefault();
                            PROJECTDOCUMENT projectDoc = db.PROJECTDOCUMENTs.Where(w => w.PROJECT.PROJECTNO == projectNo && w.DOCNO == ewpNo && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).FirstOrDefault();


                            // check if data exists in table construction service
                            if (conServ == null)
                            {
                                conServ = new CONSTRUCTIONSERVICE();
                                conServ.PROJECTNO = projectNo;
                                conServ.EWPNO = ewpNo;
                                conServ.PMONO = row[2].ToString();
                                conServ.CREATEDBY = GetCurrentNTUserId();
                                conServ.CREATEDDATE = DateTime.Now;
                            }
                            else
                            {
                                conServ.UPDATEDBY = GetCurrentNTUserId();
                                conServ.UPDATEDDATE = DateTime.Now;
                            }

                            //update another datas
                            conServ.PROJECTID = project == null ? (int?)null : project.OID;
                            conServ.PROJECTDOCUMENTID = projectDoc == null ? (int?)null : projectDoc.OID;

                            DateTime RBD;
                            if (DateTime.TryParse(row[8].ToString(), out RBD))
                            {
                                conServ.RBD = RBD;
                            }
                            else if (!string.IsNullOrEmpty(row[8].ToString()))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@wrong date format value at excel RBD column");
                                continue;
                            }

                            conServ.STATUS = string.IsNullOrEmpty(row[9].ToString().Trim()) ? null : row[9].ToString().Trim();

                            DateTime PLANSTARTDATE;
                            if (DateTime.TryParse(row[20].ToString(), out PLANSTARTDATE))
                            {
                                conServ.PLANSTARTDATE = PLANSTARTDATE;
                            }
                            else if (!string.IsNullOrEmpty(row[20].ToString()))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@wrong date format value at excel PSD column");
                                continue;
                            }

                            int ESTIMATIONDAYS;
                            if (int.TryParse(row[27].ToString(), out ESTIMATIONDAYS))
                            {
                                if (ESTIMATIONDAYS >= 0)
                                    conServ.ESTIMATIONDAYS = ESTIMATIONDAYS;
                                else
                                {
                                    ++failedCount;
                                    failedDetail.Add("row:" + row[0] + "@negative number value at excel Est Duration Days column");
                                    continue;
                                }
                            }
                            else if (!string.IsNullOrEmpty(row[27].ToString()))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@wrong number format value at excel Est Duration Days column");
                                continue;
                            }

                            if (conServ.PLANSTARTDATE != null && ESTIMATIONDAYS >= 0)
                            {
                                conServ.PLANFINISHDATE = PLANSTARTDATE.AddDays(ESTIMATIONDAYS);
                            }

                            int ACTUALDAYS;
                            if (int.TryParse(row[28].ToString(), out ACTUALDAYS))
                            {
                                if (ACTUALDAYS >= 0)
                                    conServ.ACTUALDAYS = ACTUALDAYS;
                                else
                                {
                                    ++failedCount;
                                    failedDetail.Add("row:" + row[0] + "@negative number value at excel Act Duration Days column");
                                    continue;
                                }
                            }
                            else if (!string.IsNullOrEmpty(row[28].ToString()))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@wrong number format value at excel Act Duration Days column");
                                continue;
                            }

                            float ESTIMATIONCOST;
                            if (float.TryParse(row[29].ToString(), out ESTIMATIONCOST))
                            {
                                if (ESTIMATIONCOST >= 0)
                                    conServ.ESTIMATIONCOST = ESTIMATIONCOST;
                                else
                                {
                                    ++failedCount;
                                    failedDetail.Add("row:" + row[0] + "@negative number value at excel Est Cost column");
                                    continue;
                                }
                            }
                            else if (!string.IsNullOrEmpty(row[29].ToString()))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@wrong number format value at excel Est Cost column");
                                continue;
                            }

                            float ACTUALCOST;
                            if (float.TryParse(row[30].ToString(), out ACTUALCOST))
                            {
                                if (ACTUALCOST >= 0)
                                    conServ.ACTUALCOST = ACTUALCOST;
                                else
                                {
                                    ++failedCount;
                                    failedDetail.Add("row:" + row[0] + "@negative number value at excel Act Cost column");
                                    continue;
                                }
                            }
                            else if (!string.IsNullOrEmpty(row[30].ToString()))
                            {
                                ++failedCount;
                                failedDetail.Add("row:" + row[0] + "@wrong number format value at excel Act Cost column");
                                continue;
                            }

                            conServ.EXECUTOR = string.IsNullOrEmpty(row[34].ToString().Trim()) ? null : row[34].ToString().Trim();

                            if (conServ.OID > 0)
                            {
                                //db.Entry(conServ).State = EntityState.Modified;
                            }
                            else
                            {
                                //db.Entry(conServ).State = EntityState.Added;
                                listConstServAdd.Add(conServ);
                            }
                            ++succeedCount;
                        }
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "succeedCount:" + succeedCount.ToString() + "|failedCount:" + failedCount.ToString() + "|failed_detail:" + String.Join(";", failedDetail);
                        fIntLoader.LOADERBY = GetCurrentNTUserId();
                        fIntLoader.LOADERDATE = DateTime.Now;

                        iListfIntLoader.Add(fIntLoader);
                        result.Add(new { filename = filename, success_count = succeedCount.ToString(), failed_count = failedCount, failed_detail = failedDetail });
                    }
                }
                string sourceFile = filename;
                string destinationFile = fileInt.SHAREFOLDER + "backup\\" + fileInt.FILENAME;
                if (result[0].failed_count == 0)
                {
                    if (File.Exists(destinationFile))
                        File.Delete(destinationFile);
                    File.Move(sourceFile, destinationFile);
                }
            }
            db.CONSTRUCTIONSERVICEs.AddRange(listConstServAdd);
            db.FILEINTEGRATIONLOADERs.AddRange(iListfIntLoader);

            db.SaveChanges();
            return result;
        }

        [HttpGet]
        [ActionName("ProcessLoaderCs")]
        public object ProcessLoaderCs(string param)
        {
            List<dynamic> result = new List<dynamic>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSEFILE == "CS" && x.PURPOSETYPE == "CS");
            string fileName = "";
            //string purposeFile = "";

            var listConstServEdit = db.CONSTRUCTIONSERVICEs;
            IList<CONSTRUCTIONSERVICE> listConstServAdd = new List<CONSTRUCTIONSERVICE>();

            IList<FILEINTEGRATIONLOADER> iListfIntLoader = new List<FILEINTEGRATIONLOADER>();

            bool isDoLoad = true;
            List<objFiles> lFilesMoved = new List<objFiles>();

            int idx = 0;

            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                if (File.Exists(fileInt.SHAREFOLDER + fileInt.FILENAME))
                {
                    isDoLoad = isDoLoad && true;
                }
                else {
                    isDoLoad = isDoLoad && false;
                    result.Add(new
                    {
                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        success_count = "",
                        failed_count = "",
                        error_rows = "",
                        error_column_excel = "",
                        error_column_lookup = "",
                        error_operation = "File does not exist."
                    });
                }
            }

            if (!isDoLoad)
            { // files does not exist
                return result;
            }
            else
            {
                foreach (FILEINTEGRATION fileInt in listFileIntegration)
                {
                    //error detail
                    int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                    StringBuilder errorRows = new StringBuilder();
                    List<int> idxDelRows = new List<int>();

                    var shareFolder = fileInt.SHAREFOLDER;
                    fileName = shareFolder + fileInt.FILENAME;
                    //purposeFile = fileInt.PURPOSEFILE;
                    FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();

                    IExcelDataReader excelReader;
                    byte[] data = File.ReadAllBytes(fileName);
                    MemoryStream stream = new MemoryStream(data);
                    if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    else
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                    excelReader.IsFirstRowAsColumnNames = true;

                    DataSet ds = excelReader.AsDataSet(true);
                    var dt = ds.Tables[0];
                                        
                    var len = dt.Columns.Count;

                    if (len <= 0)
                    {
                        result.Add(new
                        {
                            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            success_count = "",
                            failed_count = "",
                            error_rows = "",
                            error_column_excel = "",
                            error_column_lookup = "",
                            error_operation = "There is found no column."
                        });
                        return result;
                    }
                    else                 
                    {
                        if (fileInt.PURPOSEFILE.Trim().ToUpper() == "CS")
                        {
                            // Purpose File CS, read the excel by sequence numbers of columns
                            // the sequence numbers starts from 0            
                            // max sequence numbers is 34 -> column with title "Executor"
                            // check if datatable column length >= (max sequence + 1)

                            if (len < 35)
                            {
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = "",
                                    error_operation = "There is less of column count."
                                });
                                return result;
                            }
                            else
                            {
                                try
                                {
                                    int i = 0;


                                    foreach (DataRow row in dt.Rows)
                                    {
                                        i++;

                                        int seqNo;
                                        if (!int.TryParse(row[0].ToString(), out seqNo))
                                        {
                                            ++failedCount;                                            
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        var ewpEwr = row[1].ToString().Split('-');

                                        var pmono = row[2].ToString().Trim();

                                        if (ewpEwr.Length < 2 || string.IsNullOrEmpty(pmono))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        string projectNo = row[1].ToString().Substring(0, ewpEwr[0].Length).Trim();
                                        string ewpNo = row[1].ToString();

                                        CONSTRUCTIONSERVICE conServ = listConstServEdit.Where(w => w.PROJECTNO == projectNo && w.EWPNO == ewpNo && w.PMONO == pmono).FirstOrDefault();
                                        PROJECT project = db.PROJECTs.Where(w => w.PROJECTNO == projectNo).FirstOrDefault();
                                        PROJECTDOCUMENT projectDoc = db.PROJECTDOCUMENTs.Where(w => w.PROJECT.PROJECTNO == projectNo && w.DOCNO == ewpNo && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).FirstOrDefault();


                                        // check if data exists in table construction service
                                        if (conServ == null)
                                        {
                                            conServ = new CONSTRUCTIONSERVICE();
                                            conServ.PROJECTNO = projectNo;
                                            conServ.EWPNO = ewpNo;
                                            conServ.PMONO = row[2].ToString();
                                            conServ.CREATEDBY = GetCurrentNTUserId();
                                            conServ.CREATEDDATE = DateTime.Now;
                                        }
                                        else
                                        {
                                            conServ.UPDATEDBY = GetCurrentNTUserId();
                                            conServ.UPDATEDDATE = DateTime.Now;
                                        }

                                        //update another datas
                                        conServ.PROJECTID = project == null ? (int?)null : project.OID;
                                        conServ.PROJECTDOCUMENTID = projectDoc == null ? (int?)null : projectDoc.OID;

                                        DateTime RBD;
                                        if (DateTime.TryParse(row[8].ToString(), out RBD))
                                        {
                                            conServ.RBD = RBD;
                                        }
                                        else if (!string.IsNullOrEmpty(row[8].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        conServ.STATUS = string.IsNullOrEmpty(row[9].ToString().Trim()) ? null : row[9].ToString().Trim();

                                        DateTime PLANSTARTDATE;
                                        if (DateTime.TryParse(row[20].ToString(), out PLANSTARTDATE))
                                        {
                                            conServ.PLANSTARTDATE = PLANSTARTDATE;
                                        }
                                        else if (!string.IsNullOrEmpty(row[20].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        int ESTIMATIONDAYS;
                                        if (int.TryParse(row[27].ToString(), out ESTIMATIONDAYS))
                                        {
                                            conServ.ESTIMATIONDAYS = ESTIMATIONDAYS;
                                        }
                                        else if (!string.IsNullOrEmpty(row[27].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        if (conServ.PLANSTARTDATE != null && ESTIMATIONDAYS >= 0)
                                        {
                                            conServ.PLANFINISHDATE = PLANSTARTDATE.AddDays(ESTIMATIONDAYS);
                                        }

                                        int ACTUALDAYS;
                                        if (int.TryParse(row[28].ToString(), out ACTUALDAYS))
                                        {
                                            conServ.ACTUALDAYS = ACTUALDAYS;
                                        }
                                        else if (!string.IsNullOrEmpty(row[28].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        float ESTIMATIONCOST;
                                        if (float.TryParse(row[29].ToString(), out ESTIMATIONCOST))
                                        {
                                            conServ.ESTIMATIONCOST = ESTIMATIONCOST;
                                        }
                                        else if (!string.IsNullOrEmpty(row[29].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        float ACTUALCOST;
                                        if (float.TryParse(row[30].ToString(), out ACTUALCOST))
                                        {
                                            conServ.ACTUALCOST = ACTUALCOST;
                                        }
                                        else if (!string.IsNullOrEmpty(row[30].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            continue;
                                        }

                                        conServ.EXECUTOR = string.IsNullOrEmpty(row[34].ToString().Trim()) ? null : row[34].ToString().Trim();

                                        if (conServ.OID <= 0)
                                        {
                                            listConstServAdd.Add(conServ);
                                        }
                                        ++succeedCount;
                                    }
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                                    fIntLoader.LOADERDATE = DateTime.Now;

                                    iListfIntLoader.Add(fIntLoader);

                                    result.Add(new {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });
                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                        }
                        string backupFolder = fileInt.SHAREFOLDER + "backup\\";
                        if (!Directory.Exists(backupFolder))
                            Directory.CreateDirectory(backupFolder);
                        lFilesMoved.Add(new objFiles { id = idx + 1, sourceFile = fileInt.SHAREFOLDER + fileInt.FILENAME, destinationFile = backupFolder + fileInt.FILENAME });
                        idx++;
                    }
                    excelReader.Close();
                }
                foreach (objFiles x in lFilesMoved)
                {
                    if (File.Exists(x.destinationFile)) File.Delete(x.destinationFile);
                    File.Move(x.sourceFile, x.destinationFile);
                }
            }
            db.CONSTRUCTIONSERVICEs.AddRange(listConstServAdd);
            db.FILEINTEGRATIONLOADERs.AddRange(iListfIntLoader);

            db.SaveChanges();
            return result;
        }

       

        [HttpGet]
        [ActionName("ProcessLoaderMaterial")]
        public object ProcessLoaderMaterial(string param)
        {
            var ntUser = GetCurrentNTUserId();
            List<dynamic> result = new List<dynamic>();

            //error detail
            //StringBuilder noFiles = new StringBuilder();

            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE == "MATERIAL");
            string fileName = "";
            string purposeFile = "";

            var iMaterialComponentEdit = db.MATERIALCOMPONENTs;
            IList<MATERIALCOMPONENT> iMaterialComponentAdd = new List<MATERIALCOMPONENT>();

            var iMaterialScheduleEdit = db.MATERIALSCHEDULEs;
            IList<MATERIALSCHEDULE> iMaterialScheduleAdd = new List<MATERIALSCHEDULE>();

            var iMaterialPurchaseEdit = db.MATERIALPURCHASEs;
            IList<MATERIALPURCHASE> iMaterialPurchaseAdd = new List<MATERIALPURCHASE>();

            var iMaterialPurchaseGroupEdit = db.MATERIALPURCHASEGROUPs;
            IList<MATERIALPURCHASEGROUP> iMaterialPurchaseGroupAdd = new List<MATERIALPURCHASEGROUP>();

            var iMaterialStatusEdit = db.MATERIALSTATUS;
            IList<MATERIALSTATUS> iMaterialStatusAdd = new List<MATERIALSTATUS>();

            IList<FILEINTEGRATIONLOADER> iListfIntLoader = new List<FILEINTEGRATIONLOADER>();
            
            bool isDoLoad = true;
            List<objFiles> lFilesMoved = new List<objFiles>();            

            int idx = 0;
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                string backupFolder = fileInt.SHAREFOLDER + "backup\\";
                if (!Directory.Exists(backupFolder))
                    Directory.CreateDirectory(backupFolder);

                lFilesMoved.Add(new objFiles { id = idx + 1, sourceFile = fileInt.SHAREFOLDER + fileInt.FILENAME, destinationFile = backupFolder + fileInt.FILENAME });
                ++idx;

                if (File.Exists(fileInt.SHAREFOLDER + fileInt.FILENAME))
                {
                    isDoLoad = isDoLoad && true;
                }
                else {
                    isDoLoad = isDoLoad && false;
                    //result.Add(new { filename = fileInt.SHAREFOLDER + fileInt.FILENAME, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                    //    failed_detail = "", error = "File does not exist."
                    //});
                    result.Add( new
                    {
                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        success_count = "",
                        failed_count = "",
                        error_rows = "",
                        error_column_excel = "",
                        error_column_lookup = "",
                        error_operation = "File does not exist."
                    });
                }
            }

            if (!isDoLoad) { // files does not exist
                return result;
            }
            else
            {
                foreach (FILEINTEGRATION fileInt in listFileIntegration)
                {
                    //error detail
                    int succeedCount = 0, failedCount = 0; // valid/invalid rows count
                    StringBuilder errorRows = new StringBuilder();
                    List<int> idxDelRows = new List<int>();

                    List<string> columnNames = new List<string>(); // list column titles from lookup table with TYPE = fileIntegrationToolTipDef

                    var shareFolder = fileInt.SHAREFOLDER;
                    fileName = shareFolder + fileInt.FILENAME;
                    purposeFile = fileInt.PURPOSEFILE;
                    FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                    
                    IExcelDataReader excelReader;
                    byte[] data = null;
                    try
                    {
                        data = File.ReadAllBytes(fileName);
                    }
                    catch (IOException ex) {
                        result.Add(new
                        {
                            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            success_count = "",
                            failed_count = "",
                            error_rows = "",
                            error_column_excel = "",
                            error_column_lookup = "",
                            error_operation = ex.Message.ToString()
                        });
                        return result;
                    }
                    
                    MemoryStream stream = new MemoryStream(data);
                    if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    else
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                    excelReader.IsFirstRowAsColumnNames = true;

                    DataSet ds = excelReader.AsDataSet(true);
                    
                    // get all column names
                    var tbls = ds.Tables.Cast<DataTable>().Select(t => new
                    {
                        TableName = t.TableName,
                        Columns = t.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList()
                    }).ToList();

                    var len = tbls.First().Columns.ToList().Count; // data table columns count
                    var dt = ds.Tables[0];

                    MATERIALCOMPONENT materialComp = new MATERIALCOMPONENT();
                    MATERIALSCHEDULE materialSche = new MATERIALSCHEDULE();
                    MATERIALPURCHASE materialPurc = new MATERIALPURCHASE();
                    MATERIALPURCHASEGROUP materialPurcGroup = new MATERIALPURCHASEGROUP();
                    MATERIALSTATUS materialStat = new MATERIALSTATUS();


                    if (len <= 0)
                    {
                        result.Add( new {
                            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,  
                            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,      
                            success_count = "",
                            failed_count = "",
                            error_rows = "",
                            error_column_excel = "",
                            error_column_lookup = "",
                            error_operation = "No column found in excel file."
                        });
                        return result;
                    }
                    else
                    {
                        if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                        {
                            int i = 0;
                            List<string> listColsNotFoundExcel = new List<string>();
                            List<string> listColsNotFoundLookup = new List<string>();

                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                            List<string> excelColumns = tbls[0].Columns.ToList();

                            columnNames = new List<string> {
                                "Project definition", "WBS element", "Network", "Activity", "BOM item", "Material",
                                "Item text line 1", "Material text", "Requirement quantity", "Quantity withdrawn", "Quantity received",
                                "Base unit of measure", "Requirements date", "Goods recipient", "Reservation", "Item", "Purchase requisition",
                                "Requisition item", "Purchase Order", "Purchase Order Item"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);
                            var colsNotFoundExcel = columnNames.Except(excelColumns);

                            foreach(string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });
                                return result;
                            }

                            foreach (string x in colsNotFoundExcel) {
                                listColsNotFoundExcel.Add(x);
                            }

                            if (listColsNotFoundExcel.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundExcel);
                                result.Add( new {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,  
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,      
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = listColsStr,
                                    error_column_lookup = "",
                                    error_operation = ""
                                });
                                return result;
                            }
                            else
                            {
                                try
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        ++i;
                                        string projectNo = row["Project definition"].ToString().Trim(),
                                        wbsNo = row["WBS element"].ToString().Trim(),
                                        network = row["Network"].ToString().Trim(),
                                        networkActivity = row["Activity"].ToString().Trim(),
                                        lineItem = row["BOM item"].ToString().Trim();

                                        if (string.IsNullOrEmpty(projectNo) || string.IsNullOrEmpty(wbsNo) || string.IsNullOrEmpty(network) || string.IsNullOrEmpty(networkActivity) || string.IsNullOrEmpty(lineItem))
                                        {
                                            ++failedCount;
                                            //failedDetail.Add("row:" + i + "@null or empty value at" + fileInt.FILENAME + " column Project definition or WBS element or Network or Activity or BOM item");
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        PROJECT project = db.PROJECTs.Where(x => x.PROJECTNO == projectNo).FirstOrDefault();
                                        materialComp = iMaterialComponentEdit.Where(x => x.PROJECTNO == projectNo && x.WBSNO == wbsNo && x.NETWORK == network && x.NETWORKACTIVITY == networkActivity && x.LINEITEM == lineItem).FirstOrDefault();


                                        // check if data exists in table MATERIALCOMPONENT
                                        if (materialComp == null)
                                        //if (materialComp)
                                        {
                                            materialComp = new MATERIALCOMPONENT();
                                            materialComp.PROJECTNO = projectNo;
                                            materialComp.WBSNO = wbsNo;
                                            materialComp.NETWORK = network;
                                            materialComp.NETWORKACTIVITY = networkActivity;
                                            materialComp.LINEITEM = lineItem;
                                            materialComp.CREATEDBY = ntUser;
                                            materialComp.CREATEDDATE = DateTime.Now;
                                        }
                                        else
                                        {
                                            materialComp.UPDATEDBY = ntUser;
                                            materialComp.UPDATEDDATE = DateTime.Now;
                                        }

                                        materialComp.PROJECTID = project != null ? project.OID : (int?)null;
                                        //materialComp.EWPNO = "TBA"; 
                                        //materialComp.MRNO = "??? Sequence No";
                                        materialComp.MATERIAL = string.IsNullOrEmpty(row["Material"].ToString().Trim()) ? null : row["Material"].ToString().Trim();
                                        materialComp.MATERIALDESC = string.IsNullOrEmpty(row["Material text"].ToString().Trim()) ? (string.IsNullOrEmpty(row["Item text line 1"].ToString().Trim()) ? null : row["Item text line 1"].ToString().Trim()) : row["Material text"].ToString().Trim();

                                        int ORDERQUANTITY;
                                        if (int.TryParse(row["Requirement quantity"].ToString(), out ORDERQUANTITY))
                                        {
                                            materialComp.ORDERQUANTITY = ORDERQUANTITY;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Requirement quantity"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        int WITHDRAWNQUANTITY;
                                        if (int.TryParse(row["Quantity withdrawn"].ToString(), out WITHDRAWNQUANTITY))
                                        {
                                            materialComp.WITHDRAWNQUANTITY = WITHDRAWNQUANTITY;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Quantity withdrawn"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        int RECEIVEDQUANTITY;
                                        if (int.TryParse(row["Quantity received"].ToString(), out RECEIVEDQUANTITY))
                                        {
                                            materialComp.RECEIVEDQUANTITY = RECEIVEDQUANTITY;
                                              
                                        }
                                        else
                                        if (!string.IsNullOrEmpty(row["Quantity received"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialComp.UOM = string.IsNullOrEmpty(row["Base unit of measure"].ToString().Trim()) ? null : row["Base unit of measure"].ToString().Trim();

                                        DateTime PLANONSITE;
                                        if (DateTime.TryParse(row["Requirements date"].ToString(), out PLANONSITE))
                                        {
                                            materialComp.PLANONSITE = PLANONSITE;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Requirements date"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialComp.ENGINEER = string.IsNullOrEmpty(row["Goods recipient"].ToString().Trim()) ? null : row["Goods recipient"].ToString().Trim();
                                        materialComp.RESERVATION = string.IsNullOrEmpty(row["Reservation"].ToString().Trim()) ? null : row["Reservation"].ToString().Trim();

                                        int RESERVATIONITEMNO;
                                        if (int.TryParse(row["Item"].ToString(), out RESERVATIONITEMNO))
                                        {
                                            materialComp.RESERVATIONITEMNO = RESERVATIONITEMNO;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Item"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialComp.PRNO = string.IsNullOrEmpty(row["Purchase requisition"].ToString().Trim()) ? null : row["Purchase requisition"].ToString().Trim();
                                        materialComp.PRITEMNO = string.IsNullOrEmpty(row["Requisition item"].ToString().Trim()) ? null : row["Requisition item"].ToString().Trim();
                                        materialComp.PONO = string.IsNullOrEmpty(row["Purchase Order"].ToString().Trim()) ? null : row["Purchase Order"].ToString().Trim();
                                        materialComp.POITEMNO = string.IsNullOrEmpty(row["Purchase Order Item"].ToString().Trim()) ? null : row["Purchase Order Item"].ToString().Trim();

                                        ++succeedCount;

                                        if (materialComp.OID <= 0)
                                        {
                                            iMaterialComponentAdd.Add(materialComp);
                                        }
                                    }
                                    //update FILEINTEGRATIONLOADER
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    
                                    iListfIntLoader.Add(fIntLoader);

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    result.Add(new {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });
                                    db.MATERIALCOMPONENTs.AddRange(iMaterialComponentAdd);
                                    iMaterialComponentAdd = null;
                                }
                                catch (Exception ex)
                                {
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ex.Message.ToString()
                                    });
                                    return result;
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PR")
                        {
                            //int i = 0;
                            //    List<string> columnNames = new List<string>() {
                            //    "Purchase Requisition", "Item of Requisition", "Requisition Date", "Purchase Order Date","Req. Tracking Number"
                            //};

                            //bool isNotFoundColumn = columnNames.Except(tbls.First().Columns.ToList()).Any();

                            //if (isNotFoundColumn)
                            //{
                            //    result.Add(new
                            //    {
                            //        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            //        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            //        failed_detail = "Column not found."
                            //    });
                            //    return result;
                            //}


                            int i = 0;
                            List<string> listColsNotFoundExcel = new List<string>();
                            List<string> listColsNotFoundLookup = new List<string>();

                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                            List<string> excelColumns = tbls[0].Columns.ToList();

                            columnNames = new List<string> {
                                "Purchase Requisition", "Item of Requisition", "Requisition Date", "Purchase Order Date", "Req. Tracking Number"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);
                            var colsNotFoundExcel = columnNames.Except(excelColumns);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });
                                return result;
                            }

                            foreach (string x in colsNotFoundExcel)
                            {
                                listColsNotFoundExcel.Add(x);
                            }

                            if (listColsNotFoundExcel.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundExcel);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = listColsStr,
                                    error_column_lookup = "",
                                    error_operation = ""
                                });
                                return result;
                            }
                            else
                            {
                                try
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        ++i;
                                        string prNo = row["Purchase Requisition"].ToString().Trim();
                                        string prItemNo = row["Item of Requisition"].ToString().Trim();

                                        if (string.IsNullOrEmpty(prNo) || string.IsNullOrEmpty(prItemNo))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialPurc = iMaterialPurchaseEdit.Where(x => x.PRNO == prNo && x.PRITEMNO == prItemNo).FirstOrDefault();

                                        // check if data exists in table material purchase
                                        if (materialPurc == null)
                                        {
                                            materialPurc = new MATERIALPURCHASE();
                                            materialPurc.PRNO = prNo;
                                            materialPurc.PRITEMNO = prItemNo;
                                            materialPurc.CREATEDBY = ntUser;
                                            materialPurc.CREATEDDATE = DateTime.Now;
                                        }
                                        else
                                        {
                                            materialPurc.UPDATEDBY = ntUser;
                                            materialPurc.UPDATEDDATE = DateTime.Now;
                                        }

                                        materialPurc.TRACKINGNO = string.IsNullOrEmpty(row["Req. Tracking Number"].ToString().Trim()) ? null : row["Req. Tracking Number"].ToString().Trim();

                                        DateTime PRISSUEDDATE;
                                        if (DateTime.TryParse(row["Requisition Date"].ToString(), out PRISSUEDDATE))
                                        {
                                            materialPurc.PRISSUEDDATE = PRISSUEDDATE;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Requisition Date"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        DateTime PORAISEDDATE;
                                        if (DateTime.TryParse(row["Purchase Order Date"].ToString(), out PORAISEDDATE))
                                        {
                                            materialPurc.PORAISEDDATE = PORAISEDDATE;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Purchase Order Date"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        ++succeedCount;

                                        if (materialPurc.OID <=0)
                                        {
                                            iMaterialPurchaseAdd.Add(materialPurc);
                                        }
                                    }

                                    //update FILEINTEGRATIONLOADER
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    
                                    iListfIntLoader.Add(fIntLoader);

                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                    iMaterialComponentAdd = new List<MATERIALCOMPONENT>();

                                    db.MATERIALPURCHASEs.AddRange(iMaterialPurchaseAdd);
                                    iMaterialPurchaseAdd = null;
                                }
                                catch (Exception ex)
                                {
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ex.Message.ToString()
                                    });
                                    return result;
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PRGROUP")
                        {
                            //    //int i = 0;
                            ////    List<string> columnNames = new List<string>() {
                            ////    "Purchase Requisition", "Item of Requisition", "Purchasing Group", "Purchasing group Description"
                            ////};

                            //    bool isNotFoundColumn = columnNames.Except(tbls.First().Columns.ToList()).Any();

                            //    if (isNotFoundColumn)
                            //    {
                            //        result.Add(new
                            //        {
                            //            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            //            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            //            failed_detail = "Column not found."
                            //        });
                            //        return result;
                            //    }

                            int i = 0;
                            List<string> listColsNotFoundExcel = new List<string>();
                            List<string> listColsNotFoundLookup = new List<string>();

                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                            List<string> excelColumns = tbls[0].Columns.ToList();

                            columnNames = new List<string> {
                                "Purchase Requisition", "Item of Requisition", "Purchasing Group", "Purchasing group Description"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);
                            var colsNotFoundExcel = columnNames.Except(excelColumns);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });
                                return result;
                            }

                            foreach (string x in colsNotFoundExcel)
                            {
                                listColsNotFoundExcel.Add(x);
                            }

                            if (listColsNotFoundExcel.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundExcel);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = listColsStr,
                                    error_column_lookup = "",
                                    error_operation = ""
                                });
                                return result;
                            }
                            else
                            {
                                try
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        ++i;
                                        string prNo = row["Purchase Requisition"].ToString().Trim();
                                        string prItemNo = row["Item of Requisition"].ToString().Trim();

                                        if (string.IsNullOrEmpty(prNo) || string.IsNullOrEmpty(prItemNo))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialPurcGroup = iMaterialPurchaseGroupEdit.Where(x => x.PRNO == prNo && x.PRITEMNO == prItemNo).FirstOrDefault();

                                        // check if data exists in table material purchase
                                        if (materialPurcGroup == null)
                                        {
                                            materialPurcGroup = new MATERIALPURCHASEGROUP();
                                            materialPurcGroup.PRNO = prNo;
                                            materialPurcGroup.PRITEMNO = prItemNo;
                                            materialPurcGroup.CREATEDBY = ntUser;
                                            materialPurcGroup.CREATEDDATE = DateTime.Now;
                                        }
                                        else
                                        {
                                            materialPurcGroup.UPDATEDBY = ntUser;
                                            materialPurcGroup.UPDATEDDATE = DateTime.Now;
                                        }

                                        materialPurcGroup.PRGROUPCODE = string.IsNullOrEmpty(row["Purchasing Group"].ToString().Trim()) ? null : row["Purchasing Group"].ToString().Trim();
                                        materialPurcGroup.PRGROUPDESC = string.IsNullOrEmpty(row["Purchasing group Description"].ToString().Trim()) ? null : row["Purchasing group Description"].ToString().Trim();


                                        ++succeedCount;

                                        if (materialPurc.OID <= 0)
                                        {
                                            iMaterialPurchaseGroupAdd.Add(materialPurcGroup);
                                        }
                                    }

                                    //update FILEINTEGRATIONLOADER
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    
                                    iListfIntLoader.Add(fIntLoader);

                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });
                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                    db.MATERIALPURCHASEGROUPs.AddRange(iMaterialPurchaseGroupAdd);
                                    iMaterialPurchaseGroupAdd = null;
                                }
                                catch (Exception ex)
                                {
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ex.Message.ToString()
                                    });
                                    return result;
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "PO")
                        {
                            //    int i = 0;
                            ////    List<string> columnNames = new List<string>() {
                            ////    "Purchase Requisition", "Item of Requisition", "Delivery Date", "Order Quantity", "Currency", "Net price",
                            ////    "Order Price Unit", "Net Order Value", "Qty Delivered", "Quantity Received", "Purchasing Group", "Vendor/supplying plant"
                            ////};

                            //    bool isNotFoundColumn = columnNames.Except(tbls.First().Columns.ToList()).Any();

                            //    if (isNotFoundColumn)
                            //    {
                            //        result.Add(new
                            //        {
                            //            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            //            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            //            failed_detail = "Column not found."
                            //        });
                            //        return result;
                            //    }
                            int i = 0;
                            List<string> listColsNotFoundExcel = new List<string>();
                            List<string> listColsNotFoundLookup = new List<string>();

                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                            List<string> excelColumns = tbls[0].Columns.ToList();

                            columnNames = new List<string> {
                                "Purchase Requisition", "Item of Requisition", "Delivery Date", "Order Quantity",
                                "Currency", "Net price", "Order Price Unit", "Net Order Value", "Qty Delivered",
                                "Quantity Received", "Purchasing Group", "Vendor/supplying plant"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);
                            var colsNotFoundExcel = columnNames.Except(excelColumns);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });
                                return result;
                            }

                            foreach (string x in colsNotFoundExcel)
                            {
                                listColsNotFoundExcel.Add(x);
                            }

                            if (listColsNotFoundExcel.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundExcel);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = listColsStr,
                                    error_column_lookup = "",
                                    error_operation = ""
                                });
                                return result;
                            }
                            else
                            {
                                try
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        ++i;

                                        string prNo = row["Purchase Requisition"].ToString().Trim();
                                        string prItemNo = row["Item of Requisition"].ToString().Trim();

                                        if (string.IsNullOrEmpty(prNo) || string.IsNullOrEmpty(prItemNo))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialSche = iMaterialScheduleEdit.Where(x => x.PRNO == prNo && x.PRITEMNO == prItemNo).FirstOrDefault();

                                        // check if data exists in table material purchase
                                        if (materialSche == null)
                                        {
                                            materialSche = new MATERIALSCHEDULE();
                                            materialSche.PRNO = prNo;
                                            materialSche.PRITEMNO = prItemNo;
                                            materialSche.CREATEDBY = ntUser;
                                            materialSche.CREATEDDATE = DateTime.Now;
                                        }
                                        else
                                        {
                                            materialSche.UPDATEDBY = ntUser;
                                            materialSche.UPDATEDDATE = DateTime.Now;
                                        }

                                        DateTime DELIVERYDATE;
                                        if (DateTime.TryParse(row["Delivery Date"].ToString(), out DELIVERYDATE))
                                        {
                                            materialSche.DELIVERYDATE = DELIVERYDATE;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Delivery Date"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                        }

                                        float ORDEREDQUANTITY;
                                        if (float.TryParse(row["Order Quantity"].ToString(), out ORDEREDQUANTITY))
                                        {
                                            materialSche.ORDEREDQUANTITY = ORDEREDQUANTITY;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Order Quantity"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialSche.CURRENCY = string.IsNullOrEmpty(row["Currency"].ToString().Trim()) ? null : row["Currency"].ToString().Trim();

                                        float UNITPRICE;
                                        if (float.TryParse(row["Net price"].ToString(), out UNITPRICE))
                                        {
                                            materialSche.UNITPRICE = UNITPRICE;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Net price"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialSche.UNITQUANTITY = string.IsNullOrEmpty(row["Order Price Unit"].ToString().Trim()) ? null : row["Order Price Unit"].ToString().Trim();

                                        float TOTALPRICE;
                                        if (float.TryParse(row["Net Order Value"].ToString(), out TOTALPRICE))
                                        {
                                            materialSche.TOTALPRICE = TOTALPRICE;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Net Order Value"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        float DELIVEREDQUANTITY;
                                        if (float.TryParse(row["Qty Delivered"].ToString(), out DELIVEREDQUANTITY))
                                        {
                                            materialSche.DELIVEREDQUANTITY = DELIVEREDQUANTITY;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Qty Delivered"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        float RECEIVEDQUANTITY;
                                        if (float.TryParse(row["Quantity Received"].ToString(), out RECEIVEDQUANTITY))
                                        {
                                            materialSche.RECEIVEDQUANTITY = RECEIVEDQUANTITY;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Quantity Received"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialSche.BUYER = string.IsNullOrEmpty(row["Purchasing Group"].ToString().Trim()) ? null : row["Purchasing Group"].ToString().Trim();
                                        materialSche.VENDOR = string.IsNullOrEmpty(row["Vendor/supplying plant"].ToString().Trim()) ? null : row["Vendor/supplying plant"].ToString().Trim();

                                        ++succeedCount;

                                        if (materialSche.OID <= 0)
                                        {
                                            iMaterialScheduleAdd.Add(materialSche);
                                        }
                                    }

                                    //update FILEINTEGRATIONLOADER
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    
                                    iListfIntLoader.Add(fIntLoader);
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });
                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                    db.MATERIALSCHEDULEs.AddRange(iMaterialScheduleAdd);
                                    iMaterialScheduleAdd = null;
                                }
                                catch (Exception ex)
                                {
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ex.Message.ToString()
                                    });
                                    return result;
                                }
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "DELIVERY")
                        {
                            //int i = 0;
                            ////List<string> columnNames = new List<string>() {
                            ////    "PR", "Request Item PR", "Reservation", "Request Item Reservation", "Code Lookup", "Special Stock",
                            ////    "Reservation Withdrawal Qty", "Incoterms PO (Part 1)", "Incoterms PO (Part 2)",
                            ////    "STO Number", "Delivery No.", "Shipment No.", "Shipment Start", "PO Delivery Date", "Shipment End",
                            ////    "Shipment Route", "Unloading Point", "Latest Goods Receipt Date"
                            ////};

                            //bool isNotFoundColumn = columnNames.Except(tbls.First().Columns.ToList()).Any();

                            //if (isNotFoundColumn)
                            //{
                            //    result.Add(new
                            //    {
                            //        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            //        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            //        failed_detail = "Column not found."
                            //    });
                            //    return result;
                            //}
                            int i = 0;
                            List<string> listColsNotFoundExcel = new List<string>();
                            List<string> listColsNotFoundLookup = new List<string>();

                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                            List<string> excelColumns = tbls[0].Columns.ToList();

                            columnNames = new List<string> {
                                "PR", "Request Item PR", "Reservation", "Request Item Reservation", "Code Lookup", "Special Stock",
                                "Reservation Withdrawal Qty", "Incoterms PO (Part 1)", "Incoterms PO (Part 2)",
                                "STO Number", "Delivery No.", "Shipment No.", "Shipment Start", "PO Delivery Date", "Shipment End",
                                "Shipment Route", "Unloading Point", "Latest Goods Receipt Date"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);
                            var colsNotFoundExcel = columnNames.Except(excelColumns);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });
                                return result;
                            }

                            foreach (string x in colsNotFoundExcel)
                            {
                                listColsNotFoundExcel.Add(x);
                            }

                            if (listColsNotFoundExcel.Count > 0)
                            {
                                string listColsStr = string.Join(", ", listColsNotFoundExcel);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = listColsStr,
                                    error_column_lookup = "",
                                    error_operation = ""
                                });
                                return result;
                            }
                            else
                            {
                                try
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        ++i;
                                        string prNo = row["PR"].ToString().Trim();
                                        string prItemNo = row["Request Item PR"].ToString().Trim();
                                        string reservation = row["Reservation"].ToString().Trim();

                                        int reservationItemNo;
                                        int.TryParse(row["Request Item Reservation"].ToString(), out reservationItemNo);
                                        if ((string.IsNullOrEmpty(prNo) || string.IsNullOrEmpty(prItemNo)) && (string.IsNullOrEmpty(reservation) || !int.TryParse(row["Request Item Reservation"].ToString().Trim(), out reservationItemNo)))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialStat = iMaterialStatusEdit.Where(x => (x.PRNO == prNo && x.PRITEMNO == prItemNo && x.PRNO != "0" && x.PRNO != "" && x.PRNO != null && x.PRITEMNO != "0" && x.PRITEMNO != "" && x.PRITEMNO != null) || (x.RESERVATION == reservation && x.RESERVATIONITEMNO == reservationItemNo && x.RESERVATION != "0" && x.RESERVATION != "" && x.RESERVATION != null && x.RESERVATIONITEMNO != 0 && x.RESERVATIONITEMNO.HasValue)).FirstOrDefault();

                                        // check if data exists in table material purchase
                                        if (materialStat == null)
                                        {

                                            materialStat = new MATERIALSTATUS();
                                            materialStat.PRNO = prNo;
                                            materialStat.PRITEMNO = prItemNo;
                                            materialStat.RESERVATION = reservation;
                                            materialStat.RESERVATIONITEMNO = reservationItemNo;
                                            materialStat.CREATEDBY = ntUser;
                                            materialStat.CREATEDDATE = DateTime.Now;
                                        }
                                        else
                                        {
                                            materialStat.UPDATEDBY = ntUser;
                                            materialStat.UPDATEDDATE = DateTime.Now;
                                        }

                                        materialStat.CODELOOKUP = string.IsNullOrEmpty(row["Code Lookup"].ToString().Trim()) ? null : row["Code Lookup"].ToString().Trim();
                                        materialStat.SPECIALSTOCK = string.IsNullOrEmpty(row["Special Stock"].ToString().Trim()) ? null : row["Special Stock"].ToString().Trim();

                                        int RESERVATIONWITHDRAWALQUANTITY;
                                        if (int.TryParse(row["Reservation Withdrawal Qty"].ToString(), out RESERVATIONWITHDRAWALQUANTITY))
                                        {
                                            materialStat.RESERVATIONWITHDRAWALQUANTITY = RESERVATIONWITHDRAWALQUANTITY;
                                        }
                                        else if (!string.IsNullOrEmpty(row["Reservation Withdrawal Qty"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialStat.INCOTERMSPO = string.IsNullOrEmpty(row["Incoterms PO (Part 1)"].ToString().Trim()) ? null : row["Incoterms PO (Part 1)"].ToString().Trim();
                                        materialStat.TRAFFICMETHOD = string.IsNullOrEmpty(row["Incoterms PO (Part 2)"].ToString().Trim()) ? null : row["Incoterms PO (Part 2)"].ToString().Trim();
                                        materialStat.STONO = string.IsNullOrEmpty(row["STO Number"].ToString().Trim()) ? null : row["STO Number"].ToString().Trim();
                                        materialStat.DELIVERYNO = string.IsNullOrEmpty(row["Delivery No."].ToString().Trim()) ? null : row["Delivery No."].ToString().Trim();
                                        materialStat.SHIPMENTNO = string.IsNullOrEmpty(row["Shipment No."].ToString().Trim()) ? null : row["Shipment No."].ToString().Trim();

                                        try
                                        {
                                            DateTime? shipmentStartDate = string.IsNullOrEmpty(row["Shipment Start"].ToString()) ? (DateTime?)null : DateTime.ParseExact(row["Shipment Start"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                            materialStat.SHIPMENTSTARTDATE = shipmentStartDate;
                                        }
                                        catch
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        try
                                        {
                                            DateTime? poDeliveryDate = string.IsNullOrEmpty(row["PO Delivery Date"].ToString()) ? (DateTime?)null : DateTime.ParseExact(row["PO Delivery Date"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                            materialStat.ETASITEDATE = poDeliveryDate;
                                        }
                                        catch
                                        {
                                            ++failedCount;
                                            //failedDetail.Add("row:" + i + "@wrong date format value at " + fileInt.FILENAME + " column PO Delivery Date");
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        try
                                        {
                                            DateTime? shipmentEndDate = string.IsNullOrEmpty(row["Shipment End"].ToString()) ? (DateTime?)null : DateTime.ParseExact(row["Shipment End"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                            materialStat.SHIPMENTENDDATE = shipmentEndDate;
                                        }
                                        catch
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        materialStat.SHIPMENTROUTE = string.IsNullOrEmpty(row["Shipment Route"].ToString().Trim()) ? null : row["Shipment Route"].ToString().Trim();
                                        materialStat.LOCATION = string.IsNullOrEmpty(row["Unloading Point"].ToString().Trim()) ? null : row["Unloading Point"].ToString().Trim();

                                        try
                                        {
                                            DateTime? actualDeliveryDate = string.IsNullOrEmpty(row["Latest Goods Receipt Date"].ToString()) ? (DateTime?)null : DateTime.ParseExact(row["Latest Goods Receipt Date"].ToString(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                            materialStat.ACTUALDELIVERYDATE = actualDeliveryDate;
                                        }
                                        catch
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        ++succeedCount;

                                        if (materialStat.OID <= 0)
                                        {
                                            iMaterialStatusAdd.Add(materialStat);
                                        }
                                    }

                                    //update FILEINTEGRATIONLOADER
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    
                                    iListfIntLoader.Add(fIntLoader);

                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });
                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });

                                    db.MATERIALSTATUS.AddRange(iMaterialStatusAdd);
                                    iMaterialStatusAdd = null;
                                }
                                catch (Exception ex)
                                {
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ex.Message.ToString()
                                    });
                                    return result;
                                }
                            }
                        }
                        else continue;
                    }
                    excelReader.Close();
                }
                //remove & move file
                foreach (objFiles x in lFilesMoved)
                {
                    if (File.Exists(x.destinationFile)) File.Delete(x.destinationFile);
                    File.Move(x.sourceFile, x.destinationFile);
                }
            }

            db.FILEINTEGRATIONLOADERs.AddRange(iListfIntLoader);
            db.SaveChanges();

            //update table MATERIALPURCHASEs column MATERIALCOMPONENTID
            string sql = @"UPDATE MATERIALCOMPONENT
                            SET MATERIALCOMPONENT.MRNO = case when charindex('-', MP.TRACKINGNO) > 0 then SUBSTRING(MP.TRACKINGNO, 0, charindex('-', MP.TRACKINGNO)) else MP.TRACKINGNO end
                            FROM MATERIALCOMPONENT MC
                            INNER JOIN MATERIALPURCHASE MP
                            ON MP.PRNO = MC.PRNO and MP.PRITEMNO = MC.PRITEMNO";// AND MC.MRNO is null";
            db.Database.ExecuteSqlCommand(sql);


            //update table MATERIALPURCHASEs column MATERIALCOMPONENTID
            sql = @"UPDATE MATERIALPURCHASE
                            SET MATERIALPURCHASE.MATERIALCOMPONENTID = MC.OID
                            FROM MATERIALPURCHASE MP
                            INNER JOIN MATERIALCOMPONENT MC
                            ON MP.PRNO = MC.PRNO and MP.PRITEMNO = MC.PRITEMNO and MP.MATERIALCOMPONENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            //update table MATERIALPURCHASEGROUPs column MATERIALCOMPONENTID
            sql = @"UPDATE MATERIALPURCHASEGROUP
                            SET MATERIALPURCHASEGROUP.MATERIALCOMPONENTID = MC.OID
                            FROM MATERIALPURCHASEGROUP MP
                            INNER JOIN MATERIALCOMPONENT MC
                            ON MP.PRNO = MC.PRNO and MP.PRITEMNO = MC.PRITEMNO and MP.MATERIALCOMPONENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            //update table MATERIALSCHEDULEs column MATERIALCOMPONENTID
            sql = @"UPDATE MATERIALSCHEDULE
                            SET MATERIALSCHEDULE.MATERIALCOMPONENTID = MC.OID
                            FROM MATERIALSCHEDULE MS
                            INNER JOIN MATERIALCOMPONENT MC
                            ON MS.PRNO = MC.PRNO and MS.PRITEMNO = MC.PRITEMNO and MS.MATERIALCOMPONENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            //update table MATERIALSTATUS column MATERIALCOMPONENTID
            sql = @"UPDATE MATERIALSTATUS
                    SET MATERIALSTATUS.MATERIALCOMPONENTID = MC.OID
                    FROM MATERIALSTATUS MS
                    INNER JOIN MATERIALCOMPONENT MC
                    ON MS.PRNO = MC.PRNO and MS.PRITEMNO = MC.PRITEMNO
                    where MS.PRNO is not null and MC.PRNO != ''
                        and MS.PRITEMNO is not null and MC.PRITEMNO != ''
                        and MS.MATERIALCOMPONENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            sql = @"UPDATE MATERIALSTATUS
                    SET MATERIALSTATUS.MATERIALCOMPONENTID = MC.OID
                    FROM MATERIALSTATUS MS
                    INNER JOIN MATERIALCOMPONENT MC
                    ON MS.RESERVATION = MC.RESERVATION and MS.RESERVATIONITEMNO = MC.RESERVATIONITEMNO
                    where MS.RESERVATION is not null and MS.RESERVATION != ''
                        and MS.RESERVATIONITEMNO is not null and MC.RESERVATIONITEMNO != ''
                        and MS.MATERIALCOMPONENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            return result;
        }

        [HttpGet]
        [ActionName("ProcessLoaderCostX")]
        public object ProcessLoaderCost(string param) {

            var ntUser = GetCurrentNTUserId();
            List<dynamic> result = new List<dynamic>();
            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "COST");
            string fileName = "";
            string purposeFile = "";

            var iCostReportElementEdit = db.COSTREPORTELEMENTs;
            IList<COSTREPORTELEMENT> iCostReportElementAdd = new List<COSTREPORTELEMENT>();

            var iCostReportNetworkEdit = db.COSTREPORTNETWORKs;
            IList<COSTREPORTNETWORK> iCostReportNetworkAdd = new List<COSTREPORTNETWORK>();

            var iCostReportBudgetEdit = db.COSTREPORTBUDGETs;
            IList<COSTREPORTBUDGET> iCostReportBudgetAdd = new List<COSTREPORTBUDGET>();

            var iCostReportActualCostEdit = db.COSTREPORTACTUALCOSTs;
            IList<COSTREPORTACTUALCOST> iCostReportActualCostAdd = new List<COSTREPORTACTUALCOST>();

            var iCostReportCommitmentEdit = db.COSTREPORTCOMMITMENTs;
            IList<COSTREPORTCOMMITMENT> iCostReportCommitmentAdd = new List<COSTREPORTCOMMITMENT>();

            IList<FILEINTEGRATIONLOADER> iListfIntLoader = new List<FILEINTEGRATIONLOADER>();

            foreach (FILEINTEGRATION fileInt in listFileIntegration) {
                var shareFolder = fileInt.SHAREFOLDER;
                fileName = shareFolder + fileInt.FILENAME;
                purposeFile = fileInt.PURPOSEFILE;
                List<string> failedDetail = new List<string>();
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();

                if (string.IsNullOrEmpty(shareFolder))
                {
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = ntUser;
                    fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | Sharefolder does not exist";

                    db.Entry(fIntLoader).State = EntityState.Added;

                    failedDetail.Add(fIntLoader.REMARK);
                    result.Add(new { file_name = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = failedDetail });

                    continue;
                }
                else {
                    if (!File.Exists(fileName))
                    {
                        //log file does not exists
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.REMARK = "Purpose : " + fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE + " | File does not exist";
                        fIntLoader.LOADERBY = ntUser;
                        fIntLoader.LOADERDATE = DateTime.Now;
                        db.Entry(fIntLoader).State = EntityState.Added;

                        failedDetail.Add(fIntLoader.REMARK);
                        result.Add(new { file_name = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, failed_detail = failedDetail });

                        continue;
                    }
                    else {
                        int succeedCount = 0, failedCount = 0;
                        IExcelDataReader excelReader;
                        byte[] data = File.ReadAllBytes(fileName);
                        MemoryStream stream = new MemoryStream(data);
                        if (fileInt.FILENAME.ToUpper().Contains(".XLSX"))
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        else
                            excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                        excelReader.IsFirstRowAsColumnNames = true;

                        DataSet ds = excelReader.AsDataSet(true);
                        var dt = ds.Tables[0];

                        COSTREPORTELEMENT costRepEle = new COSTREPORTELEMENT();
                        COSTREPORTNETWORK costRepNet = new COSTREPORTNETWORK();
                        COSTREPORTBUDGET costRepBud = new COSTREPORTBUDGET();
                        COSTREPORTACTUALCOST costRepActCost = new COSTREPORTACTUALCOST();
                        COSTREPORTCOMMITMENT costRepCom = new COSTREPORTCOMMITMENT();

                        if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                        {
                            int i = 0;

                            foreach (DataRow row in dt.Rows) {
                                ++i;

                                string projectNo = row["Project definition"].ToString().Trim();
                                int wbsLevel;
                                int.TryParse(row["Level"].ToString(), out wbsLevel);
                                string wbsNo = row["WBS element"].ToString().Trim();

                                if (string.IsNullOrEmpty(projectNo) || !int.TryParse(row["Level"].ToString().Trim(), out wbsLevel) || string.IsNullOrEmpty(wbsNo)) {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@null or empty value at" + fileInt.FILENAME + " column Project definition or Level or WBS element");
                                    continue;
                                }

                                costRepEle = iCostReportElementEdit.Where(x=>x.PROJECTNO == projectNo && x.WBSLEVEL == wbsLevel && x.WBSNO == wbsNo).FirstOrDefault();

                                if (costRepEle == null){
                                    costRepEle = new COSTREPORTELEMENT();
                                    costRepEle.PROJECTNO = projectNo;
                                    costRepEle.WBSLEVEL = wbsLevel;
                                    costRepEle.WBSNO = wbsNo;
                                    costRepEle.CREATEDBY = ntUser;
                                    costRepEle.CREATEDDATE = DateTime.Now;
                                }
                                else {
                                    costRepEle.UPDATEDBY = ntUser;
                                    costRepEle.UPDATEDDATE = DateTime.Now;
                                }

                                costRepEle.DESCRIPTION = string.IsNullOrEmpty(row["Name"].ToString().Trim()) ? null : row["Name"].ToString().Trim();

                                ++succeedCount;

                                if (costRepEle.OID > 0)
                                {
                                    //db.Entry(costRepEle).State = EntityState.Modified;
                                }
                                else
                                {
                                    //db.Entry(costRepEle).State = EntityState.Added;
                                    iCostReportElementAdd.Add(costRepEle);
                                }
                            }

                            //update FILEINTEGRATIONLOADER
                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            fIntLoader.REMARK = "succeedCount:" + succeedCount.ToString() + "|failedCount:" + failedCount.ToString() + "|failed_detail:" + String.Join(";", failedDetail);
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;

                            db.Entry(fIntLoader).State = EntityState.Added;

                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount, failed_detail = failedDetail });

                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "NETWORK")
                        {
                            int i = 0;

                            foreach (DataRow row in dt.Rows) {
                                ++i;

                                string projectNo = row["Project definition"].ToString().Trim();
                                string wbsNo = row["WBS element"].ToString().Trim();

                                if (string.IsNullOrEmpty(projectNo) || string.IsNullOrEmpty(wbsNo)) {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@null or empty value at" + fileInt.FILENAME + " column Project definition or WBS element");
                                    continue;
                                }

                                costRepNet = iCostReportNetworkEdit.Where(x=>x.PROJECTNO == projectNo && x.WBSNO == wbsNo).FirstOrDefault();

                                if (costRepNet == null)
                                {
                                    costRepNet = new COSTREPORTNETWORK();
                                    costRepNet.PROJECTNO = projectNo;
                                    costRepNet.WBSNO = wbsNo;
                                    costRepNet.CREATEDBY = ntUser;
                                    costRepNet.CREATEDDATE = DateTime.Now;
                                }
                                else {
                                    costRepNet.UPDATEDBY = ntUser;
                                    costRepNet.UPDATEDDATE = DateTime.Now;
                                }

                                costRepNet.NETWORK = string.IsNullOrEmpty(row["Network"].ToString().Trim()) ? null : row["Network"].ToString().Trim();
                                costRepNet.DESCRIPTION = string.IsNullOrEmpty(row["Text"].ToString().Trim()) ? null : row["Text"].ToString().Trim();

                                ++succeedCount;

                                if (costRepNet.OID > 0)
                                {
                                    //db.Entry(costRepNet).State = EntityState.Modified;
                                }
                                else
                                {
                                    //db.Entry(costRepNet).State = EntityState.Added;
                                    iCostReportNetworkAdd.Add(costRepNet);
                                }
                            }

                            //update FILEINTEGRATIONLOADER
                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            fIntLoader.REMARK = "succeedCount:" + succeedCount.ToString() + "|failedCount:" + failedCount.ToString() + "|failed_detail:" + String.Join(";", failedDetail);
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;

                            db.Entry(fIntLoader).State = EntityState.Added;

                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount, failed_detail = failedDetail });
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "BUDGET")
                        {
                            int i = 0;
                            foreach (DataRow row in dt.Rows) {
                                ++i;

                                string projectNo = row["Project Definition"].ToString().Trim();
                                string wbsNo = row["WBS Element"].ToString().Trim();
                                int year;
                                int.TryParse(row["Fiscal Year"].ToString().Trim(), out year);

                                if (string.IsNullOrEmpty(projectNo) || string.IsNullOrEmpty(wbsNo) || !int.TryParse(row["Fiscal Year"].ToString().Trim(), out year)) {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@null or empty value at" + fileInt.FILENAME + " column Project Definition or WBS Element or Fiscal Year");
                                    continue;
                                }

                                costRepBud = iCostReportBudgetEdit.Where(x => x.PROJECTNO == projectNo && x.WBSNO == wbsNo && x.YEAR == year).FirstOrDefault();

                                if (costRepBud == null)
                                {
                                    costRepBud = new COSTREPORTBUDGET();
                                    costRepBud.PROJECTNO = projectNo;
                                    costRepBud.WBSNO = wbsNo;
                                    costRepBud.YEAR = year;
                                    costRepBud.CREATEDBY = ntUser;
                                    costRepBud.CREATEDDATE = DateTime.Now;
                                }
                                else {
                                    costRepBud.UPDATEDBY = ntUser;
                                    costRepBud.UPDATEDDATE = DateTime.Now;
                                }

                                float AMOUNT;
                                if (float.TryParse(row["Value TranCurr"].ToString(), out AMOUNT))
                                {
                                    if (AMOUNT >= 0)
                                        costRepBud.AMOUNT = AMOUNT;
                                    else
                                    {
                                        ++failedCount;
                                        failedDetail.Add("row:" + i + "@negative number value at excel Value TranCurr column");
                                        continue;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(row["Value TranCurr"].ToString()))
                                {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@wrong number format value at excel Value TranCurr column");
                                    continue;
                                }

                                ++succeedCount;

                                if (costRepBud.OID > 0)
                                {
                                    //db.Entry(costRepBud).State = EntityState.Modified;
                                }
                                else
                                {
                                    //db.Entry(costRepBud).State = EntityState.Added;
                                    iCostReportBudgetAdd.Add(costRepBud);
                                }
                            }

                            //update FILEINTEGRATIONLOADER
                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            fIntLoader.REMARK = "succeedCount:" + succeedCount.ToString() + "|failedCount:" + failedCount.ToString() + "|failed_detail:" + String.Join(";", failedDetail);
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;

                            db.Entry(fIntLoader).State = EntityState.Added;
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUAL")
                        {
                            int i = 0;
                            foreach (DataRow row in dt.Rows) {
                                ++i;

                                string projectNo = row["Project Definition"].ToString().Trim();
                                string wbsNo = row["WBS Element"].ToString().Trim();
                                int year;
                                int.TryParse(row["Fiscal Year"].ToString().Trim(), out year);
                                int month;
                                int.TryParse(row["Period"].ToString().Trim(), out month);

                                if (string.IsNullOrEmpty(projectNo) || string.IsNullOrEmpty(wbsNo) || month == 0 || year == 0) {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@null or empty value at" + fileInt.FILENAME + " column Project Definition or WBS element or Fiscal Year or Period");
                                    continue;
                                }

                                costRepActCost = iCostReportActualCostEdit.Where(x => x.PROJECTNO == projectNo && x.WBSNO == wbsNo && x.YEAR == year && x.MONTH == month).FirstOrDefault();

                                if (costRepActCost == null)
                                {
                                    costRepActCost = new COSTREPORTACTUALCOST();
                                    costRepActCost.PROJECTNO = projectNo;
                                    costRepActCost.WBSNO = wbsNo;
                                    costRepActCost.YEAR = year;
                                    costRepActCost.MONTH = month;
                                    costRepActCost.CREATEDBY = ntUser;
                                    costRepActCost.CREATEDDATE = DateTime.Now;
                                }
                                else {
                                    costRepActCost.UPDATEDBY = ntUser;
                                    costRepActCost.UPDATEDDATE = DateTime.Now;
                                }

                                costRepActCost.NETWORK = string.IsNullOrEmpty(row["Object"].ToString().Trim()) ? null : row["Object"].ToString().Trim();
                                costRepActCost.OBJECTTYPE = string.IsNullOrEmpty(row["Object type"].ToString().Trim()) ? null : row["Object type"].ToString().Trim();

                                float AMOUNT;
                                if (float.TryParse(row["Value in Obj. Crcy"].ToString().Trim(), out AMOUNT))
                                {
                                    costRepActCost.AMOUNT = AMOUNT;
                                }
                                else if (!string.IsNullOrEmpty(row["Value in Obj. Crcy"].ToString().Trim()))
                                {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@wrong number format value at excel Value in Obj. Crcy column");
                                    continue;
                                }

                                ++succeedCount;

                                if (costRepActCost.OID > 0)
                                {
                                    //db.Entry(costRepActCost).State = EntityState.Modified;
                                }
                                else
                                {
                                    //db.Entry(costRepActCost).State = EntityState.Added;
                                    iCostReportActualCostAdd.Add(costRepActCost);
                                }
                            }

                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            fIntLoader.REMARK = "succeedCount:" + succeedCount.ToString() + "|failedCount:" + failedCount.ToString() + "|failed_detail:" + String.Join(";", failedDetail);
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;

                            db.Entry(fIntLoader).State = EntityState.Added;

                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount, failed_detail = failedDetail });
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "COMMITMENT")
                        {
                            int i = 0;
                            foreach (DataRow row in dt.Rows) {
                                ++i;
                                string projectNo = row["Project Definition"].ToString().Trim();
                                string wbsNo = row["WBS Element"].ToString().Trim();
                                int year;
                                int.TryParse(row["Fiscal Year"].ToString().Trim(), out year);
                                int month;
                                int.TryParse(row["Period"].ToString().Trim(), out month);

                                if (string.IsNullOrEmpty(projectNo) || string.IsNullOrEmpty(wbsNo) || !int.TryParse(row["Fiscal Year"].ToString().Trim(), out year) || !int.TryParse(row["Period"].ToString().Trim(), out month)) {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@null or empty value at" + fileInt.FILENAME + " column Project Definition or WBS Element or Fiscal Year or Period");
                                    continue;
                                }

                                costRepCom = iCostReportCommitmentEdit.Where(x => x.PROJECTNO == projectNo && x.WBSNO == wbsNo && x.YEAR == year && x.MONTH == month).FirstOrDefault();

                                if (costRepCom == null)
                                {
                                    costRepCom = new COSTREPORTCOMMITMENT();
                                    costRepCom.PROJECTNO = projectNo;
                                    costRepCom.WBSNO = wbsNo;
                                    costRepCom.YEAR = year;
                                    costRepCom.MONTH = month;
                                    costRepCom.CREATEDBY = ntUser;
                                    costRepCom.CREATEDDATE = DateTime.Now;
                                }
                                else {
                                    costRepCom.UPDATEDBY = ntUser;
                                    costRepCom.UPDATEDDATE = DateTime.Now;
                                }

                                costRepCom.NETWORK = string.IsNullOrEmpty(row["Object"].ToString().Trim()) ? null : row["Object"].ToString().Trim();
                                costRepCom.OBJECTTYPE = string.IsNullOrEmpty(row["Object type"].ToString().Trim()) ? null : row["Object type"].ToString().Trim();
                                float AMOUNT;
                                if (float.TryParse(row["Value in Obj. Crcy"].ToString().Trim(), out AMOUNT)) {
                                    if (AMOUNT >= 0)
                                        costRepCom.AMOUNT = AMOUNT;
                                    else {
                                        ++failedCount;
                                        failedDetail.Add("row:" + i + "@negative number value at excel Value in Value in Obj. Crcy column");
                                    }
                                }
                                else {
                                    ++failedCount;
                                    failedDetail.Add("row:" + i + "@wrong number format value at excel Value in Value in Obj. Crcy column");
                                }

                                ++succeedCount;

                                if (costRepCom.OID > 0) {
                                    //db.Entry(costRepCom).State = EntityState.Modified;                 
                                }
                                else {
                                    //db.Entry(costRepCom).State = EntityState.Added;
                                    iCostReportCommitmentAdd.Add(costRepCom);
                                }
                            }

                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            fIntLoader.REMARK = "succeedCount:" + succeedCount.ToString() + "|failedCount:" + failedCount.ToString() + "|failed_detail:" + String.Join(";", failedDetail);
                            fIntLoader.LOADERBY = ntUser;
                            fIntLoader.LOADERDATE = DateTime.Now;

                            db.Entry(fIntLoader).State = EntityState.Added;

                            result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount, failed_detail = failedDetail });
                        }
                        else continue;
                    }
                }

                string sourceFile = fileName;
                string destinationFile = fileInt.SHAREFOLDER + "backup\\" + fileInt.FILENAME;
                if (result[0].failed_count == 0)
                {
                    if (File.Exists(destinationFile))
                        File.Delete(destinationFile);
                    File.Move(sourceFile, destinationFile);
                }
            }

            db.COSTREPORTELEMENTs.AddRange(iCostReportElementAdd);
            db.COSTREPORTNETWORKs.AddRange(iCostReportNetworkAdd);
            db.COSTREPORTBUDGETs.AddRange(iCostReportBudgetAdd);
            db.COSTREPORTACTUALCOSTs.AddRange(iCostReportActualCostAdd);
            db.COSTREPORTCOMMITMENTs.AddRange(iCostReportCommitmentAdd);

            db.SaveChanges();

            string sql = @"UPDATE COSTREPORTNETWORK
                            SET COSTREPORTNETWORK.COSTREPORTELEMENTID = CE.OID
                            FROM COSTREPORTNETWORK CN
                            INNER JOIN COSTREPORTELEMENT CE
                            ON CN.WBSNO = CE.WBSNO and CN.PROJECTNO = CE.PROJECTNO and CN.COSTREPORTELEMENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            sql = @"UPDATE COSTREPORTBUDGET
                            SET COSTREPORTBUDGET.COSTREPORTELEMENTID = CE.OID
                            FROM COSTREPORTBUDGET CB
                            INNER JOIN COSTREPORTELEMENT CE
                            ON CB.WBSNO = CE.WBSNO and CB.PROJECTNO = CE.PROJECTNO and CB.COSTREPORTELEMENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            sql = @"UPDATE COSTREPORTACTUALCOST
                            SET COSTREPORTACTUALCOST.COSTREPORTELEMENTID = CE.OID
                            FROM COSTREPORTACTUALCOST CA
                            INNER JOIN COSTREPORTELEMENT CE
                            ON CA.WBSNO = CE.WBSNO and CA.PROJECTNO = CE.PROJECTNO and CA.COSTREPORTELEMENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            sql = @"UPDATE COSTREPORTCOMMITMENT
                            SET COSTREPORTCOMMITMENT.COSTREPORTELEMENTID = CE.OID
                            FROM COSTREPORTCOMMITMENT CC
                            INNER JOIN COSTREPORTELEMENT CE
                            ON CC.WBSNO = CE.WBSNO and CC.PROJECTNO = CE.PROJECTNO and CC.COSTREPORTELEMENTID is null";
            db.Database.ExecuteSqlCommand(sql);

            //var cosRepNets = db.COSTREPORTNETWORKs.Where(x => !x.COSTREPORTELEMENTID.HasValue);
            //foreach (COSTREPORTNETWORK cosRepNet in cosRepNets)
            //{
            //    COSTREPORTELEMENT cosRepEle = db.COSTREPORTELEMENTs.FirstOrDefault(x => x.WBSNO == cosRepNet.WBSNO && x.PROJECTNO == cosRepNet.PROJECTNO);
            //    if (cosRepEle != null)
            //    {
            //        cosRepNet.COSTREPORTELEMENTID = cosRepEle.OID;
            //        db.Entry(cosRepNet).State = EntityState.Modified;
            //    }
            //}

            //var cosRepBuds = db.COSTREPORTBUDGETs.Where(x => !x.COSTREPORTELEMENTID.HasValue);
            //foreach (COSTREPORTBUDGET cosRepBud in cosRepBuds)
            //{
            //    COSTREPORTELEMENT cosRepEle = db.COSTREPORTELEMENTs.FirstOrDefault(x => x.WBSNO == cosRepBud.WBSNO && x.PROJECTNO == cosRepBud.PROJECTNO);
            //    if (cosRepEle != null)
            //    {
            //        cosRepBud.COSTREPORTELEMENTID = cosRepEle.OID;
            //        db.Entry(cosRepBud).State = EntityState.Modified;
            //    }
            //}

            //var cosRepActs = db.COSTREPORTACTUALCOSTs.Where(x => !x.COSTREPORTELEMENTID.HasValue);
            //foreach (COSTREPORTACTUALCOST cosRepAct in cosRepActs)
            //{
            //    COSTREPORTELEMENT cosRepEle = db.COSTREPORTELEMENTs.FirstOrDefault(x => x.WBSNO == cosRepAct.WBSNO && x.PROJECTNO == cosRepAct.PROJECTNO);
            //    if (cosRepEle != null)
            //    {
            //        cosRepAct.COSTREPORTELEMENTID = cosRepEle.OID;
            //        db.Entry(cosRepAct).State = EntityState.Modified;
            //    }
            //}

            //var cosRepComs = db.COSTREPORTCOMMITMENTs.Where(x => !x.COSTREPORTELEMENTID.HasValue);
            //foreach (COSTREPORTCOMMITMENT cosRepCom in cosRepComs)
            //{
            //    COSTREPORTELEMENT cosRepEle = db.COSTREPORTELEMENTs.FirstOrDefault(x => x.WBSNO == cosRepCom.WBSNO && x.PROJECTNO == cosRepCom.PROJECTNO);
            //    if (cosRepEle != null)
            //    {
            //        cosRepCom.COSTREPORTELEMENTID = cosRepEle.OID;
            //        db.Entry(cosRepCom).State = EntityState.Modified;
            //    }
            //}

            db.SaveChanges();

            return result;
        }

        //check all files do exist & check all required columns do exist per file
        //then do process

        [HttpGet]
        [ActionName("ProcessCostLoader")]
        public object ProcessCostLoader(string param)
        {
            var ntUser = GetCurrentNTUserId();

            List<dynamic> result = new List<dynamic>();

            var listFileIntegration = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE.Trim().ToUpper() == "COST").ToList();                     

            string localhost = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.InstanceName; //instance name sqlserver
            string idUser = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idUser; //User sqlserver
            string idCatalog = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idCatalog; //catalog sqlserver
            string idPassword = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idPassword; //password sqlserver
            string destConnString = @"Password=" + idPassword + "; Persist Security Info=True; User ID=" + idUser + "; Initial Catalog=" + idCatalog + "; Data Source=" + localhost;

            string excelConnStr03 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0 Xml;HDR=YES;IMEX=1\"";
            string excelConnStr07 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";

            List<objFiles> lFilesMoved = new List<objFiles>();
            int idx = 0;

            bool isDoLoad = true;

            var luLoaderCost = db.LOOKUPs.Where(l => l.TYPE == constants.loaderCost).ToList().Select(s => s.VALUE.ToUpper()); 

            //VALIDATION
            //check all files do exist
            foreach (FILEINTEGRATION fileInt in listFileIntegration)
            {
                FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                string fileName = fileInt.SHAREFOLDER + fileInt.FILENAME;
                //string backupFolder = fileInt.SHAREFOLDER + "backup\\";
                //if (!Directory.Exists(backupFolder))
                //    Directory.CreateDirectory(backupFolder);

                //lFilesMoved.Add(new objFiles { id = idx + 1, sourceFile = fileInt.SHAREFOLDER + fileInt.FILENAME, destinationFile = backupFolder + fileInt.FILENAME });
                //++idx;

                if (File.Exists(fileInt.SHAREFOLDER + fileInt.FILENAME))
                {
                    string connStr = "";
                    //isDoLoad = isDoLoad && true;
                    if (Path.GetExtension(fileName).ToLower() == ".xlsx" || Path.GetExtension(fileName).ToLower() == "xlsx")
                        connStr = string.Format(excelConnStr07, fileName);
                    else if (Path.GetExtension(fileName).ToLower() == ".xls" || Path.GetExtension(fileName).ToLower() == "xls")
                        connStr = string.Format(excelConnStr03, fileName);
                    else
                    {
                        result.Add(new
                        {
                            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                            success_count = "",
                            failed_count = "",
                            error_rows = "",
                            error_column_excel = "",
                            error_column_lookup = "",
                            error_operation = "File extension is not valid."
                        });

                        // make remark about the file extension is not valid
                        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        fIntLoader.FILENAME = fileInt.FILENAME;
                        fIntLoader.LOADERDATE = DateTime.Now;
                        fIntLoader.LOADERBY = GetCurrentNTUserId();
                        fIntLoader.REMARK = "File extension is not valid.";
                        db.Entry(fIntLoader).State = EntityState.Added;
                        db.SaveChanges();

                        isDoLoad = isDoLoad && false;
                        continue;
                    }

                    List<string> columnNames = new List<string>();

                    using (OleDbConnection conn = new OleDbConnection())
                    {
                        conn.Close();
                        conn.Dispose();

                        conn.ConnectionString = connStr;
                        if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                        {
                            conn.Open();
                        }

                        if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                        {
                            List<string> listColsNotFoundLookup = new List<string>();
                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                            columnNames = new List<string> {
                                "Project definition", "Level", "WBS element", "Name"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                isDoLoad = isDoLoad && false;

                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });

                                continue;
                            }

                            OleDbCommand com = new OleDbCommand(@"Select [Project definition], [Level], [WBS element], [Name] from [Sheet1$]", conn);
                            try
                            {
                                OleDbDataReader reader = com.ExecuteReader();
                                if (!reader.HasRows)
                                {
                                    result.Add(new
                                    {
                                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = "This file has no data."
                                    });
                                    // make remark about the file extension is not valid
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                                    fIntLoader.REMARK = "This file has no data.";
                                    db.Entry(fIntLoader).State = EntityState.Added;
                                    db.SaveChanges();
                                    
                                    continue;
                                }
                                reader.Close();
                            }
                            catch (OleDbException ex)
                            {
                                isDoLoad = isDoLoad && false;

                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = "",
                                    error_operation = ex.Message.ToString()
                                });

                                // make remark about the file extension is not valid
                                fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                fIntLoader.FILENAME = fileInt.FILENAME;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.LOADERBY = GetCurrentNTUserId();
                                fIntLoader.REMARK = ex.Message.ToString();
                                db.Entry(fIntLoader).State = EntityState.Added;
                                db.SaveChanges();
                                
                                continue;
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "NETWORK")
                        {
                            List<string> listColsNotFoundLookup = new List<string>();
                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                            columnNames = new List<string> {
                                "Project definition", "WBS element", "Network", "Text"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                isDoLoad = isDoLoad && false;

                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });

                                continue;
                            }

                            OleDbCommand com = new OleDbCommand(@"Select [Project definition], [WBS element], [Network], [Text] from [Sheet1$]", conn);
                            try
                            {
                                OleDbDataReader reader = com.ExecuteReader();
                                if (!reader.HasRows)
                                {
                                    result.Add(new
                                    {
                                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = "This file has no data."
                                    });
                                    // make remark about the file extension is not valid
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                                    fIntLoader.REMARK = "This file has no data.";
                                    db.Entry(fIntLoader).State = EntityState.Added;
                                    db.SaveChanges();
                                    
                                    continue;
                                }
                                reader.Close();
                            }
                            catch (OleDbException ex)
                            {
                                isDoLoad = isDoLoad && false;

                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = "",
                                    error_operation = ex.Message.ToString()
                                });

                                // make remark about the file extension is not valid
                                fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                fIntLoader.FILENAME = fileInt.FILENAME;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.LOADERBY = GetCurrentNTUserId();
                                fIntLoader.REMARK = ex.Message.ToString();
                                db.Entry(fIntLoader).State = EntityState.Added;
                                db.SaveChanges();
                                
                                continue;
                            }
                        }
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "BUDGET")
                        {
                            List<string> listColsNotFoundLookup = new List<string>();
                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                            columnNames = new List<string> {
                                "Project Definition", "WBS Element", "Fiscal Year", "Value TranCurr"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                isDoLoad = isDoLoad && false;

                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });

                                continue;
                            }

                            OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Fiscal Year], [Value TranCurr] from [Sheet1$]", conn);
                            try
                            {
                                OleDbDataReader reader = com.ExecuteReader();
                                if (!reader.HasRows)
                                {
                                    result.Add(new
                                    {
                                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = "This file has no data."
                                    });
                                    // make remark about the file extension is not valid
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                                    fIntLoader.REMARK = "This file has no data.";
                                    db.Entry(fIntLoader).State = EntityState.Added;
                                    db.SaveChanges();
                                    
                                    continue;
                                }
                                reader.Close();
                            }
                            catch (OleDbException ex)
                            {
                                isDoLoad = isDoLoad && false;

                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = "",
                                    error_operation = ex.Message.ToString()
                                });

                                // make remark about the file extension is not valid
                                fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                fIntLoader.FILENAME = fileInt.FILENAME;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.LOADERBY = GetCurrentNTUserId();
                                fIntLoader.REMARK = ex.Message.ToString();
                                db.Entry(fIntLoader).State = EntityState.Added;
                                db.SaveChanges();
                                
                                continue;
                            }
                        }
//                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUALCIDB")
                        else if (luLoaderCost.Contains(fileInt.PURPOSEFILE.Trim().ToUpper()))
                        {
                            List<string> listColsNotFoundLookup = new List<string>();
                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + db.LOOKUPs.Where(c => c.TYPE == constants.loaderCost && c.VALUE == fileInt.PURPOSEFILE).FirstOrDefault().TEXT).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                            columnNames = new List<string> {
                                "Project Definition", "WBS Element", "Object", "Object type", "Fiscal Year", "Period", "Value in Obj. Crcy"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                isDoLoad = isDoLoad && false;

                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });

                                continue;
                            }

                            OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                            try
                            {
                                OleDbDataReader reader = com.ExecuteReader();
                                if (!reader.HasRows)
                                {
                                    result.Add(new
                                    {
                                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = "This file has no data."
                                    });
                                    // make remark about the file extension is not valid
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                                    fIntLoader.REMARK = "This file has no data.";
                                    db.Entry(fIntLoader).State = EntityState.Added;
                                    db.SaveChanges();
                                    
                                    continue;
                                }
                                reader.Close();
                            }
                            catch (OleDbException ex)
                            {
                                isDoLoad = isDoLoad && false;

                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = "",
                                    error_operation = ex.Message.ToString()
                                });

                                // make remark about the file extension is not valid
                                fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                fIntLoader.FILENAME = fileInt.FILENAME;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.LOADERBY = GetCurrentNTUserId();
                                fIntLoader.REMARK = ex.Message.ToString();
                                db.Entry(fIntLoader).State = EntityState.Added;
                                db.SaveChanges();
                                
                                continue;
                            }
                        }
                        //else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "ACTUALSIDB")
                        //{
                        //    List<string> listColsNotFoundLookup = new List<string>();
                        //    List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                        //    columnNames = new List<string> {
                        //        "Project Definition", "WBS Element", "Object", "Object type", "Fiscal Year", "Period", "Value in Obj. Crcy"
                        //    };

                        //    var colsNotFoundLookup = columnNames.Except(columnNamesLookup);

                        //    foreach (string x in colsNotFoundLookup)
                        //    {
                        //        listColsNotFoundLookup.Add(x);
                        //    }

                        //    if (listColsNotFoundLookup.Count > 0)
                        //    {
                        //        isDoLoad = isDoLoad && false;

                        //        string listColsStr = string.Join(", ", listColsNotFoundLookup);
                        //        result.Add(new
                        //        {
                        //            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        //            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        //            success_count = "",
                        //            failed_count = "",
                        //            error_rows = "",
                        //            error_column_excel = "",
                        //            error_column_lookup = listColsStr,
                        //            error_operation = ""
                        //        });

                        //        continue;
                        //    }

                        //    OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                        //    try
                        //    {
                        //        OleDbDataReader reader = com.ExecuteReader();
                        //        if (!reader.HasRows)
                        //        {
                        //            result.Add(new
                        //            {
                        //                filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        //                purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        //                success_count = "",
                        //                failed_count = "",
                        //                error_rows = "",
                        //                error_column_excel = "",
                        //                error_column_lookup = "",
                        //                error_operation = "This file has no data."
                        //            });
                        //            // make remark about the file extension is not valid
                        //            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        //            fIntLoader.FILENAME = fileInt.FILENAME;
                        //            fIntLoader.LOADERDATE = DateTime.Now;
                        //            fIntLoader.LOADERBY = GetCurrentNTUserId();
                        //            fIntLoader.REMARK = "This file has no data.";
                        //            db.Entry(fIntLoader).State = EntityState.Added;
                        //            db.SaveChanges();

                        //            continue;
                        //        }
                        //        reader.Close();
                        //    }
                        //    catch (OleDbException ex)
                        //    {
                        //        isDoLoad = isDoLoad && false;

                        //        result.Add(new
                        //        {
                        //            filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        //            purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        //            success_count = "",
                        //            failed_count = "",
                        //            error_rows = "",
                        //            error_column_excel = "",
                        //            error_column_lookup = "",
                        //            error_operation = ex.Message.ToString()
                        //        });

                        //        // make remark about the file extension is not valid
                        //        fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                        //        fIntLoader.FILENAME = fileInt.FILENAME;
                        //        fIntLoader.LOADERDATE = DateTime.Now;
                        //        fIntLoader.LOADERBY = GetCurrentNTUserId();
                        //        fIntLoader.REMARK = ex.Message.ToString();
                        //        db.Entry(fIntLoader).State = EntityState.Added;
                        //        db.SaveChanges();

                        //        continue;
                        //    }
                        //}
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "COMMITMENT")
                        {
                            List<string> listColsNotFoundLookup = new List<string>();
                            List<string> columnNamesLookup = (db.LOOKUPs.Where(y => y.TYPE == "fileIntegrationToolTipDef" && y.ISACTIVE == true && y.VALUE == fileInt.PURPOSETYPE + fileInt.PURPOSEFILE).First().TEXT).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                            columnNames = new List<string> {
                                "Project Definition", "WBS Element", "Object", "Object type", "Fiscal Year", "Period", "Value in Obj. Crcy"
                            };

                            var colsNotFoundLookup = columnNames.Except(columnNamesLookup);

                            foreach (string x in colsNotFoundLookup)
                            {
                                listColsNotFoundLookup.Add(x);
                            }

                            if (listColsNotFoundLookup.Count > 0)
                            {
                                isDoLoad = isDoLoad && false;

                                string listColsStr = string.Join(", ", listColsNotFoundLookup);
                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = listColsStr,
                                    error_operation = ""
                                });

                                continue;
                            }

                            OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                            try
                            {
                                OleDbDataReader reader = com.ExecuteReader();
                                if (!reader.HasRows)
                                {
                                    result.Add(new
                                    {
                                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = "",
                                        failed_count = "",
                                        error_rows = "",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = "This file has no data."
                                    });
                                    // make remark about the file extension is not valid
                                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                    fIntLoader.FILENAME = fileInt.FILENAME;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                                    fIntLoader.REMARK = "This file has no data.";
                                    db.Entry(fIntLoader).State = EntityState.Added;
                                    db.SaveChanges();

                                    continue;
                                }
                                reader.Close();
                            }
                            catch (OleDbException ex)
                            {
                                isDoLoad = isDoLoad && false;

                                result.Add(new
                                {
                                    filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                    purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                    success_count = "",
                                    failed_count = "",
                                    error_rows = "",
                                    error_column_excel = "",
                                    error_column_lookup = "",
                                    error_operation = ex.Message.ToString()
                                });

                                // make remark about the file extension is not valid
                                fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                                fIntLoader.FILENAME = fileInt.FILENAME;
                                fIntLoader.LOADERDATE = DateTime.Now;
                                fIntLoader.LOADERBY = GetCurrentNTUserId();
                                fIntLoader.REMARK = ex.Message.ToString();
                                db.Entry(fIntLoader).State = EntityState.Added;
                                db.SaveChanges();

                                continue;
                            }
                        }
                 
                        //else if (fileInt.PURPOSEFILE.Trim().ToUpper() != "HEADER" && fileInt.PURPOSEFILE.Trim().ToUpper() != "NETWORK" &&
                        //    fileInt.PURPOSEFILE.Trim().ToUpper() != "BUDGET" && fileInt.PURPOSEFILE.Trim().ToUpper() != "ACTUALCIDB" &&
                        //    fileInt.PURPOSEFILE.Trim().ToUpper() != "ACTUALSIDB" && fileInt.PURPOSEFILE.Trim().ToUpper() != "COMMITMENT")
                        else if (fileInt.PURPOSEFILE.Trim().ToUpper() != "HEADER" && fileInt.PURPOSEFILE.Trim().ToUpper() != "NETWORK" &&
                            fileInt.PURPOSEFILE.Trim().ToUpper() != "BUDGET" && luLoaderCost.Contains(fileInt.PURPOSEFILE.Trim().ToUpper())
                            && fileInt.PURPOSEFILE.Trim().ToUpper() != "COMMITMENT")
                        {
                            isDoLoad = isDoLoad && false;

                            result.Add(new
                            {
                                filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                                purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                success_count = "",
                                failed_count = "",
                                error_rows = "",
                                error_column_excel = "",
                                error_column_lookup = "",
                                error_operation = "Wrong Purpose file name."
                            });

                            // make remark about the file extension is not valid
                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            fIntLoader.LOADERDATE = DateTime.Now;
                            fIntLoader.LOADERBY = GetCurrentNTUserId();
                            fIntLoader.REMARK = "Wrong Purpose file name.";
                            db.Entry(fIntLoader).State = EntityState.Added;
                            db.SaveChanges();

                            continue;
                        }
                        //conn.Close();
                    }
                }
                else
                {
                    isDoLoad = isDoLoad && false;

                    result.Add(new
                    {
                        filename = fileInt.SHAREFOLDER + fileInt.FILENAME,
                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                        success_count = "",
                        failed_count = "",
                        error_rows = "",
                        error_column_excel = "",
                        error_column_lookup = "",
                        error_operation = "File does not exist."
                    });

                    // make remark about the file does not exist
                    fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                    fIntLoader.FILENAME = fileInt.FILENAME;
                    fIntLoader.LOADERDATE = DateTime.Now;
                    fIntLoader.LOADERBY = GetCurrentNTUserId();
                    fIntLoader.REMARK = "File does not exist.";
                    db.Entry(fIntLoader).State = EntityState.Added;
                    db.SaveChanges();

                    continue;
                }
            }


            //PROCESS
            if (!isDoLoad)
                return result;
            else
            {
                try
                {
                    using (OleDbConnection conn = new OleDbConnection())
                    {
                        foreach (FILEINTEGRATION fileInt in listFileIntegration)
                        {
                            FILEINTEGRATIONLOADER fIntLoader = new FILEINTEGRATIONLOADER();
                            fIntLoader.FILEINTEGRATIONID = fileInt.OID;
                            fIntLoader.FILENAME = fileInt.FILENAME;
                            db.Entry(fIntLoader).State = EntityState.Added;
                            db.SaveChanges();

                            string connStr = "";
                            string fileName = fileInt.SHAREFOLDER + fileInt.FILENAME;

                            //error detail
                            int succeedCount = 0, failedCount = 0;
                            StringBuilder errorRows = new StringBuilder();
                            List<int> idxDelRows = new List<int>();

                            if (Path.GetExtension(fileName).ToLower() == ".xlsx" || Path.GetExtension(fileName).ToLower() == "xlsx")
                                connStr = string.Format(excelConnStr07, fileName);
                            else if (Path.GetExtension(fileName).ToLower() == ".xls" || Path.GetExtension(fileName).ToLower() == "xls")
                                connStr = string.Format(excelConnStr03, fileName);

                            conn.ConnectionString = connStr;

                            if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                            {
                                conn.Open();
                            }

                            if (fileInt.PURPOSEFILE.Trim().ToUpper() == "HEADER")
                            {
                                OleDbCommand com = new OleDbCommand(@"Select [Project definition], [Level], [WBS element], [Name] from [Sheet1$]", conn);

                                OleDbDataReader reader = com.ExecuteReader();

                                DataTable table = new DataTable();
                                var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                col1.DefaultValue = fIntLoader.OID;
                                var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                col2.DefaultValue = DateTime.Now;
                                var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                col3.DefaultValue = ntUser;
                                table.AcceptChanges();
                                table.Load(reader);

                                int i = 0;
                                foreach (DataRow row in table.Rows)
                                {
                                    i++;

                                    //primary keys ProjectNo, WBSLevel, WBSNo

                                    // ProjectNo
                                    if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // WBSLevel
                                    int WBSLEVEL;
                                    if (int.TryParse(row["Level"].ToString().Trim(), out WBSLEVEL) || string.IsNullOrWhiteSpace(row["Level"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Level"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // WBSNo
                                    if (string.IsNullOrWhiteSpace(row["WBS element"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                    {
                                        row["Project definition"] = null;
                                        table.AcceptChanges();
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Name"].ToString()))
                                    {
                                        row["Name"] = null;
                                        table.AcceptChanges();
                                    }

                                    ++succeedCount;
                                }

                                for (int j = 0; j < idxDelRows.Count; j++)
                                {
                                    table.Rows[idxDelRows[j] - 1].Delete();
                                }
                                table.AcceptChanges();

                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                {
                                    bulkCopy.DestinationTableName = "dbo.COSTREPORTELEMENTTEMP";

                                    bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                    bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                    bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                    bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                    bulkCopy.ColumnMappings.Add("Level", "WBSLEVEL");
                                    bulkCopy.ColumnMappings.Add("WBS element", "WBSNO");
                                    bulkCopy.ColumnMappings.Add("Name", "DESCRIPTION");

                                    bulkCopy.WriteToServer(table);

                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    db.Entry(fIntLoader).State = EntityState.Modified;
                                    db.SaveChanges();

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });

                                    bulkCopy.Close();
                                    conn.Close();
                                    conn.Dispose();
                                }
                                reader.Close();
                                db.MOVECOSTREPORTELEMENT(fIntLoader.OID);
                            }
                            else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "NETWORK")
                            {
                                OleDbCommand com = new OleDbCommand(@"Select [Project definition], [WBS element], [Network], [Text] from [Sheet1$]", conn);
                                OleDbDataReader reader = com.ExecuteReader();

                                DataTable table = new DataTable();
                                var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                col1.DefaultValue = fIntLoader.OID;
                                var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                col2.DefaultValue = DateTime.Now;
                                var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                col3.DefaultValue = ntUser;
                                table.AcceptChanges();
                                table.Load(reader);

                                int i = 0;
                                foreach (DataRow row in table.Rows)
                                {
                                    i++;

                                    // primary keys ProjectNo, WBSNo

                                    // ProjectNo
                                    if (string.IsNullOrWhiteSpace(row["Project definition"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // WbsNo
                                    if (string.IsNullOrWhiteSpace(row["WBS element"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Network"].ToString()))
                                    {
                                        row["Network"] = null;
                                        table.AcceptChanges();
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Text"].ToString()))
                                    {
                                        row["Text"] = null;
                                        table.AcceptChanges();
                                    }

                                    ++succeedCount;
                                }

                                for (int j = 0; j < idxDelRows.Count; j++)
                                {
                                    table.Rows[idxDelRows[j] - 1].Delete();
                                }
                                table.AcceptChanges();

                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                {
                                    bulkCopy.DestinationTableName = "dbo.COSTREPORTNETWORKTEMP";

                                    bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                    bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                    bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                    bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                    bulkCopy.ColumnMappings.Add("WBS element", "WBSNO");
                                    bulkCopy.ColumnMappings.Add("Network", "NETWORK");
                                    bulkCopy.ColumnMappings.Add("Text", "DESCRIPTION");

                                    bulkCopy.WriteToServer(table);

                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    db.Entry(fIntLoader).State = EntityState.Modified;
                                    db.SaveChanges();

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });

                                    bulkCopy.Close();
                                    conn.Close();
                                    conn.Dispose();
                                }
                                reader.Close();
                                db.MOVECOSTREPORTNETWORK(fIntLoader.OID);
                            }
                            else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "BUDGET")
                            {
                                OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Fiscal Year], [Value TranCurr] from [Sheet1$]", conn);
                                OleDbDataReader reader = com.ExecuteReader();
                                DataTable table = new DataTable();
                                var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                col1.DefaultValue = fIntLoader.OID;
                                var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                col2.DefaultValue = DateTime.Now;
                                var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                col3.DefaultValue = ntUser;
                                table.AcceptChanges();
                                table.Load(reader);

                                int i = 0;
                                foreach (DataRow row in table.Rows)
                                {
                                    i++;

                                    // Primary keys ProjectNo, WBSNo, YEAR

                                    // ProjectNo
                                    if (string.IsNullOrWhiteSpace(row["Project Definition"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // WbsNo
                                    if (string.IsNullOrWhiteSpace(row["WBS Element"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // Year
                                    int YEAR;
                                    if (int.TryParse(row["Fiscal Year"].ToString().Trim(), out YEAR) || string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        if (YEAR <= 0)
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }


                                    float AMOUNT;
                                    if (float.TryParse(row["Value TranCurr"].ToString().Trim(), out AMOUNT) || string.IsNullOrWhiteSpace(row["Value TranCurr"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Value TranCurr"].ToString()))
                                        {
                                            row["Value TranCurr"] = null;
                                            table.AcceptChanges();
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    ++succeedCount;
                                }

                                for (int j = 0; j < idxDelRows.Count; j++)
                                {
                                    table.Rows[idxDelRows[j] - 1].Delete();
                                }
                                table.AcceptChanges();
                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                {
                                    bulkCopy.DestinationTableName = "dbo.COSTREPORTBUDGETTEMP";

                                    bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                    bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                    bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                    bulkCopy.ColumnMappings.Add("Project definition", "PROJECTNO");
                                    bulkCopy.ColumnMappings.Add("WBS Element", "WBSNO");
                                    bulkCopy.ColumnMappings.Add("Fiscal Year", "YEAR");
                                    bulkCopy.ColumnMappings.Add("Value TranCurr", "AMOUNT");

                                    bulkCopy.WriteToServer(table);

                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    db.Entry(fIntLoader).State = EntityState.Modified;
                                    db.SaveChanges();

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });

                                    bulkCopy.Close();
                                    conn.Close();
                                    conn.Dispose();
                                }
                                reader.Close();
                                db.MOVECOSTREPORTBUDGET(fIntLoader.OID);
                            }
                            else if (luLoaderCost.Contains(fileInt.PURPOSEFILE.Trim().ToUpper()))
                            {
                                OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                                OleDbDataReader reader = com.ExecuteReader();

                                //testing
                                //com.CommandTimeout = 120;


                                DataTable table = new DataTable();
                                var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                col1.DefaultValue = fIntLoader.OID;
                                var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                col2.DefaultValue = DateTime.Now;
                                var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                col3.DefaultValue = ntUser;
                                table.AcceptChanges();
                                table.Load(reader);

                                int i = 0;
                                foreach (DataRow row in table.Rows)
                                {
                                    i++;

                                    // Primary keys ProjectNo, WBSNo, YEAR, Month

                                    // PrjectNo
                                    if (string.IsNullOrWhiteSpace(row["Project Definition"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // WbsNo
                                    if (string.IsNullOrWhiteSpace(row["WBS Element"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // Year
                                    int year;
                                    if (int.TryParse(row["Fiscal Year"].ToString().Trim(), out year) || string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        if (year <= 0)
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // Month
                                    int month;
                                    if (int.TryParse(row["Period"].ToString().Trim(), out month) || string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        if (year <= 0)
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    float AMOUNT;
                                    if (float.TryParse(row["Value in Obj# Crcy"].ToString().Trim(), out AMOUNT) || string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                        {
                                            row["Value in Obj# Crcy"] = null;
                                            table.AcceptChanges();
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Object"].ToString()))
                                    {
                                        row["Object"] = null;
                                        table.AcceptChanges();
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Object type"].ToString()))
                                    {
                                        row["Object type"] = null;
                                        table.AcceptChanges();
                                    }

                                    ++succeedCount;

                                }

                                for (int j = 0; j < idxDelRows.Count; j++)
                                {
                                    table.Rows[idxDelRows[j] - 1].Delete();
                                }
                                table.AcceptChanges();

                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                {
                                    bulkCopy.DestinationTableName = "dbo.COSTREPORTACTUALCOSTTEMP";

                                    // Write from the source to the destination
                                    bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                    bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                    bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                    bulkCopy.ColumnMappings.Add("Project Definition", "PROJECTNO");
                                    bulkCopy.ColumnMappings.Add("WBS Element", "WBSNO");
                                    bulkCopy.ColumnMappings.Add("Object", "NETWORK");
                                    bulkCopy.ColumnMappings.Add("Object type", "OBJECTTYPE");
                                    bulkCopy.ColumnMappings.Add("Fiscal Year", "YEAR");
                                    bulkCopy.ColumnMappings.Add("Period", "MONTH");
                                    bulkCopy.ColumnMappings.Add("Value in Obj# Crcy", "AMOUNT");

                                    //bulkCopy.BulkCopyTimeout = 2000;
                                    //bulkCopy.BatchSize = 50000;


                                    bulkCopy.WriteToServer(table);

                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    db.Entry(fIntLoader).State = EntityState.Modified;
                                    db.SaveChanges();

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });

                                    bulkCopy.Close();
                                    conn.Close();
                                    conn.Dispose();
                                }
                                reader.Close();
                                db.MOVECOSTREPORTACTUALCOST(fIntLoader.OID);
                            }
                            else if (fileInt.PURPOSEFILE.Trim().ToUpper() == "COMMITMENT")
                            {
                                OleDbCommand com = new OleDbCommand(@"Select [Project Definition], [WBS Element], [Object], [Object type], [Fiscal Year], [Period], [Value in Obj# Crcy] from [Sheet1$]", conn);
                                OleDbDataReader reader = com.ExecuteReader();

                                DataTable table = new DataTable();
                                var col1 = table.Columns.Add("FILEINTEGRATIONLOADERRID", typeof(Int32));
                                col1.DefaultValue = fIntLoader.OID;
                                var col2 = table.Columns.Add("CREATEDDATE", typeof(DateTime));
                                col2.DefaultValue = DateTime.Now;
                                var col3 = table.Columns.Add("CREATEDBY", typeof(string));
                                col3.DefaultValue = ntUser;
                                table.AcceptChanges();
                                table.Load(reader);

                                int i = 0;
                                foreach (DataRow row in table.Rows)
                                {
                                    i++;

                                    // Primary keys ProjectNo, WBSNo, YEAR, Month

                                    // ProjectNo
                                    if (string.IsNullOrWhiteSpace(row["Project Definition"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // WbsNo
                                    if (string.IsNullOrWhiteSpace(row["WBS Element"].ToString()))
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    //year
                                    int year;
                                    if (int.TryParse(row["Fiscal Year"].ToString().Trim(), out year) || string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Fiscal Year"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        if (year <= 0)
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    // Month     
                                    int month;
                                    if (int.TryParse(row["Period"].ToString().Trim(), out month) || string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Period"].ToString()))
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }

                                        if (month <= 0)
                                        {
                                            ++failedCount;
                                            errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                            idxDelRows.Add(i);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    float amount;
                                    if (float.TryParse(row["Value in Obj# Crcy"].ToString().Trim(), out amount) || string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                    {
                                        if (string.IsNullOrWhiteSpace(row["Value in Obj# Crcy"].ToString()))
                                        {
                                            row["Value in Obj# Crcy"] = null;
                                            table.AcceptChanges();
                                        }
                                    }
                                    else
                                    {
                                        ++failedCount;
                                        errorRows.Append(errorRows.Length > 0 ? (", " + i.ToString()) : i.ToString());
                                        idxDelRows.Add(i);
                                        continue;
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Object"].ToString()))
                                    {
                                        row["Object"] = null;
                                        table.AcceptChanges();
                                    }

                                    if (string.IsNullOrWhiteSpace(row["Object type"].ToString()))
                                    {
                                        row["Object type"] = null;
                                        table.AcceptChanges();
                                    }
                                    ++succeedCount;
                                }

                                for (int j = 0; j < idxDelRows.Count; j++)
                                {
                                    table.Rows[idxDelRows[j] - 1].Delete();
                                }
                                table.AcceptChanges();
                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                                {
                                    bulkCopy.DestinationTableName = "dbo.COSTREPORTCOMMITMENTTEMP";

                                    bulkCopy.ColumnMappings.Add("FILEINTEGRATIONLOADERRID", "FILEINTEGRATIONLOADERID");
                                    bulkCopy.ColumnMappings.Add("CREATEDDATE", "CREATEDDATE");
                                    bulkCopy.ColumnMappings.Add("CREATEDBY", "CREATEDBY");
                                    bulkCopy.ColumnMappings.Add("Project Definition", "PROJECTNO");
                                    bulkCopy.ColumnMappings.Add("WBS Element", "WBSNO");
                                    bulkCopy.ColumnMappings.Add("Object", "NETWORK");
                                    bulkCopy.ColumnMappings.Add("Object type", "OBJECTTYPE");
                                    bulkCopy.ColumnMappings.Add("Fiscal Year", "YEAR");
                                    bulkCopy.ColumnMappings.Add("Period", "MONTH");
                                    bulkCopy.ColumnMappings.Add("Value in Obj# Crcy", "AMOUNT");

                                    bulkCopy.WriteToServer(table);

                                    fIntLoader.LOADERBY = ntUser;
                                    fIntLoader.LOADERDATE = DateTime.Now;
                                    fIntLoader.REMARK = "success_count : " + succeedCount + ", failed_count : " + failedCount;
                                    db.Entry(fIntLoader).State = EntityState.Modified;
                                    db.SaveChanges();

                                    //result.Add(new { filename = fileName, purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE, success_count = succeedCount.ToString(), failed_count = failedCount.ToString(), failed_detail = errorRows.Length > 0 ? "error rows: " + errorRows : "-" });
                                    result.Add(new
                                    {
                                        filename = fileName,
                                        purpose = fileInt.PURPOSETYPE + " - " + fileInt.PURPOSEFILE,
                                        success_count = succeedCount.ToString(),
                                        failed_count = failedCount.ToString(),
                                        error_rows = errorRows.Length > 0 ? errorRows.ToString() : "-",
                                        error_column_excel = "",
                                        error_column_lookup = "",
                                        error_operation = ""
                                    });

                                    bulkCopy.Close();
                                    conn.Close();
                                    conn.Dispose();
                                }
                                reader.Close();
                                db.MOVECOSTREPORTCOMMITMENT(fIntLoader.OID);
                            }
                            string backupFolder = fileInt.SHAREFOLDER + "backup\\";
                            if (!Directory.Exists(backupFolder))
                                Directory.CreateDirectory(backupFolder);
                            lFilesMoved.Add(new objFiles { id = idx + 1, sourceFile = fileInt.SHAREFOLDER + fileInt.FILENAME, destinationFile = backupFolder + fileInt.FILENAME });
                            idx++;
                        } // end loop fil int
                        foreach (objFiles x in lFilesMoved)
                        {
                            if (File.Exists(x.destinationFile)) File.Delete(x.destinationFile);
                            File.Move(x.sourceFile, x.destinationFile);
                        }
                    }
                    db.SP_BUDGET_APPROVAL();
                }
                catch (Exception ex)
                {
                    //string eex = ex.ToString();
                    //result.Add(new
                    //{
                    //    filename = "",
                    //    purpose = "",
                    //    success_count = "",
                    //    failed_count = "",
                    //    error_rows = "",
                    //    error_column_excel = "",
                    //    error_column_lookup = "",
                    //    error_operation = ex.Message.ToString()
                    //});
                    //return result;
                    throw ex;
                }
            }
                       
            return result;
        }


        [HttpGet]
        [ActionName("tes")]
        public object tes(string param) {
            CONSTRUCTIONSERVICE cs = new CONSTRUCTIONSERVICE();
            return cs;
        }
        
        [HttpPost]
        [ActionName("UploadLoaderCs")]
        public System.Threading.Tasks.Task<HttpResponseMessage> UploadLoaderCs(string param)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();

            int count = 0;
            string listFailed = null;
            string listFailedDetail = null;

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    foreach (var stream in provider.Contents)
                    {
                        // Process The File
                        var type = Path.GetExtension(stream.Headers.ContentDisposition.FileName.Replace("\"", ""));

                        var fs = new MemoryStream(stream.ReadAsByteArrayAsync().Result);
                        //var returnVal = ProcessLoaderCs(fs, type, param).Split('|');
                        //count += int.Parse(returnVal[0]);

                        //if (!string.IsNullOrEmpty(listFailed))
                        //{
                        //    listFailed += ", " + returnVal[1];
                        //}
                        //else
                        //{
                        //    listFailed += returnVal[1];
                        //}

                        //if (!string.IsNullOrEmpty(listFailedDetail))
                        //{
                        //    listFailedDetail += "; " + returnVal[2];
                        //}
                        //else
                        //{
                        //    listFailedDetail += returnVal[2];
                        //}
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
                var aaa = listFailed.Split(',').Count();

                return Request.CreateResponse(HttpStatusCode.OK, new { success = count, Failed = (listFailed.Split(',').Count() - 1), listFailed = listFailed, listFailedDetail = listFailedDetail });
            });

            return task;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
