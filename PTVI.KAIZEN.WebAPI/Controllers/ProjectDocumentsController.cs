﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Controllers;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.IO;
using Excel;
using PTGDI.Email;
using System.Threading.Tasks;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class usernameemail { 
        public string userId { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }

    public class ProjectDocumentsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        AccountController ac = new AccountController();

        // GET: api/ProjectDocuments
        public IQueryable<PROJECTDOCUMENT> GetPROJECTDOCUMENTs()
        {
            return db.PROJECTDOCUMENTs;
        }

        // GET: api/ProjectDocuments/5
        [ResponseType(typeof(PROJECTDOCUMENT))]
        public IHttpActionResult GetPROJECTDOCUMENT(int id)
        {
            PROJECTDOCUMENT pROJECTDOCUMENT = db.PROJECTDOCUMENTs.Find(id);
            if (pROJECTDOCUMENT == null)
            {
                return NotFound();
            }

            return Ok(pROJECTDOCUMENT);
        }

        // PUT: api/ProjectDocuments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROJECTDOCUMENT(int id, PROJECTDOCUMENT pROJECTDOCUMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTDOCUMENT.OID)
            {
                return BadRequest();
            }

            db.Entry(pROJECTDOCUMENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjectDocuments
        [ResponseType(typeof(PROJECTDOCUMENT))]
        public IHttpActionResult PostPROJECTDOCUMENT(PROJECTDOCUMENT pROJECTDOCUMENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROJECTDOCUMENTs.Add(pROJECTDOCUMENT);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pROJECTDOCUMENT.OID }, pROJECTDOCUMENT);
        }

        // DELETE: api/ProjectDocuments/5
        [ResponseType(typeof(PROJECTDOCUMENT))]
        public IHttpActionResult DeletePROJECTDOCUMENT(int id)
        {
            PROJECTDOCUMENT pROJECTDOCUMENT = db.PROJECTDOCUMENTs.Find(id);
            if (pROJECTDOCUMENT == null)
            {
                return NotFound();
            }
            //var ts = db.TIMESHEETs.Where(t => t.PROJECTDOCUMENTID)
            if (pROJECTDOCUMENT.TIMESHEETs != null) pROJECTDOCUMENT.TIMESHEETs.Clear();
            if (pROJECTDOCUMENT.PROJECTDOCUMENTREFERENCEs != null) pROJECTDOCUMENT.PROJECTDOCUMENTREFERENCEs.Clear();
            if (pROJECTDOCUMENT.CONSTRUCTIONSERVICEs != null) pROJECTDOCUMENT.CONSTRUCTIONSERVICEs.Clear();
            if (pROJECTDOCUMENT.COSTREPORTELEMENTs != null) pROJECTDOCUMENT.COSTREPORTELEMENTs.Clear();
            if (pROJECTDOCUMENT.MATERIALCOMPONENTs != null) pROJECTDOCUMENT.MATERIALCOMPONENTs.Clear();
            if (pROJECTDOCUMENT.PROJECTDOCUMENTREFERENCEs1 != null) pROJECTDOCUMENT.PROJECTDOCUMENTREFERENCEs1.Clear();
            db.Entry(pROJECTDOCUMENT).State = EntityState.Deleted;

            //db.PROJECTDOCUMENTs.Remove(pROJECTDOCUMENT);
            db.SaveChanges();

            return Ok(pROJECTDOCUMENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTDOCUMENTExists(int id)
        {
            return db.PROJECTDOCUMENTs.Count(e => e.OID == id) > 0;
        }



        [HttpGet]
        [ActionName("getEngineerPosInProject")]
        public object getEngineerPosInProject(int param, string badgeNo)
        {
            var ua =
                db.USERASSIGNMENTs.Where(f => f.PROJECTID == param && f.EMPLOYEEID == badgeNo && f.ASSIGNMENTTYPE==constants.PROJECT_ENGINEER)
                .Select(s => new
                {
                    id = s.OID,
                    pos = s.EMPLOYEEPOSITION,
                    posName = db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE == s.EMPLOYEEPOSITION).FirstOrDefault().TEXT
                }).FirstOrDefault();
            return ua;
        }

        [HttpGet]
        [ActionName("getDesignerPosInProject")]
        public object getDesignerPosInProject(int param, string badgeNo)
        {
            var ua =
                db.USERASSIGNMENTs.Where(f => f.PROJECTID == param && f.EMPLOYEEID == badgeNo && f.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)
                .Select(s => new
                {
                    id = s.OID,
                    pos = s.EMPLOYEEPOSITION,
                    posName = db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE == s.EMPLOYEEPOSITION).FirstOrDefault().TEXT
                }).FirstOrDefault();
            return ua;
        }

        [HttpGet]
        [ActionName("DesignDrawingExDefaultSpan")]
        public IEnumerable<object> DesignDrawingExDefaultSpan(int param)
        {
            var ddds = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DesignDrawingDefaultSpan).FirstOrDefault().VALUE;
            var ddeds = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DesignDrawingExDefaultSpan).FirstOrDefault().VALUE;
            var ar = new List<object>();
            ar.Add(new { ddds= ddds, ddeds= ddeds });
            return ar;
        }

        [HttpGet]
        [ActionName("PDAssignExecutorforEWP")]
        public object PDAssignExecutorforEWP(int param)
        {
            var projectDocument =
                db.PROJECTDOCUMENTs.Where(f => f.OID == param).
                Select(s => new
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    keydeliverablesId = s.KEYDELIVERABLESID,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,

                    project = new projectObject { projectManagerId = s.PROJECT.PROJECTMANAGERID, projectManagerName = s.PROJECT.PROJECTMANAGERNAME, projectManagerEmail = s.PROJECT.PROJECTMANAGEREMAIL }
                }).FirstOrDefault();
            return projectDocument;
        }

        [HttpPut]
        [ActionName("PDAssignExecutorforEWPUpdate")]
        public IHttpActionResult PDAssignExecutorforEWPUpdate(int param, projectDocumentObject projectDocumentParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (param != projectDocumentParam.id)
            {
                return BadRequest();
            }

            PROJECTDOCUMENT existPD = db.PROJECTDOCUMENTs.Find(param);
            existPD.PLANSTARTDATE = projectDocumentParam.planStartDate;
            existPD.PLANFINISHDATE = projectDocumentParam.planFinishDate;
            existPD.EXECUTORASSIGNMENT = projectDocumentParam.executorAssignment;
            existPD.UPDATEDBY = GetCurrentNTUserId();
            existPD.UPDATEDDATE = DateTime.Now;
            db.Entry(existPD).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTExists(param))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(PDAssignExecutorforEWP(existPD.OID));
        }


        [HttpGet]
        [ActionName("listPDAssignExecutorforEWP")]
        public object listPDAssignExecutorforEWP(int param)
        {
            var projectDocument =
                db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param && w.STATUS == constants.STATUSIPROM_COMPLETED &&
                                          w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && w.EXECUTOR == constants.EXECUTOR_AUTO).
                Select(s => new
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    keyDeliverablesId = s.KEYDELIVERABLESID,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    isTender = s.TENDERID.HasValue ? true : false,

                    project = new projectObject { projectManagerId = s.PROJECT.PROJECTMANAGERID, projectManagerName = s.PROJECT.PROJECTMANAGERNAME, projectManagerEmail = s.PROJECT.PROJECTMANAGEREMAIL }
                });
            return projectDocument;
        }

        [HttpGet]
        [ActionName("listPDByProjectId")]
        public IEnumerable<projectDocumentObject> listPDByProjectId(int param)
        {
            var projectDocument =
                db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param).ToList().
                Select(s => new projectDocumentObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    docType = s.DOCTYPE,
                    keyDeliverablesId = s.KEYDELIVERABLESID,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    documentTrafic = s.PLANFINISHDATE.HasValue ? traficStatus(s.PLANFINISHDATE.Value, s.ACTUALFINISHDATE, s.STATUS) : "",
                    documentStatusLabel = statusLabel(s.PLANFINISHDATE, s.ACTUALFINISHDATE)
                });
            return projectDocument;
        }

        [HttpGet]
        [ActionName("getCustomProjectDocumentByOid")]
        public projectDocumentObject getCustomProjectDocumentByOid(int param)
        {
            var s = db.PROJECTDOCUMENTs.Find(param);
            return new projectDocumentObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                docType = s.DOCTYPE,
                keyDeliverablesId = s.KEYDELIVERABLESID,
                docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                docNo = s.DOCNO,
                docTitle = s.DOCTITLE,
                drawingNo = s.DRAWINGNO,
                drawingTitle = s.DRAWINGTITLE,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                discipline = s.DISCIPLINE,
                status = s.STATUS,
                remark = s.REMARK,
                executor = s.EXECUTOR,
                executorAssignment = s.EXECUTORASSIGNMENT,
                progress = s.PROGRESS,
                documentById = s.DOCUMENTBYID,
                documentByName = s.DOCUMENTBYNAME,
                documentByEmail = s.DOCUMENTBYEMAIL,
                documentTrafic = s.PLANFINISHDATE.HasValue ? traficStatus(s.PLANFINISHDATE.Value, s.ACTUALFINISHDATE, s.STATUS) : "",
                documentStatusLabel = statusLabel(s.PLANFINISHDATE, s.ACTUALFINISHDATE),
                remarksActualResults = s.REMARKSACTUALRESULTS,
                remarksDesignKpi = s.REMARKSDESIGNKPI
            };
        }

        [HttpGet]
        [ActionName("getPDByDocNo")]
        public object getPDByDocNo(string param)
        {
            var projectDocument =
                db.PROJECTDOCUMENTs.Where(w => w.DOCNO == param).ToList().
                Select(s => new
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    docType = s.DOCTYPE,
                    groupDocType = GetDocumentGroupDesignerbyProjectId(s.OID),
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    keydeliverablesId = s.KEYDELIVERABLESID,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,

                    project = new projectObject { projectManagerId = s.PROJECT.PROJECTMANAGERID, projectManagerName = s.PROJECT.PROJECTMANAGERNAME, projectManagerEmail = s.PROJECT.PROJECTMANAGEREMAIL }
                }).FirstOrDefault();
            return projectDocument;
        }

        [HttpGet]
        [ActionName("listPDByUserWeekly")]
        public IQueryable<object> listPDByUserWeekly(int param, string user, int wId)
        {
            var weekly = db.WEEKLies.Find(wId);
            string userID = db.USERs.FirstOrDefault(x => x.NTUSERID == user).BADGENO;
            var projectDocument =
                db.PROJECTDOCUMENTs.
                Where(x => x.DOCUMENTBYID == userID && x.ACTUALFINISHDATE >= weekly.STARTDATE && (db.TIMESHEETs.Where(w => w.WEEKLYID == wId && w.CREATEDBY == user).Select(ts => ts.PROJECTDOCUMENTID)).Contains(x.OID) == false).
                Select(s => new
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    planDate = s.PLANSTARTDATE,
                    planHours = (s.PLANFINISHDATE.Value.Day - s.PLANSTARTDATE.Value.Day) * 8,
                    discipline = s.DISCIPLINE,
                    task = s.DOCTYPE + " - " + s.DOCTITLE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    project = new { projectNo = s.PROJECT.PROJECTNO, projectDescription = s.PROJECT.PROJECTDESCRIPTION }
                });

            var tSupport = db.TECHNICALSUPPORTs.
                Where(x => x.STATUS != "S1" && (db.TIMESHEETs.Where(w => w.WEEKLYID == wId && w.CREATEDBY == user && w.PROJECTDOCUMENTID == null).Select(ts => ts.PROJECTID)).Contains(x.OID) == false).
                Select(tso => new
                {
                    id = tso.OID,
                    tsNo = tso.TSNO,
                    tsDesc = tso.TSDESC,
                    status = tso.STATUS,
                    userAssignments = db.USERASSIGNMENTs.Where(ua => ua.TECHNICALSUPPORTID == tso.OID && ua.ASSIGNMENTTYPE == "TSE"
                    && ua.ISACTIVE == true && ua.EMPLOYEEID == userID).Select(uao => new userAssignmentObject()
                    {
                        id = uao.OID
                    })
                }).Select(s => new
                {
                    id = s.id,
                    projectId = (int?)s.id,
                    docType = "",
                    docDesc = s.tsDesc,
                    docNo = s.tsNo,
                    docTitle = "",
                    drawingNo = "",
                    drawingTitle = "",
                    planStartDate = (DateTime?)null,
                    planFinishDate = (DateTime?)null,
                    actualStartDate = (DateTime?)null,
                    actualFinishDate = (DateTime?)null,
                    planDate = (DateTime?)null,
                    planHours = (int)0,
                    discipline = "",
                    task = (string)null,
                    status = s.status,
                    remark = "",
                    executor = "",
                    executorAssignment = "",
                    executorAssignmentText = "",
                    progress = (double?)null,
                    documentById = "",
                    documentByName = "",
                    documentByEmail = "",
                    project = new { projectNo = s.tsNo, projectDescription = s.tsDesc }
                });

            var pDocUnion = projectDocument.Union(tSupport);
            return pDocUnion;
        }
        [HttpGet]
        [ActionName("getParameterValue")]
        public double? getParameterValue(int param)
        {
            return GetTotalOfProductionByProjId(param);
        }

        [HttpGet]
        [ActionName("listPDByProjectIdDocType")]
        public IEnumerable<projectDocumentObject> listPDByProjectIdDocType(int param, string docType)
        {
            IEnumerable<projectDocumentObject> projectDocument =
                db.PROJECTDOCUMENTs.Where(w => (w.PROJECTID == param && w.DOCTYPE == docType)).ToList().
                Select(s => new projectDocumentObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    keyDeliverablesId = s.KEYDELIVERABLESID,
                    keyDeliverablesName = s.KEYDELIVERABLE != null ? s.KEYDELIVERABLE.NAME : "",
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    ewpDocType = db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).Count() > 0 ?
                        db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).FirstOrDefault().TEXT : "",
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    drawingCount = s.DRAWINGCOUNT,
                    projectDocumentReferences = s.PROJECTDOCUMENTREFERENCEs.Select(se => new projectDocumentReferenceObject
                    {
                        id = se.OID,
                        projectDocRef = se.PROJECTDOCUMENT1 != null ? new getProjectDocumentRefObject
                        {
                            id = se.PROJECTDOCUMENT1.OID,
                            docNo = se.PROJECTDOCUMENT1.DOCNO,
                            docTitle = se.PROJECTDOCUMENT1.DOCTITLE,
                            position = db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                                db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                                    f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                                ) == null ? null : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                                 db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                                     f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                                ).TEXT,
                            ewpDocType = db.LOOKUPs.FirstOrDefault(l => (l.TYPE == constants.ewpDocType && l.VALUE == (se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Length))))?.TEXT
                        } : new getProjectDocumentRefObject()
                    }),
                    cutOfDate = s.CUTOFDATE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
//                    status = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.STATUS : s.STATUS == constants.STATUSIPROM_HOLD || s.STATUS == constants.STATUSIPROM_CANCELLED ? s.STATUS : !s.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !s.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED,
                    status = s.STATUS,
                    statusText = s.STATUS!= null ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == s.STATUS)?.TEXT : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == constants.STATUSIPROM_NOTSTARTED)?.TEXT,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    documentTrafic = "",
                    revision = s.REVISION,
                    sharedFolder = s.SHAREDFOLDER,
                    PMName = s.PROJECT.PROJECTMANAGERNAME,
                    pmoNo = s.PMO,
                    contractNo = s.CONTRACTNO,
                    tenderNo = s.TENDERNO,
                    weightHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS),
                    documentGroup = GetDocumentGroupDesigner(s),
                    parameterValue = GetDocumentGroupDesigner(s) == constants.DOCUMENTTYPE_ENGINEER ? GetParameterValueForProjectDoc(s) : GetDocumentGroupDesigner(s) == constants.DOCUMENTTYPE_DESIGN_CONFORMITY ? GetFinalResultByValueByProjDocument(s) : 0,

                    position = (s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER) ? db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                    db.USERASSIGNMENTs.Where(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                            f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).Count()>0 ? db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                    db.USERASSIGNMENTs.Where(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                            f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).FirstOrDefault().TEXT : "" : "",
                    participationValue = s.PARTICIPATIONVALUE
                }).OrderBy(o => !o.actualStartDate.HasValue ? o.planStartDate : o.planFinishDate);
            return projectDocument;
        }

        [HttpGet]
        [ActionName("ConstructionListEWP")]
        public IEnumerable<projectDocumentObject> ConstructionListEWP(int param)
        {
            IEnumerable<projectDocumentObject> projectDocument =
                db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && (!string.IsNullOrEmpty(w.PMO) || !string.IsNullOrEmpty(w.CONTRACTNO))).ToList().
                Select(s => new projectDocumentObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    keyDeliverablesId = s.KEYDELIVERABLESID,
                    keyDeliverablesName = s.KEYDELIVERABLE != null ? s.KEYDELIVERABLE.NAME : "",
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    ewpDocType = db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).Count() > 0 ?
                        db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).FirstOrDefault().TEXT : "",
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    drawingCount = s.DRAWINGCOUNT,
                    projectDocumentReferences = s.PROJECTDOCUMENTREFERENCEs.Select(se => new projectDocumentReferenceObject
                    {
                        id = se.OID,
                        projectDocRefId = se.PROJECTDOCUMENTREFERENCEID,
                        projectDocRef = new getProjectDocumentRefObject
                        {
                            id = se.PROJECTDOCUMENT1.OID,
                            docNo = se.PROJECTDOCUMENT1.DOCNO,
                            docTitle = se.PROJECTDOCUMENT1.DOCTITLE
                        }
                    }),
                    cutOfDate = s.CUTOFDATE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    //                    status = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.STATUS : s.STATUS == constants.STATUSIPROM_HOLD || s.STATUS == constants.STATUSIPROM_CANCELLED ? s.STATUS : !s.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !s.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED,
                    status = s.STATUS,
                    statusText = s.STATUS != null ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == s.STATUS)?.TEXT : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == constants.STATUSIPROM_NOTSTARTED)?.TEXT,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    documentTrafic = "",
                    revision = s.REVISION,
                    sharedFolder = s.SHAREDFOLDER,
                    PMName = s.PROJECT.PROJECTMANAGERNAME,
                    pmoNo = s.PMO,
                    contractNo = s.CONTRACTNO,
                    tenderId = s.TENDERID,
                    tenderNo = s.TENDERNO,
                    position = (s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER) ? db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                    db.USERASSIGNMENTs.Where(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                            f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).Count() > 0 ? db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                      db.USERASSIGNMENTs.Where(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                              f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).FirstOrDefault().TEXT : "" : "",
                }).OrderBy(o => !o.actualStartDate.HasValue ? o.planStartDate : o.planFinishDate);
            return projectDocument;
        }

        [HttpGet]
        [ActionName("listPDByProjectIdEWPDC")]
        public IEnumerable<projectDocumentObject> listPDByProjectIdEWPDC(int param)
        {
            IEnumerable<projectDocumentObject> projectDocument =
                db.PROJECTDOCUMENTs.Where(w => (w.PROJECTID == param && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER))).ToList().
                Select(s => new projectDocumentObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    keyDeliverablesId = s.KEYDELIVERABLESID,
                    keyDeliverablesName = s.KEYDELIVERABLE != null ? s.KEYDELIVERABLE.NAME : "",
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    ewpDocType = db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).Count() > 0 ?
                        db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        s.DOCTITLE.Substring(s.DOCTITLE.IndexOf("-") >= 0 ? s.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).FirstOrDefault().TEXT : "",
                    drawingNo = s.DRAWINGNO,
                    drawingTitle = s.DRAWINGTITLE,
                    drawingCount = s.DRAWINGCOUNT,
                    projectDocumentReferences = s.PROJECTDOCUMENTREFERENCEs.Select(se => new projectDocumentReferenceObject
                    {
                        id = se.OID,
                        projectDocRef = se.PROJECTDOCUMENT1 != null ? new getProjectDocumentRefObject
                        {
                            id = se.PROJECTDOCUMENT1.OID,
                            docNo = se.PROJECTDOCUMENT1.DOCNO,
                            docTitle = se.PROJECTDOCUMENT1.DOCTITLE,
                            position = db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                                db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                                    f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                                ).TEXT,
                            ewpDocType = db.LOOKUPs.FirstOrDefault(l => (l.TYPE == constants.ewpDocType && l.VALUE == (se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Length))))?.TEXT
                        } : new getProjectDocumentRefObject()
                    }),
                    cutOfDate = s.CUTOFDATE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    //                    status = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.STATUS : s.STATUS == constants.STATUSIPROM_HOLD || s.STATUS == constants.STATUSIPROM_CANCELLED ? s.STATUS : !s.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !s.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED,
                    status = s.STATUS,
                    statusText = s.STATUS != null ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == s.STATUS)?.TEXT : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == constants.STATUSIPROM_NOTSTARTED)?.TEXT,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    documentTrafic = "",
                    revision = s.REVISION,
                    sharedFolder = s.SHAREDFOLDER,
                    PMName = s.PROJECT.PROJECTMANAGERNAME,
                    pmoNo = s.PMO,
                    contractNo = s.CONTRACTNO,
                    tenderNo = s.TENDERNO,
                    position = (s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER) ? db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                    db.USERASSIGNMENTs.Where(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                            f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).Count() > 0 ? db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                      db.USERASSIGNMENTs.Where(f => f.PROJECTID == s.PROJECTID && f.EMPLOYEEID == s.DOCUMENTBYID &&
                              f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).FirstOrDefault().TEXT : "" : "",
                }).OrderBy(o => !o.actualStartDate.HasValue ? o.planStartDate : o.planFinishDate);
            return projectDocument;
        }

        private string statusDocument(string status, DateTime? actualStartDate, DateTime? actualFinishDate)
        {
            return status == constants.STATUSIPROM_HOLD || status == constants.STATUSIPROM_CANCELLED ? status : !actualStartDate.HasValue ? constants.STATUSIPROM_NOTSTARTED : !actualFinishDate.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED;
        }

        [HttpPost]
        [ActionName("postprojectDocumentObject")]
        public HttpResponseMessage postProjectDocumentObject(int param, projectDocumentObject uAObj)
        {
            if (ModelState.IsValid)
            {
                if (db.PROJECTDOCUMENTs.Where(x => (x.DOCNO == uAObj.docNo && x.PROJECTID == uAObj.projectId && uAObj.docType != x.DOCTYPE && uAObj.docType == constants.DOCUMENTTYPE_ENGINEER)).Count() == 0)
                {
                    var pdCount = 0;
                    var newSeq = "";
                    if (uAObj.docType == constants.DOCUMENTTYPE_ENGINEER)
                    {
                        pdCount = db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCTYPE==constants.DOCUMENTTYPE_ENGINEER);
                        var uC = new UserController();
                        var badgeNo = uC.FindByNTUserID(uAObj.createdBy).FirstOrDefault().BADGENO;
                        var engPos = getEngineerPosInProject(uAObj.projectId.Value, badgeNo);
                        var engPosPos = engPos.GetType().GetProperty("pos").GetValue(engPos, null);
                        var docType = uAObj.docTitle.IndexOf(constants.docTypeWithReport) >= 0 ? engPosPos + "R" : engPosPos; 
                        newSeq = uAObj.docNo.Replace("AUTO",  docType + (pdCount + 1).ToString());
                        while (db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCNO == newSeq) > 0)
                        {
                            pdCount++;
                            newSeq = uAObj.docNo.Replace("AUTO", docType + (pdCount + 1).ToString());
                        }
                    } else if (uAObj.docType == constants.DOCUMENTTYPE_DESIGN)
                    {
                        pdCount = db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCTYPE == constants.DOCUMENTTYPE_DESIGN);
                        newSeq = uAObj.docNo.Replace("#", (pdCount + 1).ToString());
                        while (db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCNO == newSeq) > 0)
                        {
                            pdCount++;
                            newSeq = uAObj.docNo.Replace("#", (pdCount + 1).ToString());
                        }
                    }
                    else if (uAObj.docType == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                    {
                        pdCount = db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY);
                        newSeq = uAObj.docNo.Replace("XXX", (pdCount + 1).ToString());
                        while (db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCNO == newSeq) > 0)
                        {
                            pdCount++;
                            newSeq = uAObj.docNo.Replace("#", (pdCount + 1).ToString());
                        }
                    }

                    uAObj.docNo = newSeq;
                    var projectDocument = new PROJECTDOCUMENT()
                    {
                        //OID = 0,
                        PROJECTID = uAObj.projectId,
                        KEYDELIVERABLESID = uAObj.keyDeliverablesId,
                        DOCTYPE = uAObj.docType,
                        DOCNO = uAObj.docNo,
                        DOCTITLE = uAObj.docTitle,
                        DRAWINGNO = uAObj.drawingNo,
                        DRAWINGTITLE = uAObj.drawingTitle,
                        DRAWINGCOUNT = uAObj.drawingCount,
                        CUTOFDATE = uAObj.cutOfDate,
                        PLANSTARTDATE = uAObj.planStartDate,
                        PLANFINISHDATE = uAObj.planFinishDate,
                        ACTUALSTARTDATE = uAObj.actualStartDate,
                        ACTUALFINISHDATE = uAObj.actualFinishDate,
                        DISCIPLINE = uAObj.discipline,
                        STATUS = uAObj.status,
                        REMARK = uAObj.remark,
                        PROGRESS = uAObj.progress,
                        DOCUMENTBYNAME = uAObj.documentByName,
                        DOCUMENTBYEMAIL = uAObj.documentByEmail,
                        DOCUMENTBYID = uAObj.documentById,
                        REVISION = 0,
                        PMO = uAObj.pmoNo,
                        CONTRACTNO = uAObj.contractNo,
                        TENDERNO = uAObj.tenderNo,
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = uAObj.createdBy,
                        UPDATEDDATE = DateTime.Now,
                        UPDATEDBY = uAObj.updatedBy,
                        PROJECTDOCUMENTREFERENCEs = uAObj.projectDocumentReferences != null ? uAObj.projectDocumentReferences.Select(s => new PROJECTDOCUMENTREFERENCE { PROJECTDOCUMENTREFERENCEID = s.projectDocRefId } ).ToList() : null
                        
                    };

                    db.PROJECTDOCUMENTs.Add(projectDocument);
                    db.SaveChanges();
                    uAObj.statusText = projectDocument.STATUS != null ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == projectDocument.STATUS)?.TEXT : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == constants.STATUSIPROM_NOTSTARTED)?.TEXT;
                    uAObj.id = projectDocument.OID;
                    uAObj.revision = projectDocument.REVISION;
                    uAObj.keyDeliverablesName = projectDocument.KEYDELIVERABLESID != null ? db.KEYDELIVERABLES.Find(projectDocument.KEYDELIVERABLESID).NAME : "" ;
                    uAObj.status = projectDocument.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? projectDocument.STATUS : projectDocument.STATUS == constants.STATUSIPROM_HOLD || projectDocument.STATUS == constants.STATUSIPROM_CANCELLED ? projectDocument.STATUS : !projectDocument.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !projectDocument.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED;
                    uAObj.projectDocumentReferences = db.PROJECTDOCUMENTREFERENCEs.Where(w => w.PROJECTDOCUMENTID == projectDocument.OID).Select(se => new projectDocumentReferenceObject
                    {
                        id = se.OID,
                        projectDocRefId = se.PROJECTDOCUMENTREFERENCEID,
                        projectDocRef = new getProjectDocumentRefObject
                        {
                            id = se.PROJECTDOCUMENT1.OID,
                            docNo = se.PROJECTDOCUMENT1.DOCNO,
                            docTitle = se.PROJECTDOCUMENT1.DOCTITLE
                        }
                    });
                    uAObj.position = db.LOOKUPs.Where(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                    db.USERASSIGNMENTs.Where(f => f.PROJECTID == uAObj.projectId && f.EMPLOYEEID == uAObj.documentById &&
                            f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).FirstOrDefault().EMPLOYEEPOSITION
                        ).FirstOrDefault().TEXT;

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, uAObj);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = uAObj.id }));
                    return response;
                }
                else
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Duplicate Doc. No.");
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", 0));
                    return response;
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPost]
        [ActionName("postProjectDocumentObjectBadgeNo")]
        public HttpResponseMessage postProjectDocumentObjectBadgeNo(string param, projectDocumentObject uAObj)
        {
            if (ModelState.IsValid)
            {
                if (db.PROJECTDOCUMENTs.Where(x => (x.DOCNO == uAObj.docNo && x.PROJECTID == uAObj.projectId && uAObj.docType != x.DOCTYPE && uAObj.docType == constants.DOCUMENTTYPE_ENGINEER)).Count() == 0)
                {
                    var pdCount = 0;
                    var newSeq = "";
                    if (uAObj.docType == constants.DOCUMENTTYPE_ENGINEER)
                    {
                        pdCount = db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER);
                        var uC = new UserController();
                        //                        var badgeNo = uC.FindByNTUserID(uAObj.createdBy).FirstOrDefault().BADGENO;
                        var engPos = getEngineerPosInProject(uAObj.projectId.Value, param);
                        var engPosPos = engPos.GetType().GetProperty("pos").GetValue(engPos, null);
                        var docType = uAObj.docTitle.IndexOf(constants.docTypeWithReport) >= 0 ? engPosPos + "R" : engPosPos;
                        newSeq = uAObj.docNo.Replace("AUTO", docType + (pdCount + 1).ToString());
                        while (db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCNO == newSeq) > 0)
                        {
                            pdCount++;
                            newSeq = uAObj.docNo.Replace("AUTO", docType + (pdCount + 1).ToString());
                        }
                        uAObj.docNo = newSeq;
                    }
                    else if (uAObj.docType == constants.DOCUMENTTYPE_DESIGN)
                    {
                        pdCount = db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCTYPE == constants.DOCUMENTTYPE_DESIGN);
                        newSeq = uAObj.drawingNo.Replace("#", (pdCount + 1).ToString());
                        while (db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DRAWINGNO == newSeq) > 0)
                        {
                            pdCount++;
                            newSeq = uAObj.docNo.Replace("#", (pdCount + 1).ToString());
                        }
                        uAObj.drawingNo = newSeq;
                    }
                    else if (uAObj.docType == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                    {
                        pdCount = db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY);
                        newSeq = uAObj.docNo.Replace("XXX", (pdCount + 1).ToString());
                        while (db.PROJECTDOCUMENTs.Count(p => p.PROJECTID == uAObj.projectId && p.DOCNO == newSeq) > 0)
                        {
                            pdCount++;
                            newSeq = uAObj.docNo.Replace("XXX", (pdCount + 1).ToString());
                        }
                        uAObj.docNo = newSeq;
                    }

                    var projectDocument = new PROJECTDOCUMENT()
                    {
                        //OID = 0,
                        PROJECTID = uAObj.projectId,
                        KEYDELIVERABLESID = uAObj.keyDeliverablesId,
                        DOCTYPE = uAObj.docType,
                        DOCNO = uAObj.docNo,
                        DOCTITLE = uAObj.docTitle,
                        DRAWINGNO = uAObj.drawingNo,
                        DRAWINGTITLE = uAObj.drawingTitle,
                        DRAWINGCOUNT = uAObj.drawingCount,
                        CUTOFDATE = uAObj.cutOfDate,
                        PLANSTARTDATE = uAObj.planStartDate,
                        PLANFINISHDATE = uAObj.planFinishDate,
                        DISCIPLINE = uAObj.discipline,
                        STATUS = uAObj.status,
                        REMARK = uAObj.remark,
                        PROGRESS = uAObj.progress,
                        DOCUMENTBYNAME = uAObj.documentByName,
                        DOCUMENTBYEMAIL = uAObj.documentByEmail,
                        DOCUMENTBYID = uAObj.documentById,
                        REVISION = 0,
                        PMO = uAObj.pmoNo,
                        CONTRACTNO = uAObj.contractNo,
                        TENDERNO = uAObj.tenderNo,
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = uAObj.createdBy,
                        UPDATEDDATE = DateTime.Now,
                        UPDATEDBY = uAObj.updatedBy,
                        PROJECTDOCUMENTREFERENCEs = uAObj.projectDocumentReferences != null ? uAObj.projectDocumentReferences.Select(s => new PROJECTDOCUMENTREFERENCE { PROJECTDOCUMENTREFERENCEID = s.projectDocRefId }).ToList() : null,
                        PROJECTDOCUMENTWEIGHTHOURS = uAObj.projectDocumentWeightHours != null ? uAObj.projectDocumentWeightHours.Where(w => w.actualHours > 0).Select(s => new PROJECTDOCUMENTWEIGHTHOUR
                        {
                            MASTERWEIGHTHOURSID = s.masterWeightHoursId,
                            MASTERWEIGHTHOURSDESC = s.masterWeightHoursDesc,
                            ESTIMATEHOURS = s.estimateHours,
                            ACTUALHOURS = s.actualHours,
                            SEQ = s.seq,
                            PARENTID = s.parentId,
                            ISEDITABLE = s.isEditable,
                            ISPARENTGROUP = s.isParentGroup,
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        }).ToList() : null
                    };

                    db.PROJECTDOCUMENTs.Add(projectDocument);
                    db.SaveChanges();

                    uAObj.id = projectDocument.OID;
                    uAObj.documentGroup = db.PROJECTDOCUMENTs.Where(w => w.DOCNO == projectDocument.DOCNO && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).Count() > 0 ? db.PROJECTDOCUMENTs.Where(w => w.DOCNO == projectDocument.DOCNO && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).FirstOrDefault().DOCTYPE : null;
                    uAObj.revision = projectDocument.REVISION;
                    uAObj.keyDeliverablesName = projectDocument.KEYDELIVERABLESID != null ? db.KEYDELIVERABLES.Find(projectDocument.KEYDELIVERABLESID).NAME : "";
                    uAObj.status = projectDocument.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? projectDocument.STATUS : projectDocument.STATUS == constants.STATUSIPROM_HOLD || projectDocument.STATUS == constants.STATUSIPROM_CANCELLED ? projectDocument.STATUS : !projectDocument.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !projectDocument.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED;
                    uAObj.statusText = projectDocument.STATUS != null ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == projectDocument.STATUS)?.TEXT : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == constants.STATUSIPROM_NOTSTARTED)?.TEXT;
                    uAObj.projectDocumentReferences = db.PROJECTDOCUMENTREFERENCEs.Where(w => w.PROJECTDOCUMENTID == projectDocument.OID).Select(se => new projectDocumentReferenceObject
                    {
                        id = se.OID,
                        projectDocRefId = se.PROJECTDOCUMENTREFERENCEID,
                        projectDocRef = new getProjectDocumentRefObject
                        {
                            id = se.PROJECTDOCUMENT1.OID,
                            docNo = se.PROJECTDOCUMENT1.DOCNO,
                            docTitle = se.PROJECTDOCUMENT1.DOCTITLE,
                            position = db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                                db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == projectDocument.PROJECTID && f.EMPLOYEEID == projectDocument.DOCUMENTBYID &&
                                    f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                                ).TEXT,
                            ewpDocType = db.LOOKUPs.FirstOrDefault(l => (l.TYPE == constants.ewpDocType && l.VALUE == (se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                                se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).TEXT

                        }
                    });
                    uAObj.position = (projectDocument.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER) ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                    db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == projectDocument.PROJECTID && f.EMPLOYEEID == projectDocument.DOCUMENTBYID &&
                            f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                        )?.TEXT : "";
                    uAObj.ewpDocType = db.LOOKUPs.FirstOrDefault(l => (l.TYPE == constants.ewpDocType && l.VALUE == (projectDocument.DOCTITLE.Substring(projectDocument.DOCTITLE.IndexOf("-") >= 0 ? projectDocument.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        projectDocument.DOCTITLE.Substring(projectDocument.DOCTITLE.IndexOf("-") >= 0 ? projectDocument.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        projectDocument.DOCTITLE.Substring(projectDocument.DOCTITLE.IndexOf("-") >= 0 ? projectDocument.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        projectDocument.DOCTITLE.Substring(projectDocument.DOCTITLE.IndexOf("-") >= 0 ? projectDocument.DOCTITLE.IndexOf("-") + 1 : 0).Length))))?.TEXT;

                    if (uAObj.projectDocumentWeightHours != null)
                    {
                        uAObj.weightHours = uAObj.projectDocumentWeightHours.Sum(sum => sum.actualHours);
                    }

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, uAObj);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = uAObj.id }));
                    return response;
                }
                else
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Duplicate Doc. No.");
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", 0));
                    return response;
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        void sendPFDPSDChangeEmail(usernameemail designer, projectDocumentObject uAObj, PROJECTDOCUMENT userObj, DateTime? oldPSD, DateTime? oldPFD)
        {
            EmailSender emailSender = new EmailSender();
            var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;

            var email = designer.email;
            if (email != null)
            {
                var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.EmailPSDPFDChange).FirstOrDefault();

                string RecipientEmail = email;

                string RecipientName = designer.name;
                var RecipientCC = "";
                var FromEmail = "testing@ptgdi.com";
                var subject = mail.SUBJECT;
                var body = mail.BODY;
                var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + designer.userId).FirstOrDefault();
                string prefixGender = "";
                if (employeeDetail != null)
                {
                    if (employeeDetail != null && employeeDetail.GENDER != null)
                    {
                        if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                            prefixGender = constants.prefixmale;
                        else
                            if (employeeDetail.MARITAL_STATUS == constants.married)
                            prefixGender = constants.prefixfemaleMarried;
                        else
                            prefixGender = constants.prefixfemaleSingle;
                    }
                    else prefixGender = "Mr\\Mrs";
                    var t1 = userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-"));
                    var t2 = t1.IndexOf(" ") >= 0? t1.Substring(t1.IndexOf(" ")+1): t1.Substring(0);
                   // var t3 = t2.Substring(t2.IndexOf(" "));
                    var docTitleLast = t2.IndexOf(" ") >=0 ? t2.Substring(t2.IndexOf(" ")): t2.Substring(0);
                    var docTitleFirst = userObj.DOCTITLE.Substring(0, userObj.DOCTITLE.IndexOf("-"));
                    var docTitleText = userObj.PROJECT.PROJECTDESCRIPTION + " - " + uAObj.ewpDocType+ " " + uAObj.position +" " + docTitleLast;
                    body = body.Replace("[user]", prefixGender + " " + designer.name);
                    body = body.Replace("[projectNo]", userObj.PROJECT.PROJECTNO);
                    body = body.Replace("[projectDesc]", userObj.PROJECT.PROJECTDESCRIPTION);
                    body = body.Replace("[docNo]", userObj.DOCNO);
                    body = body.Replace("[docTitle]", (uAObj.ewpDocType!=null)?docTitleText:userObj.DOCTITLE);
                    body = body.Replace("[oldPSD]", oldPSD.HasValue ? oldPSD.Value.ToString("dd MMMM yyyy") : "-");
                    body = body.Replace("[oldPFD]", oldPFD.HasValue ? oldPFD.Value.ToString("dd MMMM yyyy") : "-");
                    body = body.Replace("[newPSD]", uAObj.planStartDate.HasValue ? uAObj.planStartDate.Value.ToString("dd MMMM yyyy") : "-");
                    body = body.Replace("[newPFD]", uAObj.planFinishDate.HasValue ? uAObj.planFinishDate.Value.ToString("dd MMMM yyyy") : "-");
                    body = body.Replace("[assign]", ac.getEmployeeByBN(uAObj.updatedBy).full_Name);
                    body = body.Replace("\r", "");
                    body = body.Replace("\n", "");
                    body = body.Replace("\\", "");
                    emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);
                }
            }

        }

        void sendEmailToDesigner(IEnumerable<usernameemail> designer, projectDocumentObject uAObj, PROJECTDOCUMENT userObj, DateTime? oldPSD, DateTime? oldPFD)
        {
            foreach (var u in designer)
            {
                var id = u.userId;
                var name = u.name;
                var email = u.email;
                sendPFDPSDChangeEmail(u, uAObj, userObj, oldPSD, oldPFD);
            }


        }

        [HttpPut]
        [ActionName("putProjectDocumentObject")]
        public HttpResponseMessage putProjectDocumentObject(int param, projectDocumentObject uAObj)
        {
            DateTime? oldPSD = new DateTime();
            DateTime? oldPFD = new DateTime();

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (param != uAObj.id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            if (db.PROJECTDOCUMENTs.Where(x => (x.DOCNO == uAObj.docNo && x.PROJECTID == uAObj.projectId && uAObj.id != x.OID && uAObj.docType == x.DOCTYPE && uAObj.docType == constants.DOCUMENTTYPE_ENGINEER)).Count() == 0)
            {
                var userObj = db.PROJECTDOCUMENTs.Find(uAObj.id);
                userObj.PROJECTID = uAObj.projectId;
                userObj.KEYDELIVERABLESID = uAObj.keyDeliverablesId;
                userObj.DOCTYPE = uAObj.docType;
                //userObj.DOCNO = uAObj.docNo;
                userObj.DOCTITLE = uAObj.docTitle;
                userObj.DRAWINGNO = uAObj.drawingNo;
                userObj.DRAWINGTITLE = uAObj.drawingTitle;
                userObj.DRAWINGCOUNT = uAObj.drawingCount;
                userObj.CUTOFDATE = uAObj.cutOfDate;
                oldPSD = userObj.PLANSTARTDATE;
                oldPFD = userObj.PLANFINISHDATE;
                if (userObj.PLANSTARTDATE != uAObj.planStartDate ||
                    userObj.PLANFINISHDATE != uAObj.planFinishDate)
                {
                    userObj.REVISION = (userObj.REVISION.HasValue ? userObj.REVISION.Value : 0) + 1;
                }

                userObj.SHAREDFOLDER = uAObj.sharedFolder;
                userObj.PLANSTARTDATE = uAObj.planStartDate;
                userObj.PLANFINISHDATE = uAObj.planFinishDate;
                userObj.ACTUALSTARTDATE = uAObj.actualStartDate;
                userObj.ACTUALFINISHDATE = uAObj.actualFinishDate;
                userObj.DISCIPLINE = uAObj.discipline;
                userObj.DOCUMENTBYID = uAObj.documentById;
                userObj.DOCUMENTBYNAME = uAObj.documentByName;
                userObj.DOCUMENTBYEMAIL = uAObj.documentByEmail;
                userObj.PROGRESS = uAObj.progress;
                var tmpStatus = userObj.STATUS;
                if (uAObj.actualStartDate != null)
                {
                    userObj.STATUS = constants.STATUSIPROM_INPROGRESS;
                }
                if (uAObj.actualFinishDate != null)
                {
                    userObj.STATUS = constants.STATUSIPROM_COMPLETED;
                    userObj.PROGRESS = 100;
                }
                if(tmpStatus != null  && tmpStatus != uAObj.status) userObj.STATUS = uAObj.status;
                //if (userObj.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && uAObj.executor == constants.EXECUTOR_CS && !(userObj.EXECUTOR == constants.EXECUTOR_CS))
                //{
                //    if (!userObj.TENDERID.HasValue)
                //    {
                //        TENDER tenderNew = new TENDER();
                //        tenderNew.TENDERNO = userObj.DOCNO;
                //        tenderNew.TENDERCREATEDDATE = DateTime.Now;
                //        tenderNew.TENDERCREATEDBY = GetCurrentNTUserId();
                //        tenderNew.CREATEDBY = GetCurrentNTUserId();
                //        tenderNew.CREATEDDATE = DateTime.Now;

                //        userObj.TENDER = tenderNew;
                //    }
                //    else
                //    {
                //        foreach (TENDERPROGRESS removeTender in userObj.TENDER.TENDERPROGRESSes)
                //        {
                //            userObj.TENDER.TENDERPROGRESSes.Remove(removeTender);
                //        }

                //        userObj.TENDER.TENDERNO = userObj.DOCNO;
                //        userObj.TENDER.UPDATEDBY = GetCurrentNTUserId();
                //        userObj.TENDER.UPDATEDDATE = DateTime.Now;
                //    }
                //}
                userObj.EXECUTOR = uAObj.executor;

                if (uAObj.executor == constants.EXECUTOR_CS)
                {
                    userObj.PMO = uAObj.pmoNo;
                    userObj.CONTRACTNO = null;
                    userObj.TENDERNO = null;
                }
                else if (uAObj.executor == constants.EXECUTOR_CAS)
                {
                    userObj.PMO = null;
                    userObj.CONTRACTNO = uAObj.contractNo;
                    userObj.TENDERNO = uAObj.tenderNo;
                }
                else
                {
                    userObj.PMO = null;
                    userObj.CONTRACTNO = null;
                    userObj.TENDERNO = null;
                }

                if (userObj.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && uAObj.executor == constants.EXECUTOR_CAS)
                {
                    TENDER tender = db.TENDERs.Where(w => w.CONTRACTNO == userObj.CONTRACTNO).FirstOrDefault();
                    if (tender != null)
                    {
                        userObj.TENDERID = tender.OID;
                    }
                    else
                    {
                        TENDER newTender = new TENDER();
                        newTender.TENDERNO = uAObj.tenderNo;
                        newTender.TENDERCREATEDBY = GetCurrentNTUserId();
                        newTender.TENDERCREATEDDATE = DateTime.Now;
                        newTender.CONTRACTNO = uAObj.contractNo;
                        newTender.CONTRACTCREATEDBY = GetCurrentNTUserId();
                        newTender.CONTRACTCREATEDDATE = DateTime.Now;
                        newTender.CREATEDBY = GetCurrentNTUserId();
                        newTender.CREATEDDATE = DateTime.Now;
                        userObj.TENDER = newTender;
                    }
                }
                else if (userObj.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && uAObj.executor == constants.EXECUTOR_CS)
                {
                    TENDER tender = db.TENDERs.Where(w => w.PMONO == userObj.PMO).FirstOrDefault();
                    if (tender != null)
                    {
                        userObj.TENDERID = tender.OID;
                    }
                    else
                    {
                        TENDER newTender = new TENDER();
                        newTender.PMONO = uAObj.pmoNo;
                        newTender.PMOCREATEDBY = GetCurrentNTUserId();
                        newTender.PMOCREATEDDATE = DateTime.Now;
                        newTender.CREATEDBY = GetCurrentNTUserId();
                        newTender.CREATEDDATE = DateTime.Now;
                        userObj.TENDER = newTender;
                    }
                }
                else {
                    userObj.TENDERID = null;
                }

                userObj.REMARK = uAObj.remark;
                userObj.UPDATEDDATE = DateTime.Now;
                userObj.UPDATEDBY = uAObj.updatedBy;

                if (userObj.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                {
                    //addReference
                    if (uAObj.projectDocumentReferences != null)
                    {
                        foreach (var addData in uAObj.projectDocumentReferences.Where(w => !userObj.PROJECTDOCUMENTREFERENCEs.Select(s1 => s1.PROJECTDOCUMENTREFERENCEID).Contains(w.projectDocRefId)).Select(se => new PROJECTDOCUMENTREFERENCE { PROJECTDOCUMENTREFERENCEID = se.projectDocRefId }))
                        {
                            userObj.PROJECTDOCUMENTREFERENCEs.Add(addData);
                        }

                        var janganJanganAdaYangMake = uAObj.projectDocumentReferences.Select(s1 => s1.projectDocRefId);
                        var x = db.PROJECTDOCUMENTREFERENCEs.Where(w => w.PROJECTDOCUMENTID == param && !janganJanganAdaYangMake.Contains(w.PROJECTDOCUMENTREFERENCEID)).ToList();
                        //deleteReference
                        foreach (var delData in db.PROJECTDOCUMENTREFERENCEs.Where(w => w.PROJECTDOCUMENTID == param && !janganJanganAdaYangMake.Contains(w.PROJECTDOCUMENTREFERENCEID)).ToList())
                        {
                            db.PROJECTDOCUMENTREFERENCEs.Remove(delData);
                        }
                    }
                }
                db.Entry(userObj).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    uAObj.revision = userObj.REVISION;
                    uAObj.status = uAObj.docType == constants.DOCUMENTTYPE_DESIGN ? userObj.STATUS : userObj.STATUS == constants.STATUSIPROM_HOLD || userObj.STATUS == constants.STATUSIPROM_CANCELLED ? userObj.STATUS : !userObj.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !userObj.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED;
                    if (db.PROJECTDOCUMENTREFERENCEs.Where(w => w.PROJECTDOCUMENTID == userObj.OID).Count() > 0)
                    {
                        uAObj.projectDocumentReferences = db.PROJECTDOCUMENTREFERENCEs.Where(w => w.PROJECTDOCUMENTID == userObj.OID).Select(se => new projectDocumentReferenceObject
                        {
                            id = se.OID,
                            projectDocRefId = se.PROJECTDOCUMENTREFERENCEID != null ? se.PROJECTDOCUMENTREFERENCEID : null,
                            projectDocRef = se.PROJECTDOCUMENT1 != null ? new getProjectDocumentRefObject
                            {
                                id = se.PROJECTDOCUMENT1.OID,
                                docNo = se.PROJECTDOCUMENT1.DOCNO,
                                docTitle = se.PROJECTDOCUMENT1.DOCTITLE,
                                position = db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                                    db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == userObj.PROJECTID && f.EMPLOYEEID == userObj.DOCUMENTBYID &&
                                        f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                                    ).TEXT,
                                ewpDocType = db.LOOKUPs.FirstOrDefault(l => (l.TYPE == constants.ewpDocType && l.VALUE == (se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                                    se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                                    se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                                    se.PROJECTDOCUMENT1.DOCTITLE.Substring(se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") >= 0 ? se.PROJECTDOCUMENT1.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).TEXT

                            } : null
                        });
                    }
                    else
                        uAObj.projectDocumentReferences = null;
                    uAObj.progress = userObj.PROGRESS;
                    uAObj.status = userObj.STATUS;
                    uAObj.statusText = userObj.STATUS != null ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == userObj.STATUS)?.TEXT : db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.statusIprom && l.VALUE == constants.STATUSIPROM_NOTSTARTED)?.TEXT;
                    uAObj.position = (userObj.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER) ? db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE ==
                                        db.USERASSIGNMENTs.FirstOrDefault(f => f.PROJECTID == userObj.PROJECTID && f.EMPLOYEEID == userObj.DOCUMENTBYID &&
                                                f.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).EMPLOYEEPOSITION
                                            )?.TEXT : ""; uAObj.ewpDocType = db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).Count() > 0 ?
                        db.LOOKUPs.Where(l => (l.TYPE == constants.ewpDocType && l.VALUE == (userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).Substring(0,
                        userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") >= 0 ?
                        userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).IndexOf(" ") :
                        userObj.DOCTITLE.Substring(userObj.DOCTITLE.IndexOf("-") >= 0 ? userObj.DOCTITLE.IndexOf("-") + 1 : 0).Length)))).FirstOrDefault().TEXT : "";

                    if ((oldPSD != uAObj.planStartDate) || (oldPFD != uAObj.planFinishDate))
                    {
                        //cari ada designer nya gak?
                        IEnumerable<usernameemail> designer = db.PROJECTDOCUMENTs.Where(pd =>
                        (pd.PROJECTID == uAObj.projectId && pd.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && pd.DOCNO.Contains(uAObj.docNo)))
                           .Select(s => new usernameemail()
                           {
                               userId = s.DOCUMENTBYID,
                               name = s.DOCUMENTBYNAME,
                               email = s.DOCUMENTBYEMAIL
                           }).Distinct();
                        //kalo ada
                        if (designer != null)
                        {
                            //kirim email ke designer
                            sendEmailToDesigner(designer, uAObj, userObj, oldPSD, oldPFD);

                        }
                    }


                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }

            return Request.CreateResponse(HttpStatusCode.OK, uAObj);
            }
            else
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Duplicate Doc. No.");
                response.Headers.Location = new Uri(Url.Link("DefaultApi", 0));
                return response;
            }
        }


        [HttpPost]
        [ActionName("Upload")]
        public System.Threading.Tasks.Task<HttpResponseMessage> Upload(string param)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();

            int count = 0;
            string listFailed = null;
            string listFailedDetail = null;

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    foreach (var stream in provider.Contents)
                    {
                        // Process The File
                        var type = Path.GetExtension(stream.Headers.ContentDisposition.FileName.Replace("\"", ""));

                        var fs = new MemoryStream(stream.ReadAsByteArrayAsync().Result);

                        var splitParam = param.Split('|');
                        int oid = int.Parse(splitParam[0]);
                        string updateBy = splitParam[1];
                        string docType = splitParam[2];

                        var returnVal = ProcessUploadPDListFile(fs, type, oid, updateBy, docType).Split('|');
                        count += int.Parse(returnVal[0]);

                        if (!string.IsNullOrEmpty(listFailed))
                        {
                            listFailed += ", " + returnVal[1];
                        }
                        else
                        {
                            listFailed += returnVal[1];
                        }

                        if (!string.IsNullOrEmpty(listFailedDetail))
                        {
                            listFailedDetail += "; " + returnVal[2];
                        }
                        else
                        {
                            listFailedDetail += returnVal[2];
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
                var aaa = listFailed.Split(',').Count();

                return Request.CreateResponse(HttpStatusCode.OK, new { success = count, Failed = (listFailed.Split(',').Count() - 1), listFailed = listFailed, listFailedDetail = listFailedDetail });
            });

            return task;
        }

        private string ProcessUploadPDListFile(Stream stream, string fileType, int oid, string uploadedBy, string docType)
        {
            IExcelDataReader excelReader;
            if (Path.GetExtension(fileType) == ".xls")
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            else
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            excelReader.IsFirstRowAsColumnNames = true;

            DataSet ds = excelReader.AsDataSet(true);

            int saveCount = 0;
            var dt = ds.Tables[0];
            string listErrorRow = null;
            string listDetailErrorRow = null;
            int nowYear = DateTime.UtcNow.Year;
            List<PROJECTDOCUMENT> newListPD = new List<PROJECTDOCUMENT>();
            foreach (DataRow row in dt.Rows)
            {
                if (string.IsNullOrEmpty(row["Doc No"].ToString()))
                {
                    continue;
                }

                DateTime fromDateValue;
                string inpDetailErrorRow = null;

                var cekReturn = true;

                DateTime? planStartDate = null;
                if (!DateTime.TryParse(row["Plan Start Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPDListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPDListFileMessage(inpDetailErrorRow, row[0].ToString(), "Plan Start Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    planStartDate = fromDateValue;
                }

                DateTime? planFinishDate = null;
                if (!DateTime.TryParse(row["Plan Finish Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPDListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPDListFileMessage(inpDetailErrorRow, row[0].ToString(), "Plan Finish Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    planFinishDate = fromDateValue;
                }

                DateTime? actualStartDate = null;
                if (!DateTime.TryParse(row["Actual Start Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPDListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPDListFileMessage(inpDetailErrorRow, row[0].ToString(), "Actual Start Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    actualStartDate = fromDateValue;
                }

                DateTime? actualFinishDate = null;
                if (!DateTime.TryParse(row["Actual Finish Date"].ToString(), out fromDateValue))
                {
                    if (cekReturn) listErrorRow = errorProcessUploadPDListFile(listErrorRow, row[0].ToString());
                    inpDetailErrorRow = errorProcessUploadPDListFileMessage(inpDetailErrorRow, row[0].ToString(), "Actual Finish Date is not valid");
                    cekReturn = false;
                }
                else
                {
                    actualFinishDate = fromDateValue;
                }

                int keyDeliverablesId =(int) row["KeyDeliverablesId"];
                var docNo = row["Doc No"].ToString();
                var docTitle = row["Doc Title"].ToString();
                var engineer = row["Engineer (Employee ID)"].ToString();
                var disciplineCode = row["DisciplineCode"].ToString();
                var statusCode = row["StatusCode"].ToString();
                var remark = row["Remark"].ToString();

                PROJECTDOCUMENT pd = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == oid && w.DOCNO == docNo && w.DOCTYPE == docType).FirstOrDefault();

                if (pd == null)
                {
                    pd = new PROJECTDOCUMENT();
                    pd.CREATEDDATE = DateTime.Now;
                    pd.CREATEDBY = uploadedBy;
                    pd.PROJECTID = oid;
                    pd.REVISION = 0;
                }
                else
                {
                    pd.UPDATEDDATE = DateTime.Now;
                    pd.UPDATEDBY = uploadedBy;

                    if (pd.PLANSTARTDATE != planStartDate || pd.PLANFINISHDATE != planFinishDate)
                        pd.REVISION = pd.REVISION + 1;
                }

                pd.DOCTYPE = docType;
                pd.KEYDELIVERABLESID = keyDeliverablesId;
                pd.DOCNO = docNo;
                pd.DOCTITLE = docTitle;
                pd.DISCIPLINE = disciplineCode;
                pd.DOCUMENTBYID = engineer;
                try
                {
                    employeeObject employee = ac.getEmployeeByBN(engineer);
                    pd.DOCUMENTBYNAME = employee.full_Name;
                    pd.DOCUMENTBYEMAIL = employee.email;
                }
                catch
                {
                }

                pd.PLANSTARTDATE = planStartDate;
                pd.PLANFINISHDATE = planFinishDate;
                pd.ACTUALSTARTDATE = actualStartDate;
                pd.ACTUALFINISHDATE = actualFinishDate;
                pd.STATUS = statusCode;
                pd.REMARK = remark;

                if (pd.OID > 0)
                {
                    db.Entry(pd).State = EntityState.Modified;
                }
                else
                {
                    var cekExists = newListPD.Where(w => w.DOCNO == pd.DOCNO);
                    if (cekExists.Count() > 0)
                    {
                        foreach (var cekExist in cekExists)
                        {
                            cekExist.DOCTITLE = pd.DOCTITLE;
                            cekExist.DISCIPLINE = pd.DISCIPLINE;
                            cekExist.DOCUMENTBYID = pd.DOCUMENTBYID;
                            cekExist.DOCUMENTBYNAME = pd.DOCUMENTBYNAME;
                            cekExist.DOCUMENTBYEMAIL = pd.DOCUMENTBYEMAIL;
                            cekExist.PLANSTARTDATE = pd.PLANSTARTDATE;
                            cekExist.PLANFINISHDATE = pd.PLANFINISHDATE;
                            cekExist.ACTUALSTARTDATE = pd.ACTUALSTARTDATE;
                            cekExist.ACTUALFINISHDATE = pd.ACTUALFINISHDATE;
                            cekExist.STATUS = pd.STATUS;
                            cekExist.REMARK = pd.REMARK;
                        }
                    }
                    else
                    {
                        newListPD.Add(pd);
                    }
                }
                ++saveCount;
            }

            db.PROJECTDOCUMENTs.AddRange(newListPD);
            db.SaveChanges();
            listErrorRow = saveCount.ToString() + "|" + listErrorRow + "|" + listDetailErrorRow;
            return listErrorRow;
        }

        [HttpPost]
        [ActionName("updateParticipationValue")]
        public object updateParticipationValue(int docId, float? participationValue)
        {
            var getProjDoc = db.PROJECTDOCUMENTs.Find(docId);
            getProjDoc.PARTICIPATIONVALUE = participationValue;
            db.Entry(getProjDoc).State = EntityState.Modified;
            db.SaveChanges();

            return Ok();
        }

        private string errorProcessUploadPDListFile(string listErrorRow, string numberOfError)
        {
            if (!string.IsNullOrEmpty(listErrorRow))
            {
                listErrorRow += "," + numberOfError;
            }
            else
            {
                listErrorRow += numberOfError;
            }

            return listErrorRow;
        }

        [HttpPost]
        [ActionName("UpdateRemarkProjectDocument")]
        public IHttpActionResult UpdateRemarkProjectDocument(string param, projectDocumentObject paramProjDoc) {
            try
            {
                PROJECTDOCUMENT projDoc = db.PROJECTDOCUMENTs.Find(paramProjDoc.id);

                if (param == "remarksActualResults")
                    projDoc.REMARKSACTUALRESULTS = paramProjDoc.remarksActualResults;
                else if (param == "remarksDesignKpi")
                    projDoc.REMARKSDESIGNKPI = paramProjDoc.remarksDesignKpi;
                else if (param == "all")
                {
                    projDoc.REMARKSACTUALRESULTS = paramProjDoc.remarksActualResults;
                    projDoc.REMARKSDESIGNKPI = paramProjDoc.remarksDesignKpi;
                }

                db.Entry(projDoc).State = EntityState.Modified;
                db.SaveChanges();

                return Ok(getCustomProjectDocumentByOid(projDoc.OID));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                throw;
            }
        }

        private string errorProcessUploadPDListFileMessage(string inpDetailErrorRow, string row, string Message)
        {
            if (!string.IsNullOrEmpty(inpDetailErrorRow))
            {
                inpDetailErrorRow += ", ";
            }
            else
            {
                inpDetailErrorRow = "row " + row + ": ";
            }

            return inpDetailErrorRow += Message;
        }
    }
}