﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterDefaultValueByYearController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/masterDefaultValueByYear
        public IQueryable<masterDefaultValueByYearObject> Get()
        {
            return db.MASTERDEFAULTVALUEBYYEARs.Select(s => new masterDefaultValueByYearObject
            {
                id = s.OID,
                year = s.YEAR,
                workingHours = s.WORKINGHOURS,
                publicHoliday = s.PUBLICHOLIDAY,
                saturdaySunday = s.SATURDAYSUNDAY
            });
        }

        // GET api/masterDefaultValueByYear/cekDuplicateYear/e.model.year
        [HttpGet]
        [ActionName("cekDuplicateYear")]
        public Boolean cekDuplicateYear(int param)
        {
            var duplicateYear = db.MASTERDEFAULTVALUEBYYEARs.Where(w => w.YEAR == param);
            if (duplicateYear.Count() > 0)
            {
                return true;
            }
            return false;
        }

        // POST api/masterDefaultValueByYear/addDefaultValue/1
        [HttpPost]
        [ActionName("addDefaultValue")]

        public IHttpActionResult addDefaultValue(string param, masterDefaultValueByYearObject defaultValueParam)
        {
            MASTERDEFAULTVALUEBYYEAR add = new MASTERDEFAULTVALUEBYYEAR();
            add.YEAR = defaultValueParam.year;
            add.WORKINGHOURS = defaultValueParam.workingHours;
            add.PUBLICHOLIDAY = defaultValueParam.publicHoliday;
            add.SATURDAYSUNDAY = defaultValueParam.saturdaySunday;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERDEFAULTVALUEBYYEARs.Add(add);
            db.SaveChanges();

            defaultValueParam.id = add.OID;
            return Ok(defaultValueParam);
        }

        // PUT api/masterDefaultValueByYear/updateDefaultValue/
        [HttpPut]
        [ActionName("updateDefaultValue")]

        public IHttpActionResult updateDefaultValue(int param, masterDefaultValueByYearObject defaultValueParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != defaultValueParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERDEFAULTVALUEBYYEAR update = db.MASTERDEFAULTVALUEBYYEARs.Find(param);
                update.YEAR = defaultValueParam.year;
                update.WORKINGHOURS = defaultValueParam.workingHours;
                update.PUBLICHOLIDAY = defaultValueParam.publicHoliday;
                update.SATURDAYSUNDAY = defaultValueParam.saturdaySunday;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(defaultValueParam);
            }

        // DELETE api/masterDefaultValueByYear
        [ResponseType(typeof(MASTERDEFAULTVALUEBYYEAR))]

        public IHttpActionResult DeleteMASTERDEFAULTVALUEBYYEAR(int id)
        {
            MASTERDEFAULTVALUEBYYEAR mASTERDEFAULTVALUEBYYEAR = db.MASTERDEFAULTVALUEBYYEARs.Find(id);
            if (mASTERDEFAULTVALUEBYYEAR == null)
            {
                return NotFound();
            }

            db.MASTERDEFAULTVALUEBYYEARs.Remove(mASTERDEFAULTVALUEBYYEAR);
            db.SaveChanges();

            return Ok(mASTERDEFAULTVALUEBYYEAR);
        }
    }
}