﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.Globalization;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ReportEngineeringServiceController : KAIZENController
    {

        [HttpGet]
        [ActionName("engineeringServicesReport")]
        public IEnumerable<engineeringServicesReport> engineeringServicesReport(int param)
        {
            var curryear = DateTime.Now;
            if (curryear.Year < param)
            {
                return new List<engineeringServicesReport>();
            }

            var months = Enumerable.Range(1, 12).Select(i => new { monthInt = i, monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
            var monthFactorOfYear = db.MASTERENGINEERINGSERVICESUPPORTs.Where(w => w.YEAR == param).ToList();
            var maximumTargetData = db.MASTERYEARLYTARGETSCOREs.FirstOrDefault(f => f.YEAR == param && f.CODE == constants.MAX);
            var maximumTarget = maximumTargetData == null ? 0 : maximumTargetData.VALUE;
            var forecastData = db.MASTERYEARLYTARGETSCOREs.FirstOrDefault(f => f.YEAR == param && f.CODE == constants.FORECAST);
            var forecast = forecastData == null ? 0 : forecastData.VALUE;
            var summaryTaskBaseline = db.GENERALPARAMETERs.FirstOrDefault(f => f.TITLE == constants.SummaryTaskBaseline);
            double summaryTaskBaselineValue = 0;
            if (!double.TryParse(summaryTaskBaseline.VALUE, out summaryTaskBaselineValue))
            {
                summaryTaskBaselineValue = 0;
            }

            List<int> ListDwgOnEwp = new List<int>();
            List<int> ListDwgOnDc = new List<int>();

            var getListDesignerBaseOnEwporDc = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param || w.ACTUALFINISHDATE.Value.Year == param) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN);
            foreach (PROJECTDOCUMENT pd in getListDesignerBaseOnEwporDc)
            {
                string documentGroup = GetDocumentGroupDesigner(pd);
                if (documentGroup == constants.DOCUMENTTYPE_ENGINEER)
                {
                    ListDwgOnEwp.Add(pd.OID);
                } else if (documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                {
                    ListDwgOnDc.Add(pd.OID);
                }
            }
            
            if (getListDesignerBaseOnEwporDc.Count() == 0)
            {
                return new List<engineeringServicesReport>();
            }


            #region get Document Ewp TotalWeight
            var getDataEwpTotalWeights = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param || w.ACTUALFINISHDATE.Value.Year == param) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).
                Select(s => new engineeringServicesReport
                {
                    year = param,
                    month = s.ACTUALFINISHDATE.HasValue ? s.ACTUALFINISHDATE.Value.Month : s.PLANFINISHDATE.Value.Month,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    docId = s.OID,
                    documentBy = s.DOCUMENTBYID,
                    ewpDeliverableWeightHour = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS)
                }).OrderBy(o => o.month).ToList();

            if (getDataEwpTotalWeights.Count() == 0)
            {
                return new List<engineeringServicesReport>();
            }

            foreach (engineeringServicesReport s in getDataEwpTotalWeights)
            {
                var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.documentBy);
                double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;
                double deliverableWeightPercent = baseLineHours > 0 ? ((s.ewpDeliverableWeightHour.HasValue ? s.ewpDeliverableWeightHour.Value : 0) / baseLineHours) * 100 : 0;

                //Month Factor
                s.ewpMonthFactor = monthFactorOfYear.FirstOrDefault(f => f.MONTH == s.month && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER) != null ? monthFactorOfYear.FirstOrDefault(f => f.MONTH == s.month && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).MONTHFACTOR != 0 ? monthFactorOfYear.FirstOrDefault(f => f.MONTH == s.month && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).MONTHFACTOR : 0 : 0;

                //•	Total Factored Weight
                //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                s.ewpTotalWeight = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                //•	Total Factored Weight / Month Factor
                s.ewpAvgWeightPlan = s.ewpMonthFactor != 0 ? s.ewpTotalWeight / s.ewpMonthFactor : 0;

                //•	Total Factored Weight / Month Factor
                s.ewpScorePlan = s.ewpAvgWeightPlan;

                //Final Result
                //•	Hanya dihitung jika Actual Finish Date sudah terisi
                //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                double actualFinalResultEWP = s.actualFinishDate.HasValue && s.planFinishDate.HasValue && s.planStartDate.HasValue ? (s.planFinishDate.Value - s.planStartDate.Value).TotalDays / (s.actualFinishDate.Value - s.planStartDate.Value).TotalDays : 0;

                //Performance
                //•	If[Final Result](x) < 0.8 then the performance value = 0 
                //•	If[Final Result](x) more than 0.8 and less than 1.0,
                //                then the performance value = 250 x – 150
                //•	If[Final Result](x) more than 1 and less than 1.05,
                //                then the performance value = 500 x – 400
                //•	If[Final Result](x) > 1.05 then the performance value = 125
                double finalResult2Decimal = Math.Floor(actualFinalResultEWP * 100) / 100;
                string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                    db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultEWP.ToString()) :
                                                    "";
                double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                //•	Achievement = [Factor Weight] * [Performance Value]
                s.ewpScoreActual = s.ewpMonthFactor != 0 ? (s.ewpTotalWeight * performance / 100) / s.ewpMonthFactor : 0;
            }

            var resultDataEwpTotalWeights = months.GroupJoin(getDataEwpTotalWeights.GroupBy(g => new { g.year, g.month }).Select(s => new
            {
                year = s.Key.year,
                month = s.Key.month,
                ewpTotalWeight = s.Sum(sum => sum.ewpTotalWeight),
                ewpAvgWeightPlan = s.Sum(sum => sum.ewpAvgWeightPlan),
                ewpScorePlan = s.Sum(sum => sum.ewpScorePlan),
                ewpScoreActual = s.Sum(sum => sum.ewpScoreActual),
                ewpRatioActual = s.Max(max => max.ewpRatioActual)
            }),
                m => m.monthInt, r => r.month,
                (m, r) => new { months = m, resultAll = r }).
                SelectMany(m => m.resultAll.DefaultIfEmpty(),
                        (m, rep) => new
                        {
                            year = param,
                            month = m.months.monthInt,
                            //plan
                            ewpMonthFactor = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).MONTHFACTOR,
                            ewpTotalWeight = rep == null ? 0 : rep.ewpTotalWeight,
                            ewpAvgWeightPlan = rep == null ? 0 : rep.ewpAvgWeightPlan,
                            ewpScorePlan = rep == null ? 0 : rep.ewpScorePlan,

                            //actual
                            ewpScoreActual = rep == null ? 0 : rep.ewpScoreActual,
                            ewpRatioActual = rep == null ? 0 : rep.ewpRatioActual,
                            ewpMinActual = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).MINTARGET,
                            ewpTargetActual = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).TARGET,
                            ewpMaxActual = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).MAXTARGET
                        });
            #endregion get Document Ewp TotalWeight

            #region get Document Ewp Plan Data
            var getDataEwpPlan = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.PLANFINISHDATE.Value.Year == param && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).
                Select(s => new
                {
                    year = param,
                    month = s.PLANFINISHDATE.Value.Month,
                    ewpCapPlan = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && s.PROJECT.PROJECTTYPE == constants.CAPITAL ? 1 : 0,
                    ewpOpPlan = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && s.PROJECT.PROJECTTYPE == constants.OPERATING ? 1 : 0
                }).GroupBy(g => new { g.year, g.month }).Select(s => new
                {
                    year = s.Key.year,
                    month = s.Key.month,
                    ewpCapPlan = s.Sum(sum => sum.ewpCapPlan),
                    ewpOpPlan = s.Sum(sum => sum.ewpOpPlan)
                }),
                m => m.monthInt, r => r.month,
                (m, r) => new { months = m, resultAll = r }).
                SelectMany(m => m.resultAll.DefaultIfEmpty(),
                        (m, rep) => new
                        {
                            year = param,
                            month = m.months.monthInt,
                            ewpCapPlan = rep == null ? 0 : rep.ewpCapPlan,
                            ewpOpPlan = rep == null ? 0 : rep.ewpOpPlan
                        });
            #endregion get Document Ewp Plan Data

            #region get Document Ewp Actual Data
            var getDataEwpActual = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.ACTUALFINISHDATE.Value.Year == param && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).
                Select(s => new
                {
                    year = param,
                    month = s.ACTUALFINISHDATE.Value.Month,
                    ewpCapActual = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && s.PROJECT.PROJECTTYPE == constants.CAPITAL ? 1 : 0,
                    ewpOpActual = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && s.PROJECT.PROJECTTYPE == constants.OPERATING ? 1 : 0
                }).GroupBy(g => new { g.year, g.month }).Select(s => new
                {
                    year = s.Key.year,
                    month = s.Key.month,
                    ewpCapActual = s.Sum(sum => sum.ewpCapActual),
                    ewpOpActual = s.Sum(sum => sum.ewpOpActual)
                }),
                m => m.monthInt, r => r.month,
                (m, r) => new { months = m, resultAll = r }).
                SelectMany(m => m.resultAll.DefaultIfEmpty(),
                        (m, rep) => new
                        {
                            year = param,
                            month = m.months.monthInt,
                            ewpCapActual = rep == null ? 0 : rep.ewpCapActual,
                            ewpOpActual = rep == null ? 0 : rep.ewpOpActual
                        });
            #endregion get Document Ewp Actual Data

            #region get Document DC TotalWeight
            var getDataDCTotalWeights = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param || w.ACTUALFINISHDATE.Value.Year == param) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).
                Select(s => new engineeringServicesReport
                {
                    year = param,
                    month = s.ACTUALFINISHDATE.HasValue ? s.ACTUALFINISHDATE.Value.Month : s.PLANFINISHDATE.Value.Month,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    docId = s.OID,
                    documentBy = s.DOCUMENTBYID,
                    dcDeliverableWeightHour = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS)
                }).OrderBy(o => o.month).ToList();

            foreach (engineeringServicesReport s in getDataDCTotalWeights)
            {
                var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.documentBy);
                double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;
                double deliverableWeightPercent = baseLineHours > 0 ? ((s.dcDeliverableWeightHour.HasValue ? s.dcDeliverableWeightHour.Value : 0) / baseLineHours) * 100 : 0;

                //Month Factor
                s.dcMonthFactor = monthFactorOfYear.FirstOrDefault(f => f.MONTH == s.month && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY) != null ? monthFactorOfYear.FirstOrDefault(f => f.MONTH == s.month && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).MONTHFACTOR != 0 ? monthFactorOfYear.FirstOrDefault(f => f.MONTH == s.month && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).MONTHFACTOR : 0 : 0;

                //•	Total Factored Weight
                //•	Hanya bisa dihitung jika data Drawing DC memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                s.dcTotalWeight = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                //•	Total Factored Weight / Month Factor
                s.dcAvgWeightPlan = s.dcMonthFactor != 0 ? s.dcTotalWeight / s.dcMonthFactor : 0;

                //•	Total Factored Weight / Month Factor
                s.dcScorePlan = s.dcAvgWeightPlan;

                //Final Result
                //•	Hanya dihitung jika Actual Finish Date sudah terisi
                //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                double actualFinalResultDC = s.actualFinishDate.HasValue && s.planFinishDate.HasValue && s.planStartDate.HasValue ? (s.planFinishDate.Value - s.planStartDate.Value).TotalDays / (s.actualFinishDate.Value - s.planStartDate.Value).TotalDays : 0;

                //Performance
                //•	If[Final Result](x) < 0.8 then the performance value = 0 
                //•	If[Final Result](x) more than 0.8 and less than 1.0,
                //                then the performance value = 250 x – 150
                //•	If[Final Result](x) more than 1 and less than 1.05,
                //                then the performance value = 500 x – 400
                //•	If[Final Result](x) > 1.05 then the performance value = 125
                double finalResult2Decimal = Math.Floor(actualFinalResultDC * 100) / 100;
                string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                    db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultDC.ToString()) :
                                                    "";
                double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                //•	Achievement = [Factor Weight] * [Performance Value]
                s.dcScoreActual = s.dcMonthFactor != 0 ? (s.dcTotalWeight * performance / 100) / s.dcMonthFactor : 0;
            }

            var resultDataDCTotalWeights = months.GroupJoin(getDataDCTotalWeights.GroupBy(g => new { g.year, g.month }).Select(s => new
            {
                year = s.Key.year,
                month = s.Key.month,
                dcTotalWeight = s.Sum(sum => sum.dcTotalWeight),
                dcAvgWeightPlan = s.Sum(sum => sum.dcAvgWeightPlan),
                dcScorePlan = s.Sum(sum => sum.dcScorePlan),
                dcScoreActual = s.Sum(sum => sum.dcScoreActual),
                dcRatioActual = s.Max(max => max.dcRatioActual)
            }),
                m => m.monthInt, r => r.month,
                (m, r) => new { months = m, resultAll = r }).
                SelectMany(m => m.resultAll.DefaultIfEmpty(),
                        (m, rep) => new
                        {
                            year = param,
                            month = m.months.monthInt,
                            //plan
                            dcMonthFactor = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).MONTHFACTOR,
                            dcTotalWeight = rep == null ? 0 : rep.dcTotalWeight,
                            dcAvgWeightPlan = rep == null ? 0 : rep.dcAvgWeightPlan,
                            dcScorePlan = rep == null ? 0 : rep.dcScorePlan,

                            //actual
                            dcScoreActual = rep == null ? 0 : rep.dcScoreActual,
                            dcRatioActual = rep == null ? 0 : rep.dcRatioActual,
                            dcMinActual = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).MINTARGET,
                            dcTargetActual = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).TARGET,
                            dcMaxActual = monthFactorOfYear.FirstOrDefault(f => f.MONTH == m.months.monthInt && f.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).MAXTARGET
                        });
            #endregion get Document DC TotalWeight

            #region get Document DC Plan Data
            var getDataDCPlan = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.PLANFINISHDATE.Value.Year == param && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).
                Select(s => new
                {
                    year = param,
                    month = s.PLANFINISHDATE.Value.Month,
                    dcCapPlan = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && s.PROJECT.PROJECTTYPE == constants.CAPITAL ? 1 : 0,
                    dcOpPlan = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && s.PROJECT.PROJECTTYPE == constants.OPERATING ? 1 : 0
                }).GroupBy(g => new { g.year, g.month }).Select(s => new
                {
                    year = s.Key.year,
                    month = s.Key.month,
                    dcCapPlan = s.Sum(sum => sum.dcCapPlan),
                    dcOpPlan = s.Sum(sum => sum.dcOpPlan)
                }),
                m => m.monthInt, r => r.month,
                (m, r) => new { months = m, resultAll = r }).
                SelectMany(m => m.resultAll.DefaultIfEmpty(),
                        (m, rep) => new
                        {
                            year = param,
                            month = m.months.monthInt,
                            dcCapPlan = rep == null ? 0 : rep.dcCapPlan,
                            dcOpPlan = rep == null ? 0 : rep.dcOpPlan
                        });
            #endregion get Document DC Plan Data

            #region get Document DC Actual Data
            var getDataDCActual = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.ACTUALFINISHDATE.Value.Year == param && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).
                Select(s => new
                {
                    year = param,
                    month = s.ACTUALFINISHDATE.Value.Month,
                    dcCapActual = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && s.PROJECT.PROJECTTYPE == constants.CAPITAL ? 1 : 0,
                    dcOpActual = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY && s.PROJECT.PROJECTTYPE == constants.OPERATING ? 1 : 0
                }).GroupBy(g => new { g.year, g.month }).Select(s => new
                {
                    year = s.Key.year,
                    month = s.Key.month,
                    dcCapActual = s.Sum(sum => sum.dcCapActual),
                    dcOpActual = s.Sum(sum => sum.dcOpActual)
                }),
                m => m.monthInt, r => r.month,
                (m, r) => new { months = m, resultAll = r }).
                SelectMany(m => m.resultAll.DefaultIfEmpty(),
                        (m, rep) => new
                        {
                            year = param,
                            month = m.months.monthInt,
                            dcCapActual = rep == null ? 0 : rep.dcCapActual,
                            dcOpActual = rep == null ? 0 : rep.dcOpActual
                        });
            #endregion get Document DC Actual Data

            double? totalPlanScoreEwp = resultDataEwpTotalWeights.Sum(sum => sum.ewpScorePlan);
            totalPlanScoreEwp = totalPlanScoreEwp.HasValue ? totalPlanScoreEwp.Value : 0;

            double? totalPlanScoreDc = resultDataDCTotalWeights.Sum(sum => sum.dcScorePlan);
            totalPlanScoreDc = totalPlanScoreDc.HasValue ? totalPlanScoreDc.Value : 0;

            var resultAllEWP = resultDataEwpTotalWeights.
                Join(getDataEwpPlan, gTotal => new { gTotal.year, gTotal.month }, gEwpPlan => new { gEwpPlan.year, gEwpPlan.month }, (gTotal, gEwpPlan) => new { gTotal, gEwpPlan }).
                Join(getDataEwpActual, gTotalJoinPlan => new { gTotalJoinPlan.gTotal.year, gTotalJoinPlan.gTotal.month }, gEwpActual => new { gEwpActual.year, gEwpActual.month }, (gTotalJoinPlan, gEwpActual) => new { gTotalJoinPlan, gEwpActual }).
              Select(s => new engineeringServicesReport
              {
                  year = s.gTotalJoinPlan.gTotal.year,
                  month = s.gTotalJoinPlan.gTotal.month,
                  ewpMonthFactor = s.gTotalJoinPlan.gTotal.ewpMonthFactor,
                  ewpCapPlan = s.gTotalJoinPlan.gEwpPlan.ewpCapPlan,
                  ewpOpPlan = s.gTotalJoinPlan.gEwpPlan.ewpOpPlan,
                  ewpCapActual = s.gEwpActual.ewpCapActual,
                  ewpOpActual = s.gEwpActual.ewpOpActual,
                  ewpTotalWeight = s.gTotalJoinPlan.gTotal.ewpTotalWeight,
                  ewpAvgWeightPlan = s.gTotalJoinPlan.gTotal.ewpAvgWeightPlan,
                  ewpScorePlan = s.gTotalJoinPlan.gTotal.ewpScorePlan,

                  ewpScoreActual = s.gTotalJoinPlan.gTotal.ewpScoreActual,
                  ewpRatioActual = s.gTotalJoinPlan.gTotal.ewpRatioActual,

                  ewpMinActual = s.gTotalJoinPlan.gTotal.ewpMinActual,
                  ewpTargetActual = s.gTotalJoinPlan.gTotal.ewpTargetActual,
                  ewpMaxActual = s.gTotalJoinPlan.gTotal.ewpMaxActual
              });

            var resultAllDC = resultDataDCTotalWeights.
                Join(getDataDCPlan, gTotal => new { gTotal.year, gTotal.month }, gDcPlan => new { gDcPlan.year, gDcPlan.month }, (gTotal, gDcPlan) => new { gTotal, gDcPlan }).
                Join(getDataDCActual, gTotalJoinPlan => new { gTotalJoinPlan.gTotal.year, gTotalJoinPlan.gTotal.month }, gDcActual => new { gDcActual.year, gDcActual.month }, (gTotalJoinPlan, gDcActual) => new { gTotalJoinPlan, gDcActual }).
              Select(s => new engineeringServicesReport
              {
                  year = s.gTotalJoinPlan.gTotal.year,
                  month = s.gTotalJoinPlan.gTotal.month,
                  dcMonthFactor = s.gTotalJoinPlan.gTotal.dcMonthFactor,
                  dcCapPlan = s.gTotalJoinPlan.gDcPlan.dcCapPlan,
                  dcOpPlan = s.gTotalJoinPlan.gDcPlan.dcOpPlan,
                  dcCapActual = s.gDcActual.dcCapActual,
                  dcOpActual = s.gDcActual.dcOpActual,
                  dcTotalWeight = s.gTotalJoinPlan.gTotal.dcTotalWeight,
                  dcAvgWeightPlan = s.gTotalJoinPlan.gTotal.dcAvgWeightPlan,
                  dcScorePlan = s.gTotalJoinPlan.gTotal.dcScorePlan,

                  dcScoreActual = s.gTotalJoinPlan.gTotal.dcScoreActual,
                  dcRatioActual = s.gTotalJoinPlan.gTotal.dcRatioActual,

                  dcMinActual = s.gTotalJoinPlan.gTotal.dcMinActual,
                  dcTargetActual = s.gTotalJoinPlan.gTotal.dcTargetActual,
                  dcMaxActual = s.gTotalJoinPlan.gTotal.dcMaxActual
              });

            var resultAll = resultAllEWP.
                Join(resultAllDC, gEwp => new { gEwp.year, gEwp.month }, gDc => new { gDc.year, gDc.month }, (gEwp, gDc) => new { gEwp, gDc }).Select(s => new engineeringServicesReport
                {
                    year = s.gEwp.year,
                    month = s.gEwp.month,
                    //EWP
                    ewpMonthFactor = s.gEwp.ewpMonthFactor,
                    ewpCapPlan = s.gEwp.ewpCapPlan,
                    ewpOpPlan = s.gEwp.ewpOpPlan,
                    ewpCapActual = s.gEwp.ewpCapActual,
                    ewpOpActual = s.gEwp.ewpOpActual,
                    ewpTotalWeight = s.gEwp.ewpTotalWeight,
                    ewpAvgWeightPlan = s.gEwp.ewpAvgWeightPlan,
                    ewpScorePlan = s.gEwp.ewpScorePlan,
                    ewpScoreActual = s.gEwp.ewpScoreActual,
                    ewpRatioActual = s.gEwp.ewpRatioActual,
                    ewpMinActual = s.gEwp.ewpMinActual,
                    ewpTargetActual = s.gEwp.ewpTargetActual,
                    ewpMaxActual = s.gEwp.ewpMaxActual,
                    
                    //DC
                    dcMonthFactor = s.gDc.dcMonthFactor,
                    dcCapPlan = s.gDc.dcCapPlan,
                    dcOpPlan = s.gDc.dcOpPlan,
                    dcCapActual = s.gDc.dcCapActual,
                    dcOpActual = s.gDc.dcOpActual,
                    dcTotalWeight = s.gDc.dcTotalWeight,
                    dcAvgWeightPlan = s.gDc.dcAvgWeightPlan,
                    dcScorePlan = s.gDc.dcScorePlan,
                    dcScoreActual = s.gDc.dcScoreActual,
                    dcRatioActual = s.gDc.dcRatioActual,
                    dcMinActual = s.gDc.dcMinActual,
                    dcTargetActual = s.gDc.dcTargetActual,
                    dcMaxActual = s.gDc.dcMaxActual
                }).OrderBy(o => o.month).ToList();

            int? ewpCapCummPlan = 0;
            int? ewpOpCummPlan = 0;
            int? ewpCapCummActual = 0;
            int? ewpOpCummActual = 0;
            double? ewpScoreCummPlan = 0;
            double? ewpNewScoreCummPlan = 0;
            double? ewpScoreCummActual = 0;
            double? ewpAdjScoreCummActual = 0;
            double? ewpCummMaxGraphic = 0;
            double? ewpCummForecastGraphic = 0;

            int? dcCapCummPlan = 0;
            int? dcOpCummPlan = 0;
            int? dcCapCummActual = 0;
            int? dcOpCummActual = 0;
            double? dcScoreCummPlan = 0;
            double? dcNewScoreCummPlan = 0;
            double? dcScoreCummActual = 0;
            double? dcAdjScoreCummActual = 0;
            double? dcCummMaxGraphic = 0;
            double? dcCummForecastGraphic = 0;

            //o[Adjusted plan to score 100 point] = Total Yearly[EWP Plan Score – Score] / Total Yearly[Summary Plan – Score DC + EWP]
            double? adjustedPlanToScore100PointEwp = 100 * totalPlanScoreEwp / (totalPlanScoreEwp + totalPlanScoreDc);
            double? adjustedPlanToScore100PointDc = 100 * totalPlanScoreDc / (totalPlanScoreEwp + totalPlanScoreDc);

            foreach (engineeringServicesReport s in resultAll)
            {
                s.monthName = months.FirstOrDefault(w => w.monthInt == s.month).monthName;

                //EWP
                //•	ewpCapCummPlan Cumm
                ewpCapCummPlan += s.ewpCapPlan;
                s.ewpCapCummPlan = ewpCapCummPlan;

                //•	ewpCapCummPlan Cumm
                ewpOpCummPlan += s.ewpOpPlan;
                s.ewpOpCummPlan = ewpOpCummPlan;

                //•	ewpCapCummActual Cumm
                ewpCapCummActual += s.ewpCapActual;
                s.ewpCapCummActual = ewpCapCummActual;

                //•	ewpOpCummActual Cumm
                ewpOpCummActual += s.ewpOpActual;
                s.ewpOpCummActual = ewpOpCummActual;

                //•	ewpScorePlan Cumm
                ewpScoreCummPlan += s.ewpScorePlan;
                s.ewpScoreCummPlan = ewpScoreCummPlan;


                //•	Plan Score – New Score: ([Plan Score – Score] / Total Yearly[Plan Score – Score]) * [Adjusted plan to score 100 point]
                //o[Adjusted plan to score 100 point] = Total Yearly[EWP Plan Score – Score] / Total Yearly[Summary Plan – Score DC + EWP]
                s.ewpNewScorePlan = totalPlanScoreEwp != 0 ? (s.ewpScorePlan / totalPlanScoreEwp) * adjustedPlanToScore100PointEwp : 0;

                //•	ewpScorePlan Cumm
                ewpNewScoreCummPlan += s.ewpNewScorePlan;
                s.ewpNewScoreCummPlan = ewpNewScoreCummPlan;

                //•	Actual Score – Cumm: Cummulative dari nilai [Actual Score – Score] 
                ewpScoreCummActual += s.ewpScoreActual;
                s.ewpScoreCummActual = ewpScoreCummActual;

                //•	Actual Score – Ratio: [Actual Score – Score]/ [Plan Score – Score]
                s.ewpRatioActual = s.ewpScorePlan != 0 ? (s.ewpScoreActual / s.ewpScorePlan) : 0;

                //•	adjScoreCummActual Cumm
                //•	Actual Score – Adj. Score: [Plan Score – New Score] * [Actual Score – Ratio]
                s.ewpAdjScoreActual = (s.ewpNewScorePlan) * (s.ewpRatioActual);

                //•	adjScoreCummActual Cumm
                ewpAdjScoreCummActual += s.ewpAdjScoreActual;
                s.ewpAdjScoreCummActual = ewpAdjScoreCummActual;

                //•	Actual Score – %: ([Actual EWP – CAP] + [Actual EWP – OP]) / ([Plan EWP – CAP] + [Plan EWP – OP])
                s.ewpPercentActual = (s.ewpCapPlan + s.ewpOpPlan) != 0 ? (double?)((double?)(s.ewpCapActual + s.ewpOpActual) / (double?)(s.ewpCapPlan + s.ewpOpPlan)) * 100 : 0;

                //•	Graphic – MAX: [Plan Score – New Score] * ([Maximum Target]/100)
                s.ewpMaxGraphic = s.ewpNewScorePlan * ((maximumTarget != null ? maximumTarget : 0) / 100);
                ewpCummMaxGraphic += s.ewpMaxGraphic;
                s.ewpCummMaxGraphic = ewpCummMaxGraphic;

                //•	Graphic – Forecast: [Plan Score – New Score] * ([Forecast]/100)
                s.ewpForecastGraphic = s.ewpNewScorePlan * ((forecast != null ? forecast : 0) / 100);
                ewpCummForecastGraphic += s.ewpForecastGraphic;
                s.ewpCummForecastGraphic = ewpCummForecastGraphic;

                //DC
                //•	ewpCapCummPlan Cumm
                dcCapCummPlan += s.dcCapPlan;
                s.dcCapCummPlan = dcCapCummPlan;

                //•	dcOpCummPlan Cumm
                dcOpCummPlan += s.dcOpPlan;
                s.dcOpCummPlan = dcOpCummPlan;

                //•	dcCapCummActual Cumm
                dcCapCummActual += s.dcCapActual;
                s.dcCapCummActual = dcCapCummActual;

                //•	dcOpCummActual Cumm
                dcOpCummActual += s.dcOpPlan;
                s.dcOpCummActual = dcOpCummActual;

                //•	dcScorePlan Cumm
                dcScoreCummPlan += s.dcScorePlan;
                s.dcScoreCummPlan = dcScoreCummPlan;

                //•	Plan Score – New Score: ([Plan Score – Score] / Total Yearly[Plan Score – Score]) * [Adjusted plan to score 100 point]
                //o[Adjusted plan to score 100 point] = Total Yearly[EWP Plan Score – Score] / Total Yearly[Summary Plan – Score DC + EWP]
                s.dcNewScorePlan = totalPlanScoreDc != 0 ? (s.dcScorePlan / totalPlanScoreDc) * adjustedPlanToScore100PointDc : 0;

                //•	dcScorePlan Cumm
                dcNewScoreCummPlan += s.dcNewScorePlan;
                s.dcNewScoreCummPlan = dcNewScoreCummPlan;

                //•	Actual Score – Cumm: Cummulative dari nilai [Actual Score – Score] 
                dcScoreCummActual += s.dcScoreActual;
                s.dcScoreCummActual = dcScoreCummActual;

                //•	Actual Score – Ratio: [Actual Score – Score]/ [Plan Score – Score]
                s.dcRatioActual = s.dcScorePlan != 0 ? (s.dcScoreActual / s.dcScorePlan) : 0;

                //•	adjScoreCummActual Cumm
                //•	Actual Score – Adj. Score: [Plan Score – New Score] * [Actual Score – Ratio]
                s.dcAdjScoreActual = (s.dcNewScorePlan) * (s.dcRatioActual);

                dcAdjScoreCummActual += s.dcAdjScoreActual;
                s.dcAdjScoreCummActual = dcAdjScoreCummActual;

                //•	Actual Score – %: ([Actual DC – CAP] + [Actual DC – OP]) / ([Plan DC – CAP] + [Plan DC – OP])
                s.dcPercentActual = (s.dcCapPlan + s.dcOpPlan) != 0 ? (double?)((s.dcCapActual + s.dcOpActual) / (double?)(s.dcCapPlan + s.dcOpPlan)) * 100 : 0;

                //•	Graphic – MAX: [Plan Score – New Score] * ([Maximum Target]/100)
                s.dcMaxGraphic = s.dcNewScorePlan * ((maximumTarget != null ? maximumTarget : 0) / 100);
                dcCummMaxGraphic += s.dcMaxGraphic;
                s.dcCummMaxGraphic = dcCummMaxGraphic;

                //•	Graphic – Forecast: [Plan Score – New Score] * ([Forecast]/100)
                s.dcForecastGraphic = s.dcNewScorePlan * ((forecast != null ? forecast : 0) / 100);
                dcCummForecastGraphic += s.dcForecastGraphic;
                s.dcCummForecastGraphic = dcCummForecastGraphic;

                //SUMMARY - SCORE
                s.summaryWeightEngPlan = s.ewpAvgWeightPlan + s.dcAvgWeightPlan;
                s.summaryScorePlan = s.ewpScorePlan + s.dcScorePlan;
                s.summaryScoreCummPlan = s.ewpScoreCummPlan + s.dcScoreCummPlan;
                s.summaryMaxPlan = s.ewpMaxGraphic + s.dcMaxGraphic;
                s.summaryMaxCummPlan = s.ewpCummMaxGraphic + s.dcCummMaxGraphic;
                s.summaryForecastPlan = s.ewpForecastGraphic + s.dcForecastGraphic;
                s.summaryForecastCummPlan = s.ewpCummForecastGraphic + s.dcCummForecastGraphic;

                s.summaryScoreActual = s.ewpScoreActual + s.dcScoreActual;
                s.summaryScoreCummActual = s.ewpScoreCummActual + s.dcScoreCummActual;
                s.summaryAdjScoreActual = s.ewpAdjScoreActual + s.dcAdjScoreActual;
                s.summaryAdjScoreCummActual = s.ewpAdjScoreCummActual + s.dcAdjScoreCummActual;

                //SUMMARY - GRAPHIC
                s.summaryPlanOfTask = s.ewpCapPlan + s.ewpOpPlan + s.dcCapPlan + s.dcOpPlan;
                s.summaryPlanCummOfTask = s.ewpCapCummPlan + s.ewpOpCummPlan + s.dcCapCummPlan + s.dcOpCummPlan;
                s.summaryCompletedOfTask = s.ewpCapActual + s.ewpOpActual + s.dcCapActual + s.dcOpActual;
                s.summaryCompletedCummOfTask = s.ewpCapCummActual + s.ewpOpCummActual + s.dcCapCummActual + s.dcOpCummActual;
                s.summaryPercentageOfTask = s.summaryPlanOfTask.HasValue && s.summaryPlanOfTask != 0 ? 100 * s.summaryCompletedOfTask / s.summaryPlanOfTask : 0;
                s.summaryLineOfTask = summaryTaskBaselineValue;
            }

            return resultAll.OrderBy(o => o.month);
        }

        [HttpGet]
        [ActionName("summaryReport")]
        public object summaryReport(int param, int month)
        {
            month = month + 1;
            List<int> ListDwgOnEwp = new List<int>();
            List<int> ListDwgOnDc = new List<int>();

            var getListDesignerBaseOnEwporDc = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param || w.ACTUALFINISHDATE.Value.Year == param) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN);
            foreach (PROJECTDOCUMENT pd in getListDesignerBaseOnEwporDc)
            {
                string documentGroup = GetDocumentGroupDesigner(pd);
                if (documentGroup == constants.DOCUMENTTYPE_ENGINEER)
                {
                    ListDwgOnEwp.Add(pd.OID);
                }
                else if (documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                {
                    ListDwgOnDc.Add(pd.OID);
                }
            }

            var listProjectType = db.PROJECTs.Select(s => new { projectType = s.PROJECTTYPE }).Distinct().ToList();

            #region Ewp
            var getDataEwpTotalProject = listProjectType.GroupJoin(db.PROJECTs.Where(x => x.PROJECTDOCUMENTs.Where(w => (w.PLANFINISHDATE.Value.Year == param || w.ACTUALFINISHDATE.Value.Year == param) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).Count() > 0).GroupBy(g => g.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countEwpProject = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countEwpProject = data == null ? 0 : data.countEwpProject
                    });

            var getDataEwpPlan = listProjectType.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).GroupBy(g => g.PROJECT.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countEwpPlan = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countEwpPlan = data == null ? 0 : data.countEwpPlan
                    });

            var getDataEwpPlanByMonth = listProjectType.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param && w.PLANFINISHDATE.Value.Month <= month) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).GroupBy(g => g.PROJECT.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countEwpPlanByMonth = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countEwpPlanByMonth = data == null ? 0 : data.countEwpPlanByMonth
                    }).ToList();

            var getDataEwpActualByMonth = listProjectType.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.ACTUALFINISHDATE.Value.Year == param && w.ACTUALFINISHDATE.Value.Month <= month) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnEwp.Contains(w.OID)))).GroupBy(g => g.PROJECT.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countEwpActualByMonth = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countEwpActualByMonth = data == null ? 0 : data.countEwpActualByMonth
                    });
            #endregion EWP

            #region DC
            var getDataDcTotalProject = listProjectType.GroupJoin(db.PROJECTs.Where(x => x.PROJECTDOCUMENTs.Where(w => (w.PLANFINISHDATE.Value.Year == param || w.ACTUALFINISHDATE.Value.Year == param) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).Count() > 0).GroupBy(g => g.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countDcProject = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countDcProject = data == null ? 0 : data.countDcProject
                    });

            var getDataDcPlan = listProjectType.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).GroupBy(g => g.PROJECT.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countDcPlan = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countDcPlan = data == null ? 0 : data.countDcPlan
                    });

            var getDataDcPlanByMonth = listProjectType.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.Value.Year == param && w.PLANFINISHDATE.Value.Month <= month) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).GroupBy(g => g.PROJECT.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countDcPlanByMonth = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countDcPlanByMonth = data == null ? 0 : data.countDcPlanByMonth
                    });

            var getDataDcActualByMonth = listProjectType.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.ACTUALFINISHDATE.Value.Year == param && w.ACTUALFINISHDATE.Value.Month <= month) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY || (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && ListDwgOnDc.Contains(w.OID)))).GroupBy(g => g.PROJECT.PROJECTTYPE).Select(s => new
            {
                projectType = s.Key,
                countDcActualByMonth = s.Count()
            }),
            o => o.projectType, j => j.projectType,
            (o, j) => new { projType = o, data = j }).
            SelectMany(m => m.data.DefaultIfEmpty(),
                    (m, data) => new
                    {
                        projectType = m.projType.projectType,
                        countDcActualByMonth = data == null ? 0 : data.countDcActualByMonth
                    });
            #endregion DC

            var resultEwp = getDataEwpTotalProject.
                Join(getDataEwpPlan, gEwpProject => gEwpProject.projectType, gEwpPlan => gEwpPlan.projectType, (gEwpProject, gEwpPlan) => new { gEwpProject, gEwpPlan }).
                Join(getDataEwpPlanByMonth, gEwpProjectPlan => gEwpProjectPlan.gEwpPlan.projectType, gEwpPlanByMonth => gEwpPlanByMonth.projectType, (gEwpProjectPlan, gEwpPlanByMonth) => new { gEwpProjectPlan, gEwpPlanByMonth }).
                Join(getDataEwpActualByMonth, getDataEwpProjectPlanByMonth => getDataEwpProjectPlanByMonth.gEwpPlanByMonth.projectType, gEwpActualByMonth => gEwpActualByMonth.projectType, (getDataEwpProjectPlanByMonth, gEwpActualByMonth) => new { getDataEwpProjectPlanByMonth, gEwpActualByMonth }).
                Select(s => new
                {
                    projectType = s.gEwpActualByMonth.projectType,
                    ewpNumberOfProject = s.getDataEwpProjectPlanByMonth.gEwpProjectPlan.gEwpProject.countEwpProject,
                    ewpNumberOfPlan = s.getDataEwpProjectPlanByMonth.gEwpProjectPlan.gEwpPlan.countEwpPlan,
                    ewpNumberOfPlanByMonth = s.getDataEwpProjectPlanByMonth.gEwpPlanByMonth.countEwpPlanByMonth,
                    ewpNumberOfActualByMonth = s.gEwpActualByMonth.countEwpActualByMonth
                });

            var resultDc = getDataDcTotalProject.
                Join(getDataDcPlan, gDcProject => gDcProject.projectType, gDcPlan => gDcPlan.projectType, (gDcProject, gDcPlan) => new { gDcProject, gDcPlan }).
                Join(getDataDcPlanByMonth, gDcProjectPlan => gDcProjectPlan.gDcPlan.projectType, gDcPlanByMonth => gDcPlanByMonth.projectType, (gDcProjectPlan, gDcPlanByMonth) => new { gDcProjectPlan, gDcPlanByMonth }).
                Join(getDataDcActualByMonth, getDataDcProjectPlanByMonth => getDataDcProjectPlanByMonth.gDcPlanByMonth.projectType, gDcActualByMonth => gDcActualByMonth.projectType, (getDataDcProjectPlanByMonth, gDcActualByMonth) => new { getDataDcProjectPlanByMonth, gDcActualByMonth }).
                Select(s => new
                {
                    projectType = s.gDcActualByMonth.projectType,
                    dcNumberOfProject = s.getDataDcProjectPlanByMonth.gDcProjectPlan.gDcProject.countDcProject,
                    dcNumberOfPlan = s.getDataDcProjectPlanByMonth.gDcProjectPlan.gDcPlan.countDcPlan,
                    dcNumberOfPlanByMonth = s.getDataDcProjectPlanByMonth.gDcPlanByMonth.countDcPlanByMonth,
                    dcNumberOfActualByMonth = s.gDcActualByMonth.countDcActualByMonth
                });

            var resultJoin = resultEwp.Join(resultDc, ewp => ewp.projectType, dc => dc.projectType, (ewp, dc) => new { ewp, dc }).ToList().Select(s => new
            {
                projectType = s.ewp.projectType,
                ewpNumberOfProject = s.ewp.ewpNumberOfProject,
                ewpNumberOfPlan = s.ewp.ewpNumberOfPlan,
                ewpNumberOfPlanByMonth = s.ewp.ewpNumberOfPlanByMonth,
                ewpNumberOfActualByMonth = s.ewp.ewpNumberOfActualByMonth,
                ewpPercentageCompleteByMonth = (double)s.ewp.ewpNumberOfPlanByMonth != 0 ? 100 * (double)s.ewp.ewpNumberOfActualByMonth / (double)s.ewp.ewpNumberOfPlanByMonth : 0,
                ewpPercentageComplete = (double)s.ewp.ewpNumberOfPlan != 0 ? 100 * (double)s.ewp.ewpNumberOfActualByMonth / (double)s.ewp.ewpNumberOfPlan : 0,
                dcNumberOfProject = s.dc.dcNumberOfProject,
                dcNumberOfPlan = s.dc.dcNumberOfPlan,
                dcNumberOfPlanByMonth = s.dc.dcNumberOfPlanByMonth,
                dcNumberOfActualByMonth = s.dc.dcNumberOfActualByMonth,
                dcPercentageCompleteByMonth = (double)s.dc.dcNumberOfPlanByMonth != 0 ? 100 * (double)s.dc.dcNumberOfActualByMonth / (double)s.dc.dcNumberOfPlanByMonth : 0,
                dcPercentageComplete = (double)s.dc.dcNumberOfPlan != 0 ? 100 * (double)s.dc.dcNumberOfActualByMonth / (double)s.dc.dcNumberOfPlan : 0
            });

            if (resultJoin.Count() > 0)
            {
                return resultJoin;
            }
            else
            {
                return new List<int>();
            }
        }
    }
}

public partial class engineeringServicesReport
{
    public int year { get; set; }
    public int month { get; set; }
    public string monthName { get; set; }
    public int docId { get; set; }
    public DateTime? planStartDate { get; set; }
    public DateTime? planFinishDate { get; set; }
    public DateTime? actualStartDate { get; set; }
    public DateTime? actualFinishDate { get; set; }
    public string documentBy { get; set; }
    //EWP COMPLIANCE
    public double? ewpMonthFactor { get; set; }
    public int? ewpCapPlan { get; set; }
    public int? ewpCapCummPlan { get; set; }
    public int? ewpOpPlan { get; set; }
    public int? ewpOpCummPlan { get; set; }
    public int? ewpCapActual { get; set; }
    public int? ewpCapCummActual { get; set; }
    public int? ewpOpActual { get; set; }
    public int? ewpOpCummActual { get; set; }
    public double? ewpDeliverableWeightHour { get; set; }
    public double? ewpTotalWeight { get; set; }
    public double? ewpAvgWeightPlan { get; set; }
    public double? ewpScorePlan { get; set; }
    public double? ewpScoreCummPlan { get; set; }
    public double? ewpNewScorePlan { get; set; }
    public double? ewpNewScoreCummPlan { get; set; }
    public double? ewpScoreActual { get; set; }
    public double? ewpScoreCummActual { get; set; }
    public double? ewpRatioActual { get; set; }
    public double? ewpAdjScoreActual { get; set; }
    public double? ewpAdjScoreCummActual { get; set; }
    public double? ewpPercentActual { get; set; }
    public double? ewpMinActual { get; set; }
    public double? ewpTargetActual { get; set; }
    public double? ewpMaxActual { get; set; }
    public double? ewpMaxGraphic { get; set; }
    public double? ewpCummMaxGraphic { get; set; }
    public double? ewpForecastGraphic { get; set; }
    public double? ewpCummForecastGraphic { get; set; }
    //DESIGN CONFORMITY
    public double? dcMonthFactor { get; set; }
    public int? dcCapPlan { get; set; }
    public int? dcCapCummPlan { get; set; }
    public int? dcOpPlan { get; set; }
    public int? dcOpCummPlan { get; set; }
    public int? dcCapActual { get; set; }
    public int? dcCapCummActual { get; set; }
    public int? dcOpActual { get; set; }
    public int? dcOpCummActual { get; set; }
    public int? dcCarryOver { get; set; }
    public double? dcDeliverableWeightHour { get; set; }
    public double? dcTotalWeight { get; set; }
    public double? dcAvgWeightPlan { get; set; }
    public double? dcScorePlan { get; set; }
    public double? dcScoreCummPlan { get; set; }
    public double? dcNewScorePlan { get; set; }
    public double? dcNewScoreCummPlan { get; set; }
    public double? dcScoreActual { get; set; }
    public double? dcScoreCummActual { get; set; }
    public double? dcRatioActual { get; set; }
    public double? dcAdjScoreActual { get; set; }
    public double? dcAdjScoreCummActual { get; set; }
    public double? dcPercentActual { get; set; }
    public double? dcMinActual { get; set; }
    public double? dcTargetActual { get; set; }
    public double? dcMaxActual { get; set; }
    public double? dcMaxGraphic { get; set; }
    public double? dcCummMaxGraphic { get; set; }
    public double? dcForecastGraphic { get; set; }
    public double? dcCummForecastGraphic { get; set; }
    //SUMMARY - SCORE
    public double? summaryWeightEngPlan { get; set; }
    public double? summaryScorePlan { get; set; }
    public double? summaryScoreCummPlan { get; set; }
    public double? summaryMaxPlan { get; set; }
    public double? summaryMaxCummPlan { get; set; }
    public double? summaryForecastPlan { get; set; }
    public double? summaryForecastCummPlan { get; set; }
    public double? summaryScoreActual { get; set; }
    public double? summaryScoreCummActual { get; set; }
    public double? summaryAdjScoreActual { get; set; }
    public double? summaryAdjScoreCummActual { get; set; }
    //SUMMARY - GRAPHIC
    public double? summaryPlanOfTask { get; set; }
    public double? summaryPlanCummOfTask { get; set; }
    public double? summaryCompletedOfTask { get; set; }
    public double? summaryCompletedCummOfTask { get; set; }
    public double? summaryPercentageOfTask { get; set; }
    public double? summaryLineOfTask { get; set; }
}