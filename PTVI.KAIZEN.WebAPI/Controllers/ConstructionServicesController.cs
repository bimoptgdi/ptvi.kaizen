﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ConstructionServicesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/ConstructionServices
        public IQueryable<CONSTRUCTIONSERVICE> GetCONSTRUCTIONSERVICEs()
        {
            return db.CONSTRUCTIONSERVICEs;
        }

        // GET: api/ConstructionServices/5
        [ResponseType(typeof(CONSTRUCTIONSERVICE))]
        public IHttpActionResult GetCONSTRUCTIONSERVICE(int id)
        {
            CONSTRUCTIONSERVICE cONSTRUCTIONSERVICE = db.CONSTRUCTIONSERVICEs.Find(id);
            if (cONSTRUCTIONSERVICE == null)
            {
                return NotFound();
            }

            return Ok(cONSTRUCTIONSERVICE);
        }

        // PUT: api/ConstructionServices/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCONSTRUCTIONSERVICE(int id, CONSTRUCTIONSERVICE cONSTRUCTIONSERVICE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cONSTRUCTIONSERVICE.OID)
            {
                return BadRequest();
            }

            db.Entry(cONSTRUCTIONSERVICE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CONSTRUCTIONSERVICEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ConstructionServices
        [ResponseType(typeof(CONSTRUCTIONSERVICE))]
        public IHttpActionResult PostCONSTRUCTIONSERVICE(CONSTRUCTIONSERVICE cONSTRUCTIONSERVICE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CONSTRUCTIONSERVICEs.Add(cONSTRUCTIONSERVICE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cONSTRUCTIONSERVICE.OID }, cONSTRUCTIONSERVICE);
        }

        // DELETE: api/ConstructionServices/5
        [ResponseType(typeof(CONSTRUCTIONSERVICE))]
        public IHttpActionResult DeleteCONSTRUCTIONSERVICE(int id)
        {
            CONSTRUCTIONSERVICE cONSTRUCTIONSERVICE = db.CONSTRUCTIONSERVICEs.Find(id);
            if (cONSTRUCTIONSERVICE == null)
            {
                return NotFound();
            }

            db.CONSTRUCTIONSERVICEs.Remove(cONSTRUCTIONSERVICE);
            db.SaveChanges();

            return Ok(cONSTRUCTIONSERVICE);
        }

        [HttpGet]
        [ActionName("csByProjectNo")]
        public IEnumerable<constructionServiceObject> csByProjectNo(int param)
        {
            var cs = db.CONSTRUCTIONSERVICEs.
                Where(w => w.PROJECTID == param && w.EXECUTOR != null && w.PROJECTDOCUMENT.EXECUTOR == constants.EXECUTOR_CS).
                GroupJoin(db.EXECUTORCONSTRUCTIONSERVICEs, c => c.EXECUTOR, e => e.CODE, (c, e) => new { cs = c, exec = e }).
                ToList().SelectMany(
                temp => temp.exec.Select(e=> new executorConstructionServiceObject { code = e.CODE, employeeId = e.EMPLOYEEID, name = e.NAME, email = e.EMAIL }).DefaultIfEmpty(),
                (x,
                y) => new constructionServiceObject
                {
                    id = x.cs.OID,
                    projectId = x.cs.PROJECTID,
                    projectDocumentId = x.cs.PROJECTDOCUMENTID,
                    projectNo = x.cs.PROJECTNO,
                    ewpNo = x.cs.EWPNO,
                    ewpTitle = x.cs.PROJECTDOCUMENT != null ? x.cs.PROJECTDOCUMENT.DOCTITLE : "-",
                    projectManager = x.cs.PROJECT != null ? x.cs.PROJECT.PROJECTMANAGERNAME : "-",
                    engineer = x.cs.PROJECTDOCUMENT != null ? x.cs.PROJECTDOCUMENT.DOCUMENTBYNAME : "-",
                    pmoNo = x.cs.PMONO,
                    rbd = x.cs.RBD,
                    status = x.cs.STATUS,
                    tenderId = x.cs.PROJECTDOCUMENT != null ? x.cs.PROJECTDOCUMENT.TENDERID : null,
                    planStartDate = x.cs.PLANSTARTDATE,
                    estimationDays = x.cs.ESTIMATIONDAYS,
                    planFinishDate = x.cs.PLANFINISHDATE,
                    actualStartDate = x.cs.ACTUALSTARTDATE,
                    actualDays = x.cs.ACTUALDAYS,
                    actualFinishDate = x.cs.ACTUALFINISHDATE,
                    estimationCost = x.cs.ESTIMATIONCOST,
                    actualCost = x.cs.ACTUALCOST,
                    executor = y,
                    trafficStatus = x.cs.PLANFINISHDATE.HasValue ? traficStatus(x.cs.PLANFINISHDATE.Value, null, x.cs.STATUS) : null
                });

            var result = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param && w.EXECUTOR == constants.EXECUTOR_CS && w.ACTUALSTARTDATE.HasValue && w.ACTUALFINISHDATE.HasValue && w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED).ToList().
                GroupJoin(cs, p => p.OID, c => c.projectDocumentId, (p, c) => new { pd = p, cs = c }).SelectMany(
                temp => temp.cs.DefaultIfEmpty(),
                (x,
                temp) => new constructionServiceObject
                {
                    id = x.pd.OID,
                    projectId = x.pd.PROJECTID,
                    projectNo = x.pd.PROJECT.PROJECTNO,
                    ewpNo = x.pd.DOCNO,
                    ewpTitle = x.pd.DOCTITLE,
                    projectManager = x.pd.PROJECT.PROJECTMANAGERNAME,
                    engineer = x.pd.DOCUMENTBYNAME,
                    pmoNo = temp != null ? temp.pmoNo : "-",
                    rbd = temp != null ? temp.rbd : null,
                    status = temp != null ? temp.status : "-",
                    tenderId = temp != null ? temp.tenderId : null,
                    planStartDate = temp != null ? temp.planStartDate : null,
                    estimationDays = temp != null ? temp.estimationDays : null,
                    planFinishDate = temp != null ? temp.planFinishDate : null,
                    actualStartDate = temp != null ? temp.actualStartDate : null,
                    actualDays = temp != null ? temp.actualDays : null,
                    actualFinishDate = temp != null ? temp.actualFinishDate : null,
                    estimationCost = temp != null ? temp.estimationCost : null,
                    actualCost = temp != null ? temp.actualCost : null,
                    executor = temp != null ? temp.executor : null,
                    trafficStatus = temp != null ? temp.trafficStatus : null
                });
            return result;
        }

        [HttpGet]
        [ActionName("cswithOutEwp")]
        public IEnumerable<constructionServiceObject> cswithOutEwp(int param)
        {
             return db.CONSTRUCTIONSERVICEs.
                Where(w => w.PROJECTID == param && w.EXECUTOR != null  && !w.PROJECTDOCUMENTID.HasValue).
                GroupJoin(db.EXECUTORCONSTRUCTIONSERVICEs, c => c.EXECUTOR, e => e.CODE, (c, e) => new { cs = c, exec = e }).
                ToList().SelectMany(
                temp => temp.exec.Select(e => new executorConstructionServiceObject { code = e.CODE, employeeId = e.EMPLOYEEID, name = e.NAME, email = e.EMAIL }).DefaultIfEmpty(),
                (x,
                y) => new constructionServiceObject
                {
                    id = x.cs.OID,
                    projectId = x.cs.PROJECTID,
                    projectDocumentId = x.cs.PROJECTDOCUMENTID,
                    projectNo = x.cs.PROJECTNO,
                    ewpNo = x.cs.EWPNO,
                    ewpTitle = x.cs.PROJECTDOCUMENT != null ? x.cs.PROJECTDOCUMENT.DOCTITLE : "-",
                    projectManager = x.cs.PROJECT != null ? x.cs.PROJECT.PROJECTMANAGERNAME : "-",
                    engineer = x.cs.PROJECTDOCUMENT != null ? x.cs.PROJECTDOCUMENT.DOCUMENTBYNAME : "-",
                    pmoNo = x.cs.PMONO,
                    rbd = x.cs.RBD,
                    status = x.cs.STATUS,
                    tenderId = x.cs.PROJECTDOCUMENT != null ? x.cs.PROJECTDOCUMENT.TENDERID : null,
                    planStartDate = x.cs.PLANSTARTDATE,
                    estimationDays = x.cs.ESTIMATIONDAYS,
                    planFinishDate = x.cs.PLANFINISHDATE,
                    actualStartDate = x.cs.ACTUALSTARTDATE,
                    actualDays = x.cs.ACTUALDAYS,
                    actualFinishDate = x.cs.ACTUALFINISHDATE,
                    estimationCost = x.cs.ESTIMATIONCOST,
                    actualCost = x.cs.ACTUALCOST,
                    executor = y,
                    trafficStatus = x.cs.PLANFINISHDATE.HasValue ? traficStatus(x.cs.PLANFINISHDATE.Value, null, x.cs.STATUS) : null
                });

          
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CONSTRUCTIONSERVICEExists(int id)
        {
            return db.CONSTRUCTIONSERVICEs.Count(e => e.OID == id) > 0;
        }
    }
}