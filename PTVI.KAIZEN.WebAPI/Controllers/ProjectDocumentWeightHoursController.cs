﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectDocumentWeightHoursController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/ProjectDocumentWeightHours
        public IQueryable<PROJECTDOCUMENTWEIGHTHOUR> GetPROJECTDOCUMENTWEIGHTHOURS()
        {
            return db.PROJECTDOCUMENTWEIGHTHOURS;
        }

        // GET: api/ProjectDocumentWeightHours/5
        [ResponseType(typeof(PROJECTDOCUMENTWEIGHTHOUR))]
        public IHttpActionResult GetPROJECTDOCUMENTWEIGHTHOUR(int id)
        {
            PROJECTDOCUMENTWEIGHTHOUR pROJECTDOCUMENTWEIGHTHOUR = db.PROJECTDOCUMENTWEIGHTHOURS.Find(id);
            if (pROJECTDOCUMENTWEIGHTHOUR == null)
            {
                return NotFound();
            }

            return Ok(pROJECTDOCUMENTWEIGHTHOUR);
        }

        // PUT: api/ProjectDocumentWeightHours/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROJECTDOCUMENTWEIGHTHOUR(int id, PROJECTDOCUMENTWEIGHTHOUR pROJECTDOCUMENTWEIGHTHOUR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTDOCUMENTWEIGHTHOUR.OID)
            {
                return BadRequest();
            }

            db.Entry(pROJECTDOCUMENTWEIGHTHOUR).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTWEIGHTHOURExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjectDocumentWeightHours
        [ResponseType(typeof(PROJECTDOCUMENTWEIGHTHOUR))]
        public IHttpActionResult PostPROJECTDOCUMENTWEIGHTHOUR(PROJECTDOCUMENTWEIGHTHOUR pROJECTDOCUMENTWEIGHTHOUR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROJECTDOCUMENTWEIGHTHOURS.Add(pROJECTDOCUMENTWEIGHTHOUR);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pROJECTDOCUMENTWEIGHTHOUR.OID }, pROJECTDOCUMENTWEIGHTHOUR);
        }

        [HttpGet]
        [ActionName("getByDocIdWithMasterWeight")]
        public IEnumerable<projectDocumentWeightHoursObject> getByDocIdWithMasterWeight(int param, string engineerType)
        {
            var getWeightByEwp = db.PROJECTDOCUMENTWEIGHTHOURS.Where(w => w.PROJECTDOCUMENTID == param).Select(s => new
            {
                oid = s.OID,
                projectDocumentId = s.PROJECTDOCUMENTID.Value,
                masterWeightHoursId = s.MASTERWEIGHTHOURSID.Value,
                masterWeightHoursDesc = s.MASTERWEIGHTHOURSDESC,
                estimateHours = s.ESTIMATEHOURS,
                actualHours = s.ACTUALHOURS,
                seq = s.SEQ,
                parentId = s.PARENTID,
                isEditable = s.ISEDITABLE,
                isParentGroup = s.ISPARENTGROUP
            });

            var masterWeight = db.MASTERWEIGHTHOURS.Where(w => w.WEIGHTHOURSTYPE == engineerType && !getWeightByEwp.Select(s => s.masterWeightHoursId).Contains(w.OID)).Select(s => new
            {
                oid = -1,
                projectDocumentId = param,
                masterWeightHoursId = s.OID,
                masterWeightHoursDesc = s.DESCRIPTION,
                estimateHours = s.ESTIMATEHOURS,
                actualHours = (double?)0,
                seq = s.SEQ,
                parentId = s.PARENTID,
                isEditable = s.ISEDITABLE,
                isParentGroup = s.ISPARENTGROUP
            });
            return getWeightByEwp.Union(masterWeight).OrderBy(o => o.seq).Select(s => new projectDocumentWeightHoursObject
            {
                oid = s.oid,
                projectDocumentId = s.projectDocumentId,
                masterWeightHoursId = s.masterWeightHoursId,
                masterWeightHoursDesc = s.masterWeightHoursDesc,
                estimateHours = s.estimateHours,
                actualHours = s.actualHours,
                seq = s.seq,
                parentId = s.parentId,
                isEditable = s.isEditable,
                isParentGroup = s.isParentGroup
            });
        }


        [HttpGet]
        [ActionName("totalWeightByDocID")]
        public double totalWeightByDocID (int param) {
            var totalHours = db.PROJECTDOCUMENTWEIGHTHOURS.Where(w => w.PROJECTDOCUMENTID == param).Sum(s => s.ACTUALHOURS);
            return totalHours.HasValue ? totalHours.Value : 0;
        }

        [HttpPut]
        [ActionName("updateByMasterWeightHoursIdId")]
        public IHttpActionResult updateByMasterWeightHoursIdId(int param, projectDocumentWeightHoursObject paramWeight)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROJECTDOCUMENTWEIGHTHOUR weight = new PROJECTDOCUMENTWEIGHTHOUR();
            if (param > 0)
            {
                if (paramWeight.oid > 0)
                {
                    weight = db.PROJECTDOCUMENTWEIGHTHOURS.Find(paramWeight.oid);
                    weight.MASTERWEIGHTHOURSDESC = paramWeight.masterWeightHoursDesc;
                    weight.ACTUALHOURS = paramWeight.actualHours;
                    weight.UPDATEDBY = GetCurrentNTUserId();
                    weight.UPDATEDDATE = DateTime.Now;
                    db.Entry(weight).State = EntityState.Modified;
                }
                else
                {
                    weight = new PROJECTDOCUMENTWEIGHTHOUR();
                    weight.PROJECTDOCUMENTID = paramWeight.projectDocumentId;
                    weight.MASTERWEIGHTHOURSID = paramWeight.masterWeightHoursId;
                    weight.MASTERWEIGHTHOURSDESC = paramWeight.masterWeightHoursDesc;
                    weight.ESTIMATEHOURS = paramWeight.estimateHours;
                    weight.ACTUALHOURS = paramWeight.actualHours;
                    weight.SEQ = paramWeight.seq;
                    weight.PARENTID = paramWeight.parentId;
                    weight.ISEDITABLE = paramWeight.isEditable;
                    weight.ISPARENTGROUP = paramWeight.isParentGroup;
                    weight.CREATEDBY = GetCurrentNTUserId();
                    weight.CREATEDDATE = DateTime.Now;
                    db.Entry(weight).State = EntityState.Added;
                }
            }
            else
            {
                return BadRequest();
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTWEIGHTHOURExists(paramWeight.oid))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //if (paramWeight.oid > 0)
            //{
            //    return StatusCode(HttpStatusCode.NoContent);

            //}
            //else
            //{
            paramWeight.oid = weight.OID;
            return Ok(paramWeight);

            //    return CreatedAtRoute("DefaultApi", new { masterWeightHoursId = paramWeight.masterWeightHoursId }, paramWeight);
            //}
        }


        // DELETE: api/ProjectDocumentWeightHours/5
        [ResponseType(typeof(PROJECTDOCUMENTWEIGHTHOUR))]
        public IHttpActionResult DeletePROJECTDOCUMENTWEIGHTHOUR(int id)
        {
            PROJECTDOCUMENTWEIGHTHOUR pROJECTDOCUMENTWEIGHTHOUR = db.PROJECTDOCUMENTWEIGHTHOURS.Find(id);
            if (pROJECTDOCUMENTWEIGHTHOUR == null)
            {
                return NotFound();
            }

            db.PROJECTDOCUMENTWEIGHTHOURS.Remove(pROJECTDOCUMENTWEIGHTHOUR);
            db.SaveChanges();

            return Ok(pROJECTDOCUMENTWEIGHTHOUR);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTDOCUMENTWEIGHTHOURExists(int id)
        {
            return db.PROJECTDOCUMENTWEIGHTHOURS.Count(e => e.OID == id) > 0;
        }
    }
}