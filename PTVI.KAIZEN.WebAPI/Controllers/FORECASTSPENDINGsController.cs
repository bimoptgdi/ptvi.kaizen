﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class fCastToEmailList : taskToEmailList
    {
        public decimal? FORECASTVALUE { get; set; }
        public new List<string> FIELDLIST
        {
            get
            {
                return new List<string>
                {
                    "TASK",
                    "DUEDATE",
                    "PROJECTDESC",
                    "DUEDATE",
                    "DAYSTODUE",
                    "FORECASTVALUE"
                };
            }
        }
    }

    public class FORECASTSPENDINGsController : KAIZENController
    {
        // GET: api/FORECASTSPENDINGs
        public IQueryable<FORECASTSPENDING> GetFORECASTSPENDINGs()
        {
            return db.FORECASTSPENDINGs;
        }

        // GET: api/FORECASTSPENDINGs/5
        [ResponseType(typeof(FORECASTSPENDING))]
        public IHttpActionResult GetFORECASTSPENDING(int id)
        {
            FORECASTSPENDING fORECASTSPENDING = db.FORECASTSPENDINGs.Find(id);
            if (fORECASTSPENDING == null)
            {
                return Ok();
            }
            
            return Ok(fORECASTSPENDING);
        }

        // PUT: api/FORECASTSPENDINGs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFORECASTSPENDING(int id, FORECASTSPENDING fORECASTSPENDING)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fORECASTSPENDING.OID)
            {
                return BadRequest();
            }

            fORECASTSPENDING.UPDATEDBY = GetCurrentNTUserId();
            fORECASTSPENDING.UPDATEDDATE = DateTime.Now;

            db.Entry(fORECASTSPENDING).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FORECASTSPENDINGExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FORECASTSPENDINGs
        [ResponseType(typeof(FORECASTSPENDING))]
        public IHttpActionResult PostFORECASTSPENDING(FORECASTSPENDING fORECASTSPENDING)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            fORECASTSPENDING.CREATEDBY = GetCurrentNTUserId();
            fORECASTSPENDING.CREATEDDATE = DateTime.Now;
            
            db.FORECASTSPENDINGs.Add(fORECASTSPENDING);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FORECASTSPENDINGExists(fORECASTSPENDING.OID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = fORECASTSPENDING.OID }, fORECASTSPENDING);
        }

        // DELETE: api/FORECASTSPENDINGs/5
        [ResponseType(typeof(FORECASTSPENDING))]
        public IHttpActionResult DeleteFORECASTSPENDING(int id)
        {
            FORECASTSPENDING fORECASTSPENDING = db.FORECASTSPENDINGs.Find(id);
            if (fORECASTSPENDING == null)
            {
                return NotFound();
            }

            db.FORECASTSPENDINGs.Remove(fORECASTSPENDING);
            db.SaveChanges();

            return Ok(fORECASTSPENDING);
        }

        [HttpGet]
        [ActionName("FindForecastSpendingByProject")]
        public IHttpActionResult FindForecastSpendingByProject(int param)
        {
            return Ok(db.FORECASTSPENDINGs.Where(d => d.PROJECTID == param)
                .Select(e => new ForecastSpendingDTO {
                    OID = e.OID,
                    DESCRIPTION = e.DESCRIPTION,
                    DUEDATE = e.DUEDATE,
                    FORECASTVALUE = e.FORECASTVALUE,
                    PROJECTID = e.PROJECTID
                })
            );
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        [ActionName("getForecastToEmail")]
        public IEnumerable<fCastToEmailList> getForecastToEmail(string param)
        {
            var m = db.FORECASTSPENDINGs;

            var fCast = m.Where(f => f.DUEDATE < DateTime.Now).ToList().Select(f => new fCastToEmailList()
            {
                BADGENO = f.PROJECT.USERASSIGNMENTs.Count(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true) > 0 ? f.PROJECT.USERASSIGNMENTs.Where(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true).FirstOrDefault().EMPLOYEEID : f.PROJECT.PROJECTMANAGERID,
                BADGENOTO = (f.PROJECT.USERASSIGNMENTs.Count(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true) > 0 ? f.PROJECT.USERASSIGNMENTs.Where(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true).FirstOrDefault().EMPLOYEEID : f.PROJECT.PROJECTMANAGERID)
                            + " | " + f.PROJECT.PROJECTMANAGERID,
                DAYSTODUE = f.DUEDATE != null ? Math.Floor((f.DUEDATE.Value.Subtract(DateTime.Now).TotalDays)) : 0,
                DUEDATE = f.DUEDATE.Value.Date.ToString(),
                EMAIL = f.PROJECT.USERASSIGNMENTs.Count(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true) > 0 ? f.PROJECT.USERASSIGNMENTs.Where(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true).FirstOrDefault().EMPLOYEEEMAIL : f.PROJECT.PROJECTMANAGEREMAIL,
                EMAILCC = f.PROJECT.PROJECTMANAGEREMAIL,
                FULLNAME = f.PROJECT.USERASSIGNMENTs.Count(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true) > 0 ? f.PROJECT.USERASSIGNMENTs.Where(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true).FirstOrDefault().EMPLOYEENAME : f.PROJECT.PROJECTMANAGERNAME,
                KEY = f.OID.ToString(),
                RECEIVER = f.PROJECT.USERASSIGNMENTs.Count(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true) > 0 ? f.PROJECT.USERASSIGNMENTs.Where(u => u.ASSIGNMENTTYPE == constants.PC && u.ISACTIVE == true).FirstOrDefault().EMPLOYEENAME : f.PROJECT.PROJECTMANAGERNAME,
                TASK = f.DESCRIPTION,
                FORECASTVALUE = f.FORECASTVALUE
            });
            return fCast;
        }

        private bool FORECASTSPENDINGExists(int id)
        {
            return db.FORECASTSPENDINGs.Count(e => e.OID == id) > 0;
        }
    }
}