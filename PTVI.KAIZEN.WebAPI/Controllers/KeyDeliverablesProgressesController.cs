﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class KeyDeliverablesProgressesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/KeyDeliverablesProgresses
        public IQueryable<KEYDELIVERABLESPROGRESS> GetKEYDELIVERABLESPROGRESSes()
        {
            return db.KEYDELIVERABLESPROGRESSes;
        }

        // GET: api/KeyDeliverablesProgresses/5
        [ResponseType(typeof(KEYDELIVERABLESPROGRESS))]
        public IHttpActionResult GetKEYDELIVERABLESPROGRESS(int id)
        {
            KEYDELIVERABLESPROGRESS kEYDELIVERABLESPROGRESS = db.KEYDELIVERABLESPROGRESSes.Find(id);
            if (kEYDELIVERABLESPROGRESS == null)
            {
                return NotFound();
            }

            return Ok(kEYDELIVERABLESPROGRESS);
        }

        // PUT: api/KeyDeliverablesProgresses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKEYDELIVERABLESPROGRESS(int id, KEYDELIVERABLESPROGRESS kEYDELIVERABLESPROGRESS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kEYDELIVERABLESPROGRESS.OID)
            {
                return BadRequest();
            }

            db.Entry(kEYDELIVERABLESPROGRESS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KEYDELIVERABLESPROGRESSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KeyDeliverablesProgresses
        [ResponseType(typeof(KEYDELIVERABLESPROGRESS))]
        public IHttpActionResult PostKEYDELIVERABLESPROGRESS(KEYDELIVERABLESPROGRESS kEYDELIVERABLESPROGRESS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.KEYDELIVERABLESPROGRESSes.Add(kEYDELIVERABLESPROGRESS);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = kEYDELIVERABLESPROGRESS.OID }, kEYDELIVERABLESPROGRESS);
        }

        // DELETE: api/KeyDeliverablesProgresses/5
        [ResponseType(typeof(KEYDELIVERABLESPROGRESS))]
        public IHttpActionResult DeleteKEYDELIVERABLESPROGRESS(int id)
        {
            KEYDELIVERABLESPROGRESS kEYDELIVERABLESPROGRESS = db.KEYDELIVERABLESPROGRESSes.Find(id);
            if (kEYDELIVERABLESPROGRESS == null)
            {
                return NotFound();
            }

            db.KEYDELIVERABLESPROGRESSes.Remove(kEYDELIVERABLESPROGRESS);
            db.SaveChanges();

            return Ok(kEYDELIVERABLESPROGRESS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KEYDELIVERABLESPROGRESSExists(int id)
        {
            return db.KEYDELIVERABLESPROGRESSes.Count(e => e.OID == id) > 0;
        }

        [HttpPut]
        [ActionName("updateActualProgress")]
        public HttpResponseMessage updateActualProgress(int param, keyDeliverablesProgressObject keyDeliverablesProgressParam)
        {
            KEYDELIVERABLESPROGRESS progress = db.KEYDELIVERABLESPROGRESSes.Where(w => w.KEYDELIVERABLESID == param && w.YEAR == keyDeliverablesProgressParam.year && w.MONTH == keyDeliverablesProgressParam.month).FirstOrDefault();

            if (progress != null)
            {
                if (!(progress.PLANPERCENTAGE.HasValue) && !(keyDeliverablesProgressParam.actualPercentage.HasValue))
                {
                    db.KEYDELIVERABLESPROGRESSes.Remove(progress);
                    db.Entry(progress).State = EntityState.Deleted;
                }
                else
                {
                    progress.ACTUALPERCENTAGE = keyDeliverablesProgressParam.actualPercentage;
                    progress.UPDATEDBY = GetCurrentNTUserId();
                    progress.UPDATEDDATE = DateTime.Now;
                    db.Entry(progress).State = EntityState.Modified;
                }
            }
            else
            {
                progress = new KEYDELIVERABLESPROGRESS();
                progress.KEYDELIVERABLESID = param;
                progress.YEAR = keyDeliverablesProgressParam.year;
                progress.MONTH = keyDeliverablesProgressParam.month;
                progress.ACTUALPERCENTAGE = keyDeliverablesProgressParam.actualPercentage;
                //progress.PLANPERCENTAGE = keyDeliverablesProgressParam.planPercentage;
                progress.CREATEDBY = GetCurrentNTUserId();
                progress.CREATEDDATE = DateTime.Now;

                db.KEYDELIVERABLESPROGRESSes.Add(progress);
            }
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPut]
        [ActionName("updatePlanProgress")]
        public HttpResponseMessage updatePlanProgress(int param, keyDeliverablesProgressObject keyDeliverablesProgressParam)
        {
            KEYDELIVERABLESPROGRESS progress = db.KEYDELIVERABLESPROGRESSes.Where(w => w.KEYDELIVERABLESID == param && w.YEAR == keyDeliverablesProgressParam.year && w.MONTH == keyDeliverablesProgressParam.month).FirstOrDefault();

            if (progress != null)
            {
                if (!(progress.PLANPERCENTAGE.HasValue) && !(keyDeliverablesProgressParam.planPercentage.HasValue))
                {
                    db.KEYDELIVERABLESPROGRESSes.Remove(progress);
                    db.Entry(progress).State = EntityState.Deleted;
                }
                else
                {
                    progress.PLANPERCENTAGE = keyDeliverablesProgressParam.planPercentage;
                    progress.UPDATEDBY = GetCurrentNTUserId();
                    progress.UPDATEDDATE = DateTime.Now;
                    db.Entry(progress).State = EntityState.Modified;
                }
            }
            else
            {
                progress = new KEYDELIVERABLESPROGRESS();
                progress.KEYDELIVERABLESID = param;
                progress.YEAR = keyDeliverablesProgressParam.year;
                progress.MONTH = keyDeliverablesProgressParam.month;
//                progress.ACTUALPERCENTAGE = keyDeliverablesProgressParam.actualPercentage;
                progress.PLANPERCENTAGE = keyDeliverablesProgressParam.planPercentage;
                progress.CREATEDBY = GetCurrentNTUserId();
                progress.CREATEDDATE = DateTime.Now;

                db.KEYDELIVERABLESPROGRESSes.Add(progress);
            }
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("keyDeliverablesProgressesByProject")]
        public kpiProgressObject keyDeliverablesProgressesByProject(int param)
        {
            //buang yang statusnya cancel
            var keyDeliverableMinMaxDate = db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param && w.STATUS != constants.STATUSIPROMCS_CANCELLED).GroupBy(g => g.PROJECTMANAGEMENT.PROJECTID).Select(s => new keyDeliverableObject
            {
                planStartDate = s.Min(m => m.PLANSTARTDATE),
                planFinishDate = s.Max(m => m.PLANFINISHDATE),
                actualStartDate = s.Min(m => m.ACTUALSTARTDATE),
                actualFinishDate = s.Max(m => m.ACTUALFINISHDATE)
            }).FirstOrDefault();

            var keyDeliverable = new kpiProgressObject();
            if (keyDeliverableMinMaxDate != null)
            {
                keyDeliverable = db.PROJECTMANAGEMENTs.Where(w => w.PROJECTID == param).Select(s => new kpiProgressObject
                {
                    id = s.OID,
                    startDate = keyDeliverableMinMaxDate.actualStartDate.HasValue ? keyDeliverableMinMaxDate.planStartDate.Value > keyDeliverableMinMaxDate.actualStartDate.Value ? keyDeliverableMinMaxDate.planStartDate.Value : keyDeliverableMinMaxDate.actualStartDate.Value : keyDeliverableMinMaxDate.planStartDate.Value,
                    finishDate = keyDeliverableMinMaxDate.actualFinishDate.HasValue ? keyDeliverableMinMaxDate.planFinishDate.Value > keyDeliverableMinMaxDate.actualFinishDate.Value ? keyDeliverableMinMaxDate.planFinishDate.Value : keyDeliverableMinMaxDate.actualFinishDate.Value : keyDeliverableMinMaxDate.planFinishDate.Value,
                    keyDeliverables = s.KEYDELIVERABLES.Where(w1 => w1.STATUS != constants.STATUSIPROMCS_CANCELLED).OrderBy(o => o.PLANSTARTDATE).Select(d => new keyDeliverablesKpiProgressObject
                    {
                        id = d.OID,
                        name = d.NAME,

                        weight = d.WEIGHTPERCENTAGE,
                        seq = d.SEQ,
                        keyDeliverablesProgresses = d.KEYDELIVERABLESPROGRESSes.Select(k => new keyDeliverablesDetailKpiProgressObject
                        {
                            year = k.YEAR.Value,
                            month = k.MONTH.Value,
                            planPercentage = k.PLANPERCENTAGE.HasValue ? k.PLANPERCENTAGE.Value : (double?)null,
                            actualPercentage = k.ACTUALPERCENTAGE.HasValue ? k.ACTUALPERCENTAGE.Value : (double?)null
                        })
                    })
                }).FirstOrDefault();
            }
            return keyDeliverable;
        }

        [HttpGet]
        [ActionName("projectKPIProgressMonthly")]
        public List<keyDeliverablesKpiProgressObject> projectKPIProgressMonthly(int param, int ExtKDMonth)
        {
            //get list progresss by project
            var progress = keyDeliverablesProgressesByProject(param);
            //get total month
            var totalMonths = 0;
            if ((progress.startDate != null) && (progress.finishDate != null))
                totalMonths = progress.id > 0 ? getTotalMonth(progress.startDate.Value, progress.finishDate.Value) + ExtKDMonth : 0;


            List<keyDeliverablesKpiProgressObject> listGraph = new List<keyDeliverablesKpiProgressObject>();
            List<keyDeliverablesDetailKpiProgressObject> listGraphProgress = new List<keyDeliverablesDetailKpiProgressObject>();
            DateTime curDate = new DateTime();
            //get list keydeliverable
            if (progress.keyDeliverables != null)
            {
                foreach (keyDeliverablesKpiProgressObject kd in progress.keyDeliverables)
                {
                    if ((progress.startDate != null) && (progress.finishDate != null))
                        curDate = progress.startDate.Value;

                    //reset list keyDeliverablesProgressKpiGraphObject
                    listGraphProgress = new List<keyDeliverablesDetailKpiProgressObject>();

                    //get list keydeliverable progress
                    if ((progress.startDate != null) && (progress.finishDate != null))
                    {
                        for (var i = 0; i <= totalMonths; i++)
                        {

                            //create new object keyDeliverablesProgressKpiGraphObject
                            keyDeliverablesDetailKpiProgressObject newKDProgress = new keyDeliverablesDetailKpiProgressObject();
                            newKDProgress.year = curDate.Year;
                            newKDProgress.month = curDate.Month;

                            //get planPercentage && actualPercentage by year and month
                            keyDeliverablesDetailKpiProgressObject kdProgress = kd.keyDeliverablesProgresses.Where(w => w.year.Value == curDate.Year && w.month.Value == curDate.Month).FirstOrDefault();
                            if (kdProgress != null)
                            {
                                DateTime dateCurTimeTmp = new DateTime(curDate.Year, curDate.Month, 1);

                                //getplandate
                                newKDProgress.planPercentage = kdProgress.planPercentage.HasValue ? kdProgress.planPercentage.Value : (double?)null;
                                if (!newKDProgress.planPercentage.HasValue)
                                {
                                    keyDeliverablesDetailKpiProgressObject kdPlanProgress = kd.keyDeliverablesProgresses.Where(w => new DateTime(w.year.Value, w.month.Value, 1) <= dateCurTimeTmp && w.planPercentage.HasValue).OrderByDescending(o => new DateTime(o.year.Value, o.month.Value, 1)).FirstOrDefault();
                                    if (kdPlanProgress != null)
                                    {
                                        newKDProgress.planPercentage = kdPlanProgress.planPercentage.HasValue ? kdPlanProgress.planPercentage.Value : (double?)null;
                                    }
                                }

                                //getactualdate
                                newKDProgress.actualPercentage = kdProgress.actualPercentage.HasValue ? kdProgress.actualPercentage.Value : (double?)null;
                                if (!newKDProgress.actualPercentage.HasValue)
                                {
                                    keyDeliverablesDetailKpiProgressObject kdActualProgress = kd.keyDeliverablesProgresses.Where(w => new DateTime(w.year.Value, w.month.Value, 1) <= dateCurTimeTmp && w.actualPercentage.HasValue).OrderByDescending(o => new DateTime(o.year.Value, o.month.Value, 1)).FirstOrDefault();
                                    if (kdActualProgress != null)
                                    {
                                        newKDProgress.actualPercentage = kdActualProgress.planPercentage.HasValue ? kdActualProgress.planPercentage.Value : (double?)null;
                                    }
                                }

                            }
                            else
                            {
                                DateTime dateCurTimeTmp = new DateTime(curDate.Year, curDate.Month, 1);

                                if (kd.id == 6)
                                {
                                    var x = 1;
                                }


                                //getplandate
                                if (!newKDProgress.planPercentage.HasValue)
                                {
                                    keyDeliverablesDetailKpiProgressObject kdPlanProgress = kd.keyDeliverablesProgresses.Where(w => new DateTime(w.year.Value, w.month.Value, 1) <= dateCurTimeTmp && w.planPercentage.HasValue).OrderByDescending(o => new DateTime(o.year.Value, o.month.Value, 1)).FirstOrDefault();
                                    if (kdPlanProgress != null)
                                    {
                                        newKDProgress.planPercentage = kdPlanProgress.planPercentage.HasValue ? kdPlanProgress.planPercentage.Value : (double?)null;
                                    }
                                }

                                //getactualdate
                                if (!newKDProgress.actualPercentage.HasValue)
                                {
                                    keyDeliverablesDetailKpiProgressObject kdActualProgress = kd.keyDeliverablesProgresses.Where(w => new DateTime(w.year.Value, w.month.Value, 1) <= dateCurTimeTmp && w.actualPercentage.HasValue).OrderByDescending(o => new DateTime(o.year.Value, o.month.Value, 1)).FirstOrDefault();
                                    if (kdActualProgress != null)
                                    {
                                        newKDProgress.actualPercentage = kdActualProgress.actualPercentage.HasValue ? kdActualProgress.actualPercentage.Value : (double?)null;
                                    }
                                }
                            }
                            //add List Graph Progress
                            listGraphProgress.Add(newKDProgress);

                            //next year by loop;
                            curDate = curDate.AddMonths(1);
                        }
                    }

                    //create new object keyDeliverablesKpiGraphObject
                    keyDeliverablesKpiProgressObject newKD = new keyDeliverablesKpiProgressObject();
                    newKD.id = kd.id;
                    newKD.name = kd.name;
                    newKD.nameText = System.Text.RegularExpressions.Regex.IsMatch(kd.name, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(kd.name)).KEYDELIVERABLES : kd.name;
                    newKD.weight = kd.weight;
                    newKD.keyDeliverablesProgresses = listGraphProgress;
                    newKD.seq = kd.seq;

                    //add List Graph
                    listGraph.Add(newKD);
                }
            }
            return listGraph;
        }

        [HttpGet]
        [ActionName("projectKPIChart")]
        public object projectKPIChart(int param, int ExtKDMonth)
        {
            var listProgresses = projectKPIProgressMonthly(param, ExtKDMonth);
            List<KpiGraphProgressObject> results = new List<KpiGraphProgressObject>();
            foreach (keyDeliverablesKpiProgressObject listProgress in listProgresses)
            {

                foreach (keyDeliverablesDetailKpiProgressObject keyDeliverablesProgress in listProgress.keyDeliverablesProgresses)
                {
                    KpiGraphProgressObject newResultProgress = new KpiGraphProgressObject();
                    newResultProgress.id = listProgress.id;
                    newResultProgress.name = listProgress.name;
                    newResultProgress.dateProgress = new DateTime(keyDeliverablesProgress.year.Value, keyDeliverablesProgress.month.Value, 1);
                    newResultProgress.planPercentage = listProgress.keyDeliverablesProgresses.
                        Where(w => new DateTime(w.year.Value, w.month.Value, 1) == new DateTime(keyDeliverablesProgress.year.Value, keyDeliverablesProgress.month.Value, 1)).
                        Sum(s => s.planPercentage) / (100 / listProgress.weight);
                    newResultProgress.actualPercentage = listProgress.keyDeliverablesProgresses.
                        Where(w => new DateTime(w.year.Value, w.month.Value, 1) == new DateTime(keyDeliverablesProgress.year.Value, keyDeliverablesProgress.month.Value, 1)).
                        Sum(s => s.actualPercentage) / (100 / listProgress.weight);
                    results.Add(newResultProgress);
                }
            }
            return results.GroupBy(g => new { g.dateProgress }).
                Select(s => new
                {
                    dateProgress = s.Key.dateProgress,
                    planPercentage = Math.Round(s.Sum(x => x.planPercentage).Value, 1, MidpointRounding.AwayFromZero),
                    actualPercentage = Math.Round(s.Sum(x => x.actualPercentage).Value, 1, MidpointRounding.AwayFromZero),
                    valueColor = Math.Round(s.Sum(x => x.planPercentage).Value, 1, MidpointRounding.AwayFromZero) > Math.Round(s.Sum(x => x.actualPercentage).Value, 1, MidpointRounding.AwayFromZero) ? constants.TRAFFICLIGHT_RED : constants.TRAFFICLIGHT_GREEN
        });
        }
    }

    public class kpiProgressObject
    {
        public int id { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? finishDate { get; set; }
        public int? totalMonth { get; set; }

        public virtual IEnumerable<keyDeliverablesKpiProgressObject> keyDeliverables { get; set; }
    }

    public class keyDeliverablesKpiProgressObject
    {
        public int? id { get; set; }
        public string name { get; set; }
        public double? weight { get; set; }
        public string seq { get; set; }
        public string nameText { get; set; }
        public virtual IEnumerable<keyDeliverablesDetailKpiProgressObject> keyDeliverablesProgresses { get; set; }
    }

    public class keyDeliverablesDetailKpiProgressObject
    {
        public int? year { get; set; }
        public int? month { get; set; }
        public double? planPercentage { get; set; }
        public double? actualPercentage { get; set; }
    }

    public class KpiGraphProgressObject
    {
        public int? id { get; set; }
        public string name { get; set; }
        public DateTime? dateProgress { get; set; }
        public double? planPercentage { get; set; }
        public double? actualPercentage { get; set; }
    }
}