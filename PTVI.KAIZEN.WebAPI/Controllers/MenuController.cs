using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MenuController : KAIZENController
    {
        // GET api/Menu
        public IEnumerable<MENU> GetMENUs()
        {
            var menus = db.MENUs.Include(m => m.MENU2);
            return menus.AsEnumerable();
        }

        // GET api/Menu/5
        public MENU GetMENU(int id)
        {
            MENU menu = db.MENUs.Find(id);
            if (menu == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return menu;
        }

        // PUT api/Menu/5
        public HttpResponseMessage PutMENU(int id, MENU menu)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            //}

            if (id != menu.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid && id == menu.OID)
            {
                var existing = db.MENUs.Find(menu.OID);
                existing.TITLE = menu.TITLE;
                existing.DESCRIPTION = menu.DESCRIPTION;
                existing.FILENAME = menu.FILENAME;
                existing.IMAGE = menu.IMAGE;
                existing.ISACTIVE = menu.ISACTIVE;
                existing.SHOWTITLE = menu.SHOWTITLE;
                existing.UPDATEDBY = GetCurrentNTUserId();
                existing.UPDATEDDATE = DateTime.Now;

                // hapus dulu menuaccess yang ada
                if (existing.MENUACCESSRIGHTs.Count() > 0)
                {
                    foreach (var ar in existing.MENUACCESSRIGHTs.ToList())
                        db.MENUACCESSRIGHTs.Remove(ar);
                }

                ICollection<MENUACCESSRIGHT> listParentMenuAccessRight = menu.MENUACCESSRIGHTs;
                if (listParentMenuAccessRight.Count() > 0)
                {
                    existing.MENUACCESSRIGHTs = listParentMenuAccessRight;
                }

                db.Entry(existing).State = EntityState.Modified;

                foreach (MENU childMenu in menu.MENU1)
                {
                    ICollection<MENUACCESSRIGHT> listMenuAccessRight = childMenu.MENUACCESSRIGHTs;
                    foreach (MENUACCESSRIGHT menuAccessRight in listMenuAccessRight)
                    {
                        MENUACCESSRIGHT newAccessRight = new MENUACCESSRIGHT();
                        newAccessRight.MENUID = menu.OID;
                        try
                        {
                            // cek apakah sudah ada accessright
                            ACCESSRIGHT accessRight = db.ACCESSRIGHTs.Find(menuAccessRight.ACCESSRIGHT.OID);
                            newAccessRight.ACCESSRIGHT = accessRight;

                            menuAccessRight.ACCESSRIGHT = accessRight;

                        }
                        catch (Exception ex)
                        {
                            // jika belum ada maka buat baru
                            ACCESSRIGHT accessRight = new ACCESSRIGHT();
                            accessRight.DESCRIPTION = menuAccessRight.ACCESSRIGHT.DESCRIPTION;

                            menuAccessRight.ACCESSRIGHT = accessRight;
                        }

                    }

                    if (childMenu.OID == 0)
                    {
                        MENU newChildMenu = new MENU();
                        newChildMenu.SEQ = childMenu.SEQ;
                        newChildMenu.TITLE = childMenu.TITLE;
                        newChildMenu.DESCRIPTION = childMenu.DESCRIPTION;
                        newChildMenu.FILENAME = childMenu.FILENAME;
                        newChildMenu.IMAGE = childMenu.IMAGE;
                        newChildMenu.ISACTIVE = childMenu.ISACTIVE;
                        newChildMenu.SHOWTITLE = childMenu.SHOWTITLE;
                        newChildMenu.PARENTID = existing.OID;
                        newChildMenu.CREATEDBY = GetCurrentNTUserId();
                        newChildMenu.CREATEDDATE = DateTime.Now;

                        db.MENUs.Add(newChildMenu);
                    }
                    else
                    {
                        var existingSubMenu = db.MENUs.Find(childMenu.OID);
                        existingSubMenu.TITLE = childMenu.TITLE;
                        existingSubMenu.DESCRIPTION = childMenu.DESCRIPTION;
                        existingSubMenu.FILENAME = childMenu.FILENAME;
                        existingSubMenu.IMAGE = childMenu.IMAGE;
                        existingSubMenu.ISACTIVE = childMenu.ISACTIVE;
                        existingSubMenu.SHOWTITLE = childMenu.SHOWTITLE;
                        existingSubMenu.SEQ = childMenu.SEQ;
                        existingSubMenu.UPDATEDBY = GetCurrentNTUserId();
                        existingSubMenu.UPDATEDDATE = DateTime.Now;

                        db.Entry(existingSubMenu).State = EntityState.Modified;
                    }
                }

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK, menu);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            //db.Entry(menu).State = EntityState.Modified;

            //try
            //{
            //    MENU existing = db.MENUs.Find(id);


            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException ex)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            //}

            //return Request.CreateResponse(HttpStatusCode.OK, menu);
        }

        // POST api/Menu
        public HttpResponseMessage PostMENU(MENU menu)
        {
            if (ModelState.IsValid)
            {
                //MENU newMenu = new MENU();
                //newMenu.TITLE = menu.TITLE;
                //newMenu.DESCRIPTION = menu.DESCRIPTION;
                //newMenu.FILENAME = menu.FILENAME;
                //newMenu.IMAGE = menu.IMAGE;
                //newMenu.ISACTIVE = menu.ISACTIVE;
                ICollection<MENUACCESSRIGHT> listMenuAccessRight = menu.MENUACCESSRIGHTs;
                foreach (MENUACCESSRIGHT menuAccessRight in listMenuAccessRight)
                {
                    MENUACCESSRIGHT newAccessRight = new MENUACCESSRIGHT();
                    newAccessRight.MENUID = menu.OID;
                    try 
                    {
                        // cek apakah sudah ada accessright
                        ACCESSRIGHT accessRight = db.ACCESSRIGHTs.Find(menuAccessRight.ACCESSRIGHT.OID);
                        newAccessRight.ACCESSRIGHT = accessRight;

                        menuAccessRight.ACCESSRIGHT = accessRight;

                    } 
                    catch (Exception ex)
                    {
                        // jika belum ada maka buat baru
                        ACCESSRIGHT accessRight = new ACCESSRIGHT();
                        accessRight.DESCRIPTION = menuAccessRight.ACCESSRIGHT.DESCRIPTION;

                        menuAccessRight.ACCESSRIGHT = accessRight;
                    }

                }
                menu.CREATEDBY = GetCurrentNTUserId();
                menu.CREATEDDATE = DateTime.Now;
                db.MENUs.Add(menu);
                db.SaveChanges();

                //foreach (MENU childMenu in menu.MENU1)
                //{
                //    MENU newChildMenu = new MENU();
                //    newChildMenu.SEQ = childMenu.SEQ;
                //    newChildMenu.TITLE = childMenu.TITLE;
                //    newChildMenu.DESCRIPTION = childMenu.DESCRIPTION;
                //    newChildMenu.FILENAME = childMenu.FILENAME;
                //    newChildMenu.IMAGE = childMenu.IMAGE;
                //    newChildMenu.ISACTIVE = childMenu.ISACTIVE;
                //    newChildMenu.PARENTID = newMenu.OID;

                //    db.MENUs.Add(newChildMenu);
                //    db.SaveChanges();
                //}
                //db.MENUs.Add(menu);
                

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, menu);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = menu.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Menu/5
        public HttpResponseMessage DeleteMENU(int id)
        {
            MENU menu = db.MENUs.Find(id);
            if (menu == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (menu.MENU1.Count() > 0)
            {
                for (int i = menu.MENU1.Count() - 1; i > -1; i--)
                {
                    db.MENUs.Remove(menu.MENU1.ElementAt(i));
                }
            }

            // hapus menu access right
            foreach (var ar in menu.MENUACCESSRIGHTs)
                db.MENUACCESSRIGHTs.Remove(ar);

            db.MENUs.Remove(menu);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, menu);
        }

        private bool isMenuHasChild(MENU menu)
        {
            MENU theMenu = db.MENUs.Find(menu.OID);
            if (theMenu.MENU1.Count() > 0)
            {
                return true;
            }

            return false;
        }

        [HttpGet]
        [ActionName("GetCommonMenu")]
        public IEnumerable<MENU> GetCommonMenu(string param)
        {
            // cari parent dari menu
            // parent menu akan membawa child2nya
            IQueryable<MENU> listMenu = db.MENUs.OrderBy(o => o.SEQ).Where(d => d.PARENTID == null);

            // ambil id menu yang ada di accessright
            List<int> menuIdWithAccessRight = db.MENUACCESSRIGHTs.Select(d => d.MENUID).ToList();
            // ambil id menu yang tidak memerlukan accessright
            List<int> menuIdNoNeedAccessRight = db.MENUs.Where(d => !menuIdWithAccessRight.Contains(d.OID)).Select(d => d.OID).ToList();

            IQueryable<MENU> listParentMenuCanBeAccessed = listMenu.Where(d => menuIdNoNeedAccessRight.Contains(d.OID));

            // singkirkan child menu yang mempunyai accessright
            List<int> menuEmptyId = new List<int>();
            foreach (MENU menu in listParentMenuCanBeAccessed)
            {

                IEnumerable<MENU> subMenuAlter = menu.MENU1.Where(d => menuIdNoNeedAccessRight.Contains(d.OID));
                if (subMenuAlter.Count() > 0)
                {
                    menu.MENU1 = subMenuAlter.ToList();
                }
                else
                {
                    // jika menu tersebut aslinya mempunyai child
                    // maka tandai untuk dihapus karena semua childnya tidak punya akses
                    if (isMenuHasChild(menu))
                    {
                        menuEmptyId.Add(menu.OID);
                    }

                }

            }

            // hapus menu yang semua childnya tidak dapat accessright
            if (menuEmptyId.Count() > 0)
            {
                listParentMenuCanBeAccessed = listParentMenuCanBeAccessed.Where(d => !menuEmptyId.Contains(d.OID));
            }

            return listParentMenuCanBeAccessed.AsEnumerable();
        }

        [HttpGet]
        [ActionName("GetMenuByNTUserID")]
        public IEnumerable<MENU> GetMenuByNTUserID(string param)
        {
            try
            {
                //===== Ambil USER ACCESSRIGHT =====
                USER user = db.USERs.FirstOrDefault(d => d.NTUSERID == param);
                if (user != null)
                {
                    List<int> userRoles = db.USERROLEs.Where(d => d.USERID == user.OID).Select(e => e.ROLEID).ToList();
                    List<int> userAccessRight = db.ROLEACCESSRIGHTs.Where(d => userRoles.Contains(d.ROLEID)).Select(e => e.ACCESSRIGHTID).ToList();
                    List<int> menuIdGrantedAccess = db.MENUACCESSRIGHTs.Where(d => userAccessRight.Contains(d.ACCESSRIGHTID)).Select(e => e.MENUID).ToList();

                    // cari parent dari menu
                    // parent menu akan membawa child2nya
                    IQueryable<MENU> listMenu = db.MENUs.Where(d => d.PARENTID == null).OrderBy(o => o.SEQ);

                    // ambil id menu yang ada accessright
                    List<int> menuIdWithAccessRight = db.MENUACCESSRIGHTs.Select(d => d.MENUID).ToList();
                    // ambil id menu yang dapat di access
                    List<int> menuIdCanAccess = db.MENUs.Where(d => !menuIdWithAccessRight.Contains(d.OID) || menuIdGrantedAccess.Contains(d.OID)).Select(d => d.OID).ToList();

                    // cek parent menu apakah user mempunyai hak akses untuk mengakses menu nya
                    IQueryable<MENU> listParentMenuCanBeAccessed = listMenu.Where(d => menuIdCanAccess.Contains(d.OID));
                    // singkirkan child menu yang mempunyai accessright
                    List<int> menuEmptyId = new List<int>();
                    foreach (MENU menu in listParentMenuCanBeAccessed)
                    {

                        IEnumerable<MENU> subMenuAlter = menu.MENU1.Where(d => menuIdCanAccess.Contains(d.OID));
                        if (subMenuAlter.Count() > 0)
                        {
                            menu.MENU1 = subMenuAlter.ToList();
                        }
                        else
                        {
                            // jika menu tersebut aslinya mempunyai child
                            // maka tandai untuk dihapus karena semua childnya tidak punya akses
                            if (isMenuHasChild(menu))
                            {
                                menuEmptyId.Add(menu.OID);
                            }

                        }

                    }

                    // hapus menu yang semua childnya tidak dapat accessright
                    if (menuEmptyId.Count() > 0)
                    {
                        listParentMenuCanBeAccessed = listParentMenuCanBeAccessed.Where(d => !menuEmptyId.Contains(d.OID));
                    }

                    return listParentMenuCanBeAccessed.AsEnumerable();
                }
                else
                {
                    return GetCommonMenu(param);
                }



            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        [ActionName("GetMenuByNTUserIDNew")]
        public IEnumerable<menuObject> GetMenuByNTUserIDNew(string param)
        {
            try
            {
                //===== Ambil ACCESSRIGHT =====
                var accessRight = db.ACCESSRIGHTs.Where(w => w.ROLEACCESSRIGHTs.Count(c => c.ROLE.USERROLEs.Count(cu => cu.USER.NTUSERID == param) > 0) > 0).Select(s => s.OID).ToList();

                //===== Ambil Menu yg ada ACCESSRIGHT =====
                var menuWithAccesstight = db.MENUACCESSRIGHTs.Where(w => w.MENU.PARENTID == null && accessRight.Contains(w.ACCESSRIGHTID)).Select(s => s.MENU).ToList();

                //===== Ambil Menu yg tidak ada ACCESSRIGHT =====
                var menuNoAccesstight = db.MENUs.Where(w => w.PARENTID == null && w.MENUACCESSRIGHTs.Count() == 0).ToList();

                //Get Menu
                var AllMenu = menuWithAccesstight.Union(menuNoAccesstight).ToList();
                return AllMenu.Select(s => new menuObject
                {
                    id = s.OID,
                    title = s.TITLE,
                    description = s.DESCRIPTION,
                    parentID = s.PARENTID,
                    fileName = s.FILENAME,
                    isActive = s.ISACTIVE,
                    seq = s.SEQ,
                    image = s.IMAGE,
                    showTitle = s.SHOWTITLE,
                    createdBy = s.CREATEDBY,
                    createdDate = s.CREATEDDATE,
                    updatedBy = s.UPDATEDBY,
                    updatedDate = s.UPDATEDDATE,
                    menu1 = rekursifMenuObject(param, accessRight, s.MENU1),
                    //MENUACCESSRIGHTs = s.MENUACCESSRIGHTs
                }).OrderBy(o => o.seq);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private IEnumerable<menuObject> rekursifMenuObject(string param, IEnumerable<int> accessRight, IEnumerable<MENU> subMenu)
        {
            try
            {
                //===== Ambil Menu yg ada ACCESSRIGHT =====
                var menuWithAccesstight = subMenu.Where(w => w.MENUACCESSRIGHTs.Where(sm => accessRight.Contains(sm.ACCESSRIGHTID)).Count() > 0).ToList();
                //===== Ambil Menu yg tidak ada ACCESSRIGHT =====
                var menuNoAccesstight = subMenu.Where(w => w.MENUACCESSRIGHTs.Count() == 0).ToList();

                //Get Menu
                var AllMenu = menuWithAccesstight.Union(menuNoAccesstight).ToList();
                return AllMenu.Select(s => new menuObject
                {
                    id = s.OID,
                    title = s.TITLE,
                    description = s.DESCRIPTION,
                    parentID = s.PARENTID,
                    fileName = s.FILENAME,
                    isActive = s.ISACTIVE,
                    seq = s.SEQ,
                    image = s.IMAGE,
                    showTitle = s.SHOWTITLE,
                    createdBy = s.CREATEDBY,
                    createdDate = s.CREATEDDATE,
                    updatedBy = s.UPDATEDBY,
                    updatedDate = s.UPDATEDDATE,
                    menu1 = rekursifMenuObject(param, accessRight, s.MENU1),
                    //MENUACCESSRIGHTs = s.MENUACCESSRIGHTs
                }).OrderBy(o => o.seq);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        private IEnumerable<MENU> rekursifMenu(IEnumerable<MENU> subMenu)
        {
            try
            {
                //Get Menu
                return subMenu.Select(s => new MENU
                {
                    OID = s.OID,
                    TITLE = s.TITLE,
                    DESCRIPTION = s.DESCRIPTION,
                    PARENTID = s.PARENTID,
                    FILENAME = s.FILENAME,
                    ISACTIVE = s.ISACTIVE,
                    SEQ = s.SEQ,
                    IMAGE = s.IMAGE,
                    SHOWTITLE = s.SHOWTITLE,
                    CREATEDBY = s.CREATEDBY,
                    CREATEDDATE = s.CREATEDDATE,
                    UPDATEDBY = s.UPDATEDBY,
                    UPDATEDDATE = s.UPDATEDDATE,
                    MENU1 = rekursifMenu(s.MENU1).ToList(),
                    //MENUACCESSRIGHTs = s.MENUACCESSRIGHTs
                }).OrderBy(o => o.SEQ);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        [ActionName("IsUserGetAccessRight")]
        /* Check if User get permission to access the Form
         * example: IsUserGetAccessRight(herus|SetupLookupForm.aspx)
         * **/
        public object IsUserGetAccessRight(string param)
        {
            bool result = false;
            string msg = string.Empty;

            string ntUserID = string.Empty;
            string pageForm = string.Empty;
            
            if (param.Split('|').Count() > 1)
            {
                ntUserID = param.Split('|')[0];
                pageForm = param.Split('|')[1];

                if (String.IsNullOrEmpty(ntUserID))
                {
                    result = false;
                }
                else
                {
                    List<int> menus = db.MENUs.Where(d => d.FILENAME.ToLower() == pageForm.ToLower()).Select(d => d.OID).ToList();
                    if (menus.Count() > 0)
                    {
                        // cek apakah menu ini perlu accessright
                        IQueryable<MENUACCESSRIGHT> menuAccessIDs = db.MENUACCESSRIGHTs.Where(d => menus.Contains(d.MENUID));
                        if (menuAccessIDs.Count() > 0)
                        {
                            // jika perlu akses right maka cek apakah rolenya dapat akses untuk menu ini
                            //===== Ambil USER ACCESSRIGHT =====
                            try
                            {
                                USER user = db.USERs.Single(d => d.NTUSERID == ntUserID);
                                List<int> userRoles = db.USERROLEs.Where(d => d.USERID == user.OID).Select(e => e.ROLEID).ToList();
                                List<int> userAccessRight = db.ROLEACCESSRIGHTs.Where(d => userRoles.Contains(d.ROLEID)).Select(e => e.ACCESSRIGHTID).ToList();
                                List<int> menuIdGrantedAccess = db.MENUACCESSRIGHTs.Where(d => userAccessRight.Contains(d.ACCESSRIGHTID)).Select(e => e.MENUID).ToList();

                                foreach (int menuIdGranted in menuIdGrantedAccess)
                                {
                                    foreach (int menuId in menus)
                                    {
                                        if (menuId == menuIdGranted)
                                        {
                                            result = true;
                                        }
                                    }
                                }

                                if (!result)
                                {
                                    msg = "User " + ntUserID + " is not authorized to access this form";
                                }
                            }
                            catch (InvalidOperationException ex)
                            {
                                result = false;
                                msg = "User " + ntUserID + " is not found";
                            }


                        }
                        else
                        {
                            // jika tidak perlu akses right maka user dapat akses ke menu ini
                            result = true;
                        }
                    }
                    else
                    {
                        // diberikan akses karena tidak terdaftar di menu
                        result = true;

                    }
                }
                
            }
            else
            {
                result = false;
                msg = "The passing parameter is not correct";
            }

            var v = new[] { 
                new { 
                    auth = (result ? "1" : "0"),
                    message = msg
                } 
            };
            return v;
        }

        [HttpGet]
        [ActionName("GetAllParentMenu")]
        public IEnumerable<MENU> GetAllParentMenu(string param)
        {
            // cari parent dari menu
            // parent menu akan membawa child2nya
            IQueryable<MENU> listMenu = db.MENUs.Where(d => d.PARENTID == null).OrderBy(o => o.SEQ);

            return listMenu.AsEnumerable();
        }

        [HttpGet]
        [ActionName("GetAllParentMenuNew")]
        public IEnumerable<MENU> GetAllParentMenuNew(string param)
        {
            // cari parent dari menu
            // parent menu akan membawa child2nya
            IEnumerable<MENU> listMenu = db.MENUs.Where(d => d.PARENTID == null).ToList().Select(s => new MENU
            {
                OID = s.OID,
                TITLE = s.TITLE,
                DESCRIPTION = s.DESCRIPTION,
                PARENTID = s.PARENTID,
                FILENAME = s.FILENAME,
                ISACTIVE = s.ISACTIVE,
                SEQ = s.SEQ,
                IMAGE = s.IMAGE,
                SHOWTITLE = s.SHOWTITLE,
                CREATEDBY = s.CREATEDBY,
                CREATEDDATE = s.CREATEDDATE,
                UPDATEDBY = s.UPDATEDBY,
                UPDATEDDATE = s.UPDATEDDATE,
                MENU1 = rekursifMenu(s.MENU1).ToList()
            }).OrderBy(o => o.SEQ);

            return listMenu.AsEnumerable();
        }

        [HttpGet]
        [ActionName("GetSubMenu")]
        public IEnumerable<MENU> GetSubMenu(int param)
        {
            // cari parent dari menu
            // parent menu akan membawa child2nya
            IQueryable<MENU> listMenu = db.MENUs.Where(d => d.PARENTID == param);

            return listMenu.AsEnumerable();
        }

        [HttpGet]
        [ActionName("GetSubMenuNew")]
        public IEnumerable<MENU> GetSubMenuNew(int param)
        {
            // cari parent dari menu
            // parent menu akan membawa child2nya
            IEnumerable<MENU> listMenu = db.MENUs.ToList().Where(d => d.PARENTID == param).Select(s => new MENU
            {
                OID = s.OID,
                TITLE = s.TITLE,
                DESCRIPTION = s.DESCRIPTION,
                PARENTID = s.PARENTID,
                FILENAME = s.FILENAME,
                ISACTIVE = s.ISACTIVE,
                SEQ = s.SEQ,
                IMAGE = s.IMAGE,
                SHOWTITLE = s.SHOWTITLE,
                CREATEDBY = s.CREATEDBY,
                CREATEDDATE = s.CREATEDDATE,
                UPDATEDBY = s.UPDATEDBY,
                UPDATEDDATE = s.UPDATEDDATE,
                MENU1 = rekursifMenu(s.MENU1).ToList()
            }).OrderBy(o => o.SEQ);

            return listMenu.AsEnumerable();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}