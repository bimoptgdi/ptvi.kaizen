﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterDesignerDraftingController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/masterDesignerDrafting
        public IQueryable<masterDesignerDraftingObject> GetMASTERDESIGNERPRODUCTIONs()
        {
            return db.MASTERDESIGNERPRODUCTIONs.Select(s => new masterDesignerDraftingObject
            {
                id = s.OID,
                designerTypeText = db.LOOKUPs.Where(x => x.TYPE == "positionGroupdesigner" && x.VALUE == s.DESIGNERTYPE).FirstOrDefault().TEXT,
                designerType = s.DESIGNERTYPE,
                typeOfDrawing = s.TYPEOFDRAWING,
                estimateHours = s.ESTIMATEHOURS,
                text = s.TEXT,
                isActive = s.ISACTIVE
            });
        }

        // POST api/<controller>
        [HttpPost]
        [ActionName("addDesignerDrafting")]
        public IHttpActionResult addDesignerDrafting(string param, masterDesignerDraftingObject masterDesignerParam)
        {
            MASTERDESIGNERPRODUCTION add = new MASTERDESIGNERPRODUCTION();
            add.DESIGNERTYPE = masterDesignerParam.designerType;
            add.TYPEOFDRAWING = masterDesignerParam.typeOfDrawing;
            add.ESTIMATEHOURS = masterDesignerParam.estimateHours;
            add.TEXT = masterDesignerParam.text;
            add.ISACTIVE = true;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERDESIGNERPRODUCTIONs.Add(add);
            db.SaveChanges();

            masterDesignerParam.id = add.OID;
            return Ok(masterDesignerParam);
        }

        // PUT api/<controller>/5
        [HttpPut]
        [ActionName("updateMasterDesigner")]
        public IHttpActionResult putMasterDesigner(int param, masterDesignerDraftingObject masterDesignerParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != masterDesignerParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERDESIGNERPRODUCTION update = db.MASTERDESIGNERPRODUCTIONs.Find(param);
                update.DESIGNERTYPE = masterDesignerParam.designerType;
                update.TYPEOFDRAWING = masterDesignerParam.typeOfDrawing;
                update.ESTIMATEHOURS = masterDesignerParam.estimateHours;
                update.TEXT = masterDesignerParam.text;
                update.ISACTIVE = true;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
             }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(masterDesignerParam);
        }

        // DELETE api/<controller>/5
        [ResponseType(typeof(MASTERDESIGNERPRODUCTION))]
        public IHttpActionResult DeleteMASTERDESIGNERPRODUCTION(int id)
        {
            MASTERDESIGNERPRODUCTION mASTERDESIGNERPRODUCTION = db.MASTERDESIGNERPRODUCTIONs.Find(id);
            if (mASTERDESIGNERPRODUCTION == null)
            {
                return NotFound();
            }

            db.MASTERDESIGNERPRODUCTIONs.Remove(mASTERDESIGNERPRODUCTION);
            db.SaveChanges();

            return Ok(mASTERDESIGNERPRODUCTION);
        }
        [HttpGet]
        [ActionName("getTypeNumberOfDrawing")]
        public IQueryable<masterDesignerDraftingObject> getTypeNumberOfDrawing(string param)
        {
            return db.MASTERDESIGNERPRODUCTIONs.Select(s => new masterDesignerDraftingObject
            {
                typeOfDrawing = s.TYPEOFDRAWING
            }).Distinct();
        }
    }
}