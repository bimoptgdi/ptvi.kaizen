﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TendersController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/Tenders
        public IQueryable<TENDER> GetTENDERs()
        {
            return db.TENDERs;
        }

        // GET: api/Tenders/5
        [ResponseType(typeof(TENDER))]
        public IHttpActionResult GetTENDER(int id)
        {
            TENDER tENDER = db.TENDERs.Find(id);
            if (tENDER == null)
            {
                return NotFound();
            }

            return Ok(tENDER);
        }

        // PUT: api/Tenders/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDER(int id, TENDER tENDER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDER.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tenders
        [ResponseType(typeof(TENDER))]
        public IHttpActionResult PostTENDER(TENDER tENDER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDERs.Add(tENDER);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDER.OID }, tENDER);
        }

        // DELETE: api/Tenders/5
        [ResponseType(typeof(TENDER))]
        public IHttpActionResult DeleteTENDER(int id)
        {
            TENDER tENDER = db.TENDERs.Find(id);
            if (tENDER == null)
            {
                return NotFound();
            }

            db.TENDERs.Remove(tENDER);
            db.SaveChanges();

            return Ok(tENDER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TENDERExists(int id)
        {
            return db.TENDERs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("tenderById")]
        public tenderObject tenderById(int param)
        {
            string getContractAwardValue = db.LOOKUPs.Where(w => w.TYPE == constants.TENDERPROGRESS).Max(m => m.VALUE);
            return db.TENDERs.Where(w => w.OID == param).ToList().Select(s => new tenderObject {
                id = s.OID,
                tenderNo = s.TENDERNO,
                vendorSelectedId = s.VENDORSELECTEDID,
                vendorSelectedName = s.VENDORSELECTEDNAME,
                vendorSelectedCode = s.VENDORSELECTEDCODE,
                contractNo = s.CONTRACTNO,
                docNoSummary = string.Join(", ", s.PROJECTDOCUMENTs.Select(s1 => s1.DOCNO)),
                isEnabledSelectVendor = s.TENDERPROGRESSes.FirstOrDefault(w => w.PROCESSTYPE == getContractAwardValue).ACTUALFINISHDATE.HasValue
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("listTenderOutProcessByProjectId")]
        public object listTenderOutProcessByProjectId(int param)
        {
            var listLookupExecutor = db.LOOKUPs.Where(w => w.TYPE == constants.EXECUTOR_EWP && w.VALUE == constants.EXECUTOR_EWP_CONTRACTOR).Select(s => s.VALUE);
            var listTenderOut =
                //saat menggunakana form assign executor EWP Control
                //db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param && !w.TENDERID.HasValue && listLookupExecutor.Contains(w.EXECUTORASSIGNMENT)).
                //saat set pada project document menjadi CAS
                db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param && ((w.TENDERID.HasValue ? w.TENDER.TENDERNO == null : !w.TENDERID.HasValue)) && w.EXECUTOR == constants.EXECUTOR_CAS && w.ACTUALSTARTDATE.HasValue && w.ACTUALFINISHDATE.HasValue && w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED).
                Select(s => new
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    docType = s.DOCTYPE,
                    docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
                    docNo = s.DOCNO,
                    docTitle = s.DOCTITLE,
                    drawingNo = s.DRAWINGNO,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    discipline = s.DISCIPLINE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    executor = s.EXECUTOR,
                    executorAssignment = s.EXECUTORASSIGNMENT,
                    executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
                    progress = s.PROGRESS,
                    documentById = s.DOCUMENTBYID,
                    documentByName = s.DOCUMENTBYNAME,
                    documentByEmail = s.DOCUMENTBYEMAIL,
                    isActive = (bool?)null,
                    project = new projectObject { projectManagerId = s.PROJECT.PROJECTMANAGERID, projectManagerName = s.PROJECT.PROJECTMANAGERNAME, projectManagerEmail = s.PROJECT.PROJECTMANAGEREMAIL }
                });
            return listTenderOut;
        }

        [HttpPut]
        [ActionName("tederOutSubmit")]
        public HttpResponseMessage tederOutSubmit(string param, List<projectDocumentObject> projectDocParam)
        {
            TENDER tenderNew = new TENDER();
            tenderNew.TENDERNO = param;
            tenderNew.TENDERCREATEDDATE = DateTime.Now;
            tenderNew.TENDERCREATEDBY = GetCurrentNTUserId();

            tenderNew.TENDERPROGRESSes = db.LOOKUPs.Where(w => w.TYPE == constants.TENDERPROGRESS).ToList().Select(s => new TENDERPROGRESS
            {
                PROCESSTYPE = s.VALUE,
                PROCESSBY = GetCurrentNTUserId(),
                CREATEDBY = GetCurrentNTUserId(),
                CREATEDDATE = DateTime.Now
            }).OrderBy(o => o.PROCESSBY).ToList();

            tenderNew.CREATEDBY = GetCurrentNTUserId();
            tenderNew.CREATEDDATE = DateTime.Now;

            db.Entry(tenderNew).State = EntityState.Added;
            db.SaveChanges();

            foreach (projectDocumentObject pd in projectDocParam)
            {
                PROJECTDOCUMENT updatePD = db.PROJECTDOCUMENTs.Find(pd.id);
                updatePD.TENDERID = tenderNew.OID;
                updatePD.UPDATEDBY = GetCurrentNTUserId();
                updatePD.UPDATEDDATE = DateTime.Now;

                db.Entry(updatePD).State = EntityState.Modified;
            };
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("listTenderMonitoringByProjectId")]
        public object listTenderMonitoringByProjectId(int param)
        {
            string getContractAwardValue = db.LOOKUPs.Where(w => w.TYPE == constants.TENDERPROGRESS).Max(m => m.VALUE);
            var listTenderMonitoring =
                db.TENDERs.Where(w => w.PROJECTDOCUMENTs.Where(w1 => w1.PROJECTID == param && w1.EXECUTOR == constants.EXECUTOR_CAS).Count() > 0 && w.CONTRACTNO == null).ToList().
                Select(s => new tenderObject
                {
                    id = s.OID,
                    tenderNo = s.TENDERNO,
                    vendorSelectedId = s.VENDORSELECTEDID,
                    vendorSelectedName = s.VENDORSELECTEDNAME,
                    vendorSelectedCode = s.VENDORSELECTEDCODE,
                    contractNo = s.CONTRACTNO,
                    docNoSummary = string.Join(", ", s.PROJECTDOCUMENTs.Select(s1 => s1.DOCNO)),
                    isEnabledSelectVendor = s.TENDERPROGRESSes.FirstOrDefault(w => w.PROCESSTYPE == getContractAwardValue).ACTUALFINISHDATE.HasValue
                });
            return listTenderMonitoring;
        }

        [HttpPut]
        [ActionName("updateTender")]
        public object updateTender(int param, tenderObject tenderParam)
        {
            TENDER updateTender = db.TENDERs.Find(param);
            updateTender.CONTRACTNO = tenderParam.contractNo;
            updateTender.CONTRACTCREATEDDATE = DateTime.Now;
            updateTender.CONTRACTCREATEDBY = GetCurrentNTUserId();
            updateTender.VENDORSELECTEDID = tenderParam.vendorSelectedId;
            updateTender.VENDORSELECTEDCODE = tenderParam.vendorSelectedCode;
            updateTender.VENDORSELECTEDNAME = tenderParam.vendorSelectedName;
            updateTender.UPDATEDBY = GetCurrentNTUserId();
            updateTender.UPDATEDDATE = DateTime.Now;

            db.Entry(updateTender).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(tenderById(param));
        }

        [HttpGet]
        [ActionName("ConstructionMonitoringList")]
        public IEnumerable<tenderObject> ConstructionMonitoringListByProjectId (int param)
        {
            var result = db.TENDERs.Where(w => w.PROJECTDOCUMENTs.Where(w1 => w1.PROJECTID == param).Count() > 0).ToList().Where(w => !string.IsNullOrEmpty(w.CONTRACTNO) && w.VENDORSELECTEDID.HasValue).ToList().Select(s =>
                new tenderObject
                {
                    id = s.OID,
                    tenderNo = s.TENDERNO,
                    vendorSelectedId = s.VENDORSELECTEDID,
                    vendorSelectedCode = s.VENDORSELECTEDCODE,
                    vendorSelectedName = s.VENDORSELECTEDNAME,
                    contractNo = s.CONTRACTNO,
                    docNoSummary = string.Join(", ", s.PROJECTDOCUMENTs.Select(s1 => s1.DOCNO)),
                    //tenderInstallationsMaxDate = s.TENDERINSTALLATIONs.Where(w => w.ISSUESTATUS != constants.STATUSIPROM_COMPLETED || w.ISSUESTATUS != constants.STATUSIPROM_CANCELLED || !w.ACTUALFINISHDATE.HasValue).
                    //                    Max(m => m.PLANFINISHDATE),
                    //tenderPunchlistsMaxDate = s.TENDERPUNCHLISTs.Where(w => w.ISSUESTATUS != constants.STATUSIPROM_COMPLETED || w.ISSUESTATUS != constants.STATUSIPROM_CANCELLED || !w.ACTUALFINISHDATE.HasValue).
                    //                    Max(m => m.PLANFINISHDATE),
                    tenderInstallationsMaxDate = s.TENDERINSTALLATIONs.Where(w => !w.ACTUALFINISHDATE.HasValue).
                                        Max(m => m.PLANFINISHDATE),
                    tenderPunchlistsMaxDate = s.TENDERPUNCHLISTs.Where(w => !w.ACTUALFINISHDATE.HasValue).
                                        Max(m => m.PLANFINISHDATE),

                    contractTrafic = s.TENDERINSTALLATIONs.Count() + s.TENDERPUNCHLISTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : ""
                }).ToList();

            return result.Select(s => new tenderObject
            {
                id = s.id,
                tenderNo = s.tenderNo,
                vendorSelectedId = s.vendorSelectedId,
                vendorSelectedCode = s.vendorSelectedCode,
                vendorSelectedName = s.vendorSelectedName,
                contractNo = s.contractNo,
                docNoSummary = s.docNoSummary,
                contractTrafic = s.contractTrafic == constants.TRAFFICLIGHT_GREY ? s.contractTrafic : s.tenderInstallationsMaxDate.HasValue && s.tenderPunchlistsMaxDate.HasValue ?
                                                traficStatus(new[] { s.tenderPunchlistsMaxDate.Value, s.tenderInstallationsMaxDate.Value }.Max(), null, null) :
                                                s.tenderInstallationsMaxDate.HasValue ?
                                                    traficStatus(s.tenderInstallationsMaxDate.Value, null, null) :
                                                    s.tenderPunchlistsMaxDate.HasValue ?
                                                        traficStatus(s.tenderPunchlistsMaxDate.Value, null, null) :
                                                        constants.TRAFFICLIGHT_GREEN
            });
        }

        [HttpGet]
        [ActionName("ConstructionMonitoringReportListByTenderId")]
        public tenderObject ConstructionMonitoringReportListByTenderId(int param)
        {
            return db.TENDERs.Where(w => w.OID== param).ToList().Select(s =>
                new tenderObject
                {
                    id = s.OID,
                    tenderNo = s.TENDERNO,
                    vendorSelectedId = s.VENDORSELECTEDID,
                    vendorSelectedCode = s.VENDORSELECTEDCODE,
                    vendorSelectedName = s.VENDORSELECTEDNAME,
                    contractNo = s.CONTRACTNO,
                    pmoNo = s.PMONO,
                    docNoSummary = string.Join(", ", s.PROJECTDOCUMENTs.Select(s1 => s1.DOCNO))
                }).FirstOrDefault();
        }
    }
}