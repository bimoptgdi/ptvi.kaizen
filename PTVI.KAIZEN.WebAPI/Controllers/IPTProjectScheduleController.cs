﻿using Microsoft.Office.Project.Server.Library;
using Microsoft.ProjectServer.Client;
using Microsoft.SharePoint.Client;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTVI.KAIZEN.WebAPI.PSI_CustomFieldWS;
using PTVI.KAIZEN.WebAPI.PSI_ProjectWS;
using PTVI.KAIZEN.WebAPI.PSI_QueueWS;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Services.Protocols;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class IPTProjectScheduleController : KAIZENController
    {
        [HttpGet]
        [ActionName("GanttObjectByProjectID")]
        public List<GanttObject> GanttObjectByProjectID(int projID)
        {
            List<GanttObject> ganttObjects = new List<GanttObject>();

            VALE_KAIZENEntities db = new VALE_KAIZENEntities();
            var projectData = db.PROJECTs.Find(projID);
            if (projectData == null)
                throw new ApplicationException("Project not found for id " + projID);

            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectData.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            if (projectDs.Project.Rows.Count > 0)
            {
                var projectWorkingHoursADay = projectDs.Project[0].PROJ_OPT_MINUTES_PER_DAY / 60.0;
                var outlineLevelRows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0", "TASK_OUTLINE_LEVEL") as ProjectDataSet.TaskRow[]; // .Select(x => x.OutlineLevel).Distinct().ToList();
                var outlineLevels = outlineLevelRows.Select(x => x.TASK_OUTLINE_LEVEL).Distinct().ToList();

                foreach (var outlineLevel in outlineLevels)
                {
                    var customId = 1;
                    var tasksInLevel = outlineLevelRows.Where(x => x.TASK_OUTLINE_LEVEL == outlineLevel);

                    foreach (var task in tasksInLevel)
                    {
                        GanttObject parent = null;
                        if (!task.IsTASK_PARENT_UIDNull())
                        {
                            try
                            {
                                parent = ganttObjects.Where(x => x.originalId == task.TASK_PARENT_UID.ToString()).FirstOrDefault();
                            }
                            catch { }
                        }

                        //var z = task.TASK_START_DATE.ToLocalTime();
                        //var utc = task.TASK_START_DATE.ToUniversalTime();

                        ganttObjects.Add(new GanttObject()
                        {
                            originalId = task.TASK_UID.ToString(),
                            id = task.TASK_UID.ToString(),
                            taskOutlineNum = task.TASK_OUTLINE_NUM,
                            expanded = (outlineLevel < outlineLevels[outlineLevels.Count - 1]),
                            title = string.Format("{0} - {1}", task.TASK_OUTLINE_NUM,  task.TASK_NAME),
                            start = task.TASK_START_DATE,
                            end = task.TASK_FINISH_DATE,
                            duration = (double)task.TASK_DUR / 10.0 / 60.0,
                            durationDay = (double)task.TASK_DUR / 10.0 / 60.0 / projectWorkingHoursADay,
                            percentComplete = task.TASK_PCT_COMP,
                            summary = task.TASK_IS_SUMMARY,
                            orderId = customId,
                            parentId = parent == null ? null : parent.originalId,
                            parentOriginalId = parent == null ? null: parent.originalId
                        });

                        customId++;
                    }
                }
            }

            return ganttObjects.Where(x => x.parentId == null || x.parentId != x.id).ToList();
        }

        [HttpGet]
        [ActionName("GanttObjectByProjectShiftID")]
        public List<GanttObject> GanttObjectByProjectShiftID(int ganttShiftId)
        {
            List<GanttObject> ganttObjects = new List<GanttObject>();

            VALE_KAIZENEntities db = new VALE_KAIZENEntities();
            var projectShift = db.PROJECTSHIFTs.Find(ganttShiftId);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + ganttShiftId);

            var shiftTargetDate = projectShift.ENDDATE;

            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectShift.PROJECT.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            if (projectDs.Project.Rows.Count > 0)
            {
                var outlineLevelRows = projectDs.Task.Select("TASK_OUTLINE_LEVEL > 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'", "TASK_OUTLINE_LEVEL") as ProjectDataSet.TaskRow[]; // .Select(x => x.OutlineLevel).Distinct().ToList();
                var outlineLevels = outlineLevelRows.Select(x => x.TASK_OUTLINE_LEVEL).Distinct().ToList();

                foreach (var outlineLevel in outlineLevels)
                {
                    var customId = 1;
                    var tasksInLevel = outlineLevelRows.Where(x => x.TASK_OUTLINE_LEVEL == outlineLevel);

                    foreach (var task in tasksInLevel)
                    {
                        GanttObject parent = null;
                        if (!task.IsTASK_PARENT_UIDNull())
                        {
                            try
                            {
                                parent = ganttObjects.Where(x => x.originalId == task.TASK_PARENT_UID.ToString()).FirstOrDefault();
                            }
                            catch { }
                        }

                        var taskProjectByShift = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShift.OID && x.TASKUID == task.TASK_UID.ToString()).FirstOrDefault();

                        ganttObjects.Add(new GanttObject()
                        {
                            originalId = task.TASK_UID.ToString(),
                            id = task.TASK_UID.ToString(),
                            taskOutlineNum = task.TASK_OUTLINE_NUM,
                            expanded = (outlineLevel < outlineLevels[outlineLevels.Count - 1]),
                            title = string.Format("{0} - {1}", task.TASK_OUTLINE_NUM, task.TASK_NAME),
                            start = task.IsTB_STARTNull() ? task.TASK_START_DATE : task.TB_START,
                            end = task.IsTB_FINISHNull() ? task.TASK_FINISH_DATE : task.TB_FINISH,
                            percentCompletePreviousShift = taskProjectByShift != null ? taskProjectByShift.PERCENTCOMPLETEACTUALBEFORESHIFT : null,
                            percentComplete = task.IsTASK_PCT_COMPNull() ? null : new int?(task.TASK_PCT_COMP),
                            percentCompleteBaseline = taskProjectByShift != null ? taskProjectByShift.PERCENTCOMPLETEBASELINE : null,
                            summary = task.TASK_IS_SUMMARY,
                            orderId = customId,
                            parentId = parent == null ? null : parent.id,
                            parentOriginalId = task.IsTASK_PARENT_UIDNull() ? null : task.TASK_PARENT_UID.ToString() ,
                            actualStart = task.IsTASK_ACT_STARTNull() ? null : new DateTime?(task.TASK_ACT_START),
                            actualEnd = task.IsTASK_ACT_FINISHNull() ? null : new DateTime?(task.TASK_ACT_FINISH)
                        });

                        customId++;
                    }
                }
            }

            return ganttObjects.Where(x=>x.parentId == null || x.parentId != x.id).ToList();
        }

        public static bool IsNull(ClientObject clientObject)
        {
            //check object
            if (clientObject == null)
            {
                //client object is null, so yes, we're null (we can't even check the server object null property)
                return true;
            }
            else if (!clientObject.ServerObjectIsNull.HasValue)
            {
                //server object null property is itself null, so no, we're not null
                return false;
            }
            else
            {
                //server object null check has a value, so that determines if we're null
                return clientObject.ServerObjectIsNull.Value;
            }
        }

        [HttpGet]
        [ActionName("StoreTaskInformationByShift")]
        public HttpResponseMessage StoreTaskInformationByShift(int prjShiftID, string progressCondition)
        {
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(prjShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + prjShiftID);

            if (!string.IsNullOrEmpty(progressCondition))
            {
                projectShift.PROGRESSCONDITION = progressCondition;
                db.SaveChanges();
            }

            var projectData = projectShift.PROJECT;
            var prjName = projectData.PROJECTDESCRIPTION;

            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectData.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

            //string root = System.Web.HttpContext.Current.Server.MapPath("~/App_Data");

            //var filePath = System.IO.Path.Combine(root, "ProjectDataset.xml");
            //projectDs.WriteXml(filePath);

            // check if need to refresh table
            var isNeedToRefresh = db.PROJECTTASKBYSHIFTs.Count(x => x.ESTIMATIONSHIFTID == projectShift.OID && x.PERCENTCOMPLETEACTUALAFTERSHIFT.HasValue) == 0;
            if (isNeedToRefresh)
            {
                // clear table for this shift
                db.Database.ExecuteSqlCommand("DELETE FROM [PROJECTTASKBYSHIFT] WHERE ESTIMATIONSHIFTID = " + projectShift.OID);
                db.Database.ExecuteSqlCommand("DELETE FROM [MASTERTASKBASELINEBYSHIFT] WHERE ESTIMATIONSHIFTID = " + projectShift.OID);
            }

            var tasksToBeProceed = projectShift.PROGRESSCONDITION == constants.IPT_PROGRESSCONDITION_DELAY ? 
                                    projectDs.Task.Select("TB_START < '" + projectShift.ENDDATE.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'" +
                                        " AND TASK_PCT_COMP <> 100 AND TASK_OUTLINE_LEVEL > 0") : projectDs.Task.Select("TASK_START_DATE < '" + projectShift.ENDDATE.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'" +
                                        " AND TASK_PCT_COMP <> 100 AND TASK_OUTLINE_LEVEL > 0");

            if (tasksToBeProceed.Length == 0)
            {
                throw new ApplicationException("Tasks not found.");
            }

            foreach (var row in projectDs.Task.Rows)
            {
                var taskRow = row as ProjectDataSet.TaskRow;

                //if (taskRow.TASK_START_DATE < projectShift.ENDDATE.Value && taskRow.TASK_PCT_COMP != 100 && taskRow.TASK_OUTLINE_LEVEL > 0)
                if (taskRow.TASK_OUTLINE_LEVEL > 0 && (projectShift.PROGRESSCONDITION == constants.IPT_PROGRESSCONDITION_DELAY ? taskRow.TB_START < projectShift.ENDDATE.Value : taskRow.TASK_START_DATE < projectShift.ENDDATE.Value) && taskRow.TASK_PCT_COMP != 100)
                {
                    var executor = "";

                    var filterExecutorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                               "MD_PROP_UID = '" + cfExecutor1Uid + "'");
                    if (filterExecutorRows.Length > 0)
                    {
                        executor = filterExecutorRows[0].TEXT_VALUE;
                    }

                    var projectTaskByShift = db.PROJECTTASKBYSHIFTs
                                        .Where(x => x.ESTIMATIONSHIFTID == projectShift.OID &&
                                                x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();
                    if (projectTaskByShift == null)
                    {
                        projectTaskByShift = new PROJECTTASKBYSHIFT()
                        {
                            ESTIMATIONSHIFTID = projectShift.OID,
                            PROJECTID = projectData.OID,
                            TASKNAME = taskRow.TASK_NAME,
                            PARENTTASKUID = taskRow.IsTASK_PARENT_UIDNull() ? null : taskRow.TASK_PARENT_UID.ToString(),
                            OUTLINELEVEL = taskRow.TASK_OUTLINE_LEVEL,
                            OUTLINEPOSITION = taskRow.TASK_OUTLINE_NUM,
                            ISSUMMARY = taskRow.TASK_IS_SUMMARY,
                            BASELINESTART = taskRow.IsTB_STARTNull() ? null : new DateTime?(taskRow.TB_START),
                            BASELINEFINISH = taskRow.IsTB_FINISHNull() ? null : new DateTime?(taskRow.TB_FINISH),
                            BASELINEDURATION = taskRow.IsTB_DURNull() ? null : new int?(taskRow.TB_DUR),
                            STARTDATE = taskRow.IsTASK_START_DATENull() ? null : new DateTime?(taskRow.TASK_START_DATE),
                            FINISHDATE = taskRow.IsTASK_FINISH_DATENull() ? null : new DateTime?(taskRow.TASK_FINISH_DATE),
                            ESTDURATION = taskRow.IsTASK_DURNull() ? null : new int?(taskRow.TASK_DUR),
                            ACTUALFINISH = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH),
                            ACTUALSTART = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START),
                            ACTUALDURATION = taskRow.IsTASK_ACT_DURNull() ? null : new int?(taskRow.TASK_ACT_DUR),
                            PERCENTCOMPLETEBASELINE = null, // update when print shift target (simulation calculation)
                            PERCENTCOMPLETEACTUALAFTERSHIFT = null,
                            PERCENTCOMPLETEACTUALBEFORESHIFT = taskRow.IsTASK_PCT_COMPNull() ? null : new int?(taskRow.TASK_PCT_COMP),
                            CONTRACTOR = executor,
                            OPTIMISTICFINISH = null,
                            ORIGINALFINISH = null,
                            TASKUID = taskRow.TASK_UID.ToString(),
                            REALISTICFINISH = null,
                            CREATEDBY = GetCurrentNTUserId(),
                            CREATEDDATE = DateTime.Now
                        };

                        db.PROJECTTASKBYSHIFTs.Add(projectTaskByShift);
                    }
                    else
                    {
                        projectTaskByShift.TASKNAME = taskRow.TASK_NAME;
                        projectTaskByShift.PARENTTASKUID = taskRow.IsTASK_PARENT_UIDNull() ? null : taskRow.TASK_PARENT_UID.ToString();
                        projectTaskByShift.OUTLINELEVEL = taskRow.TASK_OUTLINE_LEVEL;
                        projectTaskByShift.OUTLINEPOSITION = taskRow.TASK_OUTLINE_NUM;
                        projectTaskByShift.ISSUMMARY = taskRow.TASK_IS_SUMMARY;
                        projectTaskByShift.BASELINESTART = taskRow.IsTB_STARTNull() ? null : new DateTime?(taskRow.TB_START);
                        projectTaskByShift.BASELINEFINISH = taskRow.IsTB_FINISHNull() ? null : new DateTime?(taskRow.TB_FINISH);
                        projectTaskByShift.BASELINEDURATION = taskRow.IsTB_DURNull() ? null : new int?(taskRow.TB_DUR);
                        projectTaskByShift.STARTDATE = taskRow.IsTASK_START_DATENull() ? null : new DateTime?(taskRow.TASK_START_DATE);
                        projectTaskByShift.FINISHDATE = taskRow.IsTASK_FINISH_DATENull() ? null : new DateTime?(taskRow.TASK_FINISH_DATE);
                        projectTaskByShift.ESTDURATION = taskRow.IsTASK_DURNull() ? null : new int?(taskRow.TASK_DUR);
                        projectTaskByShift.ACTUALFINISH = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH);
                        projectTaskByShift.ACTUALSTART = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START);
                        projectTaskByShift.ACTUALDURATION = taskRow.IsTASK_ACT_DURNull() ? null : new int?(taskRow.TASK_ACT_DUR);
                        projectTaskByShift.CONTRACTOR = executor;
                        //projectTaskByShift.PERCENTCOMPLETEACTUALBEFORESHIFT = taskRow.TASK_PCT_COMP;
                        //projectTaskByShift.OPTIMISTICFINISH = null;
                        //projectTaskByShift.ORIGINALFINISH = null;
                        //projectTaskByShift.REALISTICFINISH = null;

                        db.Entry(projectTaskByShift).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                var taskBaselineByShift = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShift.OID &&
                                                x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();
                if (taskBaselineByShift == null)
                {
                    taskBaselineByShift = new MASTERTASKBASELINEBYSHIFT()
                    {
                        ESTIMATIONSHIFTID = projectShift.OID,
                        TASKUID = taskRow.TASK_UID.ToString(),
                        CREATEDBY = GetCurrentNTUserId(),
                        CREATEDDATE = DateTime.Now
                    };

                    db.MASTERTASKBASELINEBYSHIFTs.Add(taskBaselineByShift);
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException exc)
            {
                string validationMsg = "";
                foreach(var err in exc.EntityValidationErrors)
                {
                    validationMsg += (validationMsg == "" ? "" : "\\n") + err.Entry.Entity.ToString();
                    foreach (var validationError in err.ValidationErrors)
                        validationMsg += (validationMsg == "" ? "" : ", ") + validationError.PropertyName + ": " + validationError.ErrorMessage;
                }


                throw new ApplicationException(validationMsg);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("GenerateShiftTarget")]
        public IEnumerable<shiftTargetGroup> GenerateShiftTarget(int projectShiftID)
        {
            var maxTaskLevel = shiftTargetMaxTaskLevel;

            VALE_KAIZENEntities db = new VALE_KAIZENEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            CheckProjectState(projectUid);

            Guid sessionUid = Guid.NewGuid();
            try
            {
                ProjectWS.CheckOutProject(projectUid, sessionUid, "Generate shift target");
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                var hasCheckedOut = false;
                var message = "";
                foreach (var err in errors)
                {
                    if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutInOtherSession)
                    {
                        message = "You have checkout this project in another session. Please checkin it and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutToOtherUser)
                    {
                        message = "Other user has checkout this project. Please wait it is checked in and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOAlreadyCheckedOutInSameSession ||
                        err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCannotCheckOutVisibilityModeProjectWithMppInDocLib)
                    {
                        message = "This project has been checkout. Please wait it is checked in and try again";
                        hasCheckedOut = true;
                    }
                }

                if (hasCheckedOut)
                    throw new ApplicationException(message);
                else
                    throw exc;
            }

            var projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var projectName = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow).PROJ_NAME;

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);

            // initialize for shiftworkduration calculation
            //try
            //{
                // set to zero
                //var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var excludeLeaves = projectShift.PROGRESSCONDITION == constants.IPT_PROGRESSCONDITION_DELAY ? 
                                        projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'") :
                                        projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");

                foreach (var leave in excludeLeaves)
                {
                    var t = leave as ProjectDataSet.TaskRow;

                    // set start and end equal date to make it value 0
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                }

                //var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var includedLeaves = projectShift.PROGRESSCONDITION == constants.IPT_PROGRESSCONDITION_DELAY ?
                                            projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'") :
                                            projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var row in includedLeaves)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        //filterStartDateRows[0].DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START; // use this to calculate baseline duration
                        filterStartDateRows[0].DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START; 
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                        newStartDateCFRow.DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                    }


                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                        endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);
                    }
                }

                if (projectDs.TaskCustomFields.GetChanges() != null && projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                {
                    // publish project to appy calculation
                    Guid jobUid = Guid.NewGuid();
                    var changesDS = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                    ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changesDS, false); // use QueueUpdateProject2 to process scheduling engine

                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }
                else
                {

                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }

                var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();

                var groupList = new List<shiftTargetGroup>();
                var levelMaxRows = tasks.Where(x => x.OUTLINELEVEL == maxTaskLevel).ToList();
                while (levelMaxRows.Count == 0)
                {
                    maxTaskLevel--;
                    levelMaxRows = tasks.Where(x => x.OUTLINELEVEL == maxTaskLevel).ToList();
                }

                foreach (var row in levelMaxRows)
                {
                    var taskRow = projectDs.Task.Select("TASK_UID = '" + row.TASKUID + "'").FirstOrDefault() as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    groupList.Add(new shiftTargetGroup()
                    {
                        ID = taskRow.TASK_UID,
                        groupName = projectName,
                        projectName = taskRow.TASK_NAME,
                        shiftName = projectShift.SHIFTID.ToString(),
                        Date = projectShift.STARTDATE.Value, //DateTime.Now,
                        percentComplete = taskRow.IsTASK_PCT_COMPNull() ? null : new int?(taskRow.TASK_PCT_COMP),
                        outlineNumber = taskRow.TASK_OUTLINE_NUM,
                        outlineLevel = taskRow.TASK_OUTLINE_LEVEL
                    });
                }

                List<shiftTarget> result = new List<shiftTarget>();
                foreach (var row in tasks)
                {
                    var taskRow = projectDs.Task.Select("TASK_UID = '" + row.TASKUID + "'").FirstOrDefault() as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    if (!row.ISSUMMARY.Value && tasks.Count(x => x.PARENTTASKUID == row.TASKUID) == 0)
                    {
                        shiftTarget st = new shiftTarget()
                        {
                            percentCompletePrevious = taskRow.TASK_PCT_COMP,
                            actualStart = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START),
                            actualFinish = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH),
                            taskID = taskRow.TASK_UID,
                            taskOutlineNum = taskRow.TASK_OUTLINE_NUM,
                            taskOutlineLevel = taskRow.TASK_OUTLINE_LEVEL,
                            titleList = new List<string>()
                        };

                        populateShiftTarget(taskRow, st, maxTaskLevel);

                        result.Add(st);
                    }
                }

                calculateMasterTaskBaseline(ProjectWS, projectUid, projectShiftID);

                calculateProgressByDate(projectDs, ProjectWS, projectUid, ref result, projectShiftID);

                populateShiftCustomField(projectDs, ref result, new Guid(cfExecutor1Uid), new Guid(cfSupervisorUid), new Guid(cfInPlannerUid), new Guid(cfCMUid));

                var groupNewList = new List<shiftTargetGroup>();

                populateGroupList(groupList, result, ref groupNewList);

                //set to group
                foreach (var group in groupNewList)
                {
                    group.shiftTargetList = result.Where(x => x.groupTaskId == group.ID && x.Executor == group.Executor && x.ConstructionManager == group.ConstructionManager).OrderBy(x=>x.taskOutlineNum).ToList();
                }

                return groupNewList.OrderBy(x => x.Executor).ThenBy(x=>x.ConstructionManager).ThenBy(x=>x.projectName);
            //}
            //catch (Exception exc)
            //{
            //    try
            //    {
            //        Guid jobUid = Guid.NewGuid();
            //        ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

            //        WaitForQueue(q, jobUid);
            //    }
            //    catch{};

            //    throw exc;
            //}
        }

        

        private void populateGroupList(List<shiftTargetGroup> groupList, List<shiftTarget> shiftTargetList, ref List<shiftTargetGroup> groupNewList)
        {
            var shiftCount = shiftTargetList.Select(e => new { e.groupTaskId, e.Executor, e.Supervisor, e.Planner, e.ConstructionManager})
                            .Distinct().ToList();
            foreach (var shift in shiftCount)
            {
                var selectedGroup = groupList.Where(x => x.ID == shift.groupTaskId).FirstOrDefault();
                shiftTargetGroup stg = new shiftTargetGroup()
                {
                    ID = selectedGroup.ID,
                    projectName = selectedGroup.projectName,
                    groupName = selectedGroup.groupName,
                    Executor = shift.Executor,
                    Supervisor = shift.Supervisor,
                    Planner = shift.Planner,
                    ConstructionManager = shift.ConstructionManager,
                    Date = selectedGroup.Date,
                    percentComplete = selectedGroup.percentComplete,
                    outlineNumber = selectedGroup.outlineNumber,
                    shiftName = selectedGroup.shiftName
                };
                groupNewList.Add(stg);
            }

        }

        private void populateShiftCustomField(ProjectDataSet projectDs, ref List<shiftTarget> shiftTargetList, Guid executorCFUid, Guid supervisorCFUid, Guid inPlannerCFUid, Guid cmCFUid)
        {
            foreach (var shiftTarget in shiftTargetList)
            {
                var filterExecutorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + executorCFUid + "'");
                if (filterExecutorRows.Length > 0)
                {
                    shiftTarget.Executor = filterExecutorRows[0].TEXT_VALUE;
                }

                var filterSupervisorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + supervisorCFUid + "'");
                if (filterSupervisorRows.Length > 0)
                {
                    shiftTarget.Supervisor = filterSupervisorRows[0].TEXT_VALUE;
                }

                var filterInPlannerRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + inPlannerCFUid + "'");
                if (filterInPlannerRows.Length > 0)
                {
                    shiftTarget.Planner = filterInPlannerRows[0].TEXT_VALUE;
                }

                var filterCMRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + cmCFUid + "'");
                if (filterCMRows.Length > 0)
                {
                    shiftTarget.ConstructionManager = filterCMRows[0].TEXT_VALUE;
                }
            }
        }

        private void populateGroupsCustomField(ProjectDataSet projectDs, ref List<shiftTargetGroup> groupList, 
            Guid executorCFUid, Guid supervisorCFUid, Guid inPlannerCFUid, Guid cmCFUid)
        {
            foreach (var group in groupList)
            {
                var filterExecutorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + executorCFUid + "'");
                if (filterExecutorRows.Length > 0)
                {
                    group.Executor = filterExecutorRows[0].TEXT_VALUE;
                }

                var filterSupervisorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" +supervisorCFUid + "'");
                if (filterSupervisorRows.Length > 0)
                {
                    group.Supervisor = filterSupervisorRows[0].TEXT_VALUE;
                }

                var filterInPlannerRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + inPlannerCFUid + "'");
                if (filterInPlannerRows.Length > 0)
                {
                    group.Planner = filterInPlannerRows[0].TEXT_VALUE;
                }

                var filterCMRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + cmCFUid + "'");
                if (filterCMRows.Length > 0)
                {
                    group.ConstructionManager = filterCMRows[0].TEXT_VALUE;
                }
            }
        }

        private void calculateProgressByDate(ProjectDataSet projectDs, PSI_ProjectWS.Project ProjectWS, Guid projectId,
            ref List<shiftTarget> shiftTarget, int projectShiftID)
        {                    
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            // read calcuation result
            PSI_ProjectWS.ProjectDataSet projectDs3 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var durationCFUid = new Guid(cfCalcPlanDurationUid);
            var cfTotalDurationUID = new Guid(cfTotalDuration);
            foreach (var st in shiftTarget)
            {
                // get task total duration 
                var taskDur = 0.0;

                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + st.taskID + "' and " +
                        "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + st.taskID + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");
                
                if (shiftWorkDurationRows.Length > 0)
                {
                    if (taskDur == 0)
                        st.percentCompletePlan = 100;
                    else
                    {
                        var shiftWorkDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                        var percentComplete = (shiftWorkDurationMinute / taskDur) * 100.00;

                        if (percentComplete >= 100)
                            st.percentCompletePlan = 100;
                        else if (percentComplete < 0)
                            st.percentCompletePlan = 0;
                        else
                            st.percentCompletePlan = Convert.ToInt32(percentComplete);
                    }
                }
            }

            // update percentCompleteBaseline for all task and its parents
            var projectTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();

            foreach (var task in projectTasks)
            {
                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + task.TASKUID + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + task.TASKUID + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        var taskRow = projectDs.Task.Select("TASK_UID = '" + task.TASKUID + "'").FirstOrDefault() as ProjectDataSet.TaskRow;

                        // update projecttaskbyshift
                        task.PERCENTCOMPLETEBASELINE = percentComplete > 100 ? 100 : Convert.ToInt32(percentComplete);
                        //task.PERCENTCOMPLETEACTUALBEFORESHIFT = taskRow.IsTASK_PCT_COMPNull() ? null : new int?(taskRow.TASK_PCT_COMP);

                        db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                    }
                    catch { }
                }
            }

            db.SaveChanges();
        }

        private void calculateMasterTaskBaseline(PSI_ProjectWS.Project ProjectWS, Guid projectId, int projectShiftID)
        {
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            // read calcuation result
            PSI_ProjectWS.ProjectDataSet projectDs3 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var durationCFUid = new Guid(cfCalcPlanDurationUid);
            var cfTotalDurationUID = new Guid(cfTotalDuration);

            // update percentCompleteBaseline for all task and its parents
            var taskBaselineByShift = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();

            foreach (var task in taskBaselineByShift)
            {
                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + task.TASKUID + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs3.TaskCustomFields.Select("TASK_UID = '" + task.TASKUID + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        // update projecttaskbyshift
                        task.PERCENTCOMPLETEBASELINE = percentComplete > 100 ? 100 : Convert.ToInt32(percentComplete);

                        db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                    }
                    catch { }
                }
            }

            db.SaveChanges();
        }

        [HttpGet]
        [ActionName("publishedShiftTarget")]
        public object publishedShiftTarget(int param, int y)
        {
            var prjName = "Project Demo - 2";

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            var publishedProjects = prjContext.LoadQuery(prjContext.Projects.Where(x => x.Name == prjName));
            prjContext.ExecuteQuery();

            var publishedProject = publishedProjects.First();
            var pTasks = prjContext.LoadQuery(publishedProject.Tasks.Where(x => x.Name == "Safety Preparation - Mud Gun Rail Installation").IncludeWithDefaultProperties(t => t.CustomFields));
            prjContext.ExecuteQuery();

            var projectGuid = publishedProject.Id.ToString();

            var pTask = pTasks.First();
            var taskGuid = pTask.Id.ToString();


            var cfStDateP = "";
            var cfEndDateP = "";
            var cfDurationP = "";
            var cfDurationNP = "";
            var cfStartDate = "CF_CalcStartDate";
            var CF_CalcEndDate = "CF_CalcEndDate";
            var CF_CalcDuration = "CF_CalcDuration";
            var CF_CalcDurationN = "CF_CalcDurationN";
            var cfStDateGuid = "";
            var cfEdDateGuid = "";


            foreach (var cf in pTask.CustomFields)
            {
                if (cf.Name == cfStartDate)
                {
                    cfStDateGuid = cf.Id.ToString();
                    cfStDateP = pTask[cf.InternalName].ToString();
                }
                else if (cf.Name == CF_CalcEndDate)
                {
                    cfEdDateGuid = cf.Id.ToString();
                    cfEndDateP = pTask[cf.InternalName].ToString();
                }
                else if (cf.Name == CF_CalcDuration)
                {
                    cfDurationP = pTask[cf.InternalName].ToString();
                }
                else if (cf.Name == CF_CalcDurationN)
                {
                    cfDurationNP = pTask[cf.InternalName].ToString();
                }
            }



            var a = cfStDateP;
            var b = cfEndDateP;
            var c = cfDurationP;
            var d = cfDurationNP;

            return new
            {
                startDate = a,
                endDate = b,
                durarion = d
            };
        }

        private void populateShiftTarget(ProjectDataSet.TaskRow task, shiftTarget shiftTarget, int outlineLevelMax)
        {
            if (task.TASK_OUTLINE_LEVEL >= outlineLevelMax)
            {
                shiftTarget.titleList.Add(task.TASK_NAME);

                if (task.TASK_OUTLINE_LEVEL == outlineLevelMax)
                    shiftTarget.groupTaskId = task.TASK_UID;

                var taskRows = task.Table.Select("TASK_UID = '" + task.TASK_PARENT_UID + "'");

                if (taskRows.Length > 0)
                {
                    var parentTaskRow = taskRows[0] as ProjectDataSet.TaskRow;

                    populateShiftTarget(parentTaskRow, shiftTarget, outlineLevelMax);
                }
            }
        }


        static private void WaitForQueue(QueueSystem q, Guid jobId)
        {
            PSI_QueueWS.JobState jobState;
            const int QUEUE_WAIT_TIME = 2; // two seconds
            bool jobDone = false;
            string xmlError = string.Empty;
            int wait = 0;

            // Wait for the project to get through the queue.
            // Get the estimated wait time in seconds.
            wait = q.GetJobWaitTime(jobId);

            // Wait for it.
            System.Threading.Thread.Sleep(wait * 1000);
            // Wait until it is finished.

            do
            {
                // Get the job state.
                jobState = q.GetJobCompletionState(jobId, out xmlError);

                if (jobState == PSI_QueueWS.JobState.Success)
                {
                    jobDone = true;
                }
                else
                {
                    if (jobState == PSI_QueueWS.JobState.Unknown
                    || jobState == PSI_QueueWS.JobState.Failed
                    || jobState == PSI_QueueWS.JobState.FailedNotBlocking
                    || jobState == PSI_QueueWS.JobState.CorrelationBlocked
                    || jobState == PSI_QueueWS.JobState.Canceled)
                    {
                        // If the job failed, error out.
                        throw (new ApplicationException("Queue request " + jobState + " for Job ID " + jobId + ".\r\n" + xmlError));
                    }
                    else
                    {
                        Console.WriteLine("Job State: " + jobState + " for Job ID: " + jobId);
                        System.Threading.Thread.Sleep(QUEUE_WAIT_TIME * 1000);
                    }
                }
            }
            while (!jobDone);
        }

        [HttpGet]
        [ActionName("CustomFields")]
        public HttpResponseMessage CustomFields(string param)
        {
            PSI_CustomFieldWS.CustomFields cfWs = new PSI_CustomFieldWS.CustomFields();
            cfWs.Credentials = pwaCredential;

            CustomFieldDataSet cfDS = new CustomFieldDataSet();

            Filter cfFilter = new Microsoft.Office.Project.Server.Library.Filter();
            cfFilter.FilterTableName = cfDS.CustomFields.TableName;

            cfFilter.Fields.Add(new Filter.Field(cfDS.CustomFields.TableName, cfDS.CustomFields.MD_PROP_NAMEColumn.ColumnName));
            cfFilter.Fields.Add(new Filter.Field(cfDS.CustomFields.TableName, cfDS.CustomFields.MD_PROP_IDColumn.ColumnName));
            cfFilter.Fields.Add(new Filter.Field(cfDS.CustomFields.TableName, cfDS.CustomFields.MD_PROP_UIDColumn.ColumnName));
            cfFilter.Fields.Add(new Filter.Field(cfDS.CustomFields.TableName, cfDS.CustomFields.MD_LOOKUP_TABLE_UIDColumn.ColumnName));
            cfFilter.Fields.Add(new Filter.Field(cfDS.CustomFields.TableName, cfDS.CustomFields.MD_PROP_TYPE_ENUMColumn.ColumnName));

            cfDS = cfWs.ReadCustomFields(cfFilter.GetXml(), false);
            
            return Request.CreateResponse(HttpStatusCode.OK, cfDS);
        }

        [HttpPut]
        [ActionName("updateCompleteProgress")]
        public HttpResponseMessage updateCompleteProgress(int projectShiftID, List<TaskPercentComplete> percentCompletes)
        {
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            CheckProjectState(projectUid);

            Guid sessionUid = Guid.NewGuid();
            ProjectWS.CheckOutProject(projectUid, sessionUid, "Update percent complete");

            try
            {
                PSI_ProjectWS.ProjectDataSet projectDs =
                    ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                foreach (var item in percentCompletes)
                {
                    var rows = projectDs.Task.Select("TASK_UID = '" + item.taskId + "'") as ProjectDataSet.TaskRow[];

                    if (rows.Length > 0)
                    {
                        //if (item.actualStartDate.HasValue)
                        //    rows[0].TASK_ACT_START = item.actualStartDate.Value > DateTime.Now ? DateTime.Now : item.actualStartDate.Value;
                        if (item.actualStartDate.HasValue && (rows[0].IsTASK_ACT_STARTNull() || item.actualStartDate.Value != rows[0].TASK_ACT_START))
                        {
                            WriteLog("actualstart date is different with project server, task: " + item.taskId + "; actual start: " + item.actualStartDate.Value.ToString("yyyyMMddTHH:mm:ss") + "; server act start: " + (rows[0].IsTASK_ACT_STARTNull() ? "null" : rows[0].TASK_ACT_START.ToString("yyyyMMddTHH:mm:ss")));
                            rows[0].TASK_ACT_START = item.actualStartDate.Value;
                        }

                        if (item.actualEndDate.HasValue && (rows[0].IsTASK_ACT_FINISHNull() || item.actualEndDate.Value != rows[0].TASK_ACT_FINISH))
                        {
                            WriteLog("actualend date is different with project server, task: " + item.taskId + "; actual end: " + item.actualEndDate.Value.ToString("yyyyMMddTHH:mm:ss") + "; server act end: " + (rows[0].IsTASK_ACT_FINISHNull() ? "null" : rows[0].TASK_ACT_FINISH.ToString("yyyyMMddTHH:mm:ss")));
                            rows[0].TASK_ACT_FINISH = item.actualEndDate.Value;
                        }

                        rows[0].TASK_PCT_COMP = item.percentComplete;
                    }
                }

                foreach (ProjectDataSet.TaskRow taskRow in projectDs.Task)
                {
                    var filterEndShiftTimeRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                    "MD_PROP_UID = '" + cfEndShiftTime + "'");
                    if (filterEndShiftTimeRows.Length > 0)
                    {
                        filterEndShiftTimeRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newEndShiftTimeCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newEndShiftTimeCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newEndShiftTimeCFRow.TASK_UID = taskRow.TASK_UID;
                        newEndShiftTimeCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newEndShiftTimeCFRow.MD_PROP_UID = new Guid(cfEndShiftTime);

                        newEndShiftTimeCFRow.DATE_VALUE = shiftTargetDate.Value;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newEndShiftTimeCFRow);
                    }
                }

                Guid jobUid = Guid.NewGuid();
                var changedDs = projectDs.GetChanges() as ProjectDataSet;

                ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs, false);
                WaitForQueue(q, jobUid);

                projectDs = ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                var localTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();

                var cfRealisticStartDate = new Guid(cfCalcRealisticDurationStartUid);
                var cfRealisticEndDate = new Guid(cfCalcRealisticDurationEndDateUid);
                var cfOptimisticStartDate = new Guid(cfCalcOptimisticDurationStartDate);
                var cfOptimisticEndDate = new Guid(cfCalcOptimisticDurationEndDate);
                var cfRealisticDuration = new Guid(cfCalcRealisticDuration);
                var cfOptimisticDuration = new Guid(cfCalcOptimisticDuration);

                var cfRemainingOptimistic = new Guid(cfCalcRemainingOptimistic);
                foreach (ProjectDataSet.TaskRow taskRow in projectDs.Task)
                {
                    var filterDurationForRealisticCalc = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfDurationForRealisticCalc + "'");

                    if (taskRow.IsTB_DURNull())
                        continue;

                    // OPTIMISTIC
                    var remainingOptimistic = (double)taskRow.TB_DUR;
                    if (!taskRow.IsTASK_PCT_COMPNull() && taskRow.TASK_PCT_COMP != 0)
                    {
                        if (taskRow.TASK_PCT_COMP == 100)
                            remainingOptimistic = 0.0;
                        else if (filterDurationForRealisticCalc.Length > 0 && filterDurationForRealisticCalc[0].NUM_VALUE >= 0)                 
                            remainingOptimistic = ((100.0 - (double)taskRow.TASK_PCT_COMP) / 100.0) * (double)taskRow.TB_DUR;
                    }

                    var filterRemainingOptimisticEachRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");

                    if (filterRemainingOptimisticEachRows.Length > 0)
                    {
                        filterRemainingOptimisticEachRows[0].NUM_VALUE = (decimal)remainingOptimistic;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow remainingOptimisticEachCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        remainingOptimisticEachCFRow.PROJ_UID = taskRow.PROJ_UID;
                        remainingOptimisticEachCFRow.TASK_UID = taskRow.TASK_UID;
                        remainingOptimisticEachCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        remainingOptimisticEachCFRow.MD_PROP_UID = new Guid(cfRemainingOptimisticEach);

                        remainingOptimisticEachCFRow.NUM_VALUE = (decimal)remainingOptimistic;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(remainingOptimisticEachCFRow);

                    }

                    var remainingRealistic = (double)taskRow.TB_DUR;
                    if (filterDurationForRealisticCalc.Length > 0 && filterDurationForRealisticCalc[0].NUM_VALUE >= 0) {
                        if (!taskRow.IsTASK_PCT_COMPNull() && taskRow.TASK_PCT_COMP != 0)
                            remainingRealistic = (100.0 / (double)taskRow.TASK_PCT_COMP) * ((double)filterDurationForRealisticCalc[0].NUM_VALUE) * 10 * ((100.0 - (double)taskRow.TASK_PCT_COMP) / 100);
                        else
                            remainingRealistic = remainingOptimistic;
                    }                    

                    var filterRemainingRealisticEachRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");

                    if (filterRemainingRealisticEachRows.Length > 0)
                    {
                        filterRemainingRealisticEachRows[0].NUM_VALUE = (decimal)remainingRealistic;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow remainingRealisticEachCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        remainingRealisticEachCFRow.PROJ_UID = taskRow.PROJ_UID;
                        remainingRealisticEachCFRow.TASK_UID = taskRow.TASK_UID;
                        remainingRealisticEachCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        remainingRealisticEachCFRow.MD_PROP_UID = new Guid(cfRemainingRealisticEach);

                        remainingRealisticEachCFRow.NUM_VALUE = (decimal)remainingRealistic;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(remainingRealisticEachCFRow);

                    }
                }

                jobUid = Guid.NewGuid();
                var changedDs2 = projectDs.GetChanges() as ProjectDataSet;
                ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs2, false);
                WaitForQueue(q, jobUid);

                jobUid = Guid.NewGuid();
                ProjectWS.QueuePublish(jobUid, projectUid, true, null);
                WaitForQueue(q, jobUid);

                jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Update percent complete");
                WaitForQueue(q, jobUid);

                // update local task based on server result
                projectDs = ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);
                var masterBaselineTasks = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();
                if (projectDs.Task.Rows.Count > 0)
                {                    
                    foreach (ProjectDataSet.TaskRow taskRow in projectDs.Task)
                    {
                        var localTask = localTasks.Where(x => x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();

                        if (localTask != null)
                        {
                            localTask.BASELINESTART = taskRow.IsTB_STARTNull() ? null : new DateTime?(taskRow.TB_START);
                            localTask.BASELINEFINISH = taskRow.IsTB_FINISHNull() ? null : new DateTime?(taskRow.TB_FINISH);
                            localTask.BASELINEDURATION = taskRow.IsTB_DURNull() ? null : new int?(taskRow.TB_DUR);

                            localTask.STARTDATE = taskRow.IsTASK_START_DATENull() ? null : new DateTime?(taskRow.TASK_START_DATE);
                            localTask.FINISHDATE = taskRow.IsTASK_FINISH_DATENull() ? null : new DateTime?(taskRow.TASK_FINISH_DATE);
                            localTask.ESTDURATION = taskRow.IsTASK_DURNull() ? null : new int?(taskRow.TASK_DUR);

                            localTask.ACTUALSTART = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START);
                            localTask.ACTUALFINISH = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH);
                            localTask.ACTUALDURATION = taskRow.IsTASK_ACT_DURNull() ? null : new int?(taskRow.TASK_ACT_DUR);

                            localTask.PERCENTCOMPLETEACTUALAFTERSHIFT = taskRow.TASK_PCT_COMP;

                            var originalFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOriginalFinishEach + "'");
                            var originalFinish = originalFinishfilteredRows.Length == 0 ? null : new DateTime?(originalFinishfilteredRows[0].DATE_VALUE);

                            localTask.REMAININGORIGINAL = taskRow.TASK_REM_DUR;
                            localTask.ORIGINALFINISH = originalFinish;

                            var remainingOptimisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");
                            var remainingOptimistic = remainingOptimisticfilteredRows.Length == 0 ? null : new Decimal?(remainingOptimisticfilteredRows[0].NUM_VALUE);

                            var optimisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOptimisticFinishEach + "'");
                            DateTime? optimisticFinish = optimisticFinishfilteredRows.Length == 0 ? null : (optimisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishfilteredRows[0].DATE_VALUE));

                            var optimisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOptimisticFinishEachF + "'");
                            DateTime? optimisticFinishF = optimisticFinishFfilteredRows.Length == 0 || optimisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishFfilteredRows[0].DATE_VALUE);

                            var optimisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOptimisticDurationEach + "'");
                            var optimisticDuration = optimisticDurationfilteredRows.Length == 0 ? null : new Decimal?(optimisticDurationfilteredRows[0].NUM_VALUE);

                            if (remainingOptimistic.HasValue)
                                localTask.REMAININGOPTIMISTIC = (double) remainingOptimistic;

                            if (optimisticFinish.HasValue)
                                localTask.OPTIMISTICFINISH = optimisticFinish > optimisticFinishF ? optimisticFinish : optimisticFinishF;
                            else
                                localTask.OPTIMISTICFINISH = optimisticFinishF;

                            if (optimisticDuration.HasValue)
                                localTask.OPTIMISTICDURATION = (double) optimisticDuration * 10.0;

                            // REALISTIC
                            var remainingRealisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");
                            var remainingRealistic = remainingRealisticfilteredRows.Length == 0 ? null : new decimal?(remainingRealisticfilteredRows[0].NUM_VALUE);

                            var realisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticFinishEach + "'");
                            DateTime? realisticFinish = realisticFinishfilteredRows.Length == 0 ? null : (realisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishfilteredRows[0].DATE_VALUE));

                            var realisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticFinishEachF + "'");
                            DateTime? realisticFinishF = realisticFinishFfilteredRows.Length == 0 || realisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishFfilteredRows[0].DATE_VALUE);

                            var realisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticDurationEach + "'");
                            var realisticDuration = realisticDurationfilteredRows.Length == 0 ? null : new Decimal?(realisticDurationfilteredRows[0].NUM_VALUE);

                            if (remainingOptimistic.HasValue)
                                localTask.REMAININGREALISTIC = (double)remainingRealistic;

                            if (realisticDuration.HasValue)
                                localTask.REALISTICDURATION = (double)realisticDuration * 10.0;

                            if (realisticFinish.HasValue)
                                localTask.REALISTICFINISH = realisticFinish > realisticFinishF ? realisticFinish : realisticFinishF;
                            else
                                localTask.REALISTICFINISH = realisticFinishF;

                            localTask.PERCENTCOMPLETEORIGINAL = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                            localTask.PERCENTCOMPLETEOPTIMIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                            localTask.PERCENTCOMPLETEREALISTIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                            localTask.TOTALSLACK = taskRow.IsTASK_TOTAL_SLACKNull() ? 0 : taskRow.TASK_TOTAL_SLACK;
                            localTask.FREESLACK = taskRow.IsTASK_FREE_SLACKNull() ? 0 : taskRow.TASK_FREE_SLACK;
                            localTask.UPDATEDBY = GetCurrentNTUserId();
                            localTask.UPDATEDDATE = DateTime.Now;

                            db.Entry(localTask).State = System.Data.Entity.EntityState.Modified;
                        }

                        var masterBaselineTask = masterBaselineTasks.Where(x => x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();
                        if (masterBaselineTask != null)
                        {
                            var originalFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOriginalFinishEach + "'");
                            var originalFinish = originalFinishfilteredRows.Length == 0 ? null : new DateTime?(originalFinishfilteredRows[0].DATE_VALUE);

                            masterBaselineTask.REMAININGORIGINAL = taskRow.TASK_REM_DUR;

                            if (originalFinish.HasValue)
                                masterBaselineTask.ORIGINALFINISH = originalFinish;

                            var remainingOptimisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");
                            var remainingOptimistic = remainingOptimisticfilteredRows.Length == 0 ? null : new Decimal?(remainingOptimisticfilteredRows[0].NUM_VALUE);

                            var optimisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOptimisticFinishEach + "'");
                            DateTime? optimisticFinish = optimisticFinishfilteredRows.Length == 0 ? null : (optimisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishfilteredRows[0].DATE_VALUE));

                            var optimisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOptimisticFinishEachF + "'");
                            DateTime? optimisticFinishF = optimisticFinishFfilteredRows.Length == 0 || optimisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishFfilteredRows[0].DATE_VALUE);


                            var optimisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfOptimisticDurationEach + "'");
                            decimal? optimisticDuration = optimisticDurationfilteredRows.Length == 0 ? null : new decimal?(optimisticDurationfilteredRows[0].NUM_VALUE);

                            if (remainingOptimistic.HasValue)
                                masterBaselineTask.REMAININGOPTIMISTIC = (double)remainingOptimistic;

                            if (optimisticFinish.HasValue)
                                masterBaselineTask.OPTIMISTICFINISH = optimisticFinish > optimisticFinishF ? optimisticFinish : optimisticFinishF;
                            else
                                masterBaselineTask.OPTIMISTICFINISH = optimisticFinishF;

                            if (optimisticDuration.HasValue)
                                masterBaselineTask.OPTIMISTICDURATION = (double)optimisticDuration * 10.0;

                            var remainingRealisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");
                            var remainingRealistic = remainingRealisticfilteredRows.Length == 0 ? null : new decimal?(remainingRealisticfilteredRows[0].NUM_VALUE);

                            var realisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticFinishEach + "'");

                            DateTime? realisticFinish = realisticFinishfilteredRows.Length == 0 ? null : (realisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishfilteredRows[0].DATE_VALUE));

                            var realisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticFinishEachF + "'");
                            DateTime? realisticFinishF = realisticFinishFfilteredRows.Length == 0 ? null : (realisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishFfilteredRows[0].DATE_VALUE));


                            var realisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                "MD_PROP_UID = '" + cfRealisticDurationEach + "'");

                            var realisticDuration = realisticDurationfilteredRows.Length == 0 ? null : new decimal?(realisticDurationfilteredRows[0].NUM_VALUE);
                            if (remainingRealistic.HasValue)
                                masterBaselineTask.REMAININGREALISTIC = (double)remainingRealistic;

                            if (realisticDuration.HasValue)
                                masterBaselineTask.REALISTICDURATION = (double)realisticDuration * 10.0;

                            if (realisticFinish.HasValue)
                                masterBaselineTask.REALISTICFINISH = realisticFinish > realisticFinishF ? realisticFinish : realisticFinishF;
                            else
                                masterBaselineTask.REALISTICFINISH = realisticFinishF;

                            masterBaselineTask.PERCENTCOMPLETEORIGINAL = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                            masterBaselineTask.PERCENTCOMPLETEOPTIMIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                            masterBaselineTask.PERCENTCOMPLETEREALISTIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                            masterBaselineTask.TOTALSLACK = taskRow.IsTASK_TOTAL_SLACKNull() ? 0 : taskRow.TASK_TOTAL_SLACK;
                            masterBaselineTask.FREESLACK = taskRow.IsTASK_FREE_SLACKNull() ? 0 : taskRow.TASK_FREE_SLACK;

                            db.Entry(masterBaselineTask).State = System.Data.Entity.EntityState.Modified;
                        }
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                WriteLog(exc.Message + "Stack Trace: " + exc.StackTrace);

                Guid jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Update percent complete");

                throw new ApplicationException("Error occured while updating progress.");
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPut]
        [ActionName("AddShiftTargetTask")]
        public HttpResponseMessage AddShiftTargetTask(int projShiftID, List<GanttObject> ganttObjects)
        {
            var projectShift = db.PROJECTSHIFTs.Find(projShiftID);

            // get task info and its parent from PSI include the calculation
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectShift.PROJECT.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

            var filterExpression = "";
            foreach (var ganttObject in ganttObjects)
            {
                filterExpression += filterExpression == "" ? "" : " OR ";
                filterExpression += "TASK_UID = '" + ganttObject.originalId + "'";
            }

            var taskRows = projectDs.Task.Select(filterExpression) as ProjectDataSet.TaskRow[];
            List<ProjectDataSet.TaskRow> taskRowList = new List<ProjectDataSet.TaskRow>();
            foreach (var row in taskRows)
            {
                // parent2
                var curRow = row;
                while (curRow.TASK_PARENT_UID != curRow.TASK_UID)
                {
                    if (taskRowList.Count(x => x.TASK_UID == curRow.TASK_UID) == 0)
                    {
                        taskRowList.Add(curRow);

                        var taskParentRows = projectDs.Task.Select("TASK_UID = '" + curRow.TASK_PARENT_UID.ToString() + "'") as ProjectDataSet.TaskRow[];
                        if (taskParentRows.Length > 0)
                        {
                            curRow = taskParentRows[0];
                        }
                    }
                    else
                        break;
                }
            }

            foreach (var row in taskRowList)
            {
                var taskRow = row as ProjectDataSet.TaskRow;
                var executor = "";

                var filterExecutorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                           "MD_PROP_UID = '" + cfExecutor1Uid + "'");
                if (filterExecutorRows.Length > 0)
                {
                    executor = filterExecutorRows[0].TEXT_VALUE;
                }

                var projectTaskByShift = db.PROJECTTASKBYSHIFTs
                                    .Where(x => x.ESTIMATIONSHIFTID == projectShift.OID &&
                                            x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();
                if (projectTaskByShift == null)
                {
                    projectTaskByShift = new PROJECTTASKBYSHIFT()
                    {
                        ESTIMATIONSHIFTID = projectShift.OID,
                        PROJECTID = projectShift.PROJECTID,
                        TASKNAME = taskRow.TASK_NAME,
                        PARENTTASKUID = taskRow.IsTASK_PARENT_UIDNull() ? null : taskRow.TASK_PARENT_UID.ToString(),
                        OUTLINELEVEL = taskRow.TASK_OUTLINE_LEVEL,
                        OUTLINEPOSITION = taskRow.TASK_OUTLINE_NUM,
                        ISSUMMARY = taskRow.TASK_IS_SUMMARY,
                        BASELINESTART = taskRow.IsTB_STARTNull() ? null : new DateTime?(taskRow.TB_START),
                        BASELINEFINISH = taskRow.IsTB_FINISHNull() ? null : new DateTime?(taskRow.TB_FINISH),
                        BASELINEDURATION = taskRow.IsTB_DURNull() ? null : new int?(taskRow.TB_DUR),
                        STARTDATE = taskRow.IsTASK_START_DATENull() ? null : new DateTime?(taskRow.TASK_START_DATE),
                        FINISHDATE = taskRow.IsTASK_FINISH_DATENull() ? null : new DateTime?(taskRow.TASK_FINISH_DATE),
                        ESTDURATION = taskRow.IsTASK_DURNull() ? null : new int?(taskRow.TASK_DUR),
                        ACTUALFINISH = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH),
                        ACTUALSTART = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START),
                        ACTUALDURATION = taskRow.IsTASK_ACT_DURNull() ? null : new int?(taskRow.TASK_ACT_DUR),
                        PERCENTCOMPLETEBASELINE = null, // update when print shift target (simulation calculation)
                        PERCENTCOMPLETEACTUALAFTERSHIFT = null,
                        PERCENTCOMPLETEACTUALBEFORESHIFT = taskRow.IsTASK_PCT_COMPNull() ? null : new int?(taskRow.TASK_PCT_COMP),
                        CONTRACTOR = executor,
                        OPTIMISTICFINISH = null,
                        ORIGINALFINISH = null,
                        TASKUID = taskRow.TASK_UID.ToString(),
                        REALISTICFINISH = null,
                        CREATEDBY = GetCurrentNTUserId(),
                        CREATEDDATE = DateTime.Now
                    };

                    db.PROJECTTASKBYSHIFTs.Add(projectTaskByShift);
                }
                else
                {
                    projectTaskByShift.TASKNAME = taskRow.TASK_NAME;
                    projectTaskByShift.PARENTTASKUID = taskRow.IsTASK_PARENT_UIDNull() ? null : taskRow.TASK_PARENT_UID.ToString();
                    projectTaskByShift.OUTLINELEVEL = taskRow.TASK_OUTLINE_LEVEL;
                    projectTaskByShift.OUTLINEPOSITION = taskRow.TASK_OUTLINE_NUM;
                    projectTaskByShift.ISSUMMARY = taskRow.TASK_IS_SUMMARY;
                    projectTaskByShift.BASELINESTART = taskRow.IsTB_STARTNull() ? null : new DateTime?(taskRow.TB_START);
                    projectTaskByShift.BASELINEFINISH = taskRow.IsTB_FINISHNull() ? null : new DateTime?(taskRow.TB_FINISH);
                    projectTaskByShift.BASELINEDURATION = taskRow.IsTB_DURNull() ? null : new int?(taskRow.TB_DUR);
                    projectTaskByShift.STARTDATE = taskRow.IsTASK_START_DATENull() ? null : new DateTime?(taskRow.TASK_START_DATE);
                    projectTaskByShift.FINISHDATE = taskRow.IsTASK_FINISH_DATENull() ? null : new DateTime?(taskRow.TASK_FINISH_DATE);
                    projectTaskByShift.ESTDURATION = taskRow.IsTASK_DURNull() ? null : new int?(taskRow.TASK_DUR);
                    projectTaskByShift.ACTUALFINISH = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH);
                    projectTaskByShift.ACTUALSTART = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START);
                    projectTaskByShift.ACTUALDURATION = taskRow.IsTASK_ACT_DURNull() ? null : new int?(taskRow.TASK_ACT_DUR);
                    projectTaskByShift.CONTRACTOR = executor;

                    db.Entry(projectTaskByShift).State = System.Data.Entity.EntityState.Modified;
                }
            }

            db.SaveChanges();


            // calculate plan
            GenerateShiftTarget(projShiftID);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("updateCompleteNoProgress")]
        public HttpResponseMessage updateCompleteNoProgress(int param)
        {
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(param);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + param);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            CheckProjectState(projectUid);

            Guid sessionUid = Guid.NewGuid();
            ProjectWS.CheckOutProject(projectUid, sessionUid, "Update percent complete (no progress)");

            PSI_ProjectWS.ProjectDataSet projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            foreach (ProjectDataSet.TaskRow taskRow in projectDs.Task)
            {
                var filterEndShiftTimeRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                "MD_PROP_UID = '" + cfEndShiftTime + "'");
                if (filterEndShiftTimeRows.Length > 0)
                {
                    filterEndShiftTimeRows[0].DATE_VALUE = shiftTargetDate.Value;
                }
                else
                {
                    PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newEndShiftTimeCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                    newEndShiftTimeCFRow.PROJ_UID = taskRow.PROJ_UID;
                    newEndShiftTimeCFRow.TASK_UID = taskRow.TASK_UID;
                    newEndShiftTimeCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                    newEndShiftTimeCFRow.MD_PROP_UID = new Guid(cfEndShiftTime);

                    newEndShiftTimeCFRow.DATE_VALUE = shiftTargetDate.Value;

                    projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newEndShiftTimeCFRow);
                }
            }

            Guid jobUid = Guid.NewGuid();
            var changedDs = projectDs.GetChanges() as ProjectDataSet;

            ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs, false);
            WaitForQueue(q, jobUid);

            projectDs = ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var localTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == param).ToList();

            var cfRealisticStartDate = new Guid(cfCalcRealisticDurationStartUid);
            var cfRealisticEndDate = new Guid(cfCalcRealisticDurationEndDateUid);
            var cfOptimisticStartDate = new Guid(cfCalcOptimisticDurationStartDate);
            var cfOptimisticEndDate = new Guid(cfCalcOptimisticDurationEndDate);
            var cfRealisticDuration = new Guid(cfCalcRealisticDuration);
            var cfOptimisticDuration = new Guid(cfCalcOptimisticDuration);

            var cfRemainingOptimistic = new Guid(cfCalcRemainingOptimistic);
            foreach (ProjectDataSet.TaskRow taskRow in projectDs.Task)
            {
                var filterDurationForRealisticCalc = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                        "MD_PROP_UID = '" + cfDurationForRealisticCalc + "'");

                // OPTIMISTIC
                var remainingOptimistic = (double)taskRow.TB_DUR;
                if (!taskRow.IsTASK_PCT_COMPNull() && taskRow.TASK_PCT_COMP != 0)
                {
                    if (taskRow.TASK_PCT_COMP == 100)
                        remainingOptimistic = 0.0;
                    else if (filterDurationForRealisticCalc[0].NUM_VALUE >= 0)
                        remainingOptimistic = ((100.0 - (double)taskRow.TASK_PCT_COMP) / 100.0) * (double)taskRow.TB_DUR;
                }

                var filterRemainingOptimisticEachRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                            "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");

                if (filterRemainingOptimisticEachRows.Length > 0)
                {
                    filterRemainingOptimisticEachRows[0].NUM_VALUE = (decimal)remainingOptimistic;
                }
                else
                {
                    PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow remainingOptimisticEachCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                    remainingOptimisticEachCFRow.PROJ_UID = taskRow.PROJ_UID;
                    remainingOptimisticEachCFRow.TASK_UID = taskRow.TASK_UID;
                    remainingOptimisticEachCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                    remainingOptimisticEachCFRow.MD_PROP_UID = new Guid(cfRemainingOptimisticEach);

                    remainingOptimisticEachCFRow.NUM_VALUE = (decimal)remainingOptimistic;

                    projectDs.TaskCustomFields.AddTaskCustomFieldsRow(remainingOptimisticEachCFRow);

                }

                var remainingRealistic = (double)taskRow.TB_DUR;
                if (filterDurationForRealisticCalc[0].NUM_VALUE >= 0)
                {
                    if (!taskRow.IsTASK_PCT_COMPNull() && taskRow.TASK_PCT_COMP != 0)
                        remainingRealistic = (100.0 / (double)taskRow.TASK_PCT_COMP) * ((double)filterDurationForRealisticCalc[0].NUM_VALUE) * 10 * ((100.0 - (double)taskRow.TASK_PCT_COMP) / 100);
                    else
                        remainingRealistic = remainingOptimistic;
                }

                var filterRemainingRealisticEachRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                            "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");

                if (filterRemainingRealisticEachRows.Length > 0)
                {
                    filterRemainingRealisticEachRows[0].NUM_VALUE = (decimal)remainingRealistic;
                }
                else
                {
                    PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow remainingRealisticEachCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                    remainingRealisticEachCFRow.PROJ_UID = taskRow.PROJ_UID;
                    remainingRealisticEachCFRow.TASK_UID = taskRow.TASK_UID;
                    remainingRealisticEachCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                    remainingRealisticEachCFRow.MD_PROP_UID = new Guid(cfRemainingRealisticEach);

                    remainingRealisticEachCFRow.NUM_VALUE = (decimal)remainingRealistic;

                    projectDs.TaskCustomFields.AddTaskCustomFieldsRow(remainingRealisticEachCFRow);

                }
            }


            jobUid = Guid.NewGuid();
            var changedDs2 = projectDs.GetChanges() as ProjectDataSet;
            ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changedDs2, false);
            WaitForQueue(q, jobUid);

            jobUid = Guid.NewGuid();
            ProjectWS.QueuePublish(jobUid, projectUid, true, null);
            WaitForQueue(q, jobUid);

            jobUid = Guid.NewGuid();
            ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Update percent complete");
            WaitForQueue(q, jobUid);

            // update local task based on server result
            projectDs = ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);
            var masterBaselineTasks = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == param).ToList();
            if (projectDs.Task.Rows.Count > 0)
            {
                foreach (ProjectDataSet.TaskRow taskRow in projectDs.Task)
                {
                    var localTask = localTasks.Where(x => x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();

                    if (localTask != null)
                    {
                        localTask.BASELINESTART = taskRow.IsTB_STARTNull() ? null : new DateTime?(taskRow.TB_START);
                        localTask.BASELINEFINISH = taskRow.IsTB_FINISHNull() ? null : new DateTime?(taskRow.TB_FINISH);
                        localTask.BASELINEDURATION = taskRow.IsTB_DURNull() ? null : new int?(taskRow.TB_DUR);

                        localTask.STARTDATE = taskRow.IsTASK_START_DATENull() ? null : new DateTime?(taskRow.TASK_START_DATE);
                        localTask.FINISHDATE = taskRow.IsTASK_FINISH_DATENull() ? null : new DateTime?(taskRow.TASK_FINISH_DATE);
                        localTask.ESTDURATION = taskRow.IsTASK_DURNull() ? null : new int?(taskRow.TASK_DUR);

                        localTask.ACTUALSTART = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START);
                        localTask.ACTUALFINISH = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH);
                        localTask.ACTUALDURATION = taskRow.IsTASK_ACT_DURNull() ? null : new int?(taskRow.TASK_ACT_DUR);

                        localTask.PERCENTCOMPLETEACTUALAFTERSHIFT = taskRow.TASK_PCT_COMP;

                        var originalFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                        "MD_PROP_UID = '" + cfOriginalFinishEach + "'");
                        var originalFinish = originalFinishfilteredRows[0].DATE_VALUE;

                        localTask.REMAININGORIGINAL = taskRow.TASK_REM_DUR;
                        localTask.ORIGINALFINISH = originalFinish;

                        var remainingOptimisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");
                        var remainingOptimistic = remainingOptimisticfilteredRows[0].NUM_VALUE;

                        var optimisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOptimisticFinishEach + "'");
                        DateTime? optimisticFinish = optimisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishfilteredRows[0].DATE_VALUE);

                        var optimisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOptimisticFinishEachF + "'");
                        DateTime? optimisticFinishF = optimisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishFfilteredRows[0].DATE_VALUE);

                        var optimisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOptimisticDurationEach + "'");
                        var optimisticDuration = optimisticDurationfilteredRows[0].NUM_VALUE;


                        localTask.REMAININGOPTIMISTIC = (double)remainingOptimistic;

                        localTask.OPTIMISTICFINISH = optimisticFinish > optimisticFinishF ? optimisticFinish : optimisticFinishF;
                        localTask.OPTIMISTICDURATION = (double)optimisticDuration * 10.0;

                        // REALISTIC
                        var remainingRealisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");
                        var remainingRealistic = remainingRealisticfilteredRows[0].NUM_VALUE;

                        var realisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRealisticFinishEach + "'");
                        DateTime? realisticFinish = realisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishfilteredRows[0].DATE_VALUE);

                        var realisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRealisticFinishEachF + "'");
                        DateTime? realisticFinishF = realisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishFfilteredRows[0].DATE_VALUE);

                        var realisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRealisticDurationEach + "'");
                        var realisticDuration = realisticDurationfilteredRows[0].NUM_VALUE;

                        localTask.REMAININGREALISTIC = (double)remainingRealistic;
                        localTask.REALISTICDURATION = (double)realisticDuration * 10.0;
                        localTask.REALISTICFINISH = realisticFinish > realisticFinishF ? realisticFinish : realisticFinishF;

                        localTask.PERCENTCOMPLETEORIGINAL = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                        localTask.PERCENTCOMPLETEOPTIMIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                        localTask.PERCENTCOMPLETEREALISTIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                        localTask.TOTALSLACK = taskRow.IsTASK_TOTAL_SLACKNull() ? 0 : taskRow.TASK_TOTAL_SLACK;
                        localTask.FREESLACK = taskRow.IsTASK_FREE_SLACKNull() ? 0 : taskRow.TASK_FREE_SLACK;
                        localTask.UPDATEDBY = GetCurrentNTUserId();
                        localTask.UPDATEDDATE = DateTime.Now;

                        db.Entry(localTask).State = System.Data.Entity.EntityState.Modified;
                    }

                    var masterBaselineTask = masterBaselineTasks.Where(x => x.TASKUID == taskRow.TASK_UID.ToString()).FirstOrDefault();
                    if (masterBaselineTask != null)
                    {
                        var originalFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOriginalFinishEach + "'");
                        var originalFinish = originalFinishfilteredRows[0].DATE_VALUE;

                        masterBaselineTask.REMAININGORIGINAL = taskRow.TASK_REM_DUR;
                        masterBaselineTask.ORIGINALFINISH = originalFinish;

                        var remainingOptimisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRemainingOptimisticEach + "'");
                        var remainingOptimistic = remainingOptimisticfilteredRows[0].NUM_VALUE;

                        var optimisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOptimisticFinishEach + "'");
                        DateTime? optimisticFinish = optimisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishfilteredRows[0].DATE_VALUE);

                        var optimisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOptimisticFinishEachF + "'");
                        DateTime? optimisticFinishF = optimisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(optimisticFinishFfilteredRows[0].DATE_VALUE);


                        var optimisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfOptimisticDurationEach + "'");
                        var optimisticDuration = optimisticDurationfilteredRows[0].NUM_VALUE;

                        masterBaselineTask.REMAININGOPTIMISTIC = (double)remainingOptimistic;
                        masterBaselineTask.OPTIMISTICFINISH = optimisticFinish > optimisticFinishF ? optimisticFinish : optimisticFinishF;
                        masterBaselineTask.OPTIMISTICDURATION = (double)optimisticDuration * 10.0;

                        var remainingRealisticfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRemainingRealisticEach + "'");
                        var remainingRealistic = remainingRealisticfilteredRows[0].NUM_VALUE;

                        var realisticFinishfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRealisticFinishEach + "'");
                        DateTime? realisticFinish = realisticFinishfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishfilteredRows[0].DATE_VALUE);

                        var realisticFinishFfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRealisticFinishEachF + "'");
                        DateTime? realisticFinishF = realisticFinishFfilteredRows[0].IsDATE_VALUENull() ? null : new DateTime?(realisticFinishFfilteredRows[0].DATE_VALUE);


                        var realisticDurationfilteredRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + cfRealisticDurationEach + "'");
                        var realisticDuration = realisticDurationfilteredRows[0].NUM_VALUE;

                        masterBaselineTask.REMAININGREALISTIC = (double)remainingRealistic;
                        masterBaselineTask.REALISTICDURATION = (double)realisticDuration * 10.0;
                        masterBaselineTask.REALISTICFINISH = realisticFinish > realisticFinishF ? realisticFinish : realisticFinishF;

                        masterBaselineTask.PERCENTCOMPLETEORIGINAL = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                        masterBaselineTask.PERCENTCOMPLETEOPTIMIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                        masterBaselineTask.PERCENTCOMPLETEREALISTIS = !taskRow.IsTASK_PCT_COMPNull() ? new double?(taskRow.TASK_PCT_COMP) : null;
                        masterBaselineTask.TOTALSLACK = taskRow.IsTASK_TOTAL_SLACKNull() ? 0 : taskRow.TASK_TOTAL_SLACK;
                        masterBaselineTask.FREESLACK = taskRow.IsTASK_FREE_SLACKNull() ? 0 : taskRow.TASK_FREE_SLACK;

                        db.Entry(masterBaselineTask).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                db.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [ActionName("TestStoreTaskInformationByShiftQuery")]
        public HttpResponseMessage TestStoreTaskInformationByShiftQuery(int shiftProjID)
        {
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(shiftProjID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + shiftProjID);

            var projectData = projectShift.PROJECT;
            var prjName = projectData.PROJECTDESCRIPTION;

            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_ProjectWS.ProjectDataSet projectDs =
                        ProjectWS.ReadProject(new Guid(projectData.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var taskRows = projectDs.Task.Select("TASK_START_DATE < '" + projectShift.ENDDATE.Value.ToString("yyyy-MM-dd  HH:mm:ss") + "'" +
                " AND TASK_PCT_COMP <> 100 AND TASK_OUTLINE_LEVEL > 0") as ProjectDataSet.TaskRow[];

            var task = taskRows.ToList().Where(x => x.TASK_NAME == "D3 FEED END n DISCHARGE END WEBCO SEAL REPLACE").FirstOrDefault();

            return Request.CreateResponse(HttpStatusCode.OK, taskRows);
        }
    }
    
}
