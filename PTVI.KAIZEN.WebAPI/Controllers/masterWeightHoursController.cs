﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterWeightHoursController : KAIZENController
    { 
        VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/<controller>
        //public HttpResponseMessage Get(string weightHours)
        //{
        //    try
        //    {
        //        var parent = db.MASTERWEIGHTHOURS.Where(t => t.DESCRIPTION == weightHours.ToUpper()).FirstOrDefault();

        //        var childs = db.MASTERWEIGHTHOURS.Where(t => t.PARENTID == parent.OID || t.OID == parent.OID);
        //        var result = childs.Select(t => new { id = t.OID, description = t.DESCRIPTION, estimateHours = t.ESTIMATEHOURS, weightHours = t.WEIGHTHOURSTYPE, isEditable = t.ISEDITABLE }).AsEnumerable();
        //        return Request.CreateResponse(HttpStatusCode.OK, result);
        //    }
        //    catch(Exception ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);

        //    }
        //}

        // GET api/<controller>/5
        [HttpGet]
        [ActionName("getWeightHours")]
        public IEnumerable<masterWeightHoursObject> getMasterWeightHours(string param)
        {
            return db.MASTERWEIGHTHOURS.Select(s => new masterWeightHoursObject()
            {
                id = s.OID,
                seq = s.SEQ,
                pId = s.PARENTID,
                isEditable = s.ISEDITABLE,
                isParentGroup = s.ISPARENTGROUP,
                isActive = s.ISACTIVE,
                description = s.DESCRIPTION,
                estimateHours = s.ESTIMATEHOURS,
                weightHoursTypeText = db.LOOKUPs.Where(x => x.TYPE == "WeightHoursType" && x.VALUE == s.WEIGHTHOURSTYPE).FirstOrDefault().TEXT,
                weightHoursType = s.WEIGHTHOURSTYPE
            }).AsEnumerable();
        }

        // POST api/<controller>
        [HttpPost]
        [ActionName("addWeightHours")]
        public IHttpActionResult addWeightHours(string param, masterWeightHoursObject masterWeightParam)
        {
            MASTERWEIGHTHOUR add = new MASTERWEIGHTHOUR();
            add.DESCRIPTION = masterWeightParam.description;
            add.ESTIMATEHOURS = masterWeightParam.estimateHours;
            add.WEIGHTHOURSTYPE = masterWeightParam.weightHoursType;
            add.ISEDITABLE = masterWeightParam.isEditable;
            add.PARENTID = masterWeightParam.pId;
            add.SEQ = masterWeightParam.seq;
            add.ISPARENTGROUP = masterWeightParam.isParentGroup;
            add.ISACTIVE = true;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERWEIGHTHOURS.Add(add);
            db.SaveChanges();

            masterWeightParam.id = add.OID;
            return Ok(masterWeightParam);
        } 

        // PUT api/<controller>/5
        [HttpPut]
        [ActionName("updateWeightHours")]
        public IHttpActionResult updateWeightHours(int param, masterWeightHoursObject masterWeightParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != masterWeightParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERWEIGHTHOUR update = db.MASTERWEIGHTHOURS.Find(param);
                update.DESCRIPTION = masterWeightParam.description;
                update.ESTIMATEHOURS = masterWeightParam.estimateHours;
                update.WEIGHTHOURSTYPE = masterWeightParam.weightHoursType;
                update.ISEDITABLE = masterWeightParam.isEditable;
                update.PARENTID = masterWeightParam.pId;
                update.SEQ = masterWeightParam.seq;
                update.ISPARENTGROUP = masterWeightParam.isParentGroup;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(masterWeightParam);
        }

        [HttpGet]
        [ActionName("getByWeightHoursType")]
        public IEnumerable<masterWeightHoursObject> getByWeightHoursType(string param)
        {
            return db.MASTERWEIGHTHOURS.Where(w => w.WEIGHTHOURSTYPE == param).Select(s => new masterWeightHoursObject()
            {
                id = s.OID,
                seq = s.SEQ,
                pId = s.PARENTID,
                isEditable = s.ISEDITABLE,
                isParentGroup = s.ISPARENTGROUP,
                isActive = s.ISACTIVE,
                description = s.DESCRIPTION,
                estimateHours = s.ESTIMATEHOURS,
                weightHoursTypeText = db.LOOKUPs.Where(x => x.TYPE == "WeightHoursType" && x.VALUE == s.WEIGHTHOURSTYPE).FirstOrDefault().TEXT,
                weightHoursType = s.WEIGHTHOURSTYPE
            }).AsEnumerable();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}