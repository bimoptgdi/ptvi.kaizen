﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using Newtonsoft.Json;
using PTGDI.Email;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accController = new AccountController();

        // GET api/Project
        public IEnumerable<PROJECT> GetPROJECTs()
        {
            GetCurrentNTUserId();
            var projects = db.PROJECTs.Include(p => p.AREAINVOLVED);
            return projects.AsEnumerable();
        }

        // GET api/Project/5
        public projectObject GetPROJECT(int id)
        {
            PROJECT project = db.PROJECTs.Find(id);
            List<int> listInt = new List<int>();

            if (project == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            projectObject resultProject = new projectObject
            {
                id = project.OID,
                projectNo = project.PROJECTNO,
                projectDescription = project.PROJECTDESCRIPTION,
                ownerId = project.OWNERID,
                ownerName = project.OWNERNAME,
                ownerEmail = project.OWNEREMAIL,
                sponsorId = project.SPONSORID,
                sponsorName = project.SPONSORNAME,
                sponsorEmail = project.SPONSOREMAIL,
                projectManagerId = project.PROJECTMANAGERID,
                projectManagerName = project.PROJECTMANAGERNAME,
                projectManagerEmail = project.PROJECTMANAGEREMAIL,
                projectEngineerId = project.PROJECTENGINEERID,
                projectEngineerName = project.PROJECTENGINEERNAME,
                projectEngineerEmail = project.PROJECTENGINEEREMAIL,
                maintenanceRepId = project.MAINTENANCEREPID,
                maintenanceRepName = project.MAINTENANCEREPNAME,
                maintenanceRepEmail = project.MAINTENANCEREPEMAIL,
                operationRepId = project.OPERATIONREPID,
                operationRepName = project.OPERATIONREPNAME,
                operationRepEmail = project.OPERATIONREPEMAIL,
                areaInvolvedId = project.AREAINVOLVEDID,
                projectCategory = project.PROJECTCATEGORY,
                projectType = project.PROJECTTYPE,
                pmoNo = project.PMONO,
                planStartDate = project.PLANSTARTDATE,
                planFinishDate = project.PLANFINISHDATE,
                actualStartDate = project.ACTUALSTARTDATE,
                actualFinishDate = project.ACTUALFINISHDATE,
                totalCost = project.TOTALCOST,
                iptProjectId = project.IPTPROJECTID,
                shareFolderDoc = project.SHAREFOLDERDOC,
                createdDate = project.CREATEDDATE,
                momewrid = project.MOMEWRID
            };

            return resultProject;
        }

        // PUT api/Project/5
        public HttpResponseMessage PutPROJECT(int id, PROJECT project)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != project.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(project).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Project
        public HttpResponseMessage PostPROJECT(PROJECT project)
        {
            if (ModelState.IsValid)
            {
                db.PROJECTs.Add(project);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, project);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = project.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Project/5
        public HttpResponseMessage DeletePROJECT(int id)
        {
            PROJECT project = db.PROJECTs.Find(id);
            if (project == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.PROJECTs.Remove(project);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, project);
        }

        [HttpGet]
        [ActionName("getRoleInProject")]
        public List<ROLE> getRoleInProject(int param, string badgeNo)
        {
            PROJECT project = db.PROJECTs.Find(param);

            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);

            List<ROLE> result = new List<ROLE>();
            if (project != null)
            {
                if (project.PROJECTMANAGERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectManager));
                if (project.OWNERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectOwner));
                if (project.SPONSORID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectSponsor));
                if (project.PROJECTENGINEERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectEngineer));
                if (project.MAINTENANCEREPID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaintenanceReport));
                if (project.OPERATIONREPID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleOperationReport));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleEngineer));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleDesigner));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectController));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaterialCoordinator));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_OFFICER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaterialCoordinator));
                //if (project.USERASSIGNMENTs.Where(ua => otherPos.Select(s => s.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == badgeNo).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectManager));
                if (project.USERASSIGNMENTs.Where(ua => otherPos.Select(s => s.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == badgeNo).Count() > 0)
                {
                    var listAssignment = project.USERASSIGNMENTs.Where(ua => otherPos.Select(s => s.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == badgeNo).Select(SUA => SUA.ASSIGNMENTTYPE);
                    result.Add(db.ROLEs.FirstOrDefault(f => listAssignment.Contains(f.ROLENAME)));
                }
            }
            return result;
        }

        [HttpGet]
        [ActionName("getPositionInProject")]
        public List<ROLE> getPositionInProject(int param, string badgeNo)
        {
            PROJECT project = db.PROJECTs.Find(param);

            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject).ToList();

            List<ROLE> result = new List<ROLE>();
            if (project != null)
            {
                if (project.OWNERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectOwner));
                if (project.PROJECTMANAGERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectManager));
                if (project.SPONSORID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectSponsor));
                if (project.PROJECTENGINEERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectEngineer));
                if (project.MAINTENANCEREPID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaintenanceReport));
                if (project.OPERATIONREPID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleOperationReport));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleEngineer));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleDesigner));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectController));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaterialCoordinator));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaterialCoordinator));
            }
            return result;
        }

        [HttpGet]
        [ActionName("IsCloseOutProjectTo")]
        public bool IsCloseOutProjectTo(int param)
        {
            if (db.PROJECTCLOSEOUTs.Count(c => c.PROJECTID == param && c.ISAPPROVED == true) != 0)
            {
                return true;
            }

            return false;
        }


        [HttpGet]
        [ActionName("IsAllowedProjectTo")]
        public bool IsAllowedProjectTo(int param, string badgeNo, int accessrights)
        {
            List<ROLE> result = getRoleInProject(param, badgeNo);

            var count = result.Count(c => c.ROLEACCESSRIGHTs.Where(w => w.ACCESSRIGHTID == (int)accessrights).Count() > 0) + db.ROLEs.Count(c => c.USERROLEs.Where(w => w.USER.BADGENO == badgeNo).Count() > 0 && c.ROLEACCESSRIGHTs.Where(w => w.ACCESSRIGHTID == (int)accessrights).Count() > 0);

            if (count != 0)
            {
                return true;
                //foreach (var role in result)
                //{
                //    var accessrightList = role.ROLEACCESSRIGHTs;

                //    foreach (var ar in accessrightList)
                //    {
                //        if (ar.ACCESSRIGHT.OID == (int)accessrights)
                //            return true;
                //    }
                //}
            }

            return false;
        }

        [HttpGet]
        [ActionName("IsAllowedProjectToOnList")]
        public object IsAllowedProjectToOnList(int param, string badgeNo, int accessrights)
        {
            var isAllowed = IsAllowedProjectTo(param, badgeNo, accessrights);
            List<object> result = new List<object>();


            result.Add(new { result = isAllowed });

            return result;
        }

        [HttpGet]
        [ActionName("IsReadonlyProjectTo")]
        public object IsReadonlyProjectTo(int param, string badgeNo, int accessrights)
        {
            var isCloseOut = IsCloseOutProjectTo(param);
            if (isCloseOut == true)
            {
                return true;
            } 
            else
            {
                List<ROLE> role = getRoleInProject(param, badgeNo);

                var count = role.Count(c => c.ROLEACCESSRIGHTs.Where(w => w.ACCESSRIGHTID == (int)accessrights && w.ISREADONLY != true).Count() > 0) + db.ROLEs.Count(c => c.USERROLEs.Where(w => w.USER.BADGENO == badgeNo).Count() > 0 && c.ROLEACCESSRIGHTs.Where(w => w.ACCESSRIGHTID == (int)accessrights && w.ISREADONLY != true).Count() > 0);

                if (count == 0)
                {
                    return true;
                }
            }
            return false;
        }


        [HttpGet]
        [ActionName("IsReadonlyProjectToOnList")]
        public object IsReadonlyProjectToOnList(int param, string badgeNo, int accessrights)
        {
            var isReadOnly = IsReadonlyProjectTo(param, badgeNo, accessrights);
            List<object> result = new List<object>();


            result.Add(new { result = isReadOnly });

            return result;
        }

        [HttpGet]
        [ActionName("getProjectById")]
        public projectObject getProjectById(int param)
        {
            projectObject result = new projectObject();
            PROJECT projectRequest = db.PROJECTs.Find(param);

            result.id = projectRequest.OID;
            result.projectNo = projectRequest.PROJECTNO;
            result.projectDescription = projectRequest.PROJECTDESCRIPTION;
            result.ownerId = projectRequest.OWNERID;
            result.ownerName = projectRequest.OWNERNAME;
            result.ownerEmail = projectRequest.OWNEREMAIL;
            result.sponsorId = projectRequest.SPONSORID;
            result.sponsorName = projectRequest.SPONSORNAME;
            result.sponsorEmail = projectRequest.SPONSOREMAIL;
            result.projectManagerId = projectRequest.PROJECTMANAGERID;
            result.projectManagerName = projectRequest.PROJECTMANAGERNAME;
            result.projectManagerEmail = projectRequest.PROJECTMANAGEREMAIL;
            result.projectEngineerId = projectRequest.PROJECTENGINEERID;
            result.projectEngineerName = projectRequest.PROJECTENGINEERNAME;
            result.projectEngineerEmail = projectRequest.PROJECTENGINEEREMAIL;
            result.maintenanceRepId = projectRequest.MAINTENANCEREPID;
            result.maintenanceRepName = projectRequest.MAINTENANCEREPNAME;
            result.maintenanceRepEmail = projectRequest.MAINTENANCEREPEMAIL;
            result.operationRepId = projectRequest.OPERATIONREPID;
            result.operationRepName = projectRequest.OPERATIONREPNAME;
            result.operationRepEmail = projectRequest.OPERATIONREPEMAIL;
            result.areaInvolvedId = projectRequest.AREAINVOLVEDID;
            result.projectType = projectRequest.PROJECTTYPE;
            result.pmoNo = projectRequest.PMONO;
            result.planStartDate = projectRequest.PLANSTARTDATE;
            result.planFinishDate = projectRequest.PLANFINISHDATE;
            result.actualStartDate = projectRequest.ACTUALSTARTDATE;
            result.actualFinishDate = projectRequest.ACTUALFINISHDATE;
            result.totalCost = projectRequest.TOTALCOST;
            result.shareFolderDoc = projectRequest.SHAREFOLDERDOC;
            result.iptProjectId = projectRequest.IPTPROJECTID;

            //get other position
            var assignmentType = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject).Select(s => s.VALUE);
            var otherPosotions = db.USERASSIGNMENTs.Where(w => w.PROJECTID == param && assignmentType.Contains(w.ASSIGNMENTTYPE)).Select(s => new userAssignmentObject
            {
                id = s.OID,
                assignmentType = s.ASSIGNMENTTYPE,
                employeeId = s.EMPLOYEEID,
                employeeName = s.EMPLOYEENAME,
                employeeEmail = s.EMPLOYEEEMAIL,
                employeePosition = s.EMPLOYEEPOSITION
            });
            result.otherPositions = otherPosotions;


            return result;
        }

        [HttpPost]
        [ActionName("SubmitRequest")]
        public IHttpActionResult SubmitRequest(string param, projectObject projectObjectParam)
        {
            EmailSender emailSender = new EmailSender();
            if (db.PROJECTs.Where(w => w.PROJECTNO == projectObjectParam.projectNo).Count() != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "duplicateNo"));
            }
            if (db.PROJECTs.Where(w => w.PROJECTDESCRIPTION == projectObjectParam.projectDescription).Count() != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "duplicateTitle"));
            }
            try
            {
                PROJECT projectRequest = new PROJECT();

                projectRequest.PROJECTNO = projectObjectParam.projectNo;
                projectRequest.PROJECTDESCRIPTION = projectObjectParam.projectDescription;
                projectRequest.OWNERID = projectObjectParam.ownerId;
                projectRequest.OWNERNAME = projectObjectParam.ownerName;
                projectRequest.OWNEREMAIL = projectObjectParam.ownerEmail;
                projectRequest.SPONSORID = projectObjectParam.sponsorId;
                projectRequest.SPONSORNAME = projectObjectParam.sponsorName;
                projectRequest.SPONSOREMAIL = projectObjectParam.sponsorEmail;
                projectRequest.PROJECTMANAGERID = projectObjectParam.projectManagerId;
                projectRequest.PROJECTMANAGERNAME = projectObjectParam.projectManagerName;
                projectRequest.PROJECTMANAGEREMAIL = projectObjectParam.projectManagerEmail;
                projectRequest.PROJECTENGINEERID = projectObjectParam.projectEngineerId;
                projectRequest.PROJECTENGINEERNAME = projectObjectParam.projectEngineerName;
                projectRequest.PROJECTENGINEEREMAIL = projectObjectParam.projectEngineerEmail;
                projectRequest.MAINTENANCEREPID = projectObjectParam.maintenanceRepId;
                projectRequest.MAINTENANCEREPNAME = projectObjectParam.maintenanceRepName;
                projectRequest.MAINTENANCEREPEMAIL = projectObjectParam.maintenanceRepEmail;
                projectRequest.OPERATIONREPID = projectObjectParam.operationRepId;
                projectRequest.OPERATIONREPNAME = projectObjectParam.operationRepName;
                projectRequest.OPERATIONREPEMAIL = projectObjectParam.operationRepEmail;
                projectRequest.AREAINVOLVEDID = projectObjectParam.areaInvolvedId;
                projectRequest.PROJECTCATEGORY = projectObjectParam.projectCategory;
                projectRequest.PROJECTTYPE = projectObjectParam.projectType;
                projectRequest.MOMEWRID = projectObjectParam.momewrid;

                MASTERPROJECTGROUPCATEGORY masterProjectG = db.MASTERPROJECTGROUPCATEGORies.Where(w => w.CODE == projectRequest.PROJECTTYPE).FirstOrDefault();
                List<KEYDELIVERABLE> listK = new List<KEYDELIVERABLE>();
                if (masterProjectG != null)
                {
                    var listMasterKey = db.MASTERKEYDELIVERABLES.Where(w => w.PROJECTGROUPCATEGORYID == masterProjectG.OID).OrderBy(o => o.SEQ);

                    foreach (MASTERKEYDELIVERABLE masterKeyD in listMasterKey)
                    {
                        KEYDELIVERABLE keyD = new KEYDELIVERABLE();
                        keyD.NAME = masterKeyD.KEYDELIVERABLES;
                        keyD.SEQ = masterKeyD.SEQ;
                        keyD.CREATEDDATE = DateTime.Now;
                        keyD.CREATEDBY = GetCurrentNTUserId();
                        listK.Add(keyD);
                    }
                }
                List<PROJECTMANAGEMENT> listPManagement = new List<PROJECTMANAGEMENT>();
                PROJECTMANAGEMENT projectManagement = new PROJECTMANAGEMENT();
                projectManagement.KEYDELIVERABLES = listK;
                listPManagement.Add(projectManagement);

                projectRequest.PROJECTMANAGEMENTs = listPManagement;
                projectRequest.PMONO = projectObjectParam.pmoNo;
                projectRequest.PLANSTARTDATE = projectObjectParam.planStartDate;
                projectRequest.PLANFINISHDATE = projectObjectParam.planFinishDate;
                projectRequest.ACTUALSTARTDATE = projectObjectParam.actualStartDate;
                projectRequest.ACTUALFINISHDATE = projectObjectParam.actualFinishDate;
                projectRequest.TOTALCOST = projectObjectParam.totalCost;
                projectRequest.SHAREFOLDERDOC = projectObjectParam.shareFolderDoc;
                projectRequest.CREATEDBY = GetCurrentNTUserId();
                projectRequest.IPTPROJECTID = projectObjectParam.iptProjectId;
                projectRequest.CREATEDDATE = DateTime.Now;

                db.PROJECTs.Add(projectRequest);
                db.SaveChanges();

                //send email
                var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.ProjectInformation).FirstOrDefault();

                //Pengecekan Apakah Ada Yang Termasuk Dalam Project Category Yang Hanya Di Email Untuk Operation Rep
                bool isProjectCategoryOperationRepsOnly = false;
                if (string.IsNullOrEmpty(projectObjectParam.projectCategory))
                {
                    var listProjectCategory = projectObjectParam.projectCategory.Split('|').Select(int.Parse);
                    var getRemindOperationRep = db.MASTERPROJECTCATEGORies.Count(w => listProjectCategory.Contains(w.OID) && w.ISEMAILNOTIF.HasValue);
                    if (getRemindOperationRep > 0)
                    {
                        isProjectCategoryOperationRepsOnly = true;
                    }
                }

                var mailList = new List<usersToEmail>();

                //Create List Email Sesuai Dengan Pengecekan Apakah Ada Yang Termasuk Dalam Project Category Yang Hanya Di Email Untuk Operation Rep Atau Tidak
                if (isProjectCategoryOperationRepsOnly)
                {
                    mailList = new List<usersToEmail>() {
                        new usersToEmail(){userName = projectRequest.OPERATIONREPNAME, email = projectRequest.OPERATIONREPEMAIL, position = constants.posOperationRep  }
                    };


                    //insert other position
                    if (projectObjectParam.otherPositions != null)
                    {
                        foreach (var otherPosition in projectObjectParam.otherPositions.Where(w => w.assignmentType == constants.POR))
                        {
                            USERASSIGNMENT ua = new USERASSIGNMENT();
                            ua.PROJECTID = projectRequest.OID;
                            ua.ASSIGNMENTTYPE = otherPosition.assignmentType;
                            ua.EMPLOYEEID = otherPosition.employeeId;
                            ua.EMPLOYEENAME = otherPosition.employeeName;
                            ua.EMPLOYEEEMAIL = otherPosition.employeeEmail;
                            ua.EMPLOYEEPOSITION = otherPosition.employeePosition;
                            ua.CREATEDDATE = DateTime.Now;
                            ua.CREATEDBY = GetCurrentNTUserId();
                            db.USERASSIGNMENTs.Add(ua);

                            //send email
                            var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == ua.ASSIGNMENTTYPE)).FirstOrDefault();
                            if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == ua.ASSIGNMENTTYPE)).FirstOrDefault();
                            mailList.Add(new usersToEmail() { userName = ua.EMPLOYEENAME, email = ua.EMPLOYEEEMAIL, position = posName != null ? posName.TEXT : "" });
                        }
                    }
                } else
                {
                    mailList = new List<usersToEmail>() {
                        new usersToEmail(){ userName = projectRequest.OWNERNAME, email =  projectRequest.OWNEREMAIL, position = constants.posOwner },
                        new usersToEmail(){userName = projectRequest.SPONSORNAME, email = projectRequest.SPONSOREMAIL, position = constants.posSponsor  },
                        new usersToEmail(){userName = projectRequest.PROJECTMANAGERNAME, email = projectRequest.PROJECTMANAGEREMAIL, position = constants.posProjectManager  } ,
                        new usersToEmail(){userName = projectRequest.PROJECTENGINEERNAME, email = projectRequest.PROJECTENGINEEREMAIL, position = constants.posProjectEngineer  },
                        new usersToEmail(){userName = projectRequest.MAINTENANCEREPNAME, email = projectRequest.MAINTENANCEREPEMAIL, position = constants.posMaintenanceRep  },
                        new usersToEmail(){userName = projectRequest.OPERATIONREPNAME, email = projectRequest.OPERATIONREPEMAIL, position = constants.posOperationRep  }
                    };


                    //insert other position
                    if (projectObjectParam.otherPositions != null)
                    {
                        foreach (var otherPosition in projectObjectParam.otherPositions)
                        {
                            USERASSIGNMENT ua = new USERASSIGNMENT();
                            ua.PROJECTID = projectRequest.OID;
                            ua.ASSIGNMENTTYPE = otherPosition.assignmentType;
                            ua.EMPLOYEEID = otherPosition.employeeId;
                            ua.EMPLOYEENAME = otherPosition.employeeName;
                            ua.EMPLOYEEEMAIL = otherPosition.employeeEmail;
                            ua.EMPLOYEEPOSITION = otherPosition.employeePosition;
                            ua.CREATEDDATE = DateTime.Now;
                            ua.CREATEDBY = GetCurrentNTUserId();
                            db.USERASSIGNMENTs.Add(ua);

                            //send email
                            var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == ua.ASSIGNMENTTYPE)).FirstOrDefault();
                            if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == ua.ASSIGNMENTTYPE)).FirstOrDefault();
                            mailList.Add(new usersToEmail() { userName = ua.EMPLOYEENAME, email = ua.EMPLOYEEEMAIL, position = posName != null ? posName.TEXT : "" });
                        }
                    }
                }

                string RecipientEmail = string.Join(";", mailList.Select(x => x.email).ToArray());
                string userList = "<table>";
                foreach (var l in mailList)
                {
                    userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
                }
                userList = userList + "</table>";

                string RecipientName = string.Join(";", mailList.Select(x => x.userName).ToArray());
                string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
                var RecipientCC = "";
                var FromEmail = "testing@ptgdi.com";
                var subject = mail.SUBJECT;
                var body = mail.BODY;
                body = body.Replace("[user]", "All");
                body = body.Replace("[projectNo]", projectRequest.PROJECTNO);
                body = body.Replace("[projectDesc]", projectRequest.PROJECTDESCRIPTION);
                body = body.Replace("[planStartDate]", projectRequest.PLANSTARTDATE.HasValue ? projectRequest.PLANSTARTDATE.Value.ToString("dd MMMM yyyy") : "-");
                body = body.Replace("[planFinishDate]", projectRequest.PLANFINISHDATE.HasValue ? projectRequest.PLANFINISHDATE.Value.ToString("dd MMMM yyyy") : "-");
                body = body.Replace("[listuser]", userList);
                body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/MainProjectForm.aspx?projectId=" + projectRequest.OID);
                body = body.Replace("[assign]", accController.getEmployeeByBN(projectRequest.CREATEDBY).full_Name);
                body = body.Replace("\r\n", "");
                body = body.Replace("\\", "");
                emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);

                db.SaveChanges();

                var v = new
                {
                    request_no = projectRequest.PROJECTNO,
                };

                return Ok(getProjectById(projectRequest.OID));

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
        }

        [HttpPost]
        [ActionName("UpdateRequest")]
        public IHttpActionResult UpdateRequest(int param, projectObject projectObjectParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != projectObjectParam.id)
            {
                return BadRequest();
            }

            try
            {
                PROJECT projectRequest = db.PROJECTs.Find(param);

                projectRequest.PROJECTNO = projectObjectParam.projectNo;
                projectRequest.PROJECTDESCRIPTION = projectObjectParam.projectDescription;
                projectRequest.OWNERID = projectObjectParam.ownerId;
                projectRequest.OWNERNAME = projectObjectParam.ownerName;
                projectRequest.OWNEREMAIL = projectObjectParam.ownerEmail;
                projectRequest.SPONSORID = projectObjectParam.sponsorId;
                projectRequest.SPONSORNAME = projectObjectParam.sponsorName;
                projectRequest.SPONSOREMAIL = projectObjectParam.sponsorEmail;
                projectRequest.PROJECTMANAGERID = projectObjectParam.projectManagerId;
                projectRequest.PROJECTMANAGERNAME = projectObjectParam.projectManagerName;
                projectRequest.PROJECTMANAGEREMAIL = projectObjectParam.projectManagerEmail;
                projectRequest.PROJECTENGINEERID = projectObjectParam.projectEngineerId;
                projectRequest.PROJECTENGINEERNAME = projectObjectParam.projectEngineerName;
                projectRequest.PROJECTENGINEEREMAIL = projectObjectParam.projectEngineerEmail;
                projectRequest.MAINTENANCEREPID = projectObjectParam.maintenanceRepId;
                projectRequest.MAINTENANCEREPNAME = projectObjectParam.maintenanceRepName;
                projectRequest.MAINTENANCEREPEMAIL = projectObjectParam.maintenanceRepEmail;
                projectRequest.OPERATIONREPID = projectObjectParam.operationRepId;
                projectRequest.OPERATIONREPNAME = projectObjectParam.operationRepName;
                projectRequest.OPERATIONREPEMAIL = projectObjectParam.operationRepEmail;
                projectRequest.AREAINVOLVEDID = projectObjectParam.areaInvolvedId;
                projectRequest.PROJECTCATEGORY = projectObjectParam.projectCategory;
                projectRequest.PROJECTTYPE = projectObjectParam.projectType;
                projectRequest.PMONO = projectObjectParam.pmoNo;
                projectRequest.PLANSTARTDATE = projectObjectParam.planStartDate;
                projectRequest.PLANFINISHDATE = projectObjectParam.planFinishDate;
                projectRequest.ACTUALSTARTDATE = projectObjectParam.actualStartDate;
                projectRequest.ACTUALFINISHDATE = projectObjectParam.actualFinishDate;
                projectRequest.TOTALCOST = projectObjectParam.totalCost;
                projectRequest.SHAREFOLDERDOC = projectObjectParam.shareFolderDoc;
                projectRequest.IPTPROJECTID = projectObjectParam.iptProjectId;
                projectRequest.UPDATEDBY = GetCurrentNTUserId();
                projectRequest.UPDATEDDATE = DateTime.Now;
                projectRequest.MOMEWRID = projectObjectParam.momewrid;

                db.Entry(projectRequest).State = EntityState.Modified;
      
                //other position
                if (projectObjectParam.otherPositions != null)
                {
                    foreach (var otherPosition in projectObjectParam.otherPositions)
                    {
                        if (otherPosition.id == 0)
                        {
                            //insert other position
                            USERASSIGNMENT ua = new USERASSIGNMENT();
                            ua.PROJECTID = param;
                            ua.ASSIGNMENTTYPE = otherPosition.assignmentType;
                            ua.EMPLOYEEID = otherPosition.employeeId;
                            ua.EMPLOYEENAME = otherPosition.employeeName;
                            ua.EMPLOYEEEMAIL = otherPosition.employeeEmail;
                            ua.EMPLOYEEPOSITION = otherPosition.employeePosition;
                            ua.CREATEDDATE = DateTime.Now;
                            ua.CREATEDBY = GetCurrentNTUserId();
                            db.USERASSIGNMENTs.Add(ua);
                        }
                        else
                        {
                            USERASSIGNMENT ua = db.USERASSIGNMENTs.Find(otherPosition.id);
                            if (ua.ASSIGNMENTTYPE != otherPosition.assignmentType || ua.EMPLOYEEID != otherPosition.employeeId)
                            {
                                //modified other position
                                ua.ASSIGNMENTTYPE = otherPosition.assignmentType;
                                ua.EMPLOYEEID = otherPosition.employeeId;
                                ua.EMPLOYEENAME = otherPosition.employeeName;
                                ua.EMPLOYEEEMAIL = otherPosition.employeeEmail;
                                ua.EMPLOYEEPOSITION = otherPosition.employeePosition;
                                ua.UPDATEDDATE = DateTime.Now;
                                ua.UPDATEDBY = GetCurrentNTUserId();
                                db.Entry(ua).State = EntityState.Modified;
                            }
                        }
                    }



                    //delete other position
                    var listIdUAOnFile = projectObjectParam.otherPositions.Where(w => w.id != 0).Select(s => s.id);
                    var assignmentType = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject).Select(s => s.VALUE);
                    var deleteUA = db.USERASSIGNMENTs.Where(w => w.PROJECTID == param && assignmentType.Contains(w.ASSIGNMENTTYPE) && !listIdUAOnFile.Contains(w.OID));
                    db.USERASSIGNMENTs.RemoveRange(deleteUA);
                } else
                {
                    var assignmentType = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject).Select(s => s.VALUE);
                    var deleteUA = db.USERASSIGNMENTs.Where(w => w.PROJECTID == param && assignmentType.Contains(w.ASSIGNMENTTYPE));
                    db.USERASSIGNMENTs.RemoveRange(deleteUA);
                }

                db.SaveChanges();

                return Ok(getProjectById(param));

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
        }


        [HttpGet]
        [ActionName("listMyProject")]
        public IQueryable<projectObject> listMyProject(string param)
        {
            return listProjectByUser(GetCurrentNTUserId());
        }

        [HttpGet]
        [ActionName("listProjectByUser")]
        public IQueryable<projectObject> listProjectByUser(string param)
        {
            IQueryable<projectObject> projects =
                db.PROJECTs.Where(w => w.OWNERID == param || w.SPONSORID == param ||
                                  w.PROJECTMANAGERID == param || w.PROJECTENGINEERID == param ||
                                  w.MAINTENANCEREPID == param || w.OPERATIONREPID == param).
                            Select(s => new projectObject
                            {
                                id = s.OID,
                                projectNo = s.PROJECTNO,
                                projectDescription = s.PROJECTDESCRIPTION,
                                ownerId = s.OWNERID,
                                ownerName = s.OWNERNAME,
                                ownerEmail = s.OWNEREMAIL,
                                sponsorId = s.SPONSORID,
                                sponsorName = s.SPONSORNAME,
                                sponsorEmail = s.SPONSOREMAIL,
                                projectManagerId = s.PROJECTMANAGERID,
                                projectManagerName = s.PROJECTMANAGERNAME,
                                projectManagerEmail = s.PROJECTMANAGEREMAIL,
                                projectEngineerId = s.PROJECTENGINEERID,
                                projectEngineerName = s.PROJECTENGINEERNAME,
                                projectEngineerEmail = s.PROJECTENGINEEREMAIL,
                                maintenanceRepId = s.MAINTENANCEREPID,
                                maintenanceRepName = s.MAINTENANCEREPNAME,
                                maintenanceRepEmail = s.MAINTENANCEREPEMAIL,
                                operationRepId = s.OPERATIONREPID,
                                operationRepName = s.OPERATIONREPNAME,
                                operationRepEmail = s.OPERATIONREPEMAIL,
                                areaInvolvedId = s.AREAINVOLVEDID,
                                projectType = s.PROJECTTYPE,
                                pmoNo = s.PMONO,
                                planStartDate = s.PLANSTARTDATE,
                                planFinishDate = s.PLANFINISHDATE,
                                actualStartDate = s.ACTUALSTARTDATE,
                                actualFinishDate = s.ACTUALFINISHDATE,
                                totalCost = s.TOTALCOST,
                                shareFolderDoc = s.SHAREFOLDERDOC
                            });
            return projects;
        }

        private IEnumerable<PROJECT> userOnProject(string param)
        {
            return db.PROJECTs.Where(w => w.OWNERID == param || w.SPONSORID == param ||
                  w.PROJECTMANAGERID == param || w.PROJECTENGINEERID == param ||
                  w.MAINTENANCEREPID == param || w.OPERATIONREPID == param ||
                  w.USERASSIGNMENTs.Where(u => u.EMPLOYEEID == param).Count() > 0);
        }

        [HttpGet]
        [ActionName("isUserOnProject")]
        public object isUserOnProject(string param)
        {
            List<object> result = new List<object>();
            var getValue = (db.PROJECTs.Where(w => w.OWNERID == param || w.SPONSORID == param ||
                  w.PROJECTMANAGERID == param || w.PROJECTENGINEERID == param ||
                  w.MAINTENANCEREPID == param || w.OPERATIONREPID == param ||
                  w.USERASSIGNMENTs.Where(u => u.EMPLOYEEID == param).Count() > 0).Count() > 0) ? new { result = true } : new { result = false };
            result.Add(getValue);

            if (getValue.result == false)
            {
                result = new List<object>();
                var userRole = (db.USERROLEs.Where(w => w.USER.BADGENO == param && w.ROLE.ROLETYPE == "0").Count() > 0) ? new { result = true } : new { result = false };
                result.Add(userRole);
            }
            return result;
        }


        [HttpGet]
        [ActionName("listMainMyProject")]
        public object listMainMyProject(string param)
        {
            DateTime curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            int cekRoleTypeAllProject = db.USERROLEs.Count(w => w.USER.BADGENO == param && w.ROLE.ROLETYPE == "0");
            var pj = db.PROJECTs.Where(w => w.PROJECTCLOSEOUTs.Where(w1 => w1.ISAPPROVED == true).Count() < 1).AsQueryable();
            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);
            if (cekRoleTypeAllProject == 0)
            {
                pj = db.PROJECTs.Where(w => w.OWNERID == param || w.SPONSORID == param ||
                  w.PROJECTMANAGERID == param || w.PROJECTENGINEERID == param ||
                  w.MAINTENANCEREPID == param || w.OPERATIONREPID == param ||
                  w.USERASSIGNMENTs.Where(u => u.EMPLOYEEID == param && (u.ISACTIVE==true || otherPos.Select(so => so.VALUE).Contains(u.ASSIGNMENTTYPE))).Count() > 0);
            }
            DateTime[] dtArr = new DateTime[] { };

            var prj1 = pj.Select(s => new
            {
                uaCoPic = s.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                                       && s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE
                                       && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null).Count() > 0
                                )
                .Select(u => new userAssignmentObject
                {
                    projectId = u.PROJECTID,
                    assignmentType = u.ASSIGNMENTTYPE,
                    employeeId = u.EMPLOYEEID,
                    employeeName = u.EMPLOYEENAME
                }),
            }).Where(s => s.uaCoPic.Count()>0);

            var prj = pj.Select(s => new projectObject()
            {
                id = s.OID,
                projectNo = s.PROJECTNO,
                projectDescription = s.PROJECTDESCRIPTION,
                projectType = s.PROJECTTYPE,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                ownerId = s.OWNERID,
                ownerName = s.OWNERNAME,
                sponsorId = s.SPONSORID,
                sponsorName = s.SPONSORNAME,
                ownerSponsorName = new { owner = s.OWNERNAME, sponsor = s.SPONSORNAME },
                projectManagerId = s.PROJECTMANAGERID,
                projectManagerName = s.PROJECTMANAGERNAME,
                projectEngineerId = s.PROJECTENGINEERID,
                projectEngineerName = s.PROJECTENGINEERNAME,
                maintenanceRepId = s.MAINTENANCEREPID,
                maintenanceRepName = s.MAINTENANCEREPNAME,
                operationRepId = s.OPERATIONREPID,
                operationRepName = s.OPERATIONREPNAME,
                projectCategory = s.PROJECTCATEGORY,
                momewrid = s.MOMEWRID,
                userAssignments = s.USERASSIGNMENTs.Where(ua => (ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER || ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER) && ua.ISACTIVE == true).Select(u => new userAssignmentObject
                {
                    assignmentType = u.ASSIGNMENTTYPE,
                    employeeId = u.EMPLOYEEID,
                    employeeName = u.EMPLOYEENAME
                }),
                //totalCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == s.PROJECTNO).Sum(s2 => s2.AMOUNT))  + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTTOTALCOSTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                //totalCost = s.PROJECTTYPE == constants.OPERATING ? s.TOTALCOST : s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
                totalCost =  s.TOTALCOST,
                //totalCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == s.PROJECTNO).Sum(s2 => s2.AMOUNT))  + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTTOTALCOSTs.Where(w1 => w1.WBSNO.StartsWith(w.NETWORK)).Sum(s1 => s1.AMOUNT))),
                //actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                actualCost = (double?) s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)),
                //actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + s.COSTREPORTELEMENTs.Sum(sum => sum.COSTREPORTNETWORKs.Sum(s1 => s1.COSTREPORTELEMENT.COSTREPORTACTUALCOSTs.Where(w1 => w1.NETWORK.StartsWith(s1.NETWORK)).Sum(sum1 => sum1.AMOUNT))),
                //commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)),
                //commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + s.COSTREPORTELEMENTs.Sum(sum => sum.COSTREPORTNETWORKs.Sum(s1 => s1.COSTREPORTELEMENT.COSTREPORTCOMMITMENTs.Where(w1 => w1.NETWORK.StartsWith(s1.NETWORK)).Sum(sum1 => sum1.AMOUNT))),
                forecast = (double) s.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),
                pmTraficDate = s.PROJECTMANAGEMENTs.Min(s1 => s1.KEYDELIVERABLES.Where(w => w.PLANSTARTDATE.HasValue && w.PLANFINISHDATE.HasValue && w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_CANCELLED && w.STATUS != constants.STATUSIPROM_HOLD && !w.ACTUALFINISHDATE.HasValue).Min(s2 => s2.PLANFINISHDATE)),
                pmTrafic = s.PROJECTMANAGEMENTs.Where(s1 => s1.KEYDELIVERABLES.Count(c => c.PLANSTARTDATE.HasValue && c.PLANFINISHDATE.HasValue) > 0).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                esTraficDate = s.PROJECTDOCUMENTs.Where(w => /*w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && w.STATUS != constants.STATUSIPROM_COMPLETED &&*/ w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED && !w.ACTUALFINISHDATE.HasValue).Min(s2 => s2.PLANFINISHDATE),
                esTrafic = s.PROJECTDOCUMENTs.Where(w => w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                esTraficPic = s.PROJECTDOCUMENTs.Where(w => w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED && !w.ACTUALFINISHDATE.HasValue /* && w.PLANFINISHDATE.Value <= DateTime.Now */).ToList(),
                //                materialTraficDate = s.MATERIALCOMPONENTs.Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PRISSUEDDATE.HasValue).Count() > 0).Select(s1 => s1.MATERIALPLANs.Min(m => m.PRPLANDATE)).Min(),
                materialTraficDate =
                    s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) => new { x.MATERIALPURCHASEs, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).
                    Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PRISSUEDDATE.HasValue).Count() > 0).Select(ss => new { date = ss.PRPLANDATE })
                    .Union(s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) => new { x.MATERIALPURCHASEs, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).
                    Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PORAISEDDATE.HasValue).Count() > 0).Select(ss => new { date = ss.POPLANDATE }))
                    .Union(s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) => new { x.MATERIALSTATUS, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).
                    Where(ss => ss.MATERIALSTATUS.Where(w1 => !w1.SHIPMENTENDDATE.HasValue).Count() == 0).Select(d => new { date = d.ONSITEPLANDATE })).Min(m => m.date),
                materialByEngineer = s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) =>
                    new { x.ENGINEER, x.MATERIALSTATUS, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).Select(mb => new materialByEngineerObject {
                        engineer = mb.ENGINEER, prPlanDate = mb.PRPLANDATE, poPlanDate = mb.POPLANDATE, onSitePlanDate = mb.ONSITEPLANDATE
                    }),
                materialTrafic = s.MATERIALCOMPONENTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                materialTraficCount = s.MATERIALCOMPONENTs.Count(),
                contractAdminDate = s.PROJECTDOCUMENTs.Where(w => w.TENDER.CONTRACTNO != null).Min(m => m.PLANFINISHDATE),
                consTraficDate = s.CONSTRUCTIONSERVICEs.Where(w => w.STATUS != constants.STATUSCS_COMPLETED && w.STATUS != constants.STATUSCS_CLOSED && w.STATUS != constants.STATUSCS_CANCELLED).Min(s1 => s1.PLANFINISHDATE),
                //consTrafic = s.CONSTRUCTIONSERVICEs.Count() + db.TENDERINSTALLATIONs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).Count() +
                //    db.TENDERPUNCHLISTs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                consTrafic = db.TENDERINSTALLATIONs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).Count() +
                    db.TENDERPUNCHLISTs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                tenderInstallationsMaxDate = db.TENDERINSTALLATIONs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).
                                        Min(m => m.PLANFINISHDATE),
                tenderPunchlistsMaxDate = db.TENDERPUNCHLISTs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).
                                        Min(m => m.PLANFINISHDATE),
                projectDocumentTD = s.PROJECTDOCUMENTs.Where(pd => (!string.IsNullOrEmpty(pd.TENDER.CONTRACTNO) && pd.TENDER.VENDORSELECTEDID.HasValue)),
                //tenderInstallations = db.TENDERINSTALLATIONs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).Select(ti => new tenderInstallationObject {id= ti.OID, tenderId = ti.TENDERID, planFinishDate = ti.PLANFINISHDATE }),
                //tenderPunchlists = db.TENDERPUNCHLISTs.Where(w => !w.ACTUALFINISHDATE.HasValue && w.TENDER.PROJECTDOCUMENTs.Where(t => t.PROJECTID == s.OID).Count() > 0).Select(ti => new tenderPunchlistObject { id = ti.OID, tenderId = ti.TENDERID, planFinishDate = ti.PLANFINISHDATE }),
                handoverCloseOutTraficDate = s.PROJECTCLOSEOUTs.Select(s1 => s1.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue).Min(m => m.PLANDATE)).Min(),
                handoverCloseOutTrafic = s.PROJECTCLOSEOUTs.Where(s1 => s1.CLOSEOUTPROGRESSes.Where(x => x.PLANDATE.HasValue).Count() > 0 || s1.REASONCLOSURETYPE != null || s1.APPROVEDBY != null || s1.APPROVEDDATE.HasValue
                    || s1.COMMENTS != null || s1.ISAPPROVED != null || s1.REASONCLOSURETYPE != null && s1.REJECTEDBY != null
                    && s1.REJECTEDDATE.HasValue && s1.REJECTEDREMARK != null).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                handoverCloseOutTraficPic = "",
                //s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => (!cOP.SUBMITEDDATE.HasValue &&
                //(s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 && !cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1 && cop.SUBMITEDDATE.HasValue))).Count() > 0)
                //&& cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER))).Count() > 0 ?
                //"#ua#" : s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => ((!cOP.SUBMITEDDATE.HasValue &&
                //(s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 &&
                //!cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1
                //&& cop.SUBMITEDDATE.HasValue))).Count() > 0)) && (cOP.SETUPCLOSEOUTROLE.ISROLE == false))).Count() > 0 ? s.PROJECTMANAGERNAME :
                //db.ROLEs.Where(r => (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => (((!cOP.SUBMITEDDATE.HasValue &&
                //(s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 &&
                //!cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1
                //&& cop.SUBMITEDDATE.HasValue))).Count() > 0))) && r.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME)).Count() > 0)
                //&& r.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.USERNAME,
                handoverCloseOutTraficRole = "" /*s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => (!cOP.SUBMITEDDATE.HasValue &&
                 (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 && !cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1 && cop.SUBMITEDDATE.HasValue))).Count() > 0)
                 && cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR))).Count() > 0 ?
                constants.MC : s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => (!cOP.SUBMITEDDATE.HasValue &&
                 (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 && !cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1 && cop.SUBMITEDDATE.HasValue))).Count() > 0)
                 && cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER))).Count() > 0 ?
                constants.PC : s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => ((!cOP.SUBMITEDDATE.HasValue &&
                  (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 &&
                  !cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1
                  && cop.SUBMITEDDATE.HasValue))).Count() > 0)) && (cOP.SETUPCLOSEOUTROLE.ISROLE == false))).Count() > 0 ? constants.PM :
                 s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => ((!cOP.SUBMITEDDATE.HasValue &&
                 (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 &&
                 !cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1
                 && cop.SUBMITEDDATE.HasValue))).Count() > 0)))).FirstOrDefault().SETUPCLOSEOUTROLE.DESCRIPTION,
                uaCoPic = s.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => ((!cOP.SUBMITEDDATE.HasValue &&
                (s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cop => ((cOP.CLOSEOUTROLEID == 1 &&
                !cOP.SUBMITEDDATE.HasValue) || (cOP.CLOSEOUTROLEID > 1 && cop.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1
                && cop.SUBMITEDDATE.HasValue))).Count() > 0) && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == u.ASSIGNMENTTYPE)))).Count()>0) //(u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                        && s.PROJECTCLOSEOUTs.FirstOrDefault().CLOSEOUTPROGRESSes.Where(cOP => u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE 
                        && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null).Count()>0
                                ).Select(p => new {p.EMPLOYEEID, p.ASSIGNMENTTYPE, p.EMPLOYEENAME })
                .Select(u => new userAssignmentObject
                {
                    assignmentType = u.ASSIGNMENTTYPE,
                    employeeId = u.EMPLOYEEID,
                    employeeName = u.EMPLOYEENAME
                })*/,
                
                //cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                //            (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                //                s.PROJECTMANAGERNAME :
                //                db.ROLEs.Where(r => r.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && r.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.USERNAME,

                iptProjectId = s.IPTPROJECTID,
                shareFolderDoc = s.SHAREFOLDERDOC,
                roleInProject = s.PROJECTMANAGERID == param ? constants.USERPOSITION_PROJECTMANAGER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_PROJECT_CONTROLLER :
                                  s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == param).Count() > 0 ? constants.otherPositionProject :
                                  s.OWNERID == param ? constants.USERPOSITION_OWNER :
                                  s.SPONSORID == param ? constants.USERPOSITION_SPONSOR :
                                  s.PROJECTENGINEERID == param ? constants.USERPOSITION_PROJECTENGINEER :
                                  s.MAINTENANCEREPID == param ? constants.USERPOSITION_MAINTENANCEREP :
                                  s.OPERATIONREPID == param ? constants.USERPOSITION_OPERATIONREP :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEERDESIGNER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_DESIGNER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_MATERIAL_COORDINATOR : "",

                positionName = s.OWNERID == param ? constants.USERPOSITION_OWNER_NAME :
                                  s.SPONSORID == param ? constants.USERPOSITION_SPONSOR_NAME :
                                  s.PROJECTMANAGERID == param ? constants.USERPOSITION_PROJECTMANAGER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_PROJECT_CONTROLLER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == param).Count() > 0 ? otherPos.Where(op => s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == param).Select(sua => sua.ASSIGNMENTTYPE).Contains(op.VALUE)).FirstOrDefault().TEXT :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_MATERIAL_COORDINATOR_NAME :
                                  s.PROJECTENGINEERID == param ? constants.USERPOSITION_PROJECTENGINEER_NAME :
                                  s.MAINTENANCEREPID == param ? constants.USERPOSITION_MAINTENANCEREP_NAME :
                                  s.OPERATIONREPID == param ? constants.USERPOSITION_OPERATIONREP_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEERDESIGNER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_DESIGNER : ""
            }).ToList();
            var projects = prj.Select(s => new 
            {
                id = s.id,
                projectNo = s.projectNo,
                projectDescription = s.projectDescription,
                projectType = s.projectType,
                planStartDate = s.planStartDate,
                planFinishDate = s.planFinishDate,
                ownerId = s.ownerId,
                ownerName = s.ownerName,
                ownerSponsorName = s.ownerSponsorName,
                sponsorId = s.sponsorId,
                sponsorName = s.sponsorName,
                projectManagerId = s.projectManagerId,
                projectManagerName = s.projectManagerName,
                projectEngineerId = s.projectEngineerId,
                projectEngineerName = s.projectEngineerName,
                maintenanceRepId = s.maintenanceRepId,
                maintenanceRepName = s.maintenanceRepName,
                operationRepId = s.operationRepId,
                operationRepName = s.operationRepName,
                userAssignments = s.userAssignments,
                projectCategory = s.projectCategory,
                totalCost = s.totalCost,
                actualCost = s.actualCost,
                commitment = s.commitment,
                forecast = s.forecast,
                pmTrafic = s.pmTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.pmTraficDate.HasValue) ? traficStatus(s.pmTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                esTrafic = s.esTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.esTraficDate.HasValue) ? traficStatus(s.esTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                esTraficPicTextEwp = s.esTraficPic.Where(ewp => ewp.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).Where(ewp => ((ewp.PLANFINISHDATE>=s.esTraficDate && ewp.PLANFINISHDATE<= DateTime.Now) || (ewp.PLANFINISHDATE >= s.esTraficDate && s.esTraficDate > DateTime.Now  && ewp.PLANFINISHDATE <= DateTime.Now.AddDays(7))) ).Select(ewp => ewp.DOCUMENTBYNAME).Count() > 0 ? string.Join("; ", s.esTraficPic.Where(ewp => ewp.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).Where(ewp => ewp.PLANFINISHDATE >= s.esTraficDate).Select(ewp => ewp.DOCUMENTBYNAME).Distinct().ToList()) : "",
                esTraficPicTextDwg = s.esTraficPic.Where(ewp => ewp.DOCTYPE == constants.DOCUMENTTYPE_DESIGN).Where(ewp => ((ewp.PLANFINISHDATE >= s.esTraficDate && ewp.PLANFINISHDATE <= DateTime.Now) || (ewp.PLANFINISHDATE >= s.esTraficDate && s.esTraficDate > DateTime.Now && ewp.PLANFINISHDATE <= DateTime.Now.AddDays(7)))).Select(ewp => ewp.DOCUMENTBYNAME).Count() > 0 ? string.Join("; ", s.esTraficPic.Where(ewp => ewp.DOCTYPE == constants.DOCUMENTTYPE_DESIGN).Where(ewp => ewp.PLANFINISHDATE >= s.esTraficDate).Select(ewp => ewp.DOCUMENTBYNAME).Distinct().ToList()) : "",
                materialTraficPic = s.materialByEngineer.Count() > 0 ? string.Join(";", s.materialByEngineer.Where(mb => (mb.prPlanDate >= s.materialTraficDate ||
                      mb.poPlanDate >= s.materialTraficDate || mb.onSitePlanDate >= s.materialTraficDate)).Select(me => me.engineer).Distinct()) : "",
                materialTrafic = s.materialTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.materialTraficDate.HasValue) ? traficStatus(s.materialTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                consTrafic = s.consTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY :
                (s.tenderInstallationsMaxDate != null || s.tenderPunchlistsMaxDate != null) ?
                traficStatus(new[] {
                    s.tenderInstallationsMaxDate.HasValue? s.tenderInstallationsMaxDate.Value:DateTime.MaxValue,
                    s.tenderPunchlistsMaxDate.HasValue? s.tenderPunchlistsMaxDate.Value:DateTime.MaxValue }.Min(), null, null) : constants.TRAFFICLIGHT_GREEN,
                consTraficPic = s.projectDocumentTD.Where(pd => ((pd.TENDER.TENDERPUNCHLISTs.
                    Where(p => (p.PLANFINISHDATE <= s.tenderPunchlistsMaxDate)).Count()>0) || 
                    (pd.TENDER.TENDERINSTALLATIONs.Where(p => (p.PLANFINISHDATE <= s.tenderPunchlistsMaxDate)).Count() > 0))).Count() >0 ?
                    string.Join(";", s.projectDocumentTD.Select(pd => pd.DOCUMENTBYNAME)) : "" ,
                //(s.consTraficDate.HasValue && s.contractAdminDate.HasValue && s.tenderInstallationsMaxDate.HasValue && s.tenderPunchlistsMaxDate.HasValue) ?
                //                               traficStatus(new[] { s.contractAdminDate.Value, s.consTraficDate.Value, s.tenderInstallationsMaxDate.Value, s.tenderPunchlistsMaxDate.Value }.Min(), null, null) :
                //                               ((s.consTraficDate.HasValue) && (s.consTrafic != constants.TRAFFICLIGHT_GREY)) ?
                //                                   traficStatus(s.consTraficDate.Value, null, null) :
                //                                   (s.contractAdminDate.HasValue && (s.consTrafic != constants.TRAFFICLIGHT_GREY)) ?
                //                                       traficStatus(s.contractAdminDate.Value, null, null) :
                //                                       constants.TRAFFICLIGHT_GREEN,
                shareFolderDoc = s.shareFolderDoc,
                tenderInstallationsMaxDate = s.tenderInstallationsMaxDate,
                tenderPunchlistsMaxDate = s.tenderPunchlistsMaxDate,
                handoverCloseOutTrafic = s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.handoverCloseOutTraficDate.HasValue) ? traficStatus(s.handoverCloseOutTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                handoverCloseOutTraficPic = s.handoverCloseOutTraficPic=="#ua#"? string.Join(";", s.uaCoPic.Select(ua => ua.employeeName)) : s.handoverCloseOutTraficPic,
                handoverCloseOutTraficRole = s.handoverCloseOutTraficRole,
                roleInProject = s.roleInProject,
                positionName = s.positionName,
                iptProjectId = s.iptProjectId,
                ownerSponsorToString = (s.ownerName != "" || s.sponsorName != "") ? s.ownerName + ";" + s.sponsorName : null,
                PMPEToString = (s.projectManagerName != "" || s.projectEngineerName != "") ? s.projectManagerName + ";" + s.projectEngineerName : null,
                userAssignmentsToString = s.userAssignments.Select(su => su.employeeName).Count() > 0 ? string.Join("; ", s.userAssignments.Select(su => su.employeeName)) : null
            });
            return projects;
        }


        [HttpGet]
        [ActionName("checkAccessProjectCloseOut")]
        public Boolean checkAccessProjectCloseOut(int param)
        {
            var s = db.PROJECTs.Find(param);

            var pmTraficDate = s.PROJECTMANAGEMENTs.Min(s1 => s1.KEYDELIVERABLES.Where(w => w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_CANCELLED && w.STATUS != constants.STATUSIPROM_HOLD && !w.ACTUALFINISHDATE.HasValue).Min(s2 => s2.PLANFINISHDATE));
            var pmTrafic = s.PROJECTMANAGEMENTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "";
            var esTraficDate = s.PROJECTDOCUMENTs.Where(w => w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED && !w.ACTUALFINISHDATE.HasValue).Min(s2 => s2.PLANFINISHDATE);
            var esTrafic = s.PROJECTDOCUMENTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "";
            var materialTraficDate = s.MATERIALCOMPONENTs.Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PRISSUEDDATE.HasValue).Count() > 0).Select(s1 => s1.MATERIALPLANs.Min(m => m.PRPLANDATE)).Min();
            var materialTrafic = s.MATERIALCOMPONENTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "";
            var materialTraficCount = s.MATERIALCOMPONENTs.Where(w => w.MATERIALPURCHASEs.Where(w1 => w1.PORAISEDDATE.HasValue).Count() > 0).Count();
            var contractAdminDate = s.PROJECTDOCUMENTs.Where(w => (w.TENDERID.HasValue ? w.TENDER.CONTRACTNO != null : 1 == 0)).Min(m => m.PLANFINISHDATE);
            var consTraficDate = s.CONSTRUCTIONSERVICEs.Where(w => w.STATUS != constants.STATUSCS_COMPLETED && w.STATUS != constants.STATUSCS_CLOSED && w.STATUS != constants.STATUSCS_CANCELLED).Min(s1 => s1.PLANFINISHDATE);
            var consTrafic = s.CONSTRUCTIONSERVICEs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "";
            //var handoverCloseOutTraficDate = s.PROJECTCLOSEOUTs.Select(s1 => s1.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue).Min(m => m.PLANDATE)).Min();
            //var handoverCloseOutTrafic = s.PROJECTCLOSEOUTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "";


            pmTrafic = pmTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((pmTraficDate.HasValue) ? traficStatus(pmTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN);
            esTrafic = esTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((esTraficDate.HasValue) ? traficStatus(esTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN);
            materialTrafic = materialTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((materialTraficDate.HasValue) ? traficStatus(materialTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN);
            consTrafic = consTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((consTraficDate.HasValue) && contractAdminDate.HasValue) ?
                                           traficStatus(new[] { contractAdminDate.Value, consTraficDate.Value }.Max(), null, null) :
                                           ((consTraficDate.HasValue) && (consTrafic != constants.TRAFFICLIGHT_GREY)) ?
                                               traficStatus(consTraficDate.Value, null, null) :
                                               contractAdminDate.HasValue ?
                                                   traficStatus(contractAdminDate.Value, null, null) :
                                                   constants.TRAFFICLIGHT_GREEN;
            //handoverCloseOutTrafic = handoverCloseOutTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((handoverCloseOutTraficDate.HasValue) ? traficStatus(handoverCloseOutTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN);

            int countNotCompleted = (pmTrafic == constants.TRAFFICLIGHT_GREY || pmTrafic == constants.TRAFFICLIGHT_GREEN ? 0 : 1)
                + (esTrafic == constants.TRAFFICLIGHT_GREY || esTrafic == constants.TRAFFICLIGHT_GREEN ? 0 : 1)
                //+ (materialTrafic == constants.TRAFFICLIGHT_GREY || materialTrafic == constants.TRAFFICLIGHT_GREEN ? 0 : 1)
                /*+ (consTrafic == constants.TRAFFICLIGHT_GREY || consTrafic == constants.TRAFFICLIGHT_GREEN ? 0 : 1)*/;
            return countNotCompleted != 0 ? false : true;
        }

        [HttpGet]
        [ActionName("listSumaryProjectDetailEWP")]
        public object listSumaryProjectDetailEWP(int param)
        {
            var result =
                db.PROJECTDOCUMENTs.Where(w => w.PROJECTID == param && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER/* && !w.ACTUALFINISHDATE.HasValue*/).ToList().
                Select(s => new
                {
                    group = "PROJECTDOCUMENTs",
                    id = s.OID,
                    task = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? s.DOCNO + " - " + s.DOCTITLE : s.DRAWINGNO + " - " + s.DOCTITLE,
                    by = s.DOCUMENTBYNAME,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    status = s.STATUS,
                    documentTrafic = s.PLANSTARTDATE.HasValue ? traficStatus(s.PLANFINISHDATE.Value, s.ACTUALFINISHDATE, s.STATUS) : constants.TRAFFICLIGHT_GREY
                    //        }).OrderByDescending(o => !o.actualStartDate.HasValue ? o.planFinishDate : o.actualFinishDate);
                }).OrderBy(o => o.actualFinishDate).ThenBy(o => o.planFinishDate);
            return result;
        }

        /*
        [HttpGet]
        [ActionName("listSumaryProjectDetailMRLIst")]
        public object listSumaryProjectDetailMRLIst(int param)
        {
            return db.MATERIALBYENGINEERs.Where(w => w.PROJECTID == param).Where(w => !w.PRACTUALDATE.HasValue || !w.POACTUALDATE.HasValue || !w.ONSITEACTUALDATE.HasValue).ToList().Select(s => new 
            {
                id = s.OID,
                mrNo = s.MRNO,
                materialDesc = s.MATERIALDESC,
                prPlanDate = s.PRPLANDATE,
                prActualDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PRISSUEDDATE),
                poPlanDate = s.POPLANDATE,
                poActualDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PORAISEDDATE),
                onSitePlanDate = s.ONSITEPLANDATE,
                onSiteActualDate = db.MATERIALSTATUS.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.SHIPMENTENDDATE),
                rfqPlanDate = s.PRACTUALDATE.HasValue ? s.PRACTUALDATE.Value.AddDays(14) : (DateTime?)null,
                rfqActualDate = (DateTime?)null,
                revision = s.REVISION,
                engineer = string.IsNullOrEmpty(s.CREATEDBY) ? "-" : accController.getEmployeeByBN(s.CREATEDBY).full_Name,
                documentTrafic = s.POPLANDATE.HasValue ? traficStatus(s.POPLANDATE.Value, db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PORAISEDDATE), constants.TRAFFICLIGHT_GREY) : constants.TRAFFICLIGHT_GREY
            }).OrderByDescending(o => !o.prActualDate.HasValue ? o.prPlanDate : !o.poActualDate.HasValue ? o.poPlanDate : !o.onSiteActualDate.HasValue ? o.onSitePlanDate : o.onSiteActualDate);
        }
        */

        [HttpGet]
        [ActionName("listSumaryProjectDetailMRLIst")]
        public object listSumaryProjectDetailMRLIst(int param)
        {
            return db.MATERIALBYENGINEERs.Where(w => w.PROJECTID == param).Where(w => !w.PRACTUALDATE.HasValue || !w.POACTUALDATE.HasValue || !w.ONSITEACTUALDATE.HasValue).ToList().Select(s => new
            {
                id = s.OID,
                mrNo = s.MRNO,
                materialDesc = s.MATERIALDESC,
                prPlanDate = s.PRPLANDATE,
                prActualDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PRISSUEDDATE),
                poPlanDate = s.POPLANDATE,
                poActualDate = db.MATERIALPURCHASEs.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.PORAISEDDATE),
                onSitePlanDate = s.ONSITEPLANDATE,
                onSiteActualDate = db.MATERIALSTATUS.Where(w => w.MATERIALCOMPONENT.PROJECTID == param && w.MATERIALCOMPONENT.MRNO == s.MRNO).Max(m => m.SHIPMENTENDDATE),
                rfqPlanDate = s.PRACTUALDATE.HasValue ? s.PRACTUALDATE.Value.AddDays(14) : (DateTime?)null,
                rfqActualDate = (DateTime?)null,
                revision = s.REVISION,
                engineer = string.IsNullOrEmpty(s.CREATEDBY) ? "-" : accController.getEmployeeByBN(s.CREATEDBY).full_Name
            }).Select(s => new
            {
                id = s.id,
                mrNo = s.mrNo,
                materialDesc = s.materialDesc,
                prPlanDate = s.prPlanDate,
                prActualDate = s.prActualDate,
                poPlanDate = s.poPlanDate,
                poActualDate = s.poActualDate,
                onSitePlanDate = s.onSitePlanDate,
                onSiteActualDate = s.onSiteActualDate,
                rfqPlanDate = s.rfqPlanDate,
                rfqActualDate = s.rfqActualDate,
                revision = s.revision,
                engineer = s.engineer,
                documentTrafic = s.prPlanDate.HasValue || s.poPlanDate.HasValue || s.onSitePlanDate.HasValue ? traficStatus((!s.prActualDate.HasValue ? s.prPlanDate.Value : !s.poActualDate.HasValue ? s.poPlanDate.Value : s.onSitePlanDate.Value), (s.onSiteActualDate.HasValue ? s.poActualDate.HasValue ? (s.prActualDate.HasValue ? s.prActualDate : null) : null : null), constants.TRAFFICLIGHT_GREY) : constants.TRAFFICLIGHT_GREY
            }).OrderBy(o => !o.prActualDate.HasValue ? o.prPlanDate : !o.poActualDate.HasValue ? o.poPlanDate : !o.onSiteActualDate.HasValue ? o.onSitePlanDate : null).ThenByDescending(t => t.onSiteActualDate);
            //        }).OrderBy(o => o.actualFinishDate).ThenBy(o => o.planFinishDate);
        }

        [HttpGet]
        [ActionName("listSumaryProjectDetailOther")]
        public object listSumaryProjectDetailOther(int param)
        {
            var result =
                db.OTHERDEPARTMENTTASKs.Where(w => w.PROJECTID == param).ToList().
                Select(s => new
                {
                    group = "OTHERDEPARTMENTTASKs",
                    id = s.OID,
                    task = s.TASKNAME,
                    by = s.PICNAME + " (" + s.DEPARTMENTNAME + ")",
                    planStartDate = (DateTime?)null,
                    planFinishDate = s.PLANDATE,
                    actualStartDate = (DateTime?)null,
                    actualFinishDate = s.ACTUALDATE,
                    status = s.STATUS,
                    documentTrafic = s.PLANDATE.HasValue ? traficStatus(s.PLANDATE.Value, s.ACTUALDATE, s.STATUS) : ""
                }).Union(db.REQUESTOTHERUSERs.Where(w => w.PROJECTID == param).ToList().
                Select(s => new
                {
                    group = "REQUESTOTHERUSERs",
                    id = s.OID,
                    task = s.MAILSUBJECT,
                    by = s.MAILTO,
                    planStartDate = s.REQUESTDATE,
                    planFinishDate = s.RESPONSEREQUIREDBY,
                    actualStartDate = (DateTime?)null,
                    actualFinishDate = s.STATUS == constants.STATUSCS_COMPLETED || s.STATUS == constants.STATUSCS_CLOSED ? s.UPDATEDDATE : (DateTime?)null,
                    status = s.STATUS,
                    documentTrafic = s.RESPONSEREQUIREDBY.HasValue ? traficStatus(s.RESPONSEREQUIREDBY.Value, s.STATUS == constants.STATUSCS_COMPLETED ? s.UPDATEDDATE : (DateTime?)null, s.STATUS) : ""
                    //                })).OrderByDescending(o => !o.actualStartDate.HasValue ? o.planFinishDate : o.actualFinishDate);
                })).OrderBy(o => o.actualFinishDate).ThenBy(o => o.planFinishDate);
            return result;
        }

        [HttpGet]
        [ActionName("archiveProjectList")]
        public IEnumerable<projectObject> archiveProjectList(string param, string projectNo, string projectDesc, string sponsorParam,
            string pmParam, string peParam, string engineerParam, string statusArchived)
        {
            if (string.IsNullOrEmpty(projectNo) && string.IsNullOrEmpty(projectDesc) && string.IsNullOrEmpty(sponsorParam) &&
                string.IsNullOrEmpty(pmParam) && string.IsNullOrEmpty(peParam) && string.IsNullOrEmpty(engineerParam) && string.IsNullOrEmpty(statusArchived))
            {
                return new List<projectObject>();
            }
            else
            {
                IQueryable<PROJECT> allProject = db.PROJECTs.Where(w => w.PROJECTCLOSEOUTs.Where(w1 => w1.ISAPPROVED == true).Count() != 0);

                //cek role jika bukan admin
                if (param != constants.ROLEADMIN) allProject = allProject.Where(w => w.OWNERID == param || w.SPONSORID == param ||
                                      w.PROJECTMANAGERID == param || w.PROJECTENGINEERID == param ||
                                      w.MAINTENANCEREPID == param || w.OPERATIONREPID == param ||
                                      w.USERASSIGNMENTs.Where(u => u.EMPLOYEEID == param).Count() > 0);

                //filter by project no
                if (!string.IsNullOrEmpty(projectNo)) allProject = allProject.Where(w => w.PROJECTNO.Contains(projectNo));

                //filter by project description
                if (!string.IsNullOrEmpty(projectDesc)) allProject = allProject.Where(w => w.PROJECTDESCRIPTION.Contains(projectDesc));

                //filter by Sponsor
                if (!string.IsNullOrEmpty(sponsorParam))
                {
                    var sponsors = sponsorParam.Split(';');
                    allProject = allProject.Where(w => sponsors.Contains(w.SPONSORID));
                }

                //filter by project manager
                if (!string.IsNullOrEmpty(pmParam))
                {
                    var pms = pmParam.Split(';');
                    allProject = allProject.Where(w => pms.Contains(w.PROJECTMANAGERID));
                }

                //filter by project Engineer
                if (!string.IsNullOrEmpty(peParam))
                {
                    var pes = peParam.Split(';');
                    allProject = allProject.Where(w => pes.Contains(w.PROJECTENGINEERID));
                    //allProject = allProject.Where(w => w.PROJECTENGINEERID == peParam);
                }

                //filter by Engineer & Designer
                if (!string.IsNullOrEmpty(engineerParam))
                {
                    var engineers = engineerParam.Split(';');
                    allProject = allProject.Where(w => w.USERASSIGNMENTs.Where(ua => (ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER || ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER) && engineers.Contains(ua.EMPLOYEEID)).Count() > 0);
                }

                //filter by status
                if (!string.IsNullOrEmpty(statusArchived))
                {
                    var pes = statusArchived.Split(';');
                    allProject = allProject.Where(w => w.PROJECTCLOSEOUTs.Count(p => pes.Contains(p.REASONCLOSURETYPE)) > 0);
                    //allProject = allProject.Where(w => w.PROJECTENGINEERID == peParam);
                }


                var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);

                IEnumerable<projectObject> projects =
                    allProject.Select(s => new projectObject
                    {
                        id = s.OID,
                        projectNo = s.PROJECTNO,
                        projectDescription = s.PROJECTDESCRIPTION,
                        sponsorId = s.SPONSORID,
                        sponsorName = s.SPONSORNAME,
                        projectManagerId = s.PROJECTMANAGERID,
                        projectManagerName = s.PROJECTMANAGERNAME,
                        projectEngineerId = s.PROJECTENGINEERID,
                        projectEngineerName = s.PROJECTENGINEERNAME,
                        maintenanceRepId = s.MAINTENANCEREPID,
                        maintenanceRepName = s.MAINTENANCEREPNAME,
                        operationRepId = s.OPERATIONREPID,
                        operationRepName = s.OPERATIONREPNAME,
                        userAssignments = s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER || ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER).Select(u => new userAssignmentObject
                        {
                            assignmentType = u.ASSIGNMENTTYPE,
                            employeeId = u.EMPLOYEEID,
                            employeeName = u.EMPLOYEENAME
                        }).OrderBy(o => o.assignmentType),
                        projectCategory = s.PROJECTCATEGORY,
                        //totalCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == s.PROJECTNO).Sum(s2 => s2.AMOUNT)),
                        //actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                        //commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                        totalCost = s.PROJECTTYPE == constants.OPERATING ? s.TOTALCOST : s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
                        actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)),
                        commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)),
                        roleInProject = s.PROJECTMANAGERID == param ? constants.USERPOSITION_PROJECTMANAGER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_PROJECT_CONTROLLER :
                                  s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == param).Count() > 0 ? constants.otherPositionProject :
                                  s.OWNERID == param ? constants.USERPOSITION_OWNER :
                                  s.SPONSORID == param ? constants.USERPOSITION_SPONSOR :
                                  s.PROJECTENGINEERID == param ? constants.USERPOSITION_PROJECTENGINEER :
                                  s.MAINTENANCEREPID == param ? constants.USERPOSITION_MAINTENANCEREP :
                                  s.OPERATIONREPID == param ? constants.USERPOSITION_OPERATIONREP :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEERDESIGNER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_DESIGNER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_MATERIAL_COORDINATOR : "",

                        iptProjectId = s.IPTPROJECTID,
                        planStartDate = s.PLANSTARTDATE,
                        planFinishDate = s.PLANFINISHDATE,
                        status = db.LOOKUPs.Where(l => l.TYPE == constants.closeOutReason && l.VALUE == s.PROJECTCLOSEOUTs.FirstOrDefault().REASONCLOSURETYPE).FirstOrDefault().TEXT,
                        positionName = s.OWNERID == param ? constants.USERPOSITION_OWNER_NAME :
                                  s.SPONSORID == param ? constants.USERPOSITION_SPONSOR_NAME :
                                  s.PROJECTMANAGERID == param ? constants.USERPOSITION_PROJECTMANAGER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_PROJECT_CONTROLLER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == param).Count() > 0 ? otherPos.Where(op => s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == param).Select(sua => sua.ASSIGNMENTTYPE).Contains(op.VALUE)).FirstOrDefault().TEXT :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_MATERIAL_COORDINATOR_NAME :
                                  s.PROJECTENGINEERID == param ? constants.USERPOSITION_PROJECTENGINEER_NAME :
                                  s.MAINTENANCEREPID == param ? constants.USERPOSITION_MAINTENANCEREP_NAME :
                                  s.OPERATIONREPID == param ? constants.USERPOSITION_OPERATIONREP_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEERDESIGNER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_ENGINEER_NAME :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == param).Count() > 0 ? constants.USERPOSITION_DESIGNER : ""
                    });
                return projects;
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [ActionName("listCapitalProject")]
        public object listCapitalProject(int param, String area, DateTime? dateFrom, DateTime? dateTo)
        {
            var arr = new List<Int32>();
            if (area != "0" && area != "[]")
                arr = JsonConvert.DeserializeObject<List<Int32>>(area);
            else area = null;
            var listResult = db.PROJECTs.Where(w => w.PROJECTTYPE == "CAPITAL" && (arr.Contains(w.AREAINVOLVED.OID) || area == null)).Select(s => new
            {
                area = s.AREAINVOLVED.AREADESC,
                actualDone = ((s.ACTUALFINISHDATE.Value <= dateTo && s.ACTUALFINISHDATE.Value >= dateFrom)) ? 1 : 0,
                ongoing = ((s.ACTUALSTARTDATE.Value <= dateTo && s.ACTUALSTARTDATE.Value >= dateFrom) && (s.ACTUALFINISHDATE == null)) ? 1 : 0,
                backLog = ((s.PLANSTARTDATE.Value <= dateTo && s.PLANSTARTDATE.Value >= dateFrom) && (s.ACTUALSTARTDATE == null)) ? 1 : 0,
                target = (((s.ACTUALSTARTDATE.Value <= dateTo && s.ACTUALSTARTDATE.Value >= dateFrom) && s.PLANSTARTDATE.Value == s.ACTUALSTARTDATE.Value) &&
                      ((s.ACTUALFINISHDATE.Value <= dateTo && s.ACTUALFINISHDATE.Value >= dateFrom) && s.PLANFINISHDATE.Value == s.ACTUALFINISHDATE.Value)) ? 1 : 0,
            }).GroupBy(g => new { g.area }).Select(s => new
            {
                area = s.Key.area != null ? s.Key.area : "-",
                actualDone = s.Count(z => z.actualDone == 1),
                ongoing = s.Count(z => z.ongoing == 1),
                backLog = s.Count(z => z.backLog == 1),
                target = s.Count(z => z.target == 1),
                totalProject = s.Count()
            });
            return listResult;
        }

        [HttpGet]
        [ActionName("listOperatingProject")]
        public object listOperatingProject(int param, String area, DateTime? dateFrom, DateTime? dateTo)
        {
            var arr = new List<Int32>();
            if (area != "0" && area != "[]")
                arr = JsonConvert.DeserializeObject<List<Int32>>(area);
            else area = null;
            var listResult = db.PROJECTs.Where(w => w.PROJECTTYPE == "OPERATING" && (arr.Contains(w.AREAINVOLVED.OID) || area == null)).Select(s => new
            {
                area = s.AREAINVOLVED.AREADESC,
                actualDone = ((s.ACTUALFINISHDATE.Value <= dateTo && s.ACTUALFINISHDATE.Value >= dateFrom)) ? 1 : 0,
                ongoing = ((s.ACTUALSTARTDATE.Value <= dateTo && s.ACTUALSTARTDATE.Value >= dateFrom) && (s.ACTUALFINISHDATE == null)) ? 1 : 0,
                backLog = ((s.PLANSTARTDATE.Value <= dateTo && s.PLANSTARTDATE.Value >= dateFrom) && (s.ACTUALSTARTDATE == null)) ? 1 : 0,
                target = (((s.ACTUALSTARTDATE.Value <= dateTo && s.ACTUALSTARTDATE.Value >= dateFrom) && s.PLANSTARTDATE.Value == s.ACTUALSTARTDATE.Value) &&
                    ((s.ACTUALFINISHDATE.Value <= dateTo && s.ACTUALFINISHDATE.Value >= dateFrom) && s.PLANFINISHDATE.Value == s.ACTUALFINISHDATE.Value)) ? 1 : 0,
            }).GroupBy(g => new { g.area }).Select(s => new
            {
                area = s.Key.area != null ? s.Key.area : "-",
                actualDone = s.Count(z => z.actualDone == 1),
                ongoing = s.Count(z => z.ongoing == 1),
                backLog = s.Count(z => z.backLog == 1),
                target = s.Count(z => z.target == 1),
                totalProject = s.Count()
            });
            return listResult;
        }


        [HttpGet]
        [ActionName("reportBudgetPerformancebyAreaO")]
        public object reportBudgetPerformancebyAreaO(int param, DateTime? yearFrom, DateTime? yearTo)
        {
            var listResult = db.PROJECTs.Where(w => w.PROJECTTYPE.ToLower() == "operating"  && (w.ACTUALSTARTDATE.Value <= yearTo && w.ACTUALSTARTDATE.Value >= yearFrom) && ((w.ACTUALFINISHDATE.Value >= yearFrom && w.ACTUALFINISHDATE.Value <= yearTo) || w.ACTUALFINISHDATE == null)).Select(s => new
            {
                areaDesc = s.AREAINVOLVED.AREADESC,
                estCost = s.TOTALCOST == null ? 0 : s.TOTALCOST,
                actualCost = db.COSTREPORTACTUALCOSTs.Count(x => x.PROJECTNO == s.PROJECTNO) == 0 ? 0 : db.COSTREPORTACTUALCOSTs.Where(x => x.PROJECTNO == s.PROJECTNO).Sum(z => z.AMOUNT),
            }).Where(d => d.areaDesc != null).GroupBy(c => c.areaDesc).Select(s => new
            {
                area = s.Key,
                estCost = s.Sum(q => q.estCost),
                actCost = s.Sum(z => z.actualCost)
            }); ;
            return listResult;
        }

        [HttpGet]
        [ActionName("reportBudgetPerformancebyAreaC")]
        public object reportBudgetPerformancebyAreaC(int param, DateTime? yearFrom, DateTime? yearTo)
        {
            var listResult = db.PROJECTs.Where(w => w.PROJECTTYPE.ToLower() == "capital" && (w.ACTUALSTARTDATE.Value <= yearTo && w.ACTUALSTARTDATE.Value >= yearFrom) && ((w.ACTUALFINISHDATE.Value >= yearFrom && w.ACTUALFINISHDATE.Value <= yearTo) || w.ACTUALFINISHDATE == null)).Select(s => new
            {
                areaDesc = s.AREAINVOLVED.AREADESC,
                estCost = s.TOTALCOST == null ? 0 : s.TOTALCOST,
                actualCost = db.COSTREPORTACTUALCOSTs.Count(x => x.PROJECTNO == s.PROJECTNO) == 0 ? 0 : db.COSTREPORTACTUALCOSTs.Where(x => x.PROJECTNO == s.PROJECTNO).Sum(z => z.AMOUNT),
            }).Where(d => d.areaDesc != null).GroupBy(c => c.areaDesc).Select(s => new
            {
                area = s.Key,
                estCost = s.Sum(q => q.estCost),
                actCost = s.Sum(z => z.actualCost)
            }); ;
            return listResult;
        }

        [HttpGet]
        [ActionName("ProjectDataByBNandPID")]
        public IEnumerable<projectObject> ProjectDataByBNandPID(string bn, int pID)
        {
            DateTime curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);

            int cekRoleTypeAllProject = db.USERROLEs.Count(w => w.USER.BADGENO == bn && w.ROLE.ROLETYPE == "0");
            var pj = db.PROJECTs.Where(x => x.OID == pID).AsQueryable();
            if (cekRoleTypeAllProject == 0)
            {
                pj = db.PROJECTs.Where(w => w.OID == pID && (w.OWNERID == bn || w.SPONSORID == bn ||
                 w.PROJECTMANAGERID == bn || w.PROJECTENGINEERID == bn ||
                 w.MAINTENANCEREPID == bn || w.OPERATIONREPID == bn ||
                 w.USERASSIGNMENTs.Where(u => u.EMPLOYEEID == bn).Count() > 0));
            }

            var prj = pj.Select(s => new projectObject
            {
                id = s.OID,
                projectNo = s.PROJECTNO,
                projectDescription = s.PROJECTDESCRIPTION,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                ownerId = s.OWNERID,
                ownerName = s.OWNERNAME,
                sponsorId = s.SPONSORID,
                sponsorName = s.SPONSORNAME,
                projectManagerId = s.PROJECTMANAGERID,
                projectManagerName = s.PROJECTMANAGERNAME,
                projectEngineerId = s.PROJECTENGINEERID,
                projectEngineerName = s.PROJECTENGINEERNAME,
                maintenanceRepId = s.MAINTENANCEREPID,
                maintenanceRepName = s.MAINTENANCEREPNAME,
                operationRepId = s.OPERATIONREPID,
                operationRepName = s.OPERATIONREPNAME,
                userAssignments = s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER /*|| ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER */).Select(u => new userAssignmentObject
                {
                    assignmentType = u.ASSIGNMENTTYPE,
                    employeeId = u.EMPLOYEEID,
                    employeeName = u.EMPLOYEENAME
                }),
                //totalCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == s.PROJECTNO).Sum(s2 => s2.AMOUNT))  + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTTOTALCOSTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                totalCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
                //totalCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == s.PROJECTNO).Sum(s2 => s2.AMOUNT))  + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTTOTALCOSTs.Where(w1 => w1.WBSNO.StartsWith(w.NETWORK)).Sum(s1 => s1.AMOUNT))),
                //actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)),
                //actualCost = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + s.COSTREPORTELEMENTs.Sum(sum => sum.COSTREPORTNETWORKs.Sum(s1 => s1.COSTREPORTELEMENT.COSTREPORTACTUALCOSTs.Where(w1 => w1.NETWORK.StartsWith(s1.NETWORK)).Sum(sum1 => sum1.AMOUNT))),
                //commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + (s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Where(w => s1.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)),
                //commitment = s.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + s.COSTREPORTELEMENTs.Sum(sum => sum.COSTREPORTNETWORKs.Sum(s1 => s1.COSTREPORTELEMENT.COSTREPORTCOMMITMENTs.Where(w1 => w1.NETWORK.StartsWith(s1.NETWORK)).Sum(sum1 => sum1.AMOUNT))),
                pmTraficDate = s.PROJECTMANAGEMENTs.Min(s1 => s1.KEYDELIVERABLES.Where(w => w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_CANCELLED && w.STATUS != constants.STATUSIPROM_HOLD && !w.ACTUALFINISHDATE.HasValue).Min(s2 => s2.PLANFINISHDATE)),
                pmTrafic = s.PROJECTMANAGEMENTs.Where(s1 => s1.KEYDELIVERABLES.Count > 0).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                esTraficDate = s.PROJECTDOCUMENTs.Where(w => w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED && !w.ACTUALFINISHDATE.HasValue).Min(s2 => s2.PLANFINISHDATE),
                esTrafic = s.PROJECTDOCUMENTs.Where(w => w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER /*&& !w.ACTUALFINISHDATE.HasValue*/).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                //                materialTraficDate = s.MATERIALCOMPONENTs.Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PRISSUEDDATE.HasValue).Count() > 0).Select(s1 => s1.MATERIALPLANs.Min(m => m.PRPLANDATE)).Min(),
                materialTraficDate =
                    s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) => new { x.MATERIALPURCHASEs, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).
                    Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PRISSUEDDATE.HasValue).Count() > 0).Select(ss => new { date = ss.PRPLANDATE })
                    .Union(s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) => new { x.MATERIALPURCHASEs, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).
                    Where(w => w.MATERIALPURCHASEs.Where(w1 => !w1.PORAISEDDATE.HasValue).Count() > 0).Select(ss => new { date = ss.POPLANDATE }))
                    .Union(s.MATERIALCOMPONENTs.Join(s.MATERIALBYENGINEERs, x => new { x.MRNO, x.PROJECTID }, y => new { y.MRNO, y.PROJECTID }, (x, y) =>
                    new { x.MATERIALSTATUS, y.PRPLANDATE, y.POPLANDATE, y.ONSITEPLANDATE }).
                    Where(ss => ss.MATERIALSTATUS.Where(w1 => !w1.SHIPMENTENDDATE.HasValue).Count() == 0).Select(d => new { date = d.ONSITEPLANDATE })).Min(m => m.date),
                materialTrafic = s.MATERIALCOMPONENTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                materialTraficCount = s.MATERIALCOMPONENTs.Count(),
                contractAdminDate = s.PROJECTDOCUMENTs.Where(w => w.TENDER.CONTRACTNO != null).Min(m => m.PLANFINISHDATE),
                consTraficDate = s.CONSTRUCTIONSERVICEs.Where(w => w.STATUS != constants.STATUSCS_COMPLETED && w.STATUS != constants.STATUSCS_CLOSED && w.STATUS != constants.STATUSCS_CANCELLED).Min(s1 => s1.PLANFINISHDATE),
                consTrafic = s.CONSTRUCTIONSERVICEs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                handoverCloseOutTraficDate = s.PROJECTCLOSEOUTs.Select(s1 => s1.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue).Min(m => m.PLANDATE)).Min(),
                handoverCloseOutTrafic = s.PROJECTCLOSEOUTs.Where(s1 => s1.CLOSEOUTPROGRESSes.Where(x => x.PLANDATE.HasValue).Count() > 0 || s1.REASONCLOSURETYPE != null || s1.APPROVEDBY != null || s1.APPROVEDDATE.HasValue
                    || s1.COMMENTS != null || s1.ISAPPROVED != null || s1.REASONCLOSURETYPE != null && s1.REJECTEDBY != null
                    && s1.REJECTEDDATE.HasValue && s1.REJECTEDREMARK != null).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                iptProjectId = s.IPTPROJECTID,
                roleInProject = s.PROJECTMANAGERID == bn ? constants.USERPOSITION_PROJECTMANAGER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == bn).Count() > 0 ? constants.USERPOSITION_PROJECT_CONTROLLER :
                                  s.USERASSIGNMENTs.Where(ua => otherPos.Select(so => so.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == bn).Count() > 0 ? constants.otherPositionProject :
                                  s.OWNERID == bn ? constants.USERPOSITION_OWNER :
                                  s.SPONSORID == bn ? constants.USERPOSITION_SPONSOR :
                                  s.PROJECTENGINEERID == bn ? constants.USERPOSITION_PROJECTENGINEER :
                                  s.MAINTENANCEREPID == bn ? constants.USERPOSITION_MAINTENANCEREP :
                                  s.OPERATIONREPID == bn ? constants.USERPOSITION_OPERATIONREP :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == bn).Count() > 0 ? constants.USERPOSITION_ENGINEERDESIGNER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == bn).Count() > 0 ? constants.USERPOSITION_ENGINEER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == bn).Count() > 0 ? constants.USERPOSITION_DESIGNER :
                                  s.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == bn).Count() > 0 ? constants.USERPOSITION_MATERIAL_COORDINATOR : "",
            }).ToList();
            IEnumerable<projectObject> projects = prj.Select(s => new projectObject
            {
                id = s.id,
                projectNo = s.projectNo,
                projectDescription = s.projectDescription,
                planStartDate = s.planStartDate,
                planFinishDate = s.planFinishDate,
                ownerId = s.ownerId,
                ownerName = s.ownerName,
                sponsorId = s.sponsorId,
                sponsorName = s.sponsorName,
                projectManagerId = s.projectManagerId,
                projectManagerName = s.projectManagerName,
                projectEngineerId = s.projectEngineerId,
                projectEngineerName = s.projectEngineerName,
                maintenanceRepId = s.maintenanceRepId,
                maintenanceRepName = s.maintenanceRepName,
                operationRepId = s.operationRepId,
                operationRepName = s.operationRepName,
                userAssignments = s.userAssignments,
                totalCost = s.totalCost,
                actualCost = s.actualCost,
                commitment = s.commitment,
                pmTrafic = s.pmTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.pmTraficDate.HasValue) ? traficStatus(s.pmTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                esTrafic = s.esTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.esTraficDate.HasValue) ? traficStatus(s.esTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                materialTrafic = s.materialTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.materialTraficDate.HasValue) ? traficStatus(s.materialTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                consTrafic = s.consTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.consTraficDate.HasValue) && s.contractAdminDate.HasValue) ?
                                               traficStatus(new[] { s.contractAdminDate.Value, s.consTraficDate.Value }.Max(), null, null) :
                                               ((s.consTraficDate.HasValue) && (s.consTrafic != constants.TRAFFICLIGHT_GREY)) ?
                                                   traficStatus(s.consTraficDate.Value, null, null) :
                                                   s.contractAdminDate.HasValue ?
                                                       traficStatus(s.contractAdminDate.Value, null, null) :
                                                       constants.TRAFFICLIGHT_GREEN,
                handoverCloseOutTrafic = s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.handoverCloseOutTraficDate.HasValue) ? traficStatus(s.handoverCloseOutTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                roleInProject = s.roleInProject,
                iptProjectId = s.iptProjectId,
                userAssignmentsToString = string.Join("; ", s.userAssignments.Select(su => su.employeeName))
            });

            return projects;
        }

        [HttpGet]
        [ActionName("listDeliverable")]
        public object listDeliverable(string param, string projectNo, string description, string status)
        {
            var deliverable = new List<object>();
            //var tsDetail = new object();
            var tsD = db.PROJECTs.Where(x => ((x.PROJECTDOCUMENTs.Where(c => status.Contains(c.STATUS)).Count() >= 1) || status == null)
            && (x.PROJECTDESCRIPTION.Contains(description) || description == null) && (x.PROJECTNO.Contains(projectNo) || projectNo == null)).OrderBy(f => f.PROJECTNO);
            if (tsD.Count() > 0)
            {
                var tsDetail = tsD.Select(d => d.PROJECTDOCUMENTs.Where(c => status.Contains(c.STATUS) || status == null).OrderBy(o => o.DOCNO).
                    Select(v => new
                    {
                        id = v.OID,
                        projectNo = d.PROJECTNO,
                        desc = d.PROJECTDESCRIPTION,
                        docNo = v.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? v.DRAWINGNO != null ? v.DRAWINGNO : v.DOCNO : v.DOCNO,
                        docTitle = v.DOCTITLE,
                        engineer = (v.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || v.DOCTYPE==constants.DOCUMENTTYPE_DESIGN_CONFORMITY) ? v.DOCUMENTBYNAME : "",
                        designer = v.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? v.DOCUMENTBYNAME : "",
                        planStartDate = v.PLANSTARTDATE,
                        planFinishDate = v.PLANFINISHDATE,
                        actualStartDate = v.ACTUALSTARTDATE,
                        actualFinishDate = v.ACTUALFINISHDATE,
                        status = v.STATUS,
                        statusDesc = db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == v.STATUS).FirstOrDefault().TEXT,
                        remark = v.REMARK,
                    })).ToList();
                foreach (var n in tsDetail)
                {
                    foreach (var m in n)
                    {
                        deliverable.Add(m);
                    }
                }
            }

            return deliverable;
        }

        [HttpGet]
        [ActionName("listDeliverableEx")]
        public object listDeliverableEx(string param, string projectNo, string description, string status)
        {
            var deliverable = new List<object>();
            //var tsDetail = new object();
            var tsD = db.PROJECTs.Where(x => ((x.PROJECTDOCUMENTs.Where(c => status.Contains(c.STATUS)).Count() >= 1) || status == null)
            && (x.PROJECTDESCRIPTION.Contains(description) || description == null) && (x.PROJECTNO.Contains(projectNo) || projectNo == null)).OrderBy(f => f.PROJECTNO);
            if (tsD.Count() > 0)
            {
                var tsDetail = tsD
                    .Select(d => new
                    {
                        id = d.OID,
                        projectNo = d.PROJECTNO,
                        desc = d.PROJECTDESCRIPTION,
                        projectCategory = d.PROJECTCATEGORY,
                        projectManager = d.PROJECTMANAGERNAME,
                        projectEngineer = d.PROJECTENGINEERNAME,
                        projectController = d.USERASSIGNMENTs.Where(x => x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.ISACTIVE==true).Select(c => c.EMPLOYEENAME).ToList(),
                        projectOfficer = d.USERASSIGNMENTs.Where(x => x.ASSIGNMENTTYPE == constants.PROJECT_OFFICER && x.ISACTIVE == true).Select(c => c.EMPLOYEENAME).ToList(),
                        materialCoordinator = d.USERASSIGNMENTs.Where(x => x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.ISACTIVE == true).Select(c => c.EMPLOYEENAME).ToList(),
                        //totalCost = d.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == d.PROJECTNO).Sum(s2 => s2.AMOUNT)),
                        totalCost = d.PROJECTTYPE == constants.OPERATING ? d.TOTALCOST : d.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
//                        actualCost = d.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) != 0 ? d.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) : 0,
                        actualCost = (double?) d.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)),
                        commitment = d.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)),
                        forecast = (double?) d.FORECASTSPENDINGs.Sum(dew => dew.FORECASTVALUE),
                        projManagement = d.PROJECTMANAGEMENTs.Select(s => new projectManagementObject
                        {
                            scope = s.SCOPE,
                            benefit = s.BENEFIT,
                            accomplishment = s.ACCOMPLISHMENT,
                            details = s.DETAILS,
                            contractor = s.CONTRACTOR,
                            manageKeyIssue = s.MANAGEKEYISSUE
                        }).FirstOrDefault(),
                        userAssignments = d.USERASSIGNMENTs.Where(e => e.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).Select(c => new
                        {
                            employeeName = c.EMPLOYEENAME + (d.PROJECTDOCUMENTs.Count(b => b.DOCUMENTBYID == c.EMPLOYEEID) > 0 ? "" : "##"),
                        }).ToList(),
                        doc = d.PROJECTDOCUMENTs.Count>0?"Yes":"No",
                    }).ToList();
                foreach (var m in tsDetail)
                {
                    List<int> splitProjectCategory = !string.IsNullOrEmpty(m.projectCategory) ? m.projectCategory.Split('|').Select(Int32.Parse).ToList() : new List<int>();
                        var s = new
                        {
                            id = m.id,
                            projectNo = m.projectNo,
                            desc = m.desc,
                            projectCategoryText = string.Join("; ", db.MASTERPROJECTCATEGORies.Where(w => splitProjectCategory.Contains(w.OID)).Select(s1 => s1.DESCRIPTION)),
                            projectCategory = m.projectCategory,
                            projectManager = m.projectManager,
                            projectEngineer = m.projectEngineer,
                            projectController = m.projectController,
                            pcText = string.Join(",", m.projectController),
                            poText = string.Join(",", m.projectOfficer),
                            mcText = string.Join(",", m.materialCoordinator),
                            totalCost = m.totalCost,
                            actualCost = m.actualCost as double?,
                            commitment = m.commitment,
                            forecast = m.forecast,
                            assignedCost = (m.actualCost as double?) + m.commitment,
                            remainingBudget = m.totalCost - (m.actualCost as double?) + m.commitment,
                            scope = m.projManagement != null ?m.projManagement.scope:"",
                            benefit = m.projManagement != null ? m.projManagement.benefit:"",
                            accomplishment = m.projManagement != null ? m.projManagement.accomplishment:"",
                            details = m.projManagement != null ? m.projManagement.details:"",
                            contractor = m.projManagement != null ? m.projManagement.contractor:"",
                            manageKeyIssue = m.projManagement != null ? m.projManagement.manageKeyIssue:"",
                            userAssignments = m.userAssignments,
                            doc = m.doc,
                            uaText = string.Join(",", m.userAssignments.Select(n=> n.employeeName)),
                        };
                        deliverable.Add(s);
                }
            }

            return deliverable;
        }
        
    }



    public class filterArchiveProjectList
    {
        public string projectNo { get; set; }
        public string projectDesc { get; set; }
        public string sponsorParam { get; set; }
        public string pmParam { get; set; }
        public string peParam { get; set; }
        public string engineerParam { get; set; }
    }
}