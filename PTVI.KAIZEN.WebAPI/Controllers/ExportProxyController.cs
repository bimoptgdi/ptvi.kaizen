﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ExportProxyController : KAIZENController
    {
        // GET api/exportproxy
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/exportproxy/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/exportproxy
        public HttpResponseMessage Post(FileParamProxy value)
        {
            var fileContent = Convert.FromBase64String(value.base64);
            HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.OK);
            message.Content = new ByteArrayContent(fileContent);
            message.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(value.contentType);
            var cd = new ContentDispositionHeaderValue("attachment");
            cd.FileName = value.fileName;
            message.Content.Headers.ContentDisposition = cd;
            return message;
        }
        private byte[] getByteFromBase64String(string p)
        {
            return Convert.FromBase64String(p);
        }
        // PUT api/exportproxy/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/exportproxy/5
        public void Delete(int id)
        {
        }
    }
    public class FileParamProxy
    {
        public string contentType { get; set; }
        public string base64 { get; set; }
        public string fileName { get; set; }
    }

}
