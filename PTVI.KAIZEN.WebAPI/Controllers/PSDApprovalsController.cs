﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using SourceCode.Workflow.Client;
using System.Data.Entity.Validation;
using PTVI.KAIZEN.WebAPI.K2Objects;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class PSDApprovalsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/PSDApprovals
        public IQueryable<psdApprovalObject> GetPSDAPPROVALs()
        {
             return db.PSDAPPROVALs.Select(s => new psdApprovalObject {
                 id = s.OID,
                 projectId = s.PROJECTID,
                 remarks = s.REMARKS,
                 documentid = s.DOCUMENTID,
                 approvalStatus = s.APPROVALSTATUS,
                 submittedBy = s.SUBMITTEDBY,
                 submittedDate = s.SUBMITTEDDATE,
                 completionDate = s.COMPLETIONDATE,
                 wfId = s.WFID,
                 createdDate = s.CREATEDDATE,
                 createdBy = s.CREATEDBY,
                 updatedDate = s.UPDATEDDATE,
                 updatedBy = s.UPDATEDBY
             });
        }

        // GET: api/PSDApprovals/5
        [ResponseType(typeof(PSDAPPROVAL))]
        public IHttpActionResult GetPSDAPPROVAL(int id)
        {
            PSDAPPROVAL s = db.PSDAPPROVALs.Find(id);
            if (s == null)
            {
                return NotFound();
            }

            var result = new psdApprovalObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                remarks = s.REMARKS,
                documentid = s.DOCUMENTID,
                fileNamePsd = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.FILENAME : "-",
                refDocId = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.REFDOCID != null ? s.SUPPORTINGDOCUMENT.REFDOCID : null : null,
                approvalStatus = s.APPROVALSTATUS,
                submittedBy = s.SUBMITTEDBY,
                submittedDate = s.SUBMITTEDDATE,
                completionDate = s.COMPLETIONDATE,
                wfId = s.WFID,
                createdDate = s.CREATEDDATE,
                createdBy = s.CREATEDBY,
                updatedDate = s.UPDATEDDATE,
                updatedBy = s.UPDATEDBY,
                project = new projectObject
                {
                    projectNo = s.PROJECT.PROJECTNO,
                    projectDescription = s.PROJECT.PROJECTDESCRIPTION,
                    projectManagerName = s.PROJECT.PROJECTMANAGERNAME,
                    projectType = s.PROJECT.PROJECTTYPE,
                }
            };

                 List<K2History> listK2History = new List<K2History>();
            try
            {

                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {

                    listK2History = GetListFromExternalData<K2History>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_WORKLIST_HISTORY_BYWFIDIN%7C@wfID=" + result.wfId);

                }

                result.K2Histories = listK2History;
                result.k2CurrentActivityName = listK2History.Where(w => w.Status == "Active").Count() > 0 ? listK2History.FirstOrDefault(w => w.Status == "Active").ActivityName : null;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return Ok(result);
        }

        // PUT: api/PSDApprovals/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPSDAPPROVAL(int id, psdApprovalObject pSDAPPROVAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pSDAPPROVAL.id)
            {
                return BadRequest();
            }

            PSDAPPROVAL s = db.PSDAPPROVALs.Find(id);
            s.REMARKS = pSDAPPROVAL.remarks;
            s.UPDATEDBY = GetCurrentNTUserId();
            s.UPDATEDDATE = DateTime.Now;

            db.Entry(s).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PSDAPPROVALExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            pSDAPPROVAL.project = null;
            pSDAPPROVAL.K2Histories = null;
            return Ok(pSDAPPROVAL);
        }

        // POST: api/PSDApprovals
        [ResponseType(typeof(PSDAPPROVAL))]
        public IHttpActionResult PostPSDAPPROVAL(psdApprovalObject pSDAPPROVAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PSDAPPROVAL s = new PSDAPPROVAL();
            s.PROJECTID = pSDAPPROVAL.projectId;
            s.REMARKS = pSDAPPROVAL.remarks;
            s.SUBMITTEDBY = GetCurrentNTUserId();
            s.SUBMITTEDDATE = DateTime.Now;
            s.CREATEDBY = GetCurrentNTUserId();
            s.CREATEDDATE = DateTime.Now;


            db.PSDAPPROVALs.Add(s);
            db.SaveChanges();
            //*
            // call K2
            //registerToK2(s.OID); //=> dipindah setelah proses upload
            // --call K2
            //*/
            pSDAPPROVAL.id = s.OID;
            return CreatedAtRoute("DefaultApi", new { id = s.OID }, pSDAPPROVAL);
        }

        [HttpGet]
        [ActionName("getFileStream")]
        public string getFileStream(int param)
        {
            #region variable Get File Attachment Start
            PSDAPPROVAL s = db.PSDAPPROVALs.Find(param);
            String htmlCode = null;
            #endregion variable Get File Attachment Start

            #region Get File Attachment Start
            GENERALPARAMETER DocServer = db.GENERALPARAMETERs.Where(w => w.TITLE == "DocServer").FirstOrDefault();
            try
            {
                var urlFile = DocServer.VALUE.Replace("[id]", s.SUPPORTINGDOCUMENT.REFDOCID).Replace("[op]", "y");

                WebClient client = new WebClient();
                htmlCode = client.DownloadString(urlFile);

            }
            catch (Exception ex)
            {
                WriteLog("get file failed function getImageStream");
            }
            finally
            {

            }
            #endregion Get File Attachment 


            return htmlCode;
        }

        [HttpGet]
        [ActionName("registerToK2")]
        public string registerToK2(int param)
        {
            string result = "";
            PSDAPPROVAL s = db.PSDAPPROVALs.Find(param);
            //*
            // call K2
            K2ConnectController k2 = new K2ConnectController();
            System.IO.Stream _fileStream = null;
            System.IO.StreamWriter writer = null;

            try
            {
                // call K2
                string K2PSDConnectCode = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2PSDConnectCode;
                ProcessInstance edfProcessInstance = k2.Connect(K2PSDConnectCode);

                string k2AppID = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2AppID;
                edfProcessInstance.DataFields[k2AppID].Value = s.OID;


                if (s.SUPPORTINGDOCUMENT != null)
                {
                    string htmlCode = getFileStream(param);

                    _fileStream = new System.IO.MemoryStream();
                    writer = new System.IO.StreamWriter(_fileStream);

                    //System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    writer.Write(htmlCode);
                    writer.Flush();
                    _fileStream.Position = 0;
                    using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"D:\" + s.SUPPORTINGDOCUMENT.FILENAME))
                    {
                        outfile.Write(htmlCode);
                    }

                    edfProcessInstance.AddAttachment(s.SUPPORTINGDOCUMENT.FILENAME, _fileStream);
                }
                k2.StartProcessInstance(edfProcessInstance);
                result = "K2 Sukses";
            }
            catch (DbEntityValidationException exc)
            {
                var errorMessage = "";
                foreach (var error in exc.EntityValidationErrors)
                {
                    foreach (var validationError in error.ValidationErrors)
                        errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                }
                WriteLog("K2 Problem:");
                WriteLog(errorMessage);
                throw new ApplicationException(errorMessage);
                result = "K2 Problem";
            }
            catch (Exception ex)
            {
                WriteLog("K2 Problem:");
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
                result = "K2 Problem";
            }
            finally
            {
                if (_fileStream != null)
                    _fileStream.Close();

                if (writer != null)
                    writer.Close();

                k2.Disconnect();
            }
            return result;
        }

        // DELETE: api/PSDApprovals/5
        [ResponseType(typeof(PSDAPPROVAL))]
        public IHttpActionResult DeletePSDAPPROVAL(int id)
        {
            PSDAPPROVAL pSDAPPROVAL = db.PSDAPPROVALs.Find(id);
            if (pSDAPPROVAL == null)
            {
                return NotFound();
            }

            db.PSDAPPROVALs.Remove(pSDAPPROVAL);
            db.SaveChanges();

            return Ok(pSDAPPROVAL);
        }

        // GET: api/PSDApprovals/5
        [HttpGet]
        [ActionName("getPsdByProjectID")]
        public IEnumerable<psdApprovalObject> getPsdByProjectID(int param)
        {
            return db.PSDAPPROVALs.Where(w => w.PROJECTID == param).Select(s => new psdApprovalObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                remarks = s.REMARKS,
                documentid = s.DOCUMENTID,
                fileNamePsd = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.FILENAME : "-",
                refDocId = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.REFDOCID != null ? s.SUPPORTINGDOCUMENT.REFDOCID : null : null,
                approvalStatus = s.APPROVALSTATUS,
                submittedBy = s.SUBMITTEDBY,
                submittedDate = s.SUBMITTEDDATE,
                completionDate = s.COMPLETIONDATE,
                wfId = s.WFID,
                createdDate = s.CREATEDDATE,
                createdBy = s.CREATEDBY,
                updatedDate = s.UPDATEDDATE,
                updatedBy = s.UPDATEDBY
            });
        }

        [HttpPut]
        [ActionName("UpdateSubmitRevisi")]
        public psdApprovalObject UpdateSubmitRevisi(int param, psdApprovalObject resRequest)
        {
            PSDAPPROVAL s = db.PSDAPPROVALs.Find(param);
            s.REMARKS = resRequest.remarks;
            s.UPDATEDBY = GetCurrentNTUserId();
            s.UPDATEDDATE = DateTime.Now;

            db.Entry(s).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                //// call K2
                //K2ManagementController k2Management = new K2ManagementController();
                //k2Management.Connect();

                //K2ConnectController k2Connect = new K2ConnectController();

                //try
                //{
                //    string nextApprover = getNextK2Approver("");

                //    K2ConnectController connectK2 = new K2ConnectController();

                //    List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                //    EWRRequestDTO reqData = new EWRRequestDTO();
                //    reqData.WF_ID = s.WFID; //set wf id nya

                //    k2Object.Add(reqData);
                //    connectK2.populateFlightWorkListItems(k2Object);

                //    string sn = reqData.K2WorklistItems.
                //                Where(d => d.procId == s.WFID &&
                //                d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                //                Select(d => d.serialNo).FirstOrDefault();

                //    k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REVISE_K2, reqData.Remark, null);
                //}
                //catch (DbEntityValidationException exc)
                //{
                //    var errorMessage = "";
                //    foreach (var error in exc.EntityValidationErrors)
                //    {
                //        foreach (var validationError in error.ValidationErrors)
                //            errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                //    }
                //    WriteLog(errorMessage);
                //    throw new ApplicationException(errorMessage);

                //}
                //finally
                //{
                //    k2Management.Close();
                //    k2Connect.Disconnect();
                //}
                return resRequest;
            }
            catch (Exception ex)
            {
                if (!PSDAPPROVALExists(resRequest.id))
                {
                    WriteLog(ex.ToString());
                    return null;
                }
                else
                {
                    throw;
                }
            }

        }


        [HttpPost]
        [ActionName("ApproveRequest")]
        public IHttpActionResult ApproveRequest(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                PSDAPPROVAL resRequest = db.PSDAPPROVALs.Find(param);
                if (resRequest != null)
                {
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WFID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WFID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_APPROVE_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("RejectRequest")]
        public IHttpActionResult RejectRequest(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                PSDAPPROVAL resRequest = db.PSDAPPROVALs.Find(param);
                if (resRequest != null)
                {
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WFID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WFID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REJECT_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("RequestNeedRevised")]
        public IHttpActionResult RequestNeedRevised(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                PSDAPPROVAL resRequest = db.PSDAPPROVALs.Find(param);
                if (resRequest != null)
                {
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WFID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WFID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        //string sn = k2WorklistItem.
                        //            Where(d => d.procId == resRequest.WF_ID &&
                        //            d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                        //            Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REVISE_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PSDAPPROVALExists(int id)
        {
            return db.PSDAPPROVALs.Count(e => e.OID == id) > 0;
        }
    }
}