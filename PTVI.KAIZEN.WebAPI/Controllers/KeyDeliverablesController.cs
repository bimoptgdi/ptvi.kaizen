﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class KeyDeliverablesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/KeyDeliverables
        public IQueryable<KEYDELIVERABLE> GetKEYDELIVERABLES()
        {
            return db.KEYDELIVERABLES;
        }

        // GET: api/KeyDeliverables/5
        [ResponseType(typeof(KEYDELIVERABLE))]
        public IHttpActionResult GetKEYDELIVERABLE(int id)
        {
            KEYDELIVERABLE kEYDELIVERABLE = db.KEYDELIVERABLES.Find(id);
            if (kEYDELIVERABLE == null)
            {
                return NotFound();
            }

            return Ok(kEYDELIVERABLE);
        }

        // PUT: api/KeyDeliverables/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKEYDELIVERABLE(int id, KEYDELIVERABLE kEYDELIVERABLE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kEYDELIVERABLE.OID)
            {
                return BadRequest();
            }

            db.Entry(kEYDELIVERABLE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KEYDELIVERABLEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KeyDeliverables
        [ResponseType(typeof(KEYDELIVERABLE))]
        public IHttpActionResult PostKEYDELIVERABLE(KEYDELIVERABLE kEYDELIVERABLE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.KEYDELIVERABLES.Add(kEYDELIVERABLE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = kEYDELIVERABLE.OID }, kEYDELIVERABLE);
        }

        // DELETE: api/KeyDeliverables/5
        [ResponseType(typeof(KEYDELIVERABLE))]
        public IHttpActionResult DeleteKEYDELIVERABLE(int id)
        {
            KEYDELIVERABLE kEYDELIVERABLE = db.KEYDELIVERABLES.Find(id);
            if (kEYDELIVERABLE == null)
            {
                return NotFound();
            }

            var keydeliverablesProgresses = db.KEYDELIVERABLESPROGRESSes.Where(w => w.KEYDELIVERABLESID == id);
            if (keydeliverablesProgresses != null)
            {
                db.KEYDELIVERABLESPROGRESSes.RemoveRange(keydeliverablesProgresses);
            }

            var projectDoc = db.PROJECTDOCUMENTs.Where(w => w.KEYDELIVERABLESID == id);
            if (projectDoc != null)
            {
                db.PROJECTDOCUMENTs.RemoveRange(projectDoc);
            }

            db.Entry(kEYDELIVERABLE).State = EntityState.Deleted;

            db.SaveChanges();

            return Ok(kEYDELIVERABLE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KEYDELIVERABLEExists(int id)
        {
            return db.KEYDELIVERABLES.Count(e => e.OID == id) > 0;
        }


        [HttpGet]
        [ActionName("KDById")]
        public keyDeliverableObject KDById(int param)
        {
            keyDeliverableObject keyDeliverables =
                db.KEYDELIVERABLES.Where(w => w.OID == param).
                Select(s => new keyDeliverableObject
                {
                    id = s.OID,
                    projectManagementId = s.PROJECTMANAGEMENTID,
                    projectId = s.PROJECTMANAGEMENT.PROJECTID,
                    name = s.NAME,
                    weightPercentage = s.WEIGHTPERCENTAGE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    status = s.STATUS,
                    remark = s.REMARK,
                    revision = s.REVISION
                }).FirstOrDefault();
            return keyDeliverables;
        }

        [HttpGet]
        [ActionName("listKDWithProject")]
        public IEnumerable<Object> listKDWithProject(int param, string psd, string pfd, string asd, string afd)
        {
            DateTime? nullDateTime = null;
            DateTime? psd1 = psd!=null?DateTime.Parse(psd) : nullDateTime;
            DateTime? pfd1 = pfd != null ? DateTime.Parse(pfd) : nullDateTime;
            //            var psd2 = psd1.AddDays(1);
            var coKDDate = DateTime.Parse(db.GENERALPARAMETERs.Where(d => d.TITLE == constants.cutofkeydeliverable).FirstOrDefault().VALUE);
            //.ToList().Where(w => ((w.Plan_Start_Date <= DateTime.Parse(psd)) && (w.Plan_Finish_Date >= DateTime.Parse(psd))));
            IEnumerable<Object> keyDeliverables =
                db.KEYDELIVERABLES.Where(w => (/*(w.PROJECTMANAGEMENT.PROJECTID >0)&& */ ((psd != null && pfd != null) ? (w.PLANSTARTDATE >= psd1) && (w.PLANFINISHDATE <= pfd1):true) && (w.PROJECTMANAGEMENT.PROJECT.CREATEDDATE >= coKDDate))).ToList().
                Select(s => new
                {
                    groupSeq = 1,
                    seq = System.Text.RegularExpressions.Regex.IsMatch(s.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(s.NAME)).SEQ : s.NAME,
                    id = s.OID,
                    projectManagementId = s.PROJECTMANAGEMENTID,
                    projectId = s.PROJECTMANAGEMENT.PROJECTID,
                    projectPm = s.PROJECTMANAGEMENT.PROJECT.PROJECTMANAGERNAME,
                    projectNo = s.PROJECTMANAGEMENT.PROJECT.PROJECTNO,
                    projectCat = (db.MASTERPROJECTCATEGORies.Count(c => (c.OID.ToString() == s.PROJECTMANAGEMENT.PROJECT.PROJECTCATEGORY)) > 0) ? db.MASTERPROJECTCATEGORies.Where(c => (c.OID.ToString() == s.PROJECTMANAGEMENT.PROJECT.PROJECTCATEGORY)).FirstOrDefault().DESCRIPTION : "",
                    projectEng = s.PROJECTMANAGEMENT.PROJECT.PROJECTENGINEERNAME,
                    pc = s.PROJECTMANAGEMENT.PROJECT.USERASSIGNMENTs.Where(pc => pc.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER).Select(pc => pc.EMPLOYEENAME).Distinct(),
                    mc = s.PROJECTMANAGEMENT.PROJECT.USERASSIGNMENTs.Where(pc => pc.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR).Select(pc => pc.EMPLOYEENAME).Distinct(),
                    po = s.PROJECTMANAGEMENT.PROJECT.USERASSIGNMENTs.Where(pc => pc.ASSIGNMENTTYPE == constants.PROJECT_OFFICER).Select(pc => pc.EMPLOYEENAME).Distinct(),
                    engineers = s.PROJECTMANAGEMENT.PROJECT.USERASSIGNMENTs.Where(pc => ((pc.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER) || (pc.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER))).Select(pc => pc.EMPLOYEENAME).Distinct(),
                    totalBudget = s.PROJECTMANAGEMENT.PROJECT.PROJECTTYPE == constants.OPERATING ? s.PROJECTMANAGEMENT.PROJECT.TOTALCOST : s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
                    etc = (double)s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(p => p.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(p => p.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + (double)s.PROJECTMANAGEMENT.PROJECT.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),
                    assignedCost = (s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(cr => cr.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)))
                        + s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(cr => cr.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT))
                        + (double)s.PROJECTMANAGEMENT.PROJECT.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),//data.actualCost + data.commitment+data.forecast;
                    remainingBudget = (s.PROJECTMANAGEMENT.PROJECT.PROJECTTYPE == constants.OPERATING ? s.PROJECTMANAGEMENT.PROJECT.TOTALCOST : s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)))
                        - (s.PROJECTMANAGEMENT.PROJECT.COSTREPORTELEMENTs.Sum(cr => cr.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT))),//data.totalCost - (data.actualCost),
                    scope = s.PROJECTMANAGEMENT.SCOPE,
                    objective = s.PROJECTMANAGEMENT.BENEFIT,
                    accomplishment = s.PROJECTMANAGEMENT.ACCOMPLISHMENT,
                    nextPlan = s.PROJECTMANAGEMENT.DETAILS,
                    contractor = s.PROJECTMANAGEMENT.CONTRACTOR,
                    riskIssue = s.PROJECTMANAGEMENT.MANAGEKEYISSUE,
                    doc = s.PROJECTMANAGEMENT.PROJECT.PROJECTDOCUMENTs.Count() > 0 ? "Yes" : "No",
                    Project_Title_PM = s.PROJECTMANAGEMENT.PROJECT.PROJECTNO + " | " + s.PROJECTMANAGEMENT.PROJECT.PROJECTDESCRIPTION,
                    Key_Deliverable = System.Text.RegularExpressions.Regex.IsMatch(s.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(s.NAME)).KEYDELIVERABLES : s.NAME,
                    Plan_Start_Date = s.PLANSTARTDATE,
                    Plan_Finish_Date = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                }).ToList();

           var m  = db.CLOSEOUTPROGRESSes.Where(w => (/*(w.PROJECTCLOSEOUT.PROJECTID > 0) &&*/ ((psd != null && pfd != null)?(w.PLANDATE >= psd1) && (w.PLANDATE <= pfd1):true) && (w.PROJECTCLOSEOUT.PROJECT.CREATEDDATE >= coKDDate))).OrderBy(o => o.SETUPCLOSEOUTROLE.OID).ThenBy(o => o.SETUPCLOSEOUTROLE.SEQ);
            IEnumerable<Object> closeOut = m.
                            Select(s => new
                            {
                                groupSeq = 2,
                                seq = s.SETUPCLOSEOUTROLE.SEQ,
                                id = s.OID,
                                projectManagementId = (int?)0,
                                projectId = s.PROJECTCLOSEOUT.PROJECTID,
                                projectPm = s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGERNAME,
                                projectNo = s.PROJECTCLOSEOUT.PROJECT.PROJECTNO,
                                projectCat = (db.MASTERPROJECTCATEGORies.Count(c => (c.OID.ToString() == s.PROJECTCLOSEOUT.PROJECT.PROJECTCATEGORY)) > 0) ? db.MASTERPROJECTCATEGORies.Where(c => (c.OID.ToString() == s.PROJECTCLOSEOUT.PROJECT.PROJECTCATEGORY)).FirstOrDefault().DESCRIPTION : "",
                                projectEng = s.PROJECTCLOSEOUT.PROJECT.PROJECTENGINEERNAME,
                                pc = s.PROJECTCLOSEOUT.PROJECT.USERASSIGNMENTs.Where(pc => pc.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER).Select(pc => pc.EMPLOYEENAME).Distinct(),
                                mc = s.PROJECTCLOSEOUT.PROJECT.USERASSIGNMENTs.Where(pc => pc.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR).Select(pc => pc.EMPLOYEENAME).Distinct(),
                                po = s.PROJECTCLOSEOUT.PROJECT.USERASSIGNMENTs.Where(pc => pc.ASSIGNMENTTYPE == constants.PROJECT_OFFICER).Select(pc => pc.EMPLOYEENAME).Distinct(),
                                engineers = s.PROJECTCLOSEOUT.PROJECT.USERASSIGNMENTs.Where(pc => ((pc.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER) || (pc.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER))).Select(pc => pc.EMPLOYEENAME).Distinct(),
                                totalBudget = s.PROJECTCLOSEOUT.PROJECT.PROJECTTYPE == constants.OPERATING ? s.PROJECTCLOSEOUT.PROJECT.TOTALCOST : s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
                                etc = (double)s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(p => p.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(p => p.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + (double)s.PROJECTCLOSEOUT.PROJECT.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),
                                assignedCost = (s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(cr => cr.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)))
                                    + s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(cr => cr.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT))
                                    + (double)s.PROJECTCLOSEOUT.PROJECT.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),//data.actualCost + data.commitment+data.forecast;
                                remainingBudget = (s.PROJECTCLOSEOUT.PROJECT.PROJECTTYPE == constants.OPERATING ? s.PROJECTCLOSEOUT.PROJECT.TOTALCOST : s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)))
                                    - (s.PROJECTCLOSEOUT.PROJECT.COSTREPORTELEMENTs.Sum(cr => cr.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT))),//data.totalCost - (data.actualCost),
                                scope = (s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.Count()>0) ? s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.FirstOrDefault().SCOPE : "",
                                objective = (s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.Count() > 0) ? s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.FirstOrDefault().BENEFIT : "",
                                accomplishment = (s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.Count() > 0) ? s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.FirstOrDefault().ACCOMPLISHMENT : "",
                                nextPlan = (s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.Count() > 0) ? s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.FirstOrDefault().DETAILS : "",
                                contractor = (s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.Count() > 0) ? s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.FirstOrDefault().CONTRACTOR : "",
                                riskIssue = (s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.Count() > 0) ? s.PROJECTCLOSEOUT.PROJECT.PROJECTMANAGEMENTs.FirstOrDefault().MANAGEKEYISSUE : "",
                                doc = s.PROJECTCLOSEOUT.PROJECT.PROJECTDOCUMENTs.Count() > 0 ? "Yes" : "No",
                                Project_Title_PM = s.PROJECTCLOSEOUT.PROJECT.PROJECTNO + " | " + s.PROJECTCLOSEOUT.PROJECT.PROJECTDESCRIPTION,
                                Key_Deliverable = s.SETUPCLOSEOUTROLE.DESCRIPTION+" CLOSE",
                                Plan_Start_Date = (DateTime?)null,
                                Plan_Finish_Date = s.PLANDATE,
                                actualStartDate = (DateTime?)null,
                                actualFinishDate = s.SUBMITEDDATE,
                            });
            var mm = keyDeliverables.Union(closeOut);
            return mm; 
        }

        [HttpGet]
        [ActionName("listKDByProjectId")]
        public IQueryable<keyDeliverableObject> listKDByProjectId(int param)
        {
            IQueryable<keyDeliverableObject> keyDeliverables =
                db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param).
                Select(s => new keyDeliverableObject
                {
                    id = s.OID,
                    projectManagementId = s.PROJECTMANAGEMENTID,
                    projectId = s.PROJECTMANAGEMENT.PROJECTID,
                    name = s.NAME,
                    //nameText = db.MASTERKEYDELIVERABLES.FirstOrDefault(x => x.OID == Int32.Parse (s.NAME) ).KEYDELIVERABLES,
                    weightPercentage = s.WEIGHTPERCENTAGE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    seq = s.SEQ,
                    status = s.STATUS,
                    remark = s.REMARK, 
                    revision = s.REVISION
                }).OrderBy(x => x.planStartDate);
            return keyDeliverables;
        }

        [HttpGet]
        [ActionName("listKeyDByProjectIdActive")]
        public IOrderedEnumerable<keyDeliverableObject> listKeyDByProjectIdActive(int param)
        {
            var keyD =
                 db.KEYDELIVERABLES.Where(w => ((w.PLANSTARTDATE.HasValue && w.PLANFINISHDATE.HasValue) || w.PROJECTDOCUMENTs.Count() > 0) && w.PROJECTMANAGEMENT.PROJECTID == param).ToList().
                 Select(s => new keyDeliverableObject
                 {
                     id = s.OID,
                     projectManagementId = s.PROJECTMANAGEMENTID,
                     projectId = s.PROJECTMANAGEMENT.PROJECTID,
                     name = s.NAME,
                     nameText = System.Text.RegularExpressions.Regex.IsMatch(s.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(s.NAME)).KEYDELIVERABLES : s.NAME,
                     weightPercentage = s.WEIGHTPERCENTAGE,
                     planStartDate = s.PLANSTARTDATE,
                     planFinishDate = s.PLANFINISHDATE,
                     actualStartDate = s.ACTUALSTARTDATE,
                     actualFinishDate = s.ACTUALFINISHDATE,
                     seq = s.SEQ,
                     status = s.STATUS,
                     remark = s.REMARK,
                     revision = s.REVISION
                 }).OrderBy(x => x.planStartDate);
            return keyD;
        }

        [HttpGet]
        [ActionName("listKeyDByProjectId")]
        public IOrderedEnumerable<keyDeliverableObject> listKeyDByProjectId(int param)
        {
            var keyD =
                db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param).ToList().
                Select(s => new keyDeliverableObject
                {
                    id = s.OID,
                    projectManagementId = s.PROJECTMANAGEMENTID,
                    projectId = s.PROJECTMANAGEMENT.PROJECTID,
                    name = s.NAME,
                    nameText = System.Text.RegularExpressions.Regex.IsMatch(s.NAME, @"^\d+$")  ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse (s.NAME) ).KEYDELIVERABLES : s.NAME,
                    weightPercentage = s.WEIGHTPERCENTAGE,
                    planStartDate = s.PLANSTARTDATE,
                    planFinishDate = s.PLANFINISHDATE,
                    actualStartDate = s.ACTUALSTARTDATE,
                    actualFinishDate = s.ACTUALFINISHDATE,
                    seq = s.SEQ,
                    status = s.STATUS,
                    remark = s.REMARK,
                    revision = s.REVISION
                }).OrderBy(x => x.planStartDate);
            return keyD;
        }

        [HttpPost]
        [ActionName("AddKD")]
        public IHttpActionResult AddKD(int param, keyDeliverableObject keyDeliverableParam)
        {
            if (param > 0)
            {
                try
                {
                    KEYDELIVERABLE keyDeliverable = new KEYDELIVERABLE();
                    keyDeliverable.NAME = keyDeliverableParam.name;
                    keyDeliverable.WEIGHTPERCENTAGE = keyDeliverableParam.weightPercentage;
                    keyDeliverable.PLANSTARTDATE = keyDeliverableParam.planStartDate;
                    keyDeliverable.PLANFINISHDATE = keyDeliverableParam.planFinishDate;
                    keyDeliverable.REVISION = 0;
                    keyDeliverable.ACTUALSTARTDATE = keyDeliverableParam.actualStartDate;
                    keyDeliverable.ACTUALFINISHDATE = keyDeliverableParam.actualFinishDate;
                    keyDeliverable.STATUS = keyDeliverableParam.status;
                    keyDeliverable.REMARK = keyDeliverableParam.remark;
                    keyDeliverable.CREATEDBY = GetCurrentNTUserId();
                    keyDeliverable.CREATEDDATE = DateTime.Now;

                    List<keyDeliverablesProgressObject> listChilds = percentageMonthPogressByStartEnd_Date(keyDeliverable.PLANSTARTDATE.Value, keyDeliverable.PLANFINISHDATE.Value);

                    foreach (keyDeliverablesProgressObject child in listChilds)
                    {
                        KEYDELIVERABLESPROGRESS deliverablesProgress = new KEYDELIVERABLESPROGRESS();
                        deliverablesProgress.YEAR = child.year;
                        deliverablesProgress.MONTH = child.month;
                        deliverablesProgress.PLANPERCENTAGE = child.planPercentage;
                        deliverablesProgress.CREATEDBY = GetCurrentNTUserId();
                        deliverablesProgress.CREATEDDATE = DateTime.Now;

                        keyDeliverable.KEYDELIVERABLESPROGRESSes.Add(deliverablesProgress);
                    }

                    var cekProjManagement = db.PROJECTMANAGEMENTs.Where(w => w.PROJECTID == param);
                    if (keyDeliverableParam.projectManagementId.HasValue || cekProjManagement.Count() > 0)
                    {
                        keyDeliverable.PROJECTMANAGEMENTID = keyDeliverableParam.projectManagementId;
                        db.KEYDELIVERABLES.Add(keyDeliverable);
                        db.SaveChanges();
                        keyDeliverableParam = KDById(keyDeliverable.OID);
                    }
                    else
                    {
                        PROJECTMANAGEMENT projectManagement = new PROJECTMANAGEMENT();
                        projectManagement.PROJECTID = param;
                        projectManagement.KEYDELIVERABLES.Add(keyDeliverable);
                        projectManagement.CREATEDBY = GetCurrentNTUserId();
                        projectManagement.CREATEDDATE = DateTime.Now;
                        db.PROJECTMANAGEMENTs.Add(projectManagement);
                        db.SaveChanges();
                        keyDeliverableParam = KDById(projectManagement.KEYDELIVERABLES.FirstOrDefault().OID);
                    }
                    keyDeliverableParam.nameText = System.Text.RegularExpressions.Regex.IsMatch(keyDeliverable.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(keyDeliverable.NAME)).KEYDELIVERABLES : keyDeliverable.NAME;
                    return Ok(keyDeliverableParam);
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());

                    throw;
                }
            } else
            {
                //return BadRequest(ModelState);
                return BadRequest();
            }
        }


        [HttpGet]
        [ActionName("testPlanDate")]
        public object testPlanDate(string param)
        {
            var splitParam = param.Split('|');
            DateTime date1 = DateTime.Parse(splitParam[0]);
            DateTime date2 = DateTime.Parse(splitParam[1]);
            return percentageMonthPogressByStartEnd_Date(date1, date2);
        }

        [HttpPut]
        [ActionName("UpdateKD")]
        public IHttpActionResult UpdateKD(int param, keyDeliverableObject keyDeliverableParam)
        {
            try
            {
                KEYDELIVERABLE keyDeliverable = db.KEYDELIVERABLES.Find(param);
                keyDeliverable.NAME = keyDeliverableParam.name;
                if (keyDeliverable.WEIGHTPERCENTAGE != keyDeliverableParam.weightPercentage ||
                    keyDeliverable.PLANSTARTDATE != keyDeliverableParam.planStartDate ||
                    keyDeliverable.PLANFINISHDATE != keyDeliverableParam.planFinishDate)
                {
                    keyDeliverable.REVISION = keyDeliverable.REVISION + 1;
                }
                keyDeliverable.WEIGHTPERCENTAGE = keyDeliverableParam.weightPercentage;
                keyDeliverable.PLANSTARTDATE = keyDeliverableParam.planStartDate;
                keyDeliverable.PLANFINISHDATE = keyDeliverableParam.planFinishDate;
                keyDeliverable.ACTUALSTARTDATE = keyDeliverableParam.actualStartDate;
                keyDeliverable.ACTUALFINISHDATE = keyDeliverableParam.actualFinishDate;
                keyDeliverable.STATUS = keyDeliverableParam.status;
                keyDeliverable.REMARK = keyDeliverableParam.remark;
                keyDeliverable.UPDATEDBY = GetCurrentNTUserId();
                keyDeliverable.UPDATEDDATE = DateTime.Now;

                /*
                List<keyDeliverablesProgressObject> listChilds = percentageMonthPogressByStartEnd_Date(keyDeliverable.PLANSTARTDATE.Value, keyDeliverable.PLANFINISHDATE.Value);
                List<int> listId = new List<int>();
                foreach (keyDeliverablesProgressObject child in listChilds)
                {

                    KEYDELIVERABLESPROGRESS deliverablesProgress = keyDeliverable.KEYDELIVERABLESPROGRESSes.Where(w => w.YEAR == child.year && w.MONTH == child.month).FirstOrDefault();

                    if (deliverablesProgress == null)
                    {
                        deliverablesProgress = new KEYDELIVERABLESPROGRESS();
                        deliverablesProgress.CREATEDBY = GetCurrentNTUserId();
                        deliverablesProgress.CREATEDDATE = DateTime.Now;
                    }
                    else
                    {
                        listId.Add(deliverablesProgress.OID);
                        deliverablesProgress.UPDATEDBY = GetCurrentNTUserId();
                        deliverablesProgress.UPDATEDDATE = DateTime.Now;
                    }

                    deliverablesProgress.YEAR = child.year;
                    deliverablesProgress.MONTH = child.month;
                    //deliverablesProgress.PLANPERCENTAGE = child.planPercentage;

                    keyDeliverable.KEYDELIVERABLESPROGRESSes.Add(deliverablesProgress);
                }
                if (listId.Count() > 0)
                {
                    var listRemoveProgressId = keyDeliverable.KEYDELIVERABLESPROGRESSes.Where(w => !listId.Contains(w.OID) && w.OID != 0);
                    db.KEYDELIVERABLESPROGRESSes.RemoveRange(listRemoveProgressId);
                }

                */
                db.Entry(keyDeliverable).State = EntityState.Modified;
                db.SaveChanges();
                keyDeliverableParam = KDById(keyDeliverable.OID);

                keyDeliverableParam.nameText = System.Text.RegularExpressions.Regex.IsMatch(keyDeliverable.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(keyDeliverable.NAME)).KEYDELIVERABLES : keyDeliverable.NAME;
                return Ok(keyDeliverableParam);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
        }


        [HttpGet]
        [ActionName("projectManagementModuleChart")]
        public IEnumerable<object> projectManagementModuleChart(int param)
        {
            var keyDeliverablesProgresses = db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param).ToList().Select(s => new keyDeliverableObject
            {
                id = s.OID,
                name = System.Text.RegularExpressions.Regex.IsMatch(s.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(s.NAME)).KEYDELIVERABLES : s.NAME,
                planStartDate = s.PLANSTARTDATE.HasValue ? s.PLANSTARTDATE.Value : s.PLANFINISHDATE.HasValue ? s.PLANFINISHDATE.Value : (DateTime?)null,
                planFinishDate = s.PLANFINISHDATE.HasValue ? s.PLANFINISHDATE.Value : s.PLANSTARTDATE.HasValue ? DateTime.Now : (DateTime?)null,
                actualStartDate = s.ACTUALSTARTDATE.HasValue ? s.ACTUALSTARTDATE.Value : s.ACTUALFINISHDATE.HasValue ? s.ACTUALFINISHDATE.Value : (DateTime?)null,
                actualFinishDate = s.ACTUALFINISHDATE.HasValue ? s.ACTUALFINISHDATE.Value : s.ACTUALSTARTDATE.HasValue ? DateTime.Now : (DateTime?)null
            }).OrderBy(c=>c.planStartDate);

            var getPlanMin = keyDeliverablesProgresses.Max(m => m.planStartDate);
            var getPlanMax = keyDeliverablesProgresses.Max(m => m.planFinishDate);
            var getActualMin = keyDeliverablesProgresses.Max(m => m.actualStartDate);
            var getActualMax = keyDeliverablesProgresses.Max(m => m.actualFinishDate);

            return keyDeliverablesProgresses;
        }

        [HttpGet]
        [ActionName("projectManagementModuleChartMaxMinDate")]
        public IEnumerable<object> projectManagementModuleChartMaxMinDate(int param)
        {
            IEnumerable<int> listKeyDeliverablesId = db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param).Select(s => s.OID).ToList();
            var keyDeliverablesProgresses = db.KEYDELIVERABLES.Where(w => w.PROJECTMANAGEMENT.PROJECTID == param).Select(s => new keyDeliverableObject
            {
                id = s.OID,
                name = s.NAME,
                planStartDate = s.PLANSTARTDATE.HasValue ? s.PLANSTARTDATE.Value : s.PLANFINISHDATE.HasValue ? s.PLANFINISHDATE.Value : (DateTime?)null,
                planFinishDate = s.PLANFINISHDATE.HasValue ? s.PLANFINISHDATE.Value : s.PLANSTARTDATE.HasValue ? DateTime.Now : (DateTime?)null,
                actualStartDate = s.ACTUALSTARTDATE.HasValue ? s.ACTUALSTARTDATE.Value : s.ACTUALFINISHDATE.HasValue ? s.ACTUALFINISHDATE.Value : (DateTime?)null,
                actualFinishDate = s.ACTUALFINISHDATE.HasValue ? s.ACTUALFINISHDATE.Value : s.ACTUALSTARTDATE.HasValue ? DateTime.Now : (DateTime?)null
            });

            var getPlanMin = keyDeliverablesProgresses.Min(m => m.planStartDate);
            var getPlanMax = keyDeliverablesProgresses.Max(m => m.planFinishDate);
            var getActualMin = keyDeliverablesProgresses.Min(m => m.actualStartDate);
            var getActualMax = keyDeliverablesProgresses.Max(m => m.actualFinishDate);

            List<object> result = new List<object>();
            result.Add(new
            {
                //dikuringin 10 supaya graphnya tidak mepet ke y axis
                minDate = getPlanMin.HasValue && getActualMin.HasValue ? (getPlanMin.Value < getActualMin.Value ? getPlanMin.Value.AddDays(-10) : getActualMin.Value.AddDays(-10)) :
                          (getPlanMin.HasValue ? getPlanMin.Value.AddDays(-10) : (getActualMin.HasValue ? getActualMin.Value.AddDays(-10) : (DateTime?) null)),
                //ditambahkan 10 supaya graphnya tidak mepet ke akhir x axis
                maxDate = getPlanMax.HasValue && getActualMax.HasValue ? (getPlanMax.Value > getActualMax.Value ? getPlanMax.Value.AddDays(10) : getActualMax.Value.AddDays(10)) :
                          (getPlanMax.HasValue ? getPlanMax.Value.AddDays(10) : (getActualMax.HasValue ? getActualMax.Value.AddDays(10) : (DateTime?)null)),
            });
            return result;
        }
    }
}