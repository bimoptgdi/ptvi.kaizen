﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class AreaInvolvedsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/AreaInvolveds
        public IQueryable<areaInvolvedObject> GetAREAINVOLVEDs()
        {
            return db.AREAINVOLVEDs.Select(s => new areaInvolvedObject
            {
                id = s.OID,
                type = s.TYPE,
                areaName = s.AREANAME,
                areaCode = s.AREACODE,
                areaDesc = s.AREADESC
            });
        }

        // GET: api/AreaInvolveds/5
        [ResponseType(typeof(AREAINVOLVED))]
        public IHttpActionResult GetAREAINVOLVED(int id)
        {
            AREAINVOLVED aREAINVOLVED = db.AREAINVOLVEDs.Find(id);
            if (aREAINVOLVED == null)
            {
                return NotFound();
            }

            return Ok(aREAINVOLVED);
        }

        // PUT: api/AreaInvolveds/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAREAINVOLVED(int id, AREAINVOLVED aREAINVOLVED)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aREAINVOLVED.OID)
            {
                return BadRequest();
            }

            db.Entry(aREAINVOLVED).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AREAINVOLVEDExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AreaInvolveds
        [ResponseType(typeof(AREAINVOLVED))]
        public IHttpActionResult PostAREAINVOLVED(AREAINVOLVED aREAINVOLVED)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AREAINVOLVEDs.Add(aREAINVOLVED);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = aREAINVOLVED.OID }, aREAINVOLVED);
        }

        // DELETE: api/AreaInvolveds/5
        [ResponseType(typeof(AREAINVOLVED))]
        public IHttpActionResult DeleteAREAINVOLVED(int id)
        {
            AREAINVOLVED aREAINVOLVED = db.AREAINVOLVEDs.Find(id);
            if (aREAINVOLVED == null)
            {
                return NotFound();
            }

            db.AREAINVOLVEDs.Remove(aREAINVOLVED);
            db.SaveChanges();

            return Ok(aREAINVOLVED);
        }

        [HttpPut]
        [ActionName("updateAreaInvolveds")]
        public IHttpActionResult updateAreaInvolveds(int param, areaInvolvedObject areaInvolvedParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != areaInvolvedParam.id)
            {
                return BadRequest();
            }

            try
            {
                AREAINVOLVED update = db.AREAINVOLVEDs.Find(param);
                update.TYPE = areaInvolvedParam.type;
                update.AREACODE = areaInvolvedParam.areaCode;
                update.AREANAME = areaInvolvedParam.areaName;
                update.AREADESC = areaInvolvedParam.areaDesc;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(areaInvolvedParam);
        }

        [HttpPost]
        [ActionName("addAreaInvolveds")]
        public IHttpActionResult addAreaInvolveds(string param, areaInvolvedObject areaInvolvedParam)
        {
            AREAINVOLVED add = new AREAINVOLVED();
            add.TYPE = areaInvolvedParam.type;
            add.AREACODE = areaInvolvedParam.areaCode;
            add.AREANAME = areaInvolvedParam.areaName;
            add.AREADESC = areaInvolvedParam.areaDesc;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.AREAINVOLVEDs.Add(add);
            db.SaveChanges();

            areaInvolvedParam.id = add.OID;
            return Ok(areaInvolvedParam);
        }

        [HttpGet]
        [ActionName("areaInvolvedInActive")]
        public IHttpActionResult areaInvolvedInActive(string param)
        {
            var areaInvolveds = db.AREAINVOLVEDs.Where(w => w.TYPE == param).Select(s => new areaInvolvedObject
            {
                id = s.OID,
                areaName = s.AREANAME,
                areaCode = s.AREACODE,
                areaDesc = s.AREADESC
            });
            return Ok(areaInvolveds);
        }

        [HttpGet]
        [ActionName("areaInvolvedInActiveTS")]
        public IHttpActionResult areaInvolvedInActiveTS(int param)
        {
            var areaInvolveds = db.AREAINVOLVEDs.Where(x => x.TYPE == "TS").Select(s => new areaInvolvedObject
            {
                id = s.OID,
                areaName = s.AREANAME,
                areaCode = s.AREACODE,
                areaDesc = s.AREADESC
            });
            return Ok(areaInvolveds);
        }

        [HttpGet]
        [ActionName("areaInvolvedInActive")]
        public IHttpActionResult areaInvolvedInActive(string param, string area)
        {
            var areaInvolveds = db.AREAINVOLVEDs.Where(x => x.TYPE == param && x.AREACODE == area).Select(s => new areaInvolvedObject
            {
                id = s.OID,
                areaName = s.AREANAME,
                areaCode = s.AREACODE,
                areaDesc = s.AREADESC
            });
            return Ok(areaInvolveds);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AREAINVOLVEDExists(int id)
        {
            return db.AREAINVOLVEDs.Count(e => e.OID == id) > 0;
        }
    }
}