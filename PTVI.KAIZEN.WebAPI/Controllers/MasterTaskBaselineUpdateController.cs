﻿using Microsoft.Office.Project.Server.Library;
using Microsoft.ProjectServer.Client;
using Microsoft.SharePoint.Client;
using PTVI.iPROM.WebApi.Models;
using PTVI.iPROM.WebApi.Models.CustomObject;
using PTVI.iPROM.WebApi.PSI_CustomFieldWS;
using PTVI.iPROM.WebApi.PSI_ProjectWS;
using PTVI.iPROM.WebApi.PSI_QueueWS;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Services.Protocols;

namespace PTVI.KAIZEN.WebApi.Controllers
{
    public class MasterTaskBaselineUpdateController: iPROMController
    {       
        [HttpGet]
        [ActionName("DoAdd")]
        public IEnumerable<MASTERTASKBASELINEBYSHIFT> DoAdd(int param)
        {
            var projectShiftID = param;
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            CheckProjectState(projectUid);

            Guid sessionUid = Guid.NewGuid();
            try
            {
                ProjectWS.CheckOutProject(projectUid, sessionUid, "Calculate master task baseline");
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                var hasCheckedOut = false;
                var message = "";
                foreach (var err in errors)
                {
                    if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutInOtherSession)
                    {
                        message = "You have checkout this project in another session. Please checkin it and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutToOtherUser)
                    {
                        message = "Other user has checkout this project. Please wait it is checked in and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOAlreadyCheckedOutInSameSession ||
                        err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCannotCheckOutVisibilityModeProjectWithMppInDocLib)
                    {
                        message = "This project has been checkout. Please wait it is checked in and try again";
                        hasCheckedOut = true;
                    }
                }

                if (hasCheckedOut)
                    throw new ApplicationException(message);
                else
                    throw exc;
            }

            var projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var projectName = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow).PROJ_NAME;

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);

            try
            {
                // set to zero
                //var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var leave in excludeLeaves)
                {
                    var t = leave as ProjectDataSet.TaskRow;

                    // set start and end equal date to make it value 0
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                }

                //var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var row in includedLeaves)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        //filterStartDateRows[0].DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START; // use this to calculate baseline duration
                        filterStartDateRows[0].DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                        //newStartDateCFRow.DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START;
                        newStartDateCFRow.DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                    }


                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                        endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);
                    }
                }

                if (projectDs.TaskCustomFields.GetChanges() != null && projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                {
                    // publish project to appy calculation
                    Guid jobUid = Guid.NewGuid();
                    var changesDS = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                    ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changesDS, false);

                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate master task baseline");

                    WaitForQueue(q, jobUid);
                }
                else
                {

                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate master task baseline");

                    WaitForQueue(q, jobUid);
                }

                calculateProgressByDate(ProjectWS, projectUid, projectShiftID);


                return db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).AsEnumerable();
            }
            catch (Exception exc)
            {
                try
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }
                catch { };

                throw exc;
            }
        }
       
        private void calculateProgressByDate(PSI_ProjectWS.Project ProjectWS,  Guid projectId, int projectShiftID)
        {
            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            Guid cfTotalDurationUID = new Guid(cfTotalDuration);
            Guid durationCFUid = new Guid(cfCalcPlanDurationUid);

            // update percentCompleteBaseline for all task and its parents
            //var masterTasksBaseline = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();
            var projectDs =
                ProjectWS.ReadProject(projectId, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            foreach (var task in projectDs.Task.Rows)
            {
                var taskRow = task as ProjectDataSet.TaskRow;

                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        db.MASTERTASKBASELINEBYSHIFTs.Add(new MASTERTASKBASELINEBYSHIFT()
                        {
                            ESTIMATIONSHIFTID = projectShiftID,
                            TASKUID = taskRow.TASK_UID.ToString(),
                            PERCENTCOMPLETEBASELINE = percentComplete > 100 ? 100 : Convert.ToInt32(percentComplete)
                        });
                    }
                    catch { }
                }
            }

            db.SaveChanges();
        }

        static private void WaitForQueue(QueueSystem q, Guid jobId)
        {
            PSI_QueueWS.JobState jobState;
            const int QUEUE_WAIT_TIME = 2; // two seconds
            bool jobDone = false;
            string xmlError = string.Empty;
            int wait = 0;

            // Wait for the project to get through the queue.
            // Get the estimated wait time in seconds.
            wait = q.GetJobWaitTime(jobId);

            // Wait for it.
            System.Threading.Thread.Sleep(wait * 1000);
            // Wait until it is finished.

            do
            {
                // Get the job state.
                jobState = q.GetJobCompletionState(jobId, out xmlError);

                if (jobState == PSI_QueueWS.JobState.Success)
                {
                    jobDone = true;
                }
                else
                {
                    if (jobState == PSI_QueueWS.JobState.Unknown
                    || jobState == PSI_QueueWS.JobState.Failed
                    || jobState == PSI_QueueWS.JobState.FailedNotBlocking
                    || jobState == PSI_QueueWS.JobState.CorrelationBlocked
                    || jobState == PSI_QueueWS.JobState.Canceled)
                    {
                        // If the job failed, error out.
                        throw (new ApplicationException("Queue request " + jobState + " for Job ID " + jobId + ".\r\n" + xmlError));
                    }
                    else
                    {
                        Console.WriteLine("Job State: " + jobState + " for Job ID: " + jobId);
                        System.Threading.Thread.Sleep(QUEUE_WAIT_TIME * 1000);
                    }
                }
            }
            while (!jobDone);
        }
    }
}