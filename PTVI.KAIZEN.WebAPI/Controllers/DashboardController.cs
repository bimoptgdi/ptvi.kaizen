﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public partial class Eng
    {
        public string engineers { get; set; }
    }

    public partial class whbyEng
    {
        public string employeeid { get; set; }
        public string employeeName { get; set; }
        public double operating { get; set; }
        public double capital { get; set; }
        public double ts { get; set; }
        public double oh { get; set; }

    }

    public class DashboardController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accController = new AccountController();

        [HttpGet]
        [ActionName("getMinimunYearOnProject")]
        public object getMinimunYearOnProject(int param)
        {
            var getPlanMin = db.PROJECTs.Min(m => m.PLANSTARTDATE);
            var getActualMin = db.PROJECTs.Min(m => m.ACTUALSTARTDATE);

            //mengambil tahun minimum dari project antara plan start date dan actual start date
            int getMinYear = getPlanMin.HasValue && getActualMin.HasValue ?
                                getPlanMin.Value < getActualMin.Value ?
                                    getPlanMin.Value.Year :
                                    getActualMin.Value.Year :
                                getPlanMin.HasValue && !getActualMin.HasValue ?
                                    getPlanMin.Value.Year :
                                    !getPlanMin.HasValue && getActualMin.HasValue ?
                                        getActualMin.Value.Year : DateTime.Now.Year;

            //mengambil tahun Minimal dari current year dan minYear
            int minYear = Math.Min(getMinYear, DateTime.Now.Year);
            //mengambil tahun maxsimal dari current year dan minYear
            int maxYear = Math.Max(getMinYear, DateTime.Now.Year);
            List<object> result = new List<object>();
            for (int i = minYear; i <= maxYear; i++)
            {
                result.Add(new { year = i });
            }
            return result;
        }


        [HttpPost]
        [ActionName("reportBudgetPerformancebyEngineer")]
        public object reportBudgetPerformancebyEngineer(int param, Eng ss)
        {
            int yearParam = param;
            var employeeParam = ss.engineers!=null ? ss.engineers.Split(';').ToList() : new List<string>();
            var ll1 = db.PROJECTs.Where(w => w.PROJECTENGINEERID != null && (employeeParam.Count == 0 || employeeParam.Contains(w.PROJECTENGINEERID)) && w.ACTUALSTARTDATE.Value.Year <= yearParam && (w.ACTUALFINISHDATE.Value.Year >= yearParam || w.ACTUALFINISHDATE == null)).Select(s => new
            {
                projectId = s.OID,
                employeeId = s.PROJECTENGINEERID == null ? "-" : s.PROJECTENGINEERID,
                employeeName = s.PROJECTENGINEERNAME == null ? "-" : s.PROJECTENGINEERNAME,
                estimationCost = s.TOTALCOST == null ? 0 : s.TOTALCOST,
                actualCost = s.COSTREPORTELEMENTs.Count == 0 ? 0 : s.COSTREPORTELEMENTs.Sum(sum => sum.COSTREPORTACTUALCOSTs.Count == 0 ? 0 : sum.COSTREPORTACTUALCOSTs.Sum(sum1 => sum1.AMOUNT))
            });
            var l1 = ll1.Select(s => new
            {
                //projectId = s.Key.projectId,
                employeeId = s.employeeId,
                employeeName = s.employeeName,
                estimationCost = s.estimationCost,
                actualCost = s.actualCost
            });

            var l2 = db.USERASSIGNMENTs.Where(w => (employeeParam.Count == 0 || employeeParam.Contains(w.EMPLOYEEID)) && w.PROJECT.ACTUALSTARTDATE.Value.Year <= yearParam && (w.PROJECT.ACTUALFINISHDATE.Value.Year >= yearParam || w.PROJECT.ACTUALFINISHDATE == null)).Select(s => new
            {
                //projectId = s.PROJECT.OID,
                employeeId = s.EMPLOYEEID,
                employeeName = s.EMPLOYEENAME,
                estimationCost = s.PROJECT.TOTALCOST == null ? 0 : s.PROJECT.TOTALCOST,
                actualCost = s.PROJECT.COSTREPORTELEMENTs.Count == 0 ? 0 : s.PROJECT.COSTREPORTELEMENTs.Sum(sum => sum.COSTREPORTACTUALCOSTs.Count == 0 ? 0 : sum.COSTREPORTACTUALCOSTs.Sum(sum1 => sum1.AMOUNT))
            }).Select(s => new
            {
                //projectId = s.Key.projectId,
                employeeId = s.employeeId,
                employeeName = s.employeeName,
                estimationCost = s.estimationCost,
                actualCost = s.actualCost
            });
            var listResult = l1.Union(l2).GroupBy(c => new { /*c.projectId,*/ c.employeeId, c.employeeName }).Select( s => new
            {
                //projectId = s.Key.projectId,
                employeeId = s.Key.employeeId,
                employeeName = s.Key.employeeName,
                estimationCost = s.Sum(q=>  q.estimationCost),
                actualCost = s.Sum(q => q.actualCost)
            })
            ;
            return listResult;
        }

        [HttpGet]
        [ActionName("reportTechnicalSupportbyEngineer")]
        public object reportTechnicalSupportbyEngineer(string param)
        {
            var splitParam = param.Split('|');
            int yearParam = Int32.Parse(splitParam[0]);
            var employeeParam = splitParam[1].Trim() != "" ? splitParam[1].Split(';').ToList() : new List<string>();

            var result = db.USERASSIGNMENTs.Where(w => w.TECHNICALSUPPORT.REQUESTDATE.Value.Year <= yearParam 
                                                        && (w.TECHNICALSUPPORTUSERACTIONs.Where(c => c.COMPLETEDDATE.Value.Year >= yearParam || !c.COMPLETEDDATE.HasValue).Count() > 0)
                                                        && w.TECHNICALSUPPORTID.HasValue && w.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_ENGINEER
                                                        && (employeeParam.Count == 0 || employeeParam.Contains(w.EMPLOYEEID))).
                            Select(s => new
                            {
                                technicalSupportId = s.TECHNICALSUPPORTID,
                                employeeId = s.EMPLOYEEID,
                                employeeName = s.EMPLOYEENAME
                            }).Distinct().
                            GroupBy(g => new { g.employeeId, g.employeeName }).
                            Select(s => new
                            {
                                employeeId = s.Key.employeeId,
                                employeeName = s.Key.employeeName,
                                count = s.Count()
                            });
            return result;
        }

        [HttpGet]
        [ActionName("reportWorkingHoursbyEngineer")]
        public IEnumerable<whbyEng> reportWorkingHoursbyEngineer(string param)
        {
            var splitParam = param.Split('|');
            //string ntUserId = splitParam[0];
            int yearParam = Int32.Parse(splitParam[0]);
            var employeeParam = splitParam[1].Trim() != "" ? splitParam[1].Split(';').ToList() : new List<string>();

            var getTimeSheet = db.TIMESHEETs.Where(w => w.WEEKLY.YEAR.Value == yearParam).ToList().
                            Select(s => new
                            {
                                type = s.PROJECTDOCUMENTID.HasValue ?  s.PROJECT.PROJECTTYPE : 
                                    s.TECHNICALSUPPORTUSERACTIONID.HasValue ? "TS" : "OH",
                                employeeId = s.PROJECTDOCUMENTID.HasValue ? s.PROJECTDOCUMENT.DOCUMENTBYID : 
                                    s.TECHNICALSUPPORTUSERACTIONID.HasValue ? 
                                    s.TECHNICALSUPPORTUSERACTION.USERASSIGNMENT.EMPLOYEEID :
                                    accController.getEmployeeByBN(s.CREATEDBY).employeeId,
                                employeeName = s.PROJECTDOCUMENTID.HasValue ? s.PROJECTDOCUMENT.DOCUMENTBYNAME :
                                    s.TECHNICALSUPPORTUSERACTIONID.HasValue ? 
                                    s.TECHNICALSUPPORTUSERACTION.USERASSIGNMENT.EMPLOYEENAME :
                                    accController.getEmployeeByBN(s.CREATEDBY).full_Name,
                                hours = s.HOURS.HasValue ? s.HOURS.Value : 0
                            }).GroupBy(g => new { g.type, g.employeeId }).
                            Select(s => new
                            {
                                type = s.Key.type,
                                employeeid = s.Key.employeeId,
                                employeeName = s.Max(max => max.employeeName),
                                hours = s.Sum(sum => sum.hours)
                            });
            var result = getTimeSheet.Where(w => employeeParam.Contains(w.employeeid) ||employeeParam.Count == 0).GroupBy(g => new { g.employeeName, g.employeeid }).Select(s => new whbyEng
                            {
                                employeeid = s.Key.employeeid,
                                employeeName = s.Key.employeeName,
                                operating = getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "OPERATING") != null ? getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "OPERATING").hours : 0,
                                capital = getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "CAPITAL") != null ? getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "CAPITAL").hours : 0,
                                ts = getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "TS") != null ? getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "TS").hours : 0,
                                oh = getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "OH") != null ? getTimeSheet.FirstOrDefault(f => f.employeeid == s.Key.employeeid && f.type == "OH").hours : 0
                            });
            return result;
        }

        [HttpGet]
        [ActionName("listProjectDistribution")]
        public object listProjectDistribution(int param, String emp, DateTime? dateFrom, DateTime? dateTo)
        {
//            var splitParam = param.Split('|');
            DateTime? yearParamFrom = dateFrom;
            DateTime? yearParamTo = dateTo;
            var employeeParam = emp != null ? emp.Split(';').ToList() : new List<string>();
            List<roleDashboard> listRole = new List<roleDashboard>();
            listRole.Add(new roleDashboard { id = 1, roleCode = constants.PROJECT_PROJECTMANAGER });
            listRole.Add(new roleDashboard { id = 2, roleCode = constants.PROJECT_PROJECTENGINEER });
            listRole.Add(new roleDashboard { id = 3, roleCode = constants.PROJECT_ENGINEER });
            listRole.Add(new roleDashboard { id = 4, roleCode = constants.PROJECT_DESIGNER });

            var allList = db.PROJECTs.Where(w => yearParamFrom <= w.ACTUALSTARTDATE.Value && yearParamTo >= w.ACTUALSTARTDATE.Value && ((yearParamFrom <= w.ACTUALFINISHDATE.Value && yearParamTo >= w.ACTUALFINISHDATE.Value) || !w.ACTUALFINISHDATE.HasValue)).Select(s => new
            {
                projectId = s.OID,
                //role = "Project Manager",
                roleId = 1,
                employeeId = s.PROJECTMANAGERID,
                employeeName = s.PROJECTMANAGERNAME,
                targetDone = s.PLANFINISHDATE,
                done = s.ACTUALFINISHDATE
            }).Union(db.PROJECTs.Where(w => yearParamFrom <= w.ACTUALSTARTDATE.Value && yearParamTo >= w.ACTUALSTARTDATE.Value && ((yearParamFrom <= w.ACTUALFINISHDATE.Value && yearParamTo >= w.ACTUALFINISHDATE.Value) || !w.ACTUALFINISHDATE.HasValue)).Select(s => new
            {
                projectId = s.OID,
                //role = "Project Engineer",
                roleId = 2,
                employeeId = s.PROJECTMANAGERID,
                employeeName = s.PROJECTMANAGERNAME,
                targetDone = s.PLANFINISHDATE,
                done = s.ACTUALFINISHDATE
            })).Union(db.USERASSIGNMENTs.Where(w => w.JOINEDDATE.Value <= yearParamTo && w.JOINEDDATE.Value >= yearParamFrom && w.PROJECTID.HasValue && yearParamTo >= w.PROJECT.ACTUALSTARTDATE.Value && yearParamFrom <= w.PROJECT.ACTUALSTARTDATE.Value && ((yearParamFrom <= w.PROJECT.ACTUALFINISHDATE.Value && yearParamTo >= w.PROJECT.ACTUALFINISHDATE.Value) || !w.PROJECT.ACTUALFINISHDATE.HasValue)).Select(s => new
            {
                projectId = s.PROJECTID.Value,
                //role = s.ASSIGNMENTTYPE,
                roleId = s.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER ? 3 : 4,
                employeeId = s.EMPLOYEEID,
                employeeName = s.EMPLOYEENAME,
                targetDone = s.PROJECT.PLANFINISHDATE,
                done = s.PROJECT.ACTUALFINISHDATE
            }))
            .GroupBy(g => new { g.projectId, g.employeeId, g.employeeName, g.targetDone, g.done }).Select(s => new {
                projectId = s.Key.projectId,
                roleId = s.Min(m => m.roleId),
                employeeId = s.Key.employeeId,
                employeeName = s.Key.employeeName,
                targetDone = s.Key.targetDone,
                done = s.Key.done
            });

            if (employeeParam.Count() > 0) allList = allList.Where(w => employeeParam.Contains(w.employeeId));
            var a = allList.ToList();

            var employeeOnly = allList.ToList().GroupBy(g => new { g.employeeId, g.employeeName }).Select(s => new 
            {
                employeeId = s.Key.employeeId,
                employeeName = s.Key.employeeName,
                targetDone = s.Count(x => x.targetDone != null),
            }).ToList();

            //var targetDone = allList.ToList().GroupBy(g => new { g.employeeId, g.employeeName }).Select(s => new
            //{
            //    employeeId = s.Key.employeeId,
            //    employeeName = s.Key.employeeName,
            //    targetDone = s.Count(x => x.targetDone != null),
            //}).ToList();

            var employeeRoleCount = allList.ToList().GroupBy(g => new { g.roleId, g.employeeId, g.employeeName}).Select(s => new
            {
                roleId = s.Key.roleId,
                employeeId = s.Key.employeeId,
                employeeName = s.Key.employeeName,
                totalProject = s.Count(),
                targetDone = s.Count(x => x.targetDone != null),
                done = s.Count(x => x.done != null),
            });
            List<graphProjectDistribution> result = new List<graphProjectDistribution>();
            foreach (var employee in employeeOnly)
            {
                foreach (var role in listRole)
                {
                    graphProjectDistribution dataGraph =
                        employeeRoleCount.Where(w => w.employeeId == employee.employeeId && w.employeeName == employee.employeeName && w.roleId == role.id).Count() > 0 ?
                        employeeRoleCount.Where(w => w.employeeId == employee.employeeId && w.employeeName == employee.employeeName && w.roleId == role.id).
                        Select(s => new graphProjectDistribution
                        {
                            employeeId = employee.employeeId,
                            employeeName = employee.employeeName,
                            roleId = role.id,
                            roleName = role.roleName,
                            totalProject = s.totalProject,
                            targetDone = s.targetDone,
                            done = s.done,
                            PM = role.id == 1 ? s.totalProject : 0,
                            PE = role.id == 2 ? s.totalProject : 0,
                            En = role.id == 3 ? s.totalProject : 0,
                            De = role.id == 4 ? s.totalProject : 0,
                        }).FirstOrDefault() : null;
                    //new graphProjectDistribution
                    //{
                    //    employeeId = employee.employeeId,
                    //    employeeName = employee.employeeName,
                    //    roleId = role.id,
                    //    roleName = role.roleName,
                    //    totalProject = 0,
                    //    targetDone = null,
                    //    done = null
                    //};
                    if (dataGraph != null) result.Add(dataGraph);
                }
            }
            var res = result.GroupBy(g => new { g.employeeId, g.employeeName }).Select(s => new
            {
                employeeId = s.Key.employeeId,
                employeeName = s.Key.employeeName,
                totalProject = s.Sum(c => c.totalProject),
                targetDone = s.Sum(c => c.targetDone),
                done = s.Sum(c => c.done),
                PM = s.Sum(c => c.PM),
                PE = s.Sum(c => c.PE),
                En = s.Sum(c => c.En),
                De = s.Sum(c => c.De),
            });
            return res;
        }

        [HttpGet]
        [ActionName("listCapitalProject")]
        public object listCapitalProject(int param)
        {
            var listResult = db.PROJECTs.Where(w => param >= w.ACTUALSTARTDATE.Value.Year && (param <= w.ACTUALFINISHDATE.Value.Year || !w.ACTUALFINISHDATE.HasValue)).Select(s => new
            {
                area = s.AREAINVOLVED.AREADESC,
                actualDone = s.ACTUALFINISHDATE.HasValue ? 1 : 0,
                ongoing = s.ACTUALFINISHDATE.HasValue ? 0 : 1
            }).GroupBy(g => new { g.area }).Select(s => new
            {
                roleName = s.Key.area,
                totalProject = s.Count()
            });
            return listResult;
        }

        [HttpGet]
        [ActionName("currentProgress")]
        public progressCurrentPlanActual currentProgress(int param)
        {
            DateTime curDate = DateTime.Now;
            int dateNumber = int.Parse(curDate.ToString("yyyyMM"));
            var r = db.KEYDELIVERABLESPROGRESSes.Where(w => w.KEYDELIVERABLE.PROJECTMANAGEMENT.PROJECTID == param
            && w.PLANPERCENTAGE!= null && (w.YEAR.Value * 100) + w.MONTH.Value <= dateNumber).
                Select(s => new
                {
                    id = s.OID,
                    kdid = s.KEYDELIVERABLESID,
                    yearMonth = (s.YEAR.Value * 100) + s.MONTH.Value,
                });
                
                var rr = r.GroupBy(x => new { x.kdid}).Select(s => new
                {
                    kdid = s.Key.kdid,
                    ym = s.Max(d => d.yearMonth)
                }).Join(db.KEYDELIVERABLESPROGRESSes.Select(c => new {
                    id = c.OID,
                    kdid = c.KEYDELIVERABLESID,
                    ym = (c.YEAR.Value * 100) + c.MONTH.Value,
                    weightPercent = c.KEYDELIVERABLE.WEIGHTPERCENTAGE,
                    percentagePlan = c.PLANPERCENTAGE,
//                    percentageActual = c.ACTUALPERCENTAGE
                }),x => new {x.kdid, x.ym}, y => new {y.kdid, y.ym }, (x, y) => y );

            var resultPlan = rr.GroupBy(g => g.kdid).Select(s => new
            {
                id = s.Key.Value,
                percentagePlan = s.Sum(sum => sum.weightPercent * sum.percentagePlan) / 100,
  //              percentageActual = s.Sum(sum => sum.weightPercent * sum.percentageActual) / 100
            });

            var rA = db.KEYDELIVERABLESPROGRESSes.Where(w => w.KEYDELIVERABLE.PROJECTMANAGEMENT.PROJECTID == param
            && w.ACTUALPERCENTAGE != null && (w.YEAR.Value * 100) + w.MONTH.Value <= dateNumber).
                Select(s => new
                {
                    id = s.OID,
                    kdid = s.KEYDELIVERABLESID,
                    yearMonth = (s.YEAR.Value * 100) + s.MONTH.Value,
                });

            var rrA = rA.GroupBy(x => new { x.kdid }).Select(s => new
            {
                kdid = s.Key.kdid,
                ym = s.Max(d => d.yearMonth)
            }).Join(db.KEYDELIVERABLESPROGRESSes.Select(c => new {
                id = c.OID,
                kdid = c.KEYDELIVERABLESID,
                ym = (c.YEAR.Value * 100) + c.MONTH.Value,
                weightPercent = c.KEYDELIVERABLE.WEIGHTPERCENTAGE,
                //percentagePlan = c.PLANPERCENTAGE,
                percentageActual = c.ACTUALPERCENTAGE
            }), x => new { x.kdid, x.ym }, y => new { y.kdid, y.ym }, (x, y) => y);

            var resultActual = rrA.GroupBy(g => g.kdid).Select(s => new
            {
                id = s.Key.Value,
                //percentagePlan = s.Sum(sum => sum.weightPercent * sum.percentagePlan) / 100,
                percentageActual = s.Sum(sum => sum.weightPercent * sum.percentageActual) / 100
            });

            return new progressCurrentPlanActual { percentagePlan = resultPlan.Sum(s => s.percentagePlan), percentageActual = resultActual.Sum(s => s.percentageActual) };
        }


        [HttpGet]
        [ActionName("DasboardAll")]
        public object DasboardAll(int param)
        {
            var kD = new KeyDeliverablesController();
            var project = db.PROJECTs.Find(param);
            List<object> result = new List<object>();
            result.Add(new
            {
                projectNo = project.PROJECTNO,
                projectDesc = project.PROJECTDESCRIPTION,
                ownerId = project.OWNERID,
                ownerName = project.OWNERNAME,
                sponsorId = project.SPONSORID,
                sponsorName = project.SPONSORNAME,
                projectManagerId = project.PROJECTMANAGERID,
                projectManagerName = project.PROJECTMANAGERNAME,
                projectEngineerId = project.PROJECTENGINEERID,
                projectEngineerName = project.PROJECTENGINEERNAME,
                maintenanceRepId = project.MAINTENANCEREPID,
                maintenanceRepName = project.MAINTENANCEREPNAME,
                operationRepId = project.OPERATIONREPID,
                operationRepName = project.OPERATIONREPNAME,
                userAssignments = project.USERASSIGNMENTs.Where(ua => (ua.ISACTIVE == true) && (ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER || ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER || ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || ua.ASSIGNMENTTYPE == constants.PROJECT_OFFICER)).Select(u => new userAssignmentObject
                {
                    assignmentType = u.ASSIGNMENTTYPE,
                    employeeId = u.EMPLOYEEID,
                    employeeName = u.EMPLOYEENAME
                }),
                projectManagements = project.PROJECTMANAGEMENTs.Select(s => new projectManagementObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    manageKeyIssue = s.MANAGEKEYISSUE,
                    safetyPerfomance = s.SAFETYPERFOMANCE,
                    scope = s.SCOPE,
                    changeRequest = s.CHANGEREQUEST,
                    benefit = s.BENEFIT,
                    accomplishment = s.ACCOMPLISHMENT,
                    details = s.DETAILS,
                    ehsRecords = s.EHSRECORDS,
                    contractor = s.CONTRACTOR,
                    keyDeliverables = s.KEYDELIVERABLES.Select(k => new keyDeliverableObject()
                    {
                        id = k.OID,
                        projectManagementId = k.PROJECTMANAGEMENTID,
                        name = k.NAME,
                        nameText = System.Text.RegularExpressions.Regex.IsMatch(k.NAME, @"^\d+$") ? db.MASTERKEYDELIVERABLES.ToList().FirstOrDefault(x => x.OID == Int32.Parse(k.NAME)).KEYDELIVERABLES : k.NAME,
                        revision = k.REVISION,
                        weightPercentage = k.WEIGHTPERCENTAGE,
                        planStartDate = k.PLANSTARTDATE.HasValue ? k.PLANSTARTDATE.Value : k.PLANFINISHDATE.HasValue ? k.PLANFINISHDATE.Value : (DateTime?)null,
                        planFinishDate = k.PLANFINISHDATE.HasValue ? k.PLANFINISHDATE.Value : k.PLANSTARTDATE.HasValue ? DateTime.Now : (DateTime?)null,
                        actualStartDate = k.ACTUALSTARTDATE.HasValue ? k.ACTUALSTARTDATE.Value : k.ACTUALFINISHDATE.HasValue ? k.ACTUALFINISHDATE.Value : (DateTime?)null,
                        actualFinishDate = k.ACTUALFINISHDATE.HasValue ? k.ACTUALFINISHDATE.Value : k.ACTUALSTARTDATE.HasValue ? DateTime.Now : (DateTime?)null,
                        status = k.STATUS,
                        remark = k.REMARK
                    }).OrderBy(o => o.planStartDate)
                }),
                // (kondisi) ? (result 1) : (result2)
                //totalCost = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == project.PROJECTNO).Sum(s2 => s2.AMOUNT)),
                // totalCost = project.PROJECTTYPE == constants.OPERATING ? project.TOTALCOST :  project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == project.PROJECTNO).Sum(s2 => s2.AMOUNT)) + (project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Where(w => w.NETWORK != null).Where(w => s.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                totalCost = project.TOTALCOST,
                //totalCost = project.PROJECTTYPE == constants.OPERATING ? project.TOTALCOST : project.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Sum(s2 => s2.AMOUNT)),
                //actualCost = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + (project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Where(w => w.NETWORK != null).Where(w => s.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                //actualCost = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)) + project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTNETWORKs.Sum(s1 => s1.COSTREPORTELEMENT.COSTREPORTACTUALCOSTs.Where(w1 => w1.NETWORK.StartsWith(s1.NETWORK)).Sum(sum => sum.AMOUNT))),
                actualCost = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTACTUALCOSTs.Sum(s2 => s2.AMOUNT)),
                //commitment = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + (project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTCOMMITMENTs.Where(w => w.NETWORK != null).Where(w => s.WBSNO.StartsWith(w.NETWORK)).Sum(s2 => s2.AMOUNT))),
                //commitment = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)) + project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTNETWORKs.Sum(s1 => s1.COSTREPORTELEMENT.COSTREPORTCOMMITMENTs.Where(w1 => w1.NETWORK.StartsWith(s1.NETWORK)).Sum(sum => sum.AMOUNT))),
                commitment = project.COSTREPORTELEMENTs.Sum(s => s.COSTREPORTCOMMITMENTs.Sum(s2 => s2.AMOUNT)),
                forecast = (double)project.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),
                otherDepartmentTasks = project.OTHERDEPARTMENTTASKs.Where(c=>c.STATUS!=constants.STATUSIPROM_COMPLETED).Select(s => new otherDepartmentTaskObject
                {
                    id = s.OID,
                    projectId = s.PROJECTID,
                    departmentId = s.DEPARTMENTID,
                    departmentName = s.DEPARTMENTNAME,
                    taskName = s.TASKNAME,
                    picId = s.PICID,
                    picName = s.PICNAME,
                    picEmail = s.PICEMAIL,
                    planDate = s.PLANDATE,
                    actualDate = s.ACTUALDATE,
                    status = s.STATUS,
                }),
                projectManagementModuleChartMaxMinDate = kD.projectManagementModuleChartMaxMinDate(project.OID),
                progressCurrentPlanVSActual = currentProgress(param)
            });

            return result;
        }
    }

    public class progressCurrentPlanActual
    {
        public double? percentagePlan { get; set; }
        public double? percentageActual { get; set; }
    }


    public class roleDashboard
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        public int id { get; set; }
        public string roleCode { get; set; }
        public string roleName
        {
            get
            {
                return db.LOOKUPs.Where(w => w.TYPE == constants.ASSIGNMENTTYPE && w.VALUE == roleCode).FirstOrDefault().TEXT;
            }
        }
    }

    public class graphProjectDistribution
    {
        public string employeeId { get; set; }
        public string employeeName { get; set; }
        public int roleId { get; set; }
        public string roleName { get; set; }
        public int? totalProject { get; set; }
        public int? targetDone { get; set; }
        public int? done { get; set; }
        public int? PM { get; set; }
        public int? PE { get; set; }
        public int? En { get; set; }
        public int? De { get; set; }
    }

    public class typeWorkingHours
    {
        public string typeCode { get; set; }
        public string typeName { get; set; }
    }
}
