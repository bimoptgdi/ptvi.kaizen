﻿using SourceCode.Workflow.Management;
using SourceCode.Workflow.Management.Criteria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class K2ManagementController : KAIZENController
    {
        private WorkflowManagementServer _k2Mgmt;

        public WorkflowManagementServer K2Mgmt
        {
            get
            {
                return _k2Mgmt;
            }

            set
            {
                _k2Mgmt = value;
            }
        }

        public void StopProcessInstance(int id)
        {
            try
            {
                string _serverName = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ServerName;
                string _user = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserName;
                string _domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserDomain;
                string _password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserPassword;
                uint _port = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2MgmtPort;
                string _securityLabel = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2SecurityLabel;

                SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
                connectionString.Authenticate = true;
                connectionString.Host = _serverName;
                connectionString.Integrated = true;
                connectionString.IsPrimaryLogin = true;
                connectionString.EncryptedPassword = false;
                connectionString.Port = _port;

                //if (_debugMode)
                //{
                //    connectionString.UserID = _user;
                //    connectionString.WindowsDomain = _domain;
                //    connectionString.Password = _password;
                //}
                connectionString.SecurityLabelName = _securityLabel;

                K2Mgmt = new WorkflowManagementServer();
                K2Mgmt.Open(connectionString.ToString());

                K2Mgmt.StopProcessInstances(id);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
            }

        }

        public void DeleteProcessInstance(int id)
        {
            try
            {
                string _serverName = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ServerName;
                string _user = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserName;
                string _domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserDomain;
                string _password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserPassword;
                uint _port = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2MgmtPort;
                string _securityLabel = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2SecurityLabel;

                SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
                connectionString.Authenticate = true;
                connectionString.Host = _serverName;
                connectionString.Integrated = true;
                connectionString.IsPrimaryLogin = true;
                connectionString.EncryptedPassword = false;
                connectionString.Port = _port;

                connectionString.UserID = _user;
                connectionString.WindowsDomain = _domain;
                connectionString.Password = _password;

                connectionString.SecurityLabelName = _securityLabel;

                K2Mgmt = new WorkflowManagementServer();
                K2Mgmt.Open(connectionString.ToString());

                K2Mgmt.DeleteProcessInstances(id, true);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
            }

        }

        public void Connect()
        {
            string _serverName = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ServerName;
            string _user = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserName;
            string _domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserDomain;
            string _password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserPassword;
            uint _port = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2MgmtPort;
            string _securityLabel = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2SecurityLabel;

            SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
            connectionString.Authenticate = true;
            connectionString.Host = _serverName;
            connectionString.Integrated = true;
            connectionString.IsPrimaryLogin = true;
            connectionString.EncryptedPassword = false;
            connectionString.Port = _port;

            connectionString.UserID = _user;
            connectionString.WindowsDomain = _domain;
            connectionString.Password = _password;

            connectionString.SecurityLabelName = _securityLabel;

            K2Mgmt = new WorkflowManagementServer();
            K2Mgmt.Open(connectionString.ToString());
        }

        public List<K2WorklistItem> FilterWorkListItems(WorklistFields field, Comparison comparison, object value, string wfID)
        {
            WorklistCriteriaFilter filter = new WorklistCriteriaFilter();
            filter.AddRegularFilter(field, comparison, value);

            var wlItems = K2Mgmt.GetWorklistItems(filter);

            List<K2WorklistItem> workListItems = new List<K2WorklistItem>();
            for (var i = 0; i < wlItems.Count; i++)
            {
                workListItems.Add(new K2WorklistItem()
                {
                    folio = wlItems[i].Folio,
                    procId = wlItems[i].ProcInstID,
                    currentActivityName = wlItems[i].ActivityName,
                    nextApproverName = wlItems[i].Destination,
                    nextApproverID = wlItems[i].ActInstDestID
                });
            }

            if (!string.IsNullOrEmpty(wfID))
                return workListItems.Where(x => x.procId == int.Parse(wfID)).ToList();
            else
                return workListItems;
        }

        public List<K2WorklistItem> FilterWorkListItems(WorklistCriteriaFilter filter)
        {
            var wlItems = K2Mgmt.GetWorklistItems(filter);

            List<K2WorklistItem> workListItems = new List<K2WorklistItem>();
            for (var i = 0; i < wlItems.Count; i++)
            {
                workListItems.Add(new K2WorklistItem()
                {
                    folio = wlItems[i].Folio,
                    procId = wlItems[i].ProcInstID,
                    currentActivityName = wlItems[i].ActivityName,
                    nextApproverName = wlItems[i].Destination,
                    nextApproverID = wlItems[i].ActInstDestID
                });
            }

            return workListItems;
        }

        [HttpGet]
        [ActionName("testGetWorklist")]
        public WorklistItems testGetWorklist(string param)
        {
            WorklistCriteriaFilter filter = new WorklistCriteriaFilter();
            filter.AddRegularFilter(WorklistFields.Folio, Comparison.Like, "%" + param + "%");

            Connect();

            var wlItems = K2Mgmt.GetWorklistItems(filter);

            return wlItems;
        }

        public void Close()
        {
            K2Mgmt.Connection.Close();
        }
    }
}

public class K2WorklistItem
{
    public string folio { get; set; }
    public int procId { get; set; }
    public string currentActivityName { get; set; }
    public string nextApproverName { get; set; }
    public int nextApproverID { get; set; }

    public string nextApproverNtAccount
    {
        get
        {

            if (string.IsNullOrEmpty(nextApproverName))
                return "";


            return nextApproverName.Substring(nextApproverName.LastIndexOf("\\") + 1);
        }
    }

    public string serialNo
    {
        get
        {
            return String.Format("{0}_{1}", procId, nextApproverID);
        }
    }
}
