﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class DoaLoginsController : ApiController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/DoaLogins
        public IQueryable<doaLoginObject> GetDOALOGINs()
        {
            return db.DOALOGINs.Select(s => new doaLoginObject
            {
                userNameOrigin = s.USERNAMEORIGIN,
                userNameDelegated = s.USERNAMEDELEGATED,
                loginDate = s.LOGINDATE
            });
        }

        // GET: api/DoaLogins/5
        [ResponseType(typeof(doaLoginObject))]
        public IHttpActionResult GetDOALOGIN(string id)
        {
            DOALOGIN dOALOGIN = db.DOALOGINs.Find(id);
            if (dOALOGIN == null)
            {
                return NotFound();
            }

            return Ok(new doaLoginObject
            {
                userNameOrigin = dOALOGIN.USERNAMEORIGIN,
                userNameDelegated = dOALOGIN.USERNAMEDELEGATED,
                loginDate = dOALOGIN.LOGINDATE
            });
        }

        // PUT: api/DoaLogins/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDOALOGIN(string id, doaLoginObject doaLoginParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != doaLoginParam.userNameOrigin)
            {
                return BadRequest();
            }
            DOALOGIN dOALOGIN = db.DOALOGINs.Find(id);
            dOALOGIN.USERNAMEDELEGATED = doaLoginParam.userNameDelegated;
            dOALOGIN.LOGINDATE = doaLoginParam.loginDate;
            db.Entry(dOALOGIN).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DOALOGINExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DoaLogins
        [ResponseType(typeof(DOALOGIN))]
        public IHttpActionResult PostDOALOGIN(doaLoginObject doaLoginParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DOALOGIN dOALOGIN = new DOALOGIN();
            dOALOGIN.USERNAMEORIGIN = doaLoginParam.userNameOrigin;
            dOALOGIN.USERNAMEDELEGATED = doaLoginParam.userNameDelegated;
            dOALOGIN.LOGINDATE = doaLoginParam.loginDate;

            db.DOALOGINs.Add(dOALOGIN);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DOALOGINExists(dOALOGIN.USERNAMEORIGIN))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dOALOGIN.USERNAMEORIGIN }, dOALOGIN);
        }

        // DELETE: api/DoaLogins/5
        [ResponseType(typeof(DOALOGIN))]
        public IHttpActionResult DeleteDOALOGIN(string id)
        {
            DOALOGIN dOALOGIN = db.DOALOGINs.Find(id);
            if (dOALOGIN == null)
            {
                return NotFound();
            }

            db.DOALOGINs.Remove(dOALOGIN);
            db.SaveChanges();

            return Ok(dOALOGIN);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DOALOGINExists(string id)
        {
            return db.DOALOGINs.Count(e => e.USERNAMEORIGIN == id) > 0;
        }
    }
}