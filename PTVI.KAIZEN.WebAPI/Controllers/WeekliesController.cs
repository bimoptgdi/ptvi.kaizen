﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class WeekliesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/Weeklies
        public IQueryable<WEEKLY> GetWEEKLies()
        {
            return db.WEEKLies;
        }

        // GET: api/Weeklies/5
        [ResponseType(typeof(WEEKLY))]
        public IHttpActionResult GetWEEKLY(int id)
        {
            WEEKLY wEEKLY = db.WEEKLies.Find(id);
            if (wEEKLY == null)
            {
                return NotFound();
            }

            return Ok(wEEKLY);
        }

        // PUT: api/Weeklies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutWEEKLY(int id, WEEKLY wEEKLY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != wEEKLY.OID)
            {
                return BadRequest();
            }

            db.Entry(wEEKLY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WEEKLYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Weeklies
        [ResponseType(typeof(WEEKLY))]
        public IHttpActionResult PostWEEKLY(WEEKLY wEEKLY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.WEEKLies.Add(wEEKLY);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = wEEKLY.OID }, wEEKLY);
        }

        // DELETE: api/Weeklies/5
        [ResponseType(typeof(WEEKLY))]
        public IHttpActionResult DeleteWEEKLY(int id)
        {
            WEEKLY wEEKLY = db.WEEKLies.Find(id);
            if (wEEKLY == null)
            {
                return NotFound();
            }

            db.WEEKLies.Remove(wEEKLY);
            db.SaveChanges();

            return Ok(wEEKLY);
        }

        [HttpGet]
        [ActionName("createWeekly")]
        public string[] createWeekly(int param)
        {
            var checkWeekly = db.WEEKLies.Where(x => x.YEAR == param).ToList();
            if (checkWeekly.Count() > 0) return null;

            List<string> result = new List<string>();

            DateTime startOfDate = new DateTime(param, 1, 1);
            // Get jan 1st of the year

            int endOfYear = (int)startOfDate.AddYears(1).Year;
            bool loop = true;
            int addStart = 0;
            int week = 0;

            var qStartDay = db.GENERALPARAMETERs.First(w => w.TITLE == constants.STARTWEEKLY);

            int startDay = getDayOfWeek(qStartDay.VALUE);

            while (loop)
            {
                if (week++ > 0)
                    startOfDate = startOfDate.AddDays(7);
                if ((int)startOfDate.DayOfWeek > startDay)
                    addStart = 7 - (int)startOfDate.DayOfWeek;
                else
                    addStart = 0 - (int)startOfDate.DayOfWeek;

                startOfDate = startOfDate.AddDays(addStart + startDay);

                DateTime startDate = startOfDate;
                DateTime endDate = startOfDate.AddDays(6);

                if (startOfDate.Year == endOfYear) break;

                WEEKLY newWeekly = new WEEKLY();
                newWeekly.YEAR = startOfDate.Year;
                newWeekly.MONTH = startOfDate.Month;
                newWeekly.WEEK = week;
                newWeekly.STARTDATE = startDate;
                newWeekly.ENDDATE = endDate;
                newWeekly.CREATEDBY = GetCurrentNTUserId();
                newWeekly.CREATEDDATE = DateTime.Now;
                db.WEEKLies.Add(newWeekly);

                result.Add("Week = " + week.ToString() + " Year = " + startOfDate.Year.ToString() + " Month = " + startOfDate.Month.ToString() + " "+ startOfDate.DayOfWeek + " = " + startOfDate.ToString("MMMM dd, yyyy") + " => " + startDate.ToString("MMMM dd, yyyy") + " to " + endDate.ToString("MMMM dd, yyyy"));

                if (week > 1000 || (int)startOfDate.Year == endOfYear)
                    loop = false;
            }

            db.SaveChanges();
            return result.ToArray();

        }


        [HttpGet]
        [ActionName("weeklyList")]
        public object weeklyList(int param)
        {

            var defWeek = db.WEEKLies.ToList().Where(x=>x.STARTDATE<=(DateTime.Now.Date.AddDays(-7))  && x.ENDDATE >= (DateTime.Now.Date.AddDays(-7)))
                .FirstOrDefault(); 

            var wL = db.WEEKLies.ToList().Select(x => new 
            {
                id = x.OID, enddate = x.ENDDATE, month = x.MONTH, startdate = x.STARTDATE, week = x.WEEK, year = x.YEAR,
                title = x.STARTDATE.Value.ToString("dd MMM yyyy") + " - "+x.ENDDATE.Value.ToString("dd MMM yyyy")
            });
            return new {defWeek= defWeek.OID, Wl= wL};
        }

        private int getDayOfWeek(string day)
        {
            if (day != null)
            {
                String[] days = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                for (int i = 0; i < days.Count(); i++)
                    if (days[i].Equals(day, StringComparison.OrdinalIgnoreCase))
                        return i;
            }
            return -1;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WEEKLYExists(int id)
        {
            return db.WEEKLies.Count(e => e.OID == id) > 0;
        }
    }
}