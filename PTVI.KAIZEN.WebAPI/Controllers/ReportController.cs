﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.Globalization;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ReportController : KAIZENController
    {
        List<string> listTypeGroup = new List<string>(new string[] { "", "PLAN EWP", "ACTUAL EWP", "PLAN DC", "ACTUAL DC", "TOTAL PLAN", "TOTAL ACTUAL" });

        [HttpGet]
        [ActionName("listYearDocument")]
        public object listYearDocument(string param)
        {
            var minYearPlan = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.PLANSTARTDATE.HasValue).Min(m => m.PLANSTARTDATE.Value.Year);
            var minYearActual = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.ACTUALSTARTDATE.HasValue).Min(m => m.ACTUALSTARTDATE.Value.Year);
            var maxYearPlan = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.PLANFINISHDATE.HasValue).Min(m => m.PLANFINISHDATE.Value.Year);
            var maxYearActual = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.ACTUALFINISHDATE.HasValue).Min(m => m.ACTUALFINISHDATE.Value.Year);

            var minYear = minYearPlan > minYearActual ? minYearActual : minYearPlan;
            var maxYear = maxYearPlan > maxYearActual ? maxYearActual : maxYearPlan;

            maxYear = (maxYear > DateTime.Now.Year ? maxYear : DateTime.Now.Year) + 1;

            minYear = maxYear < minYear ? maxYear : minYear;
            maxYear = maxYear < minYear ? minYear : maxYear;

            return Enumerable.Range(minYear, maxYear - minYear + 1).Select(i => new { value = i, text = i.ToString() }).OrderByDescending(o => o.value);
        }

        [HttpGet]
        [ActionName("POOverdueReport")]
        public object POOverdueReport(string projectNo, string projectDesc, string pmParam, bool? overDue)
        {
            if (!string.IsNullOrEmpty(projectNo) || !string.IsNullOrEmpty(projectDesc) || !string.IsNullOrEmpty(pmParam))
            {
                var uAObj = db.MATERIALCOMPONENTs.
                    Where(w => w.PROJECTID.HasValue && w.PONO != null &&
                               ((!string.IsNullOrEmpty(projectNo) ? w.PROJECTNO.Contains(projectNo) : 1 == 1) &&
                               (!string.IsNullOrEmpty(projectDesc) ? w.PROJECT.PROJECTDESCRIPTION.Contains(projectDesc) : 1 == 1) &&
                               (!string.IsNullOrEmpty(pmParam) ? w.PROJECT.PROJECTMANAGERID == pmParam : 1 == 1))).
                    ToList().Select(s => new
                    {
                        id = s.OID,
                        projectId = s.PROJECTID,
                        projectNo = s.PROJECTNO,
                        projectDesc = s.PROJECT.PROJECTDESCRIPTION,
                        projectManager = s.PROJECT.PROJECTMANAGERNAME,
                        projectDocumentId = s.PROJECTDOCUMENTID,
                        ewpNo = string.IsNullOrEmpty(s.EWPNO) ? "-" : s.PROJECTNO + "-" + s.EWPNO,
                        mrNo = s.MRNO,
                        poNo = s.PONO,
                        poDate = s.MATERIALPLANs.Min(s1 => s1.POPLANDATE) < db.MATERIALBYENGINEERs.Where(x => x.MRNO == s.MRNO && x.PROJECTID == s.PROJECTID).Min(s1 => s1.POPLANDATE) ? s.MATERIALPLANs.Min(s1 => s1.POPLANDATE) : db.MATERIALBYENGINEERs.Where(x => x.MRNO == s.MRNO && x.PROJECTID == s.PROJECTID).Min(s1 => s1.POPLANDATE),//dibuyer hanya mengambail row 1 plan date
                        poRaisedDate = s.MATERIALPURCHASEs.Min(s1 => s1.PORAISEDDATE),//dibuyer hanya mengambail row 1 actual date
                        poItemNo = s.POITEMNO,
                        orderedQuantity = s.MATERIALSCHEDULEs.Sum(sum => sum.ORDEREDQUANTITY),//dibuyer hanya mengambail row 1
                        receivedQuantity = s.MATERIALSCHEDULEs.Sum(sum => sum.RECEIVEDQUANTITY),//dibuyer hanya mengambail row 1
                        etaSiteDate = s.MATERIALSTATUS.Min(min => min.ETASITEDATE),
                        actualDeliveryDate = s.MATERIALSTATUS.Min(min => min.ACTUALDELIVERYDATE), // dibuyer tidak ada
                        buyers = string.Join(", ", s.MATERIALPURCHASEGROUPs.GroupBy(g => new { g.PRGROUPCODE, g.PRGROUPDESC }).
                            Select(x => x.Key.PRGROUPCODE + " - " + x.Key.PRGROUPDESC))
                    }).Where(w => overDue == true ? (!w.poRaisedDate.HasValue && w.poDate < DateTime.Now) : 1 == 1).ToList();
                return uAObj;
            }
            return new List<string>();
        }

        //    [HttpGet]
        //    [ActionName("listTaskOverdueReport")]
        //    public object listTaskOverdueReport(string projectNo, string description, string projectEngineer, bool? overDue)
        //    {
        //        if (!string.IsNullOrEmpty(projectNo) || !string.IsNullOrEmpty(description) || !string.IsNullOrEmpty(projectEngineer))
        //        {
        //            var uAObj = db.PROJECTDOCUMENTs.
        //                Where(w => w.PROJECTID.HasValue && w.PONO != null &&
        //                           ((!string.IsNullOrEmpty(projectNo) ? w.PROJECTNO.Contains(projectNo) : 1 == 1) &&
        //                           (!string.IsNullOrEmpty(description) ? w.DESCRIPTION.Contains(description) : 1 == 1) &&
        //                           (!string.IsNullOrEmpty(projectEngineer) ? w.PROJECTENGINEER == projectEngineer : 1 == 1))).
        //                ToList().Select(s => new
        //                {
        //                    id = s.OID,
        //                    projectId = s.PROJECTID,
        //                    projectNo = s.PROJECTNO,
        //                    description = s.DESCRIPTION,
        //                    projectDocumentId = s.PROJECTDOCUMENTID,
        //                    docType = s.DOCTYPE,
        //                    keyDeliverablesId = s.KEYDELIVERABLESID,
        //                    docNo = s.DOCNO,
        //                    docTitle = s.DOCTITLE,
        //                    planStartDate = s.PLANSTARTDATE,
        //                    planFinishDate = s.PLANFINISHDATE,
        //                    actualStartDate = s.ACTUALSTARTDATE,
        //                    actualFinishDate = s.ACTUALFINISHDATE,
        //                    discipline = s.DISCIPLINE,
        //                    status = s.STATUS,
        //                    remark = s.REMARK,
        //                    executor = s.EXECUTOR,
        //                    executorAssignment = s.EXECUTORASSIGNMENT,
        //                    executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
        //                    progress = s.PROGRESS,
        //                    documentById = s.DOCUMENTBYID,
        //                    documentByName = s.DOCUMENTBYNAME,
        //                    documentByEmail = s.DOCUMENTBYEMAIL,
        //                    mrNo = s.MRNO,
        //                    poNo = s.PONO,
        //                    poDate = s.MATERIALPLANs.Min(s1 => s1.POPLANDATE) < db.MATERIALBYENGINEERs.Where(x => x.MRNO == s.MRNO && x.PROJECTID == s.PROJECTID).Min(s1 => s1.POPLANDATE) ? s.MATERIALPLANs.Min(s1 => s1.POPLANDATE) : db.MATERIALBYENGINEERs.Where(x => x.MRNO == s.MRNO && x.PROJECTID == s.PROJECTID).Min(s1 => s1.POPLANDATE),//dibuyer hanya mengambail row 1 plan date
        //                    poRaisedDate = s.MATERIALPURCHASEs.Min(s1 => s1.PORAISEDDATE),//dibuyer hanya mengambail row 1 actual date
        //                    poItemNo = s.POITEMNO,
        //                    orderedQuantity = s.MATERIALSCHEDULEs.Sum(sum => sum.ORDEREDQUANTITY),//dibuyer hanya mengambail row 1
        //                    receivedQuantity = s.MATERIALSCHEDULEs.Sum(sum => sum.RECEIVEDQUANTITY),//dibuyer hanya mengambail row 1
        //                    etaSiteDate = s.MATERIALSTATUS.Min(min => min.ETASITEDATE),
        //                    actualDeliveryDate = s.MATERIALSTATUS.Min(min => min.ACTUALDELIVERYDATE), // dibuyer tidak ada
        //                    buyers = string.Join(", ", s.MATERIALPURCHASEGROUPs.GroupBy(g => new { g.PRGROUPCODE, g.PRGROUPDESC }).
        //                        Select(x => x.Key.PRGROUPCODE + " - " + x.Key.PRGROUPDESC))
        //                }).Where(w => overDue == true ? (!w.poRaisedDate.HasValue && w.poDate < DateTime.Now) : 1 == 1);
        //            return uAObj;
        //        }
        //        return new List<string>();
        //    }
        //}


        public List<object> IsReadonlyProjectToOnList(int param, string badgeNo, int accessrights)
        {
            var usc = new ProjectController();
            var isReadOnly = usc.IsReadonlyProjectTo(param, badgeNo, accessrights);
            List<object> result = new List<object>();


            result.Add(new { result = isReadOnly });

            return result;
        }

        [HttpGet]
        [ActionName("getRoleInProject")]
        public List<ROLE> getRoleInProject(int param, string badgeNo)
        {
            PROJECT project = db.PROJECTs.Find(param);

            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);

            List<ROLE> result = new List<ROLE>();
            if (project != null)
            {
                if (project.PROJECTMANAGERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectManager));
                if (project.OWNERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectOwner));
                if (project.SPONSORID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectSponsor));
                if (project.PROJECTENGINEERID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectEngineer));
                if (project.MAINTENANCEREPID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaintenanceReport));
                if (project.OPERATIONREPID == badgeNo) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleOperationReport));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleEngineer));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleDesigner));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectController));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaterialCoordinator));
                if (project.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_OFFICER && ua.EMPLOYEEID == badgeNo && ua.ISACTIVE == true).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleMaterialCoordinator));
                //if (project.USERASSIGNMENTs.Where(ua => otherPos.Select(s => s.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == badgeNo).Count() > 0) result.Add(db.ROLEs.FirstOrDefault(f => f.ROLENAME == constants.RoleProjectManager));
                if (project.USERASSIGNMENTs.Where(ua => otherPos.Select(s => s.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == badgeNo).Count() > 0)
                {
                    var listAssignment = project.USERASSIGNMENTs.Where(ua => otherPos.Select(s => s.VALUE).Contains(ua.ASSIGNMENTTYPE) && ua.EMPLOYEEID == badgeNo).Select(SUA => SUA.ASSIGNMENTTYPE);
                    result.Add(db.ROLEs.FirstOrDefault(f => listAssignment.Contains(f.ROLENAME)));
                }
            }
            return result;
        }

        [HttpGet]
        [ActionName("listTaskOverdueReport")]
        public IEnumerable<object> listTaskOverdueReport(string projectNo, string description, string enParam, DateTime from, DateTime to)
        {
            var usc = new ReportController();
            DateTime yearParamFrom = from;
            DateTime yearParamTo = to;
            int cekRoleTypeAllProject = db.USERROLEs.Count(w => w.ROLE.ROLETYPE == "0");
            var otherPos = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject);

            var project = db.PROJECTs.
                    Where(wALAH => (wALAH.PROJECTMANAGEMENTs.Where(weleh => weleh.KEYDELIVERABLES.Where(w =>
                        (
                        (!w.PLANSTARTDATE.HasValue || !w.PLANFINISHDATE.HasValue || (
                        (w.PLANSTARTDATE >= from && w.PLANSTARTDATE <= to) ||
                        (w.PLANFINISHDATE >= from && w.PLANFINISHDATE <= to) ||
                        (w.PLANSTARTDATE <= from && w.PLANFINISHDATE >= from) ||
                        (w.PLANSTARTDATE <= to && w.PLANFINISHDATE >= to)))
                        )).Count() > 0).Count() > 0 ||
                        wALAH.PROJECTDOCUMENTs.Where(w => //w.PROJECTID.HasValue && //w.PONO != null &&
                               (
                                (!w.PLANSTARTDATE.HasValue || !w.PLANFINISHDATE.HasValue || (
                                (w.PLANSTARTDATE >= from && w.PLANSTARTDATE <= to) ||
                                (w.PLANFINISHDATE >= from && w.PLANFINISHDATE <= to) ||
                                (w.PLANSTARTDATE <= from && w.PLANFINISHDATE >= from) ||
                                (w.PLANSTARTDATE <= to && w.PLANFINISHDATE >= to)))
                                )).Count() > 0) &&
                        ((!string.IsNullOrEmpty(projectNo) ? wALAH.PROJECTNO.Contains(projectNo) : 1 == 1) &&
                        (!string.IsNullOrEmpty(description) ? wALAH.PROJECTDESCRIPTION.Contains(description) : 1 == 1) &&
                        (!string.IsNullOrEmpty(enParam) ? wALAH.PROJECTENGINEERNAME == enParam : 1 == 1)));

            var rr = project.Select(r => new
            {
                id = r.OID,
                projectId = r.IPTPROJECTID,
                projectNo = r.PROJECTNO,
                description = r.PROJECTDESCRIPTION,
                ownerId = r.OWNERID,
                sponsorId = r.SPONSORID,
                projectManagerId = r.PROJECTMANAGERID,
                projectEngineerId = r.PROJECTENGINEERID,
                projectEngineerName = r.PROJECTENGINEERNAME,
                maintenanceRepId = r.MAINTENANCEREPID,
                operationRepId = r.OPERATIONREPID,
                userAssignment = r.USERASSIGNMENTs.Select(ua => new
                {
                    employeeId = ua.EMPLOYEEID,
                    employeeName = ua.EMPLOYEENAME,
                }),
                projectDocument = r.PROJECTDOCUMENTs.Where(pdA =>
                    pdA.STATUS != constants.STATUSIPROM_HOLD && pdA.STATUS != constants.STATUSIPROM_CANCELLED
                    && pdA.ACTUALFINISHDATE == null
                  ).Select(pd => new
                  {
                      id = pd.OID,
                      pId = r.OID,
                      pNo = r.PROJECTNO,
                      tipe = pd.DOCTYPE,
                      tipeName = pd.DOCTYPE == "EWP" ? "EWP Compliance" : pd.DOCTYPE == "DWG" ? "Drawing" : "Design Conformity",
                      desc = pd.DOCNO,
                      name = pd.DOCTITLE,
                      pfd = pd.PLANFINISHDATE,
                      psd = pd.PLANSTARTDATE,
                      afd = pd.ACTUALFINISHDATE,
                      asd = pd.ACTUALSTARTDATE,
                      pic = pd.DOCUMENTBYNAME,
                      picId = pd.DOCUMENTBYID,
                      status = pd.STATUS,
                      statusName = pd.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom &&
                       s.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (pd.PLANSTARTDATE != null && pd.ACTUALSTARTDATE != null) ?
                       db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                      updatedBy = pd.UPDATEDBY,
                      updatedByName = pd.UPDATEDBY != null ? db.USERs.Where(u => u.NTUSERID == pd.UPDATEDBY).FirstOrDefault().USERNAME : "",
                      updatedDate = pd.UPDATEDDATE,
                  }).OrderBy(o => o.psd).ThenBy(o => o.pfd),
                pE = r.PROJECTMANAGEMENTs.Where(k => k.KEYDELIVERABLES.Count() > 0).Select(pm => new
                {
                    pId = pm.PROJECTID,
                    cb = pm.CREATEDBY,
                    ub = pm.UPDATEDBY,
                    kd = pm.KEYDELIVERABLES.Where(pmId =>
                            pmId.STATUS != constants.STATUSIPROM_HOLD && pmId.STATUS != constants.STATUSIPROM_CANCELLED
                            && pmId.ACTUALFINISHDATE == null
                          ).Select(kd => new
                          {
                              id = kd.OID,
                              pId = r.OID,
                              pNo = r.PROJECTNO,
                              tipe = "Key Deliverable",
                              tipeName = "Key Deliverable",
                              desc = kd.NAME,
                              name = "",
                              pfd = kd.PLANFINISHDATE,
                              psd = kd.PLANSTARTDATE,
                              afd = kd.ACTUALFINISHDATE,
                              asd = kd.ACTUALSTARTDATE,
                              pic = r.PROJECTMANAGERNAME,
                              picId = r.PROJECTMANAGERID,
                              status = kd.STATUS,
                              statusName = kd.ACTUALSTARTDATE == null ? db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom &&
                    s.VALUE == constants.STATUSIPROM_NOTSTARTED).FirstOrDefault().TEXT : (kd.ACTUALSTARTDATE != null) ?
                                db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE == constants.STATUSIPROM_INPROGRESS).FirstOrDefault().TEXT : "",
                              updatedBy = kd.UPDATEDBY,
                              updatedByName = kd.UPDATEDBY != null ? db.USERs.Where(u => u.NTUSERID == kd.UPDATEDBY).FirstOrDefault().USERNAME : "",
                              updatedDate = kd.UPDATEDDATE,
                          }).OrderBy(o => o.psd).ThenBy(o => o.pfd)
                }),
                cOut = r.PROJECTCLOSEOUTs.Where(pCO => pCO.CLOSEOUTPROGRESSes.Count > 0).Select(pCO => new
                {
                    cOPg = pCO.CLOSEOUTPROGRESSes.Where(cOP => cOP.SUBMITEDDATE == null).Select(cOP => new
                    {
                        id = cOP.OID,
                        pId = r.OID,
                        pNo = r.PROJECTNO,
                        tipe = "Close Out Task",
                        tipeName = "Close Out Task",
                        desc = cOP.SETUPCLOSEOUTROLE.DESCRIPTION,
                        name = "",
                        pfd = cOP.PLANDATE,
                        psd = (DateTime?)null,
                        afd = cOP.SUBMITEDDATE,
                        asd = pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.SUBMITEDDATE == null && cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1 && cOP.CLOSEOUTROLEID > 1).FirstOrDefault().SUBMITEDDATE,
                        pic = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                                (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                   r.PROJECTMANAGERNAME :
                                    db.ROLEs.Where(s => s.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && s.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.USERNAME,
                        picId = cOP.SETUPCLOSEOUTROLE.ISROLE == false && (cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER) ? "#ua#" :
                                (cOP.SETUPCLOSEOUTROLE.ISROLE == false) ?
                                    r.PROJECTMANAGERID :
                                    db.ROLEs.Where(s => s.ROLENAME == cOP.SETUPCLOSEOUTROLE.ROLENAME && s.USERROLEs.Count > 0).FirstOrDefault().USERROLEs.FirstOrDefault().USER.BADGENO,
                        picUa = r.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                            && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                    ).Select(s => s.EMPLOYEENAME).ToList(),
                        picUaId = r.USERASSIGNMENTs.Where(u => u.ISACTIVE == true && (u.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || u.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                            && u.ASSIGNMENTTYPE == cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE && cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE != null
                                    ).Select(s => s.EMPLOYEEID).ToList(),
                        status = pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                                constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS,
                        statusName = db.LOOKUPs.Where(s => s.TYPE == constants.statusIprom && s.VALUE ==
                            (pCO.CLOSEOUTPROGRESSes.Where(cOp => cOp.CLOSEOUTROLEID == cOP.CLOSEOUTROLEID - 1).FirstOrDefault().SUBMITEDDATE == null ?
                                constants.STATUSIPROM_NOTSTARTED : constants.STATUSIPROM_INPROGRESS)).FirstOrDefault().TEXT,
                        coRole = cOP.SETUPCLOSEOUTROLE.ISROLE.Value ? cOP.SETUPCLOSEOUTROLE.ROLENAME : cOP.SETUPCLOSEOUTROLE.ASSIGNMENTTYPE,
                        updatedBy = cOP.SUBMITEDBY,
                        updatedByName = cOP.SUBMITEDBY != null ? db.USERs.Where(u => u.NTUSERID == cOP.SUBMITEDBY).FirstOrDefault().USERNAME : "",
                        updatedDate = cOP.SUBMITEDDATE,
                    }).OrderBy(o => o.id)
                })
            });

            var kD = new List<object>();
            var ewp = new List<object>();
            var dwg = new List<object>();
            var coTask = new List<object>();
            foreach (var r in rr)
            {
                foreach (var pE in r.pE)
                {
                    foreach (var k in pE.kd)
                    {
                        bool editable = false;
                        var o = new
                        {
                            afd = k.afd,
                            asd = k.asd,
                            id = k.id,
                            desc = k.desc,
                            pfd = k.pfd,
                            pic = k.pic,
                            picId = k.picId,
                            pNo = k.pNo,
                            pnoName = k.pNo + (k.name != "" ? " - " + k.name : ""),
                            psd = k.psd,
                            status = k.status,
                            statusName = k.statusName,
                            tipe = k.tipe,
                            tipeName = k.tipeName,
                            editable = editable,
                            coRole = "",
                            updatedBy = k.updatedBy,
                            updatedByName = k.updatedByName,
                            updatedDate = k.updatedDate,
                        };
                        kD.Add(o);
                    }
                }

                foreach (var ep in r.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_ENGINEER)))
                {
                    bool editable = false;
                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedByName = ep.updatedByName,
                        updatedDate = ep.updatedDate,
                    };
                    ewp.Add(o);
                }

                foreach (var ep in r.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)))
                {
                    bool editable = false;

                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedByName = ep.updatedByName,
                        updatedDate = ep.updatedDate,
                    };
                    ewp.Add(o);
                }

                foreach (var ep in r.projectDocument.Where(pd => (pd.tipe == constants.DOCUMENTTYPE_DESIGN)))
                {
                    bool editable = false;

                    var o = new
                    {
                        afd = ep.afd,
                        asd = ep.asd,
                        id = ep.id,
                        name = ep.name,
                        desc = ep.desc,
                        pfd = ep.pfd,
                        pic = ep.pic,
                        picId = ep.picId,
                        pNo = ep.pNo,
                        pnoName = ep.pNo + (ep.name != "" ? " - " + ep.name : ""),
                        psd = ep.psd,
                        status = ep.status,
                        statusName = ep.statusName,
                        tipe = ep.tipe,
                        tipeName = ep.tipeName,
                        editable = editable,
                        coRole = "",
                        updatedBy = ep.updatedBy,
                        updatedByName = ep.updatedByName,
                        updatedDate = ep.updatedDate,
                    };
                    dwg.Add(o);
                }

                //var ec = new EmailController();
                //var paramname = ec.FindNameAndEmailByBadgeNo(param).Split('|')[0];
                foreach (var co in r.cOut)
                {
                    foreach (var copg in co.cOPg.Where(c => c.asd != null))
                    {
                        if (copg.pic == "#ua#")
                        {
                            var pic = String.Join(",", copg.picUa);
                            var picId = String.Join(",", copg.picUaId);
                            var o = new
                            {
                                afd = copg.afd,
                                asd = copg.asd,
                                id = copg.id,
                                name = copg.name,
                                desc = copg.desc,
                                pfd = copg.pfd,
                                pic = pic,
                                picId = picId,
                                pNo = copg.pNo,
                                pnoName = copg.pNo + (copg.name != "" ? " - " + copg.name : ""),
                                psd = copg.psd,
                                status = copg.status,
                                statusName = copg.statusName,
                                tipe = copg.tipe,
                                tipeName = copg.tipeName,
                                coRole = copg.coRole,
                                updatedBy = copg.updatedBy,
                                updatedByName = copg.updatedByName,
                                updatedDate = copg.updatedDate,
                            };
                            //if ((copg.pic != null && o.pic.Contains(paramname)) || r.projectManagerId == param)
                            coTask.Add(o);
                        }
                        else
                        {
                            //if ((copg.pic != null && copg.pic.Contains(paramname)) || r.projectManagerId == param)
                            coTask.Add(copg);
                        }

                    }
                }
            }

            var t = new List<object>();
            t.Add(new { myTask = kD.Union(ewp).Union(dwg).Union(coTask) });
            var m = kD.Union(ewp).Union(dwg).Union(coTask);
            return m;
        }

        [HttpGet]
        [ActionName("EwpPerformanceReport")]
        public object EwpPerformanceReport(int paramYear) //param is year
        {
            int daysInYear = 365;//new DateTime(paramYear, 12, 31).DayOfYear;
            DateTime currentDate = DateTime.Now;

            var getDocumentDataEngineer = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == paramYear : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == paramYear : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER)).Select(s => new ewpPerformanceReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.PROJECT.PROJECTDESCRIPTION,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,

                //•	Weight Hours = Summary dari form Drawing EWP Detail Item Hours
                deliverableWeightHour = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS),

                //•	Base Line = Total base line dari masteryearlyengineeringhours untuk masing-masing engineer 
                baseLineHours = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID) != null ? db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID).BASELINEHOURS : 0,

                //•	Weight Hours(%) = Weight Hours / Baseline Hours
                deliverableWeightPercent = null,

                //Factored Weight
                //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                //•	[Deliverable Weight %] / [Resources Factor]
                resourcesFactor = db.MASTERYEARLYENGINEERINGHOURS.Count(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID) == 0 ? 0 : db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID).ENGINEERDESIGNERFACTOR,
                factoredWeight = null,

                //Final Result
                //•	Hanya dihitung jika Actual Finish Date sudah terisi
                //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                //finalResult = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0,
                //Performance
                //•	If[Final Result](x) < 0.8 then the performance value = 0 
                //•	If[Final Result](x) more than 0.8 and less than 1.0,
                //                then the performance value = 250 x – 150
                //•	If[Final Result](x) more than 1 and less than 1.05,
                //                then the performance value = 500 x – 400
                //•	If[Final Result](x) > 1.05 then the performance value = 125
                performance = null,

                //•	Achievement = [Factor Weight] * [Performance Value]
                achievement = null,
                remark = s.REMARK
            });

            var getDocumentDataDesigner = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == paramYear : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == paramYear : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && getDocumentDataEngineer.Select(se => se.ewpNo).Contains(w.DOCNO))).Select(s => new ewpPerformanceReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.PROJECT.PROJECTDESCRIPTION,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,

                //•	Weight Hours = Summary dari form Drawing EWP Detail Item Hours
                deliverableWeightHour = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS),

                //•	Base Line = Total base line dari masteryearlyengineeringhours untuk masing-masing engineer 
                baseLineHours = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID) != null ? db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID).BASELINEHOURS : 0,

                //•	Weight Hours(%) = Weight Hours / Baseline Hours
                deliverableWeightPercent = null,

                //Factored Weight
                //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                //•	[Deliverable Weight %] / [Resources Factor]
                resourcesFactor = db.MASTERYEARLYENGINEERINGHOURS.Count(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID) == 0 ? 0 : db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == s.DOCUMENTBYID).ENGINEERDESIGNERFACTOR,
                factoredWeight = null,

                //Final Result
                //•	Hanya dihitung jika Actual Finish Date sudah terisi
                //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                //finalResult = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0,
                //Performance
                //•	If[Final Result](x) < 0.8 then the performance value = 0 
                //•	If[Final Result](x) more than 0.8 and less than 1.0,
                //                then the performance value = 250 x – 150
                //•	If[Final Result](x) more than 1 and less than 1.05,
                //                then the performance value = 500 x – 400
                //•	If[Final Result](x) > 1.05 then the performance value = 125
                performance = null,

                //•	Achievement = [Factor Weight] * [Performance Value]
                achievement = null,
                remark = s.REMARK
            });

            List<ewpPerformanceReport> result = new List<ewpPerformanceReport>();
            List<int> listProjectId = new List<int>();
            foreach (ewpPerformanceReport s in getDocumentDataEngineer.Union(getDocumentDataDesigner))
            {
                if (!listProjectId.Contains(s.projectId))
                    listProjectId.Add(s.projectId);


                //•	Weight Hours(%) = Weight Hours / Baseline Hours
                s.deliverableWeightPercent = s.baseLineHours > 0 ? (s.deliverableWeightHour / s.baseLineHours) * 100 : 0;

                //Factored Weight
                //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                //•	[Deliverable Weight %] / [Resources Factor]
                s.factoredWeight = s.resourcesFactor != 0 ? s.deliverableWeightPercent / s.resourcesFactor : 0;

                //Final Result
                //•	Hanya dihitung jika Actual Finish Date sudah terisi
                //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                s.finalResult = s.actualFinishDate.HasValue && s.planFinishDate.HasValue && s.planStartDate.HasValue ? (s.planFinishDate.Value - s.planStartDate.Value).TotalDays / (s.actualFinishDate.Value - s.planStartDate.Value).TotalDays : 0;

                //Performance
                //•	If[Final Result](x) < 0.8 then the performance value = 0 
                //•	If[Final Result](x) more than 0.8 and less than 1.0,
                //                then the performance value = 250 x – 150
                //•	If[Final Result](x) more than 1 and less than 1.05,
                //                then the performance value = 500 x – 400
                //•	If[Final Result](x) > 1.05 then the performance value = 125
                double finalResult2Decimal = Math.Floor(s.finalResult.Value * 100) / 100;
                string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                    db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", s.finalResult.ToString()) :
                                                    "";
                s.performanceFormula = performanceFormula;
                s.performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));

                //•	Achievement = [Factor Weight] * [Performance Value]
                s.achievement = s.level == 0 ? (double?)null : (s.factoredWeight * s.performance) / 100;

                result.Add(s);
            }

            result.AddRange(db.PROJECTs.Where(w => listProjectId.Contains(w.OID)).Select(s => new ewpPerformanceReport
            {
                projectId = s.OID,
                projectNo = s.PROJECTNO,
                ewpNo = s.PROJECTNO,
                dwgNo = s.PROJECTNO,
                deliverableNo = s.PROJECTNO,
                deliverableName = s.PROJECTDESCRIPTION,
                level = 0,
                projectType = s.PROJECTTYPE
            }));

            return result.OrderBy(o => o.projectNo).ThenBy(t => t.level == 0 ? null : t.ewpNo).ThenBy(t => t.dwgNo);
        }

        [HttpGet]
        [ActionName("IndividualReportEngineer")]
        public object IndividualReportEngineer(int paramYear, string badgeNo)
        {
            if (!string.IsNullOrEmpty(badgeNo))
            {
                var months = Enumerable.Range(1, 12).Select(i => new { monthInt = i, monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
                var columnParam = db.GENERALPARAMETERs.FirstOrDefault(w => w.TITLE == constants.INDIVIDUAL_REPORT_ENGINEER);

                var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear && w.EMPLOYEEID == badgeNo);
                double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                #region EWP Plan
                var planEWP = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.PLANFINISHDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).GroupBy(g => g.PLANFINISHDATE.Value.Month).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))
                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                            SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countPlanEWP = p == null ? 0 : p.count,
                                                deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0
                                            });


                double planScoreCummulativeEWP = 0;
                List<individualReportEngineer> resultPlanEWP = new List<individualReportEngineer>();
                foreach (var s in planEWP)
                {
                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreEWP = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter
                    double valueColumnParam = 0;
                    if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    {
                        s.planColumnParameterEWP = (s.planScoreEWP / 100) * valueColumnParam;
                    }
                    else
                    {
                        s.planColumnParameterEWP = 0;
                    }

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeEWP += s.planColumnParameterEWP;
                    s.planScoreCummEWP = planScoreCummulativeEWP;

                    resultPlanEWP.Add(s);
                }
                #endregion EWP Plan

                #region EWP Actual
                var getDataDocumentsActualEWP = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.ACTUALFINISHDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER);

                List<individualReportEngineer> calculationDocumentsActualEWP = new List<individualReportEngineer>();
                foreach (var s in getDataDocumentsActualEWP)
                {
                    individualReportEngineer dataActualEWP = new individualReportEngineer();
                    dataActualEWP.monthInt = s.ACTUALFINISHDATE.Value.Month;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultEWP = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultEWP * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultEWP.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualEWP.actualAchievementEWP = emplyeeFactorWeight * performance;

                    calculationDocumentsActualEWP.Add(dataActualEWP);
                };

                var actualEWP = months.GroupJoin(calculationDocumentsActualEWP.GroupBy(g => g.monthInt.Value).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    actualAchievement = s.Sum(sum => sum.actualAchievementEWP)
                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countActualEWP = p == null ? 0 : p.count,
                                                actualAchievementEWP = p == null ? 0 : p.actualAchievement
                                            });

                var xx = actualEWP.ToList();

                double actualScoreCummulativeEWP = 0;
                List<individualReportEngineer> resultActualEWP = new List<individualReportEngineer>();
                foreach (var s in actualEWP)
                {
                    actualScoreCummulativeEWP += s.actualAchievementEWP;

                    s.actualScoreCummEWP = actualScoreCummulativeEWP;

                    resultActualEWP.Add(s);
                }
                #endregion EWP Actual


                #region Conformity Plan
                var planConformity = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.PLANFINISHDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).GroupBy(g => g.PLANFINISHDATE.Value.Month).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))

                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                            SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countPlanConformity = p == null ? 0 : p.count,
                                                deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0
                                            });


                double planScoreCummulativeConformity = 0;
                List<individualReportEngineer> resultPlanConformity = new List<individualReportEngineer>();
                foreach (var s in planConformity)
                {
                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreConformity = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter
                    double valueColumnParam = 0;
                    if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    {
                        s.planColumnParameterConformity = (s.planScoreConformity / 100) * valueColumnParam;
                    }
                    else
                    {
                        s.planColumnParameterConformity = 0;
                    }

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeConformity += s.planColumnParameterConformity;
                    s.planScoreCummConformity = planScoreCummulativeConformity;

                    resultPlanConformity.Add(s);
                }
                #endregion Conformity Plan

                #region Conformity Actual
                var getDataDocumentsActualConformity = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.ACTUALFINISHDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY);

                List<individualReportEngineer> resultDocumentsActualConformity = new List<individualReportEngineer>();
                foreach (var s in getDataDocumentsActualConformity)
                {
                    individualReportEngineer dataActualConformity = new individualReportEngineer();
                    dataActualConformity.monthInt = s.ACTUALFINISHDATE.Value.Month;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultConformity = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultConformity * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultConformity.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualConformity.actualAchievementConformity = emplyeeFactorWeight * performance;

                    resultDocumentsActualConformity.Add(dataActualConformity);
                };

                var actualConformity = months.GroupJoin(resultDocumentsActualConformity.GroupBy(g => g.monthInt.Value).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    actualAchievement = s.Sum(sum => sum.actualAchievementConformity)
                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countActualConformity = p == null ? 0 : p.count,
                                                actualAchievementConformity = p == null ? 0 : p.actualAchievement
                                            });

                double actualScoreCummulativeConformity = 0;
                List<individualReportEngineer> resultActualConformity = new List<individualReportEngineer>();
                foreach (var s in actualConformity)
                {
                    actualScoreCummulativeConformity += s.actualAchievementConformity;

                    s.actualScoreCummConformity = actualScoreCummulativeConformity;

                    resultActualConformity.Add(s);
                }
                #endregion Conformity Actual

                #region calculation
                var calculateDatas = resultPlanEWP.
                    Join(resultActualEWP, planEwp => planEwp.monthInt, actualEwp => actualEwp.monthInt, (planEwp, actualEwp) => new { planEwp = planEwp, actualEwp = actualEwp }).
                    Join(resultPlanConformity, planJoinEwp => planJoinEwp.planEwp.monthInt, planConf => planConf.monthInt, (planJoinEwp, planConf) => new { planJoinEwp = planJoinEwp, planConf = planConf }).
                    Join(resultActualConformity, pJoinAll => pJoinAll.planConf.monthInt, actualConf => actualConf.monthInt, (pJoinAll, actualConf) => new { planEWP = pJoinAll.planJoinEwp.planEwp, actualEWP = pJoinAll.planJoinEwp.actualEwp, planConf = pJoinAll.planConf, actualConf = actualConf }).
                    Select(s => new individualReportEngineer
                    {
                        monthInt = s.planEWP.monthInt,
                        monthName = s.planEWP.monthName + " " + paramYear,
                        countPlanEWP = s.planEWP.countPlanEWP,
                        planScoreEWP = s.planEWP.planScoreEWP,
                        planColumnParameterEWP = s.planEWP.planColumnParameterEWP,
                        planScoreCummEWP = s.planEWP.planScoreCummEWP,
                        countActualEWP = s.actualEWP.countActualEWP,
                        actualAchievementEWP = s.actualEWP.actualAchievementEWP,
                        actualScoreCummEWP = s.actualEWP.actualScoreCummEWP,
                        countPlanConformity = s.planConf.countPlanConformity,
                        planScoreConformity = s.planConf.planScoreConformity,
                        planColumnParameterConformity = s.planConf.planColumnParameterConformity,
                        planScoreCummConformity = s.planConf.planScoreCummConformity,
                        countActualConformity = s.actualConf.countActualConformity,
                        actualAchievementConformity = s.actualConf.actualAchievementConformity,
                        actualScoreCummConformity = s.actualConf.actualScoreCummConformity
                    });
                #endregion calculation

                return calculateDatas;
            }
            else {
                return new List<individualReportEngineer>();
            }
        }

        [HttpGet]
        [ActionName("IndividualReportDesigner")]
        public object IndividualReportDesigner(int paramYearDesigner, string badgeNo)
        {
            if (!string.IsNullOrEmpty(badgeNo))
            {
                var months = Enumerable.Range(1, 12).Select(i => new { monthInt = i, monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
                var columnParam = db.GENERALPARAMETERs.FirstOrDefault(w => w.TITLE == constants.INDIVIDUAL_REPORT_ENGINEER);

                var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYearDesigner && w.EMPLOYEEID == badgeNo);
                double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                //get filter EWP or Conformity
                List<int> listEWPId = new List<int>();
                List<int> listConformityId = new List<int>();
                foreach (var projDoc in db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && w.DOCUMENTBYID == badgeNo && (w.PLANFINISHDATE.Value.Year == paramYearDesigner || w.ACTUALFINISHDATE.Value.Year == paramYearDesigner)))
                {
                    var documentGroup = GetDocumentGroupDesignerbyProjectId(projDoc.OID);
                    if (documentGroup == constants.DOCUMENTTYPE_ENGINEER)
                    {
                        listEWPId.Add(projDoc.OID);
                    }
                    else if (documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                    {
                        listConformityId.Add(projDoc.OID);
                    }
                }

                #region EWP Plan
                var planEWP = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.PLANFINISHDATE.Value.Year == paramYearDesigner) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listEWPId.Contains(w.OID)).GroupBy(g => g.PLANFINISHDATE.Value.Month).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))

                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                            SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countPlanEWP = p == null ? 0 : p.count,
                                                deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0
                                            });


                double planScoreCummulativeEWP = 0;
                List<individualReportEngineer> resultPlanEWP = new List<individualReportEngineer>();
                foreach (var s in planEWP)
                {
                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreEWP = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter
                    double valueColumnParam = 0;
                    if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    {
                        s.planColumnParameterEWP = (s.planScoreEWP / 100) * valueColumnParam;
                    }
                    else
                    {
                        s.planColumnParameterEWP = 0;
                    }

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeEWP += s.planColumnParameterEWP;
                    s.planScoreCummEWP = planScoreCummulativeEWP;

                    resultPlanEWP.Add(s);
                }
                #endregion EWP Plan

                #region EWP Actual
                var getDataDocumentsActualEWP = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.ACTUALFINISHDATE.Value.Year == paramYearDesigner) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listEWPId.Contains(w.OID));

                List<individualReportEngineer> calculationDocumentsActualEWP = new List<individualReportEngineer>();
                foreach (var s in getDataDocumentsActualEWP)
                {
                    individualReportEngineer dataActualEWP = new individualReportEngineer();
                    dataActualEWP.monthInt = s.ACTUALFINISHDATE.Value.Month;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultEWP = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultEWP * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultEWP.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualEWP.actualAchievementEWP = emplyeeFactorWeight * performance;

                    calculationDocumentsActualEWP.Add(dataActualEWP);
                };

                var actualEWP = months.GroupJoin(calculationDocumentsActualEWP.GroupBy(g => g.monthInt.Value).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    actualAchievement = s.Sum(sum => sum.actualAchievementEWP)
                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countActualEWP = p == null ? 0 : p.count,
                                                actualAchievementEWP = p == null ? 0 : p.actualAchievement
                                            });

                var xx = actualEWP.ToList();

                double actualScoreCummulativeEWP = 0;
                List<individualReportEngineer> resultActualEWP = new List<individualReportEngineer>();
                foreach (var s in actualEWP)
                {
                    actualScoreCummulativeEWP += s.actualAchievementEWP;

                    s.actualScoreCummEWP = actualScoreCummulativeEWP;

                    resultActualEWP.Add(s);
                }
                #endregion EWP Actual


                #region Conformity Plan
                var planConformity = months.GroupJoin(db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.PLANFINISHDATE.Value.Year == paramYearDesigner) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listConformityId.Contains(w.OID)).GroupBy(g => g.PLANFINISHDATE.Value.Month).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))

                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                            SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countPlanConformity = p == null ? 0 : p.count,
                                                deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0
                                            });


                double planScoreCummulativeConformity = 0;
                List<individualReportEngineer> resultPlanConformity = new List<individualReportEngineer>();
                foreach (var s in planConformity)
                {
                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreConformity = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter
                    double valueColumnParam = 0;
                    if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    {
                        s.planColumnParameterConformity = (s.planScoreConformity / 100) * valueColumnParam;
                    }
                    else
                    {
                        s.planColumnParameterConformity = 0;
                    }

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeConformity += s.planColumnParameterConformity;
                    s.planScoreCummConformity = planScoreCummulativeConformity;

                    resultPlanConformity.Add(s);
                }
                #endregion Conformity Plan

                #region Conformity Actual
                var getDataDocumentsActualConformity = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCUMENTBYID == badgeNo && (w.ACTUALFINISHDATE.Value.Year == paramYearDesigner) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listConformityId.Contains(w.OID));

                List<individualReportEngineer> resultDocumentsActualConformity = new List<individualReportEngineer>();
                foreach (var s in getDataDocumentsActualConformity)
                {
                    individualReportEngineer dataActualConformity = new individualReportEngineer();
                    dataActualConformity.monthInt = s.ACTUALFINISHDATE.Value.Month;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultConformity = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultConformity * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultConformity.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualConformity.actualAchievementConformity = emplyeeFactorWeight * performance;

                    resultDocumentsActualConformity.Add(dataActualConformity);
                };

                var actualConformity = months.GroupJoin(resultDocumentsActualConformity.GroupBy(g => g.monthInt.Value).Select(s => new
                {
                    monthInt = s.Key,
                    count = s.Count(),
                    actualAchievement = s.Sum(sum => sum.actualAchievementConformity)
                }),
                m => m.monthInt, p => p.monthInt,
                (m, p) => new { months = m, compliance = p }).
                SelectMany(temp => temp.compliance.DefaultIfEmpty(),
                                        (temp, p) =>
                                            new individualReportEngineer
                                            {
                                                monthInt = temp.months.monthInt,
                                                monthName = temp.months.monthName,
                                                countActualConformity = p == null ? 0 : p.count,
                                                actualAchievementConformity = p == null ? 0 : p.actualAchievement
                                            });

                double actualScoreCummulativeConformity = 0;
                List<individualReportEngineer> resultActualConformity = new List<individualReportEngineer>();
                foreach (var s in actualConformity)
                {
                    actualScoreCummulativeConformity += s.actualAchievementConformity;

                    s.actualScoreCummConformity = actualScoreCummulativeConformity;

                    resultActualConformity.Add(s);
                }
                #endregion Conformity Actual

                #region calculation
                var calculateDatas = resultPlanEWP.
                    Join(resultActualEWP, planEwp => planEwp.monthInt, actualEwp => actualEwp.monthInt, (planEwp, actualEwp) => new { planEwp = planEwp, actualEwp = actualEwp }).
                    Join(resultPlanConformity, planJoinEwp => planJoinEwp.planEwp.monthInt, planConf => planConf.monthInt, (planJoinEwp, planConf) => new { planJoinEwp = planJoinEwp, planConf = planConf }).
                    Join(resultActualConformity, pJoinAll => pJoinAll.planConf.monthInt, actualConf => actualConf.monthInt, (pJoinAll, actualConf) => new { planEWP = pJoinAll.planJoinEwp.planEwp, actualEWP = pJoinAll.planJoinEwp.actualEwp, planConf = pJoinAll.planConf, actualConf = actualConf }).
                    Select(s => new individualReportEngineer
                    {
                        monthInt = s.planEWP.monthInt,
                        monthName = s.planEWP.monthName + " " + paramYearDesigner,
                        countPlanEWP = s.planEWP.countPlanEWP,
                        planScoreEWP = s.planEWP.planScoreEWP,
                        planColumnParameterEWP = s.planEWP.planColumnParameterEWP,
                        planScoreCummEWP = s.planEWP.planScoreCummEWP,
                        countActualEWP = s.actualEWP.countActualEWP,
                        actualAchievementEWP = s.actualEWP.actualAchievementEWP,
                        actualScoreCummEWP = s.actualEWP.actualScoreCummEWP,
                        countPlanConformity = s.planConf.countPlanConformity,
                        planScoreConformity = s.planConf.planScoreConformity,
                        planColumnParameterConformity = s.planConf.planColumnParameterConformity,
                        planScoreCummConformity = s.planConf.planScoreCummConformity,
                        countActualConformity = s.actualConf.countActualConformity,
                        actualAchievementConformity = s.actualConf.actualAchievementConformity,
                        actualScoreCummConformity = s.actualConf.actualScoreCummConformity
                    });
                #endregion calculation

                return calculateDatas;
            }
            else {
                return new List<individualReportEngineer>();
            }
        }

        [HttpGet]
        [ActionName("SummaryReportEngineer")]
        public object SummaryReportEngineer(int? param)
        {
            var paramYear = param;
            if (paramYear != null)
            {
                //var months = Enumerable.Range(1, 12).Select(i => new { monthInt = i, monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
                var columnParam = db.GENERALPARAMETERs.FirstOrDefault(w => w.TITLE == constants.INDIVIDUAL_REPORT_ENGINEER);
                var employeeInYear = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || w.DOCTYPE==constants.DOCUMENTTYPE_DESIGN_CONFORMITY ))
                ).Select(s => 
                    new {
                        DOCUMENTBYID = s.DOCUMENTBYID,
                        DOCUMENTBYNAME = s.DOCUMENTBYNAME,
                        section = s.PROJECT.USERASSIGNMENTs.Where(ua => (ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER || ua.ASSIGNMENTTYPE==constants.PORJECT_DESIGN ) && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION
                    }).Distinct();

                #region EWP Plan
                var pdEwp1 = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).
                    Select(s => new
                    {
                        DOCUMENTBYID = s.DOCUMENTBYID,
                        ACTUALFINISHDATE = s.ACTUALFINISHDATE,
                        section = s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION,
                        PROJECTDOCUMENTWEIGHTHOURS = s.PROJECTDOCUMENTWEIGHTHOURS
                    });
                var pdEwp = pdEwp1.
                    GroupBy(g => new { g.DOCUMENTBYID, g.section }).
                    Select(s => new
                    {
                        DOCUMENTBYID = s.Key.DOCUMENTBYID,
                        section = s.Key.section,
                        count = s.Count(),
                        totalEWPCompleted = s.Where(c => c.ACTUALFINISHDATE.Value.Year == paramYear).Count(),
                        totalEWPBackLog = s.Where(c => c.ACTUALFINISHDATE.Value.Year > paramYear || c.ACTUALFINISHDATE == null).Count(),
                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1.ACTUALHOURS))
                        //                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))
                    });


                var planEWP = employeeInYear.GroupJoin(pdEwp, e1 => new { e1.DOCUMENTBYID, e1.section }, e2 => new { e2.DOCUMENTBYID, e2.section }, (e1, e2) => new { emp = e1, ewp = e2 }).
                    SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                    (temp, p) => new summaryReportEngineer
                    {
                        badgeNo = temp.emp.DOCUMENTBYID,
                        name = temp.emp.DOCUMENTBYNAME,
                        section = temp.emp.section,
                        totalEWP = p == null ? 0 : p.count,
                        totalEWPCompleted = p == null ? 0 : p.totalEWPCompleted,
                        totalEWPBackLog = p == null ? 0 : p.totalEWPBackLog,
                        deliverableWeightHour = p==null? 0: p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value:0,
                    }).OrderBy(o => o.name);

                double planScoreCummulativeEWP = 0;
                List<summaryReportEngineer> resultPlanEWP = new List<summaryReportEngineer>();
                foreach (var s in planEWP)
                {
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID==s.badgeNo);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreEWP = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter

                    //double valueColumnParam = 0;
                    //if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    //{
                    //    s.planColumnParameterEWP = s.planScoreEWP * valueColumnParam;
                    //}
                    //else
                    //{
                    //    s.planColumnParameterEWP = 0;
                    //}

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeEWP += s.planColumnParameterEWP;
                    s.planScoreCummEWP = planScoreCummulativeEWP;
                    resultPlanEWP.Add(s);
                }
                #endregion EWP Plan

                #region EWP Actual
                var getDataDocumentsActualEWP = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER);
                List<summaryReportEngineer> calculationDocumentsActualEWP = new List<summaryReportEngineer>();
                foreach (var s in getDataDocumentsActualEWP)
                {
                    summaryReportEngineer dataActualEWP = new summaryReportEngineer();
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.DOCUMENTBYID);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;
                    //dataActualEWP.monthInt = s.ACTUALFINISHDATE.Value.Month;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultEWP = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;
                    if (((s.ACTUALFINISHDATE != null) && (s.PLANSTARTDATE != null))&&(s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays == 0)
                        actualFinalResultEWP = 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultEWP * 100) / 100;
                    var test = db.MASTERCOMPLIANCEPERFORMANCEs;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultEWP.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualEWP.badgeNo = s.DOCUMENTBYID;
                    dataActualEWP.name = s.DOCUMENTBYNAME;
                    dataActualEWP.section = s.PROJECT != null && s.PROJECT.USERASSIGNMENTs != null &&
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault() != null  ? 
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION : ""; 
                    dataActualEWP.actualAchievementEWP = emplyeeFactorWeight * performance;
                    dataActualEWP.tempActualFinish = s.ACTUALFINISHDATE;
                    dataActualEWP.tempSumPDWActualHour = s.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1 == null ? 0 : sum1.ACTUALHOURS);
                    calculationDocumentsActualEWP.Add(dataActualEWP);
                };
                var rsdac = calculationDocumentsActualEWP.GroupBy(g => new { g.badgeNo, g.section }).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.Key.badgeNo,
                                        section = s.Key.section,
                                        count = s.Count(),
                                        actualAchievementEWP = s.Sum(sum => sum.actualAchievementEWP),
                                        totalEWPCompleted = s.Where(c => c.tempActualFinish != null ? c.tempActualFinish.Value.Year == paramYear : false).Count(),
                                        totalEWPBackLog = s.Where(c => (c.tempActualFinish != null ? c.tempActualFinish.Value.Year > paramYear : false) || c.tempActualFinish == null).Count(),
                                        deliverableWeightHour = s.Sum(sum => sum.tempSumPDWActualHour) == null ? 0 : s.Sum(sum => sum.tempSumPDWActualHour)
                                    });
                var ac = employeeInYear.AsEnumerable().GroupJoin(rsdac,
                    e1 => new { badgeno = e1.DOCUMENTBYID, section = e1.section },
                    e2 => new { badgeno = e2.DOCUMENTBYID, section = e2.section },
                    (e1, e2) => new { emp = e1, ewp = e2 });

                var actualEWP = ac.SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                                    (temp, p) => new summaryReportEngineer
                                    {
                                        badgeNo = temp.emp.DOCUMENTBYID,
                                        name = temp.emp.DOCUMENTBYNAME,
                                        section = temp.emp.section,
                                        totalEWP = p == null ? 0 : p.count,
                                        totalEWPCompleted = p == null ? 0 : p.totalEWPCompleted,
                                        totalEWPBackLog = p == null ? 0 : p.totalEWPBackLog,
                                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                                        actualAchievementEWP = p == null ? 0 : p.actualAchievementEWP,
                                    }).OrderBy(o => o.name);


                //var xx = actualEWP.ToList();

                double actualScoreCummulativeEWP = 0;
                List<summaryReportEngineer> resultActualEWP = new List<summaryReportEngineer>();
                foreach (var s in actualEWP)
                {
                    actualScoreCummulativeEWP += s.planColumnParameterEWP;

                    s.actualScoreCummEWP = actualScoreCummulativeEWP;

                    resultActualEWP.Add(s);
                }
                #endregion EWP Actual


                #region Conformity Plan

                var pd1 = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.DOCUMENTBYID,
                                        ACTUALFINISHDATE = s.ACTUALFINISHDATE,
                                        section = s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION,
                                        PROJECTDOCUMENTWEIGHTHOURS = s.PROJECTDOCUMENTWEIGHTHOURS.Select(pdw => new { ACTUALHOURS = pdw.ACTUALHOURS })
                                    });
//                var pd2 = pd1.Where(w => w.DOCUMENTBYID == "0000008829");
                var pd = pd1.GroupBy(g => new { g.DOCUMENTBYID, g.section }).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.Key.DOCUMENTBYID,
                                        section = s.Key.section,
                                        count = s.Count(),
                                        totalConformityCompleted = s.Where(c => c.ACTUALFINISHDATE.Value.Year == paramYear).Count(),
                                        totalConformityBackLog = s.Where(c => c.ACTUALFINISHDATE.Value.Year > paramYear || c.ACTUALFINISHDATE == null).Count(),
                                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1.ACTUALHOURS))
                                        //                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))
                                    });

                var planConformity = employeeInYear.GroupJoin(pd, e1 => new { e1.DOCUMENTBYID, e1.section }, e2 => new { e2.DOCUMENTBYID, e2.section }, (e1, e2) => new { emp = e1, ewp = e2 }).
                                    SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                                    (temp, p) => new summaryReportEngineer
                                    {
                                        badgeNo = temp.emp.DOCUMENTBYID,
                                        name = temp.emp.DOCUMENTBYNAME,
                                        section = temp.emp.section,
                                        totalConformity = p == null ? 0 : p.count,
                                        totalConformityCompleted = p == null ? 0 : p.totalConformityCompleted,
                                        totalConformityBackLog = p == null ? 0 : p.totalConformityBackLog,
                                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                                    }).OrderBy(o => o.name);


                double planScoreCummulativeConformity = 0;
                List<summaryReportEngineer> resultPlanConformity = new List<summaryReportEngineer>();
                foreach (var s in planConformity)
                {
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.badgeNo);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreConformity = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter
                    double valueColumnParam = 0;
                    if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    {
                        s.planColumnParameterConformity = s.planScoreConformity * valueColumnParam;
                    }
                    else
                    {
                        s.planColumnParameterConformity = 0;
                    }

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeConformity += s.planColumnParameterConformity;
                    s.planScoreCummConformity = planScoreCummulativeConformity;

                    resultPlanConformity.Add(s);
                }
                #endregion Conformity Plan

                #region Conformity Actual
                var getDataDocumentsActualConformity = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY);

                List<summaryReportEngineer> resultDocumentsActualConformity = new List<summaryReportEngineer>();
                foreach (var s in getDataDocumentsActualConformity)
                {
                    summaryReportEngineer dataActualConformity = new summaryReportEngineer();
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.DOCUMENTBYID);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultConformity = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;
                    if (((s.ACTUALFINISHDATE != null) && (s.PLANSTARTDATE != null)) && (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays == 0)
                        actualFinalResultConformity = 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultConformity * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultConformity.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualConformity.actualAchievementConformity = emplyeeFactorWeight * performance;
                    dataActualConformity.badgeNo = s.DOCUMENTBYID;
                    dataActualConformity.name = s.DOCUMENTBYNAME;
                    dataActualConformity.section = s.PROJECT != null && s.PROJECT.USERASSIGNMENTs != null &&
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PORJECT_DESIGN && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault() != null ?
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PORJECT_DESIGN && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION : "";
                    dataActualConformity.tempActualFinish = s.ACTUALFINISHDATE;
                    dataActualConformity.tempSumPDWActualHour = s.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1==null?0:sum1.ACTUALHOURS);

                    resultDocumentsActualConformity.Add(dataActualConformity);
                };
                var rsdacEWP = resultDocumentsActualConformity.GroupBy(g => new { g.badgeNo, g.section }).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.Key.badgeNo,
                                        section = s.Key.section,
                                        count = s.Count(),
                                        actualAchievementConformity = s.Sum(sum => sum.actualAchievementConformity),
                                        totalConformityCompleted =  s.Where(c => c.tempActualFinish != null ? c.tempActualFinish.Value.Year == paramYear:false).Count(),
                                        totalConformityBackLog = s.Where(c => (c.tempActualFinish != null ? c.tempActualFinish.Value.Year > paramYear : false) || c.tempActualFinish == null).Count(),
                                        deliverableWeightHour =s.Sum(sum => sum.tempSumPDWActualHour) == null?0: s.Sum(sum => sum.tempSumPDWActualHour)
                                    });
                var actualConformity = employeeInYear.AsEnumerable().GroupJoin(rsdacEWP,
                    e1 => new { badgeno = e1.DOCUMENTBYID, section = e1.section },
                    e2 => new { badgeno = e2.DOCUMENTBYID, section = e2.section },
                    (e1, e2) => new { emp = e1, ewp = e2 }).SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                                    (temp, p) => new summaryReportEngineer
                                    {
                                        badgeNo = temp.emp.DOCUMENTBYID,
                                        name = temp.emp.DOCUMENTBYNAME,
                                        section = temp.emp.section,
                                        totalConformity = p == null ? 0 : p.count,
                                        totalConformityCompleted = p == null ? 0 : p.totalConformityCompleted,
                                        totalConformityBackLog = p == null ? 0 : p.totalConformityBackLog,
                                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                                        actualAchievementConformity = p == null ? 0 : p.actualAchievementConformity,
                                    }).OrderBy(o => o.name);


                double actualScoreCummulativeConformity = 0;
                List<summaryReportEngineer> resultActualConformity = new List<summaryReportEngineer>();
                foreach (var s in actualConformity)
                {
                    actualScoreCummulativeConformity += s.planColumnParameterConformity;

                    s.actualScoreCummConformity = actualScoreCummulativeConformity;

                    resultActualConformity.Add(s);
                }
                #endregion Conformity Actual

                #region calculation
                var calculateDatas = resultPlanEWP.
                //Join(resultActualEWP, planEwp => new { planEwp.badgeNo, planEwp.section }, actualEwp => new { actualEwp.badgeNo, actualEwp.section }, (planEwp, actualEwp) => new { planEWP = planEwp, actualEwp = actualEwp });
                //Join(resultPlanConformity, pJoinAll => new { pJoinAll.badgeNo, pJoinAll.section }, planConf => new { planConf.badgeNo, planConf.section }, (pJoinAll, planConf) => new { planEWP = pJoinAll, planConf = planConf });
                //Join(resultPlanConformity, pJoinAll => new { pJoinAll.planEwp.badgeNo, pJoinAll.planEwp.section }, planConf => new { planConf.badgeNo, planConf.section }, (pJoinAll, planConf) => new { planEWP = pJoinAll.planEwp, planConf = planConf });
                Join(resultActualEWP, planEwp => new { planEwp.badgeNo, planEwp.section }, actualEwp => new { actualEwp.badgeNo, actualEwp.section }, (planEwp, actualEwp) => new { planEwp = planEwp, actualEwp = actualEwp }).
                Join(resultPlanConformity, planJoinEwp => new { planJoinEwp.planEwp.badgeNo, planJoinEwp.planEwp.section }, planConf => new { planConf.badgeNo, planConf.section }, (planJoinEwp, planConf) => new { planJoinEwp = planJoinEwp, planConf = planConf }).
                Join(resultActualConformity, pJoinAll => new { pJoinAll.planConf.badgeNo, pJoinAll.planConf.section }, actualConf => new { actualConf.badgeNo, actualConf.section }, (pJoinAll, actualConf) => new { planEWP = pJoinAll.planJoinEwp.planEwp, actualEWP = pJoinAll.planJoinEwp.actualEwp, planConf = pJoinAll.planConf, actualConf = actualConf });
                var sCalculateDatas =
                    calculateDatas.Select(s => new summaryReportEngineer
                    {
                        badgeNo = s.planEWP.badgeNo,
                        name = s.planEWP.name,
                        section = s.planEWP.section,
                        totalEWP = s.planEWP.totalEWP,
                        totalEWPCompleted = s.planEWP.totalEWPCompleted,
                        totalEWPBackLog = s.planEWP.totalEWPBackLog,
                        planScoreEWP = s.planEWP.planScoreEWP,
                        actualAchievementEWP = s.actualEWP.actualAchievementEWP,
                        totalConformity = s.planConf.totalConformity,
                        totalConformityCompleted = s.planConf.totalConformityCompleted,
                        totalConformityBackLog = s.planConf.totalConformityBackLog,
                        planScoreConformity = s.planConf.planScoreConformity,
                        actualAchievementConformity = s.actualConf.actualAchievementConformity,

                        //planColumnParameterConformity = s.planConf.planColumnParameterConformity,
                        //planScoreCummConformity = s.planConf.planScoreCummConformity,

                        //actualScoreCummConformity = s.actualConf.actualScoreCummConformity
                    }).OrderBy(o => o.name);
                #endregion calculation

                //return calculateDatas;
                return sCalculateDatas;
            }
            else
            {
                return new List<summaryReportEngineer>();
            }
        }

        [HttpGet]
        [ActionName("SummaryReportDesigner")]
        public object SummaryReportDesigner(int? param)
        {
            var paramYear = param;
            if (paramYear != null)
            {
                //var months = Enumerable.Range(1, 12).Select(i => new { monthInt = i, monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i) });
                var columnParam = db.GENERALPARAMETERs.FirstOrDefault(w => w.TITLE == constants.INDIVIDUAL_REPORT_ENGINEER);
                var employeeInYear = db.PROJECTDOCUMENTs.Where(w => (w.PLANSTARTDATE.Value.Year == paramYear && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN))
                && w.PROJECTID != null
                ).Select(s =>
                    new {
                        DOCUMENTBYID = s.DOCUMENTBYID,
                        DOCUMENTBYNAME = s.DOCUMENTBYNAME,
                        section = s.PROJECT.USERASSIGNMENTs.Where(ua => (ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER) && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION
                    }).Distinct();

                //get filter EWP or Conformity
                List<int> listEWPId = new List<int>();
                List<int> listConformityId = new List<int>();
                foreach (var projDoc in db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && (w.PLANSTARTDATE.Value.Year == param || w.ACTUALSTARTDATE.Value.Year == param)))
                {
                    var documentGroup = GetDocumentGroupDesignerbyProjectId(projDoc.OID);
                    if (documentGroup == constants.DOCUMENTTYPE_ENGINEER)
                    {
                        listEWPId.Add(projDoc.OID);
                    }
                    else if (documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                    {
                        listConformityId.Add(projDoc.OID);
                    }
                }
                #region EWP Plan
                var pdEwp1 = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listEWPId.Contains(w.OID)).
                    Select(s => new
                    {
                        DOCUMENTBYID = s.DOCUMENTBYID,
                        ACTUALFINISHDATE = s.ACTUALFINISHDATE,
                        section = s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION,
                        PROJECTDOCUMENTWEIGHTHOURS = s.PROJECTDOCUMENTWEIGHTHOURS
                    });
                var pdEwp = pdEwp1.
                    GroupBy(g => new { g.DOCUMENTBYID, g.section }).
                    Select(s => new
                    {
                        DOCUMENTBYID = s.Key.DOCUMENTBYID,
                        section = s.Key.section,
                        count = s.Count(),
                        totalEWPCompleted = s.Where(c => c.ACTUALFINISHDATE.Value.Year == paramYear).Count(),
                        totalEWPBackLog = s.Where(c => c.ACTUALFINISHDATE.Value.Year > paramYear || c.ACTUALFINISHDATE == null).Count(),
                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1.ACTUALHOURS))
                        //                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))
                    });


                var planEWP = employeeInYear.GroupJoin(pdEwp, e1 => new { e1.DOCUMENTBYID, e1.section }, e2 => new { e2.DOCUMENTBYID, e2.section }, (e1, e2) => new { emp = e1, ewp = e2 }).
                    SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                    (temp, p) => new summaryReportEngineer
                    {
                        badgeNo = temp.emp.DOCUMENTBYID,
                        name = temp.emp.DOCUMENTBYNAME,
                        section = temp.emp.section,
                        totalEWP = p == null ? 0 : p.count,
                        totalEWPCompleted = p == null ? 0 : p.totalEWPCompleted,
                        totalEWPBackLog = p == null ? 0 : p.totalEWPBackLog,
                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                    }).OrderBy(o => o.name);

                double planScoreCummulativeEWP = 0;
                List<summaryReportEngineer> resultPlanEWP = new List<summaryReportEngineer>();
                foreach (var s in planEWP)
                {
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.badgeNo);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreEWP = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter

                    //double valueColumnParam = 0;
                    //if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    //{
                    //    s.planColumnParameterEWP = s.planScoreEWP * valueColumnParam;
                    //}
                    //else
                    //{
                    //    s.planColumnParameterEWP = 0;
                    //}

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeEWP += s.planColumnParameterEWP;
                    s.planScoreCummEWP = planScoreCummulativeEWP;
                    resultPlanEWP.Add(s);
                }
                #endregion EWP Plan

                #region EWP Actual
                var getDataDocumentsActualEWP = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listEWPId.Contains(w.OID));
                List<summaryReportEngineer> calculationDocumentsActualEWP = new List<summaryReportEngineer>();
                foreach (var s in getDataDocumentsActualEWP)
                {
                    summaryReportEngineer dataActualEWP = new summaryReportEngineer();
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.DOCUMENTBYID);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;
                    //dataActualEWP.monthInt = s.ACTUALFINISHDATE.Value.Month;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultEWP = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;
                    if (((s.ACTUALFINISHDATE != null) && (s.PLANSTARTDATE != null)) && (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays == 0)
                        actualFinalResultEWP = 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultEWP * 100) / 100;
                    var test = db.MASTERCOMPLIANCEPERFORMANCEs;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultEWP.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualEWP.badgeNo = s.DOCUMENTBYID;
                    dataActualEWP.name = s.DOCUMENTBYNAME;
                    dataActualEWP.section = s.PROJECT != null && s.PROJECT.USERASSIGNMENTs != null &&
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault() != null ?
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION : "";
                    dataActualEWP.actualAchievementEWP = emplyeeFactorWeight * performance;
                    dataActualEWP.tempActualFinish = s.ACTUALFINISHDATE;
                    dataActualEWP.tempSumPDWActualHour = s.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1 == null ? 0 : sum1.ACTUALHOURS);
                    calculationDocumentsActualEWP.Add(dataActualEWP);
                };
                var rsdac = calculationDocumentsActualEWP.GroupBy(g => new { g.badgeNo, g.section }).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.Key.badgeNo,
                                        section = s.Key.section,
                                        count = s.Count(),
                                        actualAchievementEWP = s.Sum(sum => sum.actualAchievementEWP),
                                        totalEWPCompleted = s.Where(c => c.tempActualFinish != null ? c.tempActualFinish.Value.Year == paramYear : false).Count(),
                                        totalEWPBackLog = s.Where(c => (c.tempActualFinish != null ? c.tempActualFinish.Value.Year > paramYear : false) || c.tempActualFinish == null).Count(),
                                        deliverableWeightHour = s.Sum(sum => sum.tempSumPDWActualHour) == null ? 0 : s.Sum(sum => sum.tempSumPDWActualHour)
                                    });
                var ac = employeeInYear.AsEnumerable().GroupJoin(rsdac,
                    e1 => new { badgeno = e1.DOCUMENTBYID, section = e1.section },
                    e2 => new { badgeno = e2.DOCUMENTBYID, section = e2.section },
                    (e1, e2) => new { emp = e1, ewp = e2 });

                var actualEWP = ac.SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                                    (temp, p) => new summaryReportEngineer
                                    {
                                        badgeNo = temp.emp.DOCUMENTBYID,
                                        name = temp.emp.DOCUMENTBYNAME,
                                        section = temp.emp.section,
                                        totalEWP = p == null ? 0 : p.count,
                                        totalEWPCompleted = p == null ? 0 : p.totalEWPCompleted,
                                        totalEWPBackLog = p == null ? 0 : p.totalEWPBackLog,
                                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                                        actualAchievementEWP = p == null ? 0 : p.actualAchievementEWP,
                                    }).OrderBy(o => o.name);


                //var xx = actualEWP.ToList();

                double actualScoreCummulativeEWP = 0;
                List<summaryReportEngineer> resultActualEWP = new List<summaryReportEngineer>();
                foreach (var s in actualEWP)
                {
                    actualScoreCummulativeEWP += s.planColumnParameterEWP;

                    s.actualScoreCummEWP = actualScoreCummulativeEWP;

                    resultActualEWP.Add(s);
                }
                #endregion EWP Actual


                #region Conformity Plan

                var pd1 = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listConformityId.Contains(w.OID)).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.DOCUMENTBYID,
                                        ACTUALFINISHDATE = s.ACTUALFINISHDATE,
                                        section = s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PORJECT_DESIGN && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION,
                                        PROJECTDOCUMENTWEIGHTHOURS = s.PROJECTDOCUMENTWEIGHTHOURS.Select(pdw => new { ACTUALHOURS = pdw.ACTUALHOURS })
                                    });
                //                var pd2 = pd1.Where(w => w.DOCUMENTBYID == "0000008829");
                var pd = pd1.GroupBy(g => new { g.DOCUMENTBYID, g.section }).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.Key.DOCUMENTBYID,
                                        section = s.Key.section,
                                        count = s.Count(),
                                        totalConformityCompleted = s.Where(c => c.ACTUALFINISHDATE.Value.Year == paramYear).Count(),
                                        totalConformityBackLog = s.Where(c => c.ACTUALFINISHDATE.Value.Year > paramYear || c.ACTUALFINISHDATE == null).Count(),
                                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1.ACTUALHOURS))
                                        //                        deliverableWeightHour = s.Sum(sum => sum.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum1 => sum1.ACTUALHOURS))
                                    });

                var planConformity = employeeInYear.GroupJoin(pd, e1 => new { e1.DOCUMENTBYID, e1.section }, e2 => new { e2.DOCUMENTBYID, e2.section }, (e1, e2) => new { emp = e1, ewp = e2 }).
                                    SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                                    (temp, p) => new summaryReportEngineer
                                    {
                                        badgeNo = temp.emp.DOCUMENTBYID,
                                        name = temp.emp.DOCUMENTBYNAME,
                                        section = temp.emp.section,
                                        totalConformity = p == null ? 0 : p.count,
                                        totalConformityCompleted = p == null ? 0 : p.totalConformityCompleted,
                                        totalConformityBackLog = p == null ? 0 : p.totalConformityBackLog,
                                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                                    }).OrderBy(o => o.name);


                double planScoreCummulativeConformity = 0;
                List<summaryReportEngineer> resultPlanConformity = new List<summaryReportEngineer>();
                foreach (var s in planConformity)
                {
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.badgeNo);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                    //•	Weight Hours(%) = Weight Hours / Baseline Hours
                    double deliverableWeightPercent = baseLineHours > 0 ? (s.deliverableWeightHour / baseLineHours) * 100 : 0;

                    //Factored Weight
                    //•	Hanya bisa dihitung jika data Drawing EWP memiliki informasi: Designer, Plan Start Date, Plan Finish Date
                    //•	Factored Weight = [Deliverable Weight %] / [Resources Factor]
                    //•	Plan Score % = Sum dari Factor Weight, yang plan finish
                    s.planScoreConformity = emplyeeFactorWeight != 0 ? deliverableWeightPercent / emplyeeFactorWeight : 0;

                    //•	Kolom “125” = [Plan Score %] * 125
                    //125 simpan di general parameter
                    double valueColumnParam = 0;
                    if (double.TryParse(columnParam.VALUE, out valueColumnParam))
                    {
                        s.planColumnParameterConformity = s.planScoreConformity * valueColumnParam;
                    }
                    else
                    {
                        s.planColumnParameterConformity = 0;
                    }

                    //•	Plan Score Cumm
                    //Kumulatif dari kolom “125” sejak bulan sebelumnya.
                    planScoreCummulativeConformity += s.planColumnParameterConformity;
                    s.planScoreCummConformity = planScoreCummulativeConformity;

                    resultPlanConformity.Add(s);
                }
                #endregion Conformity Plan

                #region Conformity Actual
                var getDataDocumentsActualConformity = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANSTARTDATE.Value.Year == paramYear) && w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN && listConformityId.Contains(w.OID));

                List<summaryReportEngineer> resultDocumentsActualConformity = new List<summaryReportEngineer>();
                foreach (var s in getDataDocumentsActualConformity)
                {
                    summaryReportEngineer dataActualConformity = new summaryReportEngineer();
                    var employeeParam = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == paramYear.Value && w.EMPLOYEEID == s.DOCUMENTBYID);
                    double emplyeeFactorWeight = employeeParam != null ? employeeParam.ENGINEERDESIGNERFACTOR.HasValue ? employeeParam.ENGINEERDESIGNERFACTOR.Value : 0 : 0;
                    double baseLineHours = employeeParam != null ? employeeParam.BASELINEHOURS.HasValue ? employeeParam.BASELINEHOURS.Value : 0 : 0;

                    //Final Result
                    //•	Hanya dihitung jika Actual Finish Date sudah terisi
                    //•	Final Result: ([Plan Finish] – [Plan Start]) / [Actual Finish] – [Plan Start]
                    double actualFinalResultConformity = s.ACTUALFINISHDATE.HasValue && s.PLANFINISHDATE.HasValue && s.PLANSTARTDATE.HasValue ? (s.PLANFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays / (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays : 0;
                    if (((s.ACTUALFINISHDATE != null) && (s.PLANSTARTDATE != null)) && (s.ACTUALFINISHDATE.Value - s.PLANSTARTDATE.Value).TotalDays == 0)
                        actualFinalResultConformity = 0;

                    //Performance
                    //•	If[Final Result](x) < 0.8 then the performance value = 0 
                    //•	If[Final Result](x) more than 0.8 and less than 1.0,
                    //                then the performance value = 250 x – 150
                    //•	If[Final Result](x) more than 1 and less than 1.05,
                    //                then the performance value = 500 x – 400
                    //•	If[Final Result](x) > 1.05 then the performance value = 125
                    double finalResult2Decimal = Math.Floor(actualFinalResultConformity * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", actualFinalResultConformity.ToString()) :
                                                        "";
                    double performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    //•	Achievement = [Factor Weight] * [Performance Value]
                    dataActualConformity.actualAchievementConformity = emplyeeFactorWeight * performance;
                    dataActualConformity.badgeNo = s.DOCUMENTBYID;
                    dataActualConformity.name = s.DOCUMENTBYNAME;
                    dataActualConformity.section = s.PROJECT != null && s.PROJECT.USERASSIGNMENTs != null &&
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PORJECT_DESIGN && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault() != null ?
                        s.PROJECT.USERASSIGNMENTs.Where(ua => ua.ASSIGNMENTTYPE == constants.PORJECT_DESIGN && ua.EMPLOYEEID == s.DOCUMENTBYID).FirstOrDefault().EMPLOYEEPOSITION : "";
                    dataActualConformity.tempActualFinish = s.ACTUALFINISHDATE;
                    dataActualConformity.tempSumPDWActualHour = s.PROJECTDOCUMENTWEIGHTHOURS.DefaultIfEmpty().Sum(sum1 => sum1 == null ? 0 : sum1.ACTUALHOURS);

                    resultDocumentsActualConformity.Add(dataActualConformity);
                };
                var rsdacEWP = resultDocumentsActualConformity.GroupBy(g => new { g.badgeNo, g.section }).
                                    Select(s => new
                                    {
                                        DOCUMENTBYID = s.Key.badgeNo,
                                        section = s.Key.section,
                                        count = s.Count(),
                                        actualAchievementConformity = s.Sum(sum => sum.actualAchievementConformity),
                                        totalConformityCompleted = s.Where(c => c.tempActualFinish != null ? c.tempActualFinish.Value.Year == paramYear : false).Count(),
                                        totalConformityBackLog = s.Where(c => (c.tempActualFinish != null ? c.tempActualFinish.Value.Year > paramYear : false) || c.tempActualFinish == null).Count(),
                                        deliverableWeightHour = s.Sum(sum => sum.tempSumPDWActualHour) == null ? 0 : s.Sum(sum => sum.tempSumPDWActualHour)
                                    });
                var actualConformity = employeeInYear.AsEnumerable().GroupJoin(rsdacEWP,
                    e1 => new { badgeno = e1.DOCUMENTBYID, section = e1.section },
                    e2 => new { badgeno = e2.DOCUMENTBYID, section = e2.section },
                    (e1, e2) => new { emp = e1, ewp = e2 }).SelectMany(temp => temp.ewp.DefaultIfEmpty(),
                                    (temp, p) => new summaryReportEngineer
                                    {
                                        badgeNo = temp.emp.DOCUMENTBYID,
                                        name = temp.emp.DOCUMENTBYNAME,
                                        section = temp.emp.section,
                                        totalConformity = p == null ? 0 : p.count,
                                        totalConformityCompleted = p == null ? 0 : p.totalConformityCompleted,
                                        totalConformityBackLog = p == null ? 0 : p.totalConformityBackLog,
                                        deliverableWeightHour = p == null ? 0 : p.deliverableWeightHour.HasValue ? p.deliverableWeightHour.Value : 0,
                                        actualAchievementConformity = p == null ? 0 : p.actualAchievementConformity,
                                    }).OrderBy(o => o.name);


                double actualScoreCummulativeConformity = 0;
                List<summaryReportEngineer> resultActualConformity = new List<summaryReportEngineer>();
                foreach (var s in actualConformity)
                {
                    actualScoreCummulativeConformity += s.planColumnParameterConformity;

                    s.actualScoreCummConformity = actualScoreCummulativeConformity;

                    resultActualConformity.Add(s);
                }
                #endregion Conformity Actual

                #region calculation
                var calculateDatas = resultPlanEWP.
                Join(resultActualEWP, planEwp => new { planEwp.badgeNo, planEwp.section }, actualEwp => new { actualEwp.badgeNo, actualEwp.section }, (planEwp, actualEwp) => new { planEwp = planEwp, actualEwp = actualEwp }).
                Join(resultPlanConformity, planJoinEwp => new { planJoinEwp.planEwp.badgeNo, planJoinEwp.planEwp.section }, planConf => new { planConf.badgeNo, planConf.section }, (planJoinEwp, planConf) => new { planJoinEwp = planJoinEwp, planConf = planConf }).
                Join(resultActualConformity, pJoinAll => new { pJoinAll.planConf.badgeNo, pJoinAll.planConf.section }, actualConf => new { actualConf.badgeNo, actualConf.section }, (pJoinAll, actualConf) => new { planEWP = pJoinAll.planJoinEwp.planEwp, actualEWP = pJoinAll.planJoinEwp.actualEwp, planConf = pJoinAll.planConf, actualConf = actualConf });
                var sCalculateDatas =
                    calculateDatas.Select(s => new summaryReportEngineer
                    {
                        badgeNo = s.planEWP.badgeNo,
                        name = s.planEWP.name,
                        section = s.planEWP.section,
                        totalEWP = s.planEWP.totalEWP,
                        totalEWPCompleted = s.planEWP.totalEWPCompleted,
                        totalEWPBackLog = s.planEWP.totalEWPBackLog,
                        planScoreEWP = s.planEWP.planScoreEWP,
                        actualAchievementEWP = s.actualEWP.actualAchievementEWP,
                        totalConformity = s.planConf.totalConformity,
                        totalConformityCompleted = s.planConf.totalConformityCompleted,
                        totalConformityBackLog = s.planConf.totalConformityBackLog,
                        planScoreConformity = s.planConf.planScoreConformity,
                        actualAchievementConformity = s.actualConf.actualAchievementConformity,

                        //planColumnParameterConformity = s.planConf.planColumnParameterConformity,
                        //planScoreCummConformity = s.planConf.planScoreCummConformity,

                        //actualScoreCummConformity = s.actualConf.actualScoreCummConformity
                    }).OrderBy(o => o.name);
                #endregion calculation

                //return calculateDatas;
                return sCalculateDatas;
            }
            else
            {
                return new List<summaryReportEngineer>();
            }
        }


        [HttpGet]
        [ActionName("EwpDrawingReport")]
        public object EwpDrawingReport(int param)
        {
            DateTime currDate = DateTime.Now;

            var dataDrawing = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN)).Select(s => new ewpDrawingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                participation = s.PARTICIPATIONVALUE,
                typeOfDrawing = s.PROJECTDOCUMENTDRAWINGTYPEs.Select(n => new projectDocumentDrawingTypeObject
                {
                    numberOfDrawing = n.NUMBEROFDRAWING,
                    typeOfDrawing = n.MASTERDESIGNERPRODUCTION.TYPEOFDRAWING
                }),
                complexity = s.PROJECTDOCUMENTCOMPLEXITies.Select(m => new projectDocumentComplexityObject
                {
                    masterComplexityDCId = m.MASTERCOMPLEXITYDCID,
                    complexityValue = m.COMPLEXITYVALUE
                })
            });

            List<ewpDrawingReport> result = new List<ewpDrawingReport>();

            foreach (ewpDrawingReport s in dataDrawing)
            {
                var documentGroup = GetDocumentGroupDesignerbyProjectId(s.docId.Value);

                if (documentGroup == constants.DOCUMENTTYPE_ENGINEER)
                {
                    s.totalProduction = GetTotalOfProductionByProjId(s.docId.Value);
                    result.Add(s);
                }
            }


            List<int> listProjectId = result.Select(y => y.projectId).ToList();

            result.AddRange(db.PROJECTs.Where(w => listProjectId.Contains(w.OID)).Select(s => new ewpDrawingReport
            {
                projectId = s.OID,
                projectNo = s.PROJECTNO,
                ewpNo = s.PROJECTNO,
                dwgNo = s.PROJECTNO,
                deliverableNo = s.PROJECTNO,
                deliverableName = s.PROJECTDESCRIPTION,
                level = 0,
                projectType = s.PROJECTTYPE
            }));

            return result.OrderBy(o => o.projectNo).ThenBy(t => t.level == 0 ? null : t.ewpNo).ThenBy(t => t.dwgNo);
        }

        [HttpGet]
        [ActionName("designConformityFinalResult")]
        public object designConformityFinalResult(int param)
        {
            DateTime currDate = DateTime.Now;
            List<designConformityFinalResultReport> result = new List<designConformityFinalResultReport>();

            //Get Design Conformity
            var dataDesignConformitys = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).Select(s => new designConformityFinalResultReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                remarksActualResults = s.REMARKSACTUALRESULTS,
                remarksDesignKpi = s.REMARKSDESIGNKPI,
                level = 1,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                deliverableWeightHour = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS),
                baseLineHours = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID) != null ? db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID).BASELINEHOURS : 0,
                resourcesFactor = db.MASTERYEARLYENGINEERINGHOURS.Count(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID) == 0 ? 0 : db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID).ENGINEERDESIGNERFACTOR,
                projectDocumentFinalResult = s.PROJECTDOCUMENTFINALRESULTS.Select(n => new projectDocumentFinalResultObject
                {
                    masterFinalResultId = n.MASTERFINALRESULTID,
                    masterFinalResultDesc = n.MASTERFINALRESULT.DESCRIPTION,
                    finalResultValue = n.FINALRESULTVALUE,
                    finalResultText = n.FINALRESULTVALUE != null ? db.LOOKUPs.Where(w => w.TYPE == constants.finalResultAction && w.VALUE == n.FINALRESULTVALUE).FirstOrDefault().TEXT : "-"
                })
            });

            foreach (designConformityFinalResultReport s in dataDesignConformitys)
            {
                s.deliverableWeightPercent = s.baseLineHours > 0 ? (s.deliverableWeightHour / s.baseLineHours) * 100 : 0;
                s.finalResult = GetFinalResultByValueByProjId(s.docId.Value);
                double finalResult2Decimal = Math.Floor(s.finalResult.Value * 100) / 100;
                string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                    db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", s.finalResult.ToString()) :
                                                    "";
                s.performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                s.factoredWeight = s.resourcesFactor != 0 ? s.deliverableWeightPercent / s.resourcesFactor : 0;

                //•	Achievement = [Factor Weight] * [Performance Value]
                s.achievement = s.level == 0 ? (double?)null : s.factoredWeight * s.performance;

                result.Add(s);
            }

            //Get Drawing Design Conformity
            var dataDrawing = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN)).Select(s => new designConformityFinalResultReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                remarksActualResults = s.REMARKSACTUALRESULTS,
                remarksDesignKpi = s.REMARKSDESIGNKPI,
                level = 2,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                deliverableWeightHour = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS),
                baseLineHours = db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID) != null ? db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID).BASELINEHOURS : 0,
                resourcesFactor = db.MASTERYEARLYENGINEERINGHOURS.Count(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID) == 0 ? 0 : db.MASTERYEARLYENGINEERINGHOURS.FirstOrDefault(w => w.YEAR == param && w.EMPLOYEEID == s.DOCUMENTBYID).ENGINEERDESIGNERFACTOR,
                projectDocumentFinalResult = s.PROJECTDOCUMENTFINALRESULTS.Select(n => new projectDocumentFinalResultObject
                {
                    masterFinalResultId = n.MASTERFINALRESULTID,
                    masterFinalResultDesc = n.MASTERFINALRESULT.DESCRIPTION,
                    finalResultValue = n.FINALRESULTVALUE,
                    finalResultText = n.FINALRESULTVALUE != null ? db.LOOKUPs.Where(w => w.TYPE == constants.finalResultAction && w.VALUE == n.FINALRESULTVALUE).FirstOrDefault().TEXT : "-"
                })
            });

            foreach (designConformityFinalResultReport s in dataDrawing)
            {
                var documentGroup = GetDocumentGroupDesignerbyProjectId(s.docId.Value);

                if (documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                {
                    s.deliverableWeightPercent = s.baseLineHours > 0 ? (s.deliverableWeightHour / s.baseLineHours) * 100 : 0;
                    s.finalResult = GetFinalResultByValueByProjId(s.docId.Value);
                    double finalResult2Decimal = Math.Floor(s.finalResult.Value * 100) / 100;
                    string performanceFormula = db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal) != null ?
                                                        db.MASTERCOMPLIANCEPERFORMANCEs.FirstOrDefault(w => w.REFERENCECODE == constants.DOCUMENTTYPE_ENGINEER /* untuk ewp dan dwg sama-sama menggunakan EWP*/ && w.MINRANGE <= finalResult2Decimal && w.MAXRANGE >= finalResult2Decimal).PERFORMANCEVALUE.Replace("x", s.finalResult.ToString()) :
                                                        "";
                    s.performance = string.IsNullOrEmpty(performanceFormula) ? 0 : Convert.ToDouble(new DataTable().Compute(performanceFormula, null));
                    s.factoredWeight = s.resourcesFactor != 0 ? s.deliverableWeightPercent / s.resourcesFactor : 0;

                    //•	Achievement = [Factor Weight] * [Performance Value]
                    s.achievement = s.level == 0 ? (double?)null : s.factoredWeight * s.performance;

                    result.Add(s);
                }
            }


            List<int> listProjectId = result.Select(y => y.projectId).ToList();

            result.AddRange(db.PROJECTs.Where(w => listProjectId.Contains(w.OID)).Select(s => new designConformityFinalResultReport
            {
                projectId = s.OID,
                projectNo = s.PROJECTNO,
                deliverableNo = s.PROJECTNO,
                deliverableName = s.PROJECTDESCRIPTION,
                level = 0,
                projectType = s.PROJECTTYPE
            }));

            return result.OrderBy(o => o.projectNo).ThenBy(t => t.level == 0 ? null : t.ewpNo).ThenBy(t => t.dwgNo);
        }

        [HttpGet]
        [ActionName("GetMonthWeekOfYear")]
        public object GetMonthWeekOfYear(int param)
        {
            GENERALPARAMETER strDay = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.STARTWEEKLY).FirstOrDefault();
            DayOfWeek dayOfWeek;
            if (!Enum.TryParse<DayOfWeek>(strDay.VALUE, out dayOfWeek))
            {
                dayOfWeek = DayOfWeek.Monday;
            }

            int totalWeek = 0;
            List<object> listTotalWeekOfMonths = new List<object>();
            for (int i = 1; i <= 12; i++)
            {
                DateTime date = new DateTime(param, i, 18);

                var monthsData = new { month = i, totalWeek = totalWeeksInMonth(date, dayOfWeek) };
                listTotalWeekOfMonths.Add(monthsData);
                totalWeek = totalWeek + totalWeeksInMonth(date, dayOfWeek);
            }
            return new { totalWeekOfYear = GetWeeksInYear(param, dayOfWeek), totalWeek = totalWeek, listTotalWeekOfMonths = listTotalWeekOfMonths };
        }

        [HttpGet]
        [ActionName("EWPResourceLoadingReport")]
        public object EWPResourceLoadingReport(int param)
        {
            GENERALPARAMETER strDay = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.STARTWEEKLY).FirstOrDefault();
            DayOfWeek dayOfWeek;
            if (!Enum.TryParse<DayOfWeek>(strDay.VALUE, out dayOfWeek))
            {
                dayOfWeek = DayOfWeek.Monday;
            }

            int totalWeekOfYear = GetWeeksInYear(param, dayOfWeek);

            List<resourceLoadingReport> result = new List<resourceLoadingReport>();

            var dataEngineer = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            foreach (resourceLoadingReport s in dataEngineer)
            {
                s.planDurationWeek = s.planFinishDate.HasValue && s.planStartDate.HasValue ? Math.Ceiling((s.planFinishDate - s.planStartDate).Value.TotalDays / 7) : (double?)null;
                s.actualDurationWeek = s.actualFinishDate.HasValue && s.actualStartDate.HasValue ? Math.Ceiling((s.actualFinishDate - s.actualStartDate).Value.TotalDays / 7) : (double?)null;
                s.startWeek = !s.planDurationWeek.HasValue ? 0 : s.actualStartDate.HasValue ? (s.actualStartDate.Value.Year < param ? 1 : getDateOfWeek(s.actualStartDate.Value, dayOfWeek)) : (s.planStartDate.Value.Year < param ? 1 : getDateOfWeek(s.planStartDate.Value, dayOfWeek));
                s.endWeek = !s.planDurationWeek.HasValue ? 0 : s.actualFinishDate.HasValue ? (s.actualFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.actualFinishDate.Value, dayOfWeek)) : (s.planFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.planFinishDate.Value, dayOfWeek));

                if (s.actualDurationWeek > 0)
                {
                    s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                }
                else if (s.planDurationWeek > 0)
                {
                    s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                }
                else {
                    s.weeklyDistributedWeight = 0;
                }

                List<resourceLoadingDetail> resourceDetail = new List<resourceLoadingDetail>();
                if (s.startWeek > 0 && s.endWeek > 0)
                {
                    for (var i = 1; i <= totalWeekOfYear; i++)
                    {
                        resourceLoadingDetail detail = new resourceLoadingDetail();
                        detail.week = i;
                        detail.weight = 0;
                        if (i >= s.startWeek && i <= s.endWeek)
                        {
                            detail.weight = s.weeklyDistributedWeight.HasValue ? s.weeklyDistributedWeight : 0;
                        }
                        resourceDetail.Add(detail);
                    }
                }

                s.resourceLoadingDetail = resourceDetail;
                result.Add(s);
            }

            var dataDrawing = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            foreach (resourceLoadingReport s in dataDrawing)
            {
                var documentGroup = GetDocumentGroupDesignerbyProjectId(s.docId.Value);

                if (documentGroup == constants.DOCUMENTTYPE_ENGINEER)
                {
                    s.planDurationWeek = s.planFinishDate.HasValue && s.planStartDate.HasValue ? Math.Ceiling((s.planFinishDate - s.planStartDate).Value.TotalDays / 7) : 0;
                    s.actualDurationWeek = s.actualFinishDate.HasValue && s.actualStartDate.HasValue ? Math.Ceiling((s.actualFinishDate - s.actualStartDate).Value.TotalDays / 7) : 0;
                    s.startWeek = !s.planDurationWeek.HasValue ? 0 : s.actualStartDate.HasValue ? (s.actualStartDate.Value.Year < param ? 1 : getDateOfWeek(s.actualStartDate.Value, dayOfWeek)) : (s.planStartDate.Value.Year < param ? 1 : getDateOfWeek(s.planStartDate.Value, dayOfWeek));
                    s.endWeek = !s.planDurationWeek.HasValue ? 0 : s.actualFinishDate.HasValue ? (s.actualFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.actualFinishDate.Value, dayOfWeek)) : (s.planFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.planFinishDate.Value, dayOfWeek));
                    //s.weeklyDistributedWeight = s.actualDurationWeek > 0 ? (s.estimateHours / s.actualDurationWeek) : s.planDurationWeek > 0 ? (s.estimateHours / s.planDurationWeek) : 0;
                    if (s.actualDurationWeek > 0)
                    {
                        s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                    }
                    else if (s.planDurationWeek > 0)
                    {
                        s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                    }
                    else {
                        s.weeklyDistributedWeight = 0;
                    }

                    List<resourceLoadingDetail> resourceDetail = new List<resourceLoadingDetail>();
                    if (s.startWeek > 0 && s.endWeek > 0)
                    {
                        for (var i = 1; i <= totalWeekOfYear; i++)
                        {
                            resourceLoadingDetail detail = new resourceLoadingDetail();
                            detail.week = i;
                            detail.weight = 0;
                            if (i >= s.startWeek && i <= s.endWeek)
                            {
                                detail.weight = s.weeklyDistributedWeight.HasValue ? s.weeklyDistributedWeight : 0;
                            }
                            resourceDetail.Add(detail);
                        }
                    }

                    s.resourceLoadingDetail = resourceDetail;
                    result.Add(s);
                }
            }


            List<int> listProjectId = result.Select(y => y.projectId).ToList();

            result.AddRange(db.PROJECTs.Where(w => listProjectId.Contains(w.OID)).Select(s => new resourceLoadingReport
            {
                projectId = s.OID,
                projectNo = s.PROJECTNO,
                ewpNo = s.PROJECTNO,
                dwgNo = s.PROJECTNO,
                deliverableNo = s.PROJECTNO,
                deliverableName = s.PROJECTDESCRIPTION,
                level = 0,
                projectType = s.PROJECTTYPE
            }));

            return result.OrderBy(o => o.projectNo).ThenBy(t => t.level == 0 ? null : t.ewpNo).ThenBy(t => t.dwgNo);
        }

        [HttpGet]
        [ActionName("DCResourceLoadingReport")]
        public object DCResourceLoadingReport(int param)
        {
            GENERALPARAMETER strDay = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.STARTWEEKLY).FirstOrDefault();
            DayOfWeek dayOfWeek;
            if (!Enum.TryParse<DayOfWeek>(strDay.VALUE, out dayOfWeek))
            {
                dayOfWeek = DayOfWeek.Monday;
            }

            int totalWeekOfYear = GetWeeksInYear(param, dayOfWeek);

            List<resourceLoadingReport> result = new List<resourceLoadingReport>();

            var dataEngineer = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            foreach (resourceLoadingReport s in dataEngineer)
            {
                s.planDurationWeek = s.planFinishDate.HasValue && s.planStartDate.HasValue ? Math.Ceiling((s.planFinishDate - s.planStartDate).Value.TotalDays / 7) : (double?)null;
                s.actualDurationWeek = s.actualFinishDate.HasValue && s.actualStartDate.HasValue ? Math.Ceiling((s.actualFinishDate - s.actualStartDate).Value.TotalDays / 7) : (double?)null;
                s.startWeek = !s.planDurationWeek.HasValue ? 0 : s.actualStartDate.HasValue ? (s.actualStartDate.Value.Year < param ? 1 : getDateOfWeek(s.actualStartDate.Value, dayOfWeek)) : (s.planStartDate.Value.Year < param ? 1 : getDateOfWeek(s.planStartDate.Value, dayOfWeek));
                s.endWeek = !s.planDurationWeek.HasValue ? 0 : s.actualFinishDate.HasValue ? (s.actualFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.actualFinishDate.Value, dayOfWeek)) : (s.planFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.planFinishDate.Value, dayOfWeek));
                //s.weeklyDistributedWeight = s.actualDurationWeek > 0 ? (s.estimateHours / s.actualDurationWeek) : s.planDurationWeek > 0 ? (s.estimateHours / s.planDurationWeek) : 0;

                if (s.actualDurationWeek > 0)
                {
                    s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                }
                else if (s.planDurationWeek > 0)
                {
                    s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                }
                else {
                    s.weeklyDistributedWeight = 0;
                }

                List<resourceLoadingDetail> resourceDetail = new List<resourceLoadingDetail>();
                if (s.startWeek > 0 && s.endWeek > 0)
                {
                    for (var i = 1; i <= totalWeekOfYear; i++)
                    {
                        resourceLoadingDetail detail = new resourceLoadingDetail();
                        detail.week = i;
                        detail.weight = 0;
                        if (i >= s.startWeek && i <= s.endWeek)
                        {
                            detail.weight = s.weeklyDistributedWeight.HasValue ? s.weeklyDistributedWeight : 0;
                        }
                        resourceDetail.Add(detail);
                    }
                }

                s.resourceLoadingDetail = resourceDetail;
                result.Add(s);
            }

            var dataDrawing = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            foreach (resourceLoadingReport s in dataDrawing)
            {
                var documentGroup = GetDocumentGroupDesignerbyProjectId(s.docId.Value);

                if (documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)
                {
                    s.planDurationWeek = s.planFinishDate.HasValue && s.planStartDate.HasValue ? Math.Ceiling((s.planFinishDate - s.planStartDate).Value.TotalDays / 7) : 0;
                    s.actualDurationWeek = s.actualFinishDate.HasValue && s.actualStartDate.HasValue ? Math.Ceiling((s.actualFinishDate - s.actualStartDate).Value.TotalDays / 7) : 0;
                    s.startWeek = !s.planDurationWeek.HasValue ? 0 : s.actualStartDate.HasValue ? (s.actualStartDate.Value.Year < param ? 1 : getDateOfWeek(s.actualStartDate.Value, dayOfWeek)) : (s.planStartDate.Value.Year < param ? 1 : getDateOfWeek(s.planStartDate.Value, dayOfWeek));
                    s.endWeek = !s.planDurationWeek.HasValue ? 0 : s.actualFinishDate.HasValue ? (s.actualFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.actualFinishDate.Value, dayOfWeek)) : (s.planFinishDate.Value.Year > param ? totalWeekOfYear : getDateOfWeek(s.planFinishDate.Value, dayOfWeek));
                    //s.weeklyDistributedWeight = s.actualDurationWeek > 0 ? (s.estimateHours / s.actualDurationWeek) : s.planDurationWeek > 0 ? (s.estimateHours / s.planDurationWeek) : 0;
                    if (s.actualDurationWeek > 0)
                    {
                        s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                    }
                    else if (s.planDurationWeek > 0)
                    {
                        s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                    }
                    else {
                        s.weeklyDistributedWeight = 0;
                    }

                    List<resourceLoadingDetail> resourceDetail = new List<resourceLoadingDetail>();
                    if (s.startWeek > 0 && s.endWeek > 0)
                    {
                        for (var i = 1; i <= totalWeekOfYear; i++)
                        {
                            resourceLoadingDetail detail = new resourceLoadingDetail();
                            detail.week = i;
                            detail.weight = 0;
                            if (i >= s.startWeek && i <= s.endWeek)
                            {
                                detail.weight = s.weeklyDistributedWeight.HasValue ? s.weeklyDistributedWeight : 0;
                            }
                            resourceDetail.Add(detail);
                        }
                    }

                    s.resourceLoadingDetail = resourceDetail;
                    result.Add(s);
                }
            }


            List<int> listProjectId = result.Select(y => y.projectId).ToList();

            result.AddRange(db.PROJECTs.Where(w => listProjectId.Contains(w.OID)).Select(s => new resourceLoadingReport
            {
                projectId = s.OID,
                projectNo = s.PROJECTNO,
                ewpNo = s.PROJECTNO,
                dwgNo = s.PROJECTNO,
                deliverableNo = s.PROJECTNO,
                deliverableName = s.PROJECTDESCRIPTION,
                level = 0,
                projectType = s.PROJECTTYPE
            }));

            return result.OrderBy(o => o.projectNo).ThenBy(t => t.level == 0 ? null : t.ewpNo).ThenBy(t => t.dwgNo);
        }

        protected static double? inRangeMonthResourceLoading(int month, int startWeek, int endWeek, double? value)
        {
            return month >= startWeek && month <= endWeek ? value.HasValue ? value : 0 : 0;
        }

        protected IEnumerable<summaryResourceLoading> reformatSummaryResourceLoading(int yearParam, IEnumerable<resourceLoadingReport> documentParam, int totalWeekOfYear, DayOfWeek dayOfWeek, string typeGroupName, string documentGroup)
        {
            List<summaryResourceLoading> getData = new List<summaryResourceLoading>();

            foreach (resourceLoadingReport s in documentParam)
            {
                if (documentGroup == GetDocumentGroupDesignerbyProjectId(s.docId.Value))
                {
                    s.planDurationWeek = s.planFinishDate.HasValue && s.planStartDate.HasValue ? Math.Ceiling((s.planFinishDate - s.planStartDate).Value.TotalDays / 7) : (double?)null;
                    s.actualDurationWeek = s.actualFinishDate.HasValue && s.actualStartDate.HasValue ? Math.Ceiling((s.actualFinishDate - s.actualStartDate).Value.TotalDays / 7) : (double?)null;
                    s.startWeek = !s.planDurationWeek.HasValue ? 0 : s.actualStartDate.HasValue ? (s.actualStartDate.Value.Year < yearParam ? 1 : getDateOfWeek(s.actualStartDate.Value, dayOfWeek)) : (s.planStartDate.Value.Year < yearParam ? 1 : getDateOfWeek(s.planStartDate.Value, dayOfWeek));
                    s.endWeek = !s.planDurationWeek.HasValue ? 0 : s.actualFinishDate.HasValue ? (s.actualFinishDate.Value.Year > yearParam ? totalWeekOfYear : getDateOfWeek(s.actualFinishDate.Value, dayOfWeek)) : (s.planFinishDate.Value.Year > yearParam ? totalWeekOfYear : getDateOfWeek(s.planFinishDate.Value, dayOfWeek));
                    //s.weeklyDistributedWeight = s.actualDurationWeek > 0 ? (s.estimateHours / s.actualDurationWeek) : s.planDurationWeek > 0 ? (s.estimateHours / s.planDurationWeek) : 0;

                    if (s.actualDurationWeek > 0)
                    {
                        s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                    }
                    else if (s.planDurationWeek > 0)
                    {
                        s.weeklyDistributedWeight = s.estimateHours / (s.endWeek - s.startWeek + 1);
                    }
                    else {
                        s.weeklyDistributedWeight = 0;
                    }

                    summaryResourceLoading data = new summaryResourceLoading();
                    data.employeeId = s.picId;
                    data.name = s.pic;
                    data.typeGroupId = listTypeGroup.IndexOf(typeGroupName);
                    data.typeGroupName = typeGroupName;
                    data.week1 = inRangeMonthResourceLoading(1, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week2 = inRangeMonthResourceLoading(2, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week3 = inRangeMonthResourceLoading(3, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week4 = inRangeMonthResourceLoading(4, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week5 = inRangeMonthResourceLoading(5, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week6 = inRangeMonthResourceLoading(6, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week7 = inRangeMonthResourceLoading(7, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week8 = inRangeMonthResourceLoading(8, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week9 = inRangeMonthResourceLoading(9, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week10 = inRangeMonthResourceLoading(10, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week11 = inRangeMonthResourceLoading(11, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week12 = inRangeMonthResourceLoading(12, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week13 = inRangeMonthResourceLoading(13, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week14 = inRangeMonthResourceLoading(14, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week15 = inRangeMonthResourceLoading(15, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week16 = inRangeMonthResourceLoading(16, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week17 = inRangeMonthResourceLoading(17, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week18 = inRangeMonthResourceLoading(18, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week19 = inRangeMonthResourceLoading(19, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week20 = inRangeMonthResourceLoading(20, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week21 = inRangeMonthResourceLoading(21, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week22 = inRangeMonthResourceLoading(22, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week23 = inRangeMonthResourceLoading(23, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week24 = inRangeMonthResourceLoading(24, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week25 = inRangeMonthResourceLoading(25, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week26 = inRangeMonthResourceLoading(26, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week27 = inRangeMonthResourceLoading(27, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week28 = inRangeMonthResourceLoading(28, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week29 = inRangeMonthResourceLoading(29, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week30 = inRangeMonthResourceLoading(30, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week31 = inRangeMonthResourceLoading(31, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week32 = inRangeMonthResourceLoading(32, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week33 = inRangeMonthResourceLoading(33, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week34 = inRangeMonthResourceLoading(34, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week35 = inRangeMonthResourceLoading(35, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week36 = inRangeMonthResourceLoading(36, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week37 = inRangeMonthResourceLoading(37, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week38 = inRangeMonthResourceLoading(38, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week39 = inRangeMonthResourceLoading(39, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week40 = inRangeMonthResourceLoading(40, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week41 = inRangeMonthResourceLoading(41, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week42 = inRangeMonthResourceLoading(42, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week43 = inRangeMonthResourceLoading(43, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week44 = inRangeMonthResourceLoading(44, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week45 = inRangeMonthResourceLoading(45, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week46 = inRangeMonthResourceLoading(46, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week47 = inRangeMonthResourceLoading(47, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week48 = inRangeMonthResourceLoading(48, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week49 = inRangeMonthResourceLoading(49, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week50 = inRangeMonthResourceLoading(50, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week51 = inRangeMonthResourceLoading(51, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week52 = inRangeMonthResourceLoading(52, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);
                    data.week53 = inRangeMonthResourceLoading(53, s.startWeek.Value, s.endWeek.Value, s.weeklyDistributedWeight);

                    getData.Add(data);
                }
            }
            return getData;
        }

        //generateDataSummaryWorkResoureLoading
        protected IEnumerable<summaryResourceLoading> generateDataSummaryWorkResoureLoading(IEnumerable<employeeObject> listEnginneer, IEnumerable<summaryResourceLoading> documentParam, string typeGroupName, int? defaultValue)
        {
            var result = documentParam.GroupBy(g => new { g.employeeId }).Select(s => new summaryResourceLoading
            {
                employeeId = s.Key.employeeId,
                name = s.Max(max => max.name),
                typeGroupId = listTypeGroup.IndexOf(typeGroupName),
                typeGroupName = typeGroupName,
                week1 = s.Sum(sum => sum.week1),
                week2 = s.Sum(sum => sum.week2),
                week3 = s.Sum(sum => sum.week3),
                week4 = s.Sum(sum => sum.week4),
                week5 = s.Sum(sum => sum.week5),
                week6 = s.Sum(sum => sum.week6),
                week7 = s.Sum(sum => sum.week7),
                week8 = s.Sum(sum => sum.week8),
                week9 = s.Sum(sum => sum.week9),
                week10 = s.Sum(sum => sum.week10),
                week11 = s.Sum(sum => sum.week11),
                week12 = s.Sum(sum => sum.week12),
                week13 = s.Sum(sum => sum.week13),
                week14 = s.Sum(sum => sum.week14),
                week15 = s.Sum(sum => sum.week15),
                week16 = s.Sum(sum => sum.week16),
                week17 = s.Sum(sum => sum.week17),
                week18 = s.Sum(sum => sum.week18),
                week19 = s.Sum(sum => sum.week19),
                week20 = s.Sum(sum => sum.week20),
                week21 = s.Sum(sum => sum.week21),
                week22 = s.Sum(sum => sum.week22),
                week23 = s.Sum(sum => sum.week23),
                week24 = s.Sum(sum => sum.week24),
                week25 = s.Sum(sum => sum.week25),
                week26 = s.Sum(sum => sum.week26),
                week27 = s.Sum(sum => sum.week27),
                week28 = s.Sum(sum => sum.week28),
                week29 = s.Sum(sum => sum.week29),
                week30 = s.Sum(sum => sum.week30),
                week31 = s.Sum(sum => sum.week31),
                week32 = s.Sum(sum => sum.week32),
                week33 = s.Sum(sum => sum.week33),
                week34 = s.Sum(sum => sum.week34),
                week35 = s.Sum(sum => sum.week35),
                week36 = s.Sum(sum => sum.week36),
                week37 = s.Sum(sum => sum.week37),
                week38 = s.Sum(sum => sum.week38),
                week39 = s.Sum(sum => sum.week39),
                week40 = s.Sum(sum => sum.week40),
                week41 = s.Sum(sum => sum.week41),
                week42 = s.Sum(sum => sum.week42),
                week43 = s.Sum(sum => sum.week43),
                week44 = s.Sum(sum => sum.week44),
                week45 = s.Sum(sum => sum.week45),
                week46 = s.Sum(sum => sum.week46),
                week47 = s.Sum(sum => sum.week47),
                week48 = s.Sum(sum => sum.week48),
                week49 = s.Sum(sum => sum.week49),
                week50 = s.Sum(sum => sum.week50),
                week51 = s.Sum(sum => sum.week51),
                week52 = s.Sum(sum => sum.week52),
                week53 = s.Sum(sum => sum.week53)
            });

            return listEnginneer.GroupJoin(result, employee => employee.employeeId, summary => summary.employeeId, (x, y) => new { employee = x, summary = y }).SelectMany(
                listEmpSummary => listEmpSummary.summary.DefaultIfEmpty(),
                (x, y) => new summaryResourceLoading
                {
                    employeeId = x.employee.employeeId,
                    name = x.employee.full_Name,
                    typeGroupId = listTypeGroup.IndexOf(typeGroupName),
                    typeGroupName = typeGroupName,
                    week1 = y != null ? y.week1 : defaultValue,
                    week2 = y != null ? y.week2 : defaultValue,
                    week3 = y != null ? y.week3 : defaultValue,
                    week4 = y != null ? y.week4 : defaultValue,
                    week5 = y != null ? y.week5 : defaultValue,
                    week6 = y != null ? y.week6 : defaultValue,
                    week7 = y != null ? y.week7 : defaultValue,
                    week8 = y != null ? y.week8 : defaultValue,
                    week9 = y != null ? y.week9 : defaultValue,
                    week10 = y != null ? y.week10 : defaultValue,
                    week11 = y != null ? y.week11 : defaultValue,
                    week12 = y != null ? y.week12 : defaultValue,
                    week13 = y != null ? y.week13 : defaultValue,
                    week14 = y != null ? y.week14 : defaultValue,
                    week15 = y != null ? y.week15 : defaultValue,
                    week16 = y != null ? y.week16 : defaultValue,
                    week17 = y != null ? y.week17 : defaultValue,
                    week18 = y != null ? y.week18 : defaultValue,
                    week19 = y != null ? y.week19 : defaultValue,
                    week20 = y != null ? y.week20 : defaultValue,
                    week21 = y != null ? y.week21 : defaultValue,
                    week22 = y != null ? y.week22 : defaultValue,
                    week23 = y != null ? y.week23 : defaultValue,
                    week24 = y != null ? y.week24 : defaultValue,
                    week25 = y != null ? y.week25 : defaultValue,
                    week26 = y != null ? y.week26 : defaultValue,
                    week27 = y != null ? y.week27 : defaultValue,
                    week28 = y != null ? y.week28 : defaultValue,
                    week29 = y != null ? y.week29 : defaultValue,
                    week30 = y != null ? y.week30 : defaultValue,
                    week31 = y != null ? y.week31 : defaultValue,
                    week32 = y != null ? y.week32 : defaultValue,
                    week33 = y != null ? y.week33 : defaultValue,
                    week34 = y != null ? y.week34 : defaultValue,
                    week35 = y != null ? y.week35 : defaultValue,
                    week36 = y != null ? y.week36 : defaultValue,
                    week37 = y != null ? y.week37 : defaultValue,
                    week38 = y != null ? y.week38 : defaultValue,
                    week39 = y != null ? y.week39 : defaultValue,
                    week40 = y != null ? y.week40 : defaultValue,
                    week41 = y != null ? y.week41 : defaultValue,
                    week42 = y != null ? y.week42 : defaultValue,
                    week43 = y != null ? y.week43 : defaultValue,
                    week44 = y != null ? y.week44 : defaultValue,
                    week45 = y != null ? y.week45 : defaultValue,
                    week46 = y != null ? y.week46 : defaultValue,
                    week47 = y != null ? y.week47 : defaultValue,
                    week48 = y != null ? y.week48 : defaultValue,
                    week49 = y != null ? y.week49 : defaultValue,
                    week50 = y != null ? y.week50 : defaultValue,
                    week51 = y != null ? y.week51 : defaultValue,
                    week52 = y != null ? y.week52 : defaultValue,
                    week53 = y != null ? y.week53 : defaultValue
                });
        }

        [HttpGet]
        [ActionName("SummaryWorkResourcesLoadingReport")]
        public object SummaryWorkResourcesLoadingReport(int param)
        {
            GENERALPARAMETER strDay = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.STARTWEEKLY).FirstOrDefault();
            DayOfWeek dayOfWeek;
            if (!Enum.TryParse<DayOfWeek>(strDay.VALUE, out dayOfWeek))
            {
                dayOfWeek = DayOfWeek.Monday;
            }

            int totalWeekOfYear = GetWeeksInYear(param, dayOfWeek);

            var dataEngineer = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                picId = s.DOCUMENTBYID,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                    s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                        null :
                        (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                            null :
                            db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            var dataDrawing = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                picId = s.DOCUMENTBYID,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            IEnumerable<summaryResourceLoading> getDataPlanEWP = reformatSummaryResourceLoading(param, dataEngineer.Union(dataDrawing), totalWeekOfYear, dayOfWeek, "PLAN EWP", constants.DOCUMENTTYPE_ENGINEER);

            var dataDC = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                picId = s.DOCUMENTBYID,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                   s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                       null :
                       (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                           null :
                           db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                   s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                       null :
                       (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                           null :
                           db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            var dataDrawingDC = db.PROJECTDOCUMENTs.Where(w => w.PROJECTID.HasValue && (w.PLANFINISHDATE.HasValue ? w.PLANFINISHDATE.Value.Year == param : 1 != 1 || w.ACTUALFINISHDATE.HasValue ? w.ACTUALFINISHDATE.Value.Year == param : 1 != 1) && (w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN)).Select(s => new resourceLoadingReport
            {
                projectId = s.PROJECT.OID,
                projectNo = s.PROJECT.PROJECTNO,
                docId = s.OID,
                ewpNo = s.DOCNO,
                dwgNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                deliverableNo = s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? s.DRAWINGNO != null ? s.DRAWINGNO : s.DOCNO : s.DOCNO,
                level = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ? 1 : s.DOCTYPE == constants.DOCUMENTTYPE_DESIGN ? 2 : 3,
                deliverableName = s.DRAWINGTITLE != null ? s.DRAWINGTITLE : s.DOCTITLE,
                projectType = s.PROJECT.PROJECTTYPE,
                docType = s.DOCTYPE,
                picId = s.DOCUMENTBYID,
                pic = s.DOCUMENTBYNAME,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                category = s.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER ?
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionEngineer && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER)).EMPLOYEEPOSITION).TEXT) :
                                s.PROJECT.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).Count() == 0 ?
                                    null :
                                    (db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION) == null ?
                                        null :
                                        db.LOOKUPs.FirstOrDefault(l => l.TYPE == constants.positionDesigner && l.VALUE == s.PROJECT.USERASSIGNMENTs.FirstOrDefault(w => w.EMPLOYEEID == s.DOCUMENTBYID && (w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER)).EMPLOYEEPOSITION).TEXT),
                estimateHours = s.PROJECTDOCUMENTWEIGHTHOURS.Sum(sum => sum.ACTUALHOURS)
            });

            IEnumerable<summaryResourceLoading> getDataPlanDC = reformatSummaryResourceLoading(param, dataDC.Union(dataDrawingDC), totalWeekOfYear, dayOfWeek, "PLAN EWP", constants.DOCUMENTTYPE_ENGINEER);

            var listEnginneer = getDataPlanEWP.Union(getDataPlanDC).GroupBy(g => g.employeeId).Select(s => new employeeObject
            {
                employeeId = s.Key,
                full_Name = s.Max(max => max.name)
            }).Distinct();

            IEnumerable<summaryResourceLoading> notValue = new List<summaryResourceLoading>();

            return generateDataSummaryWorkResoureLoading(listEnginneer, getDataPlanEWP, "PLAN EWP", 0).
                Union(generateDataSummaryWorkResoureLoading(listEnginneer, notValue, "ACTUAL EWP", (int?)null)).
                Union(generateDataSummaryWorkResoureLoading(listEnginneer, getDataPlanDC, "PLAN DC", 0)).
                Union(generateDataSummaryWorkResoureLoading(listEnginneer, notValue, "ACTUAL DC", (int?)null)).
                Union(generateDataSummaryWorkResoureLoading(listEnginneer, getDataPlanEWP.Union(getDataPlanDC), "TOTAL PLAN", 0)).
                Union(generateDataSummaryWorkResoureLoading(listEnginneer, notValue, "TOTAL ACTUAL", 0)).
                OrderBy(o => o.employeeId).ThenBy(t => t.typeGroupId);
        }

    }
}


public partial class parameterReport
{
    public string projectNo { get; set; }
    public string projectDesc { get; set; }
    public string peParam { get; set; }
    public Nullable<System.DateTime> from { get; set; }
    public Nullable<System.DateTime> to { get; set; }
    public bool? overDue { get; set; }
}

public partial class ewpPerformanceReport
{
    public int projectId { get; set; }
    public string projectNo { get; set; }
    public string ewpNo { get; set; }
    public string dwgNo { get; set; }
    public string deliverableNo { get; set; }
    public string deliverableName { get; set; }
    public int level { get; set; }
    public string projectType { get; set; }
    public string docType { get; set; }
    public string pic { get; set; }
    public string category { get; set; }
    public DateTime? planStartDate { get; set; }
    public DateTime? planFinishDate { get; set; }
    public DateTime? actualStartDate { get; set; }
    public DateTime? actualFinishDate { get; set; }
    public double? deliverableWeightHour { get; set; }
    public double? baseLineHours { get; set; }
    public double? deliverableWeightPercent { get; set; }
    public double? resourcesFactor { get; set; }
    public double? factoredWeight { get; set; }
    public double? finalResult { get; set; }
    public string performanceFormula { get; set; }
    public double? performance { get; set; }
    public double? achievement { get; set; }
    public string remark { get; set; }
}

public partial class individualReportEngineer
{
    public int? monthInt { get; set; }
    public string monthName { get; set; }
    public int countPlanEWP { get; set; }
    public int countActualEWP { get; set; }
    public double deliverableWeightHour { get; set; }
    public double? deliverableWeightPercent { get; set; }
    public double planScoreEWP { get; set; }
    public double planColumnParameterEWP { get; set; }
    public double planScoreCummEWP { get; set; }
    public double actualAchievementEWP { get; set; }
    public double actualScoreCummEWP { get; set; }
    public int countPlanConformity { get; set; }
    public int countActualConformity { get; set; }
    public double planScoreConformity { get; set; }
    public double planColumnParameterConformity { get; set; }
    public double planScoreCummConformity { get; set; }
    public double actualAchievementConformity { get; set; }
    public double actualScoreCummConformity { get; set; }
    public double summaryPlanScoreCumm
    {
        get
        {
            return planScoreCummEWP + planScoreCummConformity;
        }
    }
    public double summaryActuaScoreCumm
    {
        get
        {
            return actualScoreCummEWP + actualScoreCummConformity;
        }
    }

}
public partial class summaryReportEngineer
{
    public string badgeNo { get; set; }
    public string name { get; set; }
    public string section { get; set; }
    public double totalEWP { get; set; }
    public int totalEWPCompleted { get; set; }
    public int totalEWPBackLog { get; set; }
    public int countActualEWP { get; set; }
    public double deliverableWeightHour { get; set; }
    public double? deliverableWeightPercent { get; set; }
    public double planScoreEWP { get; set; }
    public double planColumnParameterEWP { get; set; }
    public double planScoreCummEWP { get; set; }
    public double actualAchievementEWP { get; set; }
    public double actualScoreCummEWP { get; set; }
    public int totalConformity { get; set; }
    public int totalConformityCompleted { get; set; }
    public int totalConformityBackLog { get; set; }
    public double planScoreConformity { get; set; }
    public double planColumnParameterConformity { get; set; }
    public double planScoreCummConformity { get; set; }
    public double actualAchievementConformity { get; set; }
    public double actualScoreCummConformity { get; set; }
    public DateTime? tempActualFinish { get; set; }
    public double? tempSumPDWActualHour { get; set; }
    public double summaryTotalWeight
    {
        get
        {
            return planScoreEWP + planScoreConformity;
        }
    }

    public double summaryTotalScore
    {
        get
        {
            return actualAchievementEWP + actualAchievementConformity;
        }
    }
}
    public partial class ewpDrawingReport
{
    public int projectId { get; set; }
    public string ewpNo { get; set; }
    public string dwgNo { get; set; }
    public int? docId { get; set; }
    public string projectNo { get; set; }
    public string projectType { get; set; }
    public string deliverableNo { get; set; }
    public string deliverableName { get; set; }
    public DateTime? planStartDate { get; set; }
    public DateTime? planFinishDate { get; set; }
    public DateTime? actualStartDate { get; set; }
    public DateTime? actualFinishDate { get; set; }
    public int level { get; set; }
    public string docType { get; set; }
    public string pic { get; set; }
    public string category { get; set; }
    public double? participation { get; set; }
    public IEnumerable<projectDocumentDrawingTypeObject> typeOfDrawing { get; set; }
    public IEnumerable<projectDocumentComplexityObject> complexity { get; set; }
    public double? totalProduction { get; set; }
}

public partial class designConformityFinalResultReport
{
    public int projectId { get; set; }
    public int? docId { get; set; }
    public string documentGroup { get; set; }
    public string ewpNo { get; set; }
    public string dwgNo { get; set; }
    public string projectNo { get; set; }
    public string projectType { get; set; }
    public string deliverableNo { get; set; }
    public string deliverableName { get; set; }
    public DateTime? planStartDate { get; set; }
    public DateTime? planFinishDate { get; set; }
    public DateTime? actualStartDate { get; set; }
    public DateTime? actualFinishDate { get; set; }
    public string remarksActualResults { get; set; }
    public string remarksDesignKpi { get; set; }
    public int level { get; set; }
    public string docType { get; set; }
    public string pic { get; set; }
    public string category { get; set; }
    public double? deliverableWeightHour { get; set; }
    public double? deliverableWeightPercent { get; set; }
    public double? resourcesFactor { get; set; }
    public double? baseLineHours { get; set; }
    public IEnumerable<projectDocumentFinalResultObject> projectDocumentFinalResult { get; set; }
    public double? finalResult { get; set; }
    public double? performance { get; set; }
    public double? factoredWeight { get; set; }
    public double? achievement { get; set; }
}

public partial class resourceLoadingReport
{
    public int projectId { get; set; }
    public string ewpNo { get; set; }
    public string dwgNo { get; set; }
    public int? docId { get; set; }
    public string projectNo { get; set; }
    public string projectType { get; set; }
    public string deliverableNo { get; set; }
    public string deliverableName { get; set; }
    public DateTime? planStartDate { get; set; }
    public DateTime? planFinishDate { get; set; }
    public DateTime? actualStartDate { get; set; }
    public DateTime? actualFinishDate { get; set; }
    public int level { get; set; }
    public string docType { get; set; }
    public string picId { get; set; }
    public string pic { get; set; }
    public string category { get; set; }
    public double? estimateHours { get; set; }
    public double? planDurationWeek { get; set; }
    public double? actualDurationWeek { get; set; }
    public int? startWeek { get; set; }
    public int? endWeek { get; set; }
    public double? weeklyDistributedWeight { get; set; }
    public IEnumerable<resourceLoadingDetail> resourceLoadingDetail { get; set; }
}

public partial class resourceLoadingDetail
{
    public int week { get; set; }
    public double? weight { get; set; }
}

public partial class summaryResourceLoading
{
    public string employeeId { get; set; }
    public string name { get; set; }
    public int typeGroupId { get; set; }
    public string typeGroupName { get; set; }
    public double? week1 { get; set; }
    public double? week2 { get; set; }
    public double? week3 { get; set; }
    public double? week4 { get; set; }
    public double? week5 { get; set; }
    public double? week6 { get; set; }
    public double? week7 { get; set; }
    public double? week8 { get; set; }
    public double? week9 { get; set; }
    public double? week10 { get; set; }
    public double? week11 { get; set; }
    public double? week12 { get; set; }
    public double? week13 { get; set; }
    public double? week14 { get; set; }
    public double? week15 { get; set; }
    public double? week16 { get; set; }
    public double? week17 { get; set; }
    public double? week18 { get; set; }
    public double? week19 { get; set; }
    public double? week20 { get; set; }
    public double? week21 { get; set; }
    public double? week22 { get; set; }
    public double? week23 { get; set; }
    public double? week24 { get; set; }
    public double? week25 { get; set; }
    public double? week26 { get; set; }
    public double? week27 { get; set; }
    public double? week28 { get; set; }
    public double? week29 { get; set; }
    public double? week30 { get; set; }
    public double? week31 { get; set; }
    public double? week32 { get; set; }
    public double? week33 { get; set; }
    public double? week34 { get; set; }
    public double? week35 { get; set; }
    public double? week36 { get; set; }
    public double? week37 { get; set; }
    public double? week38 { get; set; }
    public double? week39 { get; set; }
    public double? week40 { get; set; }
    public double? week41 { get; set; }
    public double? week42 { get; set; }
    public double? week43 { get; set; }
    public double? week44 { get; set; }
    public double? week45 { get; set; }
    public double? week46 { get; set; }
    public double? week47 { get; set; }
    public double? week48 { get; set; }
    public double? week49 { get; set; }
    public double? week50 { get; set; }
    public double? week51 { get; set; }
    public double? week52 { get; set; }
    public double? week53 { get; set; }
}