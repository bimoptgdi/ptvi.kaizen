﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterPerformanceValueEWPController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/masterPerformanceValueEWP
        public IQueryable<masterPerformanceValueEWPObject> GetMASTERPERFORMANCEVALUEEWPs()
        {
            return db.MASTERCOMPLIANCEPERFORMANCEs.Select(s => new masterPerformanceValueEWPObject
            {
                id = s.OID,
                year = s.YEAR,
                referenceCode = s.REFERENCECODE,
                performanceValue = s.PERFORMANCEVALUE,
                min = s.MINRANGE,
                max = s.MAXRANGE
            });
        }
        // GET api/masterPerformanceValueEWP/cekDuplicateMinMax/ (for min max)
        [HttpGet]
        [ActionName("cekDuplicateMinMax")]
        public Boolean cekDuplicateMinMax(float param, float maxParam, int yearParam, string referenceParam, int objectId)
        {
            var duplicateMinMax = db.MASTERCOMPLIANCEPERFORMANCEs.Where(w => (w.YEAR == yearParam && w.REFERENCECODE == "EWP" && w.OID != objectId) && ((param < w.MINRANGE && maxParam < w.MAXRANGE) || (param > w.MINRANGE && maxParam > w.MAXRANGE)));
            if (duplicateMinMax.Count() == db.MASTERCOMPLIANCEPERFORMANCEs.Where(w => w.YEAR == yearParam && w.REFERENCECODE == referenceParam && w.OID != objectId).Count())
            {
                return false;
            }
            else
                return true;
        }
        // GET api/masterPerformanceValueEWP/getByYear/param (filter by year)
        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterPerformanceValueEWPObject> getByYear(int param)
        {
            return db.MASTERCOMPLIANCEPERFORMANCEs.Where(w => w.YEAR == param && w.REFERENCECODE == "EWP").Select(s => new masterPerformanceValueEWPObject
            {
                id = s.OID,
                year = s.YEAR,
                referenceCode = s.REFERENCECODE,
                performanceValue = s.PERFORMANCEVALUE,
                min = s.MINRANGE,
                max = s.MAXRANGE
            });
        }

        // POST api/masterPerformanceValueEWP/addPerformanceValue/1
        [HttpPost]
        [ActionName("addPerformanceValue")]
        public IHttpActionResult addPerformanceValue(string param, masterPerformanceValueEWPObject performanceValueParam)
        {

            MASTERCOMPLIANCEPERFORMANCE add = new MASTERCOMPLIANCEPERFORMANCE();
            add.MINRANGE = performanceValueParam.min;
            add.MAXRANGE = performanceValueParam.max;
            add.YEAR = performanceValueParam.year;
            add.REFERENCECODE = performanceValueParam.referenceCode;
            add.PERFORMANCEVALUE = performanceValueParam.performanceValue;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERCOMPLIANCEPERFORMANCEs.Add(add);
            db.SaveChanges();

            performanceValueParam.id = add.OID;
            return Ok(performanceValueParam);
        }

        // PUT api/masterPerformanceValueEWP/updatePerformanceValue/
        [HttpPut]
        [ActionName("updatePerformanceValue")]
        public IHttpActionResult updatePerformanceValue(int param, masterPerformanceValueEWPObject performanceValueParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != performanceValueParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERCOMPLIANCEPERFORMANCE update = db.MASTERCOMPLIANCEPERFORMANCEs.Find(param);
                update.YEAR = performanceValueParam.year;
                update.REFERENCECODE = performanceValueParam.referenceCode;
                update.PERFORMANCEVALUE = performanceValueParam.performanceValue;
                update.MINRANGE = performanceValueParam.min;
                update.MAXRANGE = performanceValueParam.max;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(performanceValueParam);
        }
        // GET api/masterPerformanceValueEWP/getMaxMinPerformanceValue/1

        [HttpGet]
        [ActionName("getMaxMinPerformanceValue")]
        public object getMaxMinPerformanceValue(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERCOMPLIANCEPERFORMANCEs.Min(w => w.YEAR);
            var max = db.MASTERCOMPLIANCEPERFORMANCEs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

    }
}