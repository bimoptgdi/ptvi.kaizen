﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class FileIntegrationController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/FileIntegration
        public IQueryable<FILEINTEGRATION> GetFILEINTEGRATIONs()
        {
            return db.FILEINTEGRATIONs;
        }

        // GET: api/FileIntegration/5
        [ResponseType(typeof(FILEINTEGRATION))]
        public IHttpActionResult GetFILEINTEGRATION(int id)
        {
            FILEINTEGRATION fILEINTEGRATION = db.FILEINTEGRATIONs.Find(id);
            if (fILEINTEGRATION == null)
            {
                return NotFound();
            }

            return Ok(fILEINTEGRATION);
        }

        // PUT: api/FileIntegration/5
        [HttpPut]
        [ActionName("fileIntegrationUpdate")]
        public IHttpActionResult fileIntegrationUpdate(int param, fileIntegrationObject fileIntegrationParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (param != fileIntegrationParam.id)
            {
                return BadRequest();
            }
            
            try
            {
                FILEINTEGRATION fileIntegration = db.FILEINTEGRATIONs.Find(param);
                fileIntegration.PURPOSEFILE = fileIntegrationParam.purposeFile;
                fileIntegration.FILENAME = fileIntegrationParam.fileName;
                fileIntegration.SHAREFOLDER = fileIntegrationParam.shareFolder;
                fileIntegration.UPDATEDBY = GetCurrentNTUserId();
                fileIntegration.UPDATEDDATE = DateTime.Now;
                db.Entry(fileIntegration).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FILEINTEGRATIONExists(param))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(GetFileById(param));
        }

        // POST: api/FileIntegration
        [ResponseType(typeof(FILEINTEGRATION))]
        public IHttpActionResult PostFILEINTEGRATION(FILEINTEGRATION fILEINTEGRATION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FILEINTEGRATIONs.Add(fILEINTEGRATION);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = fILEINTEGRATION.OID }, fILEINTEGRATION);
        }

        // DELETE: api/FileIntegration/5
        [ResponseType(typeof(FILEINTEGRATION))]
        public IHttpActionResult DeleteFILEINTEGRATION(int id)
        {
            FILEINTEGRATION fILEINTEGRATION = db.FILEINTEGRATIONs.Find(id);
            if (fILEINTEGRATION == null)
            {
                return NotFound();
            }

            db.FILEINTEGRATIONs.Remove(fILEINTEGRATION);
            db.SaveChanges();

            return Ok(fILEINTEGRATION);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FILEINTEGRATIONExists(int id)
        {
            return db.FILEINTEGRATIONs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("GetFileByType")]
        public IEnumerable<fileIntegrationObject> GetFileByType(string param)
        {
            IEnumerable<fileIntegrationObject> fileObject = db.FILEINTEGRATIONs.Where(x => x.PURPOSETYPE == param).
                Select(y => new fileIntegrationObject {
                    id = y.OID,
                    purposeType = y.PURPOSETYPE,
                    purposeFile = y.PURPOSEFILE,
                    fileName = y.FILENAME,
                    shareFolder = y.SHAREFOLDER,
                    toolTipDef = db.LOOKUPs.Where(x=>x.TYPE==constants.fileIntegrationToolTipDef && x.VALUE== y.PURPOSETYPE+y.PURPOSEFILE).FirstOrDefault().TEXT,
                    createdDate = y.CREATEDDATE,
                    createdBy = y.CREATEDBY,
                    updatedDate = y.UPDATEDDATE,
                    updatedBy = y.UPDATEDBY
                });
            return fileObject;
        }

        [HttpGet]
        [ActionName("GetFileById")]
        public fileIntegrationObject GetFileById(int param)
        {
            fileIntegrationObject fileObject = db.FILEINTEGRATIONs.Where(x => x.OID == param).
                Select(y => new fileIntegrationObject
                {
                    id = y.OID,
                    purposeType = y.PURPOSETYPE,
                    purposeFile = y.PURPOSEFILE,
                    fileName = y.FILENAME,
                    shareFolder = y.SHAREFOLDER,
                    createdDate = y.CREATEDDATE,
                    createdBy = y.CREATEDBY,
                    updatedDate = y.UPDATEDDATE,
                    updatedBy = y.UPDATEDBY
                }).FirstOrDefault();
            return fileObject;
        }
    }
}