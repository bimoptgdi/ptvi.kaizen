﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class CloseOutController : KAIZENController
    {
        public class coToEmailList : taskToEmailList
        {
            public string PROJECT { get; set; }
            public new List<string> FIELDLIST
            {
                get
                {
                return new List<string>
                {
                    "PIC",
                    "PROJECTNO",
                    "PROJECTDESC",
                    "DUEDATE",
                    "DAYSTODUE",
                };
            }
        }
        }
            public class objRole
        {
            public int id { get; set; }
            public int seq { get; set; }
            public string descript { get; set; }
            public int seqApp { get; set; }
        }
        private List<objRole> getCurRole(PROJECTCLOSEOUT co)
        {
            var currRole = co.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue && w.SETUPCLOSEOUTROLE.ISACTIVE == true && w.SETUPCLOSEOUTROLE.SEQAPPROVAL == (co.CLOSEOUTPROGRESSes.Where(w1 => !w1.SUBMITEDDATE.HasValue && w1.SETUPCLOSEOUTROLE.ISACTIVE == true).Min(m => m.SETUPCLOSEOUTROLE.SEQAPPROVAL))).Select(w => w.SETUPCLOSEOUTROLE).Select(w => new objRole { id = w.OID, seq = w.SEQ.Value, descript = w.DESCRIPTION, seqApp = w.SEQAPPROVAL.Value }).ToList();

            if (currRole.SingleOrDefault(x => x.descript == constants.MC) != null && currRole.SingleOrDefault(x => x.descript == constants.PC) != null) {
                currRole.Remove(currRole.SingleOrDefault(x => x.descript == constants.PC));
            }

            return currRole;
        }

        private string getPicNameFromRole(int projectId, int roleId)
        {
            var picName = "";
            var sCORole = db.SETUPCLOSEOUTROLEs.Where(s => s.OID == roleId).FirstOrDefault();
            if (sCORole.ASSIGNMENTTYPE != null)
            {
                if (sCORole.ASSIGNMENTTYPE == constants.projectManager)
                    picName = db.PROJECTs.Where(p => p.OID == projectId).FirstOrDefault().PROJECTMANAGERNAME;
                else if (sCORole.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER)
                {
                    var pic = db.PROJECTs.FirstOrDefault(p => p.OID == projectId).USERASSIGNMENTs.Where(g => g.ISACTIVE == true && g.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER).Select(d => d.EMPLOYEENAME).ToArray() ;
                    picName = string.Join(",",pic);
                }
                else if (sCORole.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                {
                    var pic = db.PROJECTs.FirstOrDefault(p => p.OID == projectId).USERASSIGNMENTs.Where(g => g.ISACTIVE== true && g.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR).Select(d => d.EMPLOYEENAME).ToArray();
                    picName = string.Join(",", pic);
                }
            }
            else
            {
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_C_M_ES)
                    picName = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_C_M_ES).Count() > 0).USERNAME;
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_E_I_ES)
                    picName = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_E_I_ES).Count() > 0).USERNAME;
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_DC)
                    picName = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_DC).Count() > 0).USERNAME;
            }
            return picName;
        }

        private string getPicEmailFromRole(int projectId, int roleId)
        {
            var picEmail = "";
            var sCORole = db.SETUPCLOSEOUTROLEs.Where(s => s.OID == roleId).FirstOrDefault();
            if (sCORole.ASSIGNMENTTYPE != null)
            {
                if (sCORole.ASSIGNMENTTYPE == constants.projectManager)
                    picEmail = db.PROJECTs.Where(p => p.OID == projectId).FirstOrDefault().PROJECTMANAGEREMAIL;
                else if (sCORole.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER)
                {
                    var pic = db.PROJECTs.FirstOrDefault(p => p.OID == projectId).USERASSIGNMENTs.Where(g => g.ISACTIVE == true && g.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER).Select(d => d.EMPLOYEEEMAIL).ToArray();
                    picEmail = string.Join(",", pic);
                }
                else if (sCORole.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                {
                    var pic = db.PROJECTs.FirstOrDefault(p => p.OID == projectId).USERASSIGNMENTs.Where(g => g.ISACTIVE == true && g.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR).Select(d => d.EMPLOYEEEMAIL).ToArray();
                    picEmail = string.Join(",", pic);
                }
            }
            else
            {
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_C_M_ES)
                    picEmail = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_C_M_ES).Count() > 0).EMAIL;
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_E_I_ES)
                    picEmail = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_E_I_ES).Count() > 0).EMAIL;
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_DC)
                    picEmail = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_DC).Count() > 0).EMAIL;
            }
            return picEmail;
        }

        private string getPicIDFromRole(int projectId, int roleId)
        {
            var picID = "";
            var sCORole = db.SETUPCLOSEOUTROLEs.Where(s => s.OID == roleId).FirstOrDefault();
            if (sCORole.ASSIGNMENTTYPE != null)
            {
                if (sCORole.ASSIGNMENTTYPE == constants.projectManager)
                    picID = db.PROJECTs.Where(p => p.OID == projectId).FirstOrDefault().PROJECTMANAGERID;
                else if (sCORole.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER)
                {
                    var pic = db.PROJECTs.FirstOrDefault(p => p.OID == projectId).USERASSIGNMENTs.Where(g => g.ISACTIVE == true && g.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER).Select(d => d.EMPLOYEEID).ToArray();
                    picID = string.Join(",", pic);
                }
                else if (sCORole.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR)
                {
                    var pic = db.PROJECTs.FirstOrDefault(p => p.OID == projectId).USERASSIGNMENTs.Where(g => g.ISACTIVE == true && g.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR).Select(d => d.EMPLOYEEID).ToArray();
                    picID = string.Join(",", pic);
                }
            }
            else
            {
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_C_M_ES)
                    picID = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_C_M_ES).Count() > 0).BADGENO;
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_E_I_ES)
                    picID = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_E_I_ES).Count() > 0).BADGENO;
                if (sCORole.ROLENAME == constants.RoleLeadDesigner_DC)
                    picID = db.USERs.FirstOrDefault(u => u.USERROLEs.Where(ur => ur.ROLE.ROLENAME == constants.RoleLeadDesigner_DC).Count() > 0).BADGENO;
            }
            return picID;
        }

        [HttpGet]
        [ActionName("getCloseOutPICToEmail")]
        public List<taskToEmailList> getCloseOutPICToEmail(int param)
        {
            var r_id = 0;
            var b_n = "";
            var closeOut = db.PROJECTCLOSEOUTs;
            var generalParameter = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.DISABLED_CHECKLIST).FirstOrDefault();
            List<string> rolesLoginer = new List<string>();
            List<projectCloseOutObject> listPCO = new List<projectCloseOutObject>();
            List<closeOutProgressObject> listCop = new List<closeOutProgressObject>();
            //get user that login role from PROJECT
            if (db.PROJECTs.Count(x => x.OID == param && x.PROJECTMANAGERID == b_n) > 0)
            {
                rolesLoginer.Add(constants.PM);
                rolesLoginer.Add(constants.PMF);
            }

            //get user login role from ROLE
            var userId = db.USERs.Where(x => x.BADGENO == b_n).Select(x => x.OID).FirstOrDefault();
            var roleName = db.USERROLEs.Where(x => x.USERID == userId).Select(x => x.ROLE.ROLENAME).ToList();

            foreach (string x in roleName)
            {
                switch (x)
                {
                    case constants.RoleLeadDesigner_C_M_ES:
                        rolesLoginer.Add(constants.LD_C_M_ES);
                        break;
                    case constants.RoleLeadDesigner_E_I_ES:
                        rolesLoginer.Add(constants.LD_E_I_ES);
                        break;
                    case constants.RoleLeadDesigner_DC:
                        rolesLoginer.Add(constants.DC);
                        break;
                }

            }

            //get user login role from ASSIGNMENT            
            if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.EMPLOYEEID == b_n) > 0)
            {
                rolesLoginer.Add(constants.PC);
            }
            if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.EMPLOYEEID == b_n) > 0)
            {
                rolesLoginer.Add(constants.MC);
            }

            var roleLoginerObj = db.SETUPCLOSEOUTROLEs.Select(x => new objRole { id = x.OID, seq = x.SEQ.Value, descript = x.DESCRIPTION, seqApp = x.SEQAPPROVAL.Value }).ToList();
            foreach(var key in closeOut) { 
                var rolesLoginerValidObj = roleLoginerObj.Where(x => getCurRole(key).FirstOrDefault(y => y.id == x.id) != null).ToList();
                var defaultRole = new objRole();
                int idDefaultRole = 0;

                if (key.ISAPPROVED != true || rolesLoginerValidObj.Count > 0)
                {
                    if (r_id == 0)
                    {
                        if (rolesLoginerValidObj.Count == 0)
                        {
                            defaultRole = new objRole();
                            idDefaultRole = 0;
                        }
                        else
                        {
                            defaultRole = rolesLoginerValidObj.OrderBy(x => x.seq).FirstOrDefault();
                            idDefaultRole = rolesLoginerValidObj.OrderBy(x => x.seq).FirstOrDefault().id;
                        }
                    }
                    else
                    {
                        defaultRole = rolesLoginerValidObj.Where(x => x.id == r_id).FirstOrDefault();
                        idDefaultRole = rolesLoginerValidObj.Where(x => x.id == r_id).FirstOrDefault().id;
                    }
                }
                else
                {
                    defaultRole = db.SETUPCLOSEOUTROLEs.Where(x => x.DESCRIPTION == constants.PMF).Select(x => new objRole { id = x.OID, seq = x.SEQ.Value, descript = x.DESCRIPTION, seqApp = x.SEQAPPROVAL.Value }).FirstOrDefault();
                    idDefaultRole = defaultRole.id;
                }



                var prj = db.PROJECTs.Where(x => x.OID == key.PROJECTID).FirstOrDefault();
                projectCloseOutObject pco = new projectCloseOutObject()
                {
                    id = key.OID,
                    projectId = prj.OID,
                    projectNo = prj.PROJECTNO,
                    isApproved = key.ISAPPROVED,
                    projectTitle = prj.PROJECTDESCRIPTION,
                    reasonClosureType = key.REASONCLOSURETYPE,
                    comments = key.COMMENTS,
                    curCopId = idDefaultRole > 0 ? key.CLOSEOUTPROGRESSes.Where(x => x.CLOSEOUTROLEID == idDefaultRole).FirstOrDefault().OID : idDefaultRole,
                    curRoleId = idDefaultRole,
                    createdDate = key.CREATEDDATE,
                    createdBy = key.CREATEDBY,
                    updatedDate = key.UPDATEDDATE,
                    updatedBy = key.UPDATEDBY,
                    remarkRejectCLoseOut = key.REJECTEDREMARK,
                    rolesLoginer = roleLoginerObj,
                    rolesNotSubmitted = getCurRole(key),
                    rolesLoginerValid = rolesLoginerValidObj,
                    loginerIsInCurrProj = roleLoginerObj.Count > 0 ? true : false,
                    loginerIsInCurrTask = rolesLoginerValidObj.Count > 0 ? true : false,
                    roleLoginerDefault = defaultRole,
                    closeOutProgresses = key.CLOSEOUTPROGRESSes.Where(s => s.SUBMITEDDATE == null).OrderBy(o => o.PLANDATE).ToList().Select(s => new closeOutProgressObject
                    {
                        id = s.OID,
                        projectCloseOutId = s.PROJECTCLOSEOUTID,
                        closeOutRoleId = s.CLOSEOUTROLEID,
                        closeOutRoleSeq = s.SETUPCLOSEOUTROLE.SEQ,
                        closeOutSeqApproval = s.SETUPCLOSEOUTROLE.SEQAPPROVAL,
                        closeOutRoleText = s.SETUPCLOSEOUTROLE.DESCRIPTION,
                        closeOutRolePICName = getPicNameFromRole(prj.OID, s.CLOSEOUTROLEID.Value),
                        closeOutRolePICEmail = getPicEmailFromRole(prj.OID, s.CLOSEOUTROLEID.Value),
                        closeOutRolePICID = getPicIDFromRole(prj.OID, s.CLOSEOUTROLEID.Value),
                        planDate = s.PLANDATE,
                        submitedDate = s.SUBMITEDDATE,
                        submitedBy = s.SUBMITEDBY,
                        closeOutCheckLists = s.CLOSEOUTCHECKLISTs.Where(w => (prj.PROJECTTYPE == constants.OPERATING && !generalParameter.VALUE.Contains(w.SETUPCLOSEOUTCHECKLIST.CHECKLISTITEM)) || prj.PROJECTTYPE == constants.CAPITAL).Select(s1 => new closeOutCheckListObject
                        {
                            id = s1.OID,
                            closeOutCheckListId = s1.CLOSEOUTCHECKLISTID,
                            closeOutCheckListIdSeq = s1.SETUPCLOSEOUTCHECKLIST.SEQ,
                            closeOutCheckListText = s1.SETUPCLOSEOUTCHECKLIST.CHECKLISTITEM,
                            isChecked = s1.ISCHECKED
                        }),
                        closeOutDocuments = s.CLOSEOUTDOCUMENTs.Select(s1 => new closeOutDocumentObject
                        {
                            id = s1.OID,
                            closeoutDocumentId = s1.CLOSEOUTDOCUMENTID,
                            supportingDocumentId = s1.SUPPORTINGDOCUMENTID,
                            refDocId = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.REFDOCID : null,
                            fileType = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SETUPCLOSEOUTDOCDOCUMENT.DOCUMENTTYPE : null,
                            fileSeq = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.SEQUENCENO : null,
                            fileName = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.FILENAME : null,
                            description = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.DESCRIPTION : null
                        }),
                        setupCloseOutDocuments = db.SETUPCLOSEOUTDOCDOCUMENTs.Where(w => w.ISACTIVE == true && w.CLOSEOUTROLEID == s.CLOSEOUTROLEID).Select(s1 => new setupCloseOutDocumentObject
                        {
                            id = s1.OID,
                            documentType = s1.DOCUMENTTYPE
                        })
                    })
                };
                listPCO.Add(pco);
            }
            var tEmailList = new List<taskToEmailList>();
            foreach (var pclo in listPCO)
            {
                foreach (var cop in pclo.closeOutProgresses.Where(p => !p.submitedDate.HasValue && param==14?(p.planDate <= DateTime.Now.AddDays(14) && p.planDate > DateTime.Now) : (p.planDate <= DateTime.Now)))
                {
                    var pj = db.PROJECTs.Where(p => p.OID == pclo.projectId).FirstOrDefault();
                    var coPicId = cop.closeOutRolePICID.Split(',');
                    if (coPicId.Count() > 1)
                    {
                        var coPicName = cop.closeOutRolePICName.Split(',');
                        var coPicEmail = cop.closeOutRolePICEmail.Split(',');

                        for (var n = 0; n < coPicId.Count(); n++)
                        {
                            var tEmail = new coToEmailList
                            {
                                KEY = cop.closeOutRoleId.ToString(),
                                BADGENO = coPicId[n],
                                BADGENOTO = coPicId[n] + " | " + pj.PROJECTMANAGERID,
                                PROJECTNO = pclo.projectNo,
                                PROJECTDESC = pclo.projectTitle,
                                DAYSTODUE = cop.planDate != null ? Math.Floor((cop.planDate.Value.Subtract(DateTime.Now).TotalDays)) : 0,
                                DUEDATE = cop.planDate != null ? cop.planDate.Value.Date.Date.ToString("dd-MMM-yyyy") : cop.planDate.Value.Date.ToString("dd-MMM-yyyy"),
                                FULLNAME = coPicName[n],
                                RECEIVER = coPicName[n],
                                EMAIL = coPicEmail[n],
                                EMAILCC = pj.PROJECTMANAGEREMAIL,
                                PIC = cop.closeOutRoleText,
                            };
                            if (tEmail.BADGENO != "" && tEmail.DUEDATE != null)
                                tEmailList.Add(tEmail);
                        }

                    } else
                    {
                        var tEmail = new coToEmailList
                        {
                            KEY = cop.closeOutRoleId.ToString(),
                            BADGENO = cop.closeOutRolePICID,
                            BADGENOTO = cop.closeOutRolePICID + " | " + pj.PROJECTMANAGERID,
                            PROJECTNO = pclo.projectNo,
                            PROJECTDESC = pclo.projectTitle,
                            DAYSTODUE = cop.planDate != null ? Math.Floor((cop.planDate.Value.Subtract(DateTime.Now).TotalDays)) : 0,
                            DUEDATE = cop.planDate.Value.Date.ToString("dd-MMM-yyyy"),
                            FULLNAME = cop.closeOutRolePICName,
                            RECEIVER = cop.closeOutRolePICName,
                            EMAIL = cop.closeOutRolePICEmail,
                            EMAILCC = pj.PROJECTMANAGEREMAIL,
                            PIC = cop.closeOutRoleText,
                        };
                        if (tEmail.BADGENO != "" && tEmail.DUEDATE != null)
                            tEmailList.Add(tEmail);
                    }
                }
            }
            //var listField = m.Where(p => ((DateTime?)p.GetType().GetProperty("psd").GetValue(p, null) <= DateTime.Now.AddDays(14))).
            //    Select(e => new taskToEmailList()
            //    {
            //        KEY = (string)e.GetType().GetProperty("id").GetValue(e, null).ToString(),
            //        BADGENO = (string)e.GetType().GetProperty("picId").GetValue(e, null),
            //        BADGENOTO = (string)e.GetType().GetProperty("picId").GetValue(e, null) + " | " + (string)e.GetType().GetProperty("pNo").GetValue(e, null),
            //        DAYSTODUE = Math.Floor(((DateTime?)e.GetType().GetProperty("psd").GetValue(e, null)).Value.Subtract(DateTime.Now).TotalDays),
            //        DUEDATE = (DateTime?)e.GetType().GetProperty("psd").GetValue(e, null),
            //        FULLNAME = (string)e.GetType().GetProperty("pic").GetValue(e, null),
            //        RECEIVER = (string)e.GetType().GetProperty("pic").GetValue(e, null),
            //        EMAIL = (string)e.GetType().GetProperty("picEmail").GetValue(e, null),
            //        EMAILCC = (string)e.GetType().GetProperty("picEmailCC").GetValue(e, null),
            //        STATUS = (string)e.GetType().GetProperty("statusName").GetValue(e, null),
            //        TASK = (string)e.GetType().GetProperty("pnoName").GetValue(e, null),
            //        TIPE = (string)e.GetType().GetProperty("tipeName").GetValue(e, null),
            //    });

            return tEmailList;
        }

        [HttpGet]
        [ActionName("viewProjectCloseOut")]
        public projectCloseOutObject viewProjectCloseOut(int param, string b_n, int r_id)
        {
            var generalParameter = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.DISABLED_CHECKLIST).FirstOrDefault();
            var closeOut = db.PROJECTCLOSEOUTs.Where(w => w.PROJECTID == param).FirstOrDefault();
            List<string> rolesLoginer = new List<string>();

            //get user that login role from PROJECT
            if (db.PROJECTs.Count(x => x.OID == param && x.PROJECTMANAGERID == b_n) > 0)
            {
                rolesLoginer.Add(constants.PM);
                rolesLoginer.Add(constants.PMF);
            }

            //get user login role from ROLE
            var userId = db.USERs.Where(x => x.BADGENO == b_n).Select(x => x.OID).FirstOrDefault();
            var roleName = db.USERROLEs.Where(x => x.USERID == userId).Select(x => x.ROLE.ROLENAME).ToList();

            foreach (string x in roleName)
            {
                switch (x)
                {
                    case constants.RoleLeadDesigner_C_M_ES:
                        rolesLoginer.Add(constants.LD_C_M_ES);
                        break;
                    case constants.RoleLeadDesigner_E_I_ES:
                        rolesLoginer.Add(constants.LD_E_I_ES);
                        break;
                    case constants.RoleLeadDesigner_DC:
                        rolesLoginer.Add(constants.DC);
                        break;
                }

            }

            //get user login role from ASSIGNMENT            
            if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.EMPLOYEEID == b_n) > 0)
            {
                rolesLoginer.Add(constants.PC);
            }
            if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.EMPLOYEEID == b_n) > 0)
            {
                rolesLoginer.Add(constants.MC);
            }

            var roleLoginerObj = db.SETUPCLOSEOUTROLEs.Where(x => rolesLoginer.Contains(x.DESCRIPTION)).Select(x => new objRole { id = x.OID, seq = x.SEQ.Value, descript = x.DESCRIPTION, seqApp = x.SEQAPPROVAL.Value }).ToList();
            var rolesLoginerValidObj = roleLoginerObj.Where(x => ((getCurRole(closeOut).FirstOrDefault(y => y.id == x.id) != null)) /*|| roleLoginerObj.Where(rl => rl.descript==constants.PM).Count()>0*/).ToList();
            var defaultRole = new objRole();
            int idDefaultRole = 0;

            if(closeOut.ISAPPROVED != true || rolesLoginerValidObj.Count > 0)
            {
                if (r_id == 0)
                {
                    if (rolesLoginerValidObj.Count == 0) {
                        defaultRole = new objRole();
                        idDefaultRole = 0;
                    }
                    else
                    {
                        defaultRole = rolesLoginerValidObj.OrderBy(x => x.seq).FirstOrDefault();
                        idDefaultRole = rolesLoginerValidObj.OrderBy(x => x.seq).FirstOrDefault().id;
                    }
                }
                else
                {
                    defaultRole = rolesLoginerValidObj.Where(x => x.id == r_id).FirstOrDefault();
                    idDefaultRole = rolesLoginerValidObj.Where(x => x.id == r_id).FirstOrDefault().id;
                }
            }
            else
            {
                defaultRole = db.SETUPCLOSEOUTROLEs.Where(x => x.DESCRIPTION == constants.PMF).Select(x => new objRole { id = x.OID, seq = x.SEQ.Value, descript = x.DESCRIPTION, seqApp = x.SEQAPPROVAL.Value }).FirstOrDefault();
                idDefaultRole = defaultRole.id;
            }

            

            var prj = db.PROJECTs.Where(x => x.OID == closeOut.PROJECTID).FirstOrDefault();

            projectCloseOutObject pco = new projectCloseOutObject
            {
                id = closeOut.OID,
                projectId = prj.OID,
                projectNo = prj.PROJECTNO,
                isApproved = closeOut.ISAPPROVED,
                projectTitle = prj.PROJECTDESCRIPTION,
                reasonClosureType = closeOut.REASONCLOSURETYPE,
                comments = closeOut.COMMENTS,
                curCopId = idDefaultRole > 0 ? closeOut.CLOSEOUTPROGRESSes.Where(x => x.CLOSEOUTROLEID == idDefaultRole).FirstOrDefault().OID : idDefaultRole,
                curRoleId = idDefaultRole,
                createdDate = closeOut.CREATEDDATE,
                createdBy = closeOut.CREATEDBY,
                updatedDate = closeOut.UPDATEDDATE,
                updatedBy = closeOut.UPDATEDBY,
                remarkRejectCLoseOut = closeOut.REJECTEDREMARK,
                rolesLoginer = roleLoginerObj,
                rolesNotSubmitted = getCurRole(closeOut),
                rolesLoginerValid = rolesLoginerValidObj,
                loginerIsInCurrProj = roleLoginerObj.Count > 0 ? true : false,
                loginerIsInCurrTask = rolesLoginerValidObj.Count > 0 ? true : false,
                roleLoginerDefault = defaultRole,
                closeOutProgresses = closeOut.CLOSEOUTPROGRESSes.OrderBy(o => o.SETUPCLOSEOUTROLE.SEQ).ToList().Select(s => new closeOutProgressObject
                {
                    id = s.OID,
                    projectCloseOutId = s.PROJECTCLOSEOUTID,
                    closeOutRoleId = s.CLOSEOUTROLEID,
                    closeOutRoleSeq = s.SETUPCLOSEOUTROLE.SEQ,
                    closeOutSeqApproval = s.SETUPCLOSEOUTROLE.SEQAPPROVAL,
                    closeOutRoleText = s.SETUPCLOSEOUTROLE.DESCRIPTION,
                    closeOutRolePICName = getPicNameFromRole(prj.OID, s.CLOSEOUTROLEID.Value),
                    planDate = s.PLANDATE,
                    submitedDate = s.SUBMITEDDATE,
                    submitedBy = s.SUBMITEDBY,
                    closeOutCheckLists = s.CLOSEOUTCHECKLISTs.Where(w => (prj.PROJECTTYPE == constants.OPERATING && !generalParameter.VALUE.Contains(w.SETUPCLOSEOUTCHECKLIST.CHECKLISTITEM)) || prj.PROJECTTYPE == constants.CAPITAL).Select(s1 => new closeOutCheckListObject
                    {
                        id = s1.OID,
                        closeOutCheckListId = s1.CLOSEOUTCHECKLISTID,
                        closeOutCheckListIdSeq = s1.SETUPCLOSEOUTCHECKLIST.SEQ,
                        closeOutCheckListText = s1.SETUPCLOSEOUTCHECKLIST.CHECKLISTITEM,
                        isChecked = s1.ISCHECKED
                    }),
                    closeOutDocuments = s.CLOSEOUTDOCUMENTs.Select(s1 => new closeOutDocumentObject
                    {
                        id = s1.OID,
                        closeoutDocumentId = s1.CLOSEOUTDOCUMENTID,
                        supportingDocumentId = s1.SUPPORTINGDOCUMENTID,
                        refDocId = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.REFDOCID : null,
                        fileType = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SETUPCLOSEOUTDOCDOCUMENT.DOCUMENTTYPE : null,
                        fileSeq = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.SEQUENCENO : null,
                        fileName = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.FILENAME : null,
                        description = s1.SUPPORTINGDOCUMENTID.HasValue ? s1.SUPPORTINGDOCUMENT.DESCRIPTION : null
                    }),
                    setupCloseOutDocuments = db.SETUPCLOSEOUTDOCDOCUMENTs.Where(w => w.ISACTIVE == true && w.CLOSEOUTROLEID == s.CLOSEOUTROLEID).Select(s1 => new setupCloseOutDocumentObject
                    {
                        id = s1.OID,
                        documentType = s1.DOCUMENTTYPE
                    })
                })
            };
            return pco;
        }

        [HttpGet]
        [ActionName("checkProjectCloseOut")]
        public object checkProjectCloseOut(int param, string bn, int rid)
        {
            PROJECTCLOSEOUT closeOut = db.PROJECTCLOSEOUTs.Where(w => w.PROJECTID == param).FirstOrDefault();
            if (closeOut == null)
            {
                closeOut = new PROJECTCLOSEOUT();
                closeOut.PROJECTID = param;
                closeOut.CREATEDDATE = DateTime.Now;
                closeOut.CREATEDBY = GetCurrentNTUserId();

                db.Entry(closeOut).State = EntityState.Added;
                db.SaveChanges();
            }

            var listRole = db.SETUPCLOSEOUTROLEs.Where(w => w.ISACTIVE == true);

            foreach (SETUPCLOSEOUTROLE role in listRole)
            {
                var progress = db.CLOSEOUTPROGRESSes.Where(w => w.PROJECTCLOSEOUTID == closeOut.OID && w.CLOSEOUTROLEID == role.OID).FirstOrDefault();
                if (progress == null)
                {
                    progress = new CLOSEOUTPROGRESS();
                    progress.PROJECTCLOSEOUTID = closeOut.OID;
                    progress.CLOSEOUTROLEID = role.OID;
                    db.Entry(progress).State = EntityState.Added;
                }

            }
            db.SaveChanges();

            var listProgress = db.CLOSEOUTPROGRESSes.Where(w => w.PROJECTCLOSEOUTID == closeOut.OID);
            foreach (CLOSEOUTPROGRESS progress in listProgress)
            {
                var listCheckList = db.SETUPCLOSEOUTCHECKLISTs.Where(w => w.CLOSEOUTROLEID == progress.CLOSEOUTROLEID && w.ISACTIVE == true);
                foreach (SETUPCLOSEOUTCHECKLIST checklist in listCheckList)
                {
                    var checkOutChecklist = db.CLOSEOUTCHECKLISTs.Where(w => w.CLOSEOUTPROGRESSID == progress.OID && w.CLOSEOUTCHECKLISTID == checklist.OID).FirstOrDefault();
                    if (checkOutChecklist == null)
                    {
                        checkOutChecklist = new CLOSEOUTCHECKLIST();
                        checkOutChecklist.CLOSEOUTPROGRESSID = progress.OID;
                        checkOutChecklist.CLOSEOUTCHECKLISTID = checklist.OID;
                        db.Entry(checkOutChecklist).State = EntityState.Added;
                    }
                }
            }
            db.SaveChanges();

            var result = new List<projectCloseOutObject>();
            result.Add(viewProjectCloseOut(closeOut.PROJECTID.Value, bn, rid));
            return result;
        }

        [HttpPut]
        [ActionName("setDeletedCloseOut")]
        public HttpResponseMessage setDeletedCloseOut(int param, bool isDeleted, projectCloseOutObject pCO)
        {
            var generalParameter = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.DISABLED_CHECKLIST).FirstOrDefault();
            int currProgressId = pCO.curCopId;

            //Save / Submit
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                PROJECTCLOSEOUT projCloseOut = db.PROJECTCLOSEOUTs.Where(w => w.PROJECTID == param).FirstOrDefault();
                if (projCloseOut != null)
                {
                    projCloseOut.REASONCLOSURETYPE = "DEL";
                    projCloseOut.COMMENTS = pCO.comments;
                    projCloseOut.ISAPPROVED = true ;
                    projCloseOut.APPROVEDDATE = DateTime.Now;
                    projCloseOut.APPROVEDBY = GetCurrentNTUserId();


                    foreach (closeOutProgressObject cOP in pCO.closeOutProgresses)
                    {

                        //CLOSEOUTPROGRESS cOProg = projCloseOut.CLOSEOUTPROGRESSes.Where(x => x.CLOSEOUTROLEID == cOP.closeOutRoleId).FirstOrDefault();
                        //cOProg.CLOSEOUTROLEID = cOP.closeOutRoleId;
                        //cOProg.PLANDATE = cOP.planDate;

                        ////if (cOP.id == pCO.currentRoleId)
                        //if (cOP.id == currProgressId)
                        //{
                        //    foreach (CLOSEOUTCHECKLIST chkList in cOProg.CLOSEOUTCHECKLISTs.Where(w => (projCloseOut.PROJECT.PROJECTTYPE == constants.OPERATING && !generalParameter.VALUE.Contains(w.SETUPCLOSEOUTCHECKLIST.CHECKLISTITEM)) || projCloseOut.PROJECT.PROJECTTYPE == constants.CAPITAL))
                        //    {
                        //        closeOutCheckListObject currentCheckList = cOP.closeOutCheckLists.Where(w => w.closeOutCheckListId.Value == chkList.CLOSEOUTCHECKLISTID.Value).FirstOrDefault();
                        //        chkList.ISCHECKED = currentCheckList.isChecked;
                        //    }


                        //    List<int> codIDs = db.CLOSEOUTDOCUMENTs.Where(x => x.CLOSEOUTPROGRESSID == cOP.id).Select(x => x.OID).ToList();
                        //    List<int> supIDs = db.CLOSEOUTDOCUMENTs.Where(x => x.CLOSEOUTPROGRESSID == cOP.id).Select(x => x.SUPPORTINGDOCUMENTID.Value).ToList();
                        //    List<int> codIds = new List<int>();
                        //    List<int> supIds = new List<int>();



                        //    foreach (closeOutDocumentObject cod in cOP.closeOutDocuments)
                        //    {
                        //        codIds.Add(cod.id);
                        //        supIds.Add(cod.supportingDocumentId.Value);

                        //    }

                        //    var diffCodIds = codIDs.Except(codIds);
                        //    var diffSupIds = supIDs.Except(supIds);

                        //    foreach (int i in diffCodIds)
                        //    {
                        //        CLOSEOUTDOCUMENT cod = db.CLOSEOUTDOCUMENTs.Find(i);
                        //        //remove close out document
                        //        if (cod == null)
                        //        {
                        //            return Request.CreateResponse(HttpStatusCode.NotFound);
                        //        }

                        //        db.CLOSEOUTDOCUMENTs.Remove(cod);
                        //    }

                        //    foreach (int i in diffSupIds)
                        //    {
                        //        //remove supporting document
                        //        SUPPORTINGDOCUMENT spd = db.SUPPORTINGDOCUMENTs.Find(i);
                        //        db.SUPPORTINGDOCUMENTs.Remove(spd);
                        //    }


                        //}

                    }

                    db.Entry(projCloseOut).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, pCO);
        }

        [HttpPut]
        [ActionName("saveSubmitCloseOut")]
        public HttpResponseMessage saveSubmitCloseOut(int param, bool isSubmit, projectCloseOutObject pCO)
        {
            var generalParameter = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.DISABLED_CHECKLIST).FirstOrDefault();
            int currProgressId = pCO.curCopId;

            //Save / Submit
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                PROJECTCLOSEOUT projCloseOut = db.PROJECTCLOSEOUTs.Where(w => w.PROJECTID == param).FirstOrDefault();
                if (projCloseOut != null)
                {
                    projCloseOut.REASONCLOSURETYPE = pCO.reasonClosureType;
                    projCloseOut.COMMENTS = pCO.comments;

                    foreach (closeOutProgressObject cOP in pCO.closeOutProgresses)
                    {

                        CLOSEOUTPROGRESS cOProg = projCloseOut.CLOSEOUTPROGRESSes.Where(x => x.CLOSEOUTROLEID == cOP.closeOutRoleId).FirstOrDefault();
                        cOProg.CLOSEOUTROLEID = cOP.closeOutRoleId;
                        cOProg.PLANDATE = cOP.planDate;
                        if(cOP.submitedDate != null)
                        {
                            cOProg.SUBMITEDDATE = cOP.submitedDate;
                            cOProg.SUBMITEDBY = GetCurrentNTUserId();
                        }
                        //if (cOP.id == pCO.currentRoleId)
                        if (cOP.id == currProgressId)
                        {
                            foreach (CLOSEOUTCHECKLIST chkList in cOProg.CLOSEOUTCHECKLISTs.Where(w => (projCloseOut.PROJECT.PROJECTTYPE == constants.OPERATING && !generalParameter.VALUE.Contains(w.SETUPCLOSEOUTCHECKLIST.CHECKLISTITEM)) || projCloseOut.PROJECT.PROJECTTYPE == constants.CAPITAL))
                            {
                                closeOutCheckListObject currentCheckList = cOP.closeOutCheckLists.Where(w => w.closeOutCheckListId.Value == chkList.CLOSEOUTCHECKLISTID.Value).FirstOrDefault();
                                chkList.ISCHECKED = currentCheckList.isChecked;
                            }


                            List<int> codIDs = db.CLOSEOUTDOCUMENTs.Where(x => x.CLOSEOUTPROGRESSID == cOP.id).Select(x => x.OID).ToList();
                            List<int> supIDs = db.CLOSEOUTDOCUMENTs.Where(x => x.CLOSEOUTPROGRESSID == cOP.id).Select(x => x.SUPPORTINGDOCUMENTID.Value).ToList();
                            List<int> codIds = new List<int>();
                            List<int> supIds = new List<int>();

                            

                            foreach (closeOutDocumentObject cod in cOP.closeOutDocuments)
                            {
                                codIds.Add(cod.id);
                                supIds.Add(cod.supportingDocumentId.Value);

                            }

                            var diffCodIds = codIDs.Except(codIds);
                            var diffSupIds = supIDs.Except(supIds);

                            foreach (int i in diffCodIds)
                            {
                                CLOSEOUTDOCUMENT cod = db.CLOSEOUTDOCUMENTs.Find(i);
                                //remove close out document
                                if (cod == null)
                                {
                                    return Request.CreateResponse(HttpStatusCode.NotFound);
                                }

                                db.CLOSEOUTDOCUMENTs.Remove(cod);
                            }

                            foreach (int i in diffSupIds)
                            {
                                //remove supporting document
                                SUPPORTINGDOCUMENT spd = db.SUPPORTINGDOCUMENTs.Find(i);
                                db.SUPPORTINGDOCUMENTs.Remove(spd);
                            }


                            if (isSubmit)
                            {
                                cOProg.SUBMITEDDATE = DateTime.Now;
                                cOProg.SUBMITEDBY = GetCurrentNTUserId();
                            }
                        }

                    }
                   
                    db.Entry(projCloseOut).State = EntityState.Modified;
                }
                db.SaveChanges();


                //send email has been submitted

                if (isSubmit)
                {


                    var prj = db.PROJECTs.Find(param);
                    int curSeqApproval = db.CLOSEOUTPROGRESSes.Find(currProgressId).SETUPCLOSEOUTROLE.SEQAPPROVAL.Value;
                    string curRoleDescription = db.CLOSEOUTPROGRESSes.Find(currProgressId).SETUPCLOSEOUTROLE.DESCRIPTION;
                    int maxApproval = db.SETUPCLOSEOUTROLEs.Max(x => x.SEQAPPROVAL).Value;
                    List<string> listNexRoleDescription = new List<string>();

                    if (curSeqApproval < maxApproval) // maxApproval(PM Finalize) not doing submit
                    {
                        ToEmailController tmc = new ToEmailController();

                        // all members in project
                        var usersPM = db.PROJECTs.Where(x => x.OID == pCO.projectId).Select(x => new { userName = x.PROJECTMANAGERNAME, email = x.PROJECTMANAGEREMAIL });
                        var usersLces = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_C_M_ES && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                        var usersLees = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_E_I_ES && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                        var usersDc = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_DC && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                        var usersPmc = db.USERASSIGNMENTs.Where(x => x.PROJECTID == pCO.projectId && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.ISACTIVE == true).Select(x => new { userName = x.EMPLOYEENAME, email = x.EMPLOYEEEMAIL });
                        var usersPpc = db.USERASSIGNMENTs.Where(x => x.PROJECTID == pCO.projectId && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.ISACTIVE == true).Select(x => new { userName = x.EMPLOYEENAME, email = x.EMPLOYEEEMAIL });
                        var email_redac_submitted = db.EMAILREDACTIONs.Where(x => x.CODE == constants.SUBMIT_CLOSEOUT && x.ISACTIVE == true).FirstOrDefault();

                     
                        //replacers email body
                        string curRoleDescript = db.SETUPCLOSEOUTROLEs.Find(pCO.curRoleId).DESCRIPTION;
                        string submitter = pCO.userLoginName;

                        //email parameter
                        //string sendTo, string sendCC, string sendFrom, string subject, string body
                        string sendTo = "";
                        string sendCC = "";
                        string sendFrom = pCO.userLoginEmail;
                        string subject = email_redac_submitted.SUBJECT;
                        string body = email_redac_submitted.BODY
                                        .Replace("[TONAME]", "All")
                                        .Replace("[PROJECTNAME]", pCO.projectTitle)
                                        .Replace("[PROJECTNO]", pCO.projectNo)
                                        .Replace("[ROLE]", curRoleDescript)
                                        .Replace("[SUBMITTER]", submitter)
                                        .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);


                        switch (curRoleDescript)
                        {
                            case constants.PM:
                                try
                                {
                                    if (prj.PROJECTMANAGERID == pCO.userLoginBadgeNo)
                                    {
                                        // sendTo -> usersLces + usersLees + usersPmc
                                        sendTo = string.Join("; ", usersLces.Select(y => y.email).Union(usersLees.Select(y => y.email)).Union(usersPmc.Select(y => y.email)));
                                        // sendCC -> usersPM
                                        sendCC = string.Join("; ", usersPM.Select(y => y.email));
                                        submitter = prj.PROJECTMANAGERNAME;
                                    }
                                }
                                catch (Exception e)
                                {
                                    WriteLog(e.ToString());
                                }
                                break;
                            case constants.LD_C_M_ES:
                                try
                                {
                                    if(db.USERROLEs.Count(x=>x.ROLEID == (db.ROLEs.Where(y=>y.ROLENAME == constants.RoleLeadDesigner_C_M_ES).FirstOrDefault().OID) && x.USERID == db.USERs.Where(u=>u.BADGENO == pCO.userLoginBadgeNo).FirstOrDefault().OID)>0)
                                    {
                                        // sendTo -> usersDc
                                        sendTo = string.Join("; ", usersDc.Select(y => y.email));
                                        // sendCC -> usersLces + usersPM
                                        sendCC = string.Join("; ", usersLces.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                                    }
                                }
                                catch(Exception e)
                                {
                                    WriteLog(e.ToString());
                                }                                
                                break;
                            case constants.LD_E_I_ES:
                                try {
                                    if (db.USERROLEs.Count(x => x.ROLEID == (db.ROLEs.Where(y => y.ROLENAME == constants.RoleLeadDesigner_E_I_ES).FirstOrDefault().OID) && x.USERID == db.USERs.Where(u => u.BADGENO == pCO.userLoginBadgeNo).FirstOrDefault().OID) > 0)
                                    {
                                        // sendTo -> usersDc
                                        sendTo = string.Join("; ", usersDc.Select(y => y.email));
                                        // sendCC -> usersLees + usersMP
                                        sendCC = string.Join("; ", usersLees.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                                    }
                                }
                                catch (Exception e) {
                                    WriteLog(e.ToString());
                                }                                
                                break;
                            case constants.MC:
                                try
                                {
                                    if (db.USERASSIGNMENTs.Count(x =>x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.EMPLOYEEID == pCO.userLoginBadgeNo) > 0)
                                    {
                                        // sendTo -> usersPpc
                                        sendTo = string.Join("; ", usersPpc.Select(y => y.email));
                                        // sendCC -> usersPmc + usersPM
                                        sendCC = string.Join("; ", usersPmc.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                                    }
                                }
                                catch(Exception e)
                                {
                                    WriteLog(e.ToString());
                                }
                                break;
                            case constants.PC:
                                try
                                {
                                    if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.EMPLOYEEID == pCO.userLoginBadgeNo) > 0)
                                    {
                                        // sendTo -> usersDc
                                        sendTo = string.Join("; ", usersDc.Select(y => y.email));
                                        // sendCC -> usersPpc + usersPM
                                        sendCC = string.Join("; ", usersPpc.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                                    }
                                }
                                catch(Exception e)
                                {
                                    WriteLog(e.ToString());
                                }
                                
                                break;
                            case constants.DC:
                                try {
                                    if (db.USERROLEs.Count(x => x.ROLEID == (db.ROLEs.Where(y => y.ROLENAME == constants.RoleLeadDesigner_DC).FirstOrDefault().OID) && x.USERID == db.USERs.Where(u => u.BADGENO == pCO.userLoginBadgeNo).FirstOrDefault().OID) > 0)
                                    {
                                        // sendTo -> usersPM
                                        sendTo = string.Join("; ", usersPM.Select(y => y.email));
                                        // sendCC -> usersDc
                                        sendCC = string.Join("; ", usersDc.Select(y => y.email));
                                    }
                                }
                                catch (Exception e){
                                    WriteLog(e.ToString());
                                }                                
                                break;
                            default:
                                break;
                        }

                        //send email
                        tmc.EmailUser(sendTo, sendCC, sendFrom, subject, body);
                       
                    }
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, pCO);
        }

        [HttpPut]
        [ActionName("submitAfterUploadSuccess")]
        public HttpResponseMessage submitAfterUploadSuccess(int param, projectCloseOutObject pCO)
        {
            int currProgressId = pCO.curCopId;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            try
            {
                PROJECTCLOSEOUT projCloseOut = db.PROJECTCLOSEOUTs.Where(w => w.PROJECTID == param).FirstOrDefault();
                if (projCloseOut != null)
                {
                    
                    foreach (closeOutProgressObject cOP in pCO.closeOutProgresses)
                    {
                        CLOSEOUTPROGRESS cOProg = projCloseOut.CLOSEOUTPROGRESSes.Where(x => x.CLOSEOUTROLEID == cOP.closeOutRoleId).FirstOrDefault();
                        if (cOP.id == currProgressId)
                        {
                            cOProg.SUBMITEDDATE = DateTime.Now;
                            cOProg.SUBMITEDBY = GetCurrentNTUserId();
                        }
                    }
                    db.Entry(projCloseOut).State = EntityState.Modified;
                }
                db.SaveChanges();

                var prj = db.PROJECTs.Find(param);
                ToEmailController tmc = new ToEmailController();

                // all members in project
                var usersPM = db.PROJECTs.Where(x => x.OID == pCO.projectId).Select(x => new { userName = x.PROJECTMANAGERNAME, email = x.PROJECTMANAGEREMAIL });
                var usersLces = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_C_M_ES && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                var usersLees = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_E_I_ES && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                var usersDc = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_DC && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                var usersPmc = db.USERASSIGNMENTs.Where(x => x.PROJECTID == pCO.projectId && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.ISACTIVE == true).Select(x => new { userName = x.EMPLOYEENAME, email = x.EMPLOYEEEMAIL });
                var usersPpc = db.USERASSIGNMENTs.Where(x => x.PROJECTID == pCO.projectId && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.ISACTIVE == true).Select(x => new { userName = x.EMPLOYEENAME, email = x.EMPLOYEEEMAIL });
                var email_redac_submitted = db.EMAILREDACTIONs.Where(x => x.CODE == constants.SUBMIT_CLOSEOUT && x.ISACTIVE == true).FirstOrDefault();

                //replacers email body
                string curRoleDescript = db.SETUPCLOSEOUTROLEs.Find(pCO.curRoleId).DESCRIPTION;
                string submitter = pCO.userLoginName;

                //email parameter
                //string sendTo, string sendCC, string sendFrom, string subject, string body
                string sendTo = "";
                string sendCC = "";
                string sendFrom = pCO.userLoginEmail;
                string subject = email_redac_submitted.SUBJECT;
                string body = email_redac_submitted.BODY
                                .Replace("[TONAME]", "All")
                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                .Replace("[PROJECTNO]", pCO.projectNo)
                                .Replace("[ROLE]", curRoleDescript)
                                .Replace("[SUBMITTER]", submitter)
                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);

                switch (curRoleDescript)
                {
                    case constants.PM:
                        try
                        {
                            if (prj.PROJECTMANAGERID == pCO.userLoginBadgeNo)
                            {
                                // sendTo -> usersLces + usersLees + usersPmc
                                sendTo = string.Join("; ", usersLces.Select(y => y.email).Union(usersLees.Select(y => y.email)).Union(usersPmc.Select(y => y.email)));
                                // sendCC -> usersPM
                                sendCC = string.Join("; ", usersPM.Select(y => y.email));
                                submitter = prj.PROJECTMANAGERNAME;
                            }
                        }
                        catch (Exception e)
                        {
                            WriteLog(e.ToString());
                        }
                        break;
                    case constants.LD_C_M_ES:
                        try
                        {
                            if (db.USERROLEs.Count(x => x.ROLEID == (db.ROLEs.Where(y => y.ROLENAME == constants.RoleLeadDesigner_C_M_ES).FirstOrDefault().OID) && x.USERID == db.USERs.Where(u => u.BADGENO == pCO.userLoginBadgeNo).FirstOrDefault().OID) > 0)
                            {
                                // sendTo -> usersDc
                                sendTo = string.Join("; ", usersDc.Select(y => y.email));
                                // sendCC -> usersLces + usersPM
                                sendCC = string.Join("; ", usersLces.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                            }
                        }
                        catch (Exception e)
                        {
                            WriteLog(e.ToString());
                        }
                        break;
                    case constants.LD_E_I_ES:
                        try
                        {
                            if (db.USERROLEs.Count(x => x.ROLEID == (db.ROLEs.Where(y => y.ROLENAME == constants.RoleLeadDesigner_E_I_ES).FirstOrDefault().OID) && x.USERID == db.USERs.Where(u => u.BADGENO == pCO.userLoginBadgeNo).FirstOrDefault().OID) > 0)
                            {
                                // sendTo -> usersDc
                                sendTo = string.Join("; ", usersDc.Select(y => y.email));
                                // sendCC -> usersLees + usersMP
                                sendCC = string.Join("; ", usersLees.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                            }
                        }
                        catch (Exception e)
                        {
                            WriteLog(e.ToString());
                        }
                        break;
                    case constants.MC:
                        try
                        {
                            if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.EMPLOYEEID == pCO.userLoginBadgeNo) > 0)
                            {
                                // sendTo -> usersPpc
                                sendTo = string.Join("; ", usersPpc.Select(y => y.email));
                                // sendCC -> usersPmc + usersPM
                                sendCC = string.Join("; ", usersPmc.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                            }
                        }
                        catch (Exception e)
                        {
                            WriteLog(e.ToString());
                        }
                        break;
                    case constants.PC:
                        try
                        {
                            if (db.USERASSIGNMENTs.Count(x => x.PROJECTID == param && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.EMPLOYEEID == pCO.userLoginBadgeNo) > 0)
                            {
                                // sendTo -> usersDc
                                sendTo = string.Join("; ", usersDc.Select(y => y.email));
                                // sendCC -> usersPpc + usersPM
                                sendCC = string.Join("; ", usersPpc.Select(y => y.email).Union(usersPM.Select(y => y.email)));
                            }
                        }
                        catch (Exception e)
                        {
                            WriteLog(e.ToString());
                        }

                        break;
                    case constants.DC:
                        try
                        {
                            if (db.USERROLEs.Count(x => x.ROLEID == (db.ROLEs.Where(y => y.ROLENAME == constants.RoleLeadDesigner_DC).FirstOrDefault().OID) && x.USERID == db.USERs.Where(u => u.BADGENO == pCO.userLoginBadgeNo).FirstOrDefault().OID) > 0)
                            {
                                // sendTo -> usersPM
                                sendTo = string.Join("; ", usersPM.Select(y => y.email));
                                // sendCC -> usersDc
                                sendCC = string.Join("; ", usersDc.Select(y => y.email));
                            }
                        }
                        catch (Exception e)
                        {
                            WriteLog(e.ToString());
                        }
                        break;
                    default:
                        break;
                }

                //send email
                tmc.EmailUser(sendTo, sendCC, sendFrom, subject, body);

            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, pCO);
        }


        [HttpPut]
        [ActionName("approveRejectCloseOut")]
        public HttpResponseMessage approveRejectCloseOut(int param, bool isApproved, projectCloseOutObject pCO)
        {
            PROJECTCLOSEOUT projCloseOut = db.PROJECTCLOSEOUTs.Where(w => w.PROJECTID == param).FirstOrDefault();
            int maxSeqApproval = db.SETUPCLOSEOUTROLEs.Max(m => m.SEQAPPROVAL).Value;
            //int currRoleId = db.CLOSEOUTPROGRESSes.Find(pCO.currentRoleId).CLOSEOUTROLEID.Value;
            int currRoleId = pCO.curRoleId;
            int currSeqApproval = db.SETUPCLOSEOUTROLEs.Find(currRoleId).SEQAPPROVAL.Value;
            var prj = db.PROJECTs.Find(param);

            if (currSeqApproval == maxSeqApproval)
            {
                projCloseOut.ISAPPROVED = isApproved;
                if (projCloseOut.ISAPPROVED == true)
                {
                    projCloseOut.APPROVEDDATE = DateTime.Now;
                    projCloseOut.APPROVEDBY = GetCurrentNTUserId();
                    var prjCoPmf = projCloseOut.CLOSEOUTPROGRESSes.Where(x => x.SETUPCLOSEOUTROLE.SEQAPPROVAL == maxSeqApproval).FirstOrDefault();
                    prjCoPmf.SUBMITEDDATE = DateTime.Now;
                    prjCoPmf.SUBMITEDBY = GetCurrentNTUserId();
                }
                else if (projCloseOut.ISAPPROVED == false)
                {
                    projCloseOut.REJECTEDDATE = DateTime.Now;
                    projCloseOut.REJECTEDBY = GetCurrentNTUserId();
                    projCloseOut.REJECTEDREMARK = pCO.remarkRejectCLoseOut;
                    foreach (closeOutProgressObject cop in pCO.closeOutProgresses)
                    {
                        CLOSEOUTPROGRESS copr = projCloseOut.CLOSEOUTPROGRESSes.Where(x => x.OID == cop.id).FirstOrDefault();
                        if (cop.submitedDate == null)
                        {
                            copr.SUBMITEDDATE = null;
                            copr.SUBMITEDBY = null;
                        }
                    }
                }

                db.Entry(projCloseOut).State = EntityState.Modified;
                db.SaveChanges();

                ToEmailController tmc = new ToEmailController();

                //project manager
                string from = projCloseOut.PROJECT.PROJECTMANAGEREMAIL;

                //all members in project
                var usersLces = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_C_M_ES && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                var usersLees = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_E_I_ES && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                var usersDc = db.USERROLEs.Where(x => x.ROLE.ROLENAME == constants.RoleLeadDesigner_DC && x.USER.ISACTIVE == true).Select(x => new { userName = x.USER.USERNAME, email = x.USER.EMAIL });
                var usersPmc = db.USERASSIGNMENTs.Where(x => x.PROJECTID == pCO.projectId && x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR && x.ISACTIVE == true).Select(x => new { userName = x.EMPLOYEENAME, email = x.EMPLOYEEEMAIL });
                var usersPpc = db.USERASSIGNMENTs.Where(x => x.PROJECTID == pCO.projectId && x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER && x.ISACTIVE == true).Select(x => new { userName = x.EMPLOYEENAME, email = x.EMPLOYEEEMAIL });

                if (projCloseOut.ISAPPROVED.HasValue)
                    if (projCloseOut.ISAPPROVED.Value == true)
                    {
                        if (projCloseOut.PROJECT.PROJECTMANAGERID == pCO.userLoginBadgeNo) {
                            try {
                                var all = string.Join("; ", usersLces.Select(y => y.email).Union(usersLees.Select(y => y.email))
                                            .Union(usersLees.Select(y => y.email))
                                            .Union(usersDc.Select(y => y.email))
                                            .Union(usersPmc.Select(y => y.email))
                                            .Union(usersPpc.Select(y => y.email)));

                                var email_redac_approved = db.EMAILREDACTIONs.Where(x => x.CODE == constants.APPROVE_CLOSEOUT && x.ISACTIVE == true).FirstOrDefault();
                                string bodyApproved = email_redac_approved.BODY;
                                bodyApproved = bodyApproved
                                            .Replace("[NAME]", "All")
                                            .Replace("[PROJECT]", pCO.projectTitle)
                                            .Replace("[PROJECTNO]", pCO.projectNo)
                                            .Replace("[PROJECTMANAGER]", prj.PROJECTMANAGERNAME)
                                            .Replace("[LEADROLE]", constants.PM)
                                            .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                string subject_approved = email_redac_approved.SUBJECT;
                                if (!string.IsNullOrEmpty(pCO.comments)) bodyApproved = bodyApproved.Replace("[COMMENTS]", "<p> Comments: "+pCO.comments+"<p>");
                                else bodyApproved = bodyApproved.Replace("[COMMENTS]", pCO.comments);
                                if (!string.IsNullOrEmpty(pCO.reasonClosureType)) bodyApproved = bodyApproved.Replace("[REASON]", "<p> Reason: " + db.LOOKUPs.Where(x => x.TYPE == constants.closeOutReason && x.VALUE == pCO.reasonClosureType).FirstOrDefault().TEXT + "<p>");
                                else bodyApproved = bodyApproved.Replace("[REASON]", "");
                                tmc.EmailUser(all, prj.PROJECTMANAGEREMAIL, from, subject_approved, bodyApproved);

                            }
                            catch (Exception e) {
                                WriteLog(e.ToString());
                            }
                        }                            
                    }
                    else
                    {
                        
                        if (projCloseOut.PROJECT.PROJECTMANAGERID == pCO.userLoginBadgeNo)
                        {
                            try
                            {
                                var email_redac_rejected = db.EMAILREDACTIONs.Where(x => x.CODE == constants.REJECT_CLOSEOUT && x.ISACTIVE == true).FirstOrDefault();
                                string subject_rejected = email_redac_rejected.SUBJECT;
                                string bodyRejected = email_redac_rejected.BODY;

                                List<string> listRejectedRole = new List<string>();
                                List<int> listRoleIdRejected = new List<int>();
                                var cp = db.CLOSEOUTPROGRESSes.Where(x => x.PROJECTCLOSEOUTID == projCloseOut.OID && x.SUBMITEDDATE == null).ToList();
                                listRoleIdRejected = cp.Select(x => x.CLOSEOUTROLEID.Value).ToList();

                                foreach (int i in listRoleIdRejected)
                                {
                                    listRejectedRole.Add(db.SETUPCLOSEOUTROLEs.Find(i).DESCRIPTION);
                                }

                                foreach (string x in listRejectedRole)
                                {
                                    switch (x)
                                    {
                                        case constants.PM: // PM
                                            var bodyPM = bodyRejected
                                                .Replace("[NAME]", "All")
                                                .Replace("[ROLE]", constants.PM)
                                                .Replace("[PROJECTNO]", pCO.projectNo)
                                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                                .Replace("[REMARK]", pCO.remarkRejectCLoseOut)
                                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                            tmc.EmailUser(prj.PROJECTMANAGEREMAIL, "", from, subject_rejected, bodyPM);

                                            break;
                                        case constants.LD_C_M_ES: //Lead Designer Mechanical Civil
                                            var bodyLD_C_M_ES = bodyRejected
                                                .Replace("[NAME]", "All")
                                                .Replace("[ROLE]", constants.LD_C_M_ES)
                                                .Replace("[PROJECTNO]", pCO.projectNo)
                                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                                .Replace("[REMARK]", pCO.remarkRejectCLoseOut)
                                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                            tmc.EmailUser(string.Join("; ", usersLces.Select(y => y.email)), prj.PROJECTMANAGEREMAIL, from, subject_rejected, bodyLD_C_M_ES);
                                            break;
                                        case constants.LD_E_I_ES: //Lead Designer Electrical Instrument
                                            var bodyLD_E_I_ES = bodyRejected
                                                .Replace("[NAME]", "All")
                                                .Replace("[ROLE]", constants.LD_E_I_ES)
                                                .Replace("[PROJECTNO]", pCO.projectNo)
                                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                                .Replace("[REMARK]", pCO.remarkRejectCLoseOut)
                                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                            tmc.EmailUser(string.Join("; ", usersLees.Select(y => y.email)), prj.PROJECTMANAGEREMAIL, from, subject_rejected, bodyLD_E_I_ES);
                                            break;
                                        case constants.MC: //Material Coordinator
                                            var bodyMC = bodyRejected
                                                .Replace("[NAME]", "All")
                                                .Replace("[ROLE]", constants.MC)
                                                .Replace("[PROJECTNO]", pCO.projectNo)
                                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                                .Replace("[REMARK]", pCO.remarkRejectCLoseOut)
                                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                            tmc.EmailUser(string.Join("; ", usersPmc.Select(y => y.email)), prj.PROJECTMANAGEREMAIL, from, subject_rejected, bodyMC);
                                            break;
                                        case constants.PC: //Project Controller
                                            var bodyPC = bodyRejected
                                                .Replace("[NAME]", "All")
                                                .Replace("[ROLE]", constants.PC)
                                                .Replace("[PROJECTNO]", pCO.projectNo)
                                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                                .Replace("[REMARK]", pCO.remarkRejectCLoseOut)
                                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                            tmc.EmailUser(string.Join("; ", usersPpc.Select(y => y.email)), prj.PROJECTMANAGEREMAIL, from, subject_rejected, bodyPC);
                                            break;
                                        case constants.DC: //Document Controller
                                            var bodyDC = bodyRejected
                                                .Replace("[NAME]", "All")
                                                .Replace("[ROLE]", constants.DC)
                                                .Replace("[PROJECTNO]", pCO.projectNo)
                                                .Replace("[PROJECTNAME]", pCO.projectTitle)
                                                .Replace("[REMARK]", pCO.remarkRejectCLoseOut)
                                                .Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/ProjectCloseOutModuleForm.aspx?projectId=" + pCO.projectId);
                                            tmc.EmailUser(string.Join("; ", usersDc.Select(y => y.email)), prj.PROJECTMANAGEREMAIL, from, subject_rejected, bodyDC);
                                            break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                WriteLog(e.ToString());
                            }
                        }                        
                    }
            }
            return Request.CreateResponse(HttpStatusCode.OK, pCO);
        }
        [HttpGet]
        [ActionName("ListCloseOut")]
        public IHttpActionResult ListCloseOut(string projectNo, string description, string projectManager, string reason)
        {
            List<CloseOutDTO> result = new List<CloseOutDTO>();
            if (string.IsNullOrEmpty(projectNo) && string.IsNullOrEmpty(description) && string.IsNullOrEmpty(projectManager) && string.IsNullOrEmpty(reason))
            {
                return Ok(result);
            }

            List<string> listPM = new List<string>();
            if (!String.IsNullOrEmpty(projectManager)) listPM = projectManager.Split('|').ToList();
            List<string> listReasons = new List<string>();
            if (!String.IsNullOrEmpty(reason)) listReasons = reason.Split('|').ToList();

            try
            {
                IQueryable<PROJECT> listProject = db.PROJECTs.Where(d =>
                    //filter yang belum selesai close out
                    d.PROJECTCLOSEOUTs.Where(w => w.ISAPPROVED == true).Count() < 1 &&
                    //filter yang sedang proses close out
                    d.PROJECTCLOSEOUTs.Where(s1 => 
                            s1.CLOSEOUTPROGRESSes.Where(x => x.PLANDATE.HasValue).Count() > 0 || s1.REASONCLOSURETYPE != null || s1.APPROVEDBY != null || 
                            s1.APPROVEDDATE.HasValue || s1.COMMENTS != null || s1.ISAPPROVED != null).Count() > 0 &&
                    //filter by form
                    (String.IsNullOrEmpty(projectNo) ? 1 == 1 : d.PROJECTNO.Contains(projectNo))
                    && (String.IsNullOrEmpty(description) ? 1 == 1 : d.PROJECTDESCRIPTION.Contains(description))
                    && (String.IsNullOrEmpty(projectManager) ? 1 == 1 : listPM.Contains(d.PROJECTMANAGERID))
                    && (String.IsNullOrEmpty(reason) ? 1 == 1 : (d.PROJECTCLOSEOUTs.Count(e => listReasons.Contains(e.REASONCLOSURETYPE)) > 0)));
               
                foreach (PROJECT project in listProject)
                {
                    var closeOut = project.PROJECTCLOSEOUTs.FirstOrDefault();
                    if (closeOut != null && closeOut.CLOSEOUTPROGRESSes != null && closeOut.CLOSEOUTPROGRESSes.Count() > 0)
                    {
                        foreach (CLOSEOUTPROGRESS progress in closeOut.CLOSEOUTPROGRESSes)
                        {
                            CloseOutDTO pco = new CloseOutDTO();
                            pco.id = project.OID;
                            pco.projectNo = project.PROJECTNO;
                            pco.projectDesc = project.PROJECTDESCRIPTION;
                            pco.seqCloseOut = progress.SETUPCLOSEOUTROLE.SEQ;
                            pco.projectManager = project.PROJECTMANAGERNAME;
                            pco.reasonClosure = closeOut.REASONCLOSURETYPE != null ? closeOut.REASONCLOSURETYPE : "-";
                            LOOKUP lookupReason = db.LOOKUPs.Where(x => x.TYPE == constants.closeOutReason && x.VALUE == closeOut.REASONCLOSURETYPE).FirstOrDefault();
                            pco.reason = lookupReason != null ? lookupReason.TEXT : "-";
                            pco.comment = closeOut.COMMENTS != null ? closeOut.COMMENTS : "-";
                            pco.isApproved = progress.SUBMITEDDATE != null ? closeOut.ISAPPROVED : false;
                            pco.planDate = progress.PLANDATE;
                            pco.submittedDate = progress.SUBMITEDDATE;
                            pco.submittedBy = progress.SUBMITEDBY;
                            pco.picRole = "-";
                            pco.picName = "-";
                            if (progress.SETUPCLOSEOUTROLE != null)
                            {
                                pco.picRole = progress.SETUPCLOSEOUTROLE.DESCRIPTION;
                                pco.picProcessName = progress.SETUPCLOSEOUTROLE.PROCESSNAME;
                                pco.picName = getPicNameFromRole(project.OID, progress.CLOSEOUTROLEID.Value);
                            }
                            
                            result.Add(pco);
                        }
                    }
                    else
                    {
                        CloseOutDTO pco = new CloseOutDTO();
                        pco.id = project.OID;
                        pco.projectNo = project.PROJECTNO;
                        pco.projectDesc = project.PROJECTDESCRIPTION;
                        pco.seqCloseOut = 0;
                        pco.projectManager = project.PROJECTMANAGERNAME;
                        pco.reasonClosure = "-";
                        pco.reason = "-";
                        pco.comment = "-";
                        pco.isApproved = false;
                        if (closeOut != null)
                        {
                            pco.reasonClosure = closeOut.REASONCLOSURETYPE;
                            LOOKUP lookupReason = db.LOOKUPs.Where(x => x.TYPE == constants.closeOutReason && x.VALUE == closeOut.REASONCLOSURETYPE).FirstOrDefault();
                            pco.reason = lookupReason != null ? lookupReason.TEXT : "-";
                            pco.comment = closeOut.COMMENTS;
                            pco.isApproved = closeOut.ISAPPROVED;
                        }

                        result.Add(pco);
                    }
                }
                return Ok(result.OrderBy(o => o.projectNo).ThenBy(t => t.seqCloseOut));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
           
        }

        public class docFile
        {
            public int? closeOutDocumentId { get; set; }
            public int? closeOutProgressId { get; set; }
            public int? supportingDocumentId { get; set; }
        }
        public class DuplicateException : Exception
        {

        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

    public class CloseOutDTO
    {
        public int id { get; set; }
        public string projectNo { get; set; }
        public string projectDesc { get; set; }
        public int? seqCloseOut { get; set; }
        public string projectManager { get; set; }
        public string reasonClosure { get; set; }
        public string reason { get; set; }
        public string comment { get; set; }
        public Nullable<bool> isApproved { get; set; }
        public Nullable<System.DateTime> planDate { get; set; }
        public Nullable<System.DateTime> submittedDate { get; set; }
        public string submittedBy { get; set; }
        public string picRole { get; set; }
        public string picProcessName { get; set; }
        public string picName { get; set; }
    }
}