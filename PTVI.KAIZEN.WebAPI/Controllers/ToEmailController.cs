﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using Newtonsoft.Json;
using System.Web.Configuration;
using PTGDI.Email;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class usersToEmail
    {
        public string userName { get; set; }
        public string email { get; set; }
        public string position { get; set; }
    }

    public class objTrafficEmail
    {
        public int id { get; set; }
        public int? esPDId { get; set; }
        public string pmName { get; set; }
        public string pmEmail { get; set; }
        public string esTrafic { get; set; }
        public DateTime? esTraficDate { get; set; }
        public string handoverCloseOutTrafic { get; set; }
        public DateTime? handoverCloseOutTraficDate { get; set; }
        public IEnumerable<projectDocumentObject> pDoc { get; set; }
        public IEnumerable<IEnumerable<usersToEmail>> usersToEmail1{ get; set; }
        public IEnumerable<IEnumerable<usersToEmail>> usersToEmail2 { get; set; }
    }

    public class ToEmailController : KAIZENController
    {
        protected bool _debugMode = Convert.ToBoolean(global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL);
        protected String _debugEmailTo = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL_TO;
        protected String _debugEmailCC = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL_CC;
        protected String emailSender = WebConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];

        [HttpGet]
        [ActionName("EWPDocTrafficLightChange")]
        public string EWPDocTrafficLightChange(string param)
        {
            var emailTemplateYellow = db.EMAILREDACTIONs.Where(x => x.CODE == constants.EWPDocChangeYellow).FirstOrDefault();
            var emailTemplateRed = db.EMAILREDACTIONs.Where(x => x.CODE == constants.EWPDocChangeRed).FirstOrDefault();

            DateTime curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var prj = db.PROJECTs.Where(x => x.PROJECTDOCUMENTs.Count() > 0).Select(s => new
            {
                id = s.OID,
                pmName = s.PROJECTMANAGERNAME,
                pmEmail = s.PROJECTMANAGEREMAIL,
                esTraficDate = s.PROJECTDOCUMENTs.Where(w => w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && (w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_HOLD && w.STATUS != constants.STATUSIPROM_CANCELLED && !w.ACTUALFINISHDATE.HasValue)).Min(s2 => s2.PLANFINISHDATE),
                esTrafic = s.PROJECTDOCUMENTs.Where(w => w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER).Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                pDoc = s.PROJECTDOCUMENTs.Where(g=> (g.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER && (g.STATUS != constants.STATUSIPROM_COMPLETED && g.STATUS != constants.STATUSIPROM_HOLD && g.STATUS != constants.STATUSIPROM_CANCELLED && !g.ACTUALFINISHDATE.HasValue)) && g.PLANFINISHDATE
                == s.PROJECTDOCUMENTs.Where(w => (w.STATUS != constants.STATUSIPROM_COMPLETED && w.STATUS != constants.STATUSIPROM_CANCELLED && !w.ACTUALFINISHDATE.HasValue)).Min(s2 => s2.PLANFINISHDATE))
                .Select(w => new projectDocumentObject { documentByName = w.DOCUMENTBYNAME, documentByEmail = w.DOCUMENTBYEMAIL }).Distinct()
            }).ToList();

            List<objTrafficEmail> projects = prj.Select(s => new objTrafficEmail
            {
                id = s.id,
                pmName = s.pmName,
                pmEmail = s.pmEmail,
                esTrafic = s.esTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.esTraficDate.HasValue) ? traficStatus(s.esTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                esTraficDate = s.esTraficDate,
                pDoc = s.pDoc
            }).ToList<objTrafficEmail>();

            string st="";
            foreach (var s in projects)
            {
                if (s.esTraficDate != null &&(s.esTrafic == constants.TRAFFICLIGHT_YELLOW || s.esTrafic == constants.TRAFFICLIGHT_RED))
                {
                    if ((s.esTrafic==constants.TRAFFICLIGHT_RED && traficStatus(s.esTraficDate.Value.AddDays(+1), null, null)==constants.TRAFFICLIGHT_YELLOW)
                        || (s.esTrafic == constants.TRAFFICLIGHT_YELLOW && traficStatus(s.esTraficDate.Value.AddDays(+1), null, null) == constants.TRAFFICLIGHT_GREEN))
                    {
                        var recp = new {name = s.pmName, email = s.pmEmail };
                        string cc="";
                        foreach (var c in s.pDoc)
                        {
                            cc = cc + c.documentByEmail + ";";
                        }
                        cc.TrimEnd(';');
                        var emailTemplate = s.esTrafic == constants.TRAFFICLIGHT_RED ? emailTemplateRed : emailTemplateYellow;

                        EmailUser(recp.email, cc, emailSender, emailTemplate.SUBJECT, emailTemplate.BODY);
                        st = st + "Sending email to " + recp.name + " [" + recp.email + "], cc to " + cc;
                    }
                }
            }
            return st;
        }

        [HttpGet]
        [ActionName("COTrafficLightChange")]
        public string COTrafficLightChange(string param)
        {
            var emailTemplateYellow = db.EMAILREDACTIONs.Where(x => x.CODE == constants.CloseOutChangeYellow).FirstOrDefault();
            var emailTemplateRed = db.EMAILREDACTIONs.Where(x => x.CODE == constants.CloseOutChangeRed).FirstOrDefault();

            DateTime curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var prj = db.PROJECTs.Where(x => x.PROJECTDOCUMENTs.Count() > 0).Select(s => new
            {
                id = s.OID,
                pmName = s.PROJECTMANAGERNAME,
                pmEmail = s.PROJECTMANAGEREMAIL,
                handoverCloseOutTraficDate = s.PROJECTCLOSEOUTs.Select(s1 => s1.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue).Min(m => m.PLANDATE)).Min(),
                handoverCloseOutTrafic = s.PROJECTCLOSEOUTs.Count() == 0 ? constants.TRAFFICLIGHT_GREY : "",
                users1 = s.PROJECTCLOSEOUTs.Select(s1 => s1.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue && 
                w.PLANDATE == s.PROJECTCLOSEOUTs.Select(s2 => s2.CLOSEOUTPROGRESSes.Where(w2 => !w2.SUBMITEDDATE.HasValue).Min(m => m.PLANDATE)).Min()
                ).Select(cop => cop.SETUPCLOSEOUTROLE)
                .Join(db.ROLEs, k1 => k1.ROLENAME, k2 => k2.ROLENAME, (k1, k2) => new { k2.OID })
                .Join(db.USERROLEs, k1 => k1.OID, k2 => k2.ROLEID, (k1, k2) => new { k2.USERID })
                .Join(db.USERs, k1 => k1.USERID, k2 => k2.OID, (k1, k2) => new usersToEmail{ userName= k2.USERNAME, email = k2.EMAIL }).Distinct()
                ),
                users2 = s.PROJECTCLOSEOUTs.Select(s1 => s1.CLOSEOUTPROGRESSes.Where(w => !w.SUBMITEDDATE.HasValue &&
                w.PLANDATE == s.PROJECTCLOSEOUTs.Select(s2 => s2.CLOSEOUTPROGRESSes.Where(w2 => !w2.SUBMITEDDATE.HasValue).Min(m => m.PLANDATE)).Min()
                ).Select(cop => cop.SETUPCLOSEOUTROLE)
                .Join(db.USERASSIGNMENTs, k1 => new { k1.ASSIGNMENTTYPE, s1.PROJECTID}, k2 => new { k2.ASSIGNMENTTYPE, k2.PROJECTID}, 
                    (k1, k2) => new usersToEmail { userName = k2.EMPLOYEENAME, email = k2.EMPLOYEEEMAIL }).Distinct()
                )
            }).ToList();

            List<objTrafficEmail> projects = prj.Select(s => new objTrafficEmail
            {
                id = s.id,
                pmName = s.pmName,
                pmEmail = s.pmEmail,
                handoverCloseOutTrafic = s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_GREY ? constants.TRAFFICLIGHT_GREY : ((s.handoverCloseOutTraficDate.HasValue) ? traficStatus(s.handoverCloseOutTraficDate.Value, null, null) : constants.TRAFFICLIGHT_GREEN),
                handoverCloseOutTraficDate = s.handoverCloseOutTraficDate,
                usersToEmail1 = s.users1,
                usersToEmail2 = s.users2,
            }).ToList<objTrafficEmail>();

            string st = "";
            string cc = "";
            foreach (var s in projects)
            {
                if (s.handoverCloseOutTraficDate != null && (s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_YELLOW || s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_RED))
                {
                    if ((s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_RED && traficStatus(s.handoverCloseOutTraficDate.Value.AddDays(+1), null, null) == constants.TRAFFICLIGHT_YELLOW)
                        || (s.handoverCloseOutTrafic == constants.TRAFFICLIGHT_YELLOW && traficStatus(s.handoverCloseOutTraficDate.Value.AddDays(+1), null, null) == constants.TRAFFICLIGHT_GREEN))
                    {
                        var recp = new { name = s.pmName, email = s.pmEmail };
                        foreach (var c in s.usersToEmail1)
                            foreach (var d in c)
                            {
                                cc = cc + d.email + ";";
                            }
                        foreach (var c in s.usersToEmail2)
                            foreach (var d in c)
                            {
                                cc = cc + d.email + ";";
                            }
                        cc.TrimEnd(';');
                        var emailTemplate = s.esTrafic == constants.TRAFFICLIGHT_RED ? emailTemplateRed : emailTemplateYellow;
                        EmailUser(recp.email, cc, emailSender , emailTemplate.SUBJECT, emailTemplate.BODY);
                        st = st + "Sending email to " + recp.name + " [" + recp.email + "], cc to " + cc;
                    }
                }
            }
            return st;
        }

        public int EmailUser(string Recipient, string RecipientCC, string FromEmail, string subject, string body)
        {
            // Define variable
            //var strRecipient = Recipient;
            //var strRecipientCC = RecipientCC;
            //var strFromEmail = FromEmail;
            // 1. Prepare email engine
            EmailSender emailSender = new EmailSender();
            //            string emailFrom = System.Configuration.ConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];
            // 2. Prepare email body
            string emailBody = body;
            if (!string.IsNullOrEmpty(emailBody))
            {

                // if debug mode is on
                // change recipient with email debug
                //if (_debugMode)
                //{
                //    strRecipient = _debugEmailTo;
                //    strRecipientCC = _debugEmailCC;
                //    strFromEmail = _debugEmailTo;
                //}

                //if (!_debugMode)
                    emailSender.SendMail(Recipient, RecipientCC, FromEmail, "" + subject, emailBody);
                return 1;
            }
            else
            {
                throw new Exception("E-mail body is not found in the system. Please contact your administrator");
            }

        }
    }
}