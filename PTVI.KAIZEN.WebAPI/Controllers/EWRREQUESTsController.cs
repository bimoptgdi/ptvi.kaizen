﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using SourceCode.Workflow.Client;
using System.Data.Entity.Validation;
using PTVI.KAIZEN.WebAPI.K2Objects;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class EWRREQUESTsController : KAIZENController
    {
        // GET: api/EWRREQUESTs
        public IQueryable<EWRREQUEST> GetEWRREQUESTs()
        {
            return db.EWRREQUESTs;
        }

        // GET: api/EWRREQUESTs/5
        [ResponseType(typeof(EWRREQUEST))]
        public IHttpActionResult GetEWRREQUEST(int id)
        {
            EWRREQUEST eWRREQUEST = db.EWRREQUESTs.Find(id);
            if (eWRREQUEST == null)
            {
                return NotFound();
            }

            return Ok(eWRREQUEST);
        }

        // PUT: api/EWRREQUESTs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEWRREQUEST(int id, EWRREQUEST eWRREQUEST)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eWRREQUEST.OID)
            {
                return BadRequest();
            }

            db.Entry(eWRREQUEST).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EWRREQUESTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EWRREQUESTs
        [ResponseType(typeof(EWRREQUEST))]
        public IHttpActionResult PostEWRREQUEST(EWRREQUEST eWRREQUEST)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EWRREQUESTs.Add(eWRREQUEST);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = eWRREQUEST.OID }, eWRREQUEST);
        }

        // DELETE: api/EWRREQUESTs/5
        [ResponseType(typeof(EWRREQUEST))]
        public IHttpActionResult DeleteEWRREQUEST(int id)
        {
            EWRREQUEST eWRREQUEST = db.EWRREQUESTs.Find(id);
            if (eWRREQUEST == null)
            {
                return NotFound();
            }

            db.EWRREQUESTs.Remove(eWRREQUEST);
            db.SaveChanges();

            return Ok(eWRREQUEST);
        }

        [HttpPost]
        [ActionName("SubmitEWRRequest")]
        public IHttpActionResult SubmitEWRRequest(string param, [FromBody]EWRRequestDTO reqData)
        {
            if (reqData != null)
            {
                try
                {
                    string currNtUser = GetCurrentNTUserId();

                    EWRREQUEST ewrModel = new EWRREQUEST();
                    ewrModel.ACCCODE = reqData.ACCCODE;
                    ewrModel.AREA = reqData.AREA;
                    ewrModel.ASSIGNEDPM = reqData.ASSIGNEDPM;
                    ewrModel.BUDGETALLOC = reqData.BUDGETALLOC;
                    ewrModel.CREATEDBY = currNtUser;
                    ewrModel.CREATEDDATE = DateTime.Now;
                    ewrModel.DATECOMPLETION = reqData.DATECOMPLETION;
                    ewrModel.DATEISSUED = reqData.DATEISSUED;
                    ewrModel.NETWORKNO = reqData.NETWORKNO;
                    ewrModel.OBJECTIVE = reqData.OBJECTIVE;
                    ewrModel.OPERATIONALCP = reqData.OPERATIONALCP;
                    ewrModel.PROBLEMSTATEMENT = reqData.PROBLEMSTATEMENT;
                    ewrModel.PROJECTSPONSOR = reqData.PROJECTSPONSOR;
                    ewrModel.PROJECTSPONSORBN = reqData.PROJECTSPONSORBN;
                    ewrModel.PROJECTTITLE = reqData.PROJECTTITLE;
                    ewrModel.SPONSORLOCATION = reqData.SPONSORLOCATION;
                    ewrModel.SPONSORPHONENO = reqData.SPONSORPHONENO;
                    ewrModel.STATUS = constants.EWR_SUBMIT;
                    ewrModel.SUBJECT = reqData.SUBJECT;
                    ewrModel.SPONSOREMAIL = reqData.SPONSOREMAIL;
                    ewrModel.OPERATIONALCPEMAIL = reqData.OPERATIONALCPEMAIL;
                    ewrModel.OPERATIONALCPBN = reqData.OPERATIONALCPBN;
                    ewrModel.OPERATIONALCP2 = reqData.OPERATIONALCP2;
                    ewrModel.OPERATIONALCP2EMAIL = reqData.OPERATIONALCP2EMAIL;
                    ewrModel.OPERATIONALCP2BN = reqData.OPERATIONALCP2BN;
                    ewrModel.REQUESTORBN = reqData.REQUESTORBN;
                    ewrModel.REQUESTOREMAIL = reqData.REQUESTOREMAIL;
                    ewrModel.REQUESTORNAME = reqData.REQUESTORNAME;

                    //GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                    //if (generalParam != null)
                    //{
                    //    // Get user name and email
                    //    List<Dictionary<string, dynamic>> employeedata = GetJsonDataFromWebApi(generalParam.VALUE + "externaldata/executequerybyparam?param=QRY_VALE_EMPLOYEESCC&$filter= USERNAME eq " + "'" + currNtUser + "'");
                    //    if (employeedata.Count() > 0)
                    //    {
                    //        ewrModel.REQUESTORNAME = ((string)employeedata[0]["EMPLOYEE_NAME"]).Trim();
                    //        ewrModel.REQUESTOREMAIL = employeedata[0]["EMAIL"];
                    //        ewrModel.REQUESTORBN = employeedata[0]["BADGENO"];
                    //    }
                    //}
                    
                    db.EWRREQUESTs.Add(ewrModel);

                    if (reqData.CATEGORYOFWORK != null && reqData.CATEGORYOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.CATEGORYOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }
                    
                    if (reqData.TYPEOFREQUESTEDWORK != null && reqData.TYPEOFREQUESTEDWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.TYPEOFREQUESTEDWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }
                    
                    if (reqData.DETAILOFWORK != null && reqData.DETAILOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.DETAILOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }
                    
                    db.SaveChanges();

                    // call K2
                    K2ConnectController k2 = new K2ConnectController();
                    try
                    {
                        // call K2
                        string k2ConnectCode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ConnectCode;
                        ProcessInstance edfProcessInstance = k2.Connect(k2ConnectCode);

                        string k2AppID = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2AppID;
                        edfProcessInstance.DataFields[k2AppID].Value = ewrModel.OID;

                        k2.StartProcessInstance(edfProcessInstance);
                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog("K2 Problem:");
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    catch (Exception ex)
                    {
                        WriteLog("K2 Problem:");
                        WriteLog(ex.ToString());
                        throw new Exception(ex.ToString());
                    }
                    finally
                    {
                        k2.Disconnect();
                    }
                    // --call K2
                    return Ok(ewrModel);

                } catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                
            }
            return BadRequest("No data found");
        }
        [HttpPost]
        [ActionName("UpdateEWRRequest")]
        public IHttpActionResult UpdateEWRRequest(string param, [FromBody]EWRRequestDTO reqData)
        {
            if (reqData != null)
            {
                try
                {
                    
                    EWRREQUEST ewrModel = db.EWRREQUESTs.Find(reqData.OID);
                    ewrModel.ACCCODE = reqData.ACCCODE;
                    ewrModel.AREA = reqData.AREA;
                    ewrModel.ASSIGNEDPM = reqData.ASSIGNEDPM;
                    ewrModel.BUDGETALLOC = reqData.BUDGETALLOC;
                    //ewrModel.CREATEDBY = GetCurrentNTUserId();
                    //ewrModel.CREATEDDATE = DateTime.Now;
                    ewrModel.UPDATEDBY = GetCurrentNTUserId();
                    ewrModel.UPDATEDDATE = DateTime.Now;
                    ewrModel.DATECOMPLETION = reqData.DATECOMPLETION;
                    ewrModel.DATEISSUED = reqData.DATEISSUED;
                    ewrModel.NETWORKNO = reqData.NETWORKNO;
                    ewrModel.OBJECTIVE = reqData.OBJECTIVE;
                    ewrModel.OPERATIONALCP = reqData.OPERATIONALCP;
                    ewrModel.PROBLEMSTATEMENT = reqData.PROBLEMSTATEMENT;
                    ewrModel.PROJECTSPONSOR = reqData.PROJECTSPONSOR;
                    ewrModel.PROJECTSPONSORBN = reqData.PROJECTSPONSORBN;
                    ewrModel.PROJECTTITLE = reqData.PROJECTTITLE;
                    ewrModel.SPONSORLOCATION = reqData.SPONSORLOCATION;
                    ewrModel.SPONSORPHONENO = reqData.SPONSORPHONENO;
                    //ewrModel.STATUS = "";
                    ewrModel.SUBJECT = reqData.SUBJECT;

                    db.Entry(ewrModel).State = EntityState.Modified;

                    // remove all detail first
                    // then add with new update
                    if (ewrModel.EWRREQUESTDETAILs != null)
                    {
                        db.EWRREQUESTDETAILs.RemoveRange(ewrModel.EWRREQUESTDETAILs);
                    }

                    if (reqData.CATEGORYOFWORK != null)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.CATEGORYOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = GetCurrentNTUserId();
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }
                    
                    if (reqData.TYPEOFREQUESTEDWORK != null)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.TYPEOFREQUESTEDWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = GetCurrentNTUserId();
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }
                    
                    if (reqData.DETAILOFWORK != null)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.DETAILOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = GetCurrentNTUserId();
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    db.SaveChanges();

                    return Ok();

                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

            }
            return BadRequest("No data found");
        }

        [HttpPost]
        [ActionName("SubmitEWRDraftRequest")]
        public IHttpActionResult SubmitEWRDraftRequest(string param, [FromBody]EWRRequestDTO reqData)
        {
            if (reqData != null)
            {
                try
                {
                    string currNtUser = GetCurrentNTUserId();

                    EWRREQUEST ewrModel = db.EWRREQUESTs.Find(reqData.OID);
                    ewrModel.ACCCODE = reqData.ACCCODE;
                    ewrModel.AREA = reqData.AREA;
                    ewrModel.ASSIGNEDPM = reqData.ASSIGNEDPM;
                    ewrModel.BUDGETALLOC = reqData.BUDGETALLOC;
                    //ewrModel.CREATEDBY = currNtUser;
                    //ewrModel.CREATEDDATE = DateTime.Now;
                    ewrModel.UPDATEDBY = currNtUser;
                    ewrModel.UPDATEDDATE = DateTime.Now;
                    ewrModel.DATECOMPLETION = reqData.DATECOMPLETION;
                    ewrModel.DATEISSUED = reqData.DATEISSUED;
                    ewrModel.NETWORKNO = reqData.NETWORKNO;
                    ewrModel.OBJECTIVE = reqData.OBJECTIVE;
                    ewrModel.OPERATIONALCP = reqData.OPERATIONALCP;
                    ewrModel.PROBLEMSTATEMENT = reqData.PROBLEMSTATEMENT;
                    ewrModel.PROJECTSPONSOR = reqData.PROJECTSPONSOR;
                    ewrModel.PROJECTSPONSORBN = reqData.PROJECTSPONSORBN;
                    ewrModel.PROJECTTITLE = reqData.PROJECTTITLE;
                    ewrModel.SPONSORLOCATION = reqData.SPONSORLOCATION;
                    ewrModel.SPONSORPHONENO = reqData.SPONSORPHONENO;
                    ewrModel.STATUS = constants.EWR_SUBMIT;
                    ewrModel.SUBJECT = reqData.SUBJECT;
                    ewrModel.SPONSOREMAIL = reqData.SPONSOREMAIL;
                    ewrModel.OPERATIONALCPEMAIL = reqData.OPERATIONALCPEMAIL;
                    ewrModel.OPERATIONALCPBN = reqData.OPERATIONALCPBN;
                    ewrModel.OPERATIONALCP2 = reqData.OPERATIONALCP2;
                    ewrModel.OPERATIONALCP2EMAIL = reqData.OPERATIONALCP2EMAIL;
                    ewrModel.OPERATIONALCP2BN = reqData.OPERATIONALCP2BN;
                    ewrModel.REQUESTORBN = reqData.REQUESTORBN;
                    ewrModel.REQUESTOREMAIL = reqData.REQUESTOREMAIL;
                    ewrModel.REQUESTORNAME = reqData.REQUESTORNAME;

                    db.Entry(ewrModel).State = EntityState.Modified;

                    // remove semua detail yg ada
                    IQueryable<EWRREQUESTDETAIL> currentEWRDetails = db.EWRREQUESTDETAILs.Where(d => d.EWRREQUESTID == reqData.OID);
                    db.EWRREQUESTDETAILs.RemoveRange(currentEWRDetails);

                    if (reqData.CATEGORYOFWORK != null && reqData.CATEGORYOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.CATEGORYOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (reqData.TYPEOFREQUESTEDWORK != null && reqData.TYPEOFREQUESTEDWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.TYPEOFREQUESTEDWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (reqData.DETAILOFWORK != null && reqData.DETAILOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.DETAILOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = ewrModel;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    db.SaveChanges();

                    // call K2
                    K2ConnectController k2 = new K2ConnectController();
                    try
                    {
                        // call K2
                        string k2ConnectCode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ConnectCode;
                        ProcessInstance edfProcessInstance = k2.Connect(k2ConnectCode);

                        string k2AppID = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2AppID;
                        edfProcessInstance.DataFields[k2AppID].Value = ewrModel.OID;

                        k2.StartProcessInstance(edfProcessInstance);
                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog("K2 Problem:");
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    catch (Exception ex)
                    {
                        WriteLog("K2 Problem:");
                        WriteLog(ex.ToString());
                        throw new Exception(ex.ToString());
                    }
                    finally
                    {
                        k2.Disconnect();
                    }
                    // --call K2
                    return Ok(ewrModel);

                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

            }
            return BadRequest("No data found");
        }

        [HttpPost]
        [ActionName("SaveEWRRequestAsDraft")]
        public IHttpActionResult SaveEWRRequestAsDraft(int param, [FromBody]EWRRequestDTO reqData)
        {
            EWRREQUEST currEWR = db.EWRREQUESTs.Find(reqData.OID);
            if (currEWR == null)
            {
                try
                {
                    return Ok(AddRequestData(reqData, constants.EWR_DRAFT));
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

            }
            else
            {
                return Ok(UpdateRequestData(currEWR, reqData, constants.EWR_DRAFT));
            }
           
        }

        public EWRREQUEST AddRequestData(EWRRequestDTO reqData, String status)
        {
            try
            {
                string currNtUser = GetCurrentNTUserId();

                EWRREQUEST ewrModel = new EWRREQUEST();
                ewrModel.ACCCODE = reqData.ACCCODE;
                ewrModel.AREA = reqData.AREA;
                ewrModel.ASSIGNEDPM = reqData.ASSIGNEDPM;
                ewrModel.BUDGETALLOC = reqData.BUDGETALLOC;
                ewrModel.CREATEDBY = currNtUser;
                ewrModel.CREATEDDATE = DateTime.Now;
                ewrModel.DATECOMPLETION = reqData.DATECOMPLETION;
                ewrModel.DATEISSUED = reqData.DATEISSUED;
                ewrModel.NETWORKNO = reqData.NETWORKNO;
                ewrModel.OBJECTIVE = reqData.OBJECTIVE;
                ewrModel.OPERATIONALCP = reqData.OPERATIONALCP;
                ewrModel.PROBLEMSTATEMENT = reqData.PROBLEMSTATEMENT;
                ewrModel.PROJECTSPONSOR = reqData.PROJECTSPONSOR;
                ewrModel.PROJECTSPONSORBN = reqData.PROJECTSPONSORBN;
                ewrModel.PROJECTTITLE = reqData.PROJECTTITLE;
                ewrModel.SPONSORLOCATION = reqData.SPONSORLOCATION;
                ewrModel.SPONSORPHONENO = reqData.SPONSORPHONENO;
                ewrModel.STATUS = status;
                ewrModel.SUBJECT = reqData.SUBJECT;
                ewrModel.SPONSOREMAIL = reqData.SPONSOREMAIL;
                ewrModel.OPERATIONALCPEMAIL = reqData.OPERATIONALCPEMAIL;
                ewrModel.OPERATIONALCPBN = reqData.OPERATIONALCPBN;
                ewrModel.OPERATIONALCP2 = reqData.OPERATIONALCP2;
                ewrModel.OPERATIONALCP2EMAIL = reqData.OPERATIONALCP2EMAIL;
                ewrModel.OPERATIONALCP2BN = reqData.OPERATIONALCP2BN;
                ewrModel.REQUESTORBN = reqData.REQUESTORBN;
                ewrModel.REQUESTOREMAIL = reqData.REQUESTOREMAIL;
                ewrModel.REQUESTORNAME = reqData.REQUESTORNAME;

                db.EWRREQUESTs.Add(ewrModel);

                if (reqData.CATEGORYOFWORK != null && reqData.CATEGORYOFWORK.Count() > 0)
                {
                    foreach (EWRRequestDetailDTO dto in reqData.CATEGORYOFWORK)
                    {
                        EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                        ewrdetail.CREATEDBY = currNtUser;
                        ewrdetail.CREATEDDATE = DateTime.Now;
                        ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                        ewrdetail.EWRREQUEST = ewrModel;
                        ewrdetail.TYPE = dto.TYPE;
                        ewrdetail.VALUE = dto.VALUE;

                        db.EWRREQUESTDETAILs.Add(ewrdetail);
                    }
                }

                if (reqData.TYPEOFREQUESTEDWORK != null && reqData.TYPEOFREQUESTEDWORK.Count() > 0)
                {
                    foreach (EWRRequestDetailDTO dto in reqData.TYPEOFREQUESTEDWORK)
                    {
                        EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                        ewrdetail.CREATEDBY = currNtUser;
                        ewrdetail.CREATEDDATE = DateTime.Now;
                        ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                        ewrdetail.EWRREQUEST = ewrModel;
                        ewrdetail.TYPE = dto.TYPE;
                        ewrdetail.VALUE = dto.VALUE;

                        db.EWRREQUESTDETAILs.Add(ewrdetail);
                    }
                }

                if (reqData.DETAILOFWORK != null && reqData.DETAILOFWORK.Count() > 0)
                {
                    foreach (EWRRequestDetailDTO dto in reqData.DETAILOFWORK)
                    {
                        EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                        ewrdetail.CREATEDBY = currNtUser;
                        ewrdetail.CREATEDDATE = DateTime.Now;
                        ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                        ewrdetail.EWRREQUEST = ewrModel;
                        ewrdetail.TYPE = dto.TYPE;
                        ewrdetail.VALUE = dto.VALUE;

                        db.EWRREQUESTDETAILs.Add(ewrdetail);
                    }
                }

                db.SaveChanges();

                return ewrModel;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                return null;
            }
        }

        public EWRREQUEST UpdateRequestData(EWRREQUEST resRequest, EWRRequestDTO reqData, String status)
        {
            try
            {
                string currNtUser = GetCurrentNTUserId();

                if (resRequest != null)
                {
                    resRequest.DATEISSUED = reqData.DATEISSUED;
                    resRequest.AREA = reqData.AREA;
                    resRequest.SUBJECT = reqData.SUBJECT;
                    resRequest.ACCCODE = reqData.ACCCODE;
                    resRequest.NETWORKNO = reqData.NETWORKNO;
                    resRequest.BUDGETALLOC = reqData.BUDGETALLOC;
                    resRequest.DATECOMPLETION = reqData.DATECOMPLETION;
                    resRequest.PROBLEMSTATEMENT = reqData.PROBLEMSTATEMENT;
                    resRequest.OBJECTIVE = reqData.OBJECTIVE;
                    resRequest.PROJECTSPONSOR = reqData.PROJECTSPONSOR;
                    resRequest.PROJECTSPONSORBN = reqData.PROJECTSPONSORBN;
                    resRequest.SPONSOREMAIL = reqData.SPONSOREMAIL;
                    resRequest.SPONSORLOCATION = reqData.SPONSORLOCATION;
                    resRequest.SPONSORPHONENO = reqData.SPONSORPHONENO;
                    resRequest.OPERATIONALCP = reqData.OPERATIONALCP;
                    resRequest.OPERATIONALCPEMAIL = reqData.OPERATIONALCPEMAIL;
                    resRequest.OPERATIONALCPBN = reqData.OPERATIONALCPBN;
                    resRequest.OPERATIONALCP2 = reqData.OPERATIONALCP2;
                    resRequest.OPERATIONALCP2EMAIL = reqData.OPERATIONALCP2EMAIL;
                    resRequest.OPERATIONALCP2BN = reqData.OPERATIONALCP2BN;
                    resRequest.STATUS = status;
                    resRequest.UPDATEDBY = currNtUser;
                    resRequest.UPDATEDDATE = DateTime.Now;

                    db.Entry(resRequest).State = EntityState.Modified;

                    // remove semua detail yg ada
                    IQueryable<EWRREQUESTDETAIL> currentEWRDetails = db.EWRREQUESTDETAILs.Where(d => d.EWRREQUESTID == resRequest.OID);
                    db.EWRREQUESTDETAILs.RemoveRange(currentEWRDetails);

                    // insert hasil revisinya
                    if (reqData.CATEGORYOFWORK != null && reqData.CATEGORYOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.CATEGORYOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = resRequest;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (reqData.TYPEOFREQUESTEDWORK != null && reqData.TYPEOFREQUESTEDWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.TYPEOFREQUESTEDWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = resRequest;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (reqData.DETAILOFWORK != null && reqData.DETAILOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.DETAILOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = resRequest;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    db.SaveChanges(); //submit semua perubahan
                                      //=====================

                    
                }
                return resRequest;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                return null;
            }
        }

        [HttpGet]
        [ActionName("FindAllEWRRequest")]
        public IHttpActionResult FindAllEWRRequest(string param)
        {
            List<EWRRequestDTO> result = new List<EWRRequestDTO>();
            IQueryable<EWRREQUEST> listEwrRequest = db.EWRREQUESTs;
            foreach (EWRREQUEST ewrRequest in listEwrRequest)
            {
                EWRRequestDTO ewrDTO = new EWRRequestDTO();
                ewrDTO.ACCCODE = ewrRequest.ACCCODE;
                ewrDTO.AREA = ewrRequest.AREA;
                ewrDTO.ASSIGNEDPM = ewrRequest.ASSIGNEDPM;
                ewrDTO.BUDGETALLOC = ewrRequest.BUDGETALLOC;
                ewrDTO.DATECOMPLETION = ewrRequest.DATECOMPLETION;
                ewrDTO.DATEISSUED = ewrRequest.DATEISSUED;
                ewrDTO.NETWORKNO = ewrRequest.NETWORKNO;
                ewrDTO.OBJECTIVE = ewrRequest.OBJECTIVE;
                ewrDTO.OID = ewrRequest.OID;
                ewrDTO.OPERATIONALCP = ewrRequest.OPERATIONALCP;
                ewrDTO.PROBLEMSTATEMENT = ewrRequest.PROBLEMSTATEMENT;
                ewrDTO.PROJECTSPONSOR = ewrRequest.PROJECTSPONSOR;
                ewrDTO.PROJECTTITLE = ewrRequest.PROJECTTITLE;
                ewrDTO.SPONSORLOCATION = ewrRequest.SPONSORLOCATION;
                ewrDTO.SPONSORPHONENO = ewrRequest.SPONSORPHONENO;
                //result.STATUS = ewrRequest.STATUS;
                ewrDTO.SUBJECT = ewrRequest.SUBJECT;

                List<EWRRequestDetailDTO> listCategory = new List<EWRRequestDetailDTO>();
                List<EWRRequestDetailDTO> listRequest = new List<EWRRequestDetailDTO>();
                List<EWRRequestDetailDTO> listDetail = new List<EWRRequestDetailDTO>();
                foreach (EWRREQUESTDETAIL ewrDetail in ewrRequest.EWRREQUESTDETAILs)
                {
                    EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                    detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                    detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                    detailDTO.OID = ewrDetail.OID;
                    detailDTO.TYPE = ewrDetail.TYPE;
                    detailDTO.VALUE = ewrDetail.VALUE;

                    if (ewrDetail.TYPE == "categoryofwork")
                    {
                        listCategory.Add(detailDTO);
                    }
                    else if (ewrDetail.TYPE == "requestofwork")
                    {
                        listRequest.Add(detailDTO);
                    }
                    else if (ewrDetail.TYPE == "detailofwork")
                    {
                        listDetail.Add(detailDTO);
                    }
                }

                ewrDTO.CATEGORYOFWORK = listCategory;
                ewrDTO.TYPEOFREQUESTEDWORK = listRequest;
                ewrDTO.DETAILOFWORK = listDetail;

                result.Add(ewrDTO);
            }

            return Ok(result);
        }

        [HttpGet]
        [ActionName("FindEWRByID")]
        public IHttpActionResult FindEWRByID(int param)
        {
            EWRRequestDTO result = new EWRRequestDTO();
            try
            {
                EWRREQUEST ewrRequest = db.EWRREQUESTs.Find(param);
                if (ewrRequest != null)
                {
                    result.OID = ewrRequest.OID;
                    result.ACCCODE = ewrRequest.ACCCODE;
                    result.AREA = ewrRequest.AREA;
                    result.ASSIGNEDPM = ewrRequest.ASSIGNEDPM;
                    result.BUDGETALLOC = ewrRequest.BUDGETALLOC;
                    result.DATECOMPLETION = ewrRequest.DATECOMPLETION;
                    result.DATEISSUED = ewrRequest.DATEISSUED;
                    result.NETWORKNO = ewrRequest.NETWORKNO;
                    result.OBJECTIVE = ewrRequest.OBJECTIVE;
                    result.OID = ewrRequest.OID;
                    result.OPERATIONALCP = ewrRequest.OPERATIONALCP;
                    result.OPERATIONALCPBN = ewrRequest.OPERATIONALCPBN;
                    result.OPERATIONALCPEMAIL = ewrRequest.OPERATIONALCPEMAIL;
                    result.OPERATIONALCP2 = ewrRequest.OPERATIONALCP2;
                    result.OPERATIONALCP2BN = ewrRequest.OPERATIONALCP2BN;
                    result.OPERATIONALCP2EMAIL = ewrRequest.OPERATIONALCP2EMAIL;
                    result.PROBLEMSTATEMENT = ewrRequest.PROBLEMSTATEMENT;
                    result.PROJECTSPONSOR = ewrRequest.PROJECTSPONSOR;
                    result.PROJECTTITLE = ewrRequest.PROJECTTITLE;
                    result.SPONSORLOCATION = ewrRequest.SPONSORLOCATION;
                    result.SPONSORPHONENO = ewrRequest.SPONSORPHONENO;
                    result.SPONSOREMAIL = ewrRequest.SPONSOREMAIL;
                    result.PROJECTSPONSORBN = ewrRequest.PROJECTSPONSORBN;
                    result.STATUS = ewrRequest.STATUS;
                    result.SUBJECT = ewrRequest.SUBJECT;

                    List<EWRRequestDetailDTO> listCategory = new List<EWRRequestDetailDTO>();
                    List<EWRRequestDetailDTO> listRequest = new List<EWRRequestDetailDTO>();
                    List<EWRRequestDetailDTO> listDetail = new List<EWRRequestDetailDTO>();
                    
                    foreach (EWRREQUESTDETAIL ewrDetail in ewrRequest.EWRREQUESTDETAILs)
                    {
                        EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                        detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                        detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                        detailDTO.OID = ewrDetail.OID;
                        detailDTO.TYPE = ewrDetail.TYPE;
                        detailDTO.VALUE = ewrDetail.VALUE;

                        if (ewrDetail.TYPE == "categoryofwork")
                        {
                            listCategory.Add(detailDTO);
                        }
                        else if (ewrDetail.TYPE == "requestofwork")
                        {
                            listRequest.Add(detailDTO);
                        }
                        else if (ewrDetail.TYPE == "detailofwork")
                        {
                            listDetail.Add(detailDTO);
                        }
                        //else if(ewrDetail.TYPE == "projecttype")
                        //{
                        //    listProject.Add(detailDTO);
                        //}
                    }

                    result.CATEGORYOFWORK = listCategory;
                    result.TYPEOFREQUESTEDWORK = listRequest;
                    result.DETAILOFWORK = listDetail;
                    //result.PROJECTTYPE = listProject;
                }

                IQueryable<USERASSIGNMENT> listUser = db.USERASSIGNMENTs.Where(d => d.EWRID == param && d.ASSIGNMENTTYPE == constants.EWR_PARTICIPANT);
                List<USERASSIGNMENT> listUserParticipant = new List<USERASSIGNMENT>();
                foreach(USERASSIGNMENT userAssign in listUser)
                {
                    listUserParticipant.Add(userAssign);
                }

                result.USERPARTICIPANTS = listUserParticipant;

                IQueryable<USERASSIGNMENT> listUserD = db.USERASSIGNMENTs.Where(d => d.EWRID == param && d.ASSIGNMENTTYPE == constants.EWR_DISTRIBUTION);
                List<USERASSIGNMENT> listUserDistribution = new List<USERASSIGNMENT>();
                foreach (USERASSIGNMENT userAssign in listUserD)
                {
                    listUserDistribution.Add(userAssign);
                }

                result.USERDISTRIBUTIONS = listUserDistribution;

                IQueryable<SUPPORTINGDOCUMENT> listSupportingDoc = db.SUPPORTINGDOCUMENTs.Where(d => d.TYPEID == param && d.TYPENAME == constants.DOC_EWR_TYPE);
                if (listSupportingDoc.Count() > 0)
                {
                    result.SUPPORTINGDOCUMENTS = listSupportingDoc.ToList();
                }

                //----
                List<K2History> listK2History = new List<K2History>();
                try
                {

                    GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                    if (generalParam != null)
                    {
                        
                        listK2History = GetListFromExternalData<K2History>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_WORKLIST_HISTORY_BYWFIDIN%7C@wfID=" + ewrRequest.WF_ID);
                        
                    }

                    result.K2Histories = listK2History;
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                }

                //MOMEWR
                MOMEWR momResult = db.MOMEWRs.FirstOrDefault(d => d.EWRREQUESTID == ewrRequest.OID);
                if (momResult != null)
                {
                    EWRMomDTO ewrMom = new EWRMomDTO();
                    ewrMom.MOMDATE = momResult.MOMDATE;
                    ewrMom.MOMTIME = momResult.MOMTIME;
                    ewrMom.ACTIONTIMELINE = momResult.ACTIONTIMELINE;
                    ewrMom.BACKGROUND = momResult.BACKGROUND;
                    ewrMom.DATAREQUIRED = momResult.DATAREQUIRED;
                    ewrMom.DISTRIBUTIONS = momResult.DISTRIBUTION;
                    ewrMom.EWRNO = momResult.EWRNO;
                    ewrMom.EWRREQUESTID = (int)momResult.EWRREQUESTID;
                    ewrMom.OID = momResult.OID;
                    ewrMom.PROJECTDELIVERABLE = momResult.PROJECTDELIVERABLE;
                    ewrMom.PROJECTMANAGER = momResult.PROJECTMANAGER;
                    ewrMom.PROJECTMANAGERBN = momResult.PROJECTMANAGERBN;
                    ewrMom.PROJECTMANAGEREMAIL = momResult.PROJECTMANAGEREMAIL;
                    ewrMom.PROJECTTITLE = momResult.PROJECTTITLE;
                    ewrMom.RECORDEDBY = momResult.RECORDEDBY;
                    ewrMom.RECORDEDBYBN = momResult.RECORDEDBYBN;
                    ewrMom.RECORDEDBYEMAIL = momResult.RECORDEDBYEMAIL;
                    ewrMom.SCOPEOFWORK = momResult.SCOPEOFWORK;
                    ewrMom.SPONSORCP = momResult.SPONSORCP;
                    ewrMom.VENUE = momResult.VENUE;
                    ewrMom.MOMEWRNO = momResult.MOMEWRNO;

                    List<EWRRequestDetailDTO> listProject = new List<EWRRequestDetailDTO>();
                    foreach (EWRREQUESTDETAIL ewrDetail in db.EWRREQUESTDETAILs.Where(d => d.EWRMOMID == momResult.OID))
                    {
                        EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                        detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                        //detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                        detailDTO.OID = ewrDetail.OID;
                        detailDTO.TYPE = ewrDetail.TYPE;
                        detailDTO.VALUE = ewrDetail.VALUE;

                        listProject.Add(detailDTO);
                        //else if(ewrDetail.TYPE == "projecttype")
                        //{
                        //    listProject.Add(detailDTO);
                        //}
                    }

                    result.EWRMOM = ewrMom;
                    result.PROJECTTYPE = listProject;
                }
                

                //----
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            
           
            return Ok(result);
        }

        [HttpGet]
        [ActionName("FindEWRRequestByBN")]
        public IHttpActionResult FindEWRRequestByBN(string param)
        {
            List<EWRRequestDTO> result = new List<EWRRequestDTO>();
            IQueryable<EWRREQUEST> listEwrRequest = db.EWRREQUESTs.Where(d => d.REQUESTORBN == param);
            foreach (EWRREQUEST ewrRequest in listEwrRequest)
            {
                EWRRequestDTO ewrDTO = new EWRRequestDTO();
                ewrDTO.ACCCODE = ewrRequest.ACCCODE;
                ewrDTO.AREA = ewrRequest.AREA;
                ewrDTO.ASSIGNEDPM = ewrRequest.ASSIGNEDPM;
                ewrDTO.BUDGETALLOC = ewrRequest.BUDGETALLOC;
                ewrDTO.DATECOMPLETION = ewrRequest.DATECOMPLETION;
                ewrDTO.DATEISSUED = ewrRequest.DATEISSUED;
                ewrDTO.NETWORKNO = ewrRequest.NETWORKNO;
                ewrDTO.OBJECTIVE = ewrRequest.OBJECTIVE;
                ewrDTO.OID = ewrRequest.OID;
                ewrDTO.OPERATIONALCP = ewrRequest.OPERATIONALCP;
                ewrDTO.PROBLEMSTATEMENT = ewrRequest.PROBLEMSTATEMENT;
                ewrDTO.PROJECTSPONSOR = ewrRequest.PROJECTSPONSOR;
                ewrDTO.PROJECTTITLE = ewrRequest.PROJECTTITLE;
                ewrDTO.SPONSORLOCATION = ewrRequest.SPONSORLOCATION;
                ewrDTO.SPONSORPHONENO = ewrRequest.SPONSORPHONENO;
                ewrDTO.STATUS = ewrRequest.STATUS;
                ewrDTO.SUBJECT = ewrRequest.SUBJECT;

                List<EWRRequestDetailDTO> listCategory = new List<EWRRequestDetailDTO>();
                List<EWRRequestDetailDTO> listRequest = new List<EWRRequestDetailDTO>();
                List<EWRRequestDetailDTO> listDetail = new List<EWRRequestDetailDTO>();
                foreach (EWRREQUESTDETAIL ewrDetail in ewrRequest.EWRREQUESTDETAILs)
                {
                    EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                    detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                    detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                    detailDTO.OID = ewrDetail.OID;
                    detailDTO.TYPE = ewrDetail.TYPE;
                    detailDTO.VALUE = ewrDetail.VALUE;

                    if (ewrDetail.TYPE == "categoryofwork")
                    {
                        listCategory.Add(detailDTO);
                    }
                    else if (ewrDetail.TYPE == "requestofwork")
                    {
                        listRequest.Add(detailDTO);
                    }
                    else if (ewrDetail.TYPE == "detailofwork")
                    {
                        listDetail.Add(detailDTO);
                    }
                }

                ewrDTO.CATEGORYOFWORK = listCategory;
                ewrDTO.TYPEOFREQUESTEDWORK = listRequest;
                ewrDTO.DETAILOFWORK = listDetail;

                IQueryable<SUPPORTINGDOCUMENT> listSupportingDoc = db.SUPPORTINGDOCUMENTs.Where(d => d.TYPEID == ewrRequest.OID && d.TYPENAME == constants.DOC_EWR_TYPE);
                if (listSupportingDoc.Count() > 0)
                {
                    ewrDTO.SUPPORTINGDOCUMENTS = listSupportingDoc.ToList();
                }

                //MOMEWR
                MOMEWR momResult = db.MOMEWRs.FirstOrDefault(d => d.EWRREQUESTID == ewrRequest.OID);
                if (momResult != null)
                {
                    EWRMomDTO ewrMom = new EWRMomDTO();
                    ewrMom.MOMDATE = momResult.MOMDATE;
                    ewrMom.MOMTIME = momResult.MOMTIME;
                    ewrMom.ACTIONTIMELINE = momResult.ACTIONTIMELINE;
                    ewrMom.BACKGROUND = momResult.BACKGROUND;
                    ewrMom.DATAREQUIRED = momResult.DATAREQUIRED;
                    ewrMom.DISTRIBUTIONS = momResult.DISTRIBUTION;
                    ewrMom.EWRNO = momResult.EWRNO;
                    ewrMom.EWRREQUESTID = (int)momResult.EWRREQUESTID;
                    ewrMom.OID = momResult.OID;
                    ewrMom.PROJECTDELIVERABLE = momResult.PROJECTDELIVERABLE;
                    ewrMom.PROJECTMANAGER = momResult.PROJECTMANAGER;
                    ewrMom.PROJECTMANAGERBN = momResult.PROJECTMANAGERBN;
                    ewrMom.PROJECTMANAGEREMAIL = momResult.PROJECTMANAGEREMAIL;
                    ewrMom.PROJECTTITLE = momResult.PROJECTTITLE;
                    ewrMom.RECORDEDBY = momResult.RECORDEDBY;
                    ewrMom.RECORDEDBYBN = momResult.RECORDEDBYBN;
                    ewrMom.RECORDEDBYEMAIL = momResult.RECORDEDBYEMAIL;
                    ewrMom.SCOPEOFWORK = momResult.SCOPEOFWORK;
                    ewrMom.SPONSORCP = momResult.SPONSORCP;
                    ewrMom.VENUE = momResult.VENUE;
                    ewrMom.MOMEWRNO = momResult.MOMEWRNO;

                    List<EWRRequestDetailDTO> listProject = new List<EWRRequestDetailDTO>();
                    foreach (EWRREQUESTDETAIL ewrDetail in db.EWRREQUESTDETAILs.Where(d => d.EWRMOMID == momResult.OID))
                    {
                        EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                        detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                        //detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                        detailDTO.OID = ewrDetail.OID;
                        detailDTO.TYPE = ewrDetail.TYPE;
                        detailDTO.VALUE = ewrDetail.VALUE;

                        listProject.Add(detailDTO);
                        //else if(ewrDetail.TYPE == "projecttype")
                        //{
                        //    listProject.Add(detailDTO);
                        //}
                    }

                    ewrDTO.EWRMOM = ewrMom;
                    ewrDTO.PROJECTTYPE = listProject;
                }

                result.Add(ewrDTO);
            }

            return Ok(result);
        }

        [HttpPost]
        [ActionName("ApproveRequest")]
        public IHttpActionResult ApproveRequest(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                EWRREQUEST resRequest = db.EWRREQUESTs.Find(param);
                if (resRequest != null)
                {
                    //resRequest.STATUS = GetNextApprovalStatus(resRequest.STATUS);
                    //db.Entry(resRequest).State = EntityState.Modified;

                    //db.SaveChanges();
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        //List<K2WorklistItem> k2WorklistItem = k2Management.FilterWorkListItems(
                        //            SourceCode.Workflow.Management.WorklistFields.Folio,
                        //            SourceCode.Workflow.Management.Criteria.Comparison.Like,
                        //            "%EWR%" + (resRequest.ACCCODE + "%" + resRequest.NETWORKNO) + "%", resRequest.WF_ID.HasValue ? resRequest.WF_ID.ToString() : null).ToList();

                        //string sn = k2WorklistItem.
                        //            Where(d => d.procId == resRequest.WF_ID &&
                        //            d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                        //            Select(d => d.serialNo).FirstOrDefault();

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WF_ID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WF_ID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();
                        //WriteLog("Serial Number: " + sn);
                        //WriteLog("WFID: " + resRequest.WF_ID);
                        //WriteLog("Next Approver: " + nextApprover);
                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_APPROVE_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();

                        db.SaveChanges(); //submit semua perubahan

                        // simpan history nya
                        //ProgressHistoryController progressHistory = new ProgressHistoryController();
                        //progressHistory.UpdateRequestHistory(resRequest.RESERVATIONNO, null, Constants.STATUS_APPROVE, resModel.approvalRemark, GetCurrentNTUserId());
                        //--
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("RejectRequest")]
        public IHttpActionResult RejectRequest(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                EWRREQUEST resRequest = db.EWRREQUESTs.Find(param);
                if (resRequest != null)
                {
                    //resRequest.STATUS = constants.STATUS_REJECT_K2;
                    //db.Entry(resRequest).State = EntityState.Modified;

                    //db.SaveChanges();
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        //string nextApprover = k2NextApprover;

                        string nextApprover = getNextK2Approver("");

                        //List<K2WorklistItem> k2WorklistItem = k2Management.FilterWorkListItems(
                        //            SourceCode.Workflow.Management.WorklistFields.Folio,
                        //            SourceCode.Workflow.Management.Criteria.Comparison.Like,
                        //            "%EWR%" + (resRequest.ACCCODE + "%" + resRequest.NETWORKNO) + "%", resRequest.WF_ID.HasValue ? resRequest.WF_ID.ToString() : null).ToList();

                        //string sn = k2WorklistItem.
                        //            Where(d => d.procId == resRequest.WF_ID &&
                        //            d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                        //            Select(d => d.serialNo).FirstOrDefault();
                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WF_ID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WF_ID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REJECT_K2, resModel.Remark, null);
                        //}

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();

                        //db.SaveChanges(); //submit semua perubahan

                        // simpan history nya
                        //ProgressHistoryController progressHistory = new ProgressHistoryController();
                        //progressHistory.UpdateRequestHistory(resRequest.RESERVATIONNO, null, Constants.STATUS_APPROVE, resModel.approvalRemark, GetCurrentNTUserId());
                        //--
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("RequestNeedRevised")]
        public IHttpActionResult RequestNeedRevised(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                EWRREQUEST resRequest = db.EWRREQUESTs.Find(param);
                if (resRequest != null)
                {
                    //resRequest.STATUS = GetNextApprovalStatus(resRequest.STATUS);
                    //db.Entry(resRequest).State = EntityState.Modified;

                    //db.SaveChanges();
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        //List<K2WorklistItem> k2WorklistItem = k2Management.FilterWorkListItems(
                        //            SourceCode.Workflow.Management.WorklistFields.Folio,
                        //            SourceCode.Workflow.Management.Criteria.Comparison.Like,
                        //            "%EWR%" + (resRequest.ACCCODE + "%" + resRequest.NETWORKNO) + "%", resRequest.WF_ID.HasValue ? resRequest.WF_ID.ToString() : null).ToList();

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WF_ID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WF_ID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        //string sn = k2WorklistItem.
                        //            Where(d => d.procId == resRequest.WF_ID &&
                        //            d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                        //            Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REVISE_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();

                        //db.SaveChanges(); //submit semua perubahan

                        // simpan history nya
                        //ProgressHistoryController progressHistory = new ProgressHistoryController();
                        //progressHistory.UpdateRequestHistory(resRequest.RESERVATIONNO, null, Constants.STATUS_APPROVE, resModel.approvalRemark, GetCurrentNTUserId());
                        //--
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("ReviseRequest")]
        public IHttpActionResult ReviseRequest(int param, [FromBody]EWRRequestDTO reqData)
        {
            try
            {
                string currNtUser = GetCurrentNTUserId();

                EWRREQUEST resRequest = db.EWRREQUESTs.Find(param);
                if (resRequest != null)
                {
                    resRequest.DATEISSUED = reqData.DATEISSUED;
                    resRequest.AREA = reqData.AREA;
                    resRequest.SUBJECT = reqData.SUBJECT;
                    resRequest.ACCCODE = reqData.ACCCODE;
                    resRequest.NETWORKNO = reqData.NETWORKNO;
                    resRequest.BUDGETALLOC = reqData.BUDGETALLOC;
                    resRequest.DATECOMPLETION = reqData.DATECOMPLETION;
                    resRequest.PROBLEMSTATEMENT = reqData.PROBLEMSTATEMENT;
                    resRequest.OBJECTIVE = reqData.OBJECTIVE;
                    resRequest.PROJECTSPONSOR = reqData.PROJECTSPONSOR;
                    resRequest.PROJECTSPONSORBN = reqData.PROJECTSPONSORBN;
                    resRequest.SPONSOREMAIL = reqData.SPONSOREMAIL;
                    resRequest.SPONSORLOCATION = reqData.SPONSORLOCATION;
                    resRequest.SPONSORPHONENO = reqData.SPONSORPHONENO;
                    resRequest.OPERATIONALCP = reqData.OPERATIONALCP;
                    resRequest.OPERATIONALCPEMAIL = reqData.OPERATIONALCPEMAIL;
                    resRequest.OPERATIONALCPBN = reqData.OPERATIONALCPBN;
                    resRequest.OPERATIONALCP2 = reqData.OPERATIONALCP2;
                    resRequest.OPERATIONALCP2EMAIL = reqData.OPERATIONALCP2EMAIL;
                    resRequest.OPERATIONALCP2BN = reqData.OPERATIONALCP2BN;

                    resRequest.UPDATEDBY = currNtUser;
                    resRequest.UPDATEDDATE = DateTime.Now;

                    db.Entry(resRequest).State = EntityState.Modified;

                    // remove semua detail yg ada
                    IQueryable<EWRREQUESTDETAIL> currentEWRDetails = db.EWRREQUESTDETAILs.Where(d => d.EWRREQUESTID == resRequest.OID);
                    db.EWRREQUESTDETAILs.RemoveRange(currentEWRDetails);

                    // insert hasil revisinya
                    if (reqData.CATEGORYOFWORK != null && reqData.CATEGORYOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.CATEGORYOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = resRequest;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (reqData.TYPEOFREQUESTEDWORK != null && reqData.TYPEOFREQUESTEDWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.TYPEOFREQUESTEDWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = resRequest;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    if (reqData.DETAILOFWORK != null && reqData.DETAILOFWORK.Count() > 0)
                    {
                        foreach (EWRRequestDetailDTO dto in reqData.DETAILOFWORK)
                        {
                            EWRREQUESTDETAIL ewrdetail = new EWRREQUESTDETAIL();
                            ewrdetail.CREATEDBY = currNtUser;
                            ewrdetail.CREATEDDATE = DateTime.Now;
                            ewrdetail.DESCRIPTION = dto.DESCRIPTION;
                            ewrdetail.EWRREQUEST = resRequest;
                            ewrdetail.TYPE = dto.TYPE;
                            ewrdetail.VALUE = dto.VALUE;

                            db.EWRREQUESTDETAILs.Add(ewrdetail);
                        }
                    }

                    db.SaveChanges(); //submit semua perubahan
                                      //=====================

                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        //string nextApprover = k2NextApprover;

                        string nextApprover = getNextK2Approver("");

                        //List<K2WorklistItem> k2WorklistItem = k2Management.FilterWorkListItems(
                        //            SourceCode.Workflow.Management.WorklistFields.Folio,
                        //            SourceCode.Workflow.Management.Criteria.Comparison.Like,
                        //            "%EWR%" + (resRequest.ACCCODE + "%" + resRequest.NETWORKNO) + "%", resRequest.WF_ID.HasValue ? resRequest.WF_ID.ToString() : null).ToList();
                        K2ConnectController connectK2 = new K2ConnectController();

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        reqData.WF_ID = resRequest.WF_ID; //set wf id nya

                        k2Object.Add(reqData);
                        connectK2.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WF_ID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REVISE_K2, reqData.Remark, null);
                        //}

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();


                        // simpan history nya
                        //ProgressHistoryController progressHistory = new ProgressHistoryController();
                        //progressHistory.UpdateRequestHistory(resRequest.RESERVATIONNO, null, Constants.STATUS_APPROVE, resModel.approvalRemark, GetCurrentNTUserId());
                        //--
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("FindDataByCriteria")]
        public IHttpActionResult FindDataByCriteria(string param, [FromBody]CommonDTO inquiryData)
        {
            bool isUserAdmin = false;
            USER usr = db.USERs.FirstOrDefault(d => d.BADGENO == inquiryData.REQUESTBY);
            if (usr != null)
            {
                isUserAdmin = usr.USERROLEs.Count(e => e.ROLE.OID == 1) > 0;
            }

            //....
            List<String> listProjectManager = new List<String>();
            List<String> listProjectSponsor = new List<String>();
            if (!String.IsNullOrEmpty(inquiryData.PROJECTMANAGER))
            {
                foreach(String pm in inquiryData.PROJECTMANAGER.Split(';'))
                {
                    listProjectManager.Add(pm);
                }
            }

            if (!String.IsNullOrEmpty(inquiryData.PROJECTSPONSOR))
            {
                foreach(String ps in inquiryData.PROJECTSPONSOR.Split(';'))
                {
                    listProjectSponsor.Add(ps);
                }
            }
            //....
            IQueryable<EWRREQUEST> listEWR = db.EWRREQUESTs.Where(d =>
                  (d.REQUESTORBN == inquiryData.REQUESTBY || String.IsNullOrEmpty(inquiryData.REQUESTBY))
                  && (d.SUBJECT.ToLower().Contains(inquiryData.SUBJECT.ToLower()) || String.IsNullOrEmpty(inquiryData.SUBJECT))
                  && (d.ACCCODE.ToLower().Contains(inquiryData.ACCCODE.ToLower()) || String.IsNullOrEmpty(inquiryData.ACCCODE))
                  && (db.MOMEWRs.Where(f => f.EWRREQUESTID == d.OID).Count(e => listProjectManager.Contains(e.PROJECTMANAGERBN)) > 0 || listProjectManager.Count() == 0)
                  && (listProjectSponsor.Contains(d.PROJECTSPONSORBN) || listProjectSponsor.Count() == 0)
                 
                  //&& (
                  //   (d.RESERVATIONDATE >= inquiryData.REQUESTDATEFROM || inquiryData.REQUESTDATEFROM == DateTime.MinValue)
                  //   && (d.RESERVATIONDATE <= inquiryData.REQUESTDATETO || inquiryData.REQUESTDATETO == DateTime.MinValue)
                  // )
                  //&& (
                  //   (d.MATERIALREQUESTs.Count(e => e.REQUIREMENTDATE >= inquiryData.REQMTDATEFROM) > 0 || inquiryData.REQMTDATEFROM == DateTime.MinValue)
                  //   && (d.MATERIALREQUESTs.Count(e => e.REQUIREMENTDATE <= inquiryData.REQMTDATETO) > 0 || inquiryData.REQMTDATETO == DateTime.MinValue)
                 //)
                 //&& (d.CHARGINGACCOUNT.ToLower().Contains(inquiryData.CHARGINGACCOUNT.ToLower()) || String.IsNullOrEmpty(inquiryData.CHARGINGACCOUNT))
                 //&& (inquiryData.STATUS == "ALL" ? true : d.STATUS == inquiryData.STATUS)
                 //&& (d.RESERVATIONPROGRESSes.Count(e => e.APPROVERNAME.ToLower() == inquiryData.APPROVER.ToLower()) > 0 || String.IsNullOrEmpty(inquiryData.APPROVER))
            );
            List<EWRRequestDTO> listResult = new List<EWRRequestDTO>();

            foreach (EWRREQUEST ewrRequest in listEWR)
            {
                // filter data
                bool isIncluded = false;
                if (isUserAdmin) // is Admin
                {
                    isIncluded = true;
                }
                else if (ewrRequest.REQUESTORBN == inquiryData.REQUESTBY) // is requestor
                {
                    isIncluded = true;
                }
                //else if (userController.getListSuperiorByBN(res.REQUESTFOR).Contains(inquiryData.REQUESTBY)) // is superior
                //{
                //    isIncluded = true;
                //}

                if (isIncluded)
                {
                    EWRRequestDTO ewrDTO = new EWRRequestDTO();
                    ewrDTO.ACCCODE = ewrRequest.ACCCODE;
                    ewrDTO.AREA = ewrRequest.AREA;
                    try
                    {
                        ewrDTO.ASSIGNEDPM = db.MOMEWRs.FirstOrDefault(d => d.EWRREQUESTID == ewrRequest.OID).PROJECTMANAGER;
                    }
                    catch (Exception)
                    {
                        ewrDTO.ASSIGNEDPM = "-";
                    }
                    
                    ewrDTO.BUDGETALLOC = ewrRequest.BUDGETALLOC;
                    ewrDTO.DATECOMPLETION = ewrRequest.DATECOMPLETION;
                    ewrDTO.DATEISSUED = ewrRequest.DATEISSUED;
                    ewrDTO.NETWORKNO = ewrRequest.NETWORKNO;
                    ewrDTO.OBJECTIVE = ewrRequest.OBJECTIVE;
                    ewrDTO.OID = ewrRequest.OID;
                    ewrDTO.OPERATIONALCP = ewrRequest.OPERATIONALCP;
                    ewrDTO.PROBLEMSTATEMENT = ewrRequest.PROBLEMSTATEMENT;
                    ewrDTO.PROJECTSPONSOR = ewrRequest.PROJECTSPONSOR;
                    ewrDTO.PROJECTTITLE = ewrRequest.PROJECTTITLE;
                    ewrDTO.SPONSORLOCATION = ewrRequest.SPONSORLOCATION;
                    ewrDTO.SPONSORPHONENO = ewrRequest.SPONSORPHONENO;
                    ewrDTO.STATUS = ewrRequest.STATUS;
                    ewrDTO.SUBJECT = ewrRequest.SUBJECT;

                    List<EWRRequestDetailDTO> listCategory = new List<EWRRequestDetailDTO>();
                    List<EWRRequestDetailDTO> listRequest = new List<EWRRequestDetailDTO>();
                    List<EWRRequestDetailDTO> listDetail = new List<EWRRequestDetailDTO>();
                    foreach (EWRREQUESTDETAIL ewrDetail in ewrRequest.EWRREQUESTDETAILs)
                    {
                        EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                        detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                        detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                        detailDTO.OID = ewrDetail.OID;
                        detailDTO.TYPE = ewrDetail.TYPE;
                        detailDTO.VALUE = ewrDetail.VALUE;

                        if (ewrDetail.TYPE == "categoryofwork")
                        {
                            listCategory.Add(detailDTO);
                        }
                        else if (ewrDetail.TYPE == "requestofwork")
                        {
                            listRequest.Add(detailDTO);
                        }
                        else if (ewrDetail.TYPE == "detailofwork")
                        {
                            listDetail.Add(detailDTO);
                        }
                    }

                    ewrDTO.CATEGORYOFWORK = listCategory;
                    ewrDTO.TYPEOFREQUESTEDWORK = listRequest;
                    ewrDTO.DETAILOFWORK = listDetail;

                    //MOMEWR
                    MOMEWR momResult = db.MOMEWRs.FirstOrDefault(d => d.EWRREQUESTID == ewrRequest.OID);
                    if (momResult != null)
                    {
                        EWRMomDTO ewrMom = new EWRMomDTO();
                        ewrMom.MOMDATE = momResult.MOMDATE;
                        ewrMom.MOMTIME = momResult.MOMTIME;
                        ewrMom.ACTIONTIMELINE = momResult.ACTIONTIMELINE;
                        ewrMom.BACKGROUND = momResult.BACKGROUND;
                        ewrMom.DATAREQUIRED = momResult.DATAREQUIRED;
                        ewrMom.DISTRIBUTIONS = momResult.DISTRIBUTION;
                        ewrMom.EWRNO = momResult.EWRNO;
                        ewrMom.EWRREQUESTID = (int)momResult.EWRREQUESTID;
                        ewrMom.OID = momResult.OID;
                        ewrMom.PROJECTDELIVERABLE = momResult.PROJECTDELIVERABLE;
                        ewrMom.PROJECTMANAGER = momResult.PROJECTMANAGER;
                        ewrMom.PROJECTMANAGERBN = momResult.PROJECTMANAGERBN;
                        ewrMom.PROJECTMANAGEREMAIL = momResult.PROJECTMANAGEREMAIL;
                        ewrMom.PROJECTTITLE = momResult.PROJECTTITLE;
                        ewrMom.RECORDEDBY = momResult.RECORDEDBY;
                        ewrMom.RECORDEDBYBN = momResult.RECORDEDBYBN;
                        ewrMom.RECORDEDBYEMAIL = momResult.RECORDEDBYEMAIL;
                        ewrMom.SCOPEOFWORK = momResult.SCOPEOFWORK;
                        ewrMom.SPONSORCP = momResult.SPONSORCP;
                        ewrMom.VENUE = momResult.VENUE;
                        ewrMom.MOMEWRNO = momResult.MOMEWRNO;

                        List<EWRRequestDetailDTO> listProject = new List<EWRRequestDetailDTO>();
                        foreach (EWRREQUESTDETAIL ewrDetail in db.EWRREQUESTDETAILs.Where(d => d.EWRMOMID == momResult.OID))
                        {
                            EWRRequestDetailDTO detailDTO = new EWRRequestDetailDTO();
                            detailDTO.DESCRIPTION = ewrDetail.DESCRIPTION;
                            //detailDTO.EWRREQUESTID = (int)ewrDetail.EWRREQUESTID;
                            detailDTO.OID = ewrDetail.OID;
                            detailDTO.TYPE = ewrDetail.TYPE;
                            detailDTO.VALUE = ewrDetail.VALUE;

                            listProject.Add(detailDTO);
                            //else if(ewrDetail.TYPE == "projecttype")
                            //{
                            //    listProject.Add(detailDTO);
                            //}
                        }

                        ewrDTO.EWRMOM = ewrMom;
                        ewrDTO.PROJECTTYPE = listProject;
                    }

                    listResult.Add(ewrDTO);

                }


            }
            return Ok(listResult);

        }
        //[HttpPost]
        //[ActionName("RejectRequest")]
        //public IHttpActionResult RejectRequest(int param, [FromBody]RESERVATIONREQUESTModel resModel)
        //{
        //    RESERVATIONREQUEST resRequest = db.RESERVATIONREQUESTs.Find(param);
        //    if (resRequest != null)
        //    {
        //        // call K2
        //        K2ManagementController k2Management = new K2ManagementController();
        //        k2Management.Connect();

        //        K2ConnectController k2Connect = new K2ConnectController();

        //        try
        //        {
        //            db.SaveChanges();

        //            //string nextApprover = k2NextApprover;

        //            string nextApprover = getNextK2Approver();

        //            List<K2WorklistItem> k2WorklistItem = k2Management.FilterWorkListItems(
        //                        SourceCode.Workflow.Management.WorklistFields.Folio,
        //                        SourceCode.Workflow.Management.Criteria.Comparison.Like,
        //                        "%RESERVATION%" + resRequest.RESERVATIONNO + "%", resRequest.WF_ID.HasValue ? resRequest.WF_ID.ToString() : null).ToList();

        //            string sn = k2WorklistItem.
        //                        Where(d => d.procId == resRequest.WF_ID &&
        //                        d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
        //                        Select(d => d.serialNo).FirstOrDefault();


        //            k2Connect.ExecuteWorklistAction(sn, Constants.STATUS_REJECT_K2, resModel.approvalRemark, null);
        //            //}

        //            //---
        //            IEnumerable<MATERIALREQUEST> listMaterial = db.MATERIALREQUESTs.Where(d => d.RESERVATIONID == param);
        //            foreach (MATERIALREQUEST material in listMaterial)
        //            {
        //                material.STATUS = Constants.STATUS_REJECT;
        //                db.Entry(material).State = EntityState.Modified;
        //            }

        //            db.SaveChanges();
        //            //---

        //        }
        //        catch (DbEntityValidationException exc)
        //        {
        //            var errorMessage = "";
        //            foreach (var error in exc.EntityValidationErrors)
        //            {
        //                foreach (var validationError in error.ValidationErrors)
        //                    errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
        //            }

        //            throw new ApplicationException(errorMessage);

        //        }
        //        finally
        //        {
        //            k2Management.Close();
        //            k2Connect.Disconnect();
        //        }
        //    }

        //    // simpan history nya
        //    ProgressHistoryController progressHistory = new ProgressHistoryController();
        //    progressHistory.UpdateRequestHistory(resRequest.RESERVATIONNO, null, Constants.STATUS_REJECT, resModel.approvalRemark, GetCurrentNTUserId());
        //    //--

        //    return Ok();
        //}
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EWRREQUESTExists(int id)
        {
            return db.EWRREQUESTs.Count(e => e.OID == id) > 0;
        }
    }

}