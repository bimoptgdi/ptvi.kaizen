﻿using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ContractController : KAIZENController
    {
        [HttpGet]
        [ActionName("GetContractByID")]
        public IHttpActionResult GetContractByID(string param)
        {
            contractObject contract = GetCONTRACT(param);
            return Ok(contract);
        }
        public contractObject GetCONTRACT(string id)
        {
            //string urlContract = "http://localhost/ptvi.commonservices/api/externaldata/executequerybyparam/QRY_CONTRACTWITHPM|@ct=[ct]|@vendorID=[vendorID]";
            GeneralParameterController genController = new GeneralParameterController();

            IEnumerable<GENERALPARAMETER> contractSettings = genController.FindByTitle(constants.CONTRACTWITHPM_API_URL);
            if (contractSettings.Count() > 0)
            {
                string[] param = id.Split('|');
                //string url = urlContract.Replace("[ct]", Uri.EscapeUriString(param[0]));
                string url = contractSettings.ElementAt(0).VALUE.Replace("[ct]", Uri.EscapeUriString(param[0]));
                if (param.Length > 1)
                    url = url.Replace("[vendorID]", param[1]);

                List<contractObject> contracts = GetListFromExternalData<contractObject>(url);
                if (contracts.Count > 0)
                {
                    contractObject contract = contracts[0];
                    if (String.IsNullOrEmpty(contract.ContractProjectManager))
                    {
                        contract.ContractProjectManager = contract.Sponsor_Name;
                    }
                    return contract;

                }

            }

            return null;

        }
    }
}
