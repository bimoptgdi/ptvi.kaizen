﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectDocumentDrawingTypesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/ProjectDocumentDrawingTypes
        public IQueryable<PROJECTDOCUMENTDRAWINGTYPE> GetPROJECTDOCUMENTDRAWINGTYPEs()
        {
            return db.PROJECTDOCUMENTDRAWINGTYPEs;
        }

        // GET: api/ProjectDocumentDrawingTypes/5
        [ResponseType(typeof(PROJECTDOCUMENTDRAWINGTYPE))]
        public IHttpActionResult GetPROJECTDOCUMENTDRAWINGTYPE(int id)
        {
            PROJECTDOCUMENTDRAWINGTYPE pROJECTDOCUMENTDRAWINGTYPE = db.PROJECTDOCUMENTDRAWINGTYPEs.Find(id);
            if (pROJECTDOCUMENTDRAWINGTYPE == null)
            {
                return NotFound();
            }

            return Ok(pROJECTDOCUMENTDRAWINGTYPE);
        }

        // PUT: api/ProjectDocumentDrawingTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROJECTDOCUMENTDRAWINGTYPE(int id, PROJECTDOCUMENTDRAWINGTYPE pROJECTDOCUMENTDRAWINGTYPE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTDOCUMENTDRAWINGTYPE.OID)
            {
                return BadRequest();
            }

            db.Entry(pROJECTDOCUMENTDRAWINGTYPE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTDRAWINGTYPEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjectDocumentDrawingTypes
        [ResponseType(typeof(PROJECTDOCUMENTDRAWINGTYPE))]
        public IHttpActionResult PostPROJECTDOCUMENTDRAWINGTYPE(PROJECTDOCUMENTDRAWINGTYPE pROJECTDOCUMENTDRAWINGTYPE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROJECTDOCUMENTDRAWINGTYPEs.Add(pROJECTDOCUMENTDRAWINGTYPE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pROJECTDOCUMENTDRAWINGTYPE.OID }, pROJECTDOCUMENTDRAWINGTYPE);
        }

        // DELETE: api/ProjectDocumentDrawingTypes/5
        [ResponseType(typeof(PROJECTDOCUMENTDRAWINGTYPE))]
        public IHttpActionResult DeletePROJECTDOCUMENTDRAWINGTYPE(int id)
        {
            PROJECTDOCUMENTDRAWINGTYPE pROJECTDOCUMENTDRAWINGTYPE = db.PROJECTDOCUMENTDRAWINGTYPEs.Find(id);
            if (pROJECTDOCUMENTDRAWINGTYPE == null)
            {
                return NotFound();
            }

            db.PROJECTDOCUMENTDRAWINGTYPEs.Remove(pROJECTDOCUMENTDRAWINGTYPE);
            db.SaveChanges();

            return Ok(pROJECTDOCUMENTDRAWINGTYPE);
        }



        [HttpGet]
        [ActionName("getDrawingActiveFromProjectPosition")]
        public IEnumerable<projectDocumentDrawingTypeObject> getActiveFromProjectPosition(int param, string docType, int projectId, string badgeNo)
        {
            var getPosition = db.USERASSIGNMENTs.FirstOrDefault(w => w.PROJECTID == projectId &&
            w.ASSIGNMENTTYPE == (docType == "DWG" ? constants.PROJECT_DESIGNER : "X") &&
            w.EMPLOYEEID == badgeNo);
            if (getPosition != null)
            {
                var getDrawingType = db.PROJECTDOCUMENTDRAWINGTYPEs.Where(w => w.PROJECTDOCUMENTID == param).Select(s => new projectDocumentDrawingTypeObject
                {
                    oid = s.OID,
                    projectDocumentId = s.PROJECTDOCUMENTID,
                    masterDesignerProductionId = s.MASTERDESIGNERPRODUCTIONID,
                    numberOfDrawing = s.NUMBEROFDRAWING,
                    designerType = s.MASTERDESIGNERPRODUCTIONID.HasValue ? s.MASTERDESIGNERPRODUCTION.DESIGNERTYPE : null,
                    typeOfDrawing = s.MASTERDESIGNERPRODUCTIONID.HasValue ? s.MASTERDESIGNERPRODUCTION.TYPEOFDRAWING : null,
                    estimateHours = s.MASTERDESIGNERPRODUCTIONID.HasValue ? s.MASTERDESIGNERPRODUCTION.ESTIMATEHOURS : null,
                    text = s.MASTERDESIGNERPRODUCTIONID.HasValue ? s.MASTERDESIGNERPRODUCTION.TEXT : null
                });

                var masterDrawingType = db.MASTERDESIGNERPRODUCTIONs.Where(w => !getDrawingType.Select(s => s.masterDesignerProductionId).Contains(w.OID) && w.ISACTIVE == true && w.DESIGNERTYPE.Contains(getPosition.EMPLOYEEPOSITION)).Select(s => new projectDocumentDrawingTypeObject
                {
                    oid = 0,
                    projectDocumentId = param,
                    masterDesignerProductionId = s.OID,
                    numberOfDrawing = 0,
                    designerType = s.DESIGNERTYPE,
                    typeOfDrawing = s.TYPEOFDRAWING,
                    estimateHours = s.ESTIMATEHOURS,
                    text = s.TEXT
                });
                return getDrawingType.Union(masterDrawingType).OrderBy(o => o.typeOfDrawing).Select(s => new projectDocumentDrawingTypeObject
                {
                    oid = s.oid,
                    projectDocumentId = s.projectDocumentId,
                    masterDesignerProductionId = s.masterDesignerProductionId,
                    numberOfDrawing = s.numberOfDrawing,
                    designerType = s.designerType,
                    typeOfDrawing = s.typeOfDrawing,
                    estimateHours = s.estimateHours,
                    text = s.text
                });
            }
            else {
                return new List<projectDocumentDrawingTypeObject>();
            }
        }

        [HttpPut]
        [ActionName("updateByMasterDrawingTypeId")]
        public IHttpActionResult updateByMasterDrawingTypeId(int param, projectDocumentDrawingTypeObject paramDrawing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROJECTDOCUMENTDRAWINGTYPE drawingType = new PROJECTDOCUMENTDRAWINGTYPE();
            if (param > 0)
            {
                if (paramDrawing.oid > 0)
                {
                    drawingType = db.PROJECTDOCUMENTDRAWINGTYPEs.Find(paramDrawing.oid);
                    drawingType.NUMBEROFDRAWING = paramDrawing.numberOfDrawing;
                    drawingType.UPDATEDBY = GetCurrentNTUserId();
                    drawingType.UPDATEDDATE = DateTime.Now;
                    db.Entry(drawingType).State = EntityState.Modified;
                }
                else
                {
                    drawingType.PROJECTDOCUMENTID = paramDrawing.projectDocumentId;
                    drawingType.MASTERDESIGNERPRODUCTIONID = paramDrawing.masterDesignerProductionId;
                    drawingType.NUMBEROFDRAWING = paramDrawing.numberOfDrawing;
                    drawingType.CREATEDBY = GetCurrentNTUserId();
                    drawingType.CREATEDDATE = DateTime.Now;
                    db.Entry(drawingType).State = EntityState.Added;
                }
            }
            else
            {
                return BadRequest();
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTDRAWINGTYPEExists(paramDrawing.oid))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            paramDrawing.masterDesignerProduction = null;
            paramDrawing.projectDocument = null;
            paramDrawing.oid = drawingType.OID;
            return Ok(paramDrawing);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTDOCUMENTDRAWINGTYPEExists(int id)
        {
            return db.PROJECTDOCUMENTDRAWINGTYPEs.Count(e => e.OID == id) > 0;
        }
    }
}