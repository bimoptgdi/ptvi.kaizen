﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class IntegrationLoaderController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        [HttpGet]
        [ActionName("GetLoaderByType")]
        public IEnumerable<integrationLoaderObject> GetLoaderByType(string param)
        {
            IEnumerable<integrationLoaderObject> loaderObject = db.FILEINTEGRATIONLOADERs.Where(x => x.FILEINTEGRATION.PURPOSETYPE == param).
                Select(y => new integrationLoaderObject
                {
                    id = y.OID,                    
                    fileIntegrationId = y.FILEINTEGRATIONID,
                    purposeFile = y.FILEINTEGRATION.PURPOSEFILE,
                    fileName = y.FILENAME,
                    loaderDate = y.LOADERDATE,
                    loaderBy = y.LOADERBY,
                    remark = y.REMARK
                });
            return loaderObject;
        }
    }
}
