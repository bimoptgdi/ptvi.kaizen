﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;


namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class objRep:KAIZENController
    {
        public int id;
        public string area;
        public double? workHoursProject;
        public double? workHoursTS;
    }
    public class TempName
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class TimesheetController : KAIZENController
    {
        // GET: api/Timesheet
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accCtl = new AccountController();
        private List<TempName> tempName = new List<TempName>();

        // GET: MaterialComponents
        public IQueryable<timesheetExtObject> Get()
        {
            IQueryable<timesheetExtObject> uAObj;
            uAObj = db.TIMESHEETs.Select(o => new timesheetExtObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                description = o.DESCRIPTION,
                overheadActivity = o.OVERHEADACTIVITY,
                projectDocumentId = o.PROJECTDOCUMENTID,
                weekHours = o.HOURS,
                weeklyId = o.WEEKLYID,
                //spentHours = db.TIMESHEETs.Sum(x=>x.HOURS),
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,

                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
            });
            return uAObj;
        }

        #region CustomAction

        [HttpGet]
        [ActionName("listInputTask")]
        public IQueryable<object> listInputTask(int param, string badgeNo, string userID, int wId)
        {
            var weekly = db.WEEKLies.Find(wId);
            //var projectDocument =
            //    db.PROJECTDOCUMENTs.
            //    Where(x => x.DOCUMENTBYID == userID && x.ACTUALFINISHDATE >= weekly.STARTDATE && (db.TIMESHEETs.Where(w => w.WEEKLYID == wId && w.CREATEDBY == user).Select(ts => ts.PROJECTDOCUMENTID)).Contains(x.OID) == false).
            //    Select(s => new
            //    {
            //        id = s.OID,
            //        projectId = s.PROJECTID,
            //        technicalSupportId = (int?)0,
            //        docType = s.DOCTYPE,
            //        docDesc = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.DOCUMENTTYPE && y.VALUE == s.DOCTYPE).TEXT,
            //        docNo = s.DOCNO,
            //        docTitle = s.DOCTITLE,
            //        drawingNo = s.DRAWINGNO,
            //        drawingTitle = s.DRAWINGTITLE,
            //        planStartDate = s.PLANSTARTDATE,
            //        planFinishDate = s.PLANFINISHDATE,
            //        actualStartDate = s.ACTUALSTARTDATE,
            //        actualFinishDate = s.ACTUALFINISHDATE,
            //        planDate = s.PLANSTARTDATE,
            //        planHours = (s.PLANFINISHDATE.Value.Day - s.PLANSTARTDATE.Value.Day) * 8,
            //        discipline = s.DISCIPLINE,
            //        task = s.DOCTYPE + " - " + s.DOCTITLE,
            //        status = s.STATUS,
            //        remark = s.REMARK,
            //        executor = s.EXECUTOR,
            //        executorAssignment = s.EXECUTORASSIGNMENT,
            //        executorAssignmentText = db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT) != null ? db.LOOKUPs.FirstOrDefault(y => y.TYPE == constants.EXECUTOREWPTYPE && y.VALUE == s.EXECUTORASSIGNMENT).TEXT : "-",
            //        progress = s.PROGRESS,
            //        documentById = s.DOCUMENTBYID,
            //        documentByName = s.DOCUMENTBYNAME,
            //        documentByEmail = s.DOCUMENTBYEMAIL,
            //        project = new { projectNo = s.PROJECT.PROJECTNO, projectDescription = s.PROJECT.PROJECTDESCRIPTION }
            //    });

            //var tSupport = db.TECHNICALSUPPORTs.
            //    Where(x => x.STATUS != "S1" &&
            //    (db.TIMESHEETs.Where(w => w.WEEKLYID == wId && w.CREATEDBY == user && w.PROJECTDOCUMENTID == null).Select(ts => ts.TECHNICALSUPPORTID)).Contains(x.OID) == false).
            //    Select(tso => new
            //    {
            //        id = tso.OID,
            //        tsNo = tso.TSNO,
            //        tsDesc = tso.TSDESC,
            //        status = tso.STATUS,
            //        userAssignments = db.USERASSIGNMENTs.Where(ua => ua.TECHNICALSUPPORTID == tso.OID && ua.ASSIGNMENTTYPE == "TSE"
            //        && ua.ISACTIVE == true && ua.EMPLOYEEID == userID).Select(uao => new userAssignmentObject()
            //        {
            //            id = uao.OID
            //        })
            //    }).Select(s => new
            //    {
            //        id = s.id,
            //        projectId = (int?)0,
            //        technicalSupportId = (int?)s.id,
            //        docType = "",
            //        docDesc = s.tsDesc,
            //        docNo = s.tsNo,
            //        docTitle = "",
            //        drawingNo = "",
            //        drawingTitle = "",
            //        planStartDate = (DateTime?)null,
            //        planFinishDate = (DateTime?)null,
            //        actualStartDate = (DateTime?)null,
            //        actualFinishDate = (DateTime?)null,
            //        planDate = (DateTime?)null,
            //        planHours = (int)0,
            //        discipline = "",
            //        task = (string)null,
            //        status = s.status,
            //        remark = "",
            //        executor = "",
            //        executorAssignment = "",
            //        executorAssignmentText = "",
            //        progress = (double?)null,
            //        documentById = "",
            //        documentByName = "",
            //        documentByEmail = "",
            //        project = new { projectNo = s.tsNo, projectDescription = s.tsDesc }
            //    });
            var projectDocument =
                db.PROJECTDOCUMENTs.
                Where(x => x.PROJECTID.HasValue && x.DOCUMENTBYID == badgeNo && 
                ((x.ACTUALSTARTDATE.HasValue && !x.ACTUALFINISHDATE.HasValue && x.ACTUALSTARTDATE <= weekly.ENDDATE) ||
                (x.ACTUALFINISHDATE >= weekly.STARTDATE && x.ACTUALSTARTDATE <= weekly.ENDDATE) || 
                (x.ACTUALFINISHDATE >= weekly.STARTDATE && x.ACTUALSTARTDATE <= weekly.STARTDATE) ||
                (x.ACTUALFINISHDATE <= weekly.ENDDATE && x.ACTUALFINISHDATE >= weekly.STARTDATE))
                    && x.TIMESHEETs.Where(w => w.WEEKLYID == wId && w.CREATEDBY == userID).Count() == 0).
                Select(s => new
                {
                    newId = Guid.NewGuid(),
                    projectId = (int?)s.PROJECTID,
                    projectDocumentId = (int?)s.OID,
                    technicalSupportId = (int?)0,
                    technicalSupportUserActionId = (int?)0,
                    task = s.DOCTITLE,
                    planDate = s.PLANFINISHDATE,
                    planHours = (int?)(s.PLANFINISHDATE.Value.Day - s.PLANSTARTDATE.Value.Day) * 8,
                    detailInformation = new { no = s.PROJECT.PROJECTNO, desc = s.PROJECT.PROJECTDESCRIPTION },
                    status = s.STATUS == constants.STATUSIPROM_HOLD || s.STATUS == constants.STATUSIPROM_CANCELLED ? s.STATUS : !s.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !s.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED,
                });

            var tSupport = db.TECHNICALSUPPORTUSERACTIONs.
                Where(x => x.USERASSIGNMENT.EMPLOYEEID == badgeNo && x.USERASSIGNMENT.TECHNICALSUPPORT.STATUS != "S1" && x.TIMESHEETs.Where(w => w.WEEKLYID == wId && w.CREATEDBY == userID).Count() == 0).
                Select(s => new
                {
                    newId = Guid.NewGuid(),
                    projectId = (int?)0,
                    projectDocumentId = (int?)0,
                    technicalSupportId = (int?)s.USERASSIGNMENT.TECHNICALSUPPORTID,
                    technicalSupportUserActionId = (int?)s.OID,
                    task = s.ACTIONDESCRIPTION,
                    planDate = (DateTime?)null,
                    planHours = (int?)null,
                    detailInformation = new { no = s.USERASSIGNMENT.TECHNICALSUPPORT.TSNO, desc = s.USERASSIGNMENT.TECHNICALSUPPORT.TSDESC },
                    status = s.USERASSIGNMENTID.HasValue ? s.USERASSIGNMENT.TECHNICALSUPPORTID.HasValue ? s.USERASSIGNMENT.TECHNICALSUPPORT.STATUS : null : null
                });

            var pDocUnion = projectDocument.Union(tSupport).OrderBy(o => o.planDate);
            return pDocUnion;
        }

        [HttpGet]
        [ActionName("pickTimesheetByUserIdWid")]
        public List<timesheetExtObject> pickTimesheetByUserId(int param, string userID, int? wId)
        {
            //IQueryable<timesheetExtObject> uAObj = null;
            //uAObj = db.TIMESHEETs.Where(x => x.CREATEDBY == userID && x.WEEKLYID == wId).Select(o => new timesheetExtObject()
            //{
            //    id = o.OID,
            //    projectId = o.PROJECTID,
            //    technicalSupportId = o.TECHNICALSUPPORTID,
            //    description = o.DESCRIPTION,
            //    overheadActivity = o.OVERHEADACTIVITY,
            //    projectDocumentId = o.PROJECTDOCUMENTID,
            //    weekHours = o.HOURS,
            //    weeklyId = o.WEEKLYID,
            //    spentHours = 0,
            //    createdDate = o.CREATEDDATE,
            //    createdBy = o.CREATEDBY,
            //    updatedDate = o.UPDATEDDATE,
            //    updatedBy = o.UPDATEDBY
            //});
            //if (uAObj.Count() > 0)
            //{
            //    return uAObj;
            //}
            //else
            //{
            //    wId--;
            //    var ts = db.TIMESHEETs.Where(x => x.CREATEDBY == userID && x.WEEKLYID == wId && x.OVERHEADACTIVITY == null
            //        && (x.HOURS == null || x.HOURS == 0));
            //    if (ts.Count() > 0)
            //    {
            //        foreach (var item in ts)
            //        {
            //            item.WEEKLYID = wId + 1;
            //            db.TIMESHEETs.Add(item);
            //        }
            //        db.SaveChanges();
            //    }
            //    uAObj = ts.Select(o => new timesheetExtObject()
            //    {
            //        id = o.OID,
            //        projectId = o.PROJECTID,
            //        description = o.DESCRIPTION,
            //        overheadActivity = o.OVERHEADACTIVITY,
            //        projectDocumentId = o.PROJECTDOCUMENTID,
            //        weekHours = o.HOURS,
            //        weeklyId = o.WEEKLYID,
            //        spentHours = 0,
            //        createdDate = o.CREATEDDATE,
            //        createdBy = o.CREATEDBY,
            //        updatedDate = o.UPDATEDDATE,
            //        updatedBy = o.UPDATEDBY
            //    });
            //    return uAObj;

            return db.TIMESHEETs.Where(x => x.CREATEDBY == userID && x.WEEKLYID == wId).Select(o => new timesheetExtObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                projectDocumentId = o.PROJECTDOCUMENTID,
                technicalSupportId = o.TECHNICALSUPPORTID,
                technicalSupportUserActionId = o.TECHNICALSUPPORTUSERACTIONID,
                description = o.DESCRIPTION,
                overheadActivity = o.OVERHEADACTIVITY,
                weekHours = o.HOURS,
                weeklyId = o.WEEKLYID,
                spentHours = 0,
                status = o.PROJECTDOCUMENTID.HasValue ? (o.PROJECTDOCUMENT.STATUS == constants.STATUSIPROM_HOLD || o.PROJECTDOCUMENT.STATUS == constants.STATUSIPROM_CANCELLED ? o.PROJECTDOCUMENT.STATUS : !o.PROJECTDOCUMENT.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !o.PROJECTDOCUMENT.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED) : o.TECHNICALSUPPORTID.HasValue? o.TECHNICALSUPPORT.STATUS : null,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
            }).ToList().OrderBy(o => o.planDate).ToList();
        }

        [HttpGet]
        [ActionName("pickTimesheetByUserId")]
        public IQueryable<timesheetObject> pickTimesheetByUserId(int param, string userID)
        {
            return db.TIMESHEETs.Where(x => x.CREATEDBY == userID).Select(o => new timesheetObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                description = o.DESCRIPTION,
                overheadActivity = o.OVERHEADACTIVITY,
                projectDocumentId = o.PROJECTDOCUMENTID,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
            });

        }

        [HttpPost]
        [ActionName("inputTask")]
        public HttpResponseMessage inputTask(int param, timesheetExtObject uAObj)
        {
            var ts = new TIMESHEET()
            {
                OID = 0,
                WEEKLYID = uAObj.weeklyId,
                PROJECTID = uAObj.projectId.HasValue ? uAObj.projectId.Value > 0 ? uAObj.projectId.Value : (int?)null : (int?)null,
                PROJECTDOCUMENTID = uAObj.projectDocumentId.HasValue ? uAObj.projectDocumentId.Value > 0 ? uAObj.projectDocumentId.Value : (int?)null : (int?)null,
                TECHNICALSUPPORTID = uAObj.technicalSupportId.HasValue ? uAObj.technicalSupportId.Value > 0 ? uAObj.technicalSupportId.Value : (int?)null : (int?)null,
                TECHNICALSUPPORTUSERACTIONID = uAObj.technicalSupportUserActionId.HasValue ? uAObj.technicalSupportUserActionId.Value > 0 ? uAObj.technicalSupportUserActionId.Value : (int?)null : (int?)null,
                OVERHEADACTIVITY = uAObj.overheadActivity,
                DESCRIPTION = uAObj.description,
                HOURS = uAObj.hours.HasValue ? uAObj.hours.Value : (double?)null,
                CREATEDDATE = DateTime.Now,
                CREATEDBY = uAObj.createdBy,
                UPDATEDDATE = DateTime.Now,
                UPDATEDBY = uAObj.updatedBy
            };

            db.TIMESHEETs.Add(ts);
            db.SaveChanges();
            uAObj.id = ts.OID;
            uAObj.weekHours = ts.HOURS;
            if (ts.PROJECTDOCUMENTID.HasValue)
            {
                PROJECTDOCUMENT projDoc = db.PROJECTDOCUMENTs.Find(ts.PROJECTDOCUMENTID.Value);
                uAObj.status = (projDoc.STATUS == constants.STATUSIPROM_HOLD || projDoc.STATUS == constants.STATUSIPROM_CANCELLED ? projDoc.STATUS : !projDoc.ACTUALSTARTDATE.HasValue ? constants.STATUSIPROM_NOTSTARTED : !projDoc.ACTUALFINISHDATE.HasValue ? constants.STATUSIPROM_INPROGRESS : constants.STATUSIPROM_COMPLETED);
                uAObj.planDate = projDoc.PLANSTARTDATE.Value;
            } else if (ts.TECHNICALSUPPORTID.HasValue)
            {
                TECHNICALSUPPORT techSupport = db.TECHNICALSUPPORTs.Find(ts.TECHNICALSUPPORTID.Value);
                uAObj.status = techSupport != null ? techSupport.STATUS : null;
            }
            if (uAObj.technicalSupportObject.Count > 0) uAObj.planHours = 0;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, uAObj);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = uAObj.id }));
            return response;

        }

        [HttpPut]
        [ActionName("updateTimesheet")]
        public HttpResponseMessage updateTimesheet(int param, timesheetExtObject uAObj)
        {
            if (param == uAObj.id)
            {
                var ts = db.TIMESHEETs.FirstOrDefault(x => x.OID == uAObj.id);
                ts.HOURS = uAObj.hours;
                ts.UPDATEDDATE = DateTime.Now;
                ts.UPDATEDBY = uAObj.updatedBy;
                db.Entry(ts).State = EntityState.Modified;
                db.SaveChanges();
                uAObj.weekHours = ts.HOURS;
            }
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, uAObj);
            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = uAObj.id }));
            return response;

        }

        #endregion

        [HttpDelete]
        [ActionName("deleteTimesheet")]
        public IHttpActionResult deleteTimesheet(int param)
        {
            var ts = db.TIMESHEETs.Find(param);
            if (ts == null)
            {
                return NotFound();
            }

            db.TIMESHEETs.Remove(ts);
            db.SaveChanges();

            return Ok(ts);
        }

        [HttpGet]
        [ActionName("ReportWorkhoursConsumption")]
        public object ReportWorkhoursConsumption(int param, string area)
        {
            if (area != "[]" && area != "null")
            {
                var k = db.TIMESHEETs.Where(z => (z.TECHNICALSUPPORT.REQUESTDATE.Value.Year == param)).
                    Select(t => t.TECHNICALSUPPORT.TECHNICALSUPPORTAREAs.Where(x => area.Contains(x.AREAINVOLVED.OID.ToString())).Select(c => new
                    {
                        id = c.AREAINVOLVED.OID,
                        areaDesc = c.AREAINVOLVED.AREADESC,
                        workHoursProject = (int?)0,
                        workHoursTS = t.HOURS,
                    })
                 );
                List<objRep> l = new List<objRep>();
                foreach (var x in k)
                {
                    foreach (var y in x)
                    {
                        var f = new objRep
                        {
                            id = y.id,
                            area = y.areaDesc,
                            workHoursProject = y.workHoursProject,
                            workHoursTS = y.workHoursTS
                        };
                        l.Add(f);
                    }
                }
                var n = l.Union
                    (db.TIMESHEETs.Where(w => ((area.Contains(w.PROJECT.AREAINVOLVED.OID.ToString())) && w.PROJECT.ACTUALSTARTDATE.Value.Year <= param && (w.PROJECT.ACTUALFINISHDATE.Value.Year >= param || w.PROJECT.ACTUALFINISHDATE == null))
                        ).Select(s => new objRep
                        {
                            id = s.PROJECT.AREAINVOLVED.OID,
                            area = s.PROJECT.AREAINVOLVED.AREADESC,
                            workHoursProject = s.HOURS != null ? s.HOURS : 0,
                            workHoursTS = (int?)0,
                        })
                    )
                    .GroupBy(c => new {c.area, c.id}).Select(s => new
                    {
                        id = s.Key.id,
                        area = s.Key.area,
                        workHoursProject = s.Sum(q => q.workHoursProject),
                        workHoursTS = s.Sum(q => q.workHoursTS),
                    });
                return n;
            }
            else
            {
                var k = db.TIMESHEETs.Where(z => (z.TECHNICALSUPPORT.REQUESTDATE.Value.Year == param)).
                    Select(t => t.TECHNICALSUPPORT.TECHNICALSUPPORTAREAs.Select(c => new
                    {
                        id = c.AREAINVOLVED.OID,
                        areaDesc = c.AREAINVOLVED.AREADESC,
                        workHoursProject = (int?)0,
                        workHoursTS = t.HOURS,
                    })
                 );
                List<objRep> l = new List<objRep>();
                foreach(var x in k)
                {
                    foreach(var y in x)
                    {
                        var f = new objRep
                        {
                            id = y.id,
                            area = y.areaDesc,
                            workHoursProject = y.workHoursProject,
                            workHoursTS = y.workHoursTS
                        };
                        l.Add(f);
                    }
                }
                var n = l.Union
                    (db.TIMESHEETs.Where(w => (w.PROJECT.ACTUALSTARTDATE.Value.Year <= param && (w.PROJECT.ACTUALFINISHDATE.Value.Year >= param || w.PROJECT.ACTUALFINISHDATE == null))
                        ).Select(s => new objRep
                        {
                            id = s.PROJECT.AREAINVOLVED.OID,
                            area = s.PROJECT.AREAINVOLVED.AREADESC,
                            workHoursProject = s.HOURS != null ? s.HOURS : 0,
                            workHoursTS = (int?)0,
                        })
                    )
                    .GroupBy(c => new { c.area, c.id }).Select(s => new
                    {
                        id = s.Key.id,
                        area = s.Key.area,
                        workHoursProject = s.Sum(q => q.workHoursProject),
                        workHoursTS = s.Sum(q => q.workHoursTS),
                    });
                
                return n;
            }
        }

        [HttpGet]
        [ActionName("ReportTSHours")]
        public object ReportTSHours(int param, string area)
        {
            if (area != "[]" && area != "null")
            {
                var k = db.TIMESHEETs.Where(z => (z.TECHNICALSUPPORT.REQUESTDATE.Value.Year == param)).
                    Select(t => t.TECHNICALSUPPORT.TECHNICALSUPPORTAREAs.Where(x => area.Contains(x.AREAINVOLVED.OID.ToString())).Select(c => new
                    {
                        id = c.AREAINVOLVED.OID,
                        areaDesc = c.AREAINVOLVED.AREADESC,
                        workHoursTS = t.HOURS,
                    })
                 );
                List<objRep> l = new List<objRep>();
                foreach (var x in k)
                {
                    foreach (var y in x)
                    {
                        var f = new objRep
                        {
                            id = y.id,
                            area = y.areaDesc,
                            workHoursTS = y.workHoursTS
                        };
                        l.Add(f);
                    }
                }
                var n = l.GroupBy(c => new { c.area, c.id }).Select(s => new
                {
                    id = s.Key.id,
                    area = s.Key.area,
                    workHoursTS = s.Sum(q => q.workHoursTS),
                    countTS = s.Count()
                });
                return n;
            }
            else
            {
                var k = db.TIMESHEETs.Where(z => (z.TECHNICALSUPPORT.REQUESTDATE.Value.Year == param)).
                    Select(t => t.TECHNICALSUPPORT.TECHNICALSUPPORTAREAs.Select(c => new
                    {
                        id = c.AREAINVOLVED.OID,
                        areaDesc = c.AREAINVOLVED.AREADESC,
                        workHoursTS = t.HOURS,
                    })
                 );
                List<objRep> l = new List<objRep>();
                foreach (var x in k)
                {
                    foreach (var y in x)
                    {
                        var f = new objRep
                        {
                            id = y.id,
                            area = y.areaDesc,
                            workHoursTS = y.workHoursTS
                        };
                        l.Add(f);
                    }
                }
                var n = l.GroupBy(c => new { c.area, c.id }).Select(s => new
                {
                    id = s.Key.id,
                    area = s.Key.area,
                    workHoursTS = s.Sum(q => q.workHoursTS),
                    countTS = s.Count()
                });
                return n;
            }
        }


        [HttpGet]
        [ActionName("listEngineerTimesheet")]
        public object listEngineerTimesheet(string param)
        {
            var userInTS = db.TIMESHEETs.
                Select(s => new
                {
                    employeeId = s.CREATEDBY,
                }).Distinct().ToList();

            var result = userInTS.Select(s => new
            {
                engineerid = s.employeeId,
                engineername = findTempName(s.employeeId)
            });

            return result;
        }

        [HttpGet]
        [ActionName("listDetailTimesheets")]
        public object listDetailTimesheets(string param,string engName, DateTime? weekStart, DateTime? weekEnd)
        {
            var tsDetail = new object();
            //var dStart = DateTime.Parse(weekStart);
            //var dEnd = DateTime.Parse(weekStart);
            //var fName = engName!=null ? accCtl.getEmployeeByBN(engName).full_Name : null;
            var tsD = db.TIMESHEETs.Where(x => ((engName.Contains(x.CREATEDBY) || engName == null)  
            && (x.WEEKLY.STARTDATE>= weekStart && x.WEEKLY.ENDDATE<= weekEnd) )
            /*|| (engName == null && weekStart==null && weekEnd==null)*/);
            if (tsD.Count() > 0) {
                tsDetail = tsD.Select(v => new
                {
                    id = v.OID,
                    weekStart = v.WEEKLY.STARTDATE,
                    weekEnd = v.WEEKLY.ENDDATE,
                    //week = v.WEEKLY.STARTDATE.Value.ToString("yyyy-MM-dd") + " - " + v.WEEKLY.ENDDATE.Value.ToString("yyyy-MM-dd") ,
                    engineerName = v.CREATEDBY,
                    projectNo = v.PROJECTID == null ? v.TECHNICALSUPPORTID == null?  "": v.TECHNICALSUPPORT.TSNO : v.PROJECT.PROJECTNO,
                    projectTitle = v.DESCRIPTION,
                    description = v.PROJECTDOCUMENT.DOCNO != null ? v.PROJECTDOCUMENT.DOCNO + "-" + v.PROJECTDOCUMENT.DOCTITLE :
                       db.LOOKUPs.Where(z => z.TYPE == constants.overheadTimesheet && z.VALUE == v.OVERHEADACTIVITY).FirstOrDefault().TEXT + "-" + v.DESCRIPTION,
                    spentHours = v.HOURS,
                    createdDate = v.CREATEDDATE,
                }).ToList().Select(v=>new {
                    id = v.id,
                    week = v.weekStart.Value.ToString("dd MMM yyyy") + " - " + v.weekEnd.Value.ToString("dd MMM yyyy") ,
                    engineerName = findTempName(v.engineerName),
                    projectNo = v.projectNo,
                    projectTitle = v.projectTitle,
                    description = v.description,
                    spentHours = v.spentHours,
                    createdDate = v.createdDate

                });
            }
            return tsDetail;
        }

        /// <summary>
        /// Search and Save temporary name from server
        /// so it doesn't do redundant search from server for names searched names
        /// </summary>
        /// <param name="nm"></param>
        /// <returns></returns>
        public string findTempName(string nm)
        {
            var tName = "";
            var teName = tempName.Where(x => x.id == nm);
            if (teName.Count(x => x.id == nm) ==0)
            {
                tName = accCtl.getEmployeeByBN(nm).full_Name;
                tempName.Add(new TempName{ id = nm, name = tName });
            }
            else
            {
                tName = teName.FirstOrDefault().name;
            }
            return tName;
        }

        [HttpGet]
        [ActionName("listEmployeeReportWorkinHours")]
        public object listEmployeeReportWorkinHours(string param)
        {
            var getEmployee = db.PROJECTs.Where(w => w.PROJECTTYPE == "OPERATING").
                            Select(s => new
                            {
                                roleLevel = 1,
                                roleName = "Operating",
                                employeeId = s.PROJECTENGINEERID,
                                employeeName = s.PROJECTENGINEERNAME
                            }).Union(db.USERASSIGNMENTs.Where(w => w.PROJECTID.HasValue && w.PROJECT.PROJECTTYPE == "OPERATING" && w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).
                            Select(s => new
                            {
                                roleLevel = 1,
                                roleName = "Operating",
                                employeeId = s.EMPLOYEEID,
                                employeeName = s.EMPLOYEENAME
                            })).Union(db.PROJECTs.Where(w => w.PROJECTTYPE == "CAPITAL").
                            Select(s => new
                            {
                                roleLevel = 2,
                                roleName = "Capital",
                                employeeId = s.PROJECTENGINEERID,
                                employeeName = s.PROJECTENGINEERNAME
                            })).Union(db.USERASSIGNMENTs.Where(w => w.PROJECTID.HasValue && w.PROJECT.PROJECTTYPE == "CAPITAL" && w.ASSIGNMENTTYPE == constants.PROJECT_ENGINEER).
                            Select(s => new
                            {
                                roleLevel = 2,
                                roleName = "Capital",
                                employeeId = s.EMPLOYEEID,
                                employeeName = s.EMPLOYEENAME
                            })).Union(db.USERASSIGNMENTs.Where(w => w.TECHNICALSUPPORTID.HasValue && w.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_ENGINEER).
                            Select(s => new
                            {
                                roleLevel = 3,
                                roleName = "Technical SUpport",
                                employeeId = s.EMPLOYEEID,
                                employeeName = s.EMPLOYEENAME
                            })).Union(db.TIMESHEETs.Where(w => !w.PROJECTID.HasValue && !w.TECHNICALSUPPORTID.HasValue).Join(db.USERs, time => time.CREATEDBY, user => user.NTUSERID, (time, user) => new { time = time, user = user }).
                            Select(s => new
                            {
                                roleLevel = 4,
                                roleName = "OverHead",
                                employeeId = s.user.BADGENO,
                                employeeName = s.user.USERNAME
                            }));
            var result = getEmployee.GroupBy(g => new { g.roleLevel, g.roleName }).Select(s => new
                            {
                                roleLevel = s.Key.roleLevel,
                                roleName = s.Key.roleName,
                                detail = getEmployee.Where(w => w.roleLevel == s.Key.roleLevel).
                                            Select(s1 => new
                                            {
                                                employeeid = s1.employeeId,
                                                employeename = s1.employeeName
                                            })
                            });
            return result;
        }
    }

}
