﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MasterYearlyEngineeringHoursController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/MasterYearlyEngineeringHours
        public IQueryable<masterYearlyEngineeringHoursObject> GetMasterYearlyEngineeringHours()
        {
            return db.MASTERYEARLYENGINEERINGHOURS.Select(s => new masterYearlyEngineeringHoursObject
            {
                id = s.OID,
                year = s.YEAR,
                employeeId = s.EMPLOYEEID,
                employeeName = s.EMPLOYEENAME,
                employeeEmail = s.EMPLOYEEEMAIL,
                employeePosition = s.EMPLOYEEPOSITION,
                masterYearlyFactorId = s.MASTERYEARLYFACTORID,
                engineerDesignerFactor = s.ENGINEERDESIGNERFACTOR,
                dateOfLastPromotion = s.DATEOFLASTPROMOTION,
                holidayPublic = s.HOLIDAYPUBLIC,
                holidaySatsun = s.HOLIDAYSATSUN,
                leaveAnnual = s.LEAVEANNUAL,
                leaverr = s.LEAVERR,
                leaveExt = s.LEAVEEXT,
                leaveOtherJan = s.LEAVEOTHERJAN,
                leaveOtherFeb = s.LEAVEOTHERFEB,
                leaveOtherMar = s.LEAVEOTHERMAR,
                leaveOtherApr = s.LEAVEOTHERAPR,
                leaveOtherMay = s.LEAVEOTHERMAY,
                leaveOtherJun = s.LEAVEOTHERJUN,
                leaveOtherJul = s.LEAVEOTHERJUL,
                leaveOtherAug = s.LEAVEOTHERAUG,
                leaveOtherSep = s.LEAVEOTHERSEP,
                leaveOtherOct = s.LEAVEOTHEROCT,
                leaveOtherNov = s.LEAVEOTHERNOV,
                leaveOtherDec = s.LEAVEOTHERDEC,
                totNonWorkingDay = s.TOTNONWORKINGDAY,
                availableHours = s.AVAILABLEHOURS,
                baselineHours = s.BASELINEHOURS,
            });
        }

        // GET: api/MasterYearlyEngineeringHours
        [ResponseType(typeof(masterYearlyEngineeringHoursObject))]
        public IHttpActionResult GetMASTERYEARLYENGINEERINGHOURS(int id)
        {
            MASTERYEARLYENGINEERINGHOUR mASTERYEARLYENGINEERINGHOURS = db.MASTERYEARLYENGINEERINGHOURS.Find(id);
            if (mASTERYEARLYENGINEERINGHOURS == null)
            {
                return NotFound();
            }

            return Ok(mASTERYEARLYENGINEERINGHOURS);
        }

        // PUT: api/MasterYearlyEngineeringHours/5
        [ResponseType(typeof(masterYearlyEngineeringHoursObject))]
        public IHttpActionResult PutMASTERYEARLYENGINEERINGHOURS(int id, masterYearlyEngineeringHoursObject mASTERYEARLYENGINEERINGHOURS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERYEARLYENGINEERINGHOURS.id)
            {
                return BadRequest();
            }

            db.Entry(mASTERYEARLYENGINEERINGHOURS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERYEARLYENGINEERINGHOURSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MasterYearlyEngineeringHours
        [ResponseType(typeof(masterYearlyEngineeringHoursObject))]
        public IHttpActionResult PostMasterYearlyEngineeringHours(MASTERYEARLYENGINEERINGHOUR MASTERYEARLYENGINEERINGHOURS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MASTERYEARLYENGINEERINGHOURS.Add(MASTERYEARLYENGINEERINGHOURS);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { OID = MASTERYEARLYENGINEERINGHOURS.OID }, MASTERYEARLYENGINEERINGHOURS);
        }


        // DELETE: api/MasterYearlyEngineeringHours/5
        [ResponseType(typeof(masterYearlyEngineeringHoursObject))]
        public IHttpActionResult DeleteMASTERYEARLYENGINEERINGHOURS(int id)
        {
            MASTERYEARLYENGINEERINGHOUR mASTERYEARLYENGINEERINGHOURS = db.MASTERYEARLYENGINEERINGHOURS.Find(id);
            if (mASTERYEARLYENGINEERINGHOURS == null)
            {
                return NotFound();
            }

            db.MASTERYEARLYENGINEERINGHOURS.Remove(mASTERYEARLYENGINEERINGHOURS);
            db.SaveChanges();

            return Ok(mASTERYEARLYENGINEERINGHOURS);
        }

        [HttpPut]
        [ActionName("updateMasterYearlyEngineeringHours")]
        public IHttpActionResult updateMasterYearlyEengineeringHours(int param, masterYearlyEngineeringHoursObject mASTERYEARLYENGINEERINGHOURSParam)
        {
          if (!ModelState.IsValid)
           {
                return BadRequest();
            }

            if (param != mASTERYEARLYENGINEERINGHOURSParam.id)
            {
               return BadRequest();
            }

            try
           {
                MASTERYEARLYENGINEERINGHOUR update = db.MASTERYEARLYENGINEERINGHOURS.Find(param);
                update.OID = mASTERYEARLYENGINEERINGHOURSParam.id;
               update.YEAR = mASTERYEARLYENGINEERINGHOURSParam.year;
               update.EMPLOYEEID = mASTERYEARLYENGINEERINGHOURSParam.employeeId;
               update.EMPLOYEENAME = mASTERYEARLYENGINEERINGHOURSParam.employeeName;
              update.EMPLOYEEEMAIL = mASTERYEARLYENGINEERINGHOURSParam.employeeEmail;
               update.EMPLOYEEPOSITION = mASTERYEARLYENGINEERINGHOURSParam.employeePosition;
                update.MASTERYEARLYFACTORID = mASTERYEARLYENGINEERINGHOURSParam.masterYearlyFactorId;
                update.ENGINEERDESIGNERFACTOR = mASTERYEARLYENGINEERINGHOURSParam.engineerDesignerFactor;
                update.DATEOFLASTPROMOTION = mASTERYEARLYENGINEERINGHOURSParam.dateOfLastPromotion;
                update.HOLIDAYPUBLIC = mASTERYEARLYENGINEERINGHOURSParam.holidayPublic;
                update.HOLIDAYSATSUN = mASTERYEARLYENGINEERINGHOURSParam.holidaySatsun;
                update.LEAVEANNUAL = mASTERYEARLYENGINEERINGHOURSParam.leaveAnnual;
              update.LEAVERR = mASTERYEARLYENGINEERINGHOURSParam.leaverr;
              update.LEAVEEXT = mASTERYEARLYENGINEERINGHOURSParam.leaveExt;
               update.LEAVEOTHERJAN = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherJan;
                update.LEAVEOTHERFEB = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherFeb;
                update.LEAVEOTHERMAR = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherMar;
                update.LEAVEOTHERAPR = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherApr;
                update.LEAVEOTHERMAY = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherMay;
                update.LEAVEOTHERJUN = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherJun;
                update.LEAVEOTHERJUL = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherJul;
                update.LEAVEOTHERAUG = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherAug;
                update.LEAVEOTHERSEP = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherSep;
                update.LEAVEOTHEROCT = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherOct;
                update.LEAVEOTHERNOV = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherNov;
               update.LEAVEOTHERDEC = mASTERYEARLYENGINEERINGHOURSParam.leaveOtherDec;
                update.TOTNONWORKINGDAY = mASTERYEARLYENGINEERINGHOURSParam.totNonWorkingDay;
                update.AVAILABLEHOURS = mASTERYEARLYENGINEERINGHOURSParam.availableHours;
                update.BASELINEHOURS = mASTERYEARLYENGINEERINGHOURSParam.baselineHours;
                update.UPDATEDDATE = DateTime.Now;
              update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

           }
           catch (Exception ex)
           {
               WriteLog(ex.ToString());

               throw;
           }
           return Ok(mASTERYEARLYENGINEERINGHOURSParam);
        }


        [HttpGet]
        [ActionName("getMaxMinYearly")]
        public object getMaxMinYearly(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERYEARLYENGINEERINGHOURS.Min(w => w.YEAR);
            var max = db.MASTERYEARLYENGINEERINGHOURS.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }

        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterYearlyEngineeringHoursObject> getByYear(int param)
        {
            return db.MASTERYEARLYENGINEERINGHOURS.Where(w => w.YEAR == param).Select(s => new masterYearlyEngineeringHoursObject
            {
                id = s.OID,
                year = s.YEAR,
                employeeId = s.EMPLOYEEID,
                employeeName = s.EMPLOYEENAME,
                employeeEmail = s.EMPLOYEEEMAIL,
                employeePosition = s.EMPLOYEEPOSITION,
                masterYearlyFactorId = s.MASTERYEARLYFACTORID,
                engineerDesignerFactor = s.ENGINEERDESIGNERFACTOR,
                dateOfLastPromotion = s.DATEOFLASTPROMOTION,
                holidayPublic = s.HOLIDAYPUBLIC,
                holidaySatsun = s.HOLIDAYSATSUN,
                leaveAnnual = s.LEAVEANNUAL,
                leaverr = s.LEAVERR,
                leaveExt = s.LEAVEEXT,
                leaveOtherJan = s.LEAVEOTHERJAN,
                leaveOtherFeb = s.LEAVEOTHERFEB,
                leaveOtherMar = s.LEAVEOTHERMAR,
                leaveOtherApr = s.LEAVEOTHERAPR,
                leaveOtherMay = s.LEAVEOTHERMAY,
                leaveOtherJun = s.LEAVEOTHERJUN,
                leaveOtherJul = s.LEAVEOTHERJUL,
                leaveOtherAug = s.LEAVEOTHERAUG,
                leaveOtherSep = s.LEAVEOTHERSEP,
                leaveOtherOct = s.LEAVEOTHEROCT,
                leaveOtherNov = s.LEAVEOTHERNOV,
                leaveOtherDec = s.LEAVEOTHERDEC,
                totNonWorkingDay = s.TOTNONWORKINGDAY,
                availableHours = s.AVAILABLEHOURS,
                baselineHours = s.BASELINEHOURS,
            });
        }

        [HttpGet]
        [ActionName("getAllEmployeeMasterYearlyEngineeringHours")]
        public object getAllEmployeeMasterYearlyEngineeringHours(string param)
        {
            return db.MASTERYEARLYENGINEERINGHOURS.Select(s => new
            {
                badge = s.EMPLOYEEID,
                name = s.EMPLOYEENAME
            }).Distinct();
        }

        [HttpPost]
        [ActionName("addMasterYearlyEngineeringHours")]
        public IHttpActionResult addMasterYearlyEngineeringHours(string param, masterYearlyEngineeringHoursObject masterYearlyEngineeringHoursParam)
        {
            MASTERYEARLYENGINEERINGHOUR add = new MASTERYEARLYENGINEERINGHOUR();
            add.OID = masterYearlyEngineeringHoursParam.id;
            add.YEAR = masterYearlyEngineeringHoursParam.year;
            add.ENGINEERDESIGNERFACTOR = masterYearlyEngineeringHoursParam.engineerDesignerFactor;
            add.EMPLOYEEID = masterYearlyEngineeringHoursParam.employeeId;
            add.EMPLOYEENAME = masterYearlyEngineeringHoursParam.employeeName;
            add.EMPLOYEEEMAIL = masterYearlyEngineeringHoursParam.employeeEmail;
            add.EMPLOYEEPOSITION = masterYearlyEngineeringHoursParam.employeePosition;
            add.MASTERYEARLYFACTORID = masterYearlyEngineeringHoursParam.masterYearlyFactorId;
            add.ENGINEERDESIGNERFACTOR = masterYearlyEngineeringHoursParam.engineerDesignerFactor;
            add.DATEOFLASTPROMOTION = masterYearlyEngineeringHoursParam.dateOfLastPromotion;
            add.HOLIDAYPUBLIC = masterYearlyEngineeringHoursParam.holidayPublic;
            add.HOLIDAYSATSUN = masterYearlyEngineeringHoursParam.holidaySatsun;
            add.LEAVEANNUAL = masterYearlyEngineeringHoursParam.leaveAnnual;
            add.LEAVERR = masterYearlyEngineeringHoursParam.leaverr;
            add.LEAVEEXT = masterYearlyEngineeringHoursParam.leaveExt;
            add.LEAVEOTHERJAN = masterYearlyEngineeringHoursParam.leaveOtherJan;
            add.LEAVEOTHERFEB = masterYearlyEngineeringHoursParam.leaveOtherFeb;
            add.LEAVEOTHERMAR = masterYearlyEngineeringHoursParam.leaveOtherMar;
            add.LEAVEOTHERAPR = masterYearlyEngineeringHoursParam.leaveOtherApr;
            add.LEAVEOTHERMAY = masterYearlyEngineeringHoursParam.leaveOtherMay;
            add.LEAVEOTHERJUN = masterYearlyEngineeringHoursParam.leaveOtherJun;
            add.LEAVEOTHERJUL = masterYearlyEngineeringHoursParam.leaveOtherJul;
            add.LEAVEOTHERAUG = masterYearlyEngineeringHoursParam.leaveOtherAug;
            add.LEAVEOTHERSEP = masterYearlyEngineeringHoursParam.leaveOtherSep;
            add.LEAVEOTHEROCT = masterYearlyEngineeringHoursParam.leaveOtherOct;
            add.LEAVEOTHERNOV = masterYearlyEngineeringHoursParam.leaveOtherNov;
            add.LEAVEOTHERDEC = masterYearlyEngineeringHoursParam.leaveOtherDec;
            add.TOTNONWORKINGDAY = masterYearlyEngineeringHoursParam.totNonWorkingDay;
            add.AVAILABLEHOURS = masterYearlyEngineeringHoursParam.availableHours;
            add.BASELINEHOURS = masterYearlyEngineeringHoursParam.baselineHours;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERYEARLYENGINEERINGHOURS.Add(add);
            db.SaveChanges();

            masterYearlyEngineeringHoursParam.id = add.OID;
            return Ok(masterYearlyEngineeringHoursParam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MASTERYEARLYENGINEERINGHOURSExists(int id)
        {
            return db.MASTERYEARLYENGINEERINGHOURS.Count(e => e.OID == id) > 0;
        }
    }
}