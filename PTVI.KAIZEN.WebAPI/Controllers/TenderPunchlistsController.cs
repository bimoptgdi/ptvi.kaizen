﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TenderPunchlistsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TenderPunchlists
        public IQueryable<TENDERPUNCHLIST> GetTENDERPUNCHLISTs()
        {
            return db.TENDERPUNCHLISTs;
        }

        // GET: api/TenderPunchlists/5
        [ResponseType(typeof(TENDERPUNCHLIST))]
        public IHttpActionResult GetTENDERPUNCHLIST(int id)
        {
            TENDERPUNCHLIST tENDERPUNCHLIST = db.TENDERPUNCHLISTs.Find(id);
            if (tENDERPUNCHLIST == null)
            {
                return NotFound();
            }

            return Ok(tENDERPUNCHLIST);
        }

        // PUT: api/TenderPunchlists/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDERPUNCHLIST(int id, TENDERPUNCHLIST tENDERPUNCHLIST)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDERPUNCHLIST.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDERPUNCHLIST).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDERPUNCHLISTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TenderPunchlists
        [ResponseType(typeof(TENDERPUNCHLIST))]
        public IHttpActionResult PostTENDERPUNCHLIST(TENDERPUNCHLIST tENDERPUNCHLIST)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDERPUNCHLISTs.Add(tENDERPUNCHLIST);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDERPUNCHLIST.OID }, tENDERPUNCHLIST);
        }

        // DELETE: api/TenderPunchlists/5
        [ResponseType(typeof(TENDERPUNCHLIST))]
        public IHttpActionResult DeleteTENDERPUNCHLIST(int id)
        {
            TENDERPUNCHLIST tENDERPUNCHLIST = db.TENDERPUNCHLISTs.Find(id);
            if (tENDERPUNCHLIST == null)
            {
                return NotFound();
            }

            db.TENDERPUNCHLISTs.Remove(tENDERPUNCHLIST);
            db.SaveChanges();

            return Ok(tENDERPUNCHLIST);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TENDERPUNCHLISTExists(int id)
        {
            return db.TENDERPUNCHLISTs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("TenderPunchlistById")]
        public tenderPunchlistObject TenderPunchlistById(int param)
        {
            return db.TENDERPUNCHLISTs.Where(w => w.OID == param).Select(s => new tenderPunchlistObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                punchlist = s.PUNCHLIST,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                completePercentage = s.COMPLETEPERCENTAGE,
                issue = s.ISSUE,
                issueStatus = s.ISSUESTATUS
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("TenderPunchlistsByTenderId")]
        public IEnumerable<tenderPunchlistObject> TenderPunchlistsByTenderId(int param)
        {
            return db.TENDERPUNCHLISTs.Where(w => w.TENDERID == param).Select(s => new tenderPunchlistObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                punchlist = s.PUNCHLIST,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                completePercentage = s.COMPLETEPERCENTAGE,
                issue = s.ISSUE,
                issueStatus = s.ISSUESTATUS
            });
        }

        [HttpPut]
        [ActionName("TenderPunchlistSubmit")]
        public IHttpActionResult TenderPunchlistSubmit(int param, tenderPunchlistObject punchlistParam)
        {
            TENDERPUNCHLIST punchlist = new TENDERPUNCHLIST();
            if (punchlistParam.id > 0)
            {
                punchlist = db.TENDERPUNCHLISTs.Find(punchlistParam.id);
                punchlist.UPDATEDBY = GetCurrentNTUserId();
                punchlist.UPDATEDDATE = DateTime.Now;
            }
            else
            {
                punchlist.CREATEDBY = GetCurrentNTUserId();
                punchlist.CREATEDDATE = DateTime.Now;
                punchlist.TENDERID = param;
            }
            punchlist.PUNCHLIST = punchlistParam.punchlist;
            punchlist.PLANSTARTDATE = punchlistParam.planStartDate;
            punchlist.PLANFINISHDATE = punchlistParam.planFinishDate;
            punchlist.ACTUALSTARTDATE = punchlistParam.actualStartDate;
            punchlist.ACTUALFINISHDATE = punchlistParam.actualFinishDate;
            punchlist.COMPLETEPERCENTAGE = punchlistParam.completePercentage;
            punchlist.ISSUE = punchlistParam.issue;
            punchlist.ISSUESTATUS = punchlistParam.issueStatus;

            if (punchlistParam.id > 0)
                db.Entry(punchlist).State = EntityState.Modified;
            else
                db.Entry(punchlist).State = EntityState.Added;
            db.SaveChanges();

            return Ok(TenderPunchlistById(punchlist.OID));
        }
    }
}