﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class UserController : KAIZENController
    {
        // GET api/User
        public IEnumerable<USER> GetUSERs()
        {
            IEnumerable<USER> listUser = db.USERs;
            foreach(USER user in listUser)
            {
                IEnumerable<USERROLE> listUserRole = user.USERROLEs;
                foreach (USERROLE userRole in listUserRole)
                {
                    ROLE role = userRole.ROLE;
                    var temp1 = role.USERROLEs;
                    role.USERROLEs = null;

                    IEnumerable<ROLEACCESSRIGHT> listRoleAccessRight = userRole.ROLE.ROLEACCESSRIGHTs;
                    foreach (ROLEACCESSRIGHT roleAccessRight in listRoleAccessRight)
                    {
                        var temp3 = roleAccessRight.ROLE;
                        roleAccessRight.ROLE = null;
                        ACCESSRIGHT accessRight = roleAccessRight.ACCESSRIGHT;
                        if (accessRight != null)
                        {
                            var temp4 = accessRight.ROLEACCESSRIGHTs;
                            accessRight.ROLEACCESSRIGHTs = null;
                            IEnumerable<MENUACCESSRIGHT> listMenuAccessRight = accessRight.MENUACCESSRIGHTs;
                            foreach (MENUACCESSRIGHT menuAccessRight in listMenuAccessRight)
                            {
                                var temp5 = menuAccessRight.ACCESSRIGHT;
                                menuAccessRight.ACCESSRIGHT = null;

                            }
                        }
                    }

                    USER user2 = userRole.USER;
                    var temp2 = user2.USERROLEs;
                    user2.USERROLEs = null;
                }
            }

            return listUser.AsEnumerable();
        }

        // GET api/User/5
        public USER GetUSER(int id)
        {
            USER user = db.USERs.Find(id);
            IEnumerable<USERROLE> listUserRole = user.USERROLEs;
            foreach(USERROLE userRole in listUserRole)
            {
                ROLE role = userRole.ROLE;
                var temp1 = role.USERROLEs;
                role.USERROLEs = null;
            }
            if (user == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return user;
        }

        // PUT api/User/5
        public HttpResponseMessage PutUSER(int id, USER user)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != user.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/User
        public HttpResponseMessage PostUSER(USER user)
        {
            if (ModelState.IsValid)
            {
                db.USERs.Add(user);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, user);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = user.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/User/5
        public HttpResponseMessage DeleteUSER(int id)
        {
            USER user = db.USERs.Find(id);
            if (user == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.USERs.Remove(user);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [HttpGet]
        [ActionName("FindByNTUserID")]
        public IEnumerable<USER> FindByNTUserID(string param)
        {
            return db.USERs.Where(d => d.NTUSERID == param);
        }

        [HttpGet]
        [ActionName("FindByBadgeNo")]
        public IEnumerable<USER> FindByBadgeNo(string param)
        {
            return db.USERs.Where(d => d.BADGENO == param);
        }

        [HttpGet]
        [ActionName("FindListDepartment")]
        public HttpResponseMessage FindListDepartment(string param)
        {
            try
            {
                //string partNumber = materialOdata.PART_NUMBER;
                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {
                    //List<Dictionary<string, dynamic>> materialData = GetJsonDataFromWebApi(generalParam.VALUE + "externaldata/executequerybyparam/QRY_DEPARTMENT3");
                    List<Dictionary<string, dynamic>> materialData;
                    if (param == "all") {
                        materialData = GetJsonDataFromWebApi(generalParam.VALUE + "externaldata/executequerybyparam/QRY_DEPARTMENT_EX");
                    }
                    else {
                        materialData = GetJsonDataFromWebApi(generalParam.VALUE + "externaldata/executequerybyparam?param=QRY_DEPARTMENT_EX&$filter= TABLE_CODE eq " + param);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, materialData);
                }

            return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.ToString());
            }

        }

        [HttpGet]
        [ActionName("FindContractorByBN")]
        public IHttpActionResult FindContractorByBN(string param)
        {
            try
            {
                List<contractorObject> contractorData = FindContractor(param);

                return Ok(contractorData);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.ToString());
            }

        }

        public List<contractorObject> FindContractor(string param)
        {
            try
            {
                //List<Dictionary<string, dynamic>> contractorData = new List<Dictionary<string, dynamic>>();
                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {
                    //contractorData = GetJsonDataFromWebApi(generalParam.VALUE + "externaldata/executequerybyparam/QRY_CT_EMP_INFO_BYBNLIKE|@bn=" + param);
                    List<contractorObject> contractor = GetListFromExternalData<contractorObject>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_CT_EMP_INFO_BYBNLIKE|@bn=" + param);
                    return contractor;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

        }

        [HttpGet]
        [ActionName("IsDebugUser")]
        public bool IsDebugUser(string param)
        {
            var debugUserSetting = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DEBUG_USER).FirstOrDefault();
            if (debugUserSetting == null)
            {
                return false;
            }

            var debugUsers = debugUserSetting.VALUE.Split(';');

            return debugUsers.Contains(param);
        }

        [HttpGet]
        [ActionName("FindLoginInfoByNtUserID")]
        public IEnumerable<object> FindLoginInfoByNtUserID(string param)
        {
            return db.USERs.Where(d => d.NTUSERID == param).Select(x => new
            {
                USERNAME = x.USERNAME,
                BADGENO = x.BADGENO,
                EMAIL = x.EMAIL,
                USERROLEs = x.USERROLEs.Select(y => new
                {
                    ROLE = new
                    {
                        OID = y.ROLE.OID.ToString(),
                        ROLETYPE = y.ROLE.ROLETYPE,
                        ROLENAME = y.ROLE.ROLENAME,
                        DESCRIPTION = y.ROLE.DESCRIPTION
                    }
                })
            }).ToArray();

        }

        [HttpGet]
        [ActionName("userNonEmployeeByUserName")]
        public IEnumerable<object> userNonEmployeeByUserName(string param)
        {
            return db.USERNONEMPLOYEEs.Where(w => w.USERNAME == param || w.BADGENO == param).Select(x => new userNonEmployeeObject
            {
                userName = x.USERNAME,
                badgeNo = x.BADGENO
            }).AsEnumerable();

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}