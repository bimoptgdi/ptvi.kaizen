﻿using Microsoft.Office.Project.Server.Library;
using Microsoft.ProjectServer.Client;
using Microsoft.SharePoint.Client;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTVI.KAIZEN.WebAPI.PSI_CustomFieldWS;
using PTVI.KAIZEN.WebAPI.PSI_ProjectWS;
using PTVI.KAIZEN.WebAPI.PSI_QueueWS;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Services.Protocols;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ShiftTargetPlanController: KAIZENController
    {
        [HttpGet]
        [ActionName("GenerateShiftTargetPlan")]
        public List<shiftTargetPlan> GenerateShiftTargetPlan(int startProjectShiftID, int endProjectShiftID)
        {
            var startProjectShift = db.PROJECTSHIFTs.Find(startProjectShiftID);
            var endProjectShift = db.PROJECTSHIFTs.Find(endProjectShiftID);

            var projectShiftsInRange = db.PROJECTSHIFTs.Where(x => x.PROJECTID == startProjectShift.PROJECTID &&
                x.SHIFTID >= startProjectShift.SHIFTID && x.SHIFTID <= endProjectShift.SHIFTID).ToList();

            var shiftTargetPlanResult = new List<shiftTargetPlan>();
            foreach (var ps in projectShiftsInRange)
            {
                var shiftGroupList = GenerateShiftTarget(ps.OID).ToList();

                shiftTargetPlanResult.Add(new shiftTargetPlan()
                {
                    projectShift = new projectShiftObject()
                    {
                        id = ps.OID,
                        shift = ps.SHIFTID.ToString(),
                        startDate = ps.STARTDATE.Value,
                        endDate = ps.ENDDATE.Value
                    },
                    shiftTargetGroupList = shiftGroupList
                });
            }

            return shiftTargetPlanResult;
        }


        [HttpGet]
        [ActionName("GenerateShiftTarget")]
        public IEnumerable<shiftTargetGroup> GenerateShiftTarget(int projectShiftID)
        {
            var maxTaskLevel = shiftTargetMaxTaskLevel;
            //prjName = "Project Demo - 2";
            VALE_KAIZENEntities db = new VALE_KAIZENEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            Guid sessionUid = Guid.NewGuid();
            try
            {
                ProjectWS.CheckOutProject(projectUid, sessionUid, "Calculate shift target Plan");
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                var hasCheckedOut = false;
                var message = "";
                foreach (var err in errors)
                {
                    if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutInOtherSession)
                    {
                        message = "You have checkout this project in another session. Please checkin it and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutToOtherUser)
                    {
                        message = "Other user has checkout this project. Please wait it is checked in and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOAlreadyCheckedOutInSameSession ||
                        err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCannotCheckOutVisibilityModeProjectWithMppInDocLib)
                    {
                        message = "This project has been checkout. Please wait it is checked in and try again";
                        hasCheckedOut = true;
                    }
                }

                if (hasCheckedOut)
                    throw new ApplicationException(message);
                else
                    throw exc;
            }

            var projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var projectName = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow).PROJ_NAME;

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);

            try
            {
                // set to zero
                //var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var leave in excludeLeaves)
                {
                    var t = leave as ProjectDataSet.TaskRow;

                    // set start and end equal date to make it value 0
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                }

                var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var row in includedLeaves)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;

                    var startDate = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                    var endDate = taskRow.IsTB_FINISHNull() ? taskRow.TASK_FINISH_DATE : taskRow.TB_FINISH;

                    if (shiftTargetDate.Value < endDate)
                        endDate = shiftTargetDate.Value;

                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                        newStartDateCFRow.DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                    }


                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                        endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);
                    }
                }

                if (projectDs.TaskCustomFields.GetChanges() != null && projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                {
                    // publish project to appy calculation
                    Guid jobUid = Guid.NewGuid();
                    var changesDS2 = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                    ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changesDS2, false);
                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");
                    WaitForQueue(q, jobUid);
                }
                else
                {

                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }


                var tasksRows = projectDs.Task.Select("TASK_OUTLINE_LEVEL >= " + maxTaskLevel +
                    " AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'" +
                    " AND TASK_PCT_COMP <> 100");

                while (tasksRows.Length == 0)
                {
                    maxTaskLevel--;

                    tasksRows = projectDs.Task.Select("TASK_OUTLINE_LEVEL >= " + maxTaskLevel +
                    " AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'" +
                    " AND TASK_PCT_COMP <> 100");
                }

                PSI_ProjectWS.ProjectDataSet.TaskDataTable tasks = new ProjectDataSet.TaskDataTable();
                foreach (var row in tasksRows)
                    tasks.ImportRow(row);

                var groupList = new List<shiftTargetGroup>();
                var levelMaxRows = tasks.Select("TASK_OUTLINE_LEVEL = " + maxTaskLevel);
                foreach (var row in levelMaxRows)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    groupList.Add(new shiftTargetGroup()
                    {
                        ID = taskRow.TASK_UID,
                        groupName = projectName,
                        projectName = taskRow.TASK_NAME,
                        shiftName = projectShift.SHIFTID.ToString(),
                        Date = DateTime.Now,
                        percentComplete = taskRow.IsTASK_PCT_COMPNull() ? null : new int?(taskRow.TASK_PCT_COMP),
                        outlineNumber = taskRow.TASK_OUTLINE_NUM
                    });
                }

                List<shiftTarget> result = new List<shiftTarget>();
                foreach (var row in tasks.Rows)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    if (!taskRow.TASK_IS_SUMMARY && tasks.Select("TASK_PARENT_UID = '" + taskRow.TASK_UID + "'").Length == 0)
                    {
                        shiftTarget st = new shiftTarget()
                        {
                            percentCompletePrevious = taskRow.TASK_PCT_COMP,
                            actualStart = taskRow.IsTASK_ACT_STARTNull() ? null : new DateTime?(taskRow.TASK_ACT_START),
                            actualFinish = taskRow.IsTASK_ACT_FINISHNull() ? null : new DateTime?(taskRow.TASK_ACT_FINISH),
                            taskID = taskRow.TASK_UID,
                            taskOutlineNum = taskRow.TASK_OUTLINE_NUM,
                            titleList = new List<string>(),
                            start = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START,
                            finish = taskRow.IsTB_FINISHNull() ? taskRow.TASK_FINISH_DATE : taskRow.TB_FINISH
                        };

                        populateShiftTarget(taskRow, st, maxTaskLevel);

                        result.Add(st);
                    }
                }

                calculateProgressByDate(ProjectWS, projectDs, projectUid, ref result);

                // exclude percentcomplete > 100
                result = result.Where(x => x.percentCompletePlan <= 100).ToList();

                populateShiftCustomField(projectDs, ref result, new Guid(cfExecutor1Uid), new Guid(cfSupervisorUid), new Guid(cfInPlannerUid), new Guid(cfCMUid));

                var groupNewList = new List<shiftTargetGroup>();

                populateGroupList(groupList, result, ref groupNewList);


                //set to group
                foreach (var group in groupNewList)
                {
                    group.shiftTargetList = result.Where(x => x.groupTaskId == group.ID && x.Executor == group.Executor && x.ConstructionManager == group.ConstructionManager).OrderBy(x=>x.taskOutlineNum).ToList();
                }

                return groupNewList.OrderBy(x => x.Executor).ThenBy(x=>x.ConstructionManager).ThenBy(x=>x.projectName);
            }
            catch (Exception exc)
            {
                try
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }
                catch { };

                throw exc;
            }
        }

        private void populateGroupList(List<shiftTargetGroup> groupList, List<shiftTarget> shiftTargetList, ref List<shiftTargetGroup> groupNewList)
        {
            var shiftCount = shiftTargetList.Select(e => new { e.groupTaskId, e.Executor, e.Supervisor, e.Planner, e.ConstructionManager })
                            .Distinct().ToList();
            foreach (var shift in shiftCount)
            {
                var selectedGroup = groupList.Where(x => x.ID == shift.groupTaskId).FirstOrDefault();
                shiftTargetGroup stg = new shiftTargetGroup()
                {
                    ID = selectedGroup.ID,
                    projectName = selectedGroup.projectName,
                    groupName = selectedGroup.groupName,
                    Executor = shift.Executor,
                    Supervisor = shift.Supervisor,
                    Planner = shift.Planner,
                    ConstructionManager = shift.ConstructionManager,
                    Date = selectedGroup.Date,
                    percentComplete = selectedGroup.percentComplete,
                    outlineNumber = selectedGroup.outlineNumber,
                    shiftName = selectedGroup.shiftName
                };
                groupNewList.Add(stg);
            }

        }

        private void populateShiftCustomField(ProjectDataSet projectDs, ref List<shiftTarget> shiftTargetList, Guid executorCFUid, Guid supervisorCFUid, Guid inPlannerCFUid, Guid cmCFUid)
        {
            foreach (var shiftTarget in shiftTargetList)
            {
                var filterExecutorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + executorCFUid + "'");
                if (filterExecutorRows.Length > 0)
                {
                    shiftTarget.Executor = filterExecutorRows[0].TEXT_VALUE;
                }

                var filterSupervisorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + supervisorCFUid + "'");
                if (filterSupervisorRows.Length > 0)
                {
                    shiftTarget.Supervisor = filterSupervisorRows[0].TEXT_VALUE;
                }

                var filterInPlannerRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + inPlannerCFUid + "'");
                if (filterInPlannerRows.Length > 0)
                {
                    shiftTarget.Planner = filterInPlannerRows[0].TEXT_VALUE;
                }

                var filterCMRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + shiftTarget.taskID + "' and " +
                                            "MD_PROP_UID = '" + cmCFUid + "'");
                if (filterCMRows.Length > 0)
                {
                    shiftTarget.ConstructionManager = filterCMRows[0].TEXT_VALUE;
                }
            }
        }

        private void populateGroupsCustomField(ProjectDataSet projectDs, ref List<shiftTargetGroup> groupList,
            Guid executorCFUid, Guid supervisorCFUid, Guid inPlannerCFUid, Guid cmCFUid)
        {
            foreach (var group in groupList)
            {
                var filterExecutorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + executorCFUid + "'");
                if (filterExecutorRows.Length > 0)
                {
                    //group.Executor = filterExecutorRows[0].TEXT_VALUE;
                }

                var filterSupervisorRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + supervisorCFUid + "'");
                if (filterSupervisorRows.Length > 0)
                {
                    group.Supervisor = filterSupervisorRows[0].TEXT_VALUE;
                }

                var filterInPlannerRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + inPlannerCFUid + "'");
                if (filterInPlannerRows.Length > 0)
                {
                    group.Planner = filterInPlannerRows[0].TEXT_VALUE;
                }

                var filterCMRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + group.ID + "' and " +
                                            "MD_PROP_UID = '" + cmCFUid + "'");
                if (filterCMRows.Length > 0)
                {
                    group.ConstructionManager = filterCMRows[0].TEXT_VALUE;
                }
            }
        }

        private void calculateProgressByDate(PSI_ProjectWS.Project ProjectWS,  ProjectDataSet projectDs, Guid projectId, ref List<shiftTarget> shiftTarget)
        {
            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            Guid cfTotalDurationUID = new Guid(cfTotalDuration);
            Guid durationCFUid = new Guid(cfCalcPlanDurationUid);
            foreach (var st in shiftTarget)
            {
                var taskDur = 0.0;

                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + st.taskID + "' and " +
                        "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + st.taskID + "' and " +
                            "MD_PROP_UID = '" + cfCalcPlanDurationUid + "'");

                if (shiftDurationRows.Length > 0)
                {
                    try
                    {
                        if (taskDur == 0)
                            st.percentCompletePlan = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftDurationRows[0].NUM_VALUE;
                            var percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                st.percentCompletePlan = 100;
                            else if (percentComplete < 0)
                                st.percentCompletePlan = 0;
                            else
                                st.percentCompletePlan = Convert.ToInt32(percentComplete);
                        }
                    }
                    catch { }
                }
            }            
        }

        static private void WaitForQueue(QueueSystem q, Guid jobId)
        {
            PSI_QueueWS.JobState jobState;
            const int QUEUE_WAIT_TIME = 2; // two seconds
            bool jobDone = false;
            string xmlError = string.Empty;
            int wait = 0;

            // Wait for the project to get through the queue.
            // Get the estimated wait time in seconds.
            wait = q.GetJobWaitTime(jobId);

            // Wait for it.
            System.Threading.Thread.Sleep(wait * 1000);
            // Wait until it is finished.

            do
            {
                // Get the job state.
                jobState = q.GetJobCompletionState(jobId, out xmlError);

                if (jobState == PSI_QueueWS.JobState.Success)
                {
                    jobDone = true;
                }
                else
                {
                    if (jobState == PSI_QueueWS.JobState.Unknown
                    || jobState == PSI_QueueWS.JobState.Failed
                    || jobState == PSI_QueueWS.JobState.FailedNotBlocking
                    || jobState == PSI_QueueWS.JobState.CorrelationBlocked
                    || jobState == PSI_QueueWS.JobState.Canceled)
                    {
                        // If the job failed, error out.
                        throw (new ApplicationException("Queue request " + jobState + " for Job ID " + jobId + ".\r\n" + xmlError));
                    }
                    else
                    {
                        Console.WriteLine("Job State: " + jobState + " for Job ID: " + jobId);
                        System.Threading.Thread.Sleep(QUEUE_WAIT_TIME * 1000);
                    }
                }
            }
            while (!jobDone);
        }

        private void populateShiftTarget(ProjectDataSet.TaskRow task, shiftTarget shiftTarget, int outlineLevelMax)
        {
            if (task.TASK_OUTLINE_LEVEL >= outlineLevelMax)
            {
                shiftTarget.titleList.Add(task.TASK_NAME);

                if (task.TASK_OUTLINE_LEVEL == outlineLevelMax)
                    shiftTarget.groupTaskId = task.TASK_UID;

                var taskRows = task.Table.Select("TASK_UID = '" + task.TASK_PARENT_UID + "'");

                if (taskRows.Length > 0)
                {
                    var parentTaskRow = taskRows[0] as ProjectDataSet.TaskRow;

                    populateShiftTarget(parentTaskRow, shiftTarget, outlineLevelMax);
                }
            }
        }


    }
}