﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class RoleAccessrightController : KAIZENController
    {
        // GET api/RoleAccessright
        public IEnumerable<ROLEACCESSRIGHT> GetROLEACCESSRIGHTs()
        {
            var roleaccessrights = db.ROLEACCESSRIGHTs.Include(r => r.ACCESSRIGHT).Include(r => r.ROLE);
            return roleaccessrights.AsEnumerable();
        }

        // GET api/RoleAccessright/5
        public ROLEACCESSRIGHT GetROLEACCESSRIGHT(int id)
        {
            ROLEACCESSRIGHT roleaccessright = db.ROLEACCESSRIGHTs.Find(id);
            if (roleaccessright == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return roleaccessright;
        }

        // PUT api/RoleAccessright/5
        public HttpResponseMessage PutROLEACCESSRIGHT(int id, ROLEACCESSRIGHT roleaccessright)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != roleaccessright.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(roleaccessright).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/RoleAccessright
        public HttpResponseMessage PostROLEACCESSRIGHT(ROLEACCESSRIGHT roleaccessright)
        {
            if (ModelState.IsValid)
            {
                db.ROLEACCESSRIGHTs.Add(roleaccessright);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, roleaccessright);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = roleaccessright.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/RoleAccessright/5
        public HttpResponseMessage DeleteROLEACCESSRIGHT(int id)
        {
            ROLEACCESSRIGHT roleaccessright = db.ROLEACCESSRIGHTs.Find(id);
            if (roleaccessright == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.ROLEACCESSRIGHTs.Remove(roleaccessright);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, roleaccessright);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}