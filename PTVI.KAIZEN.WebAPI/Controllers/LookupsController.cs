﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class LookupsController : KAIZENController
    {
        // GET api/Lookups
        public IEnumerable<LOOKUP> GetLOOKUPs()
        {
            return db.LOOKUPs.AsEnumerable();
        }

        // GET api/Lookups/5
        public LOOKUP GetLOOKUP(int id)
        {
            LOOKUP lookup = db.LOOKUPs.Find(id);
            if (lookup == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return lookup;
        }

        // PUT api/Lookups/5
        public HttpResponseMessage PutLOOKUP(int id, LOOKUP lookup)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != lookup.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            lookup.UPDATEDBY = GetCurrentNTUserId();
            lookup.UPDATEDDATE = DateTime.Now;
            db.Entry(lookup).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, lookup);
        }

        // POST api/Lookups
        public HttpResponseMessage PostLOOKUP(LOOKUP lookup)
        {
            if (ModelState.IsValid)
            {
                lookup.CREATEDBY = GetCurrentNTUserId();
                lookup.CREATEDDATE = DateTime.Now;
                db.LOOKUPs.Add(lookup);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, lookup);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = lookup.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Lookups/5
        public HttpResponseMessage DeleteLOOKUP(int id)
        {
            LOOKUP lookup = db.LOOKUPs.Find(id);
            if (lookup == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.LOOKUPs.Remove(lookup);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, lookup);
        }

        #region CustomAction
        // api/lookup/getbytype/type
        [HttpGet]
        [ActionName("FindByType")]
        public IEnumerable<lookupsObject> FindByType(string param)
        {
            return db.LOOKUPs.Where(x => x.TYPE == param && x.ISACTIVE == true).Select(s => new lookupsObject
            {
                id = s.OID,
                type = s.TYPE,
                text = s.TEXT,
                value = s.VALUE,
                description = s.DESCRIPTION
            });
        }
        [HttpGet]
        [ActionName("FindByTypeDCEWP")]
        public IEnumerable<lookupsObject> FindByTypeDCEWP(string param)
        {
            return db.LOOKUPs.Where(x => x.TYPE == constants.DOCUMENTTYPE && (x.VALUE == constants.DOCUMENTTYPE_ENGINEER || x.VALUE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY) && x.ISACTIVE == true).Select(s => new lookupsObject
            {
                id = s.OID,
                type = s.TYPE,
                text = s.TEXT,
                value = s.VALUE,
                description = s.DESCRIPTION
            });
        }

        //param = TYPE|CODE
        [HttpGet]
        [ActionName("FindByTypeAndValue")]
        public IEnumerable<lookupsObject> FindByTypeAndValue(string param)
        {
            string[] pars = param.Split('|');
            IEnumerable<lookupsObject> lookups;
            if (pars.Length > 1)
            {
                string type = pars[0];
                string code = pars[1];
                lookups = db.LOOKUPs.Where(x => x.TYPE == type && x.ISACTIVE == true && x.VALUE == code).Select(s => new lookupsObject
                {
                    id = s.OID,
                    type = s.TYPE,
                    text = s.TEXT,
                    value = s.VALUE,
                    description = s.DESCRIPTION
                });

                return lookups;
                //if (lookups.Count() > 0)
                //    return lookups;
                //else
                //    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            else
            {
                lookups = db.LOOKUPs.Where(x => x.TYPE == param && x.ISACTIVE == true).Select(s => new lookupsObject
                {
                    id = s.OID,
                    type = s.TYPE,
                    text = s.TEXT,
                    value = s.VALUE,
                    description = s.DESCRIPTION
                });

                if (lookups != null)
                    return lookups;
                else
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            }

        }

        [HttpGet]
        [ActionName("FindByTypeAndWithoutValue")]
        public IEnumerable<LOOKUP> FindByTypeAndWithoutValue(string param)
        {
            string[] pars = param.Split('|');
            IEnumerable<LOOKUP> lookups;
            if (pars.Length > 1)
            {
                string type = pars[0];
                List<string> code = pars[1].Split(',').ToList();
                //return db.LOOKUPs.Where(x => x.TYPE == type && x.VALUE != code && x.ISACTIVE == true);
                lookups = db.LOOKUPs.Where(x => x.TYPE == type && x.ISACTIVE == true && !code.Contains(x.VALUE));
                //lookups = lookups.Except(code);

                return lookups;

            }

            return null;
        }
        #endregion

        [HttpGet]
        [ActionName("FindLookupByType")]
        public IEnumerable<LOOKUP> FindLookupByType(string param)
        {
            return db.LOOKUPs.Where(x => x.TYPE == param && x.ISACTIVE == true);
        }


        [HttpPut]
        [ActionName("updateLookup")]
        public IHttpActionResult updateLookup(int param, LOOKUP LookupParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != LookupParam.OID)
            {
                return BadRequest();
            }

            try
            {
                LOOKUP update = db.LOOKUPs.Find(param);
                update.OID = LookupParam.OID;
                update.TYPE = LookupParam.TYPE;
                update.TEXT = LookupParam.TEXT;
                update.VALUE = LookupParam.VALUE;
                update.DESCRIPTION = LookupParam.DESCRIPTION;
                update.ISACTIVE = LookupParam.ISACTIVE;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(LookupParam);
        }


        [HttpPost]
        [ActionName("addLookup")]
        public IHttpActionResult addLookup(string param, LOOKUP LookupParam)
        {
            LOOKUP add = new LOOKUP();
            add.OID = LookupParam.OID;
            add.TYPE = LookupParam.TYPE;
            add.TEXT = LookupParam.TEXT;
            add.VALUE = LookupParam.VALUE;
            add.DESCRIPTION = LookupParam.DESCRIPTION;
            add.ISACTIVE = LookupParam.ISACTIVE;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.LOOKUPs.Add(add);
            db.SaveChanges();

            LookupParam.OID = add.OID;
            return Ok(LookupParam);
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}