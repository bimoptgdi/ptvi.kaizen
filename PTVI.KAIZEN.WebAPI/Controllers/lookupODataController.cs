﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PTVI.RESERVATION.WebAPI.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<lookup>("lookupOData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class lookupODataController : ODataController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: odata/lookupOData
        [EnableQuery]
        public IQueryable<LOOKUP> GetlookupOData()
        {
            return db.LOOKUPs;
        }

        // GET: odata/lookupOData(5)
        //[EnableQuery]
        //public SingleResult<LOOKUP> Getlookup([FromODataUri] string type, ([FromODataUri] string value)
        //{
        //    return SingleResult.Create(db.LOOKUPs.));
        //}

        //// PUT: odata/lookupOData(5)
        //public IHttpActionResult Put([FromODataUri] int key, Delta<LOOKUP> patch)
        //{
        //    Validate(patch.GetEntity());

        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    LOOKUP lookup = db.LOOKUPs.Find(key);
        //    if (lookup == null)
        //    {
        //        return NotFound();
        //    }

        //    patch.Put(lookup);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!lookupExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Updated(lookup);
        //}

        //// POST: odata/lookupOData
        //public IHttpActionResult Post(LOOKUP lookup)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.LOOKUPs.Add(lookup);
        //    db.SaveChanges();

        //    return Created(lookup);
        //}

        //// PATCH: odata/lookupOData(5)
        //[AcceptVerbs("PATCH", "MERGE")]
        //public IHttpActionResult Patch([FromODataUri] int key, Delta<LOOKUP> patch)
        //{
        //    Validate(patch.GetEntity());

        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    LOOKUP lookup = db.LOOKUPs.Find(key);
        //    if (lookup == null)
        //    {
        //        return NotFound();
        //    }

        //    patch.Patch(lookup);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!lookupExists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Updated(lookup);
        //}

        //// DELETE: odata/lookupOData(5)
        //public IHttpActionResult Delete([FromODataUri] int key)
        //{
        //    LOOKUP lookup = db.LOOKUPs.Find(key);
        //    if (lookup == null)
        //    {
        //        return NotFound();
        //    }

        //    db.LOOKUPs.Remove(lookup);
        //    db.SaveChanges();

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        private bool lookupExists(string type, string value)
        {
            return db.LOOKUPs.Count(e => e.TYPE == type && e.VALUE == value) > 0;
        }
    }
}
