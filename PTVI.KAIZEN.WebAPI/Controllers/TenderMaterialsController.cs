﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TenderMaterialsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TenderMaterials
        public IQueryable<TENDERMATERIAL> GetTENDERMATERIALs()
        {
            return db.TENDERMATERIALs;
        }

        // GET: api/TenderMaterials/5
        [ResponseType(typeof(TENDERMATERIAL))]
        public IHttpActionResult GetTENDERMATERIAL(int id)
        {
            TENDERMATERIAL tENDERMATERIAL = db.TENDERMATERIALs.Find(id);
            if (tENDERMATERIAL == null)
            {
                return NotFound();
            }

            return Ok(tENDERMATERIAL);
        }

        // PUT: api/TenderMaterials/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDERMATERIAL(int id, TENDERMATERIAL tENDERMATERIAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDERMATERIAL.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDERMATERIAL).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDERMATERIALExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TenderMaterials
        [ResponseType(typeof(TENDERMATERIAL))]
        public IHttpActionResult PostTENDERMATERIAL(TENDERMATERIAL tENDERMATERIAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDERMATERIALs.Add(tENDERMATERIAL);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDERMATERIAL.OID }, tENDERMATERIAL);
        }

        // DELETE: api/TenderMaterials/5
        [ResponseType(typeof(TENDERMATERIAL))]
        public IHttpActionResult DeleteTENDERMATERIAL(int id)
        {
            TENDERMATERIAL tENDERMATERIAL = db.TENDERMATERIALs.Find(id);
            if (tENDERMATERIAL == null)
            {
                return NotFound();
            }

            db.TENDERMATERIALs.Remove(tENDERMATERIAL);
            db.SaveChanges();

            return Ok(tENDERMATERIAL);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TENDERMATERIALExists(int id)
        {
            return db.TENDERMATERIALs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("TenderMaterialById")]
        public tenderMaterialObject TenderMaterialById(int param)
        {
            return db.TENDERMATERIALs.Where(w => w.OID == param).Select(s => new tenderMaterialObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                handover = s.HANDOVER,
                mirNo = s.MIRNO,
                materialComponentId = s.MATERIALCOMPONENTID,
                //from MATERIALCOMPONENT
                mrNo = s.MATERIALCOMPONENT.MRNO,
                poNo = s.MATERIALCOMPONENT.PONO,
                poItemNo = s.MATERIALCOMPONENT.POITEMNO,
                reservation = s.MATERIALCOMPONENT.RESERVATION,
                reservationItemNo = s.MATERIALCOMPONENT.RESERVATIONITEMNO,
                materialDesc = s.MATERIALCOMPONENT.MATERIALDESC,
                status = s.MATERIALCOMPONENT.MATERIALSTATUS.FirstOrDefault().LOCATION,
                usedQuantity = s.MATERIALCOMPONENT.ORDERQUANTITY,
                remainingQuantity = s.MATERIALCOMPONENT.WITHDRAWNQUANTITY,
                remainingLocation = s.MATERIALCOMPONENT.MATERIALSTATUS.FirstOrDefault().LOCATION
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("TenderMaterialsByTenderId")]
        public IEnumerable<tenderMaterialObject> TenderMaterialsByTenderId(int param)
        {
            return db.TENDERMATERIALs.Where(w => w.TENDERID == param).Select(s => new tenderMaterialObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                handover = s.HANDOVER,
                mirNo = s.MIRNO,
                materialComponentId = s.MATERIALCOMPONENTID,
                //from MATERIALCOMPONENT
                mrNo = s.MATERIALCOMPONENT.MRNO,
                poNo = s.MATERIALCOMPONENT.PONO,
                poItemNo = s.MATERIALCOMPONENT.POITEMNO,
                reservation = s.MATERIALCOMPONENT.RESERVATION,
                reservationItemNo = s.MATERIALCOMPONENT.RESERVATIONITEMNO,
                materialDesc = s.MATERIALCOMPONENT.MATERIALDESC,
                status = s.MATERIALCOMPONENT.MATERIALSTATUS.FirstOrDefault().LOCATION,
                usedQuantity = s.MATERIALCOMPONENT.ORDERQUANTITY,
                remainingQuantity = s.MATERIALCOMPONENT.WITHDRAWNQUANTITY,
                remainingLocation = s.MATERIALCOMPONENT.MATERIALSTATUS.FirstOrDefault().LOCATION
            });
        }

        [HttpPut]
        [ActionName("TenderMaterialSubmit")]
        public IHttpActionResult TenderMaterialSubmit(int param, tenderMaterialObject materialParam)
        {
            TENDERMATERIAL material = new TENDERMATERIAL();
            if (materialParam.id > 0)
            {
                material = db.TENDERMATERIALs.Find(materialParam.id);
                material.UPDATEDBY = GetCurrentNTUserId();
                material.UPDATEDDATE = DateTime.Now;
            }
            else
            {
                material.CREATEDBY = GetCurrentNTUserId();
                material.CREATEDDATE = DateTime.Now;
                material.TENDERID = param;
            }
            material.MATERIALCOMPONENTID = materialParam.materialComponentId;
            material.HANDOVER = materialParam.handover;

            //waiting confirmation
            //material.MIRNO = materialParam.mirNo;

            //harusnya mengambil dari material component
            //material.USEDQUANTITY = materialParam.usedQuantity;
            //material.REMAININGQUANTITY = materialParam.remainingQuantity;
            //material.REMAININGLOCATION = materialParam.remainingLocation;

            if (materialParam.id > 0)
                db.Entry(material).State = EntityState.Modified;
            else
                db.Entry(material).State = EntityState.Added;
            db.SaveChanges();

            return Ok(TenderMaterialById(material.OID));
        }
    }
}