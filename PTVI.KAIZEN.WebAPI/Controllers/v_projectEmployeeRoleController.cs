﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class v_projectEmployeeRoleController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET api/Project
        public IEnumerable<v_projectEmployeeRoleObject> GetV_projectEmployeeRoles()
        {
            return db.V_PROJECTEMPLOYEEROLE.Select(s => new v_projectEmployeeRoleObject
            {
                id = s.OID,
                projectno = s.PROJECTNO,
                projectdescription = s.PROJECTDESCRIPTION,
                planstartdate = s.PLANSTARTDATE,
                planfinishdate = s.PLANFINISHDATE,
                actualstartdate = s.ACTUALSTARTDATE,
                actualfinishdate = s.ACTUALFINISHDATE,
                employeeid = s.EMPLOYEEID,
                employeename = s.EMPLOYEENAME,
                rolelevel = s.RoleLevel,
                rolename = s.RoleName
            });
        }

        [HttpGet]
        [ActionName("V_projectEmployeeRoles_ByRole")]
        public object V_projectEmployeeRoles_ByRole(int param)
        {
            return db.V_PROJECTEMPLOYEEROLE.GroupBy(g => new { g.RoleLevel, g.RoleName }).
                Select(sg => new {
                    roleLevel = sg.Key.RoleLevel,
                    roleName = sg.Key.RoleName,
                    detail = db.V_PROJECTEMPLOYEEROLE.Where(w => w.RoleLevel == sg.Key.RoleLevel).
                            GroupBy(g => new { g.EMPLOYEEID, g.EMPLOYEENAME, g.RoleLevel }).Select(s => new
                            {
                                employeeid = s.Key.EMPLOYEEID,
                                employeename = s.Key.EMPLOYEENAME
                            })
                });
            //.
            //            Join(db.V_PROJECTEMPLOYEEROLE,
            //                Master => Master.RoleLevel,
            //                Detail => Detail.RoleLevel,
            //                (Master, Detail) => new { master = Master, detail = Detail }
            //                );

        }

        [HttpGet]
        [ActionName("V_projectEmployeeRoles_ByRoleReportBudget")]
        public object V_projectEmployeeRoles_ByRoleReportBudget(int param)
        {
            return db.V_PROJECTEMPLOYEEROLE.Where(w => w.RoleName == "PE" || w.RoleName == "Engineer").GroupBy(g => new { g.RoleLevel, g.RoleName }).ToList().
                Select(sg => new {
                    roleLevel = sg.Key.RoleLevel,
                    roleName = sg.Key.RoleName,
                    detail = db.V_PROJECTEMPLOYEEROLE.Where(w => w.RoleLevel == sg.Key.RoleLevel).
                            GroupBy(g => new { g.EMPLOYEEID, g.EMPLOYEENAME, g.RoleLevel }).Select(s => new
                            {
                                employeeid = s.Key.EMPLOYEEID,
                                employeename = s.Key.EMPLOYEENAME
                            })
                });
        }

        [HttpGet]
        [ActionName("listEmployeeByRoleId")]
        public object listEmployeeByRoleId(int param)
        {
            return db.V_PROJECTEMPLOYEEROLE.Where(w => w.RoleLevel == param && !string.IsNullOrEmpty(w.EMPLOYEEID)).
                    GroupBy(g => new { g.EMPLOYEEID, g.EMPLOYEENAME, g.RoleName }).Select(s => new
                    {
                        employeeId = s.Key.EMPLOYEEID,
                        employeeName = s.Key.EMPLOYEENAME,
                        roleName = s.Key.RoleName
                    });
        }
    }
}
