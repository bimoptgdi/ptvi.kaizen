﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class masterDesignerComplexityController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        // GET api/masterDesignerComplexity
        public IQueryable<masterDesignerComplexityObject> GetMASTERCOMPLEXITYTDCs()
        {
           return db.MASTERCOMPLEXITYDCs.Select(s => new masterDesignerComplexityObject
           {
               id = s.OID,
               year = s.YEAR,
               category = s.CATEGORY,
               weight = s.WEIGHT
           });
        }

        // GET api/masterDesignerComplexity/getByYear/param
        [HttpGet]
        [ActionName("getByYear")]
        public IQueryable<masterDesignerComplexityObject> getByYear(int param)
        {
            return db.MASTERCOMPLEXITYDCs.Where(x => x.YEAR == param).Select(s => new masterDesignerComplexityObject
            {
                id = s.OID,
                year = s.YEAR,
                category = s.CATEGORY,
                weight = s.WEIGHT
            });
        }

        // GET api/masterDesignerComplexity/getMaxMinYearlyTargetSc/1
        [HttpGet]
        [ActionName("getMaxMinYearlyTargetSc")]
        public object getMaxMinYearlyTargetSc(int param)
        {
            IDictionary<string, int> dict = new Dictionary<string, int>();
            var min = db.MASTERYEARLYTARGETSCOREs.Min(w => w.YEAR);
            var max = db.MASTERYEARLYTARGETSCOREs.Max(w => w.YEAR);
            dict.Add("min", min.HasValue ? min.Value : 0);
            dict.Add("max", max.HasValue ? max.Value : 0);
            return dict;
        }

        // POST api/masterDesignerComplexity/addDesignerComplexity/1

        [HttpPost]
        [ActionName("addDesignerComplexity")]
        public IHttpActionResult addDesignerComplexity(string param, masterDesignerComplexityObject designerComplexityParam)
        {
            MASTERCOMPLEXITYDC add = new MASTERCOMPLEXITYDC();
            add.YEAR = designerComplexityParam.year;
            add.CATEGORY = designerComplexityParam.category;
            add.WEIGHT = designerComplexityParam.weight;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERCOMPLEXITYDCs.Add(add);
            //var n = db.SaveChanges();
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return Ok(ex);

                throw;
            }

            designerComplexityParam.id = add.OID;
            return Ok(designerComplexityParam);
        }
        // GET api/masterDesignerComplexity/cekDuplicateCategory/e.model.year
        [HttpGet]
        [ActionName("cekDuplicateCategory")]
        public Boolean cekDuplicateCategory(string param, int yearParam)
        {
            var duplicateCategory = db.MASTERCOMPLEXITYDCs.Where(w => w.CATEGORY == param && w.YEAR == yearParam);
            if (duplicateCategory.Count() > 0)
            {
                return true;
            }
            return false;
        }

        // PUT api/masterDesignerComplexity/updateDesignerComplexity/
        [HttpPut]
        [ActionName("updateDesignerComplexity")]
        public IHttpActionResult updateDesignerComplexity(int param, masterDesignerComplexityObject designerComplexityParam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (param != designerComplexityParam.id)
            {
                return BadRequest();
            }
            try
            {
                MASTERCOMPLEXITYDC update = db.MASTERCOMPLEXITYDCs.Find(param);
                update.YEAR = designerComplexityParam.year;
                update.CATEGORY = designerComplexityParam.category;
                update.WEIGHT = designerComplexityParam.weight;
                update.UPDATEDDATE = DateTime.Now;
                update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();
            }
              catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(designerComplexityParam);
        }

        // DELETE api/masterDesignerComplexity
        [ResponseType(typeof(MASTERCOMPLEXITYDC))]
        public IHttpActionResult DeleteMASTERCOMPLEXITYDC(int id)
        {
            MASTERCOMPLEXITYDC mASTERCOMPLEXITYDC = db.MASTERCOMPLEXITYDCs.Find(id);
            if (mASTERCOMPLEXITYDC == null)
            {
                return NotFound();
            }

            db.MASTERCOMPLEXITYDCs.Remove(mASTERCOMPLEXITYDC);
            db.SaveChanges();

            return Ok(mASTERCOMPLEXITYDC);
        }
    }
}