﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TenderProgressesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TenderProgresses
        public IQueryable<TENDERPROGRESS> GetTENDERPROGRESSes()
        {
            return db.TENDERPROGRESSes;
        }

        // GET: api/TenderProgresses/5
        [ResponseType(typeof(TENDERPROGRESS))]
        public IHttpActionResult GetTENDERPROGRESS(int id)
        {
            TENDERPROGRESS tENDERPROGRESS = db.TENDERPROGRESSes.Find(id);
            if (tENDERPROGRESS == null)
            {
                return NotFound();
            }

            return Ok(tENDERPROGRESS);
        }

        // PUT: api/TenderProgresses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTENDERPROGRESS(int id, TENDERPROGRESS tENDERPROGRESS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tENDERPROGRESS.OID)
            {
                return BadRequest();
            }

            db.Entry(tENDERPROGRESS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TENDERPROGRESSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TenderProgresses
        [ResponseType(typeof(TENDERPROGRESS))]
        public IHttpActionResult PostTENDERPROGRESS(TENDERPROGRESS tENDERPROGRESS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TENDERPROGRESSes.Add(tENDERPROGRESS);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tENDERPROGRESS.OID }, tENDERPROGRESS);
        }

        // DELETE: api/TenderProgresses/5
        [ResponseType(typeof(TENDERPROGRESS))]
        public IHttpActionResult DeleteTENDERPROGRESS(int id)
        {
            TENDERPROGRESS tENDERPROGRESS = db.TENDERPROGRESSes.Find(id);
            if (tENDERPROGRESS == null)
            {
                return NotFound();
            }

            db.TENDERPROGRESSes.Remove(tENDERPROGRESS);
            db.SaveChanges();

            return Ok(tENDERPROGRESS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        [ActionName("tenderProgressById")]
        public tenderProgressObject tenderProgressById(int param)
        {
            string getContractAwardValue = db.LOOKUPs.Where(w => w.TYPE == constants.TENDERPROGRESS).Max(m => m.VALUE);

            return db.TENDERPROGRESSes.Where(w => w.OID == param).ToList().Select(s => new tenderProgressObject
            {
                id = s.OID,
                tenderId = s.TENDERID,
                processType = s.PROCESSTYPE,
                processText = db.LOOKUPs.Where(w2 => w2.TYPE == constants.TENDERPROGRESS && w2.VALUE == s.PROCESSTYPE).Count() > 0 ? db.LOOKUPs.Where(w2 => w2.TYPE == constants.TENDERPROGRESS && w2.VALUE == s.PROCESSTYPE).FirstOrDefault().TEXT : null,
                processBy = s.PROCESSBY,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                status = s.STATUS,
                trafficStatus = s.PLANFINISHDATE.HasValue ? traficStatus(s.PLANFINISHDATE.Value, s.ACTUALFINISHDATE, s.STATUS) : null,
                contractAward = getContractAwardValue
            }).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("listTenderProgressByTenderId")]
        public IEnumerable<tenderProgressObject> listTenderProgressByTenderId(int param)
        {
            string getContractAwardValue = db.LOOKUPs.Where(w => w.TYPE == constants.TENDERPROGRESS).Max(m => m.VALUE);

            return db.TENDERPROGRESSes.Where(w => w.TENDERID == param).ToList().Select(s => new tenderProgressObject {
                id = s.OID,
                tenderId = s.TENDERID,
                processType = s.PROCESSTYPE,
                processText = db.LOOKUPs.Where(w2 => w2.TYPE == constants.TENDERPROGRESS && w2.VALUE == s.PROCESSTYPE).Count() > 0 ? db.LOOKUPs.Where(w2 => w2.TYPE == constants.TENDERPROGRESS && w2.VALUE == s.PROCESSTYPE).FirstOrDefault().TEXT : null,
                processBy = s.PROCESSBY,
                planStartDate = s.PLANSTARTDATE,
                planFinishDate = s.PLANFINISHDATE,
                actualStartDate = s.ACTUALSTARTDATE,
                actualFinishDate = s.ACTUALFINISHDATE,
                status = s.STATUS,
                trafficStatus = s.PLANFINISHDATE.HasValue ? traficStatus(s.PLANFINISHDATE.Value, s.ACTUALFINISHDATE, s.STATUS) : null,
                contractAward = getContractAwardValue
            });
        }

        [HttpPut]
        [ActionName("updateTenderProgresses")]
        public object updateTenderProgresses(int param, tenderProgressObject tenderProgressParam)
        {
            TENDERPROGRESS updateTender = db.TENDERPROGRESSes.Find(param);

            updateTender.PROCESSTYPE = tenderProgressParam.processType;
            updateTender.PROCESSBY = tenderProgressParam.processBy;
            updateTender.PLANSTARTDATE = tenderProgressParam.planStartDate;
            updateTender.PLANFINISHDATE = tenderProgressParam.planFinishDate;
            updateTender.ACTUALSTARTDATE = tenderProgressParam.actualStartDate;
            updateTender.ACTUALFINISHDATE = tenderProgressParam.actualFinishDate;
            updateTender.STATUS = tenderProgressParam.status;
            updateTender.UPDATEDBY = GetCurrentNTUserId();
            updateTender.UPDATEDDATE = DateTime.Now;

            db.Entry(updateTender).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(tenderProgressById(param));
        }
        
        private bool TENDERPROGRESSExists(int id)
        {
            return db.TENDERPROGRESSes.Count(e => e.OID == id) > 0;
        }
    }
}