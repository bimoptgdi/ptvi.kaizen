﻿using System;
using Excel;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using SourceCode.Workflow.Client;
using System.Data.Entity.Validation;
using PTVI.KAIZEN.WebAPI.K2Objects;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectBudgetApprovalController : KAIZENController
    {
       
     

        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/ProjectBudgetApproval
        public IQueryable<projectBudgetApprovalObject> GetProjectBudgetApproval()
        {
            return db.PROJECTBUDGETAPPROVALs.Select(s => new projectBudgetApprovalObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                budgetAmount = s.BUDGETAMOUNT,
                budgetStatus = s.BUDGETSTATUS,
                fileName = s.FILENAME,
            });
        }

        // GET: api/ProjectBudgetApproval/1
        //        [ResponseType(typeof(projectBudgetApprovalObject))]
        [HttpGet]
        [ActionName("GetPROJECTBUDGETAPPROVAL")]
        public IHttpActionResult GetPROJECTBUDGETAPPROVAL(int id)
        {
            PROJECTBUDGETAPPROVAL s = db.PROJECTBUDGETAPPROVALs.Find(id);
            if (s == null)
            {
                return NotFound();
            }

            var result = new projectBudgetApprovalObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                budgetAmount = s.BUDGETAMOUNT,
                budgetStatus = s.BUDGETSTATUS,
                fileName = s.FILENAME,
                refDocId = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.REFDOCID != null ? s.SUPPORTINGDOCUMENT.REFDOCID : null : null,
                submittedBy = s.SUBMITTEDBY,
                submittedDate = s.SUBMITTEDDATE,
                completionDate = s.COMPLETIONDATE,
                wfId = s.WFID,
                createdDate = s.CREATEDDATE,
                createdBy = s.CREATEDBY,
                updatedDate = s.UPDATEDDATE,
                updatedBy = s.UPDATEDBY,
                project = new projectObject
                {
                    projectNo = s.PROJECT.PROJECTNO,
                    projectDescription = s.PROJECT.PROJECTDESCRIPTION,
                    projectManagerName = s.PROJECT.PROJECTMANAGERNAME,
                    projectType = s.PROJECT.PROJECTTYPE,
                }
            };

            List<K2History> listK2History = new List<K2History>();
            try
            {

                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                if (generalParam != null)
                {

                    listK2History = GetListFromExternalData<K2History>(generalParam.VALUE + "externaldata/executequerybyparam/QRY_WORKLIST_HISTORY_BYWFIDIN%7C@wfID=" + result.wfId);

                }

                result.K2Histories = listK2History;
                result.k2CurrentActivityName = listK2History.Where(w => w.Status == "Active").Count() > 0 ? listK2History.FirstOrDefault(w => w.Status == "Active").ActivityName : null;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return Ok(result);
        }

        //// PUT: api/ProjectBudgetApproval/5
        //[ResponseType(typeof(void))]
        [HttpPut]
        [ActionName("PutPROJECTBUDGETAPPROVAL")]
        public IHttpActionResult PutPROJECTBUDGETAPPROVAL(int id, projectBudgetApprovalObject pROJECTBUDGETAPPROVAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTBUDGETAPPROVAL.id)
            {
                return BadRequest();
            }

            PROJECTBUDGETAPPROVAL s = db.PROJECTBUDGETAPPROVALs.Find(id);
            s.BUDGETAMOUNT = pROJECTBUDGETAPPROVAL.budgetAmount;
            s.UPDATEDBY = GetCurrentNTUserId();
            s.UPDATEDDATE = DateTime.Now;

            db.Entry(s).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTBUDGETAPPROVALExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            pROJECTBUDGETAPPROVAL.project = null;
            pROJECTBUDGETAPPROVAL.K2Histories = null;
            return Ok(pROJECTBUDGETAPPROVAL);
        }

        // POST: api/ProjectBudgetApproval
        [ResponseType(typeof(projectBudgetApprovalObject))]
        public IHttpActionResult PostProjectBudgetApproval(PROJECTBUDGETAPPROVAL pROJECTBUDGETAPPROVAL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROJECTBUDGETAPPROVAL s = new PROJECTBUDGETAPPROVAL();
            s.PROJECTID = pROJECTBUDGETAPPROVAL.PROJECTID;
            s.BUDGETAMOUNT = pROJECTBUDGETAPPROVAL.BUDGETAMOUNT;
            s.SUBMITTEDBY = GetCurrentNTUserId();
            s.SUBMITTEDDATE = DateTime.Now;
            s.CREATEDBY = GetCurrentNTUserId();
            s.CREATEDDATE = DateTime.Now;


            db.PROJECTBUDGETAPPROVALs.Add(pROJECTBUDGETAPPROVAL);
            db.SaveChanges();
            //*
            // call K2
            //registerToK2(s.OID); //=> dipindah setelah proses upload
            // --call K2
            //*/

            pROJECTBUDGETAPPROVAL.OID = s.OID;
            return CreatedAtRoute("DefaultApi", new { OID = pROJECTBUDGETAPPROVAL.OID }, pROJECTBUDGETAPPROVAL);
        }


        [HttpGet]
        [ActionName("getFileStream")]
        public string getFileStream(int param)
        {
            #region variable Get File Attachment Start
            PSDAPPROVAL s = db.PSDAPPROVALs.Find(param);
            String htmlCode = null;
            #endregion variable Get File Attachment Start

            #region Get File Attachment Start
            GENERALPARAMETER DocServer = db.GENERALPARAMETERs.Where(w => w.TITLE == "DocServer").FirstOrDefault();
            try
            {
                var urlFile = DocServer.VALUE.Replace("[id]", s.SUPPORTINGDOCUMENT.REFDOCID).Replace("[op]", "y");

                WebClient client = new WebClient();
                htmlCode = client.DownloadString(urlFile);

            }
            catch (Exception ex)
            {
                WriteLog("get file failed function getImageStream");
            }
            finally
            {

            }
            #endregion Get File Attachment 


            return htmlCode;
        }

        [HttpGet]
        [ActionName("registerToK2")]
        public string registerToK2(int param)
        {
            string result = "";
            PROJECTBUDGETAPPROVAL s = db.PROJECTBUDGETAPPROVALs.Find(param);
            //*
            // call K2
            K2ConnectController k2 = new K2ConnectController();
            System.IO.Stream _fileStream = null;
            System.IO.StreamWriter writer = null;

            try
            {
                // call K2
                string K2ProjectBudgetConnectCode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ProjectBudgetConnectCode;
                ProcessInstance edfProcessInstance = k2.Connect(K2ProjectBudgetConnectCode);

                string k2AppID = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2AppID;
                edfProcessInstance.DataFields[k2AppID].Value = s.OID;


                if (s.SUPPORTINGDOCUMENT != null)
                {
                    string htmlCode = getFileStream(param);

                    _fileStream = new System.IO.MemoryStream();
                    writer = new System.IO.StreamWriter(_fileStream);

                    //System.IO.MemoryStream stream = new System.IO.MemoryStream();
                    writer.Write(htmlCode);
                    writer.Flush();
                    _fileStream.Position = 0;
                    using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"D:\" + s.SUPPORTINGDOCUMENT.FILENAME))
                    {
                        outfile.Write(htmlCode);
                    }

                    edfProcessInstance.AddAttachment(s.SUPPORTINGDOCUMENT.FILENAME, _fileStream);
                }
                k2.StartProcessInstance(edfProcessInstance);
                result = "K2 Sukses";
            }
            catch (DbEntityValidationException exc)
            {
                var errorMessage = "";
                foreach (var error in exc.EntityValidationErrors)
                {
                    foreach (var validationError in error.ValidationErrors)
                        errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                }
                WriteLog("K2 Problem:");
                WriteLog(errorMessage);
                throw new ApplicationException(errorMessage);
                result = "K2 Problem";
            }
            catch (Exception ex)
            {
                WriteLog("K2 Problem:");
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
                result = "K2 Problem";
            }
            finally
            {
                if (_fileStream != null)
                    _fileStream.Close();

                if (writer != null)
                    writer.Close();

                k2.Disconnect();
            }
            return result;
        }

        // DELETE: api/ProjectBudgetApproval/5
        [ResponseType(typeof(projectBudgetApprovalObject))]
        public IHttpActionResult DeletePROJECTBUDGETAPPROVAL(int id)
        {
            PROJECTBUDGETAPPROVAL pROJECTBUDGETAPPROVAL = db.PROJECTBUDGETAPPROVALs.Find(id);
            if (pROJECTBUDGETAPPROVAL == null)
            {
                return NotFound();
            }

            db.PROJECTBUDGETAPPROVALs.Remove(pROJECTBUDGETAPPROVAL);
            db.SaveChanges();

            return Ok(pROJECTBUDGETAPPROVAL);
        }

        // GET: api/ProjectBudgetApproval
        [HttpGet]
        [ActionName("getBudgetApprovalByProjectID")]
        public IEnumerable<projectBudgetApprovalObject> getPsdByProjectID(int param)
        {
            return db.PROJECTBUDGETAPPROVALs.Where(w => w.PROJECTID == param).Select(s => new projectBudgetApprovalObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                budgetAmount = s.BUDGETAMOUNT,
                budgetStatus = s.BUDGETSTATUS,
                fileName = s.FILENAME,
                refDocId = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.REFDOCID != null ? s.SUPPORTINGDOCUMENT.REFDOCID : null : null,
                submittedBy = s.SUBMITTEDBY,
                submittedDate = s.SUBMITTEDDATE,
                completionDate = s.COMPLETIONDATE,
                wfId = s.WFID,
                createdDate = s.CREATEDDATE,
                createdBy = s.CREATEDBY,
                updatedDate = s.UPDATEDDATE,
                updatedBy = s.UPDATEDBY
            });
        }
        [HttpGet]
        [ActionName("getBudgetByProjectID")]
        public IEnumerable<projectBudgetApprovalObject> getBudgetByProjectID(int param)
        {
            return db.PROJECTBUDGETAPPROVALs.Where(w => w.PROJECTID == param).Select(s => new projectBudgetApprovalObject
            {
                id = s.OID,
                projectId = s.PROJECTID,
                budgetAmount = s.BUDGETAMOUNT,
                budgetStatus = s.BUDGETSTATUS,
                fileName = s.FILENAME,
                refDocId = s.DOCUMENTID.HasValue ? s.SUPPORTINGDOCUMENT.REFDOCID != null ? s.SUPPORTINGDOCUMENT.REFDOCID : null : null,
                submittedBy = s.SUBMITTEDBY,
                submittedDate = s.SUBMITTEDDATE,
                completionDate = s.COMPLETIONDATE,
                wfId = s.WFID,
                createdDate = s.CREATEDDATE,
                createdBy = s.CREATEDBY,
                updatedDate = s.UPDATEDDATE,
                updatedBy = s.UPDATEDBY,
                project = new projectObject
                {
                    projectNo = s.PROJECT.PROJECTNO,
                    projectDescription = s.PROJECT.PROJECTDESCRIPTION,
                    projectManagerName = s.PROJECT.PROJECTMANAGERNAME,
                    projectType = s.PROJECT.PROJECTTYPE,
                }
            });
        }

        [HttpPut]
        [ActionName("UpdateSubmitRevisi")]
        public projectBudgetApprovalObject UpdateSubmitRevisi(int param, projectBudgetApprovalObject resRequest)
        {
            PROJECTBUDGETAPPROVAL s = db.PROJECTBUDGETAPPROVALs.Find(param);
            s.BUDGETAMOUNT = resRequest.budgetAmount;
            s.UPDATEDBY = GetCurrentNTUserId();
            s.UPDATEDDATE = DateTime.Now;

            db.Entry(s).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                //// call K2
                //K2ManagementController k2Management = new K2ManagementController();
                //k2Management.Connect();

                //K2ConnectController k2Connect = new K2ConnectController();

                //try
                //{
                //    string nextApprover = getNextK2Approver("");

                //    K2ConnectController connectK2 = new K2ConnectController();

                //    List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                //    EWRRequestDTO reqData = new EWRRequestDTO();
                //    reqData.WF_ID = s.WFID; //set wf id nya

                //    k2Object.Add(reqData);
                //    connectK2.populateFlightWorkListItems(k2Object);

                //    string sn = reqData.K2WorklistItems.
                //                Where(d => d.procId == s.WFID &&
                //                d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                //                Select(d => d.serialNo).FirstOrDefault();

                //    k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REVISE_K2, reqData.Remark, null);
                //}
                //catch (DbEntityValidationException exc)
                //{
                //    var errorMessage = "";
                //    foreach (var error in exc.EntityValidationErrors)
                //    {
                //        foreach (var validationError in error.ValidationErrors)
                //            errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                //    }
                //    WriteLog(errorMessage);
                //    throw new ApplicationException(errorMessage);

                //}
                //finally
                //{
                //    k2Management.Close();
                //    k2Connect.Disconnect();
                //}
                return resRequest;
            }
            catch (Exception ex)
            {
                if (!PROJECTBUDGETAPPROVALExists(resRequest.id))
                {
                    WriteLog(ex.ToString());
                    return null;
                }
                else
                {
                    throw;
                }
            }

        }

        [HttpPost]
        [ActionName("ApproveRequest")]
        public IHttpActionResult ApproveRequest(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                PROJECTBUDGETAPPROVAL resRequest = db.PROJECTBUDGETAPPROVALs.Find(param);
                if (resRequest != null)
                {
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WFID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WFID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_APPROVE_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }

        [HttpPost]
        [ActionName("RejectRequest")]
        public IHttpActionResult RejectRequest(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                PROJECTBUDGETAPPROVAL resRequest = db.PROJECTBUDGETAPPROVALs.Find(param);
                if (resRequest != null)
                {
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WFID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WFID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REJECT_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }


        [HttpPost]
        [ActionName("RequestNeedRevised")]
        public IHttpActionResult RequestNeedRevised(int param, [FromBody]CommonDTO resModel)
        {
            try
            {
                PROJECTBUDGETAPPROVAL resRequest = db.PROJECTBUDGETAPPROVALs.Find(param);
                if (resRequest != null)
                {
                    // call K2
                    K2ManagementController k2Management = new K2ManagementController();
                    k2Management.Connect();

                    K2ConnectController k2Connect = new K2ConnectController();

                    try
                    {
                        string nextApprover = getNextK2Approver("");

                        List<EWRRequestDTO> k2Object = new List<EWRRequestDTO>();
                        EWRRequestDTO reqData = new EWRRequestDTO();
                        reqData.WF_ID = resRequest.WFID; //set wf id nya
                        k2Object.Add(reqData);

                        k2Connect.populateFlightWorkListItems(k2Object);

                        string sn = reqData.K2WorklistItems.
                                    Where(d => d.procId == resRequest.WFID &&
                                    d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                                    Select(d => d.serialNo).FirstOrDefault();

                        //string sn = k2WorklistItem.
                        //            Where(d => d.procId == resRequest.WF_ID &&
                        //            d.nextApproverNtAccount.ToLower().Contains(nextApprover)).
                        //            Select(d => d.serialNo).FirstOrDefault();

                        k2Connect.ExecuteWorklistAction(sn, constants.STATUS_REVISE_K2, resModel.Remark, null);

                    }
                    catch (DbEntityValidationException exc)
                    {
                        var errorMessage = "";
                        foreach (var error in exc.EntityValidationErrors)
                        {
                            foreach (var validationError in error.ValidationErrors)
                                errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                        }
                        WriteLog(errorMessage);
                        throw new ApplicationException(errorMessage);

                    }
                    finally
                    {
                        k2Management.Close();
                        k2Connect.Disconnect();
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }

            return NotFound();

        }


        [HttpPut]
        [ActionName("updateProjectBudgetApproval")]
        public IHttpActionResult updateProjectBudgetApproval(int param, projectBudgetApprovalObject pROJECTBUDGETAPPROVALParam)
        {
          if (!ModelState.IsValid)
           {
                return BadRequest();
            }

            if (param != pROJECTBUDGETAPPROVALParam.id)
            {
               return BadRequest();
            }

            try
           {
                PROJECTBUDGETAPPROVAL update = db.PROJECTBUDGETAPPROVALs.Find(param);
                update.OID = pROJECTBUDGETAPPROVALParam.id;
                update.PROJECTID = pROJECTBUDGETAPPROVALParam.projectId;
                update.BUDGETAMOUNT = pROJECTBUDGETAPPROVALParam.budgetAmount;
               update.BUDGETSTATUS = pROJECTBUDGETAPPROVALParam.budgetStatus;
                update.FILENAME = pROJECTBUDGETAPPROVALParam.fileName;
                update.UPDATEDDATE = DateTime.Now;
              update.UPDATEDBY = GetCurrentNTUserId();

                db.Entry(update).State = EntityState.Modified;
                db.SaveChanges();

           }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            return Ok(pROJECTBUDGETAPPROVALParam);
        }

        [HttpPost]
        [ActionName("addProjectBudgetApproval")]
        public IHttpActionResult addProjectBudgetApproval(string param, projectBudgetApprovalObject projectBudgetApprovalParam)
        {

            PROJECTBUDGETAPPROVAL add = new PROJECTBUDGETAPPROVAL();
            add.OID = projectBudgetApprovalParam.id;
            add.PROJECTID = projectBudgetApprovalParam.projectId;
            add.BUDGETAMOUNT = projectBudgetApprovalParam.budgetAmount;
            add.BUDGETSTATUS = projectBudgetApprovalParam.budgetStatus;
            add.FILENAME = projectBudgetApprovalParam.fileName;
            add.SUBMITTEDBY = GetCurrentNTUserId();
            add.SUBMITTEDDATE = DateTime.Now;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();
            db.PROJECTBUDGETAPPROVALs.Add(add);
            db.SaveChanges();

            // call K2
            K2ConnectController k2 = new K2ConnectController();
            try
            {
                // call K2
                string k2ConnectCode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ProjectBudgetConnectCode;
                ProcessInstance edfProcessInstance = k2.Connect(k2ConnectCode);

                string k2AppID = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2AppID;
                edfProcessInstance.DataFields[k2AppID].Value = add.OID;

                k2.StartProcessInstance(edfProcessInstance);
            }
            catch (DbEntityValidationException exc)
            {
                var errorMessage = "";
                foreach (var error in exc.EntityValidationErrors)
                {
                    foreach (var validationError in error.ValidationErrors)
                        errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                }
                WriteLog("K2 Problem:");
                WriteLog(errorMessage);
                throw new ApplicationException(errorMessage);

            }
            catch (Exception ex)
            {
                WriteLog("K2 Problem:");
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
            }
            finally
            {
                k2.Disconnect();
            }
            // --call K2


            projectBudgetApprovalParam.id = add.OID;
            return Ok(projectBudgetApprovalParam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTBUDGETAPPROVALExists(int id)
        {
            return db.PROJECTBUDGETAPPROVALs.Count(e => e.OID == id) > 0;
        }
    }
}