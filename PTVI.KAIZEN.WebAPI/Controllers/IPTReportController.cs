﻿using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Services.Protocols;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class IPTReportController : KAIZENController
    {
        [HttpGet]
        [ActionName("ReportData")]
        public object ReportData(string reportName, int projectShiftID, string taskUid)
        {

            if (reportName == constants.IPT_PROGRESS_TODATE_SUMMARY)
                return ToDateProgressSummaryData(projectShiftID, taskUid);
            else if (reportName == constants.IPT_PROGRESS_UPDOWN)
                return UpDownProgressData(projectShiftID, taskUid);
            else if (reportName == constants.IPT_FORECAST_BASELINEVSACTUAL)
                return ForecastActualVsBaselineData(projectShiftID, taskUid);
            else if (reportName == constants.IPT_FORECAST_OPTIMISVSPROPORSIONAL)
                return ForecastOptimistVsProporsionalData(projectShiftID, taskUid);
            else if (reportName == constants.IPT_SCURVE_ACTUAL_AND_FORECAST)
                return ScurveActualAndForecastData(projectShiftID, taskUid);

            return null;
        }

        public object ScurveActualAndForecastData(int projectShiftID, string taskUid)
        {
            VALE_KAIZENEntities db = new VALE_KAIZENEntities();

            try
            {

                var result = db.SCURVEs.Where(x => x.ESTIMATIIONSHIFTID == projectShiftID &&
                                x.TASKUID == taskUid).Select(x => new scurveModel
                                {
                                    taskUid = x.TASKUID,
                                    taskName = x.TASKNAME,
                                    actualPctComplete = x.PERCENTCOMPLETEACTUAL,
                                    baselinePctComplete =  x.PERCENTCOMPLETEBASELINE,
                                    originalPctComplete = x.PERCENTCOMPLETEORIGINAL,
                                    optimisticPctComplete = x.PERCENTCOMPLETEOPTIMIS,
                                    realisticPctComplete = x.PERCENTCOMPLETEREALISTIC,
                                    shiftno = x.SHIFTNO
                                }).OrderBy(x=>x.shiftno).ToList();

                var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == taskUid);



                if (tasks.Count() > 0)
                {
                    var task = tasks.FirstOrDefault();

                    // modify percentcomplete baseline for only project 52,53, 55 -> there is mistake on %baseline calculation previous date 22 Oct 2017 20:00
                    var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
                    if (projectShift.PROJECTID == 52 || projectShift.PROJECTID == 53 || projectShift.PROJECTID == 55)
                    {
                        var prev = result[0];
                        var i = 1;
                        while (i < result.Count)
                        {
                            if (prev.baselinePctComplete > result[i].baselinePctComplete)
                                result[i].baselinePctComplete = prev.baselinePctComplete;

                            if (result[i].shiftno == projectShift.SHIFTID)
                                task.PERCENTCOMPLETEBASELINE = result[i].baselinePctComplete;

                            prev = result[i];
                            i++;
                        }
                    }

                    //var originalDiff = task.FINISHDATE.GetValueOrDefault().Subtract(task.ORIGINALFINISH.GetValueOrDefault()).TotalHours;
                    //var optimisDiff = task.FINISHDATE.GetValueOrDefault().Subtract(task.OPTIMISTICFINISH.GetValueOrDefault()).TotalHours;
                    //var realisDiff = task.FINISHDATE.GetValueOrDefault().Subtract(task.REALISTICFINISH.GetValueOrDefault()).TotalHours;
                    var originalDiff = task.BASELINEFINISH.GetValueOrDefault().Subtract(task.ORIGINALFINISH.GetValueOrDefault()).TotalHours;

                    var optimisDiff = task.PERCENTCOMPLETEACTUALAFTERSHIFT >= 100 ?
                        (task.ACTUALFINISH.HasValue ? task.BASELINEFINISH.GetValueOrDefault().Subtract(task.ACTUALFINISH.GetValueOrDefault()).TotalHours : 0) :
                        (task.OPTIMISTICFINISH.HasValue ? task.BASELINEFINISH.GetValueOrDefault().Subtract(task.OPTIMISTICFINISH.GetValueOrDefault()).TotalHours: 0);

                    var realisDiff = task.PERCENTCOMPLETEACTUALAFTERSHIFT >= 100 ?
                        (task.ACTUALFINISH.HasValue ? task.BASELINEFINISH.GetValueOrDefault().Subtract(task.ACTUALFINISH.GetValueOrDefault()).TotalHours : 0) : 
                        (task.REALISTICFINISH.HasValue ? task.BASELINEFINISH.GetValueOrDefault().Subtract(task.REALISTICFINISH.GetValueOrDefault()).TotalHours: 0);
                    return new
                    {
                        shiftNo = db.PROJECTSHIFTs.Where(x=>x.OID == task.ESTIMATIONSHIFTID).FirstOrDefault().SHIFTID,
                        taskName = task == null ? taskUid : task.TASKNAME,
                        baseDate = task.BASELINEFINISH,
                        originalDate = task.ORIGINALFINISH,
                        optimisDate = task.PERCENTCOMPLETEACTUALAFTERSHIFT >= 100 ? task.ACTUALFINISH : task.OPTIMISTICFINISH,
                        realisDate = task.PERCENTCOMPLETEACTUALAFTERSHIFT >= 100 ? task.ACTUALFINISH : task.REALISTICFINISH,
                        originalDiff = originalDiff,
                        optimisDiff = optimisDiff,
                        realisDiff = realisDiff,
                        basePct = task.PERCENTCOMPLETEBASELINE,
                        actualPct = task.PERCENTCOMPLETEACTUALAFTERSHIFT,
                        list = result
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public object ToDateProgressSummaryData(int projectShiftID, string taskUid)
        {
            iPROMEntities db = new iPROMEntities();
            var results = new List<object>();
            try
            {
                var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
                if (projectShift == null)
                    throw new ApplicationException("Project shift not found for id " + projectShiftID);

                // read project data
                var credential = pwaCredential;
                PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
                ProjectWS.Credentials = credential;

                PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(new Guid(projectShift.PROJECT.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

                var contractorCFID = new Guid(cfExecutor1Uid);

                // get its data
                var itSelfRows = projectDs.Task.Select("TASK_UID = '" + taskUid + "'") as PSI_ProjectWS.ProjectDataSet.TaskRow[];

                var allPreviousShifts = projectShift.PROJECT.PROJECTSHIFTs.Where(x => x.SHIFTID <= projectShift.SHIFTID).OrderBy(x => x.SHIFTID).ToList();

                // get all childs
                var childs = projectDs.Task.Select("TASK_PARENT_UID = '" + taskUid + "'", "TASK_ID") as PSI_ProjectWS.ProjectDataSet.TaskRow[];
                if (childs.Length > 0)
                {
                    foreach (var child in childs)
                    {
                        var uid = child.TASK_UID.ToString();
                        //var localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        PROJECTTASKBYSHIFT localTask = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        // get baseline info 
                        var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Find(projectShiftID, child.TASK_UID.ToString());

                        var currentPctComplete = localTask == null ? 0 : (localTask.ESTIMATIONSHIFTID == projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : (child.IsTASK_PCT_COMPNull() ? 0 : child.TASK_PCT_COMP));
                        var baselinePctComplete = masterTaskBaseline == null ? (localTask == null ? 0 : localTask.PERCENTCOMPLETEBASELINE) : masterTaskBaseline.PERCENTCOMPLETEBASELINE;
                        var totalSlack = masterTaskBaseline != null ? masterTaskBaseline.TOTALSLACK : (localTask == null || !localTask.TOTALSLACK.HasValue ? (child.IsTASK_TOTAL_SLACKNull() ? 0 : child.TASK_TOTAL_SLACK) : localTask.TOTALSLACK);
                        var freeSlack = masterTaskBaseline != null ? masterTaskBaseline.FREESLACK : (localTask == null || !localTask.FREESLACK.HasValue ? (child.IsTASK_FREE_SLACKNull() ? 0 : child.TASK_FREE_SLACK) : localTask.FREESLACK);
                        var optimisDiff = 0.0;
                        if (masterTaskBaseline != null)
                            optimisDiff = child.TB_FINISH.Subtract(localTask != null ? localTask.OPTIMISTICFINISH.GetValueOrDefault() : masterTaskBaseline.OPTIMISTICFINISH.GetValueOrDefault()).TotalHours;

                        results.Add(new
                        {
                            taskUid = child.TASK_UID.ToString(),
                            taskName = child.TASK_NAME,
                            actualPctComplete = currentPctComplete,
                            baselinePctComplete = baselinePctComplete,
                            aheadeOrDelay = currentPctComplete - baselinePctComplete,
                            contractor = contractor,
                            duration = localTask != null ? localTask.ESTDURATION : masterTaskBaseline.ESTIMATEDURATION,
                            totalSlack = totalSlack,
                            freeSlack = freeSlack,
                            optimisDiff = optimisDiff
                        });
                    }
                }
                else
                {
                    if (itSelfRows.Length > 0)
                    {
                        var itSelf = itSelfRows[0];
                        var uid = itSelf.TASK_UID.ToString();

                        PROJECTTASKBYSHIFT localTask = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        // get baseline info 
                        var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Find(projectShiftID, itSelf.TASK_UID.ToString());

                        var currentPctComplete = localTask == null ? 0 : (localTask.ESTIMATIONSHIFTID == projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : (itSelf.IsTASK_PCT_COMPNull() ? 0 : itSelf.TASK_PCT_COMP));
                        var baselinePctComplete = masterTaskBaseline == null ? (localTask == null ? 0 : localTask.PERCENTCOMPLETEBASELINE) : masterTaskBaseline.PERCENTCOMPLETEBASELINE;
                        var totalSlack = masterTaskBaseline != null ? masterTaskBaseline.TOTALSLACK : (localTask == null || !localTask.TOTALSLACK.HasValue ? (itSelf.IsTASK_TOTAL_SLACKNull() ? 0 : itSelf.TASK_TOTAL_SLACK) : localTask.TOTALSLACK);
                        var freeSlack = masterTaskBaseline != null ? masterTaskBaseline.FREESLACK : (localTask == null || !localTask.FREESLACK.HasValue ? (itSelf.IsTASK_FREE_SLACKNull() ? 0 : itSelf.TASK_FREE_SLACK) : localTask.FREESLACK);
                        var optimisDiff = 0.0;
                        if (masterTaskBaseline != null)
                            optimisDiff = itSelf.TB_FINISH.Subtract(localTask != null ? localTask.OPTIMISTICFINISH.GetValueOrDefault() : masterTaskBaseline.OPTIMISTICFINISH.GetValueOrDefault()).TotalHours;

                        results.Add(new
                        {
                            taskUid = itSelf.TASK_UID.ToString(),
                            taskName = itSelf.TASK_NAME,
                            actualPctComplete = currentPctComplete,
                            baselinePctComplete = baselinePctComplete,
                            aheadeOrDelay = currentPctComplete - baselinePctComplete,
                            contractor = contractor,
                            duration = localTask != null ? localTask.ESTDURATION : masterTaskBaseline.ESTIMATEDURATION,
                            totalSlack = totalSlack,
                            freeSlack = freeSlack,
                            optimisDiff = optimisDiff
                        });

                    }
                }


                var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == taskUid);

                if (tasks.Count() > 0)
                {
                    var task = tasks.FirstOrDefault();
                    return new
                    {
                        taskName = task.TASKNAME,
                        position = projectShift == null ? DateTime.Now : projectShift.ENDDATE,
                        list = results
                    };
                }
                else if (itSelfRows.Length > 0)
                {
                    var task = itSelfRows[0];
                    return new
                    {
                        taskName = task.TASK_NAME,
                        position = projectShift == null ? DateTime.Now : projectShift.ENDDATE,
                        list = results
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public object UpDownProgressData(int projectShiftID, string taskUid)
        {
            iPROMEntities db = new iPROMEntities();
            var results = new List<object>();
            try
            {
                var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
                if (projectShift == null)
                    throw new ApplicationException("Project shift not found for id " + projectShiftID);

                // read project data
                var credential = pwaCredential;
                PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
                ProjectWS.Credentials = credential;

                PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(new Guid(projectShift.PROJECT.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

                var contractorCFID = new Guid(cfExecutor1Uid);

                // get its data 
                var itSelfRows = projectDs.Task.Select("TASK_UID = '" + taskUid + "'") as PSI_ProjectWS.ProjectDataSet.TaskRow[];                

                var allPreviousShifts = projectShift.PROJECT.PROJECTSHIFTs.Where(x=>x.PROJECTID == projectShift.PROJECTID && x.SHIFTID <= projectShift.SHIFTID).OrderBy(x=> x.SHIFTID).ToList();

                // get all childs
                var childs = projectDs.Task.Select("TASK_PARENT_UID = '" + taskUid + "'", "TASK_ID") as PSI_ProjectWS.ProjectDataSet.TaskRow[];
                if (childs.Length > 0)
                {
                    foreach (var child in childs)
                    {
                        var uid = child.TASK_UID.ToString();

                        PROJECTTASKBYSHIFT localTask  = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        var currentPctComplete = localTask == null ? 0 : (localTask.ESTIMATIONSHIFTID == projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : (child.IsTASK_PCT_COMPNull() ? 0 : child.TASK_PCT_COMP));
                        //var currentPctComplete = localTask == null ? masterTaskBaseline.PERCENTCOMPLETEBASELINE : (localTask.ESTIMATIONSHIFTID == projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : (child.IsTASK_PCT_COMPNull() ? 0 : child.TASK_PCT_COMP));
                        var optimisDiff = 0.0;
                        if (masterTaskBaseline != null)
                            optimisDiff = child.TB_FINISH.Subtract(localTask != null ? localTask.OPTIMISTICFINISH.GetValueOrDefault() : masterTaskBaseline.OPTIMISTICFINISH.GetValueOrDefault()).TotalHours;

                        results.Add(new
                        {
                            taskUid = child.TASK_UID.ToString(),
                            taskName = child.TASK_NAME,
                            currentPctComplete = currentPctComplete,
                            previousPctComplete = localTask != null ? (localTask.ESTIMATIONSHIFTID != projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : localTask.PERCENTCOMPLETEACTUALBEFORESHIFT) : 0,
                            upDown = localTask != null ? (currentPctComplete - (localTask.ESTIMATIONSHIFTID != projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : localTask.PERCENTCOMPLETEACTUALBEFORESHIFT)) : 0,
                            contractor = contractor,
                            duration = localTask != null ? localTask.ESTDURATION : (child.IsTASK_DURNull() ? 0 : child.TASK_DUR),
                            totalSlack = (masterTaskBaseline != null ? masterTaskBaseline.TOTALSLACK : (localTask != null && localTask.TOTALSLACK.HasValue ? localTask.TOTALSLACK : (child.IsTASK_TOTAL_SLACKNull() ? 0 : child.TASK_TOTAL_SLACK))),
                            freeSlack = (masterTaskBaseline != null ? masterTaskBaseline.FREESLACK : (localTask != null && localTask.FREESLACK.HasValue ? localTask.FREESLACK : (child.IsTASK_FREE_SLACKNull() ? 0 : child.TASK_FREE_SLACK))),
                            optimisDiff = optimisDiff
                        });
                    }
                }
                else
                {
                    if (itSelfRows.Length > 0)
                    {
                        var itSelf = itSelfRows[0];
                        var uid = itSelf.TASK_UID.ToString();

                        PROJECTTASKBYSHIFT localTask  = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        var currentPctComplete = localTask == null ? 0 : (localTask.ESTIMATIONSHIFTID == projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : (itSelf.IsTASK_PCT_COMPNull() ? 0 : itSelf.TASK_PCT_COMP));
                        var optimisDiff = 0.0;
                        if (masterTaskBaseline != null)
                            optimisDiff = itSelf.TB_FINISH.Subtract(localTask != null ? localTask.OPTIMISTICFINISH.GetValueOrDefault() : masterTaskBaseline.OPTIMISTICFINISH.GetValueOrDefault()).TotalHours;

                        results.Add(new
                        {
                            taskUid = itSelf.TASK_UID.ToString(),
                            taskName = itSelf.TASK_NAME,
                            currentPctComplete = currentPctComplete,
                            previousPctComplete = localTask != null ? (localTask.ESTIMATIONSHIFTID != projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : localTask.PERCENTCOMPLETEACTUALBEFORESHIFT) : 0,
                            upDown = localTask != null ? (currentPctComplete - (localTask.ESTIMATIONSHIFTID != projectShift.OID ? localTask.PERCENTCOMPLETEACTUALAFTERSHIFT : localTask.PERCENTCOMPLETEACTUALBEFORESHIFT)) : 0,
                            contractor = contractor,
                            duration = localTask != null ? localTask.ESTDURATION : (itSelf.IsTASK_DURNull() ? 0 : itSelf.TASK_DUR),
                            totalSlack = masterTaskBaseline != null ? masterTaskBaseline.TOTALSLACK : (localTask != null && localTask.TOTALSLACK.HasValue ? localTask.TOTALSLACK : (itSelf.IsTASK_TOTAL_SLACKNull() ? 0 : itSelf.TASK_TOTAL_SLACK)),
                            freeSlack = masterTaskBaseline != null ? masterTaskBaseline.FREESLACK : (localTask != null && localTask.FREESLACK.HasValue ? localTask.FREESLACK : (itSelf.IsTASK_FREE_SLACKNull() ? 0 : itSelf.TASK_FREE_SLACK)),
                            optimisDiff = optimisDiff
                        });

                    }
                }


                var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == taskUid);

                if (tasks.Count() > 0)
                {
                    var task = tasks.FirstOrDefault();
                    return new
                    {
                        taskName = task.TASKNAME,
                        position = projectShift == null ? DateTime.Now : projectShift.ENDDATE,
                        list = results
                    };
                }
                else if (itSelfRows.Length > 0)
                {
                    var task = itSelfRows[0];
                    return new
                    {
                        taskName = task.TASK_NAME,
                        position = projectShift == null ? DateTime.Now : projectShift.ENDDATE,
                        list = results
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public object ForecastActualVsBaselineData(int projectShiftID, string taskUid)
        {
            iPROMEntities db = new iPROMEntities();
            var results = new List<object>();
            try
            {
                var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
                if (projectShift == null)
                    throw new ApplicationException("Project shift not found for id " + projectShiftID);

                // read project data
                var credential = pwaCredential;
                PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
                ProjectWS.Credentials = credential;

                PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(new Guid(projectShift.PROJECT.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

                var contractorCFID = new Guid(cfExecutor1Uid);

                // get its data
                var itSelfRows = projectDs.Task.Select("TASK_UID = '" + taskUid + "'") as PSI_ProjectWS.ProjectDataSet.TaskRow[];

                var allPreviousShifts = projectShift.PROJECT.PROJECTSHIFTs.Where(x => x.SHIFTID <= projectShift.SHIFTID).OrderBy(x => x.SHIFTID).ToList();

                // get all childs
                var childs = projectDs.Task.Select("TASK_PARENT_UID = '" + taskUid + "'", "TASK_ID") as PSI_ProjectWS.ProjectDataSet.TaskRow[];
                if (childs.Length > 0)
                {
                    foreach (var child in childs)
                    {
                        var uid = child.TASK_UID.ToString();
                        //var localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        PROJECTTASKBYSHIFT localTask = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        //var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Where(x=>x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        var actualFinish = localTask != null ? localTask.FINISHDATE : child.TASK_FINISH_DATE;
                        var actualDuration = localTask != null ? localTask.ACTUALDURATION : child.TASK_DUR;

                        results.Add(new
                        {
                            taskUid = child.TASK_UID,
                            taskName = child.TASK_NAME + " (" + contractor + ")",
                            baselineStart = localTask != null ? localTask.BASELINESTART : child.TB_START,
                            estimatedStart = localTask != null ? localTask.STARTDATE : child.TASK_START_DATE,
                            actualStart = localTask != null ? localTask.ACTUALSTART : (child.IsTASK_ACT_STARTNull() ? null : new DateTime?(child.TASK_ACT_START)),
                            baselineFinish = localTask != null ? localTask.BASELINEFINISH : child.TB_FINISH,
                            estimatedFinish = localTask != null ? localTask.FINISHDATE : child.TASK_FINISH_DATE,
                            actualFinish = localTask != null ? localTask.ACTUALFINISH : (child.IsTASK_ACT_FINISHNull() ? null : new DateTime?(child.TASK_ACT_FINISH)),
                            baselineDuration = localTask != null ? localTask.BASELINEDURATION : child.TB_DUR,
                            estimatedDuration = localTask != null ? localTask.ESTDURATION : child.TASK_DUR,
                            actualDuration = localTask != null ? localTask.ACTUALDURATION : child.TASK_ACT_DUR,
                            timeVar = child.TB_FINISH == actualFinish ? "On Schedule" :
                                            (child.TB_FINISH > actualFinish ? "Ahead" : "Delay"),
                            durationVar = child.TB_DUR == actualDuration ? "On Duration" :
                                            (child.TB_DUR > actualDuration ? "Ahead" : "Delay"),
                            contractor = contractor
                        });
                    }
                }
                else
                {
                    if (itSelfRows.Length > 0)
                    {
                        var itSelf = itSelfRows[0];
                        var uid = itSelf.TASK_UID.ToString();

                        PROJECTTASKBYSHIFT localTask = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        var actualFinish = localTask != null ? localTask.ACTUALFINISH : (itSelf.IsTASK_ACT_FINISHNull() ? itSelf.TASK_FINISH_DATE : itSelf.TASK_ACT_FINISH);
                        var actualDuration = localTask != null ? localTask.ACTUALDURATION : (itSelf.IsTASK_ACT_DURNull() ? itSelf.TASK_DUR : itSelf.TASK_ACT_DUR);

                        results.Add(new
                        {
                            taskUid = itSelf.TASK_UID,
                            taskName = itSelf.TASK_NAME + " (" + contractor + ")",
                            baselineStart = localTask != null ? localTask.BASELINESTART : itSelf.TB_START,
                            estimatedStart = localTask != null ? localTask.STARTDATE : itSelf.TASK_START_DATE,
                            actualStart = localTask != null ? localTask.ACTUALSTART : (itSelf.IsTASK_ACT_STARTNull() ? null : new DateTime?(itSelf.TASK_ACT_START)),
                            baselineFinish = localTask != null ? localTask.BASELINEFINISH : itSelf.TB_FINISH,
                            estimatedFinish = localTask != null ? localTask.FINISHDATE : itSelf.TASK_FINISH_DATE,
                            actualFinish = localTask != null ? localTask.ACTUALFINISH :  (itSelf.IsTASK_ACT_FINISHNull() ? null : new DateTime?(itSelf.TASK_ACT_FINISH)),
                            baselineDuration = localTask != null ? localTask.BASELINEDURATION : itSelf.TB_DUR,
                            estimatedDuration = localTask != null ? localTask.ESTDURATION : itSelf.TASK_DUR,
                            actualDuration = localTask != null ? localTask.ACTUALDURATION : itSelf.TASK_ACT_DUR,
                            timeVar = itSelf.TB_FINISH == actualFinish ? "On Schedule" :
                                            (itSelf.TB_FINISH > actualFinish ? "Ahead" : "Delay"),
                            durationVar = itSelf.TB_DUR == actualDuration ? "On Duration" :
                                            (itSelf.TB_DUR > actualDuration ? "Ahead" : "Delay"),
                            contractor = contractor
                        });

                    }
                }

                var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == taskUid);

                if (tasks.Count() > 0)
                {
                    var task = tasks.FirstOrDefault();

                    return new
                    {
                        taskName = task.TASKNAME,
                        position = projectShift == null ? DateTime.Now : projectShift.ENDDATE,
                        list = results,
                        baselineFinish = task.BASELINEFINISH,
                        estimatedFinish = task.FINISHDATE,
                        timeVar = task.BASELINEFINISH == task.FINISHDATE ? "On Schedule" :
                                                    (task.BASELINEFINISH > task.FINISHDATE ? "Ahead" : "Delay"),
                        durationVar = task.BASELINEDURATION == task.ESTDURATION ? "On Duration" :
                                (task.BASELINEDURATION > task.ESTDURATION ? "Ahead" : "Delay"),
                    };
                }
                else
                {
                    return null;
                }
                
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public object ForecastOptimistVsProporsionalData(int projectShiftID, string taskUid)
        {
            iPROMEntities db = new iPROMEntities();
            var results = new List<object>();

            try
            {
                var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
                if (projectShift == null)
                    throw new ApplicationException("Project shift not found for id " + projectShiftID);

                // read project data
                var credential = pwaCredential;
                PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
                ProjectWS.Credentials = credential;

                PSI_ProjectWS.ProjectDataSet projectDs =
                            ProjectWS.ReadProject(new Guid(projectShift.PROJECT.IPTPROJECTID), PSI_ProjectWS.DataStoreEnum.PublishedStore);

                var contractorCFID = new Guid(cfExecutor1Uid);

                // get its data
                var itSelfRows = projectDs.Task.Select("TASK_UID = '" + taskUid + "'") as PSI_ProjectWS.ProjectDataSet.TaskRow[];

                var allPreviousShifts = projectShift.PROJECT.PROJECTSHIFTs.Where(x => x.SHIFTID <= projectShift.SHIFTID).OrderBy(x => x.SHIFTID).ToList();

                // get all childs
                var childs = projectDs.Task.Select("TASK_PARENT_UID = '" + taskUid + "'", "TASK_ID") as PSI_ProjectWS.ProjectDataSet.TaskRow[];
                if (childs.Length > 0)
                {
                    foreach (var child in childs)
                    {
                        var uid = child.TASK_UID.ToString();
                        PROJECTTASKBYSHIFT localTask = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        results.Add(new
                        {
                            taskUid = child.TASK_UID,
                            taskName = child.TASK_NAME,
                            baselineStart = localTask != null ? localTask.BASELINESTART : child.TB_START,
                            estimatedStart = localTask != null ? localTask.STARTDATE : child.TASK_START_DATE,
                            baselineFinish = localTask != null ? localTask.BASELINEFINISH : child.TB_FINISH,
                            optimisticFinish = masterTaskBaseline != null ? masterTaskBaseline.OPTIMISTICFINISH : (localTask == null ?  child.TASK_FINISH_DATE : localTask.OPTIMISTICFINISH),
                            realisticFinish = masterTaskBaseline != null ? masterTaskBaseline.REALISTICFINISH : (localTask == null ? child.TASK_FINISH_DATE : localTask.REALISTICFINISH),
                            baselineDuration = localTask != null ? localTask.BASELINEDURATION : child.TB_DUR,
                            optimisticDuration = masterTaskBaseline != null ? masterTaskBaseline.OPTIMISTICDURATION : (localTask == null ? child.TASK_DUR : localTask.OPTIMISTICDURATION),
                            realisticDuration = masterTaskBaseline != null ? masterTaskBaseline.REALISTICDURATION : (localTask == null ? child.TASK_DUR : localTask.REALISTICDURATION),
                            optimisticTimeVar = masterTaskBaseline != null ? 
                                            (child.TB_FINISH == masterTaskBaseline.OPTIMISTICFINISH ? "On Schedule" :
                                                (child.TB_FINISH > masterTaskBaseline.OPTIMISTICFINISH ? "Ahead" : "Delay")) : 
                                            (localTask == null ?
                                                (child.TB_FINISH == child.TASK_FINISH_DATE ? "On Schedule" :
                                                    (child.TB_FINISH > child.TASK_FINISH_DATE ? "Ahead" : "Delay")) : 
                                                (localTask.BASELINEFINISH == localTask.OPTIMISTICFINISH ? "On Schedule" :
                                                    (localTask.BASELINEFINISH > localTask.OPTIMISTICFINISH ? "Ahead" : "Delay"))),
                            optimisticDurationVar = masterTaskBaseline != null ?
                                            (child.TB_DUR == masterTaskBaseline.OPTIMISTICDURATION ? "On Schedule" :
                                                (child.TB_DUR > masterTaskBaseline.OPTIMISTICDURATION ? "Ahead" : "Delay")) :
                                            (localTask == null ?
                                                (child.TB_DUR == child.TASK_DUR ? "On Duration" :
                                                    (child.TB_DUR > child.TASK_DUR ? "Ahead" : "Delay")) : 
                                                (localTask.BASELINEDURATION == localTask.OPTIMISTICDURATION ? "On Duration" :
                                                    (localTask.BASELINEDURATION > localTask.OPTIMISTICDURATION ? "Ahead" : "Delay"))),
                            realisticTimeVar = masterTaskBaseline != null ? 
                                            (child.TB_FINISH == masterTaskBaseline.REALISTICFINISH ? "On Schedule" : 
                                                (child.TB_FINISH > masterTaskBaseline.REALISTICFINISH ? "Ahead" : "Delay")) : 
                                            (localTask == null ?
                                                (child.TB_FINISH == child.TASK_FINISH_DATE ? "On Schedule" :
                                                    (child.TB_FINISH > child.TASK_FINISH_DATE ? "Ahead" : "Delay")) : 
                                                (localTask.BASELINEFINISH == localTask.REALISTICFINISH ? "On Schedule" :
                                                    (localTask.BASELINEFINISH > localTask.REALISTICFINISH ? "Ahead" : "Delay"))),
                            realisticDurationVar = masterTaskBaseline != null ?
                                            (child.TB_DUR == masterTaskBaseline.REALISTICDURATION ? "On Schedule" : 
                                                (child.TB_DUR > masterTaskBaseline.REALISTICDURATION ? "Ahead" : "Delay")):
                                            (localTask == null ?
                                                (child.TB_DUR == child.TASK_DUR ? "On Duration" :
                                                    (child.TB_DUR > child.TASK_DUR ? "Ahead" : "Delay")) : 
                                                (localTask.BASELINEDURATION == localTask.REALISTICDURATION ? "On Duration" :
                                                    (localTask.BASELINEDURATION > localTask.REALISTICDURATION ? "Ahead" : "Delay"))),
                            contractor = contractor
                        });
                    }
                }
                else
                {
                    if (itSelfRows.Length > 0)
                    {
                        var itSelf = itSelfRows[0];
                        var uid = itSelf.TASK_UID.ToString();
                        PROJECTTASKBYSHIFT localTask = null;
                        for (var i = allPreviousShifts.Count - 1; i >= 0; i--)
                        {
                            var oid = allPreviousShifts[i].OID;
                            localTask = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == oid && x.TASKUID == uid).FirstOrDefault();

                            if (localTask != null)
                                break;
                        }

                        var contractor = "";
                        if (localTask != null)
                            contractor = localTask.CONTRACTOR;
                        else
                        {
                            var contractorCustomFieldRows = projectDs.TaskCustomFields.Select("TASK_UID = '" + uid + "' AND MD_PROP_UID = '" + contractorCFID + "'");
                            if (contractorCustomFieldRows.Length > 0)
                            {
                                contractor = (contractorCustomFieldRows[0] as PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow).TEXT_VALUE;
                            }
                        }

                        var masterTaskBaseline = db.MASTERTASKBASELINEBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == uid).FirstOrDefault();

                        results.Add(new
                        {
                            taskUid = itSelf.TASK_UID,
                            taskName = itSelf.TASK_NAME,
                            baselineStart = localTask == null ? itSelf.TB_START : localTask.BASELINESTART,
                            estimatedStart = localTask == null ? itSelf.TASK_START_DATE : localTask.STARTDATE,
                            baselineFinish = localTask == null ? itSelf.TB_FINISH : localTask.BASELINEFINISH,
                            optimisticFinish = masterTaskBaseline != null ? masterTaskBaseline.OPTIMISTICFINISH : (localTask == null ? itSelf.TASK_FINISH_DATE : localTask.OPTIMISTICFINISH),
                            realisticFinish = masterTaskBaseline != null ? masterTaskBaseline.REALISTICFINISH : (localTask == null ? itSelf.TASK_FINISH_DATE : localTask.REALISTICFINISH),
                            baselineDuration = localTask != null ? localTask.BASELINEDURATION : itSelf.TB_DUR,
                            optimisticDuration = masterTaskBaseline != null ? masterTaskBaseline.OPTIMISTICDURATION : (localTask == null ? itSelf.TASK_DUR : localTask.OPTIMISTICDURATION),
                            realisticDuration = masterTaskBaseline != null ? masterTaskBaseline.REALISTICDURATION : (localTask == null ? itSelf.TASK_DUR : localTask.REALISTICDURATION),
                            optimisticTimeVar = masterTaskBaseline != null ?
                                            (itSelf.TB_FINISH == masterTaskBaseline.OPTIMISTICFINISH ? "On Schedule" :
                                                (itSelf.TB_FINISH > masterTaskBaseline.OPTIMISTICFINISH ? "Ahead" : "Delay")) :
                                            (localTask == null ?
                                                (itSelf.TB_FINISH == itSelf.TASK_FINISH_DATE ? "On Schedule" :
                                                    (itSelf.TB_FINISH > itSelf.TASK_FINISH_DATE ? "Ahead" : "Delay")) :
                                                (localTask.BASELINEFINISH == localTask.OPTIMISTICFINISH ? "On Schedule" :
                                                    (localTask.BASELINEFINISH > localTask.OPTIMISTICFINISH ? "Ahead" : "Delay"))),
                            optimisticDurationVar = masterTaskBaseline != null ?
                                            (itSelf.TB_DUR == masterTaskBaseline.OPTIMISTICDURATION ? "On Schedule" :
                                                (itSelf.TB_DUR > masterTaskBaseline.OPTIMISTICDURATION ? "Ahead" : "Delay")) :
                                            (localTask == null ?
                                                (itSelf.TB_DUR == itSelf.TASK_DUR ? "On Duration" :
                                                    (itSelf.TB_DUR > itSelf.TASK_DUR ? "Ahead" : "Delay")) :
                                                (localTask.BASELINEDURATION == localTask.OPTIMISTICDURATION ? "On Duration" :
                                                    (localTask.BASELINEDURATION > localTask.OPTIMISTICDURATION ? "Ahead" : "Delay"))),
                            realisticTimeVar = masterTaskBaseline != null ?
                                            (itSelf.TB_FINISH == masterTaskBaseline.REALISTICFINISH ? "On Schedule" :
                                                (itSelf.TB_FINISH > masterTaskBaseline.REALISTICFINISH ? "Ahead" : "Delay")) :
                                            (localTask == null ?
                                                (itSelf.TB_FINISH == itSelf.TASK_FINISH_DATE ? "On Schedule" :
                                                    (itSelf.TB_FINISH > itSelf.TASK_FINISH_DATE ? "Ahead" : "Delay")) :
                                                (localTask.BASELINEFINISH == localTask.REALISTICFINISH ? "On Schedule" :
                                                    (localTask.BASELINEFINISH > localTask.REALISTICFINISH ? "Ahead" : "Delay"))),
                            realisticDurationVar = masterTaskBaseline != null ?
                                            (itSelf.TB_DUR == masterTaskBaseline.REALISTICDURATION ? "On Schedule" :
                                                (itSelf.TB_DUR > masterTaskBaseline.REALISTICDURATION ? "Ahead" : "Delay")) :
                                            (localTask == null ?
                                                (itSelf.TB_DUR == itSelf.TASK_DUR ? "On Duration" :
                                                    (itSelf.TB_DUR > itSelf.TASK_DUR ? "Ahead" : "Delay")) :
                                                (localTask.BASELINEDURATION == localTask.REALISTICDURATION ? "On Duration" :
                                                    (localTask.BASELINEDURATION > localTask.REALISTICDURATION ? "Ahead" : "Delay"))),

                            contractor = contractor
                        });
                    }
                }

                var tasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID && x.TASKUID == taskUid);

                if (tasks.Count() > 0)
                {
                    var task = tasks.FirstOrDefault();
                    return new
                    {
                        taskName = task.TASKNAME,
                        position = projectShift == null ? DateTime.Now : projectShift.ENDDATE,
                        list = results,
                        baselineFinish = task.BASELINEFINISH,
                        optimisticFinish = task.OPTIMISTICFINISH,
                        realisticFinish = task.REALISTICFINISH,
                        timeVar = task.BASELINEFINISH == task.OPTIMISTICFINISH ? "On Schedule" :
                                (task.BASELINEFINISH > task.OPTIMISTICFINISH ? "Ahead" : "Delay"),
                        durationVar = task.BASELINEDURATION == task.OPTIMISTICDURATION ? "On Duration" :
                                (task.BASELINEDURATION > task.OPTIMISTICDURATION ? "Ahead" : "Delay"),
                        proporsionalTimeVar = task.BASELINEFINISH == task.REALISTICFINISH ? "On Schedule" :
                            (task.BASELINEFINISH > task.REALISTICFINISH ? "Ahead" : "Delay"),
                        proporsionalDurationVar = task.BASELINEDURATION == task.REALISTICDURATION ? "On Duration" :
                                (task.BASELINEDURATION > task.REALISTICDURATION ? "Ahead" : "Delay")

                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

    }

    public class scurveModel
    {
        public string taskUid {get;set;}

        public string taskName {get;set;}
        public int? actualPctComplete {get;set;}
        public int? baselinePctComplete {get;set;}
        public double? originalPctComplete {get;set;}
        public double? optimisticPctComplete {get;set;}
        public double? realisticPctComplete {get;set;}
        public int? shiftno { get; set; }
    }
}