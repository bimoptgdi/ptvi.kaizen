﻿using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using SourceCode.Workflow.Client;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class K2ConnectController : KAIZENController
    {
        private Connection _k2Connection;

        public Connection K2Connection
        {
            get
            {
                return _k2Connection;
            }

            set
            {
                _k2Connection = value;
            }
        }

        public ProcessInstance Connect(string processName)
        {
            string _serverName = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ServerName;
            string _user = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserName;
            string _domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserDomain;
            string _password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserPassword;
            uint _port = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2Port;
            string _securityLabel = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2SecurityLabel;
            string _processName = processName; //"ESS\\RecLetter";

            SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
            connectionString.Authenticate = true;
            connectionString.Host = _serverName;
            connectionString.Integrated = true;
            connectionString.IsPrimaryLogin = true;
            connectionString.Port = _port;

            connectionString.UserID = _user;
            connectionString.WindowsDomain = _domain;
            connectionString.Password = _password;

            connectionString.SecurityLabelName = _securityLabel;

            K2Connection = new Connection();
            ProcessInstance processInstance = null;
            try
            {
                //open connection to K2 server   
                K2Connection.Open(_serverName, connectionString.ToString());

                processInstance = K2Connection.CreateProcessInstance(processName);
            }
            catch (Exception ex)
            {
                K2Connection.Close();

                throw ex;
            }

            return processInstance;
        }

        public void StartProcessInstance(ProcessInstance processInstance)
        {
            try
            {
                // start workflow
                K2Connection.StartProcessInstance(processInstance, false);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public void Disconnect()
        {
            K2Connection.Close();
        }

        [HttpGet]
        [ActionName("ExecuteWorklistAction")]
        public bool ExecuteWorklistAction(string param, string action, string comment, Dictionary<string, dynamic> dataToPass)
        {
            string _serverName = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ServerName;
            string _user = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserName;
            string _domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserDomain;
            string _password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2UserPassword;
            uint _port = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2Port;
            string _securityLabel = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2SecurityLabel;
            string _sn = param;

            SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
            connectionString.Authenticate = true;
            connectionString.Host = _serverName;
            connectionString.Integrated = true;
            connectionString.IsPrimaryLogin = true;
            connectionString.Port = _port;

            if (_K2DebugMode)
            {
                connectionString.UserID = _user;  // use login user
                connectionString.WindowsDomain = _domain;
                connectionString.Password = _password;
            }

            connectionString.SecurityLabelName = _securityLabel;

            K2Connection = new Connection();
            WorklistItem pd = null;
            try
            {
                //WriteLog("Server Name: " + _serverName);
                //WriteLog("ConnectionString: " + connectionString.ToString());
                //open connection to K2 server   
                K2Connection.Open(_serverName, connectionString.ToString());

                pd = K2Connection.OpenWorklistItem(_sn);

                if (dataToPass != null)
                {
                    foreach (var k in dataToPass.Keys)
                    {
                        pd.ProcessInstance.DataFields[k].Value = dataToPass[k];
                    }
                }

                pd.Actions[action].Execute(true);


                K2Connection.AddComment(pd.ProcessInstance.ID, comment);
            }
            catch (Exception exc)
            {
                K2Connection.Close();

                throw exc;
            }

            return true;
        }

        [HttpGet]
        [ActionName("GetWorkListItem")]
        public WorklistItem GetWorkListItem(string param)
        {
            string _serverName = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2ServerName;
            string _user = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2UserName;
            string _domain = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2UserDomain;
            string _password = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2UserPassword;
            uint _port = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2Port;
            string _securityLabel = global::PTVI.iPROM.WebApi.Properties.Settings.Default.K2SecurityLabel;
            string _sn = param;

            SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder connectionString = new SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder();
            connectionString.Authenticate = true;
            connectionString.Host = _serverName;
            connectionString.Integrated = true;
            connectionString.IsPrimaryLogin = true;
            connectionString.Port = _port;
            connectionString.UserID = _user;
            connectionString.WindowsDomain = _domain;
            connectionString.Password = _password;
            connectionString.SecurityLabelName = _securityLabel;

            K2Connection = new Connection();
            WorklistItem pd = null;
            try
            {
                //open connection to K2 server   
                K2Connection.Open(_serverName, connectionString.ToString());

                pd = K2Connection.OpenWorklistItem(_sn);
            }
            catch (Exception ex)
            {
                K2Connection.Close();

                throw ex;
            }

            return pd;
        }

        [HttpGet]
        [ActionName("RegisterK2ByOID")]
        public IHttpActionResult RegisterK2ByOID(int param)
        {
            K2ConnectController k2 = new K2ConnectController();
            try
            {
                // call K2
                string k2ConnectCode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2ConnectCode;
                ProcessInstance edfProcessInstance = k2.Connect(k2ConnectCode);

                string k2AppID = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2AppID;
                edfProcessInstance.DataFields[k2AppID].Value = param;

                k2.StartProcessInstance(edfProcessInstance);
            }
            catch (DbEntityValidationException exc)
            {
                var errorMessage = "";
                foreach (var error in exc.EntityValidationErrors)
                {
                    foreach (var validationError in error.ValidationErrors)
                        errorMessage += (errorMessage == "" ? "" : ";") + validationError.ErrorMessage;
                }
                WriteLog("K2 Problem:");
                WriteLog(errorMessage);
                throw new ApplicationException(errorMessage);

            }
            catch (Exception ex)
            {
                WriteLog("K2 Problem:");
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
            }
            finally
            {
                k2.Disconnect();
            }

            return Ok();
        }

        public void populateFlightWorkListItems(List<EWRRequestDTO> header)
        {
            var wfIds = string.Join(",", header.Select(x => x.WF_ID.Value).ToArray());

            var wlItemParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.WORKLISTITEM_API_URL);
            var wlItemUrl = wlItemParam.VALUE.Replace("[wfID]", wfIds);

            var workListItems = GetListFromExternalData<K2WorklistItem>(wlItemUrl);

            foreach (var wl in workListItems)
            {
                var gp = header.Where(x => x.WF_ID == wl.procId).FirstOrDefault();
                if (gp != null)
                {
                    if (gp.K2WorklistItems == null)
                        gp.K2WorklistItems = new List<K2WorklistItem>();

                    gp.K2WorklistItems.Add(wl);
                }
            }
        }
        public void populateWorkListItems(List<BudgetApproval> header)
        {
            var wfIds = string.Join(",", header.Select(x => x.WF_ID.Value).ToArray());

            var wlItemParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.WORKLISTITEM_API_URL);
            var wlItemUrl = wlItemParam.VALUE.Replace("[wfID]", wfIds);

            var workListItems = GetListFromExternalData<K2WorklistItem>(wlItemUrl);

            foreach (var wl in workListItems)
            {
                var gp = header.Where(x => x.WF_ID == wl.procId).FirstOrDefault();
                if (gp != null)
                {
                    if (gp.K2WorklistItems == null)
                        gp.K2WorklistItems = new List<K2WorklistItem>();

                    gp.K2WorklistItems.Add(wl);
                }
            }
        }
    }
}
