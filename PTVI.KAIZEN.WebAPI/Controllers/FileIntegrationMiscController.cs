﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class FileIntegrationMiscController : KAIZENController
    {
        private VALE_KAIZENEntities dbMisc = new VALE_KAIZENEntities();

        [HttpGet]
        [ActionName("matexIntegration")]
        public IEnumerable<object> matexIntegration(string param)
        {
            IEnumerable<object> integrations = dbMisc.FILEINTEGRATIONMISCs.
                Select(y => new {
                    id = y.OID,
                    purpose = y.PURPOSE,
                    fileName = y.FILENAME,
                    sheetName = y.SHEETNAME,
                    shareFolder = y.SHAREFOLDER,
                    createdDate = y.CREATEDDATE,
                    createdBy = y.CREATEDBY,
                    updatedDate = y.UPDATEDDATE,
                    updatedBy = y.UPDATEDBY
                }).OrderByDescending(x => x.updatedDate != null ? x.updatedDate:x.createdDate);
            return integrations;
        }

        public class integration
        {

            public int id { get; set; }
            public string purpose { get; set; }
            public string fileName { get; set; }
            public string sheetName { get; set; }
            public string shareFolder { get; set; }
            public Nullable<System.DateTime> createdDate { get; set; }
            public string createdBy { get; set; }
            public Nullable<System.DateTime> updatedDate { get; set; }
            public string updatedBy { get; set; }
        }

        private bool fileIntegrationMiscExist(int id)
        {
            return db.FILEINTEGRATIONs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("integrationById")]
        public integration integrationById(int param)
        {
            integration integrations = db.FILEINTEGRATIONMISCs.Where(x => x.OID == param).
                Select(y => new integration
                {
                    id = y.OID,
                    purpose = y.PURPOSE,
                    fileName = y.FILENAME,
                    sheetName = y.SHEETNAME,
                    shareFolder = y.SHAREFOLDER,
                    createdDate = y.CREATEDDATE,
                    createdBy = y.CREATEDBY,
                    updatedDate = y.UPDATEDDATE,
                    updatedBy = y.UPDATEDBY
                }).FirstOrDefault();
            return integrations;
        }

        [HttpPut]
        [ActionName("updateMatexIntegration")]
        public IHttpActionResult updateMatexIntegration(int param, integration matexInteg)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (param != matexInteg.id)
            {
                return BadRequest();
            }

            try
            {
                FILEINTEGRATIONMISC fin = dbMisc.FILEINTEGRATIONMISCs.Find(param);
                fin.PURPOSE = matexInteg.purpose;
                fin.FILENAME = matexInteg.fileName;
                fin.SHEETNAME = matexInteg.sheetName;
                fin.SHAREFOLDER = matexInteg.shareFolder;
                fin.UPDATEDBY = GetCurrentNTUserId();
                fin.UPDATEDDATE = DateTime.Now;
                dbMisc.Entry(fin).State = EntityState.Modified;
                dbMisc.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!fileIntegrationMiscExist(param))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(integrationById(param));
        }

        [HttpGet]
        [ActionName("matexIntegLoader")]
        public IEnumerable<object> matexIntegLoader(int param)
        {
            IEnumerable<object> loaderObject = db.FILEINTEGRATIONLOADERMISCs.Where(x => x.FILEINTEGRATIONMISCID == param).
                Select(y => new
                {
                    id = y.OID,
                    fileIntegrationId = y.FILEINTEGRATIONMISCID,
                    //purpose = y.FILEINTEGRATIONMISC.PURPOSE,
                    //fileName = y.FILEINTEGRATIONMISC.FILENAME,
                    //sheetName = y.FILEINTEGRATIONMISC.SHEETNAME,
                    loaderDate = y.LOADERDATE,
                    loaderBy = y.LOADERBY,
                    remark = y.REMARK
                }).OrderByDescending(x => x.loaderDate); ;
            return loaderObject;
        }
    }
}