﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTGDI.Email;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TechnicalSupportController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accController = new AccountController();

        // GET: api/TechnicalSupport
        public IQueryable<TECHNICALSUPPORT> GetTECHNICALSUPPORTs()
        {
            return db.TECHNICALSUPPORTs;
        }

        // GET: api/TechnicalSupport/5
        [ResponseType(typeof(TECHNICALSUPPORT))]
        public IHttpActionResult GetTECHNICALSUPPORT(int id)
        {
            TECHNICALSUPPORT tECHNICALSUPPORT = db.TECHNICALSUPPORTs.Find(id);
            if (tECHNICALSUPPORT == null)
            {
                return NotFound();
            }

            return Ok(tECHNICALSUPPORT);
        }

        // PUT: api/TechnicalSupport/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTECHNICALSUPPORT(int id, TECHNICALSUPPORT tECHNICALSUPPORT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tECHNICALSUPPORT.OID)
            {
                return BadRequest();
            }

            db.Entry(tECHNICALSUPPORT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TECHNICALSUPPORTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TechnicalSupport
        [ResponseType(typeof(TECHNICALSUPPORT))]
        public IHttpActionResult PostTECHNICALSUPPORT(TECHNICALSUPPORT tECHNICALSUPPORT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TECHNICALSUPPORTs.Add(tECHNICALSUPPORT);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tECHNICALSUPPORT.OID }, tECHNICALSUPPORT);
        }

        // DELETE: api/TechnicalSupport/5
        [ResponseType(typeof(TECHNICALSUPPORT))]
        public IHttpActionResult DeleteTECHNICALSUPPORT(int id)
        {
            TECHNICALSUPPORT tECHNICALSUPPORT = db.TECHNICALSUPPORTs.Find(id);
            if (tECHNICALSUPPORT == null)
            {
                return NotFound();
            }

            db.TECHNICALSUPPORTs.Remove(tECHNICALSUPPORT);
            db.SaveChanges();

            return Ok(tECHNICALSUPPORT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TECHNICALSUPPORTExists(int id)
        {
            return db.TECHNICALSUPPORTs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("listTechnicalSupport")]
        public IEnumerable<technicalSupportObject> listTechnicalSupport(string param, string loptions)
        {
            var ts = new List<TECHNICALSUPPORT>();
            if (param != "1")
            {
                ts = db.TECHNICALSUPPORTs.Where(w => w.USERASSIGNMENTs.Where(s => s.EMPLOYEEID == param).Count() > 0 && 
                    loptions=="lOpen"? w.STATUS != constants.STATUSIPROM_COMPLETED: loptions == "lCompleted"? 
                    w.STATUS == constants.STATUSIPROM_COMPLETED : w.STATUS != null
                    ).OrderByDescending(z=>z.CREATEDDATE).ToList();
                var tsupport = ts.Select(s => new technicalSupportObject
                {
                    id = s.OID,
                    tsNo = s.TSNO,
                    tsDesc = s.TSDESC,
                    sharedFolder = s.SHAREFOLDER,
                    requestDate = s.REQUESTDATE,
                    problemStatement = s.PROBLEMSTATEMENT,
                    technicalSupportAreas = s.TECHNICALSUPPORTAREAs.Select(tsa => new technicalSupportAreaObject
                    {
                        areaInvolved = new areaInvolvedObject { areaName = tsa.AREAINVOLVED.AREANAME, areaCode = tsa.AREAINVOLVED.AREACODE, areaDesc = tsa.AREAINVOLVED.AREADESC, id = tsa.AREAINVOLVED.OID },
                        areaInvolvedId = tsa.AREAINVOLVEDID

                    }).ToList(),
                    //requestBy = db.USERASSIGNMENTs.FirstOrDefault(y => y.TECHNICALSUPPORTID == s.OID && y.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_REQUESTER && y.ISACTIVE == true).EMPLOYEENAME,
                    userAssignments = db.USERASSIGNMENTs.Where(w1 => w1.TECHNICALSUPPORTID == s.OID && w1.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_REQUESTER && w1.ISACTIVE == true).
                                 Select(requestByEmployee => new userAssignmentObject
                                 {
                                     id = requestByEmployee.OID,
                                     employeeName = requestByEmployee.EMPLOYEENAME,
                                     employeeId = requestByEmployee.EMPLOYEEID,
                                     employeeEmail = requestByEmployee.EMPLOYEEEMAIL,
                                     employeePosition = requestByEmployee.EMPLOYEEPOSITION
                                 }).ToList(),

                    engineerLists = db.USERASSIGNMENTs.Where(we => we.TECHNICALSUPPORTID == s.OID && we.ASSIGNMENTTYPE == "TSE").
                             Select(englist => new userAssignmentObject { employeeName = englist.EMPLOYEENAME }).ToList(),
                    designerLists = db.USERASSIGNMENTs.Where(wd => wd.TECHNICALSUPPORTID == s.OID && wd.ASSIGNMENTTYPE == "TSD").
                             Select(deslist => new userAssignmentObject { employeeName = deslist.EMPLOYEENAME }).ToList(),
                    status = s.STATUS

                });
                return tsupport;
            }
            else
            {
                ts = db.TECHNICALSUPPORTs.Where(w => 
                (
                    loptions == "lOpen" ? w.STATUS != constants.STATUSIPROM_COMPLETED : loptions == "lCompleted" ?
                    w.STATUS == constants.STATUSIPROM_COMPLETED : w.STATUS != null
                )
                ).OrderByDescending(z=>z.CREATEDDATE).ToList();

                var tsupport = ts.Select(s => new technicalSupportObject
                {
                    id = s.OID,
                    tsNo = s.TSNO,
                    tsDesc = s.TSDESC,
                    requestDate = s.REQUESTDATE,
                    problemStatement = s.PROBLEMSTATEMENT,
                    sharedFolder = s.SHAREFOLDER,
                    technicalSupportAreas = s.TECHNICALSUPPORTAREAs.Select(tsa => new technicalSupportAreaObject
                    {
                        areaInvolved = new areaInvolvedObject { areaName = tsa.AREAINVOLVED.AREANAME, areaCode = tsa.AREAINVOLVED.AREACODE, areaDesc = tsa.AREAINVOLVED.AREADESC, id = tsa.AREAINVOLVED.OID },
                        areaInvolvedId = tsa.AREAINVOLVEDID

                    }).ToList(),
                    //requestBy = db.USERASSIGNMENTs.FirstOrDefault(y => y.TECHNICALSUPPORTID == s.OID && y.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_REQUESTER && y.ISACTIVE == true).EMPLOYEENAME,
                    userAssignments = db.USERASSIGNMENTs.Where(w1 => w1.TECHNICALSUPPORTID == s.OID && w1.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_REQUESTER && w1.ISACTIVE == true).
                 Select(requestByEmployee => new userAssignmentObject
                 {
                     id = requestByEmployee.OID,
                     employeeName = requestByEmployee.EMPLOYEENAME,
                     employeeId = requestByEmployee.EMPLOYEEID,
                     employeeEmail = requestByEmployee.EMPLOYEEEMAIL,
                     employeePosition = requestByEmployee.EMPLOYEEPOSITION
                 }).ToList(),

                    engineerLists = db.USERASSIGNMENTs.Where(we => we.TECHNICALSUPPORTID == s.OID && we.ASSIGNMENTTYPE == "TSE").
                             Select(englist => new userAssignmentObject { employeeName = englist.EMPLOYEENAME }).ToList(),
                    designerLists = db.USERASSIGNMENTs.Where(wd => wd.TECHNICALSUPPORTID == s.OID && wd.ASSIGNMENTTYPE == "TSD").
                             Select(deslist => new userAssignmentObject { employeeName = deslist.EMPLOYEENAME }).ToList(),
                    status = s.STATUS

                });
                return tsupport;
            }
        }

        [HttpGet]
        [ActionName("areaInvolved")]
        public IQueryable<technicalSupportAreaObject> areaInvolved(int param)
        {
            IQueryable<technicalSupportAreaObject> uAObj;

            uAObj = db.TECHNICALSUPPORTAREAs.Where(x => x.TECHNICALSUPPORTID == param).Select(o => new technicalSupportAreaObject()
            {
                id = o.OID,
                areaInvolvedId = o.AREAINVOLVEDID

            });

            return uAObj;
        }

        [HttpGet]
        [ActionName("GetRunningNo")]
        public string GetRunningNo(string param)
        {
            var tsNoOBJ = db.TECHNICALSUPPORTs.OrderByDescending(s => s.OID).FirstOrDefault().TSNO;
            string runningNo;
            if (tsNoOBJ != null)
            { runningNo = tsNoOBJ.Substring(4, 4); }
            else { runningNo = "0"; }
            return runningNo; 
        }

        [HttpPost]
        [ActionName("SubmitRequest")]
        public IHttpActionResult SubmitRequest(string param, technicalSupportObject technicalSupportObjectParam)
        {
                var tsCount= "";
                if (db.TECHNICALSUPPORTs.Count() > 0)
                    tsCount = (db.TECHNICALSUPPORTs.Count() + 1).ToString().PadLeft(5, '0');
                else tsCount = "00001";
                TECHNICALSUPPORT technicalSupport = new TECHNICALSUPPORT();
                technicalSupport.TSNO = technicalSupportObjectParam.tsNo+tsCount;
                technicalSupport.TSDESC = technicalSupportObjectParam.tsDesc;
                technicalSupport.REQUESTDATE = technicalSupportObjectParam.requestDate;
                technicalSupport.PROBLEMSTATEMENT = technicalSupportObjectParam.problemStatement;
                technicalSupport.STATUS = constants.STATUSIPROM_NOTSTARTED;
                technicalSupport.CREATEDDATE = DateTime.Now;
                technicalSupport.CREATEDBY = GetCurrentNTUserId();

            technicalSupport.TECHNICALSUPPORTAREAs = technicalSupportObjectParam.technicalSupportAreas.Select(s =>
                new TECHNICALSUPPORTAREA
                {
                    AREAINVOLVEDID = s.areaInvolvedId,
                    TECHNICALSUPPORT = { },
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = GetCurrentNTUserId()
                    }
                    ).ToList();


                technicalSupport.USERASSIGNMENTs = technicalSupportObjectParam.userAssignments.Select(u =>
                    new USERASSIGNMENT
                    {
                        ASSIGNMENTTYPE = constants.TECHNICAL_SUPPORT_REQUESTER,
                        EMPLOYEEID = u.employeeId,
                        EMPLOYEENAME = u.employeeName,
                        EMPLOYEEEMAIL = u.employeeEmail,
                        EMPLOYEEPOSITION = u.employeePosition,
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = GetCurrentNTUserId(),
                        ISACTIVE = true
                    }
                    ).ToList();
                db.TECHNICALSUPPORTs.Add(technicalSupport);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw;
            }
            EmailSender emailSender = new EmailSender();
            //send email
            var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.technicalSupport).FirstOrDefault();
            var mailList = new List<usersToEmail>();
            var ua = technicalSupport.USERASSIGNMENTs;
            foreach (var u in ua)
            {
                var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = u.EMPLOYEEEMAIL, position = posName != null ? posName.TEXT : "" });
            }
            string userList = "<table>";
            foreach (var l in mailList)
            {
                userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
            }
            userList = userList + "</table>";

            string RecipientEmail = ua.FirstOrDefault().EMPLOYEEEMAIL;
            string RecipientName = ua.FirstOrDefault().EMPLOYEENAME;
            string ass = ua.FirstOrDefault().ASSIGNMENTTYPE;
            var RecipientCC = "";
            var FromEmail = "testing@ptgdi.com";
            var subject = mail.SUBJECT;
            var body = mail.BODY;
            var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
            var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + ua.FirstOrDefault().EMPLOYEEID).FirstOrDefault();
            string prefixGender = "";
            if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                prefixGender = constants.prefixmale;
            else
                if (employeeDetail.MARITAL_STATUS == constants.married)
                prefixGender = constants.prefixfemaleMarried;
            else
                prefixGender = constants.prefixfemaleSingle;
            body = body.Replace("[user]", prefixGender + " " + RecipientName);
            var asstype = db.LOOKUPs.Where(x => x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == ass).FirstOrDefault().TEXT;
            body = body.Replace("[assignmenttype]", asstype);
            body = body.Replace("[tsNo]", technicalSupport.TSNO);
            body = body.Replace("[tsDesc]", technicalSupport.TSDESC);
            body = body.Replace("[reqDate]", technicalSupport.REQUESTDATE.Value.ToString("dd MMMM yyyy"));
            body = body.Replace("[area]", string.Join(", ", technicalSupport.TECHNICALSUPPORTAREAs.Join(db.AREAINVOLVEDs,s=>s.AREAINVOLVEDID,t=>t.OID, (s,t) => new {t.AREADESC}).Select(x=>x.AREADESC).ToArray()));
            body = body.Replace("[problemStat]", technicalSupport.PROBLEMSTATEMENT);
            body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/HomeForm.aspx");
            body = body.Replace("[assign]", accController.getEmployeeByBN(technicalSupport.CREATEDBY).full_Name);
            body = body.Replace("\r\n", "");
            body = body.Replace("\\", "");
            emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);
            technicalSupport.TECHNICALSUPPORTAREAs=null;
            technicalSupport.USERASSIGNMENTs = null;
            return Ok(technicalSupport);
        }

        [HttpGet]
        [ActionName("TechnicalSupportListBytsId")]
        public technicalSupportObject TechnicalSupportListBytsId(int param)
        {
            return db.TECHNICALSUPPORTs.Where(w => w.OID == param).ToList().Select(s =>
                 new technicalSupportObject
                 {
                     id = s.OID,
                     tsNo = s.TSNO,
                     tsDesc = s.TSDESC
                     
                 }).FirstOrDefault();
        }

        [HttpPut]
        [ActionName("updateTechnicalSupport")]
        public HttpResponseMessage updateTechnicalSupport(int param, technicalSupportObject technicalSupportObj)
        {
            TECHNICALSUPPORT updateTechnicalSupport = db.TECHNICALSUPPORTs.Find(param);

            updateTechnicalSupport.TSNO = technicalSupportObj.tsNo;
            updateTechnicalSupport.TSDESC = technicalSupportObj.tsDesc;
            updateTechnicalSupport.SHAREFOLDER = technicalSupportObj.sharedFolder;
            updateTechnicalSupport.PROBLEMSTATEMENT = technicalSupportObj.problemStatement;
            updateTechnicalSupport.UPDATEDBY = GetCurrentNTUserId();
            updateTechnicalSupport.UPDATEDDATE = DateTime.Now;
            updateTechnicalSupport.REQUESTDATE = technicalSupportObj.requestDate;
            updateTechnicalSupport.STATUS = technicalSupportObj.status;

            //Start untuk update TECHNICALSUPPORTAREAs data yg ada technicalSupportObj.technicalSupportAreas
            foreach (TECHNICALSUPPORTAREA ts in updateTechnicalSupport.TECHNICALSUPPORTAREAs.Where(w => technicalSupportObj.technicalSupportAreas.Select(s => s.areaInvolvedId).Contains(w.AREAINVOLVEDID)))
            {
                ts.UPDATEDBY = GetCurrentNTUserId();
                ts.UPDATEDDATE = DateTime.Now;
            }
            //End untuk update TECHNICALSUPPORTAREAs data yg ada technicalSupportObj.technicalSupportAreas
            List<TECHNICALSUPPORTAREA> toBeDelete = new List<TECHNICALSUPPORTAREA>();
            int i = 0;
            //Start untuk delete TECHNICALSUPPORTAREAs data yg tidak ada technicalSupportObj.technicalSupportAreas
            foreach (TECHNICALSUPPORTAREA ts in updateTechnicalSupport.TECHNICALSUPPORTAREAs.Where(w => !technicalSupportObj.technicalSupportAreas.Select(s => s.areaInvolvedId).Contains(w.AREAINVOLVEDID)))
            {
                toBeDelete.Add(ts);
                i++;
            }

            foreach(TECHNICALSUPPORTAREA tbd in toBeDelete)
            {
                db.TECHNICALSUPPORTAREAs.Remove(tbd);
            }
            //End untuk delete TECHNICALSUPPORTAREAs data yg tidak ada technicalSupportObj.technicalSupportAreas

            //Start untuk insert TECHNICALSUPPORTAREAs data baru
            foreach (technicalSupportAreaObject tsArea in technicalSupportObj.technicalSupportAreas)
            {
                if (db.TECHNICALSUPPORTAREAs.Count(x => x.AREAINVOLVEDID == tsArea.areaInvolvedId)==0)
                {
                    TECHNICALSUPPORTAREA ts = new TECHNICALSUPPORTAREA();
                    ts.AREAINVOLVEDID = tsArea.areaInvolvedId;
                    ts.CREATEDBY = GetCurrentNTUserId();
                    ts.CREATEDDATE = DateTime.Now;
                    updateTechnicalSupport.TECHNICALSUPPORTAREAs.Add(ts);
                }
                tsArea.areaInvolved = db.AREAINVOLVEDs.Where(x => x.OID == tsArea.areaInvolvedId).Select(y => new areaInvolvedObject()
                {
                    areaCode = y.AREACODE,
                    areaDesc = y.AREADESC,
                    areaName = y.AREANAME,
                    id = y.OID
                }).FirstOrDefault();
            }
            //End untuk insert TECHNICALSUPPORTAREAs data baru

            updateTechnicalSupport.USERASSIGNMENTs.ToList()[0].EMPLOYEEID = technicalSupportObj.userAssignments.ToList()[0].employeeId;
            updateTechnicalSupport.USERASSIGNMENTs.ToList()[0].EMPLOYEENAME = technicalSupportObj.userAssignments.ToList()[0].employeeName;
            updateTechnicalSupport.USERASSIGNMENTs.ToList()[0].EMPLOYEEEMAIL = technicalSupportObj.userAssignments.ToList()[0].employeeEmail;
            updateTechnicalSupport.USERASSIGNMENTs.ToList()[0].EMPLOYEEPOSITION = technicalSupportObj.userAssignments.ToList()[0].employeePosition;

            db.Entry(updateTechnicalSupport).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
            //            var lts = listTechnicalSupport("1").ToList().Find(x => x.id==param);
            technicalSupportObj.engineerLists = db.USERASSIGNMENTs.Where(we => we.TECHNICALSUPPORTID == param && we.ASSIGNMENTTYPE == "TSE").
         Select(englist => new userAssignmentObject { employeeName = englist.EMPLOYEENAME }).ToList();
            technicalSupportObj.designerLists = db.USERASSIGNMENTs.Where(wd => wd.TECHNICALSUPPORTID == param && wd.ASSIGNMENTTYPE == "TSD").
                     Select(deslist => new userAssignmentObject { employeeName = deslist.EMPLOYEENAME }).ToList();

       //     technicalSupportObj.userAssignments = null;
            return Request.CreateResponse(HttpStatusCode.OK, technicalSupportObj);
        }


        public IEnumerable<userAssignmentObject> GetByAssType(int param, string AssignmentType, int? tsID)
        {
            IEnumerable<userAssignmentObject> uAObj = new List<userAssignmentObject>();
            if (tsID != null)
            {
                uAObj = db.USERASSIGNMENTs.Where(x => x.ASSIGNMENTTYPE == AssignmentType && x.TECHNICALSUPPORTID == tsID).Select(o => new userAssignmentObject()
                {
                    id = o.OID,
                    projectId = o.PROJECTID,
                    technicalSupportId = o.TECHNICALSUPPORTID,
                    assignmentType = o.ASSIGNMENTTYPE,
                    employeeId = o.EMPLOYEEID,
                    employeeName = o.EMPLOYEENAME,
                    employeeEmail = o.EMPLOYEEEMAIL,
                    employeePosition = o.EMPLOYEEPOSITION,
                    joinedDate = o.JOINEDDATE,
                    isActive = o.ISACTIVE,
                    createdDate = o.CREATEDDATE,
                    createdBy = o.CREATEDBY,
                    //updatedDate = o.UPDATEDDATE,
                    updatedBy = o.UPDATEDBY


                });
            }
            return uAObj;
        }

        [HttpPost]
        [ActionName("postUserAssignmentObject")]
        public HttpResponseMessage postUserAssignmentObject(int param, userAssignmentObject uAObj)
        {
            EmailSender emailSender = new EmailSender();
            if (ModelState.IsValid)
            {
                if (db.USERASSIGNMENTs.Where(x => (x.EMPLOYEEID == uAObj.employeeId && x.ASSIGNMENTTYPE == uAObj.assignmentType && x.TECHNICALSUPPORTID == uAObj.technicalSupportId)).Count() < 1)
                {
                    USERASSIGNMENT users = new USERASSIGNMENT();
                    users.OID = 0;
                    users.PROJECTID = null;
                    users.TECHNICALSUPPORTID = uAObj.technicalSupportId;
                    users.ASSIGNMENTTYPE = uAObj.assignmentType;
                    users.EMPLOYEEID = uAObj.employeeId;
                    users.EMPLOYEENAME = uAObj.employeeName;
                    users.EMPLOYEEEMAIL = uAObj.employeeEmail;
                    users.EMPLOYEEPOSITION = uAObj.employeePosition;
                    users.JOINEDDATE = uAObj.joinedDate;
                    users.ISACTIVE = uAObj.isActive;
                    users.CREATEDDATE = DateTime.Now;
                    users.CREATEDBY = uAObj.createdBy;
                    users.UPDATEDDATE = DateTime.Now;
                    users.UPDATEDBY = uAObj.updatedBy;
                    users.TECHNICALSUPPORTUSERACTIONs = uAObj.toTechnicalSupportInteraction.Select(s => new TECHNICALSUPPORTUSERACTION
                    {

                        CREATEDBY = uAObj.createdBy,
                        CREATEDDATE = DateTime.Now,
                        UPDATEDBY = uAObj.createdBy,
                        UPDATEDDATE = uAObj.updatedDate


                    }).ToList();

                    db.USERASSIGNMENTs.Add(users);
                    db.SaveChanges();
                    uAObj.id = users.OID;

                    //send email
                    if (uAObj.employeeEmail != null)
                    {
                        var assType = (uAObj.assignmentType == constants.PROJECT_ENGINEER || uAObj.assignmentType == constants.PROJECT_DESIGNER) ? constants.engineerDesigner :
                        (uAObj.assignmentType == constants.PROJECT_PROJECT_CONTROLLER || uAObj.assignmentType == constants.PROJECT_MATERIAL_COORDINATOR) ? constants.projectService :
                        (uAObj.assignmentType == constants.TECHNICAL_SUPPORT_ENGINEER || uAObj.assignmentType == constants.TECHNICAL_SUPPORT_DESIGNER) ? constants.engineerDesignerTS : "";
                        var mail = db.EMAILREDACTIONs.Where(x => x.CODE == assType).FirstOrDefault();
                        var projectRequest = db.PROJECTs.Where(x => x.OID == uAObj.projectId).FirstOrDefault();
                        var mailList = new List<usersToEmail>() {
                        };
                        var TSRequest = db.TECHNICALSUPPORTs.Where(x => x.OID == uAObj.technicalSupportId).FirstOrDefault();
                        var ua = db.USERASSIGNMENTs.Where(x => (x.TECHNICALSUPPORTID == uAObj.technicalSupportId && x.ISACTIVE == true)).OrderBy(z => z.ASSIGNMENTTYPE);
                        foreach (var u in ua)
                        {
                            var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                            if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                            if (u.EMPLOYEEID != uAObj.employeeId)
                                mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = u.EMPLOYEEEMAIL, position = posName != null ? posName.DESCRIPTION.ToUpper() : "" });
                        }
                        string RecipientEmail = uAObj.employeeEmail;
                        string userList = "<table style='width:550px'>";
                        foreach (var l in mailList)
                        {
                            userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
                        }
                        userList = userList + "</table>";

                        string RecipientName = uAObj.employeeEmail;
                        string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
                        var RecipientCC = "";
                        var FromEmail = "testing@ptgdi.com";
                        var subject = mail.SUBJECT;
                        var body = mail.BODY;
                        var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
                        var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + uAObj.employeeId).FirstOrDefault();
                        string prefixGender = "";
                        if (employeeDetail != null && employeeDetail.GENDER != null)
                        {
                            if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                                prefixGender = constants.prefixmale;
                            else
                                if (employeeDetail.MARITAL_STATUS == constants.married)
                                prefixGender = constants.prefixfemaleMarried;
                            else
                                prefixGender = constants.prefixfemaleSingle;
                        }
                        else prefixGender = "Mr\\Mrs";
                        body = body.Replace("[user]", prefixGender + " " + uAObj.employeeName);
                        var n = body.IndexOf("<p id='assignedstatus'>");
                        var m = body.IndexOf("</p>", n) + 4;
                        var str = body.Substring(n, m - n);
                        var asstype = db.LOOKUPs.Where(x => x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == uAObj.assignmentType).FirstOrDefault().DESCRIPTION;
                        body = body.Replace("[assignmenttype]", asstype);
                        body = body.Replace("[tsNo]", TSRequest.TSNO);
                        body = body.Replace("[tsDesc]", TSRequest.TSDESC);
                        body = body.Replace("[reqDate]", TSRequest.REQUESTDATE.Value.ToString("dd MMMM yyyy"));
                        body = body.Replace("[area]", string.Join(", ", db.TECHNICALSUPPORTAREAs.Where(x => x.TECHNICALSUPPORTID == TSRequest.OID).Join(db.AREAINVOLVEDs, s => s.AREAINVOLVEDID, t => t.OID, (s, t) => new { t.AREADESC }).Select(x => x.AREADESC).ToArray()));
                        body = body.Replace("[problemStat]", TSRequest.PROBLEMSTATEMENT);
                        body = body.Replace("[listUser]", userList);
                        body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/HomeForm.aspx?tab=TS");
                        body = body.Replace("[assign]", accController.getEmployeeByBN(TSRequest.CREATEDBY).full_Name);
                        body = body.Replace("\r\n", "");
                        body = body.Replace("\\", "");
                        emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, uAObj);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = uAObj.id }));
                    return response;
                }
                else
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Employee ID Exist");
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", 0));
                    return response;
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [ActionName("PutUserAssignmentObject")]
        public HttpResponseMessage 
        PutUserAssignmentObject(int param, userAssignmentObject uAObj)
        {
            EmailSender emailSender = new EmailSender();
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (param != uAObj.id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            bool deactivate = false;
            bool activeChange = false;

            var userObj = db.USERASSIGNMENTs.Where(x => x.OID == uAObj.id).FirstOrDefault();
            userObj.PROJECTID = null;
            userObj.TECHNICALSUPPORTID = uAObj.technicalSupportId;
            userObj.ASSIGNMENTTYPE = uAObj.assignmentType;
            userObj.EMPLOYEEID = uAObj.employeeId;
            userObj.EMPLOYEENAME = uAObj.employeeName;
            userObj.EMPLOYEEEMAIL = uAObj.employeeEmail;
            userObj.EMPLOYEEPOSITION = uAObj.employeePosition;
            userObj.JOINEDDATE = uAObj.joinedDate;
            if (userObj.ISACTIVE != uAObj.isActive)
                activeChange = true;
            if (userObj.ISACTIVE == true && uAObj.isActive == false)
                deactivate = true;
            userObj.ISACTIVE = uAObj.isActive;
            userObj.UPDATEDDATE = DateTime.Now;
            userObj.UPDATEDBY = uAObj.updatedBy;
            db.Entry(userObj).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            if (activeChange == true)
            {
                //send email
                var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.engineerDesignerTS).FirstOrDefault();
                var TSRequest = db.TECHNICALSUPPORTs.Where(x => x.OID == uAObj.technicalSupportId).FirstOrDefault();
                var mailList = new List<usersToEmail>()
                {
                };

                var ua = db.USERASSIGNMENTs.Where(x => (x.TECHNICALSUPPORTID == uAObj.technicalSupportId && x.ISACTIVE == true)).OrderBy(z => z.ASSIGNMENTTYPE);
                foreach (var u in ua)
                {
                    var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                    if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                    if (u.EMPLOYEEID != uAObj.employeeId)
                        mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = u.EMPLOYEEEMAIL, position = posName != null ? posName.DESCRIPTION : "" });
                }

                string RecipientEmail = uAObj.employeeEmail;
                string userList = "<table style='width:550px'>";
                foreach (var l in mailList)
                {
                    userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
                }
                userList = userList + "</table>";
                string RecipientName = uAObj.employeeName;
                string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
                var RecipientCC = "";
                var FromEmail = "testing@ptgdi.com";
                var subject = mail.SUBJECT;
                var body = mail.BODY;
                var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
                var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + uAObj.employeeId).FirstOrDefault();
                string prefixGender = "";
                if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                    prefixGender = constants.prefixmale;
                else
                    if (employeeDetail.MARITAL_STATUS == constants.married)
                    prefixGender = constants.prefixfemaleMarried;
                else
                    prefixGender = constants.prefixfemaleSingle;
                body = body.Replace("[user]", prefixGender + " " + uAObj.employeeName);
                var n = body.IndexOf("<p id='assignedstatus'>");
                var m = body.IndexOf("</p>", n) + 4;
                var str = body.Substring(n, m - n);
                var asstype = db.LOOKUPs.Where(x => x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == uAObj.assignmentType).FirstOrDefault().DESCRIPTION;
                if (deactivate == true) body = body.Replace(str, "<p style='background-color: red; color:white;text-align: center; font-size: 14'>You are no longer involved in this project as " + asstype + "</p>");
                body = body.Replace("[tsNo]", TSRequest.TSNO);
                body = body.Replace("[tsDesc]", TSRequest.TSDESC);
                body = body.Replace("[reqDate]", TSRequest.REQUESTDATE.Value.ToString("dd MMMM yyyy"));
                body = body.Replace("[area]", string.Join(", ", db.TECHNICALSUPPORTAREAs.Where(x => x.TECHNICALSUPPORTID == TSRequest.OID).Join(db.AREAINVOLVEDs, s => s.AREAINVOLVEDID, t => t.OID, (s, t) => new { t.AREADESC }).Select(x => x.AREADESC).ToArray()));
                body = body.Replace("[problemStat]", TSRequest.PROBLEMSTATEMENT);
                body = body.Replace("[listUser]", userList);
                n = body.IndexOf("<td id='pagelink'");
                n = body.IndexOf(">", n);
                m = body.IndexOf("</td>", n);
                str = body.Substring(n, m - n);
                if (deactivate == true)
                    body = body.Replace(str,
                        "&nbsp;");
                else
                    body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/HomeForm.aspx?tab=TS");
                body = body.Replace("[assign]", accController.getEmployeeByBN(TSRequest.CREATEDBY).full_Name);
                body = body.Replace("\r\n", "");
                body = body.Replace("\\", "");
                emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);
            }
            return Request.CreateResponse(HttpStatusCode.OK, uAObj);
        }

        [HttpDelete]
        [ActionName("delUserAssignmentObject")]
        public HttpResponseMessage
        delUserAssignmentObject(int param)
        {
            EmailSender emailSender = new EmailSender();
            var ua = db.USERASSIGNMENTs.Find(param);
            var tsid = 0;
            var empid = "";
            var empmail = "";
            var empname = "";
            var astype = "";
            var tsua = new List<TECHNICALSUPPORTUSERACTION>();
            if (ua != null)
            {
                tsua = db.TECHNICALSUPPORTUSERACTIONs.Where(x => x.USERASSIGNMENTID == param).ToList();
                foreach (var tsu in tsua)
                    db.TECHNICALSUPPORTUSERACTIONs.Remove(tsu);
                db.SaveChanges();
                tsid = ua.TECHNICALSUPPORTID.Value;
                empid = ua.EMPLOYEEID;
                empmail = ua.EMPLOYEEEMAIL;
                empname = ua.EMPLOYEENAME;
                astype = ua.ASSIGNMENTTYPE;
                db.USERASSIGNMENTs.Remove(ua);
            }
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            //send email
            var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.engineerDesignerTS).FirstOrDefault();
                var TSRequest = db.TECHNICALSUPPORTs.Where(x => x.OID == tsid).FirstOrDefault();
                var mailList = new List<usersToEmail>()
                {
                };

                var uas = db.USERASSIGNMENTs.Where(x => (x.TECHNICALSUPPORTID == tsid && x.ISACTIVE == true)).OrderBy(z => z.ASSIGNMENTTYPE).ToList();
                foreach (var u in uas)
                {
                    var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                    if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                    mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = u.EMPLOYEEEMAIL, position = posName != null ? posName.TEXT : "" });
                }

                string RecipientEmail = empmail;
                string userList = "<table style='width:550px'>";
                foreach (var l in mailList)
                {
                    userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
                }
                userList = userList + "</table>";
                string RecipientName = empname;
                string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
                var RecipientCC = "";
                var FromEmail = "testing@ptgdi.com";
                var subject = mail.SUBJECT;
                var body = mail.BODY;
                var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
                var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + empid).FirstOrDefault();
                string prefixGender = "";
                if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                    prefixGender = constants.prefixmale;
                else
                    if (employeeDetail.MARITAL_STATUS == constants.married)
                    prefixGender = constants.prefixfemaleMarried;
                else
                    prefixGender = constants.prefixfemaleSingle;
                body = body.Replace("[user]", prefixGender + " " + empname);
                var n = body.IndexOf("<p id='assignedstatus'>");
                var m = body.IndexOf("</p>", n) + 4;
                var str = body.Substring(n, m - n);
                var asstype = db.LOOKUPs.Where(x => x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == astype).FirstOrDefault().TEXT;
                body = body.Replace(str, "<p style='background-color: red; color:white;text-align: center; font-size: 14'>You are no longer involved in this project as " + asstype + "</p>");
                body = body.Replace("[tsNo]", TSRequest.TSNO);
                body = body.Replace("[tsDesc]", TSRequest.TSDESC);
                body = body.Replace("[reqDate]", TSRequest.REQUESTDATE.Value.ToString("dd MMMM yyyy"));
                body = body.Replace("[area]", string.Join(", ", db.TECHNICALSUPPORTAREAs.Where(x => x.TECHNICALSUPPORTID == TSRequest.OID).Join(db.AREAINVOLVEDs, s => s.AREAINVOLVEDID, t => t.OID, (s, t) => new { t.AREADESC }).Select(x => x.AREADESC).ToArray()));
                body = body.Replace("[problemStat]", TSRequest.PROBLEMSTATEMENT);
                body = body.Replace("[listUser]", userList);
                n = body.IndexOf("<td id='pagelink'");
                n = body.IndexOf(">", n);
                m = body.IndexOf("</td>", n);
                str = body.Substring(n, m - n);
                    body = body.Replace(str,
                        "&nbsp;");
                body = body.Replace("[assign]", accController.getEmployeeByBN(TSRequest.CREATEDBY).full_Name);
                body = body.Replace("\r\n", "");
                body = body.Replace("\\", "");
                emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);


            return Request.CreateResponse(HttpStatusCode.OK, ua);
        }



        [HttpGet]
        [ActionName("listEngineerDesignerByTechnicalSupportId")]
        public IEnumerable<userAssignmentObject> listEngineerDesignerByTechnicalSupportId(int param, string badgeNo)
        {
            //by bimo
            //return db.USERASSIGNMENTs.Where(w => w.TECHNICALSUPPORTID == param && (w.ASSIGNMENTTYPE == "TSE" || w.ASSIGNMENTTYPE == "TSD")).Select(s => new userAssignmentObject
            //{
            //    id = db.TECHNICALSUPPORTUSERACTIONs.Where(w1 => w1.USERASSIGNMENTID == s.OID).FirstOrDefault().OID,
            //    employeeName = s.EMPLOYEENAME,
            //    employeePosition = s.EMPLOYEEPOSITION,
            //    result = db.TECHNICALSUPPORTUSERACTIONs.Where(w2 => w2.USERASSIGNMENTID == s.OID).FirstOrDefault().RESULT,
            //    completedDate = db.TECHNICALSUPPORTUSERACTIONs.Where(w3 => w3.USERASSIGNMENTID == s.OID).FirstOrDefault().COMPLETEDDATE,
            //    completePercentage = db.TECHNICALSUPPORTUSERACTIONs.Where(w4 => w4.USERASSIGNMENTID == s.OID).FirstOrDefault().COMPLETEPERCENTAGE,
            //    actionDescription = db.TECHNICALSUPPORTUSERACTIONs.Where(w5 => w5.USERASSIGNMENTID == s.OID).FirstOrDefault().ACTIONDESCRIPTION
            //});

            return badgeNo != "1" ? db.TECHNICALSUPPORTUSERACTIONs.Where(w => w.USERASSIGNMENT.EMPLOYEEID == badgeNo && w.USERASSIGNMENT.TECHNICALSUPPORTID == param && (w.USERASSIGNMENT.ASSIGNMENTTYPE == "TSE" || w.USERASSIGNMENT.ASSIGNMENTTYPE == "TSD")).Select(s => new userAssignmentObject
            {
                id = s.OID,
                employeeId = s.USERASSIGNMENT.EMPLOYEEID,
                employeeName = s.USERASSIGNMENT.EMPLOYEENAME,
                employeePosition = s.USERASSIGNMENT.EMPLOYEEPOSITION,
                engineerDesigner = s.USERASSIGNMENT.ASSIGNMENTTYPE == "TSE" ? "Engineer" : "Designer",
                result = s.RESULT,
                completedDate = s.COMPLETEDDATE,
                completePercentage = s.COMPLETEPERCENTAGE,
                actionDescription = s.ACTIONDESCRIPTION
            }) : db.TECHNICALSUPPORTUSERACTIONs.Where(w => w.USERASSIGNMENT.TECHNICALSUPPORTID == param && (w.USERASSIGNMENT.ASSIGNMENTTYPE == "TSE" || w.USERASSIGNMENT.ASSIGNMENTTYPE == "TSD")).Select(s => new userAssignmentObject
            {
                id = s.OID,
                employeeId = s.USERASSIGNMENT.EMPLOYEEID,
                employeeName = s.USERASSIGNMENT.EMPLOYEENAME,
                employeePosition = s.USERASSIGNMENT.EMPLOYEEPOSITION,
                engineerDesigner = s.USERASSIGNMENT.ASSIGNMENTTYPE == "TSE" ? "Engineer" : "Designer",
                result = s.RESULT,
                completedDate = s.COMPLETEDDATE,
                completePercentage = s.COMPLETEPERCENTAGE,
                actionDescription = s.ACTIONDESCRIPTION
            });
        }

        [HttpPut]
        [ActionName("updateTechnicalSupportInteraction")]
        public object updateTechnicalSupportInteraction(int param, technicalSupportInteractionObject technicalSupportInteractionobj)
        {
            TECHNICALSUPPORTUSERACTION updateTechnicalSupportInteraction = db.TECHNICALSUPPORTUSERACTIONs.Find(param);

            updateTechnicalSupportInteraction.ACTIONDESCRIPTION = technicalSupportInteractionobj.actionDescription;
            updateTechnicalSupportInteraction.RESULT = technicalSupportInteractionobj.result;
            updateTechnicalSupportInteraction.COMPLETEPERCENTAGE = technicalSupportInteractionobj.completePercentage;
            updateTechnicalSupportInteraction.COMPLETEDDATE = technicalSupportInteractionobj.completedDate;
            updateTechnicalSupportInteraction.UPDATEDBY = GetCurrentNTUserId();
            updateTechnicalSupportInteraction.UPDATEDDATE = DateTime.Now;



            var tsa = db.USERASSIGNMENTs.Where(x=>x.TECHNICALSUPPORTID== updateTechnicalSupportInteraction.USERASSIGNMENT.TECHNICALSUPPORTID)
                .Join(db.TECHNICALSUPPORTUSERACTIONs,n=>n.OID,m=>m.USERASSIGNMENTID, (n,m) => m);
            var status="";
            double? statusVal = 0;
            foreach(var n in tsa)
            {

                if (n.COMPLETEPERCENTAGE.HasValue) statusVal = statusVal + n.COMPLETEPERCENTAGE;
            }
            statusVal = (statusVal / tsa.Count());
            if (statusVal < 100 && statusVal > 0)
            {
                status = constants.STATUSIPROM_INPROGRESS;
            }
            else
            if (statusVal.Value == 0) status = constants.STATUSIPROM_NOTSTARTED;
            else
            if (statusVal.Value == 100) status = constants.STATUSIPROM_COMPLETED;
            updateTechnicalSupportInteraction.USERASSIGNMENT.TECHNICALSUPPORT.STATUS = status;
            db.Entry(updateTechnicalSupportInteraction).State = EntityState.Modified;
            db.SaveChanges();

            return Ok(listEngineerDesignerByTechnicalSupportId(param, "1"));
        }

        [HttpGet]
        [ActionName("listEmployeeReportTechnicalSupport")]
        public object listEmployeeReportTechnicalSupport(string param)
        {
            var userInTS = db.USERASSIGNMENTs.Where(w => w.TECHNICALSUPPORTID.HasValue && 
                            (   (param == "ALL" && (w.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_ENGINEER || w.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_DESIGNER)) 
                                || (param == constants.TECHNICAL_SUPPORT_ENGINEER && w.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_ENGINEER)
                                || (param == constants.TECHNICAL_SUPPORT_DESIGNER && w.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_DESIGNER))).
                Select(s => new
                {
                    roleLevel = s.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_ENGINEER ? "2" : s.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_DESIGNER ? "4" : "0",
                    roleName = s.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_ENGINEER ? "Engineer" : s.ASSIGNMENTTYPE == constants.TECHNICAL_SUPPORT_DESIGNER ? "Designer" : "-",
                    employeeId = s.EMPLOYEEID,
                    employeeName = s.EMPLOYEENAME
                });

            var result = userInTS.Select(d => new { d.roleLevel, d.roleName }).Distinct().Select(s => new
            {
                roleLevel = s.roleLevel,
                roleName = s.roleName,
                detail = userInTS.Where(w => w.roleLevel == s.roleLevel).
                            Select(s1 => new
                            {
                                employeeid = s1.employeeId,
                                employeename = s1.employeeName
                            }).Distinct()
            });

            return result;
        }


    }
}