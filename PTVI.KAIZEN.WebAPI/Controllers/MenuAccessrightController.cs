﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MenuAccessrightController : KAIZENController
    {
        // GET api/MenuAccessright
        public IEnumerable<MENUACCESSRIGHT> GetMENUACCESSRIGHTs()
        {
            var menuaccessrights = db.MENUACCESSRIGHTs.Include(m => m.ACCESSRIGHT).Include(m => m.MENU);
            return menuaccessrights.AsEnumerable();
        }

        // GET api/MenuAccessright/5
        public MENUACCESSRIGHT GetMENUACCESSRIGHT(int id)
        {
            MENUACCESSRIGHT menuaccessright = db.MENUACCESSRIGHTs.Find(id);
            if (menuaccessright == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return menuaccessright;
        }

        // PUT api/MenuAccessright/5
        public HttpResponseMessage PutMENUACCESSRIGHT(int id, MENUACCESSRIGHT menuaccessright)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != menuaccessright.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(menuaccessright).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/MenuAccessright
        public HttpResponseMessage PostMENUACCESSRIGHT(MENUACCESSRIGHT menuaccessright)
        {
            if (ModelState.IsValid)
            {
                db.MENUACCESSRIGHTs.Add(menuaccessright);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, menuaccessright);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = menuaccessright.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/MenuAccessright/5
        public HttpResponseMessage DeleteMENUACCESSRIGHT(int id)
        {
            MENUACCESSRIGHT menuaccessright = db.MENUACCESSRIGHTs.Find(id);
            if (menuaccessright == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.MENUACCESSRIGHTs.Remove(menuaccessright);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, menuaccessright);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}