﻿using Microsoft.Office.Project.Server.Library;
using Microsoft.ProjectServer.Client;
using Microsoft.SharePoint.Client;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTVI.KAIZEN.WebAPI.PSI_CustomFieldWS;
using PTVI.KAIZEN.WebAPI.PSI_ProjectWS;
using PTVI.KAIZEN.WebAPI.PSI_QueueWS;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Services.Protocols;
using PTVI.KAIZEN.WebAPI.PSI_CalendarWS;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ShiftTargetBaselineUpdateController: KAIZENController
    {       
        [HttpGet]
        [ActionName("DoUpdate")]
        public IEnumerable<PROJECTTASKBYSHIFT> DoUpdate(int param)
        {
            var projectShiftID = param;
            VALE_KAIZENEntities db = new VALE_KAIZENEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            Guid sessionUid = Guid.NewGuid();
            try
            {
                ProjectWS.CheckOutProject(projectUid, sessionUid, "Calculate shift target Plan");
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                var hasCheckedOut = false;
                var message = "";
                foreach (var err in errors)
                {
                    if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutInOtherSession)
                    {
                        message = "You have checkout this project in another session. Please checkin it and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutToOtherUser)
                    {
                        message = "Other user has checkout this project. Please wait it is checked in and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOAlreadyCheckedOutInSameSession ||
                        err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCannotCheckOutVisibilityModeProjectWithMppInDocLib)
                    {
                        message = "This project has been checkout. Please wait it is checked in and try again";
                        hasCheckedOut = true;
                    }
                }

                if (hasCheckedOut)
                    throw new ApplicationException(message);
                else
                    throw exc;
            }

            var projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var projectRow = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow);
            var projectName = projectRow.PROJ_NAME;

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);

            try
            {
                // set to zero
                //var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var leave in excludeLeaves)
                {
                    var t = leave as ProjectDataSet.TaskRow;

                    // set start and end equal date to make it value 0
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                }

                //var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TASK_START_DATE < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var row in includedLeaves)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        //filterStartDateRows[0].DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START; // use this to calculate baseline duration
                        filterStartDateRows[0].DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                        //newStartDateCFRow.DATE_VALUE = taskRow.IsTASK_ACT_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TASK_ACT_START;
                        newStartDateCFRow.DATE_VALUE = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                    }


                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                        endStartDateCFRow.DATE_VALUE = shiftTargetDate.Value;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);
                    }
                }

                if (projectDs.TaskCustomFields.GetChanges() != null && projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                {
                    // publish project to appy calculation
                    Guid jobUid = Guid.NewGuid();
                    var changesDS = projectDs.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                    ProjectWS.QueueUpdateProject2(jobUid, sessionUid, changesDS, false);

                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate baseline");

                    WaitForQueue(q, jobUid);
                }
                else
                {

                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate baseline");

                    WaitForQueue(q, jobUid);
                }

                calculateProgressByDate(ProjectWS, projectDs, projectUid, projectShiftID);


                return db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).AsEnumerable();
            }
            catch (Exception exc)
            {
                try
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }
                catch { };

                throw exc;
            }
        }
       
        private void calculateProgressByDate(PSI_ProjectWS.Project ProjectWS,  ProjectDataSet projectDs, Guid projectId, int projectShiftID)
        {
            var projectRow = (projectDs.Project.Select("PROJ_UID='" + projectId + "'")[0] as ProjectDataSet.ProjectRow);

            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            Guid cfTotalDurationUID = new Guid(cfTotalDuration);
            Guid durationCFUid = new Guid(cfCalcPlanDurationUid);
        
            // update percentCompleteBaseline for all task and its parents
            var projectTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();

            foreach (var task in projectTasks)
            {
                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + task.TASKUID + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + task.TASKUID + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        var taskRow = projectDs.Task.Select("TASK_UID = '" + task.TASKUID + "'").FirstOrDefault() as ProjectDataSet.TaskRow;

                        // update projecttaskbyshift
                        task.PERCENTCOMPLETEBASELINE = percentComplete > 100 ? 100 : Convert.ToInt32(percentComplete);

                        db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                    }
                    catch { }
                }
            }

            db.SaveChanges();
        }

        static private void WaitForQueue(QueueSystem q, Guid jobId)
        {
            PSI_QueueWS.JobState jobState;
            const int QUEUE_WAIT_TIME = 2; // two seconds
            bool jobDone = false;
            string xmlError = string.Empty;
            int wait = 0;

            // Wait for the project to get through the queue.
            // Get the estimated wait time in seconds.
            wait = q.GetJobWaitTime(jobId);

            // Wait for it.
            System.Threading.Thread.Sleep(wait * 1000);
            // Wait until it is finished.

            do
            {
                // Get the job state.
                jobState = q.GetJobCompletionState(jobId, out xmlError);

                if (jobState == PSI_QueueWS.JobState.Success)
                {
                    jobDone = true;
                }
                else
                {
                    if (jobState == PSI_QueueWS.JobState.Unknown
                    || jobState == PSI_QueueWS.JobState.Failed
                    || jobState == PSI_QueueWS.JobState.FailedNotBlocking
                    || jobState == PSI_QueueWS.JobState.CorrelationBlocked
                    || jobState == PSI_QueueWS.JobState.Canceled)
                    {
                        // If the job failed, error out.
                        throw (new ApplicationException("Queue request " + jobState + " for Job ID " + jobId + ".\r\n" + xmlError));
                    }
                    else
                    {
                        Console.WriteLine("Job State: " + jobState + " for Job ID: " + jobId);
                        System.Threading.Thread.Sleep(QUEUE_WAIT_TIME * 1000);
                    }
                }
            }
            while (!jobDone);
        }

        private Guid CheckOutProject(PSI_ProjectWS.Project ProjectWS, Guid projectUid)
        {
            Guid sessionUid = Guid.NewGuid();
            try
            {
                ProjectWS.CheckOutProject(projectUid, sessionUid, "Calculate shift target Plan");
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                var hasCheckedOut = false;
                var message = "";
                foreach (var err in errors)
                {
                    if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutInOtherSession)
                    {
                        message = "You have checkout this project in another session. Please checkin it and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCheckedOutToOtherUser)
                    {
                        message = "Other user has checkout this project. Please wait it is checked in and try again.";
                        hasCheckedOut = true;
                    }
                    else if (err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOAlreadyCheckedOutInSameSession ||
                        err.ErrId == Microsoft.Office.Project.Server.Library.PSErrorID.CICOCannotCheckOutVisibilityModeProjectWithMppInDocLib)
                    {
                        message = "This project has been checkout. Please wait it is checked in and try again";
                        hasCheckedOut = true;
                    }
                }

                if (hasCheckedOut)
                    throw new ApplicationException(message);
                else
                    throw exc;
            }

            return sessionUid;
        }

        [HttpGet]
        [ActionName("TestDoUpdate")]
        public List<object> TestDoUpdate(int param)
        {
            var projectShiftID = param;
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;

            PSI_CalendarWS.Calendar calendar = new PSI_CalendarWS.Calendar();
            calendar.Credentials = pwaCredential;

            var calendarDS = calendar.ListCalendars();

            PSI_CustomFieldWS.CustomFields cf = new CustomFields();
            cf.Credentials = pwaCredential;

            Guid sessionUid = CheckOutProject(ProjectWS, projectUid);

            var projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);


            var projectRow = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow);
            var projectName = projectRow.PROJ_NAME;

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);
            //var cfTaskCalendarUid = new Guid(cfTaskCalendar);

            var calendarList = ReadCalendarException(calendar);
            CalendarExceptionObject projectCalendarExceptionObject = null;
            if (!projectRow.IsCAL_UIDNull())
            {
                projectCalendarExceptionObject = calendarList.Where(x => x.Uid == projectRow.CAL_UID).FirstOrDefault();
            }

            try
            {
                // set to zero
                var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var leave in excludeLeaves)
                {
                    var t = leave as ProjectDataSet.TaskRow;

                    // set start and end equal date to make it value 0
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    //var filterPlanDurationResultRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                    //                                "MD_PROP_UID = '" + cfCalcPlanDurationResult + "'");
                    //if (filterPlanDurationResultRows.Length > 0)
                    //    filterPlanDurationResultRows[0].NUM_VALUE = 0;
                }

                var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var row in includedLeaves)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;

                    var startDate = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                    var endDate = taskRow.IsTB_FINISHNull() ? taskRow.TASK_FINISH_DATE : taskRow.TB_FINISH;

                    if (shiftTargetDate.Value < endDate)
                        endDate = shiftTargetDate.Value;

                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = startDate;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                        newStartDateCFRow.DATE_VALUE = startDate;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                    }


                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = endDate;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                        endStartDateCFRow.DATE_VALUE = endDate;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);
                    }
                }

                if (projectDs.TaskCustomFields.GetChanges() != null && projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueUpdateProject(jobUid, sessionUid, projectDs, false);
                    WaitForQueue(q, jobUid);

                    // calculate duration
                    var ENTITY_TYPE_CUSTOMFIELD = 64;
                    PSI_ProjectWS.ProjectDataSet projectDs2 =
                        ProjectWS.ReadProjectEntities(projectUid, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

                    foreach (var row in includedLeaves)
                    {
                        var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;

                        var startDate = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                        var endDate = taskRow.IsTB_FINISHNull() ? taskRow.TASK_FINISH_DATE : taskRow.TB_FINISH;

                        if (shiftTargetDate.Value < endDate)
                            endDate = shiftTargetDate.Value;

                        CalendarExceptionObject calendarExceptionObject = null;
                        if (!taskRow.IsTASK_CAL_UIDNull())
                        {
                            calendarExceptionObject = calendarList.Where(x => x.Uid == taskRow.TASK_CAL_UID).FirstOrDefault();
                        }


                        decimal planDuration = 0;
                        if (!taskRow.IsTASK_CAL_UIDNull() && calendarExceptionObject.WorkingHours.Count > 0)
                        {
                            planDuration = (decimal)calculateDuration(startDate, endDate, calendarExceptionObject);
                        }
                        else
                        {
                            // get from formula field
                            var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID.ToString() + "' and " +
                                "MD_PROP_UID = '" + cfCalcPlanDurationUid + "'");

                            planDuration = shiftWorkDurationRows[0].NUM_VALUE;
                        }

                        //var filterPlanDurationResultRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                        //                            "MD_PROP_UID = '" + cfCalcPlanDurationResult + "'");

                        //if (filterPlanDurationResultRows.Length > 0)
                        //{
                        //    filterPlanDurationResultRows[0].NUM_VALUE = planDuration;
                        //}
                        //else
                        //{
                        //    PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow planDurationResultNewRows = projectDs2.TaskCustomFields.NewTaskCustomFieldsRow();
                        //    planDurationResultNewRows.PROJ_UID = taskRow.PROJ_UID;
                        //    planDurationResultNewRows.TASK_UID = taskRow.TASK_UID;
                        //    planDurationResultNewRows.CUSTOM_FIELD_UID = Guid.NewGuid();
                        //    //planDurationResultNewRows.MD_PROP_UID = new Guid(cfCalcPlanDurationResult);

                        //    planDurationResultNewRows.NUM_VALUE = planDuration;

                        //    projectDs2.TaskCustomFields.AddTaskCustomFieldsRow(planDurationResultNewRows);
                        //}
                    }

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueUpdateProject(jobUid, sessionUid, projectDs2, false);
                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueuePublish(jobUid, projectUid, true, null);
                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");
                    WaitForQueue(q, jobUid);                   
                }
                else
                {

                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }

                var result = testCalculateProgressByDate(ProjectWS, projectDs, projectUid, projectShiftID, calendarDS);

                return result;
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                Guid jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                WaitForQueue(q, jobUid);

                throw exc;
            }
            catch (Exception exc)
            {
                try
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }
                catch { };

                throw exc;
            }
        }

        

        private double extractCalendarWorkingHour(string calendarName)
        {
            var a = new string(calendarName.Where(x => char.IsNumber(x)).ToArray());

            return double.Parse(a);
        }

        private List<object> deleteFormulaCustomFields(PSI_ProjectWS.Project ProjectWS, Guid projectId, PSI_QueueWS.QueueSystem q)
        {
            var sessionUid = CheckOutProject(ProjectWS, projectId);

            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            var result = new List<object>();

            var shiftWorkDurationRows2 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])
                projectDs2.TaskCustomFields.Select("MD_PROP_UID = '6a975f17-4cc7-e711-80e2-005056900d63'");

            foreach (var cf in shiftWorkDurationRows2)
            {
                (cf as ProjectDataSet.TaskCustomFieldsRow).Delete();                
            }

            if (projectDs2.TaskCustomFields.GetChanges() != null && projectDs2.TaskCustomFields.GetChanges().Rows.Count > 0)
            {
                var changesDS = projectDs2.GetChanges() as PSI_ProjectWS.ProjectDataSet;
                Guid jobUid = Guid.NewGuid();
                ProjectWS.QueueUpdateProject(jobUid, sessionUid, changesDS, false);
                WaitForQueue(q, jobUid);

                jobUid = Guid.NewGuid();
                ProjectWS.QueuePublish(jobUid, projectId, true, null);
                WaitForQueue(q, jobUid);

                jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectId, false, sessionUid, "Calculate shift target plan");
                WaitForQueue(q, jobUid);

            }

            return result;
        }

        private List<object> testCalculateProgressByDate(PSI_ProjectWS.Project ProjectWS, ProjectDataSet projectDs, Guid projectId, int projectShiftID, PSI_CalendarWS.CalendarDataSet calendarDS)
        {
            //Guid sessionUid = Guid.NewGuid();
            //ProjectWS.CheckOutProject(projectId, sessionUid, "test read custom field");
            var projectRow = (projectDs.Project.Select("PROJ_UID='" + projectId + "'")[0] as ProjectDataSet.ProjectRow);

            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);


            Guid cfTotalDurationUID = new Guid(cfTotalDuration);
            Guid durationCFUid = new Guid(cfCalcPlanDurationUid);

            //// update percentCompleteBaseline for all task and its parents
            //var projectTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();
            var projectCalendarName = "";
            var projetCalendarNameRows = calendarDS.Calendars.Select("CAL_UID = '" + projectRow.CAL_UID + "'");
            if (projetCalendarNameRows.Length > 0)
                projectCalendarName = (projetCalendarNameRows[0] as PSI_CalendarWS.CalendarDataSet.CalendarsRow).CAL_NAME;

            var result = new List<object>();

            foreach (var task in projectDs.Task.Rows)
            {
                var t = task as ProjectDataSet.TaskRow;

                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                var shiftWorkDurationRows2 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                            "MD_PROP_UID = '6a975f17-4cc7-e711-80e2-005056900d63'");

                //var shiftWorkDurationRows3 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '" + cfCalcPlanDurationResult + "'");

                //var percentCompletePlanRows3 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '3e7566ba-bdc7-e711-80e2-005056900d63'");

                //var isCalendar16Rows3 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '0fa2807a-c3c7-e711-80e2-005056900d63'");

                //var baselineDurationFormulaRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '514911d3-48c8-e711-80e2-005056900d63'");

                var calendarName = "-";
                if (!t.IsTASK_CAL_UIDNull())
                {
                    var calendarNameRows = calendarDS.Calendars.Select("CAL_UID = '" + t.TASK_CAL_UID + "'");
                    if (calendarNameRows.Length > 0)
                        calendarName = (calendarNameRows[0] as PSI_CalendarWS.CalendarDataSet.CalendarsRow).CAL_NAME;
                }

                //var calendarNameRows2 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '" + cfTaskCalendar + "'");

                //var baseDurationFRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '26969d13-50c8-e711-80e2-005056900d63'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        //var taskRow = projectDs.Task.Select("TASK_UID = '" + task.TASKUID + "'").FirstOrDefault() as ProjectDataSet.TaskRow;

                        // update projecttaskbyshift
                        //task.PERCENTCOMPLETEBASELINE = percentComplete > 100 ? 100 : Convert.ToInt32(percentComplete);

                        //db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                        result.Add(new
                        {
                            taskName = t.TASK_NAME,
                            shiftWorkDuration = (double)shiftWorkDurationRows[0].NUM_VALUE,
                            shiftWorkDuration2 = (double)shiftWorkDurationRows2[0].NUM_VALUE,
                            //shiftWorkDuration3 = (double)shiftWorkDurationRows3[0].NUM_VALUE,
                            //shiftWorkDuration3Fmt = shiftWorkDurationRows3[0].IsDUR_FMTNull() ? -1 : shiftWorkDurationRows3[0].DUR_FMT,
                            durationSummary = taskDur,
                            //percentCompleteBaseline1 = percentComplete,
                            percentCompleteOriginal = ((double)shiftWorkDurationRows[0].NUM_VALUE / taskDur) * 100.0,
                            percentCompleteBaseline2 = ((double)shiftWorkDurationRows2[0].NUM_VALUE / taskDur) * 100.0,
                            calendarUid = t.IsTASK_CAL_UIDNull() ? "-" : t.TASK_CAL_UID.ToString(),
                            //calendar16 = isCalendar16Rows3[0].TEXT_VALUE,
                            calendar = calendarName,
                            projectCalendar = projectCalendarName,
                            //calendarCFName = calendarNameRows2[0].TEXT_VALUE,
                            isIgnore = t.IsTASK_IGNORES_RES_CALNull() ? false : t.TASK_IGNORES_RES_CAL,
                            //baselineByF = baselineDurationFormulaRows[0].NUM_VALUE,
                            //baselineByFDur = baseDurationFRows[0].DUR_VALUE,
                            //baselineByFDurFMT = baseDurationFRows[0].IsDUR_FMTNull() ? -1 : baseDurationFRows[0].DUR_FMT
                        });

                    }
                    catch(Exception exc)
                    {
                        result.Add(new
                        {
                            errorMessage = exc.Message,
                            stackTrace = exc.StackTrace
                        });
                    }
                }
            }

            //Guid jobUid = Guid.NewGuid();
            //ProjectWS.QueueCheckInProject(jobUid, projectId, false, sessionUid, "Finish test read custom field");

            return result;
        }

        private List<object> testCalculateProgressByDate2(PSI_ProjectWS.Project ProjectWS, ProjectDataSet projectDs, Guid projectId, int projectShiftID)
        {
            //Guid sessionUid = Guid.NewGuid();
            //ProjectWS.CheckOutProject(projectId, sessionUid, "test read custom field");
            var projectRow = (projectDs.Project.Select("PROJ_UID='" + projectId + "'")[0] as ProjectDataSet.ProjectRow);

            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectId, ENTITY_TYPE_CUSTOMFIELD, PSI_ProjectWS.DataStoreEnum.WorkingStore);


            Guid cfTotalDurationUID = new Guid(cfTotalDuration);
            Guid durationCFUid = new Guid(cfCalcPlanDurationUid);

            var result = new List<object>();

            foreach (var task in projectDs.Task.Rows)
            {
                var t = task as ProjectDataSet.TaskRow;

                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                var shiftWorkDurationRows2 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                            "MD_PROP_UID = '6a975f17-4cc7-e711-80e2-005056900d63'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        result.Add(new
                        {
                            taskName = t.TASK_NAME,
                            shiftWorkDuration = (double)shiftWorkDurationRows[0].NUM_VALUE,
                            shiftWorkDuration2 = (double)shiftWorkDurationRows2[0].NUM_VALUE,
                            durationSummary = taskDur,
                            percentCompleteOriginal = ((double)shiftWorkDurationRows[0].NUM_VALUE / taskDur) * 100.0,
                            percentCompleteBaseline2 = ((double)shiftWorkDurationRows2[0].NUM_VALUE / taskDur) * 100.0,
                            isIgnore = t.IsTASK_IGNORES_RES_CALNull() ? false : t.TASK_IGNORES_RES_CAL,
                        });

                    }
                    catch (Exception exc)
                    {
                        result.Add(new
                        {
                            errorMessage = exc.Message,
                            stackTrace = exc.StackTrace
                        });
                    }
                }
            }

            return result;
        }

        [HttpGet]
        [ActionName("DurationPlanCustomField")]
        public List<object> DurationPlanCustomField(int param)
        {
            //Guid sessionUid = Guid.NewGuid();
            //ProjectWS.CheckOutProject(projectId, sessionUid, "test read custom field");
            var projectShiftID = param;
            //iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            //var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            //var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            PSI_CalendarWS.Calendar calendar = new PSI_CalendarWS.Calendar();
            calendar.Credentials = pwaCredential;

            var calendarDS = calendar.ListCalendars();

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);
            //var projectUid = new Guid("3d430d0f-93c7-e711-a1f3-dada763914a5");

            // read calcuation result
            var ENTITY_TYPE_CUSTOMFIELD = 64;
            var ENTITY_TYPE_TASK = 2;
            var ENTITY_TYPE_PROJECT = 1;
            PSI_ProjectWS.ProjectDataSet projectDs2 =
                ProjectWS.ReadProjectEntities(projectUid, ENTITY_TYPE_CUSTOMFIELD | ENTITY_TYPE_TASK | ENTITY_TYPE_PROJECT, PSI_ProjectWS.DataStoreEnum.WorkingStore);

            //PSI_ProjectWS.ProjectDataSet projectDs2 =
            //    ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.PublishedStore);


            //Guid cfTotalDurationUID = new Guid(cfTotalDuration);
            //Guid durationCFUid = new Guid(cfCalcPlanDurationUid);

            Guid cfTotalDurationUID = new Guid("1cbf9e3b-e393-e711-80e2-005056900d63");
            Guid durationCFUid = new Guid("97685582-4726-e711-80e0-005056900d63");

            //// update percentCompleteBaseline for all task and its parents
            //var projectTasks = db.PROJECTTASKBYSHIFTs.Where(x => x.ESTIMATIONSHIFTID == projectShiftID).ToList();

            var result = new List<object>();

            var projectRow = (projectDs2.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow);

            var projectCalendarName = "";
            var projetCalendarNameRows = calendarDS.Calendars.Select("CAL_UID = '" + projectRow.CAL_UID + "'");
            if (projetCalendarNameRows.Length > 0)
                projectCalendarName = (projetCalendarNameRows[0] as PSI_CalendarWS.CalendarDataSet.CalendarsRow).CAL_NAME;


            foreach (var task in projectDs2.Task.Rows)
            {
                var t = task as ProjectDataSet.TaskRow;

                var taskDur = 0.0;
                var totalDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                    "MD_PROP_UID = '" + cfTotalDurationUID + "'");
                if (totalDurationRows.Length > 0)
                    taskDur = (double)totalDurationRows[0].NUM_VALUE;

                var shiftWorkDurationRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                            "MD_PROP_UID = '" + durationCFUid + "'");

                var shiftWorkDurationRows2 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                            "MD_PROP_UID = '6a975f17-4cc7-e711-80e2-005056900d63'");

                //var shiftWorkDurationRows3 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '208fa086-95c7-e711-80e2-005056900d63'");

                //var isCalendar16Rows3 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '0fa2807a-c3c7-e711-80e2-005056900d63'");


                //var percentCompletePlanRows3 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '3e7566ba-bdc7-e711-80e2-005056900d63'");

                //var baselineDurationFormulaRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '514911d3-48c8-e711-80e2-005056900d63'");


                var calendarName = "-";
                if (!t.IsTASK_CAL_UIDNull())
                {
                    var calendarNameRows = calendarDS.Calendars.Select("CAL_UID = '" + t.TASK_CAL_UID + "'");
                    if (calendarNameRows.Length > 0)
                        calendarName = (calendarNameRows[0] as PSI_CalendarWS.CalendarDataSet.CalendarsRow).CAL_NAME;
                }

                //var calendarNameRows2 = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '" + cfTaskCalendar + "'");

                //var baseDurationFRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs2.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID.ToString() + "' and " +
                //            "MD_PROP_UID = '26969d13-50c8-e711-80e2-005056900d63'");

                if (shiftWorkDurationRows.Length > 0)
                {
                    try
                    {
                        var percentComplete = 0.0;

                        if (taskDur == 0)
                            percentComplete = 100;
                        else
                        {
                            var calcDurationMinute = (double)shiftWorkDurationRows[0].NUM_VALUE;
                            percentComplete = (calcDurationMinute / taskDur) * 100.00;
                            if (percentComplete >= 100)
                                percentComplete = 100;
                            else if (percentComplete < 0)
                                percentComplete = 0;
                        }

                        //var taskRow = projectDs.Task.Select("TASK_UID = '" + task.TASKUID + "'").FirstOrDefault() as ProjectDataSet.TaskRow;

                        // update projecttaskbyshift
                        //task.PERCENTCOMPLETEBASELINE = percentComplete > 100 ? 100 : Convert.ToInt32(percentComplete);

                        //db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                        result.Add(new
                        {
                            taskName = t.TASK_NAME,
                            shiftWorkDuration = (double)shiftWorkDurationRows[0].NUM_VALUE,
                            shiftWorkDuration2 = (double)shiftWorkDurationRows2[0].NUM_VALUE,
                            //shiftWorkDuration3 = shiftWorkDurationRows3[0].DUR_VALUE,
                            //shiftWorkDuration3Fmt = shiftWorkDurationRows3[0].IsDUR_FMTNull() ? -1 : shiftWorkDurationRows3[0].DUR_FMT,
                            durationSummary = taskDur,
                            //percentCompleteBaseline1 = percentComplete,
                            percentCompleteOriginal = ((double)shiftWorkDurationRows[0].NUM_VALUE / taskDur) * 100.0,
                            percentCompleteBaseline2 = ((double)shiftWorkDurationRows2[0].NUM_VALUE / taskDur) * 100.0,
                            calendarUid = t.IsTASK_CAL_UIDNull() ? "-" : t.TASK_CAL_UID.ToString(),
                            //calendar16 = isCalendar16Rows3[0].TEXT_VALUE,
                            calendar = calendarName,
                            projectCalendar = projectCalendarName,
                            //calendarCFName = calendarNameRows2[0].TEXT_VALUE,
                            isIgnore = t.IsTASK_IGNORES_RES_CALNull() ? false : t.TASK_IGNORES_RES_CAL,
                            //baselineByF = baselineDurationFormulaRows[0].NUM_VALUE,
                            //baselineByFDur = baseDurationFRows[0].DUR_VALUE,
                            //baselineByFDurFMT = baseDurationFRows[0].IsDUR_FMTNull() ? -1 : baseDurationFRows[0].DUR_FMT
                        });

                    }
                    catch (Exception exc) {
                        result.Add(new
                        {
                            taskName = t.TASK_NAME,
                            errorMessage = exc.Message,
                            stackTrace = exc.StackTrace
                        });
                    }
                }
            }

            //Guid jobUid = Guid.NewGuid();
            //ProjectWS.QueueCheckInProject(jobUid, projectId, false, sessionUid, "Finish test read custom field");

            return result;
        }

        [HttpGet]
        [ActionName("TestDoUpdate2")]
        public List<object> TestDoUpdate2(int param)
        {
            var projectShiftID = param;
            iPROMEntities db = new iPROMEntities();
            var projectShift = db.PROJECTSHIFTs.Find(projectShiftID);
            if (projectShift == null)
                throw new ApplicationException("Project shift not found for id " + projectShiftID);

            var prjName = projectShift.PROJECT.PROJECTDESCRIPTION;

            var shiftTargetDate = projectShift.ENDDATE;

            var prjContext = new ProjectContext(pwaPath);
            prjContext.Credentials = pwaCredential;

            // get projectDs by name
            var credential = pwaCredential;
            PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
            ProjectWS.Credentials = credential;

            var projectUid = new Guid(projectShift.PROJECT.IPTPROJECTID);

            PSI_QueueWS.QueueSystem q = new QueueSystem();
            q.Credentials = pwaCredential;


            PSI_CustomFieldWS.CustomFields cf = new CustomFields();
            cf.Credentials = pwaCredential;

            Guid sessionUid = CheckOutProject(ProjectWS, projectUid);

            var projectDs =
                ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);


            var projectRow = (projectDs.Project.Select("PROJ_UID='" + projectUid + "'")[0] as ProjectDataSet.ProjectRow);
            var projectName = projectRow.PROJ_NAME;

            var cfPlanStartDateUid = new Guid(cfCalcPlanDurationStartDateUid);
            var cfPlanEndDateUid = new Guid(cfCalcPlanDurationEndDateUid);
            //var cfTaskCalendarUid = new Guid(cfTaskCalendar);

            try
            {
                // set to zero
                var excludeLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START >= '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var leave in excludeLeaves)
                {
                    var t = leave as ProjectDataSet.TaskRow;

                    // set start and end equal date to make it value 0
                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }

                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + t.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = shiftTargetDate.Value;
                    }
                }

                var includedLeaves = projectDs.Task.Select("TASK_IS_SUMMARY = 0 AND TB_START < '" + shiftTargetDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'");
                foreach (var row in includedLeaves)
                {
                    var taskRow = row as PSI_ProjectWS.ProjectDataSet.TaskRow;

                    var startDate = taskRow.IsTB_STARTNull() ? taskRow.TASK_START_DATE : taskRow.TB_START;
                    var endDate = taskRow.IsTB_FINISHNull() ? taskRow.TASK_FINISH_DATE : taskRow.TB_FINISH;

                    if (shiftTargetDate.Value < endDate)
                        endDate = shiftTargetDate.Value;

                    var filterStartDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                                    "MD_PROP_UID = '" + cfPlanStartDateUid + "'");
                    if (filterStartDateRows.Length > 0)
                    {
                        filterStartDateRows[0].DATE_VALUE = startDate;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow newStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        newStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        newStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        newStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        newStartDateCFRow.MD_PROP_UID = cfPlanStartDateUid;

                        newStartDateCFRow.DATE_VALUE = startDate;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(newStartDateCFRow);
                    }


                    var filterEndDateRows = (PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow[])projectDs.TaskCustomFields.Select("TASK_UID = '" + taskRow.TASK_UID + "' and " +
                                                "MD_PROP_UID = '" + cfPlanEndDateUid + "'");

                    if (filterEndDateRows.Length > 0)
                    {
                        filterEndDateRows[0].DATE_VALUE = endDate;
                    }
                    else
                    {
                        PSI_ProjectWS.ProjectDataSet.TaskCustomFieldsRow endStartDateCFRow = projectDs.TaskCustomFields.NewTaskCustomFieldsRow();
                        endStartDateCFRow.PROJ_UID = taskRow.PROJ_UID;
                        endStartDateCFRow.TASK_UID = taskRow.TASK_UID;
                        endStartDateCFRow.CUSTOM_FIELD_UID = Guid.NewGuid();
                        endStartDateCFRow.MD_PROP_UID = cfPlanEndDateUid;

                        endStartDateCFRow.DATE_VALUE = endDate;

                        projectDs.TaskCustomFields.AddTaskCustomFieldsRow(endStartDateCFRow);
                    }
                }

                if (projectDs.TaskCustomFields.GetChanges() != null && projectDs.TaskCustomFields.GetChanges().Rows.Count > 0)
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueUpdateProject2(jobUid, sessionUid, projectDs, false);
                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueuePublish(jobUid, projectUid, true, null);
                    WaitForQueue(q, jobUid);

                    jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");
                    WaitForQueue(q, jobUid);
                }
                else
                {

                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, false, sessionUid, "Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }

                var result = testCalculateProgressByDate2(ProjectWS, projectDs, projectUid, projectShiftID);

                return result;
            }
            catch (SoapException exc)
            {
                var error = new PSClientError(exc);
                var errors = error.GetAllErrors();

                Guid jobUid = Guid.NewGuid();
                ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                WaitForQueue(q, jobUid);

                throw exc;
            }
            catch (Exception exc)
            {
                try
                {
                    Guid jobUid = Guid.NewGuid();
                    ProjectWS.QueueCheckInProject(jobUid, projectUid, true, sessionUid, "Error handling Calculate shift target plan");

                    WaitForQueue(q, jobUid);
                }
                catch { };

                throw exc;
            }
        }
    }
}