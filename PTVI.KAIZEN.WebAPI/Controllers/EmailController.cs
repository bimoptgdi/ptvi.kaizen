﻿using PTGDI.Email;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Web;
using System.IO;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class EmailController : KAIZENController
    {
        // use web.config setting for email
        
        protected bool _debugEmailMode = Convert.ToBoolean(global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL);
        protected String _debugEmailTo = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL_TO;
        protected String _debugEmailCC = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL_CC;

        protected String _emailServices = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.EmailService;

        protected bool _sendEmailNotification = Convert.ToBoolean(global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.SEND_EMAIL); // untuk me non-aktifkan email set value menjadi false

        private string findWebAppUrl()
        {
            GENERALPARAMETER genParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == "WEB_APP_URL");
            if (genParam != null)
            {
                return genParam.VALUE;
            }
            else
            {
                return string.Empty;
            }
        }

        private bool isDataExist(List<string> arrData, string newData)
        {
            foreach (string oldData in arrData)
            {
                if (oldData == newData)
                {
                    return true;
                }
            }

            return false;
        }
        

        public String FindNameAndEmailByBadgeNo(string param)
        {
            String recipientName = "";
            String recipientEmail = "";

            // find originator email in table User
            USER user = db.USERs.FirstOrDefault(d => d.BADGENO == param);
            if (user != null)
            {

                recipientEmail = user.EMAIL;
                recipientName = user.USERNAME;
            }
            else
            {
                // cek dari ellipse
                AccountController accController = new AccountController();
                DataTable dt = accController.EmployeeData(param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    recipientEmail = dt.Rows[0]["EMAIL"].ToString();
                    recipientName = dt.Rows[0]["FULL_NAME"].ToString();
                }

            }

            return recipientName + "|" + recipientEmail;
        }

        [HttpGet]
        [ActionName("getEmailBody")]
        public object getEmailBody(string param) {
            return db.EMAILREDACTIONs.Select(s => new emailRedactionObject
            {
                id = s.OID,
                code = s.CODE,
                description = s.DESCRIPTION,
                subject = s.SUBJECT,
                body = s.BODY,
                isActive = s.ISACTIVE
            });
        }

        [HttpPost]
        [ActionName("createEmailBody")]
        public IHttpActionResult createEmailBody(emailRedactionObject emails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                EMAILREDACTION email = new EMAILREDACTION();
                email.CODE = emails.code;
                email.DESCRIPTION = emails.description;
                email.SUBJECT = emails.subject;
                email.BODY = emails.body;
                email.ISACTIVE = emails.isActive;
                email.CREATEDBY = GetCurrentNTUserId();
                email.CREATEDDATE = DateTime.Now;
                db.Entry(email).State = EntityState.Added;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }

            return Ok(emails);
        }

        [HttpPut]
        [ActionName("updateEmailBody")]
        public IHttpActionResult updateEmailBody(int param, emailRedactionObject emails) {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (param != emails.id)
            {
                return BadRequest();
            }

            try
            {
                EMAILREDACTION email = db.EMAILREDACTIONs.Find(param);
                email.CODE = emails.code;
                email.DESCRIPTION = emails.description;
                email.SUBJECT = emails.subject;
                email.BODY = emails.body;
                email.ISACTIVE = emails.isActive;
                email.UPDATEDBY = GetCurrentNTUserId();
                email.UPDATEDDATE = DateTime.Now;
                db.Entry(email).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EMAILREDACTIONExists(param))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(emails);
        }
        
        private bool EMAILREDACTIONExists(int id)
        {
            return db.EMAILREDACTIONs.Count(e => e.OID == id) > 0;
        }
        public string FindEmailBodyByCode(string code)
        {
            //IEnumerable<EMAILREDACTION> emailRed = db.EMAILREDACTIONs.Where(d => d.TITLE == code);
            //if (emailRed.Count() > 0)
            //{
            //    return emailRed.ElementAt(0).VALUE;
            //}

            return null;
        }

        public void SendEmail(string adrURL, string recipientName, string recipientEmail, string remark, string statusCategory)
        {
            // Define variable
            var strRecipient = string.Empty;
            var strRecipientCC = string.Empty;

            // 1. Prepare email engine
            EmailSender emailSender = new EmailSender();
            string emailFrom = System.Configuration.ConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];

            AccountController accController = new AccountController();

            // 2. Prepare email body
            string emailBody = FindEmailBodyByCode("EMAIL_GENERAL");
            if (!string.IsNullOrEmpty(emailBody))
            {

                // if debug mode is on
                // change recipient with email debug
                if (_debugEmailMode)
                {
                    strRecipient = _debugEmailTo;
                    strRecipientCC = _debugEmailCC;
                }

                emailSender.SendMail(strRecipient, strRecipientCC, emailFrom, "[GATEPASS-REMINDER] GATEPASS Request Need Your Respond", emailBody);

            }
            else
            {
                throw new Exception("E-mail body is not found in the system. Please contact your administrator");
            }

        }

        public String SendEmail(string key, string sender, string recipients, string cc, string bcc, string subject, string content, string attachmentInfo)
        {
            /*
             * sourceKey diisi EMLATTCH untuk file yang akan diattach dalam email
             * docID formatnya <docID><int>docID1</int><int>docID2</int></docID> --> docID masukkan sejumlah file yang akan diattach
             * */
            string soapRequest, xmlHttpRequest, sourceKey, isHTML, importance, srcURL;

            sourceKey = String.IsNullOrEmpty(key) ? "EMAIL" : key;// Application/client context

            isHTML = "Y";//                      'valid values: Y, N
            importance = "H";//         'valid values: H, N, L
            srcURL = _emailServices;
            //WriteLog(srcURL);
            //----
            soapRequest = "<?xml version='1.0' encoding='UTF-8'?>";
            soapRequest = soapRequest + "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>";
            soapRequest = soapRequest + "   <soap:Body>";
            soapRequest = soapRequest + "      <createEmail xmlns='http://tempuri.org/'>";
            soapRequest = soapRequest + "         <sourceKey>" + sourceKey + "</sourceKey>";
            soapRequest = soapRequest + "         <sender>" + sender + "</sender>";
            soapRequest = soapRequest + "         <recipients>" + recipients + "</recipients>";
            soapRequest = soapRequest + "         <cc>" + cc + "</cc>";
            soapRequest = soapRequest + "         <bcc>" + bcc + "</bcc>";
            soapRequest = soapRequest + "         <isHTML>" + isHTML + "</isHTML>";
            soapRequest = soapRequest + "         <importance>" + importance + "</importance>";
            soapRequest = soapRequest + "         <subject>" + subject + "</subject>";
            soapRequest = soapRequest + "         <body><![CDATA[" + content + "]]></body>";
            if (!string.IsNullOrEmpty(attachmentInfo))
            {
                soapRequest = soapRequest + "<registerAttachment>";
                //soapRequest = soapRequest + "         <docID>" + docID + "</docID>";
                soapRequest = soapRequest + attachmentInfo;
                soapRequest = soapRequest + "</registerAttachment>";
            }
            soapRequest = soapRequest + "      </createEmail>";
            soapRequest = soapRequest + "   </soap:Body>";
            soapRequest = soapRequest + "</soap:Envelope>";

            //WriteLog(soapRequest);
            //==
            xmlHttpRequest = PostContent(srcURL, soapRequest);

            //====
            return xmlHttpRequest;

        }

        public String SendEmailWithAttachment(string sender, string recipients, string cc, string bcc, string subject, string content, bool isInline, List<string> listRefDocID)
        {
            string attachmentInfo = "";
            if (listRefDocID.Count() > 0)
            {
                foreach (String refDocID in listRefDocID)
                {
                    //listDocID += "<int>" + refDocID + "</int>";
                    attachmentInfo += @"<attachmentInfo>
                                    <DocumentID>" + refDocID + @"</DocumentID>
                                    <isInline>" + (isInline ? "true" : "false") + @"</isInline>
                                    <cid>string</cid>
                                </attachmentInfo>";
                }
            }
            //WriteLog(attachmentInfo);

            //WriteLog("Send Email Mom Submit To: ");
            //WriteLog("sender: " + sender);
            //WriteLog("recipient: " + recipients);
            //WriteLog("content: " + content);
            //WriteLog("===============================");
            if (_debugEmailMode)
            {
                recipients = "fina@ptgdi.com;dewa@ptgdi.com";
            }
            

            return SendEmail("EMLATTCH", sender, recipients, cc, bcc, subject, content, attachmentInfo);
        }

        [HttpGet]
        [ActionName("SendSCurveGenerationFinishedEmail")]
        public bool SendSCurveGenerationFinishedEmail(int param)
        {
            try
            {
                var emailRedaction = db.EMAILREDACTIONs.Where(x => x.CODE == constants.EMAILREDACTION_SCURVEGENERATION_COMPLETED).FirstOrDefault();

                var scurveGeneration = db.SCURVEGENERATIONs.Find(param);
                var requester = db.USERs.Where(x=>x.NTUSERID == scurveGeneration.CREATEDBY).FirstOrDefault();

                if (requester == null)
                {
                    scurveGeneration.ERRORMESSAGE = "Requester data not found to get the email address (" + scurveGeneration.CREATEDBY + ")";
                    db.Entry(scurveGeneration).State = EntityState.Modified;
                    
                    db.SaveChanges();
                    //throw new ApplicationException("Requester data not found to get the email address");
                }

                var projectShift = db.PROJECTSHIFTs.Find(scurveGeneration.ESTIMATIONSHIFTID);
                var project = projectShift.PROJECT;

                var emailBody = emailRedaction.BODY
                                .Replace("[FULL_NAME]", requester.USERNAME)
                                .Replace("[SHIFT_NO]", projectShift.SHIFTID.ToString())
                                .Replace("[PROJECT_NAME]", project.PROJECTDESCRIPTION)
                                .Replace("[REFFID]", projectShift.OID.ToString())
                                .Replace("[BASE_URL]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl)
                                .Replace("[PROJECT_ID]", projectShift.PROJECTID.ToString());


                // find email information
                EmailSender emailSender = new EmailSender();
                string emailFrom = System.Configuration.ConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];

                emailSender.SendMail(requester.EMAIL, "", emailFrom, emailRedaction.SUBJECT, emailBody);

                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        
        }

        [HttpGet]
        [ActionName("TestSendEmail")]
        public HttpResponseMessage TestSendEmail(string param)
        {
            EmailSender emailSender = new EmailSender();
            emailSender.SendMail("anullah@gmail.com", "testing-only@vale.com", "Test Send Email method 1", "<h2>Email sent.. </h2>", MailPriority.High);

            emailSender.SendMail("anullah@gmail.com", "", "testing-only@vale.com", "Test Send Email method 2", "<h2>Email sent.. </h2>");

            var attachments = new List<Attachment>();
            var root = System.Web.HttpContext.Current.Server.MapPath("~/App_Data");

            var pathFile = System.IO.Path.Combine(root, "logout.png");
            attachments.Add(new Attachment(pathFile));

            //emailSender.SendMail("anullah@gmail.com", "", "testing-only@vale.com", "Test Send Email method 3", "<h2>Email sent.. </h2>", attachments);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
        public void GenerateMoMPDF(int param)
        {
            string webappurl = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl;

            webappurl = webappurl.Replace("/", "|") + "|TemplateEWRDetailForm.aspx?EWRID=" + param;

            string webapiurl = "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20";
            string ellipseurl = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.WebApiUrlEllipse;
            string pdfurl = ellipseurl + webapiurl + Uri.EscapeDataString(webappurl) + "/";
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            Uri uri = new Uri(pdfurl);

            string fileName = "EWR-" + param + ".pdf";
            string filePath = Path.Combine(root, fileName);

            using (var client = new WebClient())
            {
                client.DownloadFile(pdfurl, filePath);

                UploadController upload = new UploadController();
                upload.UploadFileInAppData(fileName, param, constants.DOC_EWR_MOM, "EWR Mom PDF", "herus");
                
            }
            WriteLog("PDF Generated");
        }
        //[HttpGet]
        //[ActionName("SendMomEmail")]
        public void SendMomEmail(int ewrID, string momDateTime, string emailAddresses, string emailCCAddresses) {
            // 1. Generate PDF and Upload
            GenerateMoMPDF(ewrID);

            // send Email MoM
            string emailFrom = System.Configuration.ConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];
            string emailBody = "Dear All, <br />Berikut hasil EWR Minutes of Meeting (" + momDateTime + "):<br/>";

            emailBody += GetMomEmailBody(ewrID);

            // ambil refdoc id documentnya
            List<string> listDocID = new List<string>();
            IQueryable<SUPPORTINGDOCUMENT> listEWRDoc = db.SUPPORTINGDOCUMENTs.Where(d => d.TYPEID == ewrID && d.TYPENAME == constants.DOC_EWR_MOM);
            if (listEWRDoc.Count() > 0)
            {
                foreach(SUPPORTINGDOCUMENT ewrDoc in listEWRDoc)
                {
                    listDocID.Add(ewrDoc.REFDOCID);
                }
            }

            SendEmailWithAttachment(emailFrom, emailAddresses, emailCCAddresses, "", "[iProM] EWR - Minutes of Meeting Results", emailBody, true, listDocID);
            
        }
        public void TestSendMomEmail(int ewrID, string emailAddresses, string emailCCAddresses)
        {
            EmailSender emailSender = new EmailSender();

            string emailFrom = System.Configuration.ConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];
            string emailBody = "Dear All, <br />Please review the result of EWR Minutes of Meeting below:<br/>";
            //emailBody += emailAddresses + "<br />";
            emailBody += GetMomEmailBody(ewrID);
            WriteLog(emailAddresses);
            WriteLog(emailBody);

            
            emailSender.SendMail("dewanta.widhipradja@ptgdi.com", emailCCAddresses, emailFrom, "Minutes of Meeting Results", emailBody);
        }
        public string GetMomEmailBody(int param)
        {
            EmailSender email = new EmailSender();
            //string connstrings = System.Configuration.ConfigurationManager.ConnectionStrings["iPROMEntities"].ConnectionString;
            ObjectParameter htmlOutput = new ObjectParameter("htmlOutput", typeof(string));
            db.SP_EWR_MOM_EMAIL_HTML(param, htmlOutput);

            if (htmlOutput != null && htmlOutput.Value != null)
            {
                return ((string)htmlOutput.Value);
            } else
            {
                return "";
            }
           
        }
        
        string PostContent(string url, string oRequest)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                byte[] requestBytes = System.Text.Encoding.ASCII.GetBytes(oRequest);
                req.Method = "POST";
                req.ContentType = "text/xml;charset=utf-8";
                req.ContentLength = requestBytes.Length;
                System.IO.Stream requestStream = req.GetRequestStream();
                requestStream.Write(requestBytes, 0, requestBytes.Length);
                requestStream.Close();

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                System.IO.StreamReader sr = new System.IO.StreamReader(res.GetResponseStream(), System.Text.Encoding.Default);
                string backstr = sr.ReadToEnd();


                sr.Close();
                res.Close();

                return backstr;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                throw new Exception("Failed to send email");
            }

        }
    }
}
