﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class CostReportController : ApiController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        [ActionName("ProjectServiceCostReport")]
        public object ProjectServiceCostReport(string param)
        {
            /*
            = IF(LEFT(TRIM($A12), 1) = "N",
                    SUMIF('CJI3-ActualCost'!$E:$E,$B12 & "*", 'CJI3-ActualCost'!BA:BA),
                    SUMIF('CJI3-ActualCost'!$E:$E,$B12, 'CJI3-ActualCost'!BA:BA)) +
            IF(LEFT(TRIM($A12), 1) = "N",
                    SUMIF('CJI5-Commitment-Prd'!$U:$U,$B12 & "*", 'CJI5-Commitment-Prd'!AQ:AQ),
                    SUMIF('CJI5-Commitment-Prd'!$U:$U,$B12, 'CJI5-Commitment-Prd'!AQ:AQ)) +
            IF($A$3 = 1,
                    IF(LEFT(TRIM($A12), 1) = "N",
                        SUMIF('Uncomitted Cost'!$B:$B,$B12 & "*", 'Uncomitted Cost'!W:W),
                        SUMIF('Uncomitted Cost'!$B:$B,$B12, 'Uncomitted Cost'!W:W)
                    ),
                    0) +
            SUMIF('CN52N-Reservation'!$C:$C,$B12, 'CN52N-Reservation'!AU:AU) +
            SUMIF(ContractSummary!$C:$C,$B12, ContractSummary!AE:AE)
            */
            var splitParam = param.Split('|');
            int projectId = int.Parse(splitParam[0]);
            int curYear = int.Parse(splitParam[1]);
            int currMonth = int.Parse(splitParam[2]);


            var elementNetwork = db.COSTREPORTELEMENTs.Where(s => s.PROJECTID == projectId).Select(s => new costReportObject
            {
                projectId = projectId,
                wbsLevelNo = s.WBSLEVEL.Value,
                wbsNo = s.WBSNO,
                wbsLevelDesc = s.WBSLEVEL.ToString(),
                costReportNo = s.WBSNO,
                description = s.DESCRIPTION,
                budgetTotal = null,//hanya di project
                actualCost = s.COSTREPORTACTUALCOSTs.Sum(s1 => s1.AMOUNT),
                allCommitment = s.COSTREPORTCOMMITMENTs.Sum(s1 => s1.AMOUNT),
                forecast = null,
                assignedCost = null,//actualCost+allCommitment
                budgetPercentage = null,//hanya di project
                remainingBudget = null,//hanya di project
                budgetCurrYear = null,//hanya di project
                actualCostCurrYear = s.COSTREPORTACTUALCOSTs.Where(w => w.YEAR == curYear).Sum(s1 => s1.AMOUNT),//bener ngak ya ambil dari sini
                commitmentCurrYear = s.COSTREPORTCOMMITMENTs.Where(w => w.YEAR == curYear).Sum(s1 => s1.AMOUNT),
                assignedCostCurrYear = null,//actualCostCurrYear+commitmentCurrYear
                budgetPercentageCurrYear = null,//hanya di project
                remainingBudgetCurrYear = null,//hanya di project
                totalForecastCurrYear = s.COSTREPORTCOMMITMENTs.Where(w => w.YEAR >= curYear).Where(w => !(w.MONTH <= currMonth && w.YEAR == curYear)).Sum(s1 => s1.AMOUNT),//pusing keneh timana nya? akhirnya ketemu hahaha
                curYear = curYear
            }).Union(db.COSTREPORTNETWORKs.Where(s => s.COSTREPORTELEMENT.PROJECTID == projectId).Select(s => new costReportObject
            {
                projectId = projectId,
                wbsLevelNo = s.COSTREPORTELEMENT.WBSLEVEL.Value,
                wbsNo = s.WBSNO,
                wbsLevelDesc = "Network",
                costReportNo = s.NETWORK,
                description = s.DESCRIPTION,
                budgetTotal = null,//hanya di project
                actualCost = (s.COSTREPORTELEMENT.COSTREPORTACTUALCOSTs.Where(w => w.NETWORK.StartsWith(s.NETWORK)).Sum(s1 => s1.AMOUNT)),
                allCommitment = (s.COSTREPORTELEMENT.COSTREPORTCOMMITMENTs.Where(w => w.NETWORK.StartsWith(s.NETWORK)).Sum(s1 => s1.AMOUNT)),
                forecast = null,
                assignedCost = null,//actualCost+allCommitment
                budgetPercentage = null,//hanya di project
                remainingBudget = null,//hanya di project
                budgetCurrYear = null,//hanya di project
                actualCostCurrYear = (s.COSTREPORTELEMENT.COSTREPORTACTUALCOSTs.Where(w => w.NETWORK.StartsWith(s.NETWORK) && w.YEAR == curYear).Sum(s1 => s1.AMOUNT)),
                commitmentCurrYear = (s.COSTREPORTELEMENT.COSTREPORTCOMMITMENTs.Where(w => w.NETWORK.StartsWith(s.NETWORK) && w.YEAR == curYear).Sum(s1 => s1.AMOUNT)),
                assignedCostCurrYear = null,//actualCostCurrYear+commitmentCurrYear
                budgetPercentageCurrYear = null,//hanya di project
                remainingBudgetCurrYear = null,//hanya di project
                totalForecastCurrYear = s.COSTREPORTELEMENT.COSTREPORTCOMMITMENTs.Where(w => w.YEAR >= curYear).Where(w => !(w.MONTH <= currMonth && w.YEAR == curYear)).Sum(s1 => s1.AMOUNT),//pusing keneh timana nya? akhirnya ketemu hahaha
                curYear = curYear
            }));

            var project = db.PROJECTs.Find(projectId);

            costReportObject resultProject = new costReportObject
            {
                projectId = projectId,
                wbsLevelNo = 0,
                wbsNo = project.PROJECTNO,
                wbsLevelDesc = "Prj. Def.",
                costReportNo = project.PROJECTNO,
                description = project.PROJECTDESCRIPTION,
                budgetTotal = project.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.PROJECTNO == project.PROJECTNO).Sum(s2 => s2.AMOUNT)),
                //actualCost = elementNetwork.Sum(sum => sum.actualCost),//sum totalActualCost element dan network
                actualCost = elementNetwork.Where(w => w.wbsLevelDesc != "Network").Sum(sum => sum.actualCost),//sum totalActualCost element dan network
                //1allCommitment = elementNetwork.Sum(sum => sum.allCommitment),//sum allCommitment element dan network
                allCommitment = elementNetwork.Where(w => w.wbsLevelDesc != "Network").Sum(sum => sum.allCommitment),//sum allCommitment element dan network
                forecast = (double)project.FORECASTSPENDINGs.Sum(d1 => d1.FORECASTVALUE),
                assignedCost = null,//actualCost+allCommitment
                budgetPercentage = null,//assignedCost/budgetTotal
                remainingBudget = null,//budgetTotal-assignedCost
                budgetCurrYear = project.COSTREPORTELEMENTs.Sum(s1 => s1.COSTREPORTBUDGETs.Where(w => w.YEAR == curYear && w.PROJECTNO == project.PROJECTNO).Sum(s2 => s2.AMOUNT)),//==> dari mana kamu itu
                actualCostCurrYear = elementNetwork.Where(w => w.wbsLevelDesc != "Network").Sum(sum => sum.actualCostCurrYear),//sum actualCostCurrYear element dan network
                commitmentCurrYear = elementNetwork.Where(w => w.wbsLevelDesc != "Network").Sum(sum => sum.commitmentCurrYear),//sum commitmentCurrYear element dan network
                assignedCostCurrYear = null,//actualCostCurrYear-commitmentCurrYear
                budgetPercentageCurrYear = null,//assignedCostCurrYear/budgetCurrYear
                remainingBudgetCurrYear = null,//budgetCurrYear+assignedCostCurrYear
                totalForecastCurrYear = elementNetwork.Where(w => w.wbsLevelDesc != "Network").Sum(sum => sum.totalForecastCurrYear),//pusing keneh timana nya? akhirnya ketemu hahaha
                curYear = curYear
            };

            var resume = elementNetwork.ToList();
            resume.Add(resultProject);

            var element = new {
                dataCostReport = resume.OrderBy(o => o.wbsNo).ThenBy(t => t.wbsLevelNo).ThenBy(t => t.wbsLevelDesc),
                min = db.COSTREPORTCOMMITMENTs.Where(w => w.COSTREPORTELEMENT.PROJECTID == projectId && w.YEAR == curYear && (w.COSTREPORTELEMENT.WBSNO == w.NETWORK || w.COSTREPORTELEMENT.COSTREPORTNETWORKs.Select(s=>s.NETWORK).Contains(w.NETWORK))).Min(m => m.MONTH),
                max = db.COSTREPORTCOMMITMENTs.Where(w => w.COSTREPORTELEMENT.PROJECTID == projectId && w.YEAR == curYear && (w.COSTREPORTELEMENT.WBSNO == w.NETWORK || w.COSTREPORTELEMENT.COSTREPORTNETWORKs.Select(s => s.NETWORK).Contains(w.NETWORK))).Max(m => m.MONTH)
            };
            List<object> result = new List<object>();
            result.Add(element);
            return result;
        }

        [HttpGet]
        [ActionName("getLastUpdate")]
        public DateTime? getLastUpdate(int param)
        {
            return db.COSTREPORTELEMENTs.Where(x => x.PROJECTID == param).Max(x => x.UPDATEDDATE > x.CREATEDDATE ? x.UPDATEDDATE : x.CREATEDDATE);
        }

    }

    public class costReportObject
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        public int projectId { get; set; }
        public int wbsLevelNo { get; set; }
        public string wbsLevelDesc { get; set; }
        public string wbsNo { get; set; }
        public string costReportNo { get; set; }
        public string description { get; set; }
        public double? budgetTotal { get; set; }
        public double? actualCost { get; set; }
        public double? allCommitment { get; set; }
        public double? forecast { get; set; }
        public double? assignedCost { get; set; }
        public double? budgetPercentage { get; set; }
        public double? remainingBudget { get; set; }
        public double? budgetCurrYear { get; set; }
        public double? actualCostCurrYear { get; set; }
        public double? commitmentCurrYear { get; set; }
        public double? assignedCostCurrYear { get; set; }
        public double? budgetPercentageCurrYear { get; set; }
        public double? remainingBudgetCurrYear { get; set; }
        public double? totalForecastCurrYear { get; set; }
        public int? curYear { get; set; }
        public int? minMonth {
            get
            {
                //return costReportActualCostMonths.Select(s =>s.month).Union(costReportCommitmentMonths.Select(s => s.month)).Min();
                return costReportActualCostMonths.Min(s => s.month) < costReportCommitmentMonths.Min(s => s.month) ? costReportActualCostMonths.Min(s => s.month) : costReportCommitmentMonths.Min(s => s.month);
            }
        }
        public int? maxMonth {
            get
            {
                //return costReportActualCostMonths.Select(s => s.month).Union(costReportCommitmentMonths.Select(s => s.month)).Max();
                return costReportActualCostMonths.Max(s => s.month) > costReportCommitmentMonths.Max(s => s.month) ? costReportActualCostMonths.Max(s => s.month) : costReportCommitmentMonths.Max(s => s.month);
            }
        }
        public virtual IEnumerable<costReportMonthlyObject> costReportActualCostMonths
        {
            get
            {
                if (wbsLevelNo == 0)
                {
                    return db.COSTREPORTACTUALCOSTs.Where(w => w.YEAR == curYear && w.COSTREPORTELEMENT.PROJECTID == projectId).GroupBy(g => new { g.YEAR, g.MONTH }).Select(s1 => new costReportMonthlyObject
                    {
                        year = s1.Key.YEAR,
                        month = s1.Key.MONTH,
                        amount = s1.Sum(sum => sum.AMOUNT)
                    });
                } else
                {
                    return db.COSTREPORTACTUALCOSTs.Where(w => w.YEAR == curYear && w.NETWORK == costReportNo).GroupBy(g => new { g.YEAR, g.MONTH }).Select(s1 => new costReportMonthlyObject
                    {
                        year = s1.Key.YEAR,
                        month = s1.Key.MONTH,
                        amount = s1.Sum(sum => sum.AMOUNT)
                    });
                }
            }
        }
        public virtual IEnumerable<costReportMonthlyObject> costReportCommitmentMonths
        {
            get
            {
                if (wbsLevelNo == 0)
                {
                    return db.COSTREPORTCOMMITMENTs.Where(w => w.YEAR == curYear && w.COSTREPORTELEMENT.PROJECTID == projectId).GroupBy(g => new { g.YEAR, g.MONTH }).Select(s1 => new costReportMonthlyObject
                    {
                        year = s1.Key.YEAR,
                        month = s1.Key.MONTH,
                        amount = s1.Sum(sum => sum.AMOUNT)
                    });
                }
                else
                {
                    return db.COSTREPORTCOMMITMENTs.Where(w => w.YEAR == curYear && w.NETWORK == costReportNo).GroupBy(g => new { g.YEAR, g.MONTH }).Select(s1 => new costReportMonthlyObject
                    {
                        year = s1.Key.YEAR,
                        month = s1.Key.MONTH,
                        amount = s1.Sum(sum => sum.AMOUNT)
                    });
                }
            }
        }
    }

    public class costReportMonthlyObject
    {
        public int? year { get; set; }
        public int? month { get; set; }
        public double? amount { get; set; }
    }
}