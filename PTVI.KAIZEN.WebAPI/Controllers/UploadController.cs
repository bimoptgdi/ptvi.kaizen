﻿using Excel;
using OfficeOpenXml;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class UploadController : KAIZENController
    {

        public class prmFile{
          public string typeName { get; set; }
            public string badgeNo { get; set; }
            public string projectID { get; set; }
            public uploadFile[] datas { get; set; }
        }
        public class uploadFile
        {
            public int closeoutDocumentId { get; set; }
            public string fileType { get; set; }
            public string fileName { get; set; }
            public string description { get; set; }
        }
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUpload()
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                int oid = -1;
                SUPPORTINGDOCUMENT sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                            //int typeId = int.Parse(provider.FormData.GetValues("typeId")[0]);
                            string typeName = provider.FormData.GetValues("typeName")[0];
                            string uploadedBy = provider.FormData.GetValues("badgeNo")[0];
                            int currProgrId = int.Parse(provider.FormData.GetValues("currProggId")[0]);
                            int projectId = int.Parse(provider.FormData.GetValues("projectId")[0]);
                            

                            string datas = provider.FormData.GetValues("datas")[0];
                            List<uploadFile> jSonDatas = JsonConvert.DeserializeObject<List<uploadFile>>(datas);

                            int setupCLoseOutDocId = jSonDatas[0].closeoutDocumentId;

                            //JavaScriptSerializer serializer = new JavaScriptSerializer();
                            //var output = serializer.Deserialize < Dictionary<string, dynamic>>(provider.FormData.GetValues("datas")[0]);
                            //var array = provider.FormData.GetValues("datas")[0];
                            //foreach (var x in array)
                            //{
                            //    if (x.=["fileName"] == newName)
                            //    {
                            //        break;
                            //    }
                            //}

                            // 2. simpan informasi file2 yang diupload ke dalam supporting document

                            CLOSEOUTDOCUMENT closeDoc = new CLOSEOUTDOCUMENT();
                            var setupCloseDoc = db.SETUPCLOSEOUTDOCDOCUMENTs.Where(x=>x.OID == setupCLoseOutDocId).FirstOrDefault();
                            var closeProgress = db.CLOSEOUTPROGRESSes.Find(currProgrId);
                            closeDoc.CLOSEOUTPROGRESSID = closeProgress.OID;
                            closeDoc.CLOSEOUTDOCUMENTID = setupCloseDoc.OID;
                            db.Entry(closeDoc).State = EntityState.Added;
                            //db.SaveChanges();

                            int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper() && x.TYPEID == closeDoc.OID).Max(y => y.SEQUENCENO));
                            sp = new SUPPORTINGDOCUMENT();
                            //sp.TYPEID = closeDoc.OID;
                            sp.TYPENAME = typeName.ToUpper();
                            sp.SEQUENCENO = maxSeqNo + 1;
                            sp.UPLOADEDBY = uploadedBy;
                            sp.TYPEDETAIL = jSonDatas[0].fileType;
                            sp.DESCRIPTION = jSonDatas[0].description;


                            if (provider.FormData.GetValues("year") != null)
                            {
                                int year = Convert.ToInt32(provider.FormData.GetValues("year")[0]);
                                sp.UPLOADEDDATE = new DateTime(year, 1, 1);
                            }
                            else
                            {
                                sp.UPLOADEDDATE = DateTime.Now;
                            }

                            sp.UPDATEDDATE = DateTime.Now;
                            sp.UPDATEDBY = GetCurrentNTUserId();

                            db.SUPPORTINGDOCUMENTs.Add(sp);
                            db.SaveChanges();

                            oid = sp.OID;

                            sp.FILENAME = theFileName.Replace(root, "").Replace("\\", ""); //buang url rootnya, jadi tinggal filenamenya saja
                            try
                            {
                                sp.REFDOCID = uploadDocumentToDocRepo(theFileName, newName, oid); // nanti diganti dari respon webservice vale untuk upload doc
                                if (sp.REFDOCID == null)
                                    throw new Exception();
                                                                
                            }
                            catch (Exception e)
                            {
                                //remove the new close out document row
                                CLOSEOUTDOCUMENT failedCod = db.CLOSEOUTDOCUMENTs.Find(closeDoc.OID);
                                db.CLOSEOUTDOCUMENTs.Remove(failedCod);
                                //remove the new supporting document row
                                SUPPORTINGDOCUMENT failedSp = db.SUPPORTINGDOCUMENTs.Find(oid);
                                db.SUPPORTINGDOCUMENTs.Remove(failedSp);
                                db.SaveChanges();
                                throw new Exception("Failed upload to document center");
                            }
                            CLOSEOUTDOCUMENT cd = db.CLOSEOUTDOCUMENTs.Find(closeDoc.OID);
                            cd.SUPPORTINGDOCUMENTID = sp.OID;
                            db.Entry(cd).State = EntityState.Modified;


                            db.SaveChanges();
                            // Trace.WriteLine("Server file path: " + file.LocalFileName);

                            // 3. delete file after sent to webservice vale
                            System.IO.File.Delete(System.IO.Path.Combine(root, theFileName));
                        }

                    }


                }
                catch (Exception ex)
                {
                    if(ex.Message != "Failed upload to document center")
                    {
                        if (provider.FormData.GetValues("action") == null && oid != -1) //for upload new file, not replace
                        {
                            if (sp != null)
                            {
                                db.SUPPORTINGDOCUMENTs.Remove(sp);
                                db.SaveChanges();
                            }

                            WriteLog(ex.ToString());
                        }

                    }

                    throw ex;
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;

        }

        [HttpPost]
        [ActionName("ImportMaterialList")]
        public System.Threading.Tasks.Task<HttpResponseMessage> ImportMaterialList(string param)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            List<MaterialOData> result = new List<MaterialOData>();
            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    var newName = "";
                    foreach (var file in provider.FileData)
                    {
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');
                        //var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }
                        else
                        {
                            newName = fileNameSplit[0];
                        }

                        newName = param + newName.Replace("\"", "");
                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));// beri nama baru dengan badgeno nya, agar tidak conflict dengan excel user lain
                            //theFileName = System.IO.Path.Combine(root, newName);

                        }

                    }

                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }


            });

            return task;

        }

        
        public IExcelDataReader FindFileExcel(string fileName)
        {
            try
            {
                GENERALPARAMETER generalParam = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.ADR_WEB_API_ODATA);
                //List<MaterialOData> result = new List<MaterialOData>();

                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);

                System.IO.FileStream fs = System.IO.File.Open(System.IO.Path.Combine(root, fileName), System.IO.FileMode.Open, System.IO.FileAccess.Read);

                IExcelDataReader excelReader;

                string[] excelFile = fileName.Split('.');
                if (excelFile[1] == "xlsx")
                {
                    // xlsx
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                }
                else
                {
                    // xls
                    excelReader = ExcelReaderFactory.CreateBinaryReader(fs);
                }



                return excelReader;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw new Exception(ex.ToString());
            }
            
        }

        [HttpGet]
        [ActionName("ImportMDCMaterialNumber")]
        public HttpResponseMessage ImportMDCMaterialNumber(string param)
        {
            try
            {
                List<Dictionary<string, dynamic>> result = new List<Dictionary<string, dynamic>>();

                IExcelDataReader excelReader = FindFileExcel(param);

                DataSet ds = excelReader.AsDataSet(true);

                var dt = ds.Tables[0];
                // excel dari mdc dimulai dari row ke 9
                for (int i = 8; i < dt.Rows.Count; i++)
                {
                    Dictionary<string, dynamic> listMaterialNumber = new Dictionary<string, dynamic>();
                    // cek tipe datanya
                    String materialNumber = "";
                    if (dt.Rows[i][3] is int)
                    {
                        int materialNumberInt = (int)dt.Rows[i][3];
                        materialNumber = materialNumberInt.ToString();
                    }
                    else if (dt.Rows[i][3] is string)
                    {
                        materialNumber = (string)dt.Rows[i][3];
                    }
                    else if (dt.Rows[i][3] is double)
                    {
                        double materialNumberDouble = (double)dt.Rows[i][3];
                        materialNumber = materialNumberDouble.ToString();
                    }

                    if (!String.IsNullOrEmpty(materialNumber))
                    {
                        listMaterialNumber.Add("ITEMNUMBER", (string)dt.Rows[i][1]);
                        listMaterialNumber.Add("ADRNUMBER", (string)dt.Rows[i][2]);

                        listMaterialNumber.Add("MATERIALCODE", materialNumber);
                        listMaterialNumber.Add("MATERIALDESC", (string)dt.Rows[i][4]);
                        listMaterialNumber.Add("MRPCODE", (string)dt.Rows[i][6]);
                        listMaterialNumber.Add("SENDTOMDCDATE", (DateTime)dt.Rows[i][7]);
                        listMaterialNumber.Add("MDCCOMPLETEDATE", (DateTime)dt.Rows[i][8]);


                        //==
                        result.Add(listMaterialNumber);
                    }

                }

                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);

                System.IO.File.Delete(System.IO.Path.Combine(root, param));

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw new Exception(ex.ToString());
            }
            
        }

        [HttpPost]
        [ActionName("PostUploadEWR")]
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUploadEWR(string param)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                int oid = -1;
                SUPPORTINGDOCUMENT sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                            int typeId = int.Parse(provider.FormData.GetValues("typeId")[0]);
                            string typeName = provider.FormData.GetValues("typeName")[0];
                            string uploadedBy = provider.FormData.GetValues("badgeNo")[0];

                            // 2. simpan informasi file2 yang diupload ke dalam supporting document
                            if (provider.FormData.GetValues("action") != null)
                            {
                                if (provider.FormData.GetValues("action")[0].ToUpper() == "EDIT") // for replace file
                                {
                                    oid = int.Parse(provider.FormData.GetValues("oid")[0]);
                                    sp = db.SUPPORTINGDOCUMENTs.Find(oid);
                                    sp.UPDATEDBY = GetCurrentNTUserId();
                                    sp.UPDATEDDATE = DateTime.Now;

                                    // delete file lamanya dari repo
                                    DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

                                }
                            }
                            else // for upload file
                            {
                                int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper()).Max(y => y.SEQUENCENO));
                                sp = new SUPPORTINGDOCUMENT();
                                sp.TYPEID = typeId;
                                sp.TYPENAME = typeName.ToUpper();
                                sp.SEQUENCENO = maxSeqNo + 1;
                                sp.UPLOADEDBY = uploadedBy;
                                if (provider.FormData.GetValues("typeDetail")[0] != null)
                                {
                                    sp.TYPEDETAIL = provider.FormData.GetValues("typeDetail")[0];
                                }

                                if (provider.FormData.GetValues("year") != null)
                                {
                                    int year = Convert.ToInt32(provider.FormData.GetValues("year")[0]);
                                    sp.UPLOADEDDATE = new DateTime(year, 1, 1);
                                }
                                else
                                {
                                    sp.UPLOADEDDATE = DateTime.Now;
                                }

                                sp.UPDATEDDATE = DateTime.Now;
                                sp.UPDATEDBY = GetCurrentNTUserId();

                                db.SUPPORTINGDOCUMENTs.Add(sp);
                                db.SaveChanges();
                                oid = sp.OID;
                            }

                            sp.FILENAME = theFileName.Replace(root, "").Replace("\\", ""); //buang url rootnya, jadi tinggal filenamenya saja
                            sp.REFDOCID = uploadDocumentToDocRepo(theFileName, newName, oid); // nanti diganti dari respon webservice vale untuk upload doc
                            //sp.REFDOCID = "-1"; //for testing
                            db.SaveChanges();

                            // 3. delete file after sent to webservice vale
                            System.IO.File.Delete(System.IO.Path.Combine(root, theFileName));

                            // 4. Done
                           
                        }

                    }


                }
                catch (Exception ex)
                {
                    if (provider.FormData.GetValues("action") == null && oid != -1) //for upload new file, not replace
                    {
                        if (sp != null)
                        {
                            db.SUPPORTINGDOCUMENTs.Remove(sp);
                            db.SaveChanges();
                        }

                    }

                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;
        }

        [HttpPost]
        [ActionName("PostUploadPSD")]
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUploadPSD(string param)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                int oid = -1;
                SUPPORTINGDOCUMENT sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                            int typeId = int.Parse(provider.FormData.GetValues("typeId")[0]);
                            string typeName = provider.FormData.GetValues("typeName")[0];
                            string uploadedBy = provider.FormData.GetValues("badgeNo")[0];

                            // 2. simpan informasi file2 yang diupload ke dalam supporting document
                            if (provider.FormData.GetValues("action") != null)
                            {
                                if (provider.FormData.GetValues("action")[0].ToUpper() == "EDIT") // for replace file
                                {
                                    oid = int.Parse(provider.FormData.GetValues("oid")[0]);
                                    sp = db.SUPPORTINGDOCUMENTs.Find(oid);
                                    sp.UPDATEDBY = GetCurrentNTUserId();
                                    sp.UPDATEDDATE = DateTime.Now;

                                    // delete file lamanya dari repo
                                    DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

                                }
                            }
                            else // for upload file
                            {
                                int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper()).Max(y => y.SEQUENCENO));
                                sp = new SUPPORTINGDOCUMENT();
                                sp.TYPEID = typeId;
                                sp.TYPENAME = typeName.ToUpper();
                                sp.SEQUENCENO = maxSeqNo + 1;
                                sp.UPLOADEDBY = uploadedBy;
                                if (provider.FormData.GetValues("typeDetail")[0] != null)
                                {
                                    sp.TYPEDETAIL = provider.FormData.GetValues("typeDetail")[0];
                                }

                                if (provider.FormData.GetValues("year") != null)
                                {
                                    int year = Convert.ToInt32(provider.FormData.GetValues("year")[0]);
                                    sp.UPLOADEDDATE = new DateTime(year, 1, 1);
                                }
                                else
                                {
                                    sp.UPLOADEDDATE = DateTime.Now;
                                }

                                sp.UPDATEDDATE = DateTime.Now;
                                sp.UPDATEDBY = GetCurrentNTUserId();

                                db.SUPPORTINGDOCUMENTs.Add(sp);
                                db.SaveChanges();
                                oid = sp.OID;
                            }

                            sp.FILENAME = theFileName.Replace(root, "").Replace("\\", ""); //buang url rootnya, jadi tinggal filenamenya saja
                            sp.REFDOCID = uploadDocumentToDocRepo(theFileName, newName, oid); // nanti diganti dari respon webservice vale untuk upload doc
                            //sp.REFDOCID = "-1"; //for testing
                            db.SaveChanges();

                            //update table master psdapproval
                            if (typeId > 0)
                            {
                                PSDAPPROVAL psdApprv = db.PSDAPPROVALs.Find(typeId);
                                psdApprv.DOCUMENTID = sp.OID;
                                db.Entry(psdApprv).State = EntityState.Modified;

                                db.SaveChanges();

                            }

                            // 3. delete file after sent to webservice vale
                            System.IO.File.Delete(System.IO.Path.Combine(root, theFileName));

                            // 4. Done

                        }

                    }


                }
                catch (Exception ex)
                {
                    if (provider.FormData.GetValues("action") == null && oid != -1) //for upload new file, not replace
                    {
                        if (sp != null)
                        {
                            db.SUPPORTINGDOCUMENTs.Remove(sp);
                            db.SaveChanges();
                        }

                    }

                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;
        }

        public String uploadDocumentToDocRepo(string uri, string fileName, int oid)
        {
            try
            {
                String result = "";
                List<GENERALPARAMETER> parameter = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DOCUMENT_REPOSITORY_CID).ToList();
                if (parameter.Count == 0)
                    throw new WebException("Configuration for document repository cid not found. Please contact administrator.");

                DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

                string parent = parameter[0].VALUE; //client identification
                System.IO.FileStream openStream = new System.IO.FileStream(uri, System.IO.FileMode.Open);
                System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(openStream);
                byte[] data = binaryReader.ReadBytes(Convert.ToInt32(openStream.Length));
                binaryReader.Close();

                int recId = oid;

                result = service.UploadFile(data, fileName, parent, recId, "iprom");

                return result;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw new Exception(ex.ToString());
            }
            
        }

        [HttpPost]
        [ActionName("PostUploadBudgetApproval")]
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUploadBudgetApproval(string param)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                int oid = -1;
                SUPPORTINGDOCUMENT sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                            int typeId = int.Parse(provider.FormData.GetValues("typeId")[0]);
                            string typeName = provider.FormData.GetValues("typeName")[0];
                            //string typeDetail = provider.FormData.GetValues("typeDetail")[0];
                            //string description = provider.FormData.GetValues("description")[0];
                            string uploadedBy = provider.FormData.GetValues("badgeNo")[0];

                            // 2. simpan informasi file2 yang diupload ke dalam supporting document
                            if (provider.FormData.GetValues("action") != null)
                            {
                                if (provider.FormData.GetValues("action")[0].ToUpper() == "EDIT") // for replace file
                                {
                                    oid = int.Parse(provider.FormData.GetValues("oid")[0]);
                                    sp = db.SUPPORTINGDOCUMENTs.Find(oid);
                                    sp.UPDATEDBY = GetCurrentNTUserId();
                                    sp.UPDATEDDATE = DateTime.Now;

                                    // delete file lamanya dari repo
                                    DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

                                }
                            }
                            else // for upload file
                            {
                                int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper()).Max(y => y.SEQUENCENO));
                                sp = new SUPPORTINGDOCUMENT();
                                sp.TYPEID = typeId;
                                sp.TYPENAME = typeName.ToUpper();
                                //sp.DESCRIPTION = description;
                                sp.SEQUENCENO = maxSeqNo + 1;
                                sp.UPLOADEDBY = uploadedBy;
                                //if (provider.FormData.GetValues("typeDetail")[0] != null)
                                //{
                                //    sp.TYPEDETAIL = provider.FormData.GetValues("typeDetail")[0];
                                //}

                                if (provider.FormData.GetValues("year") != null)
                                {
                                    int year = Convert.ToInt32(provider.FormData.GetValues("year")[0]);
                                    sp.UPLOADEDDATE = new DateTime(year, 1, 1);
                                }
                                else
                                {
                                    sp.UPLOADEDDATE = DateTime.Now;
                                }

                                sp.UPDATEDDATE = DateTime.Now;
                                sp.UPDATEDBY = GetCurrentNTUserId();

                                db.SUPPORTINGDOCUMENTs.Add(sp);
                                db.SaveChanges();
                                oid = sp.OID;
                            }

                            sp.FILENAME = theFileName.Replace(root, "").Replace("\\", ""); //buang url rootnya, jadi tinggal filenamenya saja
                            sp.REFDOCID = uploadDocumentToDocRepo(theFileName, newName, oid); // nanti diganti dari respon webservice vale untuk upload doc
                            //sp.REFDOCID = "-1"; //for testing
                            db.SaveChanges();

                            PROJECTBUDGETAPPROVAL update = db.PROJECTBUDGETAPPROVALs.Find(typeId);
                            update.FILENAME = sp.FILENAME;
                            update.DOCUMENTID = sp.OID;
                            update.UPDATEDDATE = DateTime.Now;
                            update.UPDATEDBY = GetCurrentNTUserId();
                            db.Entry(update).State = EntityState.Modified;
                            db.SaveChanges();


                            // 3. delete file after sent to webservice vale
                            System.IO.File.Delete(System.IO.Path.Combine(root, theFileName));

                            // 4. Done

                        }

                    }


                }
                catch (Exception ex)
                {
                    if (provider.FormData.GetValues("action") == null && oid != -1) //for upload new file, not replace
                    {
                        if (sp != null)
                        {
                            db.SUPPORTINGDOCUMENTs.Remove(sp);
                            db.SaveChanges();
                        }

                    }

                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;
        }

        [HttpGet]
        [ActionName("GetPhotoProfileByBadgeNo")]
        public HttpResponseMessage GetPhotoProfileByBadgeNo(string param)
        {
            try
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage();

                // Photo.Resize is a static method to resize the image
                Image image = Image.FromFile(@"\\\\pti-ctx03a\\photos\\E" + param + ".jpg");

                MemoryStream memoryStream = new MemoryStream();

                image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                httpResponseMessage.Content = new ByteArrayContent(memoryStream.ToArray());

                httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");
                httpResponseMessage.StatusCode = HttpStatusCode.OK;

                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());

                throw new Exception(ex.ToString());
            }
            
        }

        public static DataTable ImportToDataTable(string FilePath, string SheetName)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);
            // Check if the file exists    
            if (!fi.Exists)
                throw new Exception("File " + FilePath + " Does Not Exists");

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook        
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[SheetName];

                // Fetch the WorkSheet size        
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn        
                for (int col = startCell.Column; col <= endCell.Column; col++)
                    dt.Columns.Add(worksheet.Cells[1, col].Text);

                // place all the data into DataTable        
                for (int row = startCell.Row + 1; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        [HttpPost]
        [ActionName("PostUploadIconMenu")]
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUploadIconMenu(string param)
        {
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("../../../../PTVI.iPROM.WebApp/Images");

            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                //MENU sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /Images
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        //oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                        }

                    }
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;
        }

        [HttpPost]
        [ActionName("PostUploadNew")]
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUploadNew(string param)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                int oid = -1;
                SUPPORTINGDOCUMENT sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                            int typeId = int.Parse(provider.FormData.GetValues("typeId")[0]);
                            string typeName = provider.FormData.GetValues("typeName")[0];
                            string uploadedBy = provider.FormData.GetValues("badgeNo")[0];

                            // 2. simpan informasi file2 yang diupload ke dalam supporting document
                            if (provider.FormData.GetValues("action") != null)
                            {
                                if (provider.FormData.GetValues("action")[0].ToUpper() == "EDIT") // for replace file
                                {
                                    oid = int.Parse(provider.FormData.GetValues("oid")[0]);
                                    sp = db.SUPPORTINGDOCUMENTs.Find(oid);
                                    sp.UPDATEDBY = GetCurrentNTUserId();
                                    sp.UPDATEDDATE = DateTime.Now;

                                    // delete file lamanya dari repo
                                    DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

                                }
                            }
                            else // for upload file
                            {
                                int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper()).Max(y => y.SEQUENCENO));
                                sp = new SUPPORTINGDOCUMENT();
                                sp.TYPEID = typeId;
                                sp.TYPENAME = typeName.ToUpper();
                                sp.SEQUENCENO = maxSeqNo + 1;
                                sp.UPLOADEDBY = uploadedBy;
                                if (provider.FormData.GetValues("typeDetail")[0] != null)
                                {
                                    sp.TYPEDETAIL = provider.FormData.GetValues("typeDetail")[0];
                                }

                                if (provider.FormData.GetValues("year") != null)
                                {
                                    int year = Convert.ToInt32(provider.FormData.GetValues("year")[0]);
                                    sp.UPLOADEDDATE = new DateTime(year, 1, 1);
                                }
                                else
                                {
                                    sp.UPLOADEDDATE = DateTime.Now;
                                }

                                sp.UPDATEDDATE = DateTime.Now;
                                sp.UPDATEDBY = GetCurrentNTUserId();

                                db.SUPPORTINGDOCUMENTs.Add(sp);
                                db.SaveChanges();
                                oid = sp.OID;
                            }

                            sp.FILENAME = theFileName.Replace(root, "").Replace("\\", ""); //buang url rootnya, jadi tinggal filenamenya saja
                            sp.REFDOCID = uploadDocumentToDocRepo(theFileName, newName, oid); // nanti diganti dari respon webservice vale untuk upload doc
                            db.SaveChanges();
                            // Trace.WriteLine("Server file path: " + file.LocalFileName);

                            // 3. delete file after sent to webservice vale
                            System.IO.File.Delete(System.IO.Path.Combine(root, theFileName));
                        }

                    }


                }
                catch (Exception ex)
                {
                    if (provider.FormData.GetValues("action") == null && oid != -1) //for upload new file, not replace
                    {
                        if (sp != null)
                        {
                            db.SUPPORTINGDOCUMENTs.Remove(sp);
                            db.SaveChanges();
                        }

                    }

                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }



                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;
        }

        [HttpPost]
        [ActionName("PostUploadRejectDoc")]
        public System.Threading.Tasks.Task<HttpResponseMessage> PostUploadRejectDoc(string param)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string content = Request.Content.ToString();
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            // Read the form data and return an async task. 
            var task = Request.Content.ReadAsMultipartAsync(provider).
            ContinueWith<HttpResponseMessage>(t =>
            {
                int oid = -1;
                SUPPORTINGDOCUMENT sp = null;

                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                try
                {
                    // 1. copy file yang diupload ke /app_data
                    //string[] fileNames = new string[provider.FileData.Count];
                    //int i = 0;
                    string theFileName = "";
                    foreach (var file in provider.FileData)
                    {
                        oid = -1;
                        var fileName = file.Headers.ContentDisposition.FileName;
                        String[] fileNameSplit = fileName.Split('\\');

                        var newName = fileName;
                        if (fileNameSplit.Count() > 1)
                        {
                            newName = fileNameSplit[fileNameSplit.Count() - 1];
                        }

                        newName = newName.Replace("\"", "");

                        //delete file yang sudah ada
                        if (System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                            System.IO.File.Delete(System.IO.Path.Combine(root, newName));

                        //cek dulu sudah ada file dengan nama yang sama atau belum
                        if (!System.IO.File.Exists(System.IO.Path.Combine(root, newName)))
                        {
                            System.IO.File.Move(file.LocalFileName, System.IO.Path.Combine(root, newName));
                            theFileName = System.IO.Path.Combine(root, newName);

                            string typeId = provider.FormData.GetValues("typeId")[0];
                            string[] arrMaterialOID = typeId.Split('|');

                            string typeName = provider.FormData.GetValues("typeName")[0];
                            string uploadedBy = provider.FormData.GetValues("badgeNo")[0];

                            string refDocId = "";

                            foreach (string materialOID in arrMaterialOID)
                            {
                                int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper()).Max(y => y.SEQUENCENO));
                                sp = new SUPPORTINGDOCUMENT();
                                sp.TYPEID = int.Parse(materialOID);
                                sp.TYPENAME = typeName.ToUpper();
                                sp.SEQUENCENO = maxSeqNo + 1;
                                sp.UPLOADEDBY = uploadedBy;
                                sp.UPLOADEDDATE = DateTime.Now;

                                sp.UPDATEDDATE = DateTime.Now;
                                sp.UPDATEDBY = GetCurrentNTUserId();

                                db.SUPPORTINGDOCUMENTs.Add(sp);
                                db.SaveChanges();
                                oid = sp.OID;


                                sp.FILENAME = theFileName.Replace(root, "").Replace("\\", ""); //buang url rootnya, jadi tinggal filenamenya saja
                                
                                // satu ref doc untuk seluruh material yg direject
                                if (string.IsNullOrEmpty(refDocId))
                                {
                                    refDocId = uploadDocumentToDocRepo(theFileName, newName, oid); // nanti diganti dari respon webservice vale untuk upload doc
                                }

                                sp.REFDOCID = refDocId;

                                db.SaveChanges();

                               
                            }
                            // 3. delete file after sent to webservice vale
                            System.IO.File.Delete(System.IO.Path.Combine(root, theFileName));

                        }

                    }


                }
                catch (Exception ex)
                {
                    if (provider.FormData.GetValues("action") == null && oid != -1) //for upload new file, not replace
                    {
                        if (sp != null)
                        {
                            db.SUPPORTINGDOCUMENTs.Remove(sp);
                            db.SaveChanges();
                        }

                    }
                    WriteLog(ex.ToString());
                    throw new Exception(ex.ToString());
                }



                return Request.CreateResponse(HttpStatusCode.OK);
            });

            return task;
        }
        
        public String removeDocRepo(string fileName, int recId, int refDocID)
        {
            try
            {
                String result = "";
                List<GENERALPARAMETER> parameter = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.DOCUMENT_REPOSITORY_CID).ToList();
                if (parameter.Count == 0)
                    throw new WebException("Configuration for document repository cid not found. Please contact administrator.");

                DocumentRepositoryService.Service service = new DocumentRepositoryService.Service();

                string parent = parameter[0].VALUE; //client identification

                result = service.RemoveFile(refDocID, fileName, parent, recId);
                return result;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                throw new Exception(ex.ToString());
            }
            
        }

        public void UploadFileInAppData(string filename, int typeId, string typeName, string typeDetail, string uploadedBy)
        {
           
            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var provider = new MultipartFormDataStreamProvider(root);

            int oid = -1;
            
            string newfilename = System.IO.Path.Combine(root, filename);

            int maxSeqNo = Convert.ToInt32(db.SUPPORTINGDOCUMENTs.Where(x => x.TYPENAME == typeName.ToUpper()).Max(y => y.SEQUENCENO));
            SUPPORTINGDOCUMENT sp = new SUPPORTINGDOCUMENT();
            sp.TYPEID = typeId;
            sp.TYPENAME = typeName.ToUpper();
            sp.TYPEDETAIL = typeDetail;
            sp.SEQUENCENO = maxSeqNo + 1;
            sp.UPLOADEDBY = uploadedBy;
            sp.UPLOADEDDATE = DateTime.Now;
            
            db.SUPPORTINGDOCUMENTs.Add(sp);
            db.SaveChanges();
            oid = sp.OID;

            sp.FILENAME = filename; //buang url rootnya, jadi tinggal filenamenya saja
            sp.REFDOCID = uploadDocumentToDocRepo(newfilename, filename, oid); // nanti diganti dari respon webservice vale untuk upload doc
                                                                                //sp.REFDOCID = "-1"; //for testing
            db.SaveChanges();

            // 3. delete file after sent to webservice vale
            System.IO.File.Delete(newfilename);

            // 4. Done

        }

            
        
    }
}
