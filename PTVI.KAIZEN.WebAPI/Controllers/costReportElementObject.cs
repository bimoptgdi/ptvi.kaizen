﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PTVI.KAIZEN.WebAPI.Models.CustomObject
{
    public partial class costReportElementObject
    {
        public costReportElementObject()
        {
            this.costReportActualCosts = new HashSet<costReportActualCostObject>();
            this.costReportBudgets = new HashSet<costReportBudgetObject>();
            this.costReportCommitements = new HashSet<costReportCommitmentObject>();
            this.costReportNetworks = new HashSet<costReportNetworkObject>();
        }

        public int id { get; set; }
        public int? projectId { get; set; }
        public int? projectDocumentId { get; set; }
        public string projectNo { get; set; }
        public int? wbsLevel { get; set; }
        public string wbsNo { get; set; }
        public string description { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string updatedBy { get; set; }

        public virtual ICollection<costReportActualCostObject> costReportActualCosts { get; set; }
        public virtual ICollection<costReportBudgetObject> costReportBudgets { get; set; }
        public virtual ICollection<costReportCommitmentObject> costReportCommitements { get; set; }
        public virtual projectObject project { get; set; }
        public virtual ICollection<costReportNetworkObject> costReportNetworks { get; set; }
    }
}
