﻿using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Script.Serialization;
using PTVI.KAIZEN.WebAPI.Controllers;
//using Microsoft.Office.Project.Server.Library;
using System.Globalization;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class KAIZENController : ApiController
    {
        protected VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        //protected bool _debugMode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_MODE;
        //protected bool _K2DebugMode = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.K2_DEBUG_MODE;
        protected string GetCurrentNTUserId()
        {
            string ntUserId = "";

            System.Security.Principal.IIdentity identity = (Request.Properties["MS_HttpContext"] as System.Web.HttpContextWrapper).Request.LogonUserIdentity;
            string userName = identity.Name;

            string[] identityName = userName.Split('\\');
            if (identityName.Length > 1)
            {
                ntUserId = identityName[1];
            }
            else
            {
                ntUserId = userName;
            }
            return ntUserId;
        }

        public Dictionary<string, dynamic> GetJsonObjectFromWebApi(string url)
        {
            Uri uri = new Uri(url);

            WebRequest webRequest = WebRequest.Create(uri);
            webRequest.UseDefaultCredentials = true;

            using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
            {
                StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                string json = reader.ReadToEnd();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                //var output = serializer.Deserialize<Dictionary<string, dynamic>>(json);
                var output = serializer.Deserialize<Dictionary<string, dynamic>>(json);

                return output;
            }

            //return null;
        }
        protected DataTable GetListFromExternalData(string apiUrl)
        {
            // initiate web api client 
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            HttpClient client = new HttpClient(handler);
            List<employeeObject> list = new List<employeeObject>();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var serializer = new JavaScriptSerializer();

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;

            if (response.IsSuccessStatusCode)
            {
                list = response.Content.ReadAsAsync<List<employeeObject>>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsStringAsync().Result;
                throw new ApplicationException(exception);
            }

            DataTable result = new DataTable();
            result.Columns.Add("employeeId");
            result.Columns.Add("fullName");
            result.Columns.Add("userName");
            result.Columns.Add("email");
            result.Columns.Add("deptCode");

            foreach (employeeObject emp in list)
            {
                DataRow row = result.NewRow();
                row["employeeId"] = emp.employeeId;
                row["fullName"] = emp.full_Name;
                row["userName"] = emp.userName;
                row["email"] = emp.email;
                row["deptCode"] = emp.dept_Code;

                result.Rows.Add(row);

            }

            result.AcceptChanges();

            return result;
        }

        protected DataTable getDataByQuery(string qry, System.Data.SqlClient.SqlConnection connection)
        {
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = new System.Data.SqlClient.SqlCommand(qry, connection);

            DataTable dt = new DataTable();
            adapter.Fill(dt);

            return dt;
        }

        public List<T> GetListFromExternalData<T>(string apiUrl)
        {
            // initiate web api client 
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;

            HttpClient client = new HttpClient(handler);
            List<T> list = new List<T>();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var serializer = new JavaScriptSerializer();

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;

            if (response.IsSuccessStatusCode)
            {
                list = response.Content.ReadAsAsync<List<T>>().Result;
            }
            else
            {
                var exception = response.Content.ReadAsStringAsync().Result;
                throw new ApplicationException(exception);
            }

            return list;
        }

        public List<Dictionary<string, dynamic>> GetJsonDataFromWebApi(string url)
        {
            try
            {
                List<Dictionary<string, dynamic>> output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }
                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public OData GetJsonDataFromWebApiOData(string url)
        {
            try
            {
                OData output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);

                    output = serializer.Deserialize<OData>(json);


                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }
                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public class FileData
        {
            public string ContentType { get; set; }
            public string Base64 { get; set; }
            public string FileName { get; set; }
        }

        // POST api/adr/ExportToExcelProxy
        [HttpPost]
        [ActionName("ExportToExcelProxy")]
        public HttpResponseMessage ExportToExcelProxy(string param, [FromBody]FileData file)
        {
            var data = Convert.FromBase64String(file.Base64);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(data))
            };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = file.FileName
            };

            return result;
        }

        public void WriteLog(string p)
        {
            //string logPath = global::PTVI.iPROM.WebApi.Properties.Settings.Default.LogPath;
            //using (StreamWriter writer = new StreamWriter(new FileStream(Path.Combine(logPath, "Log.txt"), FileMode.Append)))
            {
                //writer.WriteLine(DateTime.Now + ": " + p);
            }
        }

        [HttpGet]
        [ActionName("percentageMonthPogressByStartEnd_Date")]
        public List<keyDeliverablesProgressObject> percentageMonthPogressByStartEnd_Date(DateTime startDate, DateTime endDate)
        {
            double totalPercentage = 0;
            List<keyDeliverablesProgressObject> listMonth = new List<keyDeliverablesProgressObject>();
            if (startDate.Year == endDate.Year && startDate.Month == endDate.Month)
            {
                listMonth.Add(new keyDeliverablesProgressObject { year = startDate.Year, month = startDate.Month, planPercentage = 100 });
            }
            else
            {
                int days;
                DateTime nextDate = new DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(startDate.Year, startDate.Month));
                //var totalDays = (endDate - startDate).TotalDays;
                var totalDays = (double)getWorkingDays(startDate, endDate);
                var totalMonths = getTotalMonth(startDate, endDate);
                for (var i = 0; i <= totalMonths; i++)
                {
                    if (i == 0)
                    {
                        days = getWorkingDays(startDate, nextDate);
                    }
                    else if (i == totalMonths)
                    {
                        days = getWorkingDays(new DateTime(nextDate.Year, nextDate.Month, 1), endDate);
                    }
                    else
                    {
                        days = getWorkingDays(new DateTime(nextDate.Year, nextDate.Month, 1), new DateTime(nextDate.Year, nextDate.Month, DateTime.DaysInMonth(nextDate.Year, nextDate.Month)));
                    }
                    var getPercentage = days / totalDays * 100;
                    totalPercentage += getPercentage;

                    listMonth.Add(new keyDeliverablesProgressObject { year = nextDate.Year, month = nextDate.Month, planPercentage = Math.Truncate(totalPercentage > 100 ? 100 : totalPercentage), actualPercentage = getPercentage - Math.Truncate(getPercentage) });
                    nextDate = nextDate.AddMonths(1);
                }
            }

            int remainingPercentage = 100 - Convert.ToInt32(listMonth.Sum(s => s.planPercentage).Value);
            var orderByMaxDecimal = listMonth.OrderByDescending(o => o.actualPercentage);

            for (var i = 0; i < remainingPercentage; i++)
            {
                orderByMaxDecimal.ElementAt(i).planPercentage += 1;
            }

            return listMonth;
        }

        [HttpGet]
        [ActionName("getWorkingDays")]
        public int getWorkingDays(DateTime from, DateTime to)
        {
            var dayDifference = (int)to.Subtract(from).TotalDays + 1;
            return Enumerable
                .Range(1, dayDifference)
                .Select(x => from.AddDays(x))
                .Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);
        }

        [HttpGet]
        [ActionName("getTotalMonth")]
        public int getTotalMonth(DateTime firstDate, DateTime secondDate)
        {
            return ((secondDate.Year - firstDate.Year) * 12) + secondDate.Month - firstDate.Month;
        }

        //public string mainSPTraficStatus(DateTime planStart, DateTime? actualStart, DateTime? planFinish, DateTime? actualFinish, string status)
        //{
        //    DateTime curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        //    DateTime planStartDate = new DateTime(planStart.Year, planStart.Month, planStart.Day);
        //    GENERALPARAMETER trafficYellow = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.TOTALDAYS_YELLOW).First();
        //    GENERALPARAMETER trafficRed = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.TOTALDAYS_RED).First();

        //    int rangeDate = 0;
        //    if (curDate < planStartDate)
        //        rangeDate = getWorkingDays(curDate, planStartDate) - 1;
        //    else
        //        rangeDate = (getWorkingDays(planStartDate, curDate) - 1) * -1;

        //    if (status == constants.STATUSIPROM_HOLD || status == constants.STATUSIPROM_CANCELLED) return constants.TRAFFICLIGHT_GREY;
        //    if ((actualStart != null) && (actualFinish == null) && (planFinish < curDate))
        //        return constants.TRAFFICLIGHT_RED;
        //    if (!actualStart.HasValue)
        //    {
        //        if (actualFinish == null && planFinish != null)
        //        {
        //            int rdate = 0;
        //            if (curDate < planFinish)
        //                rdate = getWorkingDays(curDate, planFinish.Value) - 1;
        //            else
        //                rdate = (getWorkingDays(planFinish.Value, curDate) - 1) * -1;

        //            if (rdate <= int.Parse(trafficRed.VALUE)) return constants.TRAFFICLIGHT_RED;
        //            else if (rdate <= int.Parse(trafficYellow.VALUE)) return constants.TRAFFICLIGHT_YELLOW;
        //            else return constants.TRAFFICLIGHT_GREEN;
        //        }
        //        else
        //        {
        //            if ((planStartDate > curDate)) return constants.TRAFFICLIGHT_RED;
        //            else if ((rangeDate * -1)<= int.Parse(trafficYellow.VALUE))
        //            {
        //                return constants.TRAFFICLIGHT_YELLOW;
        //            }
        //            else
        //            {
        //                return constants.TRAFFICLIGHT_GREEN;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        if ((actualStart != null) && (actualFinish != null) && (planStart != null) && (planFinish != null) && (actualFinish <= curDate))
        //        {
        //            return constants.TRAFFICLIGHT_GREEN;
        //        } else
        //        if (actualFinish == null)
        //        {
        //            int rdate = 0;
        //            if (curDate < planFinish)
        //                rdate = getWorkingDays(curDate, planFinish.Value) - 1;
        //            else
        //                rdate = (getWorkingDays(planFinish.Value, curDate) - 1) * -1;

        //            if (rdate <= int.Parse(trafficRed.VALUE)) return constants.TRAFFICLIGHT_RED;
        //            else if (rdate <= int.Parse(trafficYellow.VALUE)) return constants.TRAFFICLIGHT_YELLOW;
        //            else return constants.TRAFFICLIGHT_GREEN;
        //        }
        //        return constants.TRAFFICLIGHT_GREEN;
        //    }
        //}
        public string traficStatus(DateTime plan, DateTime? actual, string status)
        {
            DateTime curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime planDate = new DateTime(plan.Year, plan.Month, plan.Day);
            GENERALPARAMETER trafficYellow = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.TOTALDAYS_YELLOW).First();
            GENERALPARAMETER trafficRed = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.TOTALDAYS_RED).First();

            int rangeDate = 0;
            if (curDate < planDate)
                rangeDate = getWorkingDays(curDate, planDate);
            else
                rangeDate = (getWorkingDays(planDate, curDate)) * -1;

            if (!actual.HasValue)
            {
                //if (status == constants.STATUSIPROM_HOLD || status == constants.STATUSIPROM_CANCELLED) return constants.TRAFFICLIGHT_GREY;
                //if ((actual == null)&&(planDate > curDate)) return constants.TRAFFICLIGHT_RED;
                //if (status == "Closed" || status == "Completed" || status == "Cancelled")
                if (status == constants.STATUSIPROM_COMPLETED || status == constants.STATUSIPROM_CANCELLED || status == constants.STATUSCS_CLOSED || status == constants.STATUSCS_COMPLETED || status == constants.STATUSCS_CANCELLED || rangeDate > int.Parse(trafficYellow.VALUE))
                {
                    return constants.TRAFFICLIGHT_GREEN;
                }
                else if (rangeDate <= int.Parse(trafficRed.VALUE))
                {
                    return constants.TRAFFICLIGHT_RED;
                }
                else
                {
                    return constants.TRAFFICLIGHT_YELLOW;
                }
            }
            else
            {
                return constants.TRAFFICLIGHT_GREEN;
            }
        }

        public string statusLabel(DateTime? plan, DateTime? actual)
        {
            var result = constants.TRAFFICLIGHT_BLACK;
            if (plan.HasValue && actual.HasValue)
                if (plan.Value < actual.Value)
                    result = constants.TRAFFICLIGHT_RED;
            return result;
        }

        protected NetworkCredential pwaCredential
        {
            get
            {
                var username = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.ProjectServerUserName;
                var password = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.ProjectServerUserPassword;
                var domain = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.ProjectServerUserDomain;

                return new System.Net.NetworkCredential(username, password, domain);
            }
        }

        protected string pwaPath
        {
            get
            {
                return PTVI.KAIZEN.WebAPI.Properties.Settings.Default.ProjectServerUrl;
            }
        }

        protected string cfExecutor1Uid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_EXECUTOR).First();

                return gp.VALUE;
            }
        }

        protected string cfSupervisorUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_SUPERVISOR).First();

                return gp.VALUE;
            }
        }

        protected string cfInPlannerUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_INPLANNER).First();

                return gp.VALUE;
            }
        }

        protected string cfCMUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_CM).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcPlanDurationStartDateUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_PLAN_CALC_STARTDATE).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcPlanDurationEndDateUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_PLAN_CALC_ENDDATE).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcPlanDurationUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_PLAN_CALC_DURATION).First();

                return gp.VALUE;

                //return "6a975f17-4cc7-e711-80e2-005056900d63";
            }
        }

        protected string cfCalcOptimisticDurationStartDate
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_STARTDATE).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcOptimisticDurationEndDate
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_ENDDATE).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcOptimisticDuration
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_OPTIMIS_CALC_DURATION).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcOptimisticDurationSum
        {
            get
            {
                return "15513f2b-27d3-e711-80e4-005056900d63";
            }
        }


        protected string cfCalcRealisticDurationSum
        {
            get
            {
                return "224357c0-90d5-e711-80e4-005056900d63";
            }
        }
        protected string cfCalcRealisticDurationStartUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_REALIS_CALC_STARTDATE).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcRealisticDurationEndDateUid
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_REALIS_CALC_ENDDATE).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcRealisticDuration
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_REALIS_CALC_DURATION).First();

                return gp.VALUE;
            }
        }

        protected string cfCalcOriginalDurationStartUid
        {
            get
            {
                return "7dd27a37-b4d2-e711-80e4-005056900d63";
            }
        }

        protected string cfCalcOriginalDurationEndDateUid
        {
            get
            {
                return "ceb6a443-b4d2-e711-80e4-005056900d63";
            }
        }

        protected string cfCalcOriginalDuration
        {
            get
            {
                return "afb374c0-b4d2-e711-80e4-005056900d63";
            }
        }

        protected string cfCalcOriginalDurationSum
        {
            get
            {
                return "657701a4-27d3-e711-80e4-005056900d63";
            }
        }


        protected string cfTotalDuration
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_CUSTOMFIELD_UID_TOTALDURATION).First();

                return gp.VALUE;
            }
        }

        protected int shiftTargetMaxTaskLevel
        {
            get
            {
                var gp = db.GENERALPARAMETERs.Where(x => x.TITLE == constants.IPT_SHIFTTARGET_MAXTASKLEVEL).First();

                return Convert.ToInt32(gp.VALUE);
            }
        }

        protected string cfCalcRemainingOptimistic
        {
            get
            {
                return "6a975f17-4cc7-e711-80e2-005056900d63";
            }
        }

        protected string cfRemainingOptimisticEach
        {
            get
            {
                return "8a5bc847-87d3-e711-80e4-005056900d63";
            }
        }

        protected string cfOptimisticFinishEach
        {
            get
            {
                return "d1e9aed7-8ad3-e711-80e4-005056900d63";
            }
        }

        protected string cfOptimisticDurationEach
        {
            get
            {
                return "27395451-8bd3-e711-80e4-005056900d63";
            }
        }

        protected string cfRemainingRealisticEach
        {
            get
            {
                return "dcfefe80-ebd3-e711-80e4-005056900d63";
            }
        }

        protected string cfRealisticFinishEach
        {
            get
            {
                return "570a81b3-ebd3-e711-80e4-005056900d63";
            }
        }

        protected string cfRealisticDurationEach
        {
            get
            {
                return "eb2d4999-f1d3-e711-80e4-005056900d63";
            }
        }

        protected string cfOriginalFinishEach
        {
            get
            {
                return "272923b4-edd3-e711-80e4-005056900d63";
            }
        }

        protected string cfCalcRemainingOriginal
        {
            get
            {
                return "3655294a-28d3-e711-80e4-005056900d63";
            }
        }

        protected string cfDurationForRealisticCalc
        {
            get
            {
                return "a932bea2-61d3-e711-80e4-005056900d63";
            }
        }

        protected string cfPercentCompleteBaseline
        {
            get
            {
                return "3e7566ba-bdc7-e711-80e2-005056900d63";
            }
        }

        protected string cfEndShiftTime
        {
            get
            {
                return "989efaae-41d2-e711-80e4-005056900d63";
            }
        }

        protected string cfRemainingRealistic
        {
            get
            {
                return "363f52ea-3ad2-e711-80e4-005056900d63";
            }
        }

        protected string cfOptimisticFinishEachF
        {
            get
            {
                return "f4858d76-2600-e811-80e4-005056900d63";
            }
        }

        protected string cfRealisticFinishEachF
        {
            get
            {
                return "54737c97-2600-e811-80e4-005056900d63";
            }
        }

        //protected string cfCalcRemainingOptimisticF
        //{
        //    get
        //    {
        //        return "cddf0ca1-4e00-e811-80e4-005056900d63";
        //    }
        //}

        //protected string cfCalcRemainingRealisticF
        //{
        //    get
        //    {
        //        return "af818b06-4e00-e811-80e4-005056900d63";
        //    }
        //}

        protected List<CalendarExceptionObject> ReadCalendarException(PSI_CalendarWS.Calendar calendarWS)
        {
            if (calendarWS == null)
            {
                calendarWS = new PSI_CalendarWS.Calendar();
                //calendarWS.Credentials = pwaCredential;
            }

            var calendarDS = calendarWS.ListCalendars();
            var result = new List<CalendarExceptionObject>();

            //// initialize standard calendar
            //var stdCalendarExceptionObject = new CalendarExceptionObject()
            //{
            //    Uid = calExceptionRow.CAL_UID,
            //    Name = calExceptionRow.Name,
            //    StartDate = calExceptionRow.Start,
            //    FinishDate = calExceptionRow.Finish
            //};

            //var workingHours = new List<CalendarWorkingHour>();
            //if (!calExceptionRow.IsShift1StartNull())
            //{
            //    workingHours.Add(new CalendarWorkingHour()
            //    {
            //        StartTime = new TimeSpan(0, calExceptionRow.Shift1Start, 0),
            //        EndTime = new TimeSpan(0, calExceptionRow.Shift1Finish, 0)
            //    });
            //}

            //calendarExceptionObject.WorkingHours = workingHours;

            //result.Add(calendarExceptionObject);

            //Filter filter = new Filter();

            //filter.FilterTableName = calendarDS.CalendarExceptions.TableName;

            //foreach (System.Data.DataColumn c in calendarDS.CalendarExceptions.Columns)
            //{
            //    filter.Fields.Add(new Filter.Field(calendarDS.CalendarExceptions.TableName,
            //          c.ColumnName, Filter.SortOrderTypeEnum.None));
            //}

            //var calendarDSException = calendarWS.ReadCalendars(filter.GetXml(), false);
            //foreach (var row in calendarDSException.CalendarExceptions.Rows)
            {
                //var calExceptionRow = row as PSI_CalendarWS.CalendarDataSet.CalendarExceptionsRow;

                var calendarExceptionObject = new CalendarExceptionObject()
                {
                    //Uid = calExceptionRow.CAL_UID,
                    //Name = calExceptionRow.Name,
                    //StartDate = calExceptionRow.Start,
                    //FinishDate = calExceptionRow.Finish
                };

                var workingHours = new List<CalendarWorkingHour>();
                //if (!calExceptionRow.IsShift1StartNull())
                //{
                //    workingHours.Add(new CalendarWorkingHour()
                //    {
                //        StartTime = new TimeSpan(0, calExceptionRow.Shift1Start, 0),
                //        EndTime = new TimeSpan(0, calExceptionRow.Shift1Finish, 0)
                //    });
                //}

                calendarExceptionObject.WorkingHours = workingHours;

                result.Add(calendarExceptionObject);
            }

            return result;
        }

        protected double calculateDuration(DateTime startDate, DateTime endDate, CalendarExceptionObject exceptionCalendar)
        {
            double duration = 0;
            foreach (var wh in exceptionCalendar.WorkingHours)
            {
                var whStartDate = startDate.Date.Add(wh.StartTime);
                var whEndDate = startDate.Date.Add(wh.EndTime);

                var startCal = startDate > whStartDate ? startDate : whStartDate;
                var endCal = endDate < whEndDate ? endDate : whEndDate;

                if (endCal > startCal)
                    duration += (endCal - startCal).TotalMinutes;
            }

            return duration;
        }

        //protected void CheckProjectState(Guid projectUid)
        //{
        //    var credential = pwaCredential;
        //    PSI_ProjectWS.Project ProjectWS = new PSI_ProjectWS.Project();
        //    ProjectWS.Credentials = credential;

        //    var projectDs = ProjectWS.ReadProject(projectUid, PSI_ProjectWS.DataStoreEnum.WorkingStore);
        //    var projectRow = projectDs.Project.Rows[0] as PSI_ProjectWS.ProjectDataSet.ProjectRow;

        //    if (!projectRow.IsPROJ_CHECKOUTBYNull())
        //    {
        //        // get checkout by user info
        //        var checkOutById = projectRow.PROJ_CHECKOUTBY;
        //        var checkOutDate = projectRow.PROJ_CHECKOUTDATE;
        //        var checkOutDesc = projectRow.PROJ_SESSION_DESCRIPTION;

        //        PSI_ResourceWS.Resource resWs = new PSI_ResourceWS.Resource();
        //        resWs.Credentials = credential;

        //        var resDS = resWs.ReadResource(checkOutById);
        //        var resRow = resDS.Resources.Rows[0] as PSI_ResourceWS.ResourceDataSet.ResourcesRow;

        //        throw new ApplicationException(string.Format("Project is currently checked out by '{0}' at '{1}' with description '{2}'",
        //                                        ""/*resRow.RES_NAME*/, checkOutDate.ToString("dd MMM yyyy  HH:mm"), checkOutDesc));
        //    }
        //}

        //protected string getNextK2Approver(string param)
        //{
        //    if (_K2DebugMode)
        //    {
        //        return "wnotifier";
        //    }
        //    else
        //    {
        //        //return "wnotifier";
        //        return GetCurrentNTUserId().ToLower();
        //    }

        //}

        public string GetNextApprovalStatus(string currStatus)
        {
            if (currStatus == constants.EWR_SUBMIT)
            {
                return constants.EWR_L2_APPROVED;
            }
            else if (currStatus == constants.EWR_L2_APPROVED)
            {
                return constants.EWR_ENGINEER_SEC_APPROVED;
            }
            else if (currStatus == constants.EWR_ENGINEER_SEC_APPROVED)
            {
                return constants.EWR_ENGINEER_L2_APPROVED;
            }
            return "-";
        }

        //protected string GetDocumentGroupDesignerbyProjectId(int projectId)
        //{
        //    PROJECTDOCUMENT getProjDocument = db.PROJECTDOCUMENTs.Find(projectId);
        //    return GetDocumentGroupDesigner(getProjDocument);
        //}


        //protected string GetDocumentGroupDesigner(PROJECTDOCUMENT getProjDocument)
        //{
        //    int lastIndexOfDocNo = getProjDocument.DOCNO.LastIndexOf("-");
        //    var parentDocNo = "";
        //    if (lastIndexOfDocNo < 0)
        //    {
        //        parentDocNo = getProjDocument.DOCNO;
        //    }
        //    else
        //    {
        //        parentDocNo = getProjDocument.DOCNO.Substring(0, lastIndexOfDocNo);
        //        if (parentDocNo == "" || parentDocNo.IndexOf("-") < 0)
        //        {
        //            parentDocNo = getProjDocument.DOCNO;
        //        }
        //    }
        //    var documentGroup = db.PROJECTDOCUMENTs.Where(w => w.DOCNO == parentDocNo && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).Count() > 0 ? db.PROJECTDOCUMENTs.Where(w => w.DOCNO == parentDocNo && (w.DOCTYPE == constants.DOCUMENTTYPE_ENGINEER || w.DOCTYPE == constants.DOCUMENTTYPE_DESIGN_CONFORMITY)).FirstOrDefault().DOCTYPE : null;
        //    return documentGroup;
        //}

        //protected double? GetParameterValueForProjectDoc(PROJECTDOCUMENT getProjDocument)
        //{
        //    string documentGroup = GetDocumentGroupDesigner(getProjDocument);

        //    return documentGroup == constants.DOCUMENTTYPE_ENGINEER ? GetTotalOfProductionByProjDoc(getProjDocument) : documentGroup == constants.DOCUMENTTYPE_DESIGN_CONFORMITY ? (double?)null : (double?)null;
        //}

        //protected double? GetTotalOfProductionByProjId(int param)
        //{
        //    PROJECTDOCUMENT getProjDocument = db.PROJECTDOCUMENTs.Find(param);
        //    return GetTotalOfProductionByProjDoc(getProjDocument);
        //}

        //protected double? GetTotalOfProductionByProjDoc(PROJECTDOCUMENT getProjDocument)
        //{
        //    //Get Position User Mechanical Civil Hrs per sheet
        //    var getPositionDrawing = db.USERASSIGNMENTs.Where(w => w.EMPLOYEEID == getProjDocument.DOCUMENTBYID && w.ASSIGNMENTTYPE == constants.PROJECT_DESIGNER && w.PROJECTID == getProjDocument.PROJECTID).FirstOrDefault();
        //    string position = getPositionDrawing != null ? getPositionDrawing.EMPLOYEEPOSITION : null;

        //    //Get default Complexity Value
        //    double? complexityDefaultValue = db.MASTERCOMPLEXITYDCs.Where(w => w.YEAR == getProjDocument.PLANFINISHDATE.Value.Year).Sum(s => s.WEIGHT);

        //    //Get Value Mechanical Civil Hrs per sheet
        //    var gpMC = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.FactoredDrawingMC);
        //    double gpMCValue = 0;
        //    if (gpMC.Count() > 0)
        //    {
        //        double.TryParse(gpMC.FirstOrDefault().VALUE, out gpMCValue);
        //    }

        //    //Get Value Electrical Engineer Hrs per sheet
        //    var gpEI = db.GENERALPARAMETERs.Where(w => w.TITLE == constants.FactoredDrawingEI);
        //    double gpEIValue = 0;
        //    if (gpEI.Count() > 0)
        //    {
        //        double.TryParse(gpEI.FirstOrDefault().VALUE, out gpEIValue);
        //    }

        //    return GetTotalOfProductionByValue(getProjDocument, complexityDefaultValue, position == constants.positionGroupDesignerMC ? gpMCValue : position == constants.positionGroupDesignerEI ? gpEIValue : 0);
        //}

        //protected double? GetTotalOfProductionByValue(PROJECTDOCUMENT projDocument, double? complexityDefaultValue, double hrsPerSheetValue)
        //{
        //    double? typeOfDrawingValue = projDocument.PROJECTDOCUMENTDRAWINGTYPEs.Select(s => s.NUMBEROFDRAWING * s.MASTERDESIGNERPRODUCTION.ESTIMATEHOURS).Sum();

        //    double? complexityValue = projDocument.PROJECTDOCUMENTCOMPLEXITies.Select(s => s.COMPLEXITYVALUE * s.MASTERCOMPLEXITYDC.WEIGHT).Sum();
        //    double? valueComplexityByDevaultValue = complexityValue / complexityDefaultValue;

        //    double? valueComplexityByParticipant = projDocument.PARTICIPATIONVALUE * valueComplexityByDevaultValue / 100; // 100 adalah nilai %
        //    double? result = (valueComplexityByParticipant / hrsPerSheetValue) * typeOfDrawingValue;
        //    //return Math.Round(result.HasValue ? result.Value : 0, 2);
        //    return result;

        //}

        //protected double? GetFinalResultByValueByProjId(int param)
        //{
        //    PROJECTDOCUMENT getProjDocument = db.PROJECTDOCUMENTs.Find(param);
        //    return GetFinalResultByValueByProjDocument(getProjDocument);
        //}

        //protected double? GetFinalResultByValueByProjDocument(PROJECTDOCUMENT projDocument)
        //{
        //    return projDocument.PROJECTDOCUMENTFINALRESULTS.Select(s => (s.FINALRESULTVALUE == constants.finalResultActionYes || s.FINALRESULTVALUE == constants.finalResultActionNotRequired ? 1 : 0) * s.MASTERFINALRESULT.VALUE).Sum();
        //}

        //protected int GetWeeksInYear(int year, DayOfWeek dayOfWeek)
        //{
        //    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
        //    DateTime date1 = new DateTime(year, 12, 31);
        //    Calendar cal = dfi.Calendar;
        //    return cal.GetWeekOfYear(date1, CalendarWeekRule.FirstFullWeek, dayOfWeek);
        //}

        protected static int? getDateOfWeek(DateTime dateTime, DayOfWeek dayOfWeek)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(dateTime);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                dateTime = dateTime.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstFourDayWeek, dayOfWeek);
        }

        protected static int totalWeeksInMonth(DateTime dateTime, DayOfWeek dayOfWeek)
        {
            int weeks = 0;
            int month = dateTime.Month;
            int year = dateTime.Year;
            int daysThisMonth = DateTime.DaysInMonth(year, month);
            DateTime beginingOfThisMonth = new DateTime(year, month, 1);
            for (int i = 0; i < daysThisMonth; i++)
                if (beginingOfThisMonth.AddDays(i).DayOfWeek == dayOfWeek)
                    weeks++;
            return weeks;
        }
    }
}
