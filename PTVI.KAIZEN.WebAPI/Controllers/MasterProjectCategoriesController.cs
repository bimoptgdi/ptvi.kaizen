﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class MasterProjectCategoriesController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/MasterProjectCategories

        public IQueryable<masterProjectCategoryObject> GetMASTERPROJECTCATEGORies()
        {
            return db.MASTERPROJECTCATEGORies.Select(s => new masterProjectCategoryObject
            {
                id = s.OID,
                description = s.DESCRIPTION,
                isEmailNotif = s.ISEMAILNOTIF
            });
        }

        // GET: api/MasterProjectCategories/5
        [ResponseType(typeof(MASTERPROJECTCATEGORY))]
        public IHttpActionResult masterProjectCategoryObject(int id)
        {
            MASTERPROJECTCATEGORY mASTERPROJECTCATEGORY = db.MASTERPROJECTCATEGORies.Find(id);
            if (mASTERPROJECTCATEGORY == null)
            {
                return NotFound();
            }
            masterProjectCategoryObject result = new masterProjectCategoryObject();

            result.id = mASTERPROJECTCATEGORY.OID;
            result.description = mASTERPROJECTCATEGORY.DESCRIPTION;
            result.isEmailNotif = mASTERPROJECTCATEGORY.ISEMAILNOTIF;
            return Ok(result);
        }

        // PUT: api/MasterProjectCategories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMASTERPROJECTCATEGORY(int id, masterProjectCategoryObject mASTERPROJECTCATEGORY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mASTERPROJECTCATEGORY.id)
            {
                return BadRequest();
            }
            MASTERPROJECTCATEGORY update = db.MASTERPROJECTCATEGORies.Find(id);
            update.DESCRIPTION = mASTERPROJECTCATEGORY.description;
            update.ISEMAILNOTIF = mASTERPROJECTCATEGORY.isEmailNotif;
            update.UPDATEDDATE = DateTime.Now;
            update.UPDATEDBY = GetCurrentNTUserId();

            db.Entry(update).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MASTERPROJECTCATEGORYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MasterProjectCategories
        [ResponseType(typeof(MASTERPROJECTCATEGORY))]
        public IHttpActionResult PostMASTERPROJECTCATEGORY(masterProjectCategoryObject mASTERPROJECTCATEGORY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MASTERPROJECTCATEGORY add = new MASTERPROJECTCATEGORY();
            add.DESCRIPTION = mASTERPROJECTCATEGORY.description;
            add.ISEMAILNOTIF = mASTERPROJECTCATEGORY.isEmailNotif;
            add.CREATEDDATE = DateTime.Now;
            add.CREATEDBY = GetCurrentNTUserId();

            db.MASTERPROJECTCATEGORies.Add(add);
            db.SaveChanges();
            mASTERPROJECTCATEGORY.id = add.OID;

            return CreatedAtRoute("DefaultApi", new { id = add.OID }, mASTERPROJECTCATEGORY);
        }

        // DELETE: api/MasterProjectCategories/5
        [ResponseType(typeof(MASTERPROJECTCATEGORY))]
        public IHttpActionResult DeleteMASTERPROJECTCATEGORY(int id)
        {
            MASTERPROJECTCATEGORY mASTERPROJECTCATEGORY = db.MASTERPROJECTCATEGORies.Find(id);
            if (mASTERPROJECTCATEGORY == null)
            {
                return NotFound();
            }

            db.MASTERPROJECTCATEGORies.Remove(mASTERPROJECTCATEGORY);
            db.SaveChanges();

            return Ok(mASTERPROJECTCATEGORY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MASTERPROJECTCATEGORYExists(int id)
        {
            return db.MASTERPROJECTCATEGORies.Count(e => e.OID == id) > 0;
        }
    }
}
