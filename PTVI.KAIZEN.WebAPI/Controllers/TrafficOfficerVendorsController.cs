﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class TrafficOfficerVendorsController : ApiController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/TrafficOfficerVendors
        public IEnumerable<trafficOfficerVendorsObject> GetTRAFFICOFFICERVENDORs()
        {
            return db.TRAFFICOFFICERVENDORs.Where(w => w.ISACTIVE == true).Select(s => new trafficOfficerVendorsObject {
                id =s.OID,
                trafficOfficer=s.TRAFFICOFFICER,
                vendor = s.VENDOR
            });
        }

        // GET: api/TrafficOfficerVendors/5
        [ResponseType(typeof(TRAFFICOFFICERVENDOR))]
        public IHttpActionResult GetTRAFFICOFFICERVENDOR(int id)
        {
            TRAFFICOFFICERVENDOR tRAFFICOFFICERVENDOR = db.TRAFFICOFFICERVENDORs.Find(id);

            if (tRAFFICOFFICERVENDOR == null)
            {
                return NotFound();
            }

            trafficOfficerVendorsObject trafficOfficerVendors = new trafficOfficerVendorsObject();
            trafficOfficerVendors.id = tRAFFICOFFICERVENDOR.OID;
            trafficOfficerVendors.trafficOfficer = tRAFFICOFFICERVENDOR.TRAFFICOFFICER;
            trafficOfficerVendors.vendor = tRAFFICOFFICERVENDOR.VENDOR;

            return Ok(trafficOfficerVendors);
        }

        // PUT: api/TrafficOfficerVendors/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTRAFFICOFFICERVENDOR(int id, TRAFFICOFFICERVENDOR tRAFFICOFFICERVENDOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tRAFFICOFFICERVENDOR.OID)
            {
                return BadRequest();
            }

            db.Entry(tRAFFICOFFICERVENDOR).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TRAFFICOFFICERVENDORExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TrafficOfficerVendors
        [ResponseType(typeof(TRAFFICOFFICERVENDOR))]
        public IHttpActionResult PostTRAFFICOFFICERVENDOR(TRAFFICOFFICERVENDOR tRAFFICOFFICERVENDOR)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TRAFFICOFFICERVENDORs.Add(tRAFFICOFFICERVENDOR);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tRAFFICOFFICERVENDOR.OID }, tRAFFICOFFICERVENDOR);
        }

        // DELETE: api/TrafficOfficerVendors/5
        [ResponseType(typeof(TRAFFICOFFICERVENDOR))]
        public IHttpActionResult DeleteTRAFFICOFFICERVENDOR(int id)
        {
            TRAFFICOFFICERVENDOR tRAFFICOFFICERVENDOR = db.TRAFFICOFFICERVENDORs.Find(id);
            if (tRAFFICOFFICERVENDOR == null)
            {
                return NotFound();
            }

            db.TRAFFICOFFICERVENDORs.Remove(tRAFFICOFFICERVENDOR);
            db.SaveChanges();

            return Ok(tRAFFICOFFICERVENDOR);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TRAFFICOFFICERVENDORExists(int id)
        {
            return db.TRAFFICOFFICERVENDORs.Count(e => e.OID == id) > 0;
        }
    }
}