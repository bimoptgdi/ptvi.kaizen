﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using System.IO;
using Excel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class UploadExcelMiscelController : KAIZENController
    {
        //send parameter fileintegrationmisc oid
        [HttpGet]
        [ActionName("LoadMisc")]
        public string LoadMisc(int param)
        {
            var ntUser = GetCurrentNTUserId();
            var fin = db.FILEINTEGRATIONMISCs.Find(param);
            var finl = new FILEINTEGRATIONLOADERMISC();

            // File material excess location
            string fileName = fin.FILENAME;
            string path = fin.SHAREFOLDER;

            // Check file does exist or not
            if (!File.Exists(path + fileName))
            {
                finl.FILEINTEGRATIONMISCID = fin.OID;
                finl.REMARK = "File " + fileName + " does not exist.";
                finl.LOADERDATE = DateTime.Now;
                finl.LOADERBY = ntUser;
                db.Entry(finl).State = EntityState.Added;
                db.SaveChanges();
                throw new FileNotFoundException();
            }

            string fn = Path.GetFileNameWithoutExtension(fileName);

            switch (fn)
            {
                case constants.MATERIAL_EXCESS:
                    // if filename = Material Excess Form
                    LoadMatex(param, path, fileName, fin.SHEETNAME, ntUser);
                    break;
                default:
                    throw new ApplicationException("Wrong filename.");
            }
            return "Succeeded.";
        }

        private void LoadMatex(int id, string path, string fileName, string sheetName, string ntUser)
        {
            var finMatex = db.FILEINTEGRATIONMISCs.Find(id);
            var finlMatex = new FILEINTEGRATIONLOADERMISC();
            // Read excel,
            // start from column B row 3, 
            // finish at column AA, 
            // first row is 3rd row as colum name
            string startRowCol = "A3";
            string endCol = "AA";

            string localhost = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.InstanceName; //instance name sqlserver
            string idUser = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idUser; //User sqlserver
            string idCatalog = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idCatalog; //catalog sqlserver
            string idPassword = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.idPassword; //password sqlserver
            string destConnString = @"Password=" + idPassword + "; Persist Security Info=True; User ID=" + idUser + "; Initial Catalog=" + idCatalog + "; Data Source=" + localhost;

            string connStr = "";
            string excelConnStr03 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0 Xml;HDR=YES;IMEX=1\"";
            string excelConnStr07 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";

            string fileXtens = Path.GetExtension(path + fileName);
            //string sheetName = "";

            string[] cols = null;

            try
            {
                using (OleDbConnection conn = new OleDbConnection())
                {
                    if (fileXtens.ToUpper().Contains(".XLSX"))
                        connStr = string.Format(excelConnStr07, path + fileName);
                    else
                        connStr = string.Format(excelConnStr03, path + fileName);

                    conn.ConnectionString = connStr;
                    if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
                    {
                        conn.Open();
                        //var dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        //sheetName = dtSchema.Rows[0].Field<string>("TABLE_NAME");
                    }
                    //sheetName = 
                    OleDbCommand oleCom = new OleDbCommand(@"Select * from [" + sheetName + "$" + startRowCol + ":" + endCol + "]", conn);
                    DataTable table = new DataTable();
                    OleDbDataAdapter oleAdapter = new OleDbDataAdapter();
                    oleAdapter.SelectCommand = oleCom;
                    oleAdapter.Fill(table);

                    DataColumn createdDate = new DataColumn("CREATEDDATE", typeof(DateTime));
                    createdDate.DefaultValue = DateTime.Now;
                    DataColumn createdBy = new DataColumn("CREATEDBY", typeof(string));
                    createdBy.DefaultValue = ntUser;
                    table.Columns.Add(createdDate);
                    table.Columns.Add(createdBy);

                    cols = (from dc in table.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();

                    //// data validation
                    foreach (DataRow dr in table.Rows)
                    {
                        int DTNO;
                        if (!int.TryParse(dr[0].ToString(), out DTNO))
                        {
                            dr[cols[0]] = DBNull.Value;
                        }
                        float UNITPRICE;
                        if (!float.TryParse(dr[10].ToString(), out UNITPRICE))
                        {
                            dr[cols[10]] = DBNull.Value;
                        }
                        int QTY;
                        if (!int.TryParse(dr[11].ToString(), out QTY))
                        {
                            dr[cols[11]] = DBNull.Value;
                        }
                        float VALUE;
                        if (!float.TryParse(dr[12].ToString(), out VALUE))
                        {
                            dr[cols[12]] = DBNull.Value;
                        }
                        int ISSUEQTY;
                        if (!int.TryParse(dr[13].ToString(), out ISSUEQTY))
                        {
                            dr[cols[13]] = DBNull.Value;
                        }
                        float ISSUEVALUE;
                        if (!float.TryParse(dr[18].ToString(), out ISSUEVALUE))
                        {
                            dr[cols[18]] = DBNull.Value;
                        }
                        int BOOKINGQTY;
                        if (!int.TryParse(dr[19].ToString(), out BOOKINGQTY))
                        {
                            dr[cols[19]] = DBNull.Value;
                        }
                        float BOOKINGVALUE;
                        if (!float.TryParse(dr[24].ToString(), out BOOKINGVALUE))
                        {
                            dr[cols[24]] = DBNull.Value;
                        }
                        int BALANCEQTY;
                        if (!int.TryParse(dr[25].ToString(), out BALANCEQTY))
                        {
                            dr[cols[25]] = DBNull.Value;
                        }
                        int BALANCEVALUE;
                        if (!int.TryParse(dr[26].ToString(), out BALANCEVALUE))
                        {
                            dr[cols[26]] = DBNull.Value;
                        }
                        table.AcceptChanges();
                    }
                    //// end data validation

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destConnString))
                    {
                        bulkCopy.DestinationTableName = "dbo.MATERIALEXCESSTEMP";

                        bulkCopy.ColumnMappings.Add(cols[0], "DTNO");
                        bulkCopy.ColumnMappings.Add(cols[1], "STOCKCODE");
                        bulkCopy.ColumnMappings.Add(cols[2], "MATERIALGROUP");
                        bulkCopy.ColumnMappings.Add(cols[3], "ITEMDESCRIPTION");
                        bulkCopy.ColumnMappings.Add(cols[4], "PO");
                        bulkCopy.ColumnMappings.Add(cols[5], "MR");
                        bulkCopy.ColumnMappings.Add(cols[6], "PROJECTNO");
                        bulkCopy.ColumnMappings.Add(cols[7], "STORELOCATION");
                        bulkCopy.ColumnMappings.Add(cols[8], "BINLOCATION");
                        bulkCopy.ColumnMappings.Add(cols[9], "UOM");
                        bulkCopy.ColumnMappings.Add(cols[10], "UNITPRICE");
                        bulkCopy.ColumnMappings.Add(cols[11], "QTY");
                        bulkCopy.ColumnMappings.Add(cols[12], "VALUE");
                        bulkCopy.ColumnMappings.Add(cols[13], "ISSUEQTY");
                        bulkCopy.ColumnMappings.Add(cols[14], "ISSUEWBS");
                        bulkCopy.ColumnMappings.Add(cols[15], "ISSUEMIR");
                        bulkCopy.ColumnMappings.Add(cols[16], "ISSUEBN");
                        bulkCopy.ColumnMappings.Add(cols[17], "ISSUEUSER");
                        bulkCopy.ColumnMappings.Add(cols[18], "ISSUEVALUE");
                        bulkCopy.ColumnMappings.Add(cols[19], "BOOKINGQTY");
                        bulkCopy.ColumnMappings.Add(cols[20], "BOOKINGWBS");
                        bulkCopy.ColumnMappings.Add(cols[21], "BOOKINGMIR");
                        bulkCopy.ColumnMappings.Add(cols[22], "BOOKINGBN");
                        bulkCopy.ColumnMappings.Add(cols[23], "BOOKINGUSER");
                        bulkCopy.ColumnMappings.Add(cols[24], "BOOKINGVALUE");
                        bulkCopy.ColumnMappings.Add(cols[25], "BALANCEQTY");
                        bulkCopy.ColumnMappings.Add(cols[26], "BALANCEVALUE");
                        bulkCopy.ColumnMappings.Add(cols[27], "CREATEDDATE");
                        bulkCopy.ColumnMappings.Add(cols[28], "CREATEDBY");

                        bulkCopy.WriteToServer(table);
                        bulkCopy.Close();

                    }
                    conn.Close();
                    conn.Dispose();
                    oleAdapter.Dispose();
                    db.sp_materialexcess();

                    // insert file integaration loader misc
                    finlMatex.FILEINTEGRATIONMISCID = finMatex.OID;
                    finlMatex.REMARK = "Succeeded";
                    finlMatex.LOADERBY = ntUser;
                    finlMatex.LOADERDATE = DateTime.Now;
                    db.Entry(finlMatex).State = EntityState.Added;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class matexParam
        {
            public string prjNo { get; set; }
            public string prjDescrip { get; set; }
        }
        
        public HttpResponseMessage matexInquiry(matexParam prm)
        {
            string prjNo = prm.prjNo;
            string prjDescrip = prm.prjDescrip;
            var prjs = db.PROJECTs.Where(x => (string.IsNullOrEmpty(prjNo) ? true : x.PROJECTNO.Contains(prjNo)) && (string.IsNullOrEmpty(prjDescrip) ? true : x.PROJECTDESCRIPTION.Contains(prjDescrip))).Select(y=>y.PROJECTNO);
            var matexs = db.MATERIALEXCESSes.Where(x => prjs.Contains(x.PROJECTNO));
            return Request.CreateResponse(HttpStatusCode.OK,matexs);
        }
        public HttpResponseMessage Get([FromUri] matexParam prm)
        {
            try
            {
                return matexInquiry(prm);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, ex);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
