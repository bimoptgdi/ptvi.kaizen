﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTGDI.Email;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class valePersonnels
    {
        public string POS_TITLE { get; set; }
        public string EMPLOYEE_ID { get; set; }
        public string EMPLOYEEID { get; set; }
        public string BADGENO { get; set; }
        public string FULL_NAME { get; set; }
        public string EMPLOYEE_NAME{ get; set; }
        public string USERNAME { get; set; }
        public string EMAIL { get; set; }
        public string DEPT_CODE { get; set; }
        public string POSITION_ID { get; set; }
        public string POSITIONID { get; set; }
        public string SUBDEPT_CODE { get; set; }
    }
    public class UserAssignmentController : KAIZENController
    {
        private AccountController accController = new AccountController();
        // GET api/<controller>
        public IQueryable<userAssignmentObject> Get()
        {
            IQueryable<userAssignmentObject> uAObj;
            uAObj = db.USERASSIGNMENTs.Select(o => new userAssignmentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                technicalSupportId = o.TECHNICALSUPPORTID,
                assignmentType = o.ASSIGNMENTTYPE,
                employeeId = o.EMPLOYEEID,
                employeeName = o.EMPLOYEENAME,
                employeeEmail = o.EMPLOYEEEMAIL,
                employeePosition = o.EMPLOYEEPOSITION,
                joinedDate = o.JOINEDDATE,
                isActive = o.ISACTIVE,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
                //project { get; set; }
                //technicalsupport { get; set; }

            });

            return uAObj;
        }

        public IQueryable<userAssignmentObject> GetByAssType(int param, string AssignmentType)
        {
            IQueryable<userAssignmentObject> uAObj;
            uAObj = db.USERASSIGNMENTs.Where(x=>x.ASSIGNMENTTYPE==AssignmentType).Select(o => new userAssignmentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                technicalSupportId = o.TECHNICALSUPPORTID,
                assignmentType = o.ASSIGNMENTTYPE,
                employeeId = o.EMPLOYEEID,
                employeeName = o.EMPLOYEENAME,
                employeeEmail = o.EMPLOYEEEMAIL,
                employeePosition = o.EMPLOYEEPOSITION,
                joinedDate = o.JOINEDDATE,
                isActive = o.ISACTIVE,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
                //project { get; set; }
                //technicalsupport { get; set; }

            });

            return uAObj;
        }

        [HttpGet]
        [ActionName("otherPostionOnProject")]
        public IEnumerable<userAssignmentObject> otherPostionOnProject(int param)
        {
            var assignmentType = db.LOOKUPs.Where(w => w.TYPE == constants.otherPositionProject).Select(s => s.VALUE).ToList();
            return db.USERASSIGNMENTs.Where(x => (assignmentType.Contains(x.ASSIGNMENTTYPE) && x.PROJECTID == param)).Select(o => new userAssignmentObject()
            {
                id = o.OID,
                employeeId = o.EMPLOYEEID,
                employeeName = o.EMPLOYEENAME,
                employeeEmail = o.EMPLOYEEEMAIL,
                employeePosition = o.EMPLOYEEPOSITION,
                assignmentType = o.ASSIGNMENTTYPE
            });
        }


        [HttpGet]
        [ActionName("GetByAssTypePrID")]
        public IQueryable<userAssignmentObject> GetByAssTypePrID(int param, string AssignmentType, int PrID)
        {
            IQueryable<userAssignmentObject> uAObj;

            uAObj = db.USERASSIGNMENTs.Where(x => (AssignmentType.Contains(x.ASSIGNMENTTYPE)  && x.PROJECTID==PrID)).Select(o => new userAssignmentObject()
            {
                id = o.OID,
                projectId = o.PROJECTID,
                technicalSupportId = o.TECHNICALSUPPORTID,
                assignmentType = o.ASSIGNMENTTYPE,
                employeeId = o.EMPLOYEEID,
                employeeName = o.EMPLOYEENAME,
                employeeEmail = o.EMPLOYEEEMAIL,
                employeePosition = o.EMPLOYEEPOSITION,
                joinedDate = o.JOINEDDATE,
                isActive = o.ISACTIVE,
                createdDate = o.CREATEDDATE,
                createdBy = o.CREATEDBY,
                updatedDate = o.UPDATEDDATE,
                updatedBy = o.UPDATEDBY
                //project { get; set; }
                //technicalsupport { get; set; }

            });

            return uAObj;
        }


        // POST api/<controller>
        [HttpPost]
        [ActionName("postUserAssignmentObject")]
        public HttpResponseMessage postUserAssignmentObject(int param, userAssignmentObject uAObj)
        {
            EmailSender emailSender = new EmailSender();
            var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
            if (ModelState.IsValid)
            {
                if (db.USERASSIGNMENTs.Where(x => (x.EMPLOYEEID == uAObj.employeeId && x.ASSIGNMENTTYPE == uAObj.assignmentType && x.PROJECTID==uAObj.projectId) ).Count() < 1)
                {
                    var user = new USERASSIGNMENT()
                    {
                        OID = 0,
                        PROJECTID = uAObj.projectId,
                        TECHNICALSUPPORTID = null,
                        ASSIGNMENTTYPE = uAObj.assignmentType,
                        EMPLOYEEID = uAObj.employeeId,
                        EMPLOYEENAME = uAObj.employeeName,
                        EMPLOYEEEMAIL = uAObj.employeeEmail,
                        EMPLOYEEPOSITION = uAObj.employeePosition,
                        JOINEDDATE = uAObj.joinedDate,
                        ISACTIVE = uAObj.isActive,
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = uAObj.createdBy,
                        UPDATEDDATE = DateTime.Now,
                        UPDATEDBY = uAObj.updatedBy
                    };
                    db.USERASSIGNMENTs.Add(user);
                    db.SaveChanges();
                    uAObj.id = user.OID;

                    if (uAObj.isActive == true)
                    {
                        var s = db.USERASSIGNMENTs.Where(x => (x.EMPLOYEEID != uAObj.employeeId && x.ASSIGNMENTTYPE == uAObj.assignmentType && x.PROJECTID == uAObj.projectId 
                        && (x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || x.ASSIGNMENTTYPE == constants.PROJECT_OFFICER)));
                        foreach(var ass in s)
                        {
                            ass.ISACTIVE = false;
                        }
                        db.SaveChanges();
                    }

                    //send email
                    if (uAObj.employeeEmail != null)
                    {
                        var assType = (uAObj.assignmentType == constants.PROJECT_ENGINEER || uAObj.assignmentType == constants.PROJECT_DESIGNER) ? constants.engineerDesigner :
                        (uAObj.assignmentType == constants.PROJECT_PROJECT_CONTROLLER || uAObj.assignmentType == constants.PROJECT_MATERIAL_COORDINATOR || uAObj.assignmentType == constants.PROJECT_OFFICER) ? constants.projectService :
                        (uAObj.assignmentType == constants.TECHNICAL_SUPPORT_ENGINEER || uAObj.assignmentType == constants.TECHNICAL_SUPPORT_DESIGNER) ? constants.engineerDesignerTS : "";
                        var mail = db.EMAILREDACTIONs.Where(x => x.CODE == assType).FirstOrDefault();
                        var projectRequest = db.PROJECTs.Where(x => x.OID == uAObj.projectId).FirstOrDefault();
                        var mailList = new List<usersToEmail>() {
                        new usersToEmail(){ userName = projectRequest.OWNERNAME, email =  projectRequest.OWNEREMAIL, position = constants.posOwner },
                        new usersToEmail(){userName = projectRequest.SPONSORNAME, email = projectRequest.SPONSOREMAIL, position = constants.posSponsor  },
                        new usersToEmail(){userName = projectRequest.PROJECTMANAGERNAME, email = projectRequest.PROJECTMANAGEREMAIL, position = constants.posProjectManager  } ,
                        new usersToEmail(){userName = projectRequest.PROJECTENGINEERNAME, email = projectRequest.PROJECTENGINEEREMAIL, position = constants.posProjectEngineer  },
                        new usersToEmail(){userName = projectRequest.MAINTENANCEREPNAME, email = projectRequest.MAINTENANCEREPEMAIL, position = constants.posMaintenanceRep  },
                        new usersToEmail(){userName = projectRequest.OPERATIONREPNAME, email = projectRequest.OPERATIONREPEMAIL, position = constants.posOperationRep  }
                        };
                        var ua = db.USERASSIGNMENTs.Where(x => (x.PROJECTID == uAObj.projectId && x.ISACTIVE == true)).OrderBy(z => z.ASSIGNMENTTYPE);
                        foreach (var u in ua)
                        {
                            var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                            if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                            if (u.EMPLOYEEID != uAObj.employeeId)
                            {
                                mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = u.EMPLOYEEEMAIL, position = posName != null ? posName.DESCRIPTION.ToUpper() : "" });
                            }
                        }
                        string RecipientEmail = uAObj.employeeEmail;
                        string userList = "<table style='width:550px'>";
                        foreach (var l in mailList)
                        {
                            userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
                        }
                        userList = userList + "</table>";

                        string RecipientName = uAObj.employeeEmail;
                        string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
                        var ccDetail = GetListFromExternalData<valePersonnels>(urlCommonService + "externaldata/executequerybyparam?param=QRY_VALEPERSONNELS&$filter=USERNAME%20eq%20%27" + uAObj.createdBy + "%27").FirstOrDefault();

                        var RecipientCC = ccDetail.EMAIL;
                        var FromEmail = "testing@ptgdi.com";
                        var subject = mail.SUBJECT;
                        var body = mail.BODY;
                        var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno=" + uAObj.employeeId).FirstOrDefault();
                        string prefixGender = "";
                        if (employeeDetail != null)
                        {
                            if (employeeDetail != null && employeeDetail.GENDER != null)
                            {
                                if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                                    prefixGender = constants.prefixmale;
                                else
                                    if (employeeDetail.MARITAL_STATUS == constants.married)
                                    prefixGender = constants.prefixfemaleMarried;
                                else
                                    prefixGender = constants.prefixfemaleSingle;
                            }
                            else prefixGender = "Mr\\Mrs";
                            body = body.Replace("[user]", prefixGender + " " + uAObj.employeeName);
                            var n = body.IndexOf("<p id='assignedstatus'>");
                            var m = body.IndexOf("</p>", n) + 4;
                            var str = body.Substring(n, m - n);
                            var asstype = db.LOOKUPs.Where(x => x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == uAObj.assignmentType).FirstOrDefault().DESCRIPTION;
                            body = body.Replace("[assignmenttype]", asstype);
                            body = body.Replace("[projectNo]", projectRequest.PROJECTNO);
                            body = body.Replace("[projectDesc]", projectRequest.PROJECTDESCRIPTION);
                            body = body.Replace("[planStartDate]", projectRequest.PLANSTARTDATE.HasValue ? projectRequest.PLANSTARTDATE.Value.ToString("dd MMMM yyyy") : "-");
                            body = body.Replace("[planFinishDate]", projectRequest.PLANFINISHDATE.HasValue ? projectRequest.PLANFINISHDATE.Value.ToString("dd MMMM yyyy") : "-");
                            body = body.Replace("[listuser]", userList);
                            body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/MainProjectForm.aspx?projectId=" + projectRequest.OID);
                            body = body.Replace("[assign]", accController.getEmployeeByBN(uAObj.createdBy).full_Name);
                            body = body.Replace("\r", "");
                            body = body.Replace("\n", "");
                            body = body.Replace("\\", "");
                            emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);
                        }
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, uAObj);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = uAObj.id }));
                    return response;
                } else
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Employee ID Exist");
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", 0));
                    return response;
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [ActionName("PutUserAssignmentObject")]
        public HttpResponseMessage PutUserAssignmentObject(int param, userAssignmentObject uAObj)
        {
            EmailSender emailSender = new EmailSender();
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (param != uAObj.id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            bool deactivate=false;
            bool activeChange = false;
            var userObj = db.USERASSIGNMENTs.Where(x => x.OID == uAObj.id).FirstOrDefault();
                userObj.PROJECTID = uAObj.projectId;
                userObj.TECHNICALSUPPORTID = null;
                userObj.ASSIGNMENTTYPE = uAObj.assignmentType;
                userObj.EMPLOYEEID = uAObj.employeeId;
                userObj.EMPLOYEENAME = uAObj.employeeName;
                userObj.EMPLOYEEEMAIL = uAObj.employeeEmail;
                userObj.EMPLOYEEPOSITION = uAObj.employeePosition;
                userObj.JOINEDDATE = uAObj.joinedDate;
            if (userObj.ISACTIVE != uAObj.isActive)
                activeChange = true;
            if (userObj.ISACTIVE == true && uAObj.isActive == false)
                deactivate = true;
                userObj.ISACTIVE = uAObj.isActive;
                userObj.UPDATEDDATE = DateTime.Now;
                userObj.UPDATEDBY = uAObj.updatedBy;
                db.Entry(userObj).State = EntityState.Modified;
            if (uAObj.isActive == true)
            {
                var s = db.USERASSIGNMENTs.Where(x => (x.EMPLOYEEID != uAObj.employeeId && x.ASSIGNMENTTYPE == uAObj.assignmentType && x.PROJECTID == uAObj.projectId
                && (x.ASSIGNMENTTYPE == constants.PROJECT_PROJECT_CONTROLLER || x.ASSIGNMENTTYPE == constants.PROJECT_MATERIAL_COORDINATOR || x.ASSIGNMENTTYPE == constants.PROJECT_OFFICER)));
                foreach (var ass in s)
                {
                    ass.ISACTIVE = false;
                }
               // db.SaveChanges();
            }
            try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
                }
            if(activeChange==true)
            {
                //send email
                var mail = db.EMAILREDACTIONs.Where(x => x.CODE == constants.engineerDesigner).FirstOrDefault();
                var projectRequest = db.PROJECTs.Where(x => x.OID == uAObj.projectId).FirstOrDefault();
                var mailList = new List<usersToEmail>() {
                        new usersToEmail(){ userName = projectRequest.OWNERNAME, email =  "", position = constants.posOwner },
                        new usersToEmail(){userName = projectRequest.SPONSORNAME, email = "", position = constants.posSponsor  },
                        new usersToEmail(){userName = projectRequest.PROJECTMANAGERNAME, email = "", position = constants.posProjectManager  } ,
                        new usersToEmail(){userName = projectRequest.PROJECTENGINEERNAME, email = "", position = constants.posProjectEngineer  },
                        new usersToEmail(){userName = projectRequest.MAINTENANCEREPNAME, email = "", position = constants.posMaintenanceRep  },
                        new usersToEmail(){userName = projectRequest.OPERATIONREPNAME, email = "", position = constants.posOperationRep  }
                    };
                var ua = db.USERASSIGNMENTs.Where(x => (x.PROJECTID == uAObj.projectId && x.ISACTIVE == true)).OrderBy(z=>z.ASSIGNMENTTYPE);
                foreach (var u in ua)
                {
                    var posName = db.LOOKUPs.Where(x => (x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                    if (posName == null) posName = db.LOOKUPs.Where(x => (x.TYPE == constants.otherPositionProject && x.VALUE == u.ASSIGNMENTTYPE)).FirstOrDefault();
                    if(u.EMPLOYEEID != uAObj.employeeId)
                        mailList.Add(new usersToEmail() { userName = u.EMPLOYEENAME, email = u.EMPLOYEEEMAIL, position = posName != null ? posName.DESCRIPTION : "" });
                }
                string RecipientEmail = uAObj.employeeEmail;
                string userList = "<table>";
                foreach (var l in mailList)
                {
                    userList = userList + "<tr><td>" + l.position + "</td><td>" + l.userName + "</td></tr>";
                }
                userList = userList + "</table>";
                string RecipientName = uAObj.employeeEmail;
                string RecipientPos = string.Join(";", mailList.Select(x => x.position).ToArray());
                var urlCommonService = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.UrlCommonService;
                var ccDetail = GetListFromExternalData<valePersonnels>(urlCommonService + "externaldata/executequerybyparam?param=QRY_VALEPERSONNELS&$filter=USERNAME%20eq%20%27" + uAObj.updatedBy + "%27").FirstOrDefault();

                var RecipientCC = ccDetail.EMAIL;
                var FromEmail = "testing@ptgdi.com";
                var subject = mail.SUBJECT;
                var body = mail.BODY;
                var employeeDetail = GetListFromExternalData<employeeDetails>(urlCommonService + "externaldata/executequerybyparam/QRY_PERSONALDETAIL%7C@badgeno="+uAObj.employeeId).FirstOrDefault();
                string prefixGender="";
                if (employeeDetail != null)
                {
                    if (employeeDetail.GENDER.ToUpper() == constants.male.ToUpper())
                        prefixGender = constants.prefixmale;
                    else
                        if (employeeDetail.MARITAL_STATUS == constants.married)
                        prefixGender = constants.prefixfemaleMarried;
                    else
                        prefixGender = constants.prefixfemaleSingle;
                    body = body.Replace("[user]", prefixGender + " " + uAObj.employeeName);
                    var n = body.IndexOf("<p id='assignedstatus'>");
                    var m = body.IndexOf("</p>", n) + 4;
                    var str = body.Substring(n, m - n);
                    var asstype = db.LOOKUPs.Where(x => x.TYPE == constants.ASSIGNMENTTYPE && x.VALUE == uAObj.assignmentType).FirstOrDefault().DESCRIPTION;
                    if (deactivate == true) body = body.Replace(str, "<p style='background-color: red; color:white;text-align: center; font-size: 14'>You are no longer involved in this project as " + asstype + "</p>");
                    body = body.Replace("[assignmenttype]", asstype);
                    body = body.Replace("[projectNo]", projectRequest.PROJECTNO);
                    body = body.Replace("[projectDesc]", projectRequest.PROJECTDESCRIPTION);
                    body = body.Replace("[planStartDate]", projectRequest.PLANSTARTDATE.HasValue ? projectRequest.PLANSTARTDATE.Value.ToString("dd MMMM yyyy") : "-");
                    body = body.Replace("[planFinishDate]", projectRequest.PLANFINISHDATE.HasValue ? projectRequest.PLANFINISHDATE.Value.ToString("dd MMMM yyyy") : "-");
                    body = body.Replace("[listuser]", userList);
                    n = body.IndexOf("<td id='pagelink'");
                    n = body.IndexOf(">", n);
                    m = body.IndexOf("</td>", n);
                    str = body.Substring(n, m - n);
                    if (deactivate == true)
                        body = body.Replace(str,
                            "&nbsp;");
                    else
                        body = body.Replace("[pagelink]", global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.KAIZEN_WebApp_BaseUrl + "/MainProjectForm.aspx?projectId=" + projectRequest.OID);
                    body = body.Replace("[assign]", accController.getEmployeeByBN(uAObj.createdBy).full_Name);

                    body = body.Replace("\r\n", "");
                    body = body.Replace("\\", "");
                    emailSender.SendMail(RecipientEmail, RecipientCC, FromEmail, subject, body);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, uAObj);
        }


    }
}