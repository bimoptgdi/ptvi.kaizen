﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using Microsoft.Data.OData;
using System.Configuration;
using System.Web;
using System.Web.Configuration;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class EmployeesController : ODataController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        private KAIZENController controller = new KAIZENController();

        [EnableQuery]
        public IQueryable<employeeObject> Get()
        {
            try
            {
                GENERALPARAMETER generalParamUrl = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.COMMON_SERVICE_API_URL);
                GENERALPARAMETER generalParamExtQry = db.GENERALPARAMETERs.FirstOrDefault(d => d.TITLE == constants.CODE_EMPLOYEE_EXTERNALQRY);
                
                if (generalParamUrl != null && generalParamExtQry != null)
                {
                    List<employeeObject> result = controller.GetListFromExternalData<employeeObject>(generalParamUrl.VALUE + generalParamExtQry.VALUE); // untuk production/development server

                    return result.Where(d => d.employeeId != "-").AsQueryable<employeeObject>();
                }
            }
            catch (ODataException ex)
            {
                return null;
            }

            // return Ok<IEnumerable<EMPLOYEE>>(eMPLOYEEs);
            return null;

        }
    }
}
