﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;
using PTGDI.Email;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class RequestToTheUsersController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();
        private AccountController accController = new AccountController();
        protected bool _debugMode = Convert.ToBoolean(global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL);
        protected String _debugEmailTo = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL_TO;
        protected String _debugEmailCc = global::PTVI.KAIZEN.WebAPI.Properties.Settings.Default.DEBUG_EMAIL_CC;

        // GET: api/RequestToTheUsers
        public IQueryable<REQUESTOTHERUSER> GetREQUESTOTHERUSERs()
        {
            return db.REQUESTOTHERUSERs;
        }

        // GET: api/RequestToTheUsers/5
        [ResponseType(typeof(REQUESTOTHERUSER))]
        public async Task<IHttpActionResult> GetREQUESTOTHERUSER(int id)
        {
            REQUESTOTHERUSER rEQUESTOTHERUSER = await db.REQUESTOTHERUSERs.FindAsync(id);
            if (rEQUESTOTHERUSER == null)
            {
                return NotFound();
            }

            return Ok(rEQUESTOTHERUSER);
        }

        // PUT: api/RequestToTheUsers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutREQUESTOTHERUSER(int id, REQUESTOTHERUSER rEQUESTOTHERUSER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rEQUESTOTHERUSER.OID)
            {
                return BadRequest();
            }

            db.Entry(rEQUESTOTHERUSER).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!REQUESTOTHERUSERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RequestToTheUsers
        [ResponseType(typeof(REQUESTOTHERUSER))]
        public async Task<IHttpActionResult> PostREQUESTOTHERUSER(REQUESTOTHERUSER rEQUESTOTHERUSER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.REQUESTOTHERUSERs.Add(rEQUESTOTHERUSER);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = rEQUESTOTHERUSER.OID }, rEQUESTOTHERUSER);
        }

        // DELETE: api/RequestToTheUsers/5
        [ResponseType(typeof(REQUESTOTHERUSER))]
        public async Task<IHttpActionResult> DeleteREQUESTOTHERUSER(int id)
        {
            REQUESTOTHERUSER rEQUESTOTHERUSER = await db.REQUESTOTHERUSERs.FindAsync(id);
            if (rEQUESTOTHERUSER == null)
            {
                return NotFound();
            }

            db.REQUESTOTHERUSERs.Remove(rEQUESTOTHERUSER);
            await db.SaveChangesAsync();

            return Ok(rEQUESTOTHERUSER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool REQUESTOTHERUSERExists(int id)
        {
            return db.REQUESTOTHERUSERs.Count(e => e.OID == id) > 0;
        }

        [HttpGet]
        [ActionName("RequestList")]
        public IQueryable<requestOtherUserObject> RequestList(int param)
        {
            var sru = db.REQUESTOTHERUSERs.Where(y => y.PROJECTID==param).Select(x => new requestOtherUserObject()
            {
                id = x.OID,
                mailBody = x.MAILBODY,
                mailFromId = x.MAILFROMID,
                mailFromName = x.MAILFROMNAME,
                mailFrom = x.MAILFROM,
                mailSubject = x.MAILSUBJECT,
                mailToId = x.MAILTOID,
                mailToName = x.MAILTONAME,
                mailTo = x.MAILTO,
                mailCcId = x.MAILCCID,
                mailCcName = x.MAILCCNAME,
                mailCc = x.MAILCC,
                priority = x.PRIORITY,
                projectId = x.PROJECTID,
                requestDate = x.REQUESTDATE,
                requestNo = x.PROJECT.PROJECTNO+"-"+x.REQUESTNO,
                responseRequiredBy = x.RESPONSEREQUIREDBY,
                status = x.STATUS,
                priorityText = db.LOOKUPs.Where(y => y.TYPE == "requestListPriority" && y.VALUE==x.PRIORITY).Select(z=>z.TEXT).FirstOrDefault()
            }).OrderBy(m=>new { m.status, m.responseRequiredBy}).OrderBy(o => o.status != constants.STATUSCS_COMPLETED ? o.requestDate : o.responseRequiredBy);
            return sru; 
        }


        [HttpPost]
        [ActionName("SendRequest")]
        public HttpStatusCode SendRequest(int param, requestOtherUserObject rttu) // string adrURL, string recipientName, string recipientEmail, string remark, string statusCategory, string emailBody)
        {
            string emailFrom = rttu.mailFrom;
            string recipientName = "";
            string recipientEmail = rttu.mailTo;
            string Cc = rttu.mailCc;
            string body = rttu.mailBody;
            string subject = rttu.mailSubject;
            rttu.project = db.PROJECTs.Where(x => x.OID == rttu.projectId).Select(s => new projectObject()
            {
                projectNo = s.PROJECTNO,
                projectDescription = s.PROJECTDESCRIPTION
            }).FirstOrDefault();
            rttu.priorityText = db.LOOKUPs.Where(x => x.TYPE == constants.requestListPriority && x.VALUE == rttu.priority).FirstOrDefault().TEXT;
            var header = "";
            header = header + "You receive a request on project: <br/>" + rttu.project.projectNo + " - " + rttu.project.projectDescription + "<br/>";
            header = header + "<br/> From: " + accController.getEmployeeByBN(rttu.createdBy).full_Name;
            header = header + "<br/> Respond required by: " + rttu.responseRequiredBy.Value.ToString("dd MMMM yyyy");
            header = header + "<br/> Priority: " + rttu.priorityText+ "<br/>Message / request:<br/> <hr/>";
            body = body.Insert(0, header);
            body = body + "<hr/><br/><br/><br/>";
            var res = SendRequestToUser(emailFrom, Cc, recipientName, recipientEmail, subject, body);
            if(res == 1)    
            {
                var sru = new REQUESTOTHERUSER();
                var dt = DateTime.Now;
                var reqNo = string.Format("{0:yyyMMdd}",dt);
                var seq = db.REQUESTOTHERUSERs.Where(x => x.PROJECTID == rttu.projectId).ToList().Where(x=> x.CREATEDDATE.Value.Date == DateTime.Now.Date).Count() + 1; ;
                sru.REQUESTDATE = dt;
                sru.REQUESTNO = reqNo+ seq.ToString().PadLeft(3,'0');
                sru.PROJECTID = rttu.projectId;
                sru.CREATEDBY = rttu.createdBy;
                sru.CREATEDDATE = DateTime.Now;
                sru.MAILBODY = rttu.mailBody;
                sru.MAILFROMID = rttu.mailFromId;
                sru.MAILFROMNAME = rttu.mailFromName;
                sru.MAILFROM = rttu.mailFrom;
                sru.MAILSUBJECT = rttu.mailSubject;
                sru.MAILTOID = rttu.mailToId;
                sru.MAILTONAME = rttu.mailToName;
                sru.MAILTO = rttu.mailTo;
                sru.MAILCCID = rttu.mailCcId;
                sru.MAILCCNAME = rttu.mailCcName;
                sru.MAILCC = rttu.mailCc;
                sru.OID = 0;
                sru.PRIORITY = rttu.priority;
                sru.RESPONSEREQUIREDBY = rttu.responseRequiredBy;
                sru.UPDATEDBY = rttu.createdBy;
                sru.UPDATEDDATE = DateTime.Now;
                db.REQUESTOTHERUSERs.Add(sru);
                db.SaveChanges();
                return HttpStatusCode.OK;
            }
            else
            return HttpStatusCode.BadRequest;
        }

        [HttpPut]
        [ActionName("updateRequest")]
        public HttpResponseMessage updateRequest(int param, requestOtherUserObject rttu)
        {
                var sru = db.REQUESTOTHERUSERs.Where(x=>x.OID==param).FirstOrDefault();
                sru.STATUS = rttu.status;
                sru.UPDATEDBY = rttu.updatedBy;
                sru.UPDATEDDATE = DateTime.Now;
                db.Entry(sru).State = EntityState.Modified;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, rttu);
        }

        public int SendRequestToUser(string emailFrom, string emailCc, string recipientName, string recipientEmail, string subject, string body)
        {
            // Define variable
            var strRecipient = string.Empty;
            var strRecipientCC = string.Empty;

            // 1. Prepare email engine
            EmailSender emailSender = new EmailSender();
//            string emailFrom = System.Configuration.ConfigurationManager.AppSettings["EMAIL_SENDER_ADDRESS"];

  //          AccountController accController = new AccountController();

            // 2. Prepare email body
            string emailBody = body;
            if (!string.IsNullOrEmpty(emailBody))
            {

                // if debug mode is on
                // change recipient with email debug
                //if (_debugMode)
                //{
                //    strRecipient = _debugEmailTo;
                //    strRecipientCC = _debugEmailCc;
                //}

                //                if (! _debugMode)

                emailSender.SendMail(recipientEmail, emailCc, emailFrom, "[iProM] New Request: " + subject, emailBody);
                return 1;
            }
            else
            {
                throw new Exception("E-mail body is not found in the system. Please contact your administrator");
            }

        }

    }
}