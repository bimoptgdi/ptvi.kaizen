﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PTVI.KAIZEN.WebAPI.Models;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class AccessrightController : KAIZENController
    {
        // GET api/Accessright
        public IEnumerable<ACCESSRIGHT> GetACCESSRIGHTs()
        {
            IEnumerable<ACCESSRIGHT> listAccessRight = db.ACCESSRIGHTs;
            foreach (ACCESSRIGHT accessRight in listAccessRight)
            {
                var temp4 = accessRight.ROLEACCESSRIGHTs;
                accessRight.ROLEACCESSRIGHTs = null;
                IEnumerable<MENUACCESSRIGHT> listMenuAccessRight = accessRight.MENUACCESSRIGHTs;
                foreach (MENUACCESSRIGHT menuAccessRight in listMenuAccessRight)
                {
                    var temp5 = menuAccessRight.ACCESSRIGHT;
                    menuAccessRight.ACCESSRIGHT = null;

                }
            }
            
            return listAccessRight.AsEnumerable();
        }

        // GET api/Accessright/5
        public ACCESSRIGHT GetACCESSRIGHT(int id)
        {
            ACCESSRIGHT accessright = db.ACCESSRIGHTs.Find(id);
            var temp4 = accessright.ROLEACCESSRIGHTs;
            accessright.ROLEACCESSRIGHTs = null;
            IEnumerable<MENUACCESSRIGHT> listMenuAccessRight = accessright.MENUACCESSRIGHTs;
            foreach (MENUACCESSRIGHT menuAccessRight in listMenuAccessRight)
            {
                var temp5 = menuAccessRight.ACCESSRIGHT;
                menuAccessRight.ACCESSRIGHT = null;

            }
            if (accessright == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return accessright;
        }

        // PUT api/Accessright/5
        public HttpResponseMessage PutACCESSRIGHT(int id, ACCESSRIGHT accessright)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != accessright.OID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(accessright).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, accessright);
        }

        // POST api/Accessright
        public HttpResponseMessage PostACCESSRIGHT(ACCESSRIGHT accessright)
        {
            if (ModelState.IsValid)
            {
                db.ACCESSRIGHTs.Add(accessright);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, accessright);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = accessright.OID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Accessright/5
        public HttpResponseMessage DeleteACCESSRIGHT(int id)
        {
            ACCESSRIGHT accessright = db.ACCESSRIGHTs.Find(id);
            if (accessright == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.ACCESSRIGHTs.Remove(accessright);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, accessright);
        }

        [HttpPost]
        [ActionName("UpdateAccessRight")]
        public HttpResponseMessage UpdateAccessRight(int param, [FromBody] ACCESSRIGHT accessRight)
        {
            ACCESSRIGHT currAccessRight = db.ACCESSRIGHTs.Find(accessRight.OID);
            if (currAccessRight != null)
            {
                // cek apakah sudah ada description yang sama
                int countAccessRight = db.ACCESSRIGHTs.Count(d => d.DESCRIPTION == accessRight.DESCRIPTION);
                if (countAccessRight > 0)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Description is already exist.");
                }
                else
                {
                    currAccessRight.DESCRIPTION = accessRight.DESCRIPTION;
                    db.Entry(currAccessRight).State = EntityState.Modified;

                    db.SaveChanges();
                }
                
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}