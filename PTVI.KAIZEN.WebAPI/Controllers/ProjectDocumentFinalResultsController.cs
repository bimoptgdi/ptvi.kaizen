﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PTVI.KAIZEN.WebAPI.Models;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI.Controllers
{
    public class ProjectDocumentFinalResultsController : KAIZENController
    {
        private VALE_KAIZENEntities db = new VALE_KAIZENEntities();

        // GET: api/ProjectDocumentFinalResults
        public IQueryable<PROJECTDOCUMENTFINALRESULT> GetPROJECTDOCUMENTFINALRESULTS()
        {
            return db.PROJECTDOCUMENTFINALRESULTS;
        }

        // GET: api/ProjectDocumentFinalResults/5
        [ResponseType(typeof(PROJECTDOCUMENTFINALRESULT))]
        public IHttpActionResult GetPROJECTDOCUMENTFINALRESULT(int id)
        {
            PROJECTDOCUMENTFINALRESULT pROJECTDOCUMENTFINALRESULT = db.PROJECTDOCUMENTFINALRESULTS.Find(id);
            if (pROJECTDOCUMENTFINALRESULT == null)
            {
                return NotFound();
            }

            return Ok(pROJECTDOCUMENTFINALRESULT);
        }

        // PUT: api/ProjectDocumentFinalResults/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROJECTDOCUMENTFINALRESULT(int id, PROJECTDOCUMENTFINALRESULT pROJECTDOCUMENTFINALRESULT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROJECTDOCUMENTFINALRESULT.OID)
            {
                return BadRequest();
            }

            db.Entry(pROJECTDOCUMENTFINALRESULT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTFINALRESULTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProjectDocumentFinalResults
        [ResponseType(typeof(PROJECTDOCUMENTFINALRESULT))]
        public IHttpActionResult PostPROJECTDOCUMENTFINALRESULT(PROJECTDOCUMENTFINALRESULT pROJECTDOCUMENTFINALRESULT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROJECTDOCUMENTFINALRESULTS.Add(pROJECTDOCUMENTFINALRESULT);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pROJECTDOCUMENTFINALRESULT.OID }, pROJECTDOCUMENTFINALRESULT);
        }

        // DELETE: api/ProjectDocumentFinalResults/5
        [ResponseType(typeof(PROJECTDOCUMENTFINALRESULT))]
        public IHttpActionResult DeletePROJECTDOCUMENTFINALRESULT(int id)
        {
            PROJECTDOCUMENTFINALRESULT pROJECTDOCUMENTFINALRESULT = db.PROJECTDOCUMENTFINALRESULTS.Find(id);
            if (pROJECTDOCUMENTFINALRESULT == null)
            {
                return NotFound();
            }

            db.PROJECTDOCUMENTFINALRESULTS.Remove(pROJECTDOCUMENTFINALRESULT);
            db.SaveChanges();

            return Ok(pROJECTDOCUMENTFINALRESULT);
        }


        [HttpGet]
        [ActionName("getFinalResultFromProjectDocId")]
        public IEnumerable<projectDocumentFinalResultObject> getFinalResultFromProjectDocId(int param)
        {
            var getProjDocument = db.PROJECTDOCUMENTs.Find(param);
            List<projectDocumentFinalResultObject> result = new List<projectDocumentFinalResultObject>();

            var getFinalResultTypes = db.PROJECTDOCUMENTFINALRESULTS.Where(w => w.PROJECTDOCUMENTID == param);
            foreach (PROJECTDOCUMENTFINALRESULT s in getFinalResultTypes)
            {
                projectDocumentFinalResultObject getFinalResultType = new projectDocumentFinalResultObject();
                getFinalResultType.oid = s.OID;
                getFinalResultType.projectDocumentId = s.PROJECTDOCUMENTID;
                getFinalResultType.masterFinalResultId = s.MASTERFINALRESULTID;
                getFinalResultType.masterFinalResultDesc = s.MASTERFINALRESULTID.HasValue ? s.MASTERFINALRESULT.DESCRIPTION : null;
                getFinalResultType.finalResultValue = s.FINALRESULTVALUE;
                getFinalResultType.defaultValuePercentage = s.MASTERFINALRESULTID.HasValue ? s.MASTERFINALRESULT.VALUE : null;

                var listAction = s.MASTERFINALRESULT.ACTIONCHOICE.Split('|');
                getFinalResultType.lookupFinalResultValues = db.LOOKUPs.Where(w => w.TYPE == constants.finalResultAction && listAction.Contains(w.VALUE)).Select(l => new lookupObject
                {
                    type = l.TYPE,
                    text = l.TEXT,
                    value = l.VALUE,
                    description = l.DESCRIPTION
                });

                result.Add(getFinalResultType);
            };

            if (db.MASTERFINALRESULTs.Where(w => w.YEAR == getProjDocument.PLANFINISHDATE.Value.Year).Count() == 0)
            {
                int? maxYear = db.MASTERFINALRESULTs.Max(m => m.YEAR);
                if (maxYear.HasValue)
                {
                    var defaultValue = db.MASTERFINALRESULTs.Where(w => w.YEAR == maxYear).ToList().Select(s => new MASTERFINALRESULT
                    {
                        YEAR = getProjDocument.PLANFINISHDATE.Value.Year,
                        DESCRIPTION = s.DESCRIPTION,
                        VALUE = s.VALUE,
                        ACTIONCHOICE = s.ACTIONCHOICE,
                        CREATEDDATE = DateTime.Now,
                        CREATEDBY = GetCurrentNTUserId()
                    });
                    db.MASTERFINALRESULTs.AddRange(defaultValue);

                    db.SaveChanges();
                }
            }

            List<int?> listExistMasterFinalResult = getFinalResultTypes.Select(s => s.MASTERFINALRESULTID).ToList();
            var masterFinalResultTypes = db.MASTERFINALRESULTs.Where(w => w.YEAR == getProjDocument.PLANFINISHDATE.Value.Year && !listExistMasterFinalResult.Contains(w.OID));
            foreach (MASTERFINALRESULT s in masterFinalResultTypes) {
                projectDocumentFinalResultObject masterFinalResultType = new projectDocumentFinalResultObject();
                masterFinalResultType.oid = 0;
                masterFinalResultType.projectDocumentId = param;
                masterFinalResultType.masterFinalResultId = s.OID;
                masterFinalResultType.masterFinalResultDesc = s.DESCRIPTION;
                masterFinalResultType.finalResultValue = null;
                masterFinalResultType.defaultValuePercentage = s.VALUE;
                var listAction = s.ACTIONCHOICE.Split('|');
                masterFinalResultType.lookupFinalResultValues = db.LOOKUPs.Where(w => w.TYPE == constants.finalResultAction && listAction.Contains(w.VALUE)).Select(l => new lookupObject
                {
                    type = l.TYPE,
                    text = l.TEXT,
                    value = l.VALUE,
                    description = l.DESCRIPTION
                });
                result.Add(masterFinalResultType);
            }

            return result.OrderBy(o => o.masterFinalResultId);
        }

        [HttpPut]
        [ActionName("updateByMasterFinalResult")]
        public IHttpActionResult updateByMasterFinalResult(int param, projectDocumentFinalResultObject paramFinalResult)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PROJECTDOCUMENTFINALRESULT FinalResult = new PROJECTDOCUMENTFINALRESULT();
            if (param > 0)
            {
                if (paramFinalResult.oid > 0)
                {
                    FinalResult = db.PROJECTDOCUMENTFINALRESULTS.Find(paramFinalResult.oid);
                    FinalResult.FINALRESULTVALUE = paramFinalResult.finalResultValue;
                    FinalResult.UPDATEDBY = GetCurrentNTUserId();
                    FinalResult.UPDATEDDATE = DateTime.Now;
                    db.Entry(FinalResult).State = EntityState.Modified;
                }
                else
                {
                    FinalResult.PROJECTDOCUMENTID = paramFinalResult.projectDocumentId;
                    FinalResult.MASTERFINALRESULTID = paramFinalResult.masterFinalResultId;
                    FinalResult.FINALRESULTVALUE = paramFinalResult.finalResultValue;
                    FinalResult.CREATEDBY = GetCurrentNTUserId();
                    FinalResult.CREATEDDATE = DateTime.Now;
                    db.Entry(FinalResult).State = EntityState.Added;
                }
            }
            else
            {
                return BadRequest();
            }


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROJECTDOCUMENTFINALRESULTExists(FinalResult.OID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            paramFinalResult.masterFinalResult = null;
            paramFinalResult.projectDocument = null;
            paramFinalResult.oid = FinalResult.OID;
            return Ok(paramFinalResult);
        }

        [HttpGet]
        [ActionName("totalFinalResultByDocID")]
        public double totalFinalResultByDocID(int param)
        {
            var totalHours = GetFinalResultByValueByProjId(param);
            return totalHours.HasValue ? totalHours.Value : 0;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROJECTDOCUMENTFINALRESULTExists(int id)
        {
            return db.PROJECTDOCUMENTFINALRESULTS.Count(e => e.OID == id) > 0;
        }
    }
}