﻿using PTVI.MineAssay.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace PTVI.RESERVATION.WebAPI.Handler
{
    public class APIKeyHandler: DelegatingHandler
    {
        protected Entities db = new Entities();
        public APIKeyHandler(HttpConfiguration configuration)
        {
            InnerHandler = new HttpControllerDispatcher(configuration);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //bypass
            if (1 == 1)
            {
                return base.SendAsync(request, cancellationToken);
            }

            var qry = request.RequestUri.ParseQueryString();
            var fullUri = request.RequestUri.ToString().ToLower();

            if (!fullUri.Contains("users/findbyntuserid"))
            {
                var ntu = qry["ntuserid"];
                if (ntu == null)
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }

                USER user = db.USERs.Find(ntu);
                if (user == null)
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }
                
                var apk = qry["apikey"];
                if (apk == null)
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }

                if (user.APIKEY == null)
                {
                    user.APIKEY = Guid.NewGuid();
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                }

                if (user.APIKEY.ToString() != apk.ToString())
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }
            }

            
           
            return base.SendAsync(request, cancellationToken);
        }
    }
}