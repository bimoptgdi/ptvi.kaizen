﻿using PTVI.KAIZEN.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using System.Web.Cors;
using System.Web.Http.Cors;
using PTVI.KAIZEN.WebAPI.Models.CustomObject;

namespace PTVI.KAIZEN.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "CustomApi",
                routeTemplate: "api/{controller}/{action}/{param}",
                defaults: new { param = RouteParameter.Optional }
            );

            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();

            var employeeSet = builder.EntitySet<EMPLOYEE>("EMPLOYEEs");
            employeeSet.EntityType.HasKey(d => d.EMPLOYEEID);
            


            builder.EntitySet<PERSONALINFO>("BadgeInquiries");

            //=======
            //config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());

            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());

        }
        
    }
}
