﻿var SampleModel = kendo.data.Model.define({
    id: "OID",
    fields: {
        WAYBILLNO: { type: "string", validation: { required: true } },
        SAMPLENO: { type: "string" },
        SAMPLESEQ: { type: "int" },
        SAMPLEDATE: { type: "date" },
        FRACTION: { type: "string" },
        URGENCY: { type: "string" },
        VARIAN: { type: "string" },
        SEMIDRY: { type: "string" },
        TYPE: { type: "string" },
        SAMPLER: { type: "string" }
    }
});