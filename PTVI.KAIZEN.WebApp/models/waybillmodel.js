﻿var WaybillModel = kendo.data.Model.define({
    id: "OID",
    fields: {
        WAYBILLNO: { type: "string", validation: { required: true } },
        MININGAREA: { type: "string" },
        AREA: { type: "string" },
        COMPARTMENT: { type: "string" },
        LEVEL: { type: "int" },
        EQUIPMENTNO: { type: "string" },
        SHIFT: { type: "string" },
        SHIFTGROUP: { type: "string" },
        SAMPLER: { type: "string" },
        EQPROCESS: { type: "string" },
        EQPROCESSDTL: { type: "string" },
        LOCATION: { type: "string" },
        BATCHINGNO: { type: "string" },
        TESTNO: { type: "string" },
        WETWEIGHT: { type: "int" },
        RECEIVEDDATE: { type: "date" },
        WAYBILLTYPE: { type: "string" }
    }
});