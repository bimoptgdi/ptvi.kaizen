﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="K2HistoryControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.K2HistoryControl" %>
<div id="<%: this.ID %>_Content" class="k-content wide">
    <fieldset class="gdiFieldset">
        <legend class="gdiLegend">Request's History</legend>
        <div data-role="grid"
            data-bind="source: dsHistory"
            data-columns="[
                { 'field': 'CreatedDate', 'title': 'Date' },
                { 'field': 'ActivityName', 'title': 'Activity' },
                { 'field': 'Name', 'title': 'Name' },
                { 'field': 'ActionResult', 'title': 'Action' },
                { 'field': 'Comment', 'title': 'Comment' }
                ]">
            </div>
    </fieldset>
</div>
<script>

    var <%: this.ID %>_viewModel = kendo.observable({
        dsHistory: [
           
        ]
    });
    // Document Ready
    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_Content"), <%: this.ID %>_viewModel);
    });

    function <%: this.ID %>_Init(historyData) {
        kendo.bind($("#<%: this.ID %>_Content"), <%: this.ID %>_viewModel);
        <%: this.ID %>_viewModel.set("dsHistory", historyData);
    }
</script>