﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeMyProjectControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.HomeMyProjectControl" %>
<%@ Register Src="~/UserControl/RequestProjectControl.ascx" TagPrefix="uc1" TagName="RequestProjectControl" %>

<style>
        .tooltip {
            position: relative;
            display: inline-block;
            text-align: center;
            margin: 0 0 0 4px;
            cursor: pointer;
        }
        /* Tooltip text */
        .tooltip .tooltiptext {
            visibility: hidden;
            color: #fff;
            border-radius: 6px;
            width: 200px;
            text-align: left;
            padding-left: 10px;
            padding-right: 10px;
            background-color: #808080;
            padding-bottom: 10px;
             
            /* Position the tooltip text - see examples below! */
            position: absolute;
            top: -5px;
            right: 105%; 
            z-index: 999999;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
        }

        .tooltip:hover {
            color: #fff;
            padding-right: 1px;
            padding-top: 1px;
            box-sizing: padding-box;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            -webkit-transition: all 0.3s linear;
            -o-transition: all 0.3s linear;
            -moz-transition: all 0.3s linear;
            transition: all 0.3s linear;
            color: #fff;
            border: none;
        }
        .tooltip .tooltiptext::after {
            content: " ";
            position: absolute;
            top: 50%;
            left: 100%; /* To the right of the tooltip */
            margin-top: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent transparent #808080;
        }

        .container-page {
            margin-left: auto;
            margin-right: auto;
            border: none;
            overflow: hidden;
            height: 100%;
        }

        .sub-title-page {
            padding: 10px;
            color: #333;
            background-color: #f5f5f5;
            border: solid 1px #aeaeae;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            margin-bottom: -1px;
        }

        .content-page {
            padding: 10px;
            border: solid 1px #aeaeae;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        .container-page > .content-page {
            border-color: transparent !important;
        }

        .blank-div {
            height: 10px;
        }



        .hrStyle {
            border: 0;
            height: 1px;
            background-image: -webkit-linear-gradient(left, #808080, #ffffff, #808080);
            background-image: -moz-linear-gradient(left, #808080, #ffffff, #808080);
            background-image: -ms-linear-gradient(left, #808080, #ffffff, #808080);
            background-image: -o-linear-gradient(left, #808080, #ffffff, #808080);
        }

        .popup-title {
            text-align: center;
            padding-top: 5px;
            font-weight: 100;
        }

        .tooltip_wrapper {
            display: inline-block;
            width: 30px;
        }

        .popup-content {
            width: 200px;
            text-align: left;
            padding-left: 10px;
            padding-right: 10px;
            background-color: #808080;
            padding-bottom: 10px;
        }
</style>

<script>
    var <%: this.ID %>_popViewProject;
    var <%: this.ID %>_viewModel = kendo.observable({
        isVisibleButtonClear: false,
        selectedTabIndex: 0,
        dsListProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
            },
            change: function (e) {
                if (this.filter() == null) {
                    localStorage.removeItem("kendo-grid-options-Home-MyProject");
                    <%: this.ID %>_viewModel.set("isVisibleButtonClear", false);

                } else {
                    localStorage["kendo-grid-options-Home-MyProject"] = kendo.stringify(this.filter());
                    <%: this.ID %>_viewModel.set("isVisibleButtonClear", true);
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        totalCost: { type: "number" },
                        actualCost: { type: "number" },
                        commitment: { type: "number" },
                        shareFolderDoc: { type: "string" },
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" }
                    }
                }
            },
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "Project/listMainMyProject/" + _currBadgeNo,
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsListMasterProjectCategory: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
            },
            transport: {
                read: {
                    async: false,
                    url: "<%: WebApiUrl %>" + "MasterProjectCategories",
                    dataType: "json"
                }
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            pageSize: 10,
            sort: [{ field: "description", dir: "asc" }]
        }),
        openProjectDashboard: function (data) {
            var form = $('<form action="DashboardProjectForm.aspx" method="post">' +
                        '<input type="hidden" name="_projectId" value="' + data.id + '"></input>' +
                        '<input type="hidden" name="_projectNo" value="' + data.projectNo + '"></input>' +
                        '<input type="hidden" name="_projectDesc" value="' + data.projectDescription + '"></input>' +
                        '<input type="hidden" name="_projectType" value="' + data.projectType + '"></input>' +
                        '<input type="hidden" name="_roleInProject" value="' + data.roleInProject + '"></input>' +
                        '<input type="hidden" name="_positionProjectName" value="' + data.positionName + '"></input>' +
                        '<input type="hidden" name="_iptProjectId" value="' + data.iptProjectId + '"></input>' +
                        '<input type="hidden" name="_planStartDate" value="' + (data.planStartDate ? kendo.toString(data.planStartDate, 'yyyyMMdd') : null) + '"></input>' +
                        '<input type="hidden" name="_planFinishDate" value="' + (data.planFinishDate ? kendo.toString(data.planFinishDate, 'yyyyMMdd') : null) + '"></input>' +
                        '</form>');
            $('body').append(form);
            $(form).submit();
        },
        trafficFilter: function (element) {
            element.kendoDropDownList({
                dataTextField: "color",
                dataValueField: "color",
                optionLabel: "Select Traffic Light",
                dataSource: _trafficLight,
                valueTemplate: '<span class="selected-value" style="width: 32px;display: inline-block;background-repeat:no-repeat;background-image: url(\'' + _webAppUrl + 'images/#: common_getTrafficImageUrl(data.color) #\')">' +
                    '&nbsp;</span><span>#:data.color#</span>',
                template: '<span class="k-state-default" style="width: 32px;display: inline-block; background-repeat:no-repeat ;background-image: url(\'' + _webAppUrl + 'images/#: common_getTrafficImageUrl(data.color) #\')">' +
                    '&nbsp;</span><span>#:data.color#</span>',
            });
        },
        openProjectDetail: function (data) {
            //this.openProjectDashboard(data)
            //_redirectPost("HomeProjectForm.aspx", "_projectID", e.data.id)
            //var form = $('<form action="HomeProjectForm.aspx" method="post">' +
            var form = $('<form action="MainProjectForm.aspx" method="post">' +
                        '<input type="hidden" name="_projectId" value="' + data.id + '"></input>' +
                        '<input type="hidden" name="_projectNo" value="' + data.projectNo + '"></input>' +
                        '<input type="hidden" name="_projectDesc" value="' + data.projectDescription + '"></input>' +
                        '<input type="hidden" name="_projectType" value="' + data.projectType + '"></input>' +
                        '<input type="hidden" name="_roleInProject" value="' + data.roleInProject + '"></input>' +
                        '<input type="hidden" name="_iptProjectId" value="' + data.iptProjectId + '"></input>' +
                        '<input type="hidden" name="_planStartDate" value="' + (data.planStartDate ? kendo.toString(data.planStartDate, 'yyyyMMdd') : null) + '"></input>' +
                        '<input type="hidden" name="_planFinishDate" value="' + (data.planFinishDate ? kendo.toString(data.planFinishDate, 'yyyyMMdd') : null) + '"></input>' +
                        '<input type="hidden" name="_positionProjectName" value="' + data.positionName + '"></input>' +
                        '</form>');
            $('body').append(form);
            $(form).submit();
        },
        editViewProject: function (data) {
            $.ajax({
                url: _webApiUrl + "Project/" + data.id,
                type: "GET",
                success: function (data) {
                    RequestProject_viewModel.set("requestData", data);
                    RequestProject_viewModel.dsEWR.read().then(function () {
                        RequestProject_viewModel.set("requestData", data);
                    });
                    if (!data.iptProjectId) {
                        RequestProject_viewModel.set("isAvailToIntegrated", true);
                        RequestProject_viewModel.set("integratedStatus", "");
                        RequestProject_viewModel.set("btnIntegrate", "Integrate");
                    } else {
                        RequestProject_viewModel.set("isAvailToIntegrated", true);
                        RequestProject_viewModel.set("integratedStatus", "(Integrated)");
                        RequestProject_viewModel.set("btnIntegrate", "Re-Integrate");
                    }
                    RequestProject_viewModel.set("projectCategoryList", (data.projectCategory ? data.projectCategory.split("|") : []));
                    RequestProject_viewModel.set("employeeOwnerSelected", { employeeId: data.ownerId, full_Name: data.ownerName, email: data.ownerEmail });
                    RequestProject_viewModel.set("employeeSponsorSelected", { employeeId: data.sponsorId, full_Name: data.sponsorName, email: data.sponsorEmail });
                    RequestProject_viewModel.set("employeePMSelected", { employeeId: data.projectManagerId, full_Name: data.projectManagerName, email: data.projectManagerEmail });
                    RequestProject_viewModel.set("employeePESelected", { employeeId: data.projectEngineerId, full_Name: data.projectEngineerName, email: data.projectEngineerEmail });
                    RequestProject_viewModel.set("employeeMaintenanceSelected", { employeeId: data.maintenanceRepId, full_Name: data.maintenanceRepName, email: data.maintenanceRepEmail });
                    RequestProject_viewModel.set("employeeOperationSelected", { employeeId: data.operationRepId, full_Name: data.operationRepName, email: data.operationRepEmail });

                    RequestProject_viewModel.dsOtherPositionGetData.transport.options.read.url = _webApiUrl + "UserAssignment/otherPostionOnProject/" + data.id;
                    RequestProject_viewModel.dsLookupOtherPositionProject.read();
                    RequestProject_viewModel.dsOtherPositionGetData.read();
                    RequestProject_checkAccessRight();
                },
                error: function (jqXHR) {
                }
            });
            $('#RequestProject_inputProjectLegend').text("Detail Project");
            $('#RequestProject_projectNo').attr('readonly', true);
            $('#RequestProject_create').hide();
            $('#RequestProject_update').show();
            
            
            <%: this.ID %>_popViewProject.center().open();
            RequestProject_viewModel.set("onCloseUpdateClick", function (e) {
                <%: this.ID %>_popViewProject.close();
            });

            RequestProject_viewModel.set("onRefreshGridMaster", function (e) {
                <%: this.ID %>_viewModel.dsListProject.read();
            });

            if (_currRole == "ADM")
                $("#RequestProject_projectNo").removeAttr('readonly');

        },
        onFilter: function (e) {
            localStorage["kendo-grid-options-Home-MyProject-Page"] = 1;
        },
        onPage: function () {
            localStorage["kendo-grid-options-Home-MyProject-Page"] = <%: this.ID %>_viewModel.dsListProject.page();
        },
        onRefreshClick: function (e) {
            localStorage["kendo-grid-options-Home-MyProject-Page"] = 1;
            localStorage.removeItem("kendo-grid-options-Home-MyProject");
            <%: this.ID %>_viewModel.dsListProject.filter([]);
        }
    });

    $(document).ready(function () {
        <%: this.ID %>_viewModel.dsListMasterProjectCategory.read();
        kendo.bind($("#<%: this.ID %>_HomeMyProjectControl"), <%: this.ID %>_viewModel);

        if (localStorage["kendo-grid-options-Home-MyProject"]) {
            <%: this.ID %>_viewModel.dsListProject.filter(JSON.parse(localStorage["kendo-grid-options-Home-MyProject"]));
            <%: this.ID %>_viewModel.set("isVisibleButtonClear", true);
        } else {
            <%: this.ID %>_viewModel.set("isVisibleButtonClear", false);
        }

        $("#<%: this.ID %>_grdListProject").data("kendoGrid").one("dataBound", function () {
            if (localStorage["kendo-grid-options-Home-MyProject-Page"]) {
                <%: this.ID %>_viewModel.dsListProject.page(localStorage["kendo-grid-options-Home-MyProject-Page"]);
            }
        });

        $("#<%: this.ID %>_menu").kendoContextMenu({
            orientation: "vertical",
            target: "#<%: this.ID %>_grdListProject",
            filter: ".projectNo-template",
            animation: {
                open: { effects: "fadeIn" },
                duration: 500
            },
            showOn: "click",
            select: function (e) {
                var row = $(e.target).parent().parent();
                var grid = this.target.data("kendoGrid");
                var data = grid.dataItem(row);

                if (e.item.id == "<%: this.ID %>_menuDashboard") {
                    //var accessRight;
                    //$.ajax({
                    //    url: _webApiUrl + "project/getPositionInProject/" + data.id + "?badgeNo=" + _currBadgeNo,
                    //    type: "GET",
                    //    success: function (data) {
                    //        accessRight = data;
                    //    },
                    //    error: function (jqXHR) {
                    //    }
                    //});

                    $.ajax({
                        url: _webApiUrl + "project/IsAllowedProjectTo/" + data.id + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ProjectDashboard%> ",
                        type: "GET",
                        success: function (result) {
                            if (result) {
                                <%: this.ID %>_viewModel.openProjectDashboard(data)
                            } else {
                                _showDialogMessage("Access Denied", "You do not have access privileges", "");
                            }
                        },
                        error: function (jqXHR) {
                        }
                    });

                    <%--                    if (data.roleInProject == _roleOwner || data.roleInProject == _roleSponsor || data.roleInProject == _roleProjectManager || data.roleInProject == _roleProjectEngineer) {
                        <%: this.ID %>_viewModel.openProjectDashboard(data)
                    } else { 
                        _showDialogMessage("Access Denied", "You do not have access privileges to project", "");
                    } --%>
                }
                else if (e.item.id == "<%: this.ID %>_menuSummary") {
                    $.ajax({
                        url: _webApiUrl + "project/IsAllowedProjectTo/" + data.id + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ProjectDashboard%> ",
                        type: "GET",
                        success: function (result) {
                            if (result) {
                                <%: this.ID %>_viewModel.openProjectDashboard(data)
                            } else {
                                <%: this.ID %>_viewModel.openProjectDetail(data)
            //                    _showDialogMessage("Access Denied", "You do not have access privileges", "");
                            }
                        },
                        error: function (jqXHR) {
                        }
                    });
                }
                else if (e.item.id == "<%: this.ID %>_menuViewProject") {
                    $.ajax({
                        url: _webApiUrl + "project/IsAllowedProjectTo/" + data.id + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                        type: "GET",
                        success: function (result) {
                            if (result) {
                                <%: this.ID %>_viewModel.editViewProject(data)
                            } else {
                                _showDialogMessage("Access Denied", "You do not have access privileges", "");
                            }
                        },
                        error: function (jqXHR) {
                        }
                    });
                }

            }
        });

        <%: this.ID %>_popViewProject = $("#<%: this.ID %>_popViewProject").kendoWindow({
            title: "Project Detail",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

    });



</script>

<div id="<%: this.ID %>_HomeMyProjectControl">
    <div id="<%: this.ID %>_popViewProject">
        <uc1:RequestProjectControl runat="server" ID="RequestProject" />
    </div>

    <ul id="<%: this.ID %>_menu" style="width: 200px;">
<%--        <li id="<%: this.ID %>_menuDashboard" style="font-size: 15px;">
            <img src="Images/dashboard-icon.png" style="width: 20px; height: 20px;" /> Dashboard
        </li>--%>
        <li id="<%: this.ID %>_menuSummary" style="font-size: 15px;">
            <img src="Images/summary-icon.png" style="width: 20px; height: 20px;" /> Summary
        </li>
        <li class="k-separator"></li>
        <li id="<%: this.ID %>_menuViewProject" style="font-size: 15px;">
            <img src="Images/view-detail-icon.png" style="width: 20px; height: 20px;" /> View Project
        </li>
    </ul>
    <table style="width:100%;">
        <tr>
            <td>
                <span style="color: #EB9617;vertical-align: middle;">*Left click on a Project No to show options</span> 
                <div style="float: right;">
                    <div
                        data-role="button"
                        data-bind="visible: isVisibleButtonClear, events: { click: onRefreshClick }"
                        class="k-button">
                        <span class="k-icon k-i-refresh"></span>&nbsp;Clear Filter
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <div id="<%: this.ID %>_grdListProject"
        data-role="grid"
        data-scrollable="true"
        data-sortable="true"
        data-editable="false"
        data-columns="[
                {
                    field: 'projectNo', title: 'Project No',
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_projectNo-template').html()), 
                    width: 100
                },                                
                {
                    field: 'projectDescription', title: 'Project Description',
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_projectDesc-template').html()), 
                    width: 180
                },                                
                { 
                    field: 'ownerSponsorToString', title: 'Owner/Sponsor' , 
                    attributes: { style: 'vertical-align: top !important;' },
                    template: '# if(ownerSponsorName.owner!=null) { #<b>Owner</b>:<br /> #: ownerSponsorName.owner #<br /># } if(ownerSponsorName.sponsor!=null) {#<b>Sponsor</b>:<br />#: ownerSponsorName.sponsor # #} #', 
                    width: 130
                },                               
                { 
                    field: 'PMPEToString', title: 'PM & PE' , 
<%--                    field: 'projectManagerName', title: 'PM & PE' , --%>
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_pmpe-template').html()), 
                    width: 130
                },                               
                { 
                    field: 'userAssignmentsToString', title: 'Engineer/Designer', 
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_engineer-template').html()),
                    width: 130
                },                               
                { 
                    field: 'totalCost', title: 'Budget', 
                    attributes: { style: 'text-align: right; vertical-align: top !important;overflow: visible;' },
                    format: '{0:n2}', width: 100,
                    template: kendo.template($('#<%: this.ID %>_budget-template').html())
                },      
<%--                {
                    field: 'actualCost', title: 'ETC',
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_etc-template').html())
                },                         --%>
                <%--{
                    field: 'actualCost', title: 'Actual Cost', 
                    attributes: { style: 'text-align: right; vertical-align: top !important;' },
                    format: '{0:n2}', width: 100
                },                               
                { 
                    field: 'commitment', title: 'Commitment', 
                    attributes: { style: 'text-align: right; vertical-align: top !important;' },
                    format: '{0:n2}', width: 100
                },
                { 
                    field: 'forecast', title: 'Forecast', 
                    attributes: { style: 'text-align: right; vertical-align: top !important;' },
                    format: '{0:n2}', width: 100
                },--%>
                { 
                    field: 'pmTrafic', title: 'PM',  headerTemplate: '<div title=\'Project Management\'>PM</div>',
                    filterable: {
                          ui: <%: this.ID %>_viewModel.trafficFilter
                        },
                    
                    attributes: { style: 'text-align: center; vertical-align: top !important; overflow: visible;' },
                    template: kendo.template($('#<%: this.ID %>_pmTraffic-template').html()),
                    width: 55
                },                               
                { 
                    field: 'esTrafic', title: 'ES', headerTemplate: '<div title=\'Engineering Service\'>ES</div>', 
                    filterable: {
                          ui: <%: this.ID %>_viewModel.trafficFilter
                        },
                    attributes: { style: 'text-align: center; vertical-align: top !important; overflow: visible;' },
                    template: kendo.template($('#<%: this.ID %>_esTraffic-template').html()),
                    width: 55
                },                               
                { 
                    field: 'materialTrafic', title: 'MC', headerTemplate: '<div title=\'Material\'>MC</div>',
                    filterable: {
                          ui: <%: this.ID %>_viewModel.trafficFilter
                        },
                    attributes: { style: 'text-align: center; vertical-align: top !important; overflow: visible;' },
                    template: kendo.template($('#<%: this.ID %>_mcTraffic-template').html()),
                    width: 55
                },                               
                { 
                    field: 'consTrafic', title: 'CS', headerTemplate: '<div title=\'Construction Service\'>CS</div>',
                    filterable: {
                          ui: <%: this.ID %>_viewModel.trafficFilter
                        },
                    attributes: { style: 'text-align: center; vertical-align: top !important; overflow: visible;' },
                    template: kendo.template($('#<%: this.ID %>_csTraffic-template').html()),
                    width: 55
                },                               
                { 
                    field: 'handoverCloseOutTrafic', title: 'CO', headerTemplate: '<div title=\'Close Out\'>CO</div>',
                    filterable: {
                          ui: <%: this.ID %>_viewModel.trafficFilter
                        },
                    attributes: { style: 'text-align: center; vertical-align: top !important; overflow: visible;' },
                    template: kendo.template($('#<%: this.ID %>_coTraffic-template').html()),
                    width:55, 
                },
            ]"
        data-bind="source: dsListProject, events: { filter: onFilter }"
        data-filterable="true"
        data-pageable="{ buttonCount: 5, change: <%: this.ID %>_viewModel.onPage }">
    </div>
</div>

<script type="text/x-kendo-template" id="<%: this.ID %>_pmTraffic-template">
    
#if (pmTrafic) { #
    #if (pmTrafic == "Red" || pmTrafic == "Yellow") { #
        <div class='tooltip'>
            <img class='logo' src='images/#: common_getTrafficImageUrl(pmTrafic) #' style='padding-top: 0px;'>
            <div class='tooltiptext'>
                <div class='popup-title'>PIC</div>
                <hr class='hrStyle' /><div> # projectManagerName = projectManagerName.replace(/,/g, '<br/>'); # #= projectManagerName #
            </div>
        </div>
    #} else {#
        <img class='logo' src='images/#: common_getTrafficImageUrl(pmTrafic) #' style='padding-top: 0px;'>
    #}#
#}#
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_esTraffic-template">
#if (esTrafic) { #
    #if (esTrafic == "Red" || esTrafic == "Yellow") { #
        <div class='tooltip'>
            <img class='logo' src='images/#: common_getTrafficImageUrl(esTrafic) #' style='padding-top: 0px;'>
            <div class='tooltiptext'>
                <div class='popup-title'>PIC</div>
                <hr class='hrStyle' />
                     # if(esTraficPicTextEwp) { # Engineer: # esTraficPicTextEwp = '<div style="padding-left: 10px">'+esTraficPicTextEwp.replace(/;/g, '<br/>')+'</div>';}# #= esTraficPicTextEwp #
                     # if(esTraficPicTextDwg) {# Designer: # esTraficPicTextDwg = '<div style="padding-left: 10px">'+esTraficPicTextDwg.replace(/;/g, '<br/>')+'</div>';}# #= esTraficPicTextDwg #
            </div>
        </div>
    #} else {#
        <img class='logo' src='images/#: common_getTrafficImageUrl(esTrafic) #' style='padding-top: 0px;'>
    #}#
#}#
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_mcTraffic-template">
#if (materialTrafic) { #
    #if (materialTrafic == "Red" || materialTrafic == "Yellow") { #
        <div class='tooltip'>
            <img class='logo' src='images/#: common_getTrafficImageUrl(materialTrafic) #' style='padding-top: 0px;'>
            <div class='tooltiptext'>
                <div class='popup-title'>PIC</div>
                <hr class='hrStyle' />
                     # materialTraficPic = '<div style="padding-left: 10px">'+materialTraficPic.replace(/;/g, '<br/>')+'</div>'; # #= materialTraficPic #
            </div>
        </div>
    #} else {#
        <img class='logo' src='images/#: common_getTrafficImageUrl(materialTrafic) #' style='padding-top: 0px;'>
    #}#
#}#
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_csTraffic-template">
#if (consTrafic) { #
    #if (consTrafic == "Red" || consTrafic == "Yellow") { #
        <div class='tooltip'>
            <img class='logo' src='images/#: common_getTrafficImageUrl(consTrafic) #' style='padding-top: 0px;'>
            <div class='tooltiptext'>
                <div class='popup-title'>PIC</div>
                <hr class='hrStyle' />
                     # consTraficPic = '<div style="padding-left: 10px">'+consTraficPic.replace(/;/g, '<br/>')+'</div>'; # #= consTraficPic #
            </div>
        </div>
    #} else {#
        <img class='logo' src='images/#: common_getTrafficImageUrl(consTrafic) #' style='padding-top: 0px;'>
    #}#
#}#
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_coTraffic-template">
#if (handoverCloseOutTrafic) { #
    #if (handoverCloseOutTrafic == "Red" || handoverCloseOutTrafic == "Yellow") { #
        <div class='tooltip'>
            <img class='logo' src='images/#: common_getTrafficImageUrl(handoverCloseOutTrafic) #' style='padding-top: 0px;'>
            <div class='tooltiptext'>
                <div class='popup-title'>PIC</div>
                <hr class='hrStyle' />
                     #= handoverCloseOutTraficRole #:<br/>
                     # handoverCloseOutTraficPic = '<div style="padding-left: 10px">'+handoverCloseOutTraficPic.replace(/;/g, '<br/>')+'</div>'; # #= handoverCloseOutTraficPic #
            </div>
        </div>
    #} else {#
        <img class='logo' src='images/#: common_getTrafficImageUrl(handoverCloseOutTrafic) #' style='padding-top: 0px;'>
    #}#
#}#
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_projectNo-template">
        <div class="projectNo-template">
            # var url = ""; #
            #: projectNo #
           <%-- <a href="\#" data-bind="events: { click: openProjectDashboard }">#: projectNo #</a>
            <a href="\#" data-bind="events: { click: editViewProject }"><img style="width: 12px; height: 12px;" src="Images/editing-edit.png" /></a>--%>
        </div>
        #if (data.projectCategory) {#
            <div class='subColumnTitle'>
                Project Category
            </div>
            <div>
                #var splitPCategory = data.projectCategory.split('|')#
                #for (var l = 0; l < splitPCategory.length; l++) {#
                    #:(<%: this.ID %>_viewModel.dsListMasterProjectCategory.get(splitPCategory[l]) ? <%: this.ID %>_viewModel.dsListMasterProjectCategory.get(splitPCategory[l]).description + "; " : "-")#
                #}#
            </div>
        #}#
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_projectDesc-template">
    #if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {#
        <div> 
            #: projectDescription # <br><br>
            #if (shareFolderDoc) {#
                <a href="file://#= shareFolderDoc#"  target="_explorer.exe">Share Folder</a>
            #}#
        </div>
    #} else { #
        <div> 
            #: projectDescription # <br><br>
            #if (shareFolderDoc) {#
                <div class='subColumnTitle'>
                        Share Folder
                </div>
                <div>
                    #= shareFolderDoc#
                </div>
            #}#
        </div>
    #} #

<%--            <a href="\#" data-bind="events: { click: openProjectDetail }">#: projectDescription #</a>--%>
        </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_pmpe-template">
        <div>
            # if (projectManagerId == projectEngineerId) {#
                PM & PE: #: projectManagerName ? projectManagerName : '-' #
            # } else { #
                PM: #: projectManagerName ? projectManagerName : '-' #</br>
                PE: #: projectEngineerName ? projectEngineerName : '-' #
            # } #
        </div>
</script>
<script id="<%: this.ID %>_engineer-template" type="text/x-kendo-template">
         <div>
        # var PE = userAssignments.filter(function(val) {
            return val.assignmentType === "PE";
            }); #
        #if(PE.length>0){#
            <b>Engineer:</b><br\>
            # for (var i = 0; i < userAssignments.length; i++) { #
                # if(userAssignments[i].assignmentType=='PE'){ #
                #: userAssignments[i].employeeName #;
            #} } }# <br\>
        # var PD = userAssignments.filter(function(val) {
            return val.assignmentType === "PD";
            }); #
        #if(PD.length>0){#
            <b>Designer:</b><br\>
            # for (var i = 0; i < userAssignments.length; i++) { #
                # if(userAssignments[i].assignmentType=='PD'){#
                #: userAssignments[i].employeeName #;
            # } } } #
        </div>
       
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_budget-template">

# var etcCost = actualCost + commitment + forecast; #    
# var budgetDisplay = kendo.toString(totalCost ? totalCost : 0, "n2"); #
<div class='tooltip'>
    # if (etcCost > totalCost) { #
        <div style="color:red;">
            #: budgetDisplay #
        </div>
        <div class='tooltiptext'>
            <div class='popup-title'>Over Budget</div>
            <hr class='hrStyle' />ETC > Budget
        </div>
    # } else { #
        <div style="color:green;">
            #: budgetDisplay #
        </div>
        <div class='tooltiptext'>
            <div class='popup-title'>In Budget</div>
            <hr class='hrStyle' />Budget > ETC
        </div>
    # } #
    
</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_etc-template">
# var actualDisplay = kendo.toString(actualCost ? actualCost : 0, "n2"); #
# var commitmentDisplay = kendo.toString(commitment ? commitment : 0, "n2"); #
# var forecastDisplay = kendo.toString(forecast ? forecast : 0, "n2"); #

<div>
    <b>Actual Cost</b>: #: actualDisplay #
        <br/>
    <b>Commitment</b>: #: commitmentDisplay #
        <br/>
    <b>Service Commitment</b>: #: forecastDisplay #
</div>
</script>