﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportUpDownProgressControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportUpDownProgressControl" %>
<div id="<%: this.ID %>_reportUpDownProgressContainer">
    <div data-bind="visible:loaded" style="display: none;">
        <div style="font-weight: bold">
            <div style="text-align: center">Up Down Report</div>
            <div style="text-align: center" data-bind="html:taskName"></div>
            <div style="text-align: center">Position <span data-bind="html: position"></span></div>
        </div>
        <br />
        <br />
        <br />
        <div style="height:600px" 
            data-role="chart"
            data-value-axis="[{
                    max: 100,
                    majorTicks: {size: 10, width: 2}, 
                    minorTicks: {size: 5, width: 2, visible: true}
            }]"
            data-transitions="false"
            data-bind="source: dsUpDownProgress"
            data-series="[  {type: 'rangeColumn', fromField: 'currentPctComplete', colorField: 'color', toField: 'previousPctComplete', labels: {visible: true, template:'Test', from: {template: '#=value.from# %'}, to: {template: '#=value.to# %'}}},
                        {type: 'line', field: 'currentPctComplete', name: 'Current % Complete', color: 'yellow'},
                        {type: 'line', field: 'previousPctComplete', name: 'Previous % Complete', color: 'blue'}]"
            data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}"
            data-chart-area="{background: 'lightgray'}">
        </div>
        <br />
        <br />
        <div data-role="grid" data-bind="source: dsUpDownProgress" data-scrollable="false"
            data-columns="[{ template: '#= ++<%: this.ID %>_vm.record #', title:'No', width: '5%', attributes: {style: 'text-align: right'} }, 
                        { field: 'taskName', title: 'Project / Task Name', width: '50%' },
                        { title: '% Complete', headerAttributes: {style: 'text-align: center'} , columns: [
                            { field: 'currentPctComplete', title: 'Current', attributes: {style: 'text-align: right'} },
                            { field: 'previousPctComplete', title: 'Previous', attributes: {style: 'text-align: right'} },
                            { field: 'upDown', title: 'Up / Down', attributes: {style: 'text-align: right'} }]
                        },
                        { field: 'durationHour', title: 'Duration (Hours)', attributes: {style: 'text-align: right'} },
                        { field: 'totalSlackHour', title: 'Total Slack (Hours)', template: '#: totalSlackHour == null ? \'NA\' : kendo.toString(totalSlackHour, \'n2\') #', attributes: {style: 'text-align: right'} },
                        { field: 'freeSlackHour', hidden: true, title: 'Free Slack (Hours)', template: '#: freeSlackHour == null ? \'NA\' : kendo.toString(freeSlackHour, \'n2\') #', attributes: {style: 'text-align: right'} },
                        { field: 'optimisDiff', hidden: true, title: 'Potential Ahead / Delay Optimist (Hours)', format: '{0:n2}', attributes: {style: 'text-align: right'} },
                        { field: 'contractor', title: 'Contractor' }]">
        </div>
    </div>
</div>
<div  id="<%: this.ID %>_errorMessageContainer">
    <div data-bind="visible: notFound" style="top: 60px;width: 400px;text-align: center;margin: auto;position: relative;" >
        <div style="border:1px solid red;color:red;font-size:x-large;" data-bind="html: ErrMsg"></div>
    </div>
</div>

<script>
    var <%: this.ID %>_vm = kendo.observable({
        taskName: null,
        position: null,
        dsUpDownProgress: null,
        loaded: false,
        record: 0
    });

    var <%: this.ID %>_ervm = kendo.observable({
        notFound: false,
        ErrMsg: "No Data Found"
    });

    function <%: this.ID %>_populate(projectShiftID, taskUid, callback) {
        $.ajax({
            url: webApiUrl + "iptreport/reportdata?reportName=IPT_PROGRESS_UPDOWN&projectshiftid=" + projectShiftID + "&taskUid=" + taskUid,
            success: function (result) {
                if (result != null) {
                    <%: this.ID %>_vm.set("taskName", result.taskName);
                    <%: this.ID %>_vm.set("position", kendo.toString(kendo.parseDate(result.position), 'dd MMM yyyy  hh:mm tt'));
                    
                    for (var i = 0; i < result.list.length; i++) {
                        result.list[i]["color"] = result.list[i].upDown < 0 ? 'red' : 'green';
                        result.list[i]["durationHour"] = result.list[i].duration / 60 / 10;
                        result.list[i]["totalSlackHour"] = result.list[i].totalSlack == null ? null : result.list[i].totalSlack / 60 / 10;
                        result.list[i]["freeSlackHour"] = result.list[i].freeSlack == null ? null : result.list[i].freeSlack / 60 / 10;
                    }

                    <%: this.ID %>_vm.dsUpDownProgress = new kendo.data.DataSource({
                        data: result.list
                    });
                    
                    <%: this.ID %>_vm.set("loaded", true);
                    <%: this.ID %>_ervm.set("notFound", false);
                    status = "loaded";
                    kendo.bind($("#<%: this.ID %>_reportUpDownProgressContainer"), <%: this.ID %>_vm);
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
                }

                else {
                    <%: this.ID %>_ervm.set("notFound", true);
                    status = "notfound";
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);                   
                }

                callback(status);
            },
            error: function (e) {

                var jsonResponse = e.responseJSON;
                <%: this.ID %>_ervm.set("ErrMsg", jsonResponse.ExceptionMessage);
                <%: this.ID %>_ervm.set("notFound", true);
                status = "Error";
                kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
            }
        });
    }

    function <%: this.ID %>_shortLabels(value) {
        return value;

        var result = "";
        while (value.length > 20) {
            var clipIndex = 20;
            while (value[clipIndex] != ' ')
                clipIndex--;

            result = result + (result != "" ? "\n" : "") + value.substring(0, clipIndex);

            value = value.substring(clipIndex);
        }
    
        result = result + (result != "" ? "\n" : "") + value;

        return result;
    }

</script>

