﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectServiceForecastSpendingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ProjectServiceForecastSpendingControl" %>

<script type="text/x-kendo-template" id="addForecastTemplate">
    <div class="forecastContainer" id="forecastContainer">
        <div>
           <div class="labelForecast" >Description</div><div class="labelForecast"><input name="Description" required="true" class="inputForecast k-textbox" type="text" id="description" data-bind="value: DESCRIPTION"/></div>
        </div>
        
        <div class="" >
            <div class="labelForecast">Due Date</div><div class="labelForecast"><input name="Due Date" required='true' id="dtDueDate" data-role="datepicker"
             data-bind="value: DUEDATE" data-format="dd MMM yyyy"/></div>
        </div>
        <div class="" >
            <div class="labelForecast">Service Commitment Value</div><div class="labelForecast"><input name="Forecast Value" required='true' id="forecastValue" data-role="numerictextbox" data-min="0" data-format="c" style="text-align: right; padding-right: 10px"
            data-bind="value: FORECASTVALUE" /></div>
        </div>
        

    <div>
</script>
<style>
    .labelForecast {
        width: 100px;
        display: inline-block;
        padding-left: 10px;
    }

    .inputForecast {
        width: 200px;
    }

    .forecastContainer {
        width: 350px;
    }

    .textArea {
        width: 300px;
        height: 50px;
    }
</style>

<div id="<%: this.ID %>_FormContent">
      
    <div>
        <div id="<%: this.ID %>_grdForecast" data-role="grid" 
            data-bind="source: dsForecast, events: {
                edit: onGridEdit,
                dataBound: onDataBound
                
            }" 
            data-columns="[
                <%--{ 
                    field: 'PROJECTID', title: 'Project ID', width: 200
                },--%>
                {
                    field:'DESCRIPTION', title: 'Description', width: 200
                },
                { 
                    field: 'DUEDATE', title: 'Due Date', format: '{0: dd MMM yyyy}',
                    attributes: { style: 'vertical-align: top !important;' },
                    width: 100, filterable: true
                },
                {
                    field:'FORECASTVALUE', title: 'Service Commitment Value', format: '{0:c}',
                    width: 200, attributes: { style: 'text-align: right !important;' },
                },
            
                {
                    command: [
            <%
                var isReadOnly = IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ForecastSpending);

                if (!isReadOnly)
                {
            %>
                        { name:'edit', text:{ edit: 'Edit', update: 'Save' } },
                        { name:'destroy' }
            <% 
                }
            %>
                    ] 
                }
            
            ]" 
            data-editable= "{ mode: 'popup', confirmation:true, window:{ width: 400, title: 'Service Commitment Spending', modal: false}, template:kendo.template($('#addForecastTemplate').html()) }", 
            <%
            //var isReadOnly = IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ForecastSpending);
            
            if (!isReadOnly)
            { %>
                data-toolbar= "[{name:'create', text:'Add New Service Commitment Spending'}]", 
            <% } %>
            data-filterable="true"
            data-pageable="true", 
            data-sortable="true"
            >

        </div>
 
    </div>
</div>


<script>

    var <%: this.ID %>_dsForecast = new kendo.data.DataSource({
        requestStart: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_grdForecast"), true);
        },
        requestEnd: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_grdForecast"), false);
        },
        transport: {
            create: {
                type: "POST", dataType: "json",
                url: _webApiUrl + "FORECASTSPENDINGs"
            },
            read: {
                dataType: "json",
                url: _webApiUrl + "FORECASTSPENDINGs/FindForecastSpendingByProject/" + _projectId
            },
            update: {
                type: "PUT", dataType: "json",
                url: function (e) {
                    return _webApiUrl + "FORECASTSPENDINGs/" + e.OID;
                }

            },
            destroy: {
                type: "DELETE", dataType: "json",
                url: function (e) {
                    return _webApiUrl + "FORECASTSPENDINGs/" + e.OID;
                }

            },
            parameterMap: function (options, operation) {
                if (operation == "create") {
                   
                    options.PROJECTID = _projectId;
                    options.DUEDATE = kendo.toString(options.DUEDATE, 'MM/dd/yyyy');
                    return options;
                } else if (operation == "update") {
                    options.PROJECTID = _projectId;
                    options.DUEDATE = kendo.toString(options.DUEDATE, 'MM/dd/yyyy');
                    return options;
                }
            }
        },
        //error: function (e) {
            
        //    _showDialogMessage("Error", e.xhr.responseText, "");
            
        //},
        schema: {
            model: {
                id: "OID",
                fields: {
                    OID: { type: "number" },
                    PROJECTID: { type: "number" },
                    DESCRIPTION: { type: "string" },
                    DUEDATE: { type: "date", validation: { required: true }, defaultValue: null },
                    FORECASTVALUE: { type: "number" }
                    
                }
            }
        },
        pageSize: 10,
        change: function (e) {
        }
    });

   
    var <%: this.ID %>_viewModel = new kendo.observable({
        dsForecast: <%: this.ID %>_dsForecast,
        
        onGridEdit: EditForecast,
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grdForecast").getKendoGrid();
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);
                if (_currRole != "ADM" && (_roleInProject == "engineer")) {
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();
                }
                
            
            });
        }
    });

    function EditForecast(e) {
        
        if (e.model.isNew()) {
            
            $(".k-window-title").html("Add");
        } else {
           
            $(".k-window-title").html("Edit");
           
        }
    }

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel);
    });

</script>