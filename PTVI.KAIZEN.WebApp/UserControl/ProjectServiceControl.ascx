﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectServiceControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ProjectServiceControl" %>
<%@ Register Src="~/UserControl/AssignPCMCControl.ascx" TagPrefix="uc1" TagName="AssignPCMCControl" %>
<%@ Register Src="~/UserControl/MaterialCoordListControl.ascx" TagPrefix="uc1" TagName="MaterialCoordListControl" %>
<%@ Register Src="~/UserControl/ProjectServiceCostReportControl.ascx" TagPrefix="uc1" TagName="ProjectServiceCostReportControl" %>
<%@ Register Src="~/UserControl/ProjectServiceForecastSpendingControl.ascx" TagPrefix="uc1" TagName="ProjectServiceForecastSpendingControl" %>


<div id="ProjectServiceForm">
        <div class="k-content wide">
            <div>
                <div id="ProjectServiceTabStrip">
                    <ul>
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectController) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignMaterialCoordinator) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectOfficer))
                    { %>
                        <li>Assign PC, MC & PO</li>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.MaterialStatusReport))
                    { %>
                        <li>Material Status</li>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.CostReport))
                    { %>
                        <li>Cost Report</li>
                     <% }%>
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ForecastSpending))
                    { %>

                        <li>Service Commitment Spending</li>
                     <% }%>
                    </ul>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectController) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignMaterialCoordinator) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectOfficer))
                    { %>
                    <div>
                        <uc1:AssignPCMCControl runat="server" ID="AssignPCMC" />
                    </div>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.MaterialStatusReport))
                    { %>
                    <div>
                        <uc1:MaterialCoordListControl runat="server" ID="MaterialCoordListControl1" />
                    </div>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.CostReport))
                    { %>
                    <div>
                        <uc1:ProjectServiceCostReportControl runat="server" ID="ProjectServiceCostReport" />
                    </div>
                     <% }%>
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ForecastSpending))
                    { %>
                    <div>
                        <uc1:ProjectServiceForecastSpendingControl runat="server" id="ProjectServiceForecastSpendingControl" />
                    </div>
                     <% }%>

                </div>
            </div>
        </div>
    </div>

    <script>

        var <%: this.ID %>_ProjectSeviceVM = kendo.observable({
            selectedTabIndex: 0
        });


            // Tab control
        function onSelectTabstripProjectService(e) {
            var indexTab = $(e.item).index();

            if (indexTab == 2) {
                kendo.ui.progress($("#ProjectServiceForm"), true);
              
                if (ProjectServiceCostReport_viewCostReportModel.columnProjectKPIProgressMonthlyModule == null) {
                    //setTimeout(function () {
                          //do something special
                          kendo.bind($("#ProjectServiceCostReport_CostReportControl"), ProjectServiceCostReport_viewCostReportModel);
                          ProjectServiceCostReport_viewCostReportModel.generateGriCostReportControl();
                    //}, 100);
                }
                kendo.ui.progress($("#ProjectServiceForm"), false);
            }
        }

        var ProjectServiceTabStrip = $("#ProjectServiceTabStrip").kendoTabStrip({
            select: onSelectTabstripProjectService,
            tabPosition: "top",
            animation: {
                open: {
                    effects: "fadeIn"
                }
            },
        });
        
        ProjectServiceTabStrip = $("#ProjectServiceTabStrip").getKendoTabStrip();
        ProjectServiceTabStrip.select(0);

        kendo.bind($("#ProjectServiceForm"), <%: this.ID %>_ProjectSeviceVM);
           // --Tab control
//        OnDocumentReady = function () {
//        }

    </script>
