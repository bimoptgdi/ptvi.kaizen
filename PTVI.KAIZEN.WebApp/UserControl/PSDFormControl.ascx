﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PSDFormControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.PSDFormControl" %>
<%@ Register Src="~/UserControl/UploaderNewControl.ascx" TagPrefix="uc1" TagName="UploadControl" %>
<%@ Register Src="~/UserControl/IpromK2PSDApprovalControl.ascx" TagPrefix="uc1" TagName="IpromK2PSDApprovalControl" %>

<script>
    var <%: this.ID %>_popUpAttachments;
    var <%: this.ID %>_viewPSDListModel = new kendo.observable({
        remarksRivise: "",
        curPsdApprovalId: 0,
        dsPSDListSource: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                if (e.type == "create" || e.type == "update") {
                    <%: this.ID %>_viewPSDListModel.set("curPsdApprovalId", e.response.id);
                    <%--   if ($("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles().length > 0) {
                        $("#<%: this.ID %>_fileNameId").getKendoUpload().upload();
                        e.response.fileName = $("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles()[0].name;
                    }--%>

                    if ($("#<%: this.ID %>_uploaderDocCR").getKendoUpload().getFiles().length > 0) {
                        $("#<%: this.ID %>_uploaderDocCR").getKendoUpload().upload();
                        e.response.fileName = $("#<%: this.ID %>_uploaderDocCR").getKendoUpload().getFiles()[0].name;
                    } else {
                        $.ajax({
                            url: _webApiUrl + "PSDApprovals/registerToK2/" + <%: this.ID %>_viewPSDListModel.get("curPsdApprovalId"),
                            type: "GET",
                            success: function (iPromData) {
                            },
                            error: function (jqXHR) {
                            }
                        });

                    }
                }

                if (e.type == "update") {
                    var request = {
                        Remark: <%: this.ID %>_viewPSDListModel.get("remarksRivise")
                    }
                    $.ajax({
                        url: _webApiUrl + "PSDApprovals/RequestNeedRevised/" + <%: this.ID %>_viewPSDListModel.get("curPsdApprovalId"),
                        type: "POST",
                        async: false,
                        data: request,
                        success: function (data) {
                            kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);

                            alert("This request is successfully revised");

                            <%: this.ID %>_viewPSDListModel.set("remarksRivise", "");
                        },
                        error: function (jqXHR) {
                            kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
                            alert(jqXHR);
                        }
                    });

                }

                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number", defaultValue: _projectId },
                        submittedDate: { type: "date", defaultValue: null },
                        createdDate: { type: "date", defaultValue: null },
                        completionDate: { type: "date", defaultValue: null },
                    }
                }
            },
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "PSDApprovals/getPsdByProjectID/" + _projectId,
                    dataType: "json"
                },
                create: { url: "<%: WebApiUrl %>" + "PSDApprovals", type: "POST" },
                update: {
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "PSDApprovals/" + data.id;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "PSDApprovals/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (data, operation) {
                    if (operation === "update" || operation === "create") {
                        data.submittedDate = kendo.toString(data.submittedDate, "MM/dd/yyyy HH:mm:ssZ");
                        data.createdDate = kendo.toString(data.createdDate, "MM/dd/yyyy HH:mm:ssZ");
                    }
                    return data;
                }
            },
            pageSize: 10
        }),
        savePSDList: function (e) {
            if (e.model.isNew()) {
                if ($("#<%: this.ID %>_uploaderDocCR").getKendoUpload().getFiles().length == 0) {
                    alert("Please Select Data Attachment!")
                    e.preventDefault()
                }
            }
            this.set("remarksRivise", $("#<%: this.ID %>_remarksRivise").val())
        },
        editPSDList: function (e) {
            if (e.model.isNew() && e.model.employeeId == null) {
                $(".k-grid-update").closest('.k-widget.k-window').find(".k-window-title").html("Add PSD");
                $(".k-grid-update").html('<span class="k-icon k-i-check"></span>Save');
            }
            else
                $(".k-grid-update").closest('.k-widget.k-window').find(".k-window-title").html("Edit PSD");

            e.model.dirty = true;

            //remove session kendo upload
            $("#<%: this.ID %>_popUpAttachments .k-clear-selected").click();

            $("#<%: this.ID %>_tdFileNameId").closest(".k-edit-form-container").find(".k-edit-buttons").find(".k-grid-update").html('<span class="k-icon k-i-check"></span>Submit');

            <%-- $("#<%: this.ID %>_fileNameId").kendoUpload({
                async: {
                    saveUrl:  <%: this.ID %>_viewPSDListModel.saveHandler,
                    autoUpload: false
                },
                multiple: false,
                success:  <%: this.ID %>_viewPSDListModel.onSuccessAtthment,
                upload:  <%: this.ID %>_viewPSDListModel.onUploadAttachment,
                error: function (e) {
                    kendo.ui.progress($(".container-main"), false);
                    var that = this;
                    $("<div />").kendoDialog({
                        title: false,
                        closable: false,
                        modal: true,
                        content: "<div>File failed to upload to document center. But if there are another changes, they have been saved.</div><div>Please check your network connection and upload again.</div>",
                        actions: [{
                            text: 'OK',
                            action: function (e) {
                                window.location = "<%: WebAppUrl%>ProjectBudgetApprovalForm.aspx";
                                return true;
                            }
                        }]
                    }).data("kendoDialog").open();
                },
                complete: <%: this.ID %>_onComplete
            });--%>

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(660).data("kendoWindow").center();
                $(".k-edit-buttons").width(640);
            }, 100);
        },
        saveHandler: _webApiUrl + "upload/PostUploadPSD/1",
        onSuccessAtthment: function (e) {
            $.ajax({
                url: _webApiUrl + "PSDApprovals/registerToK2/" + <%: this.ID %>_viewPSDListModel.get("curPsdApprovalId"),
                type: "GET",
                success: function (iPromData) {
                },
                error: function (jqXHR) {
                }
            });

            <%: this.ID %>_viewPSDListModel.dsPSDListSource.read();
        },
        onUploadAttachment: function (e) {
            e.data = { typeName: "psdApproval", typeDetail: "psdApproval", badgeNo: _currBadgeNo, typeId:  <%: this.ID %>_viewPSDListModel.curPsdApprovalId };
        },
        onErrorAttachment: function (e) {
            kendo.ui.progress($(".container-main"), false);
            var that = this;
            $("<div />").kendoDialog({
                title: false,
                closable: false,
                modal: true,
                content: "<div>File failed to upload to document center. But if there are another changes, they have been saved.</div><div>Please check your network connection and upload again.</div>",
                actions: [{
                    text: 'OK',
                    action: function (e) {
                        //window.location = "<%: WebAppUrl%>ProjectBudgetApprovalForm.aspx";
                        return true;
                    }
                }]
            }).data("kendoDialog").open();
        },
        onSelectAttachment: function (e) {
            if (<%: this.ID %>_viewPSDListModel.cekValidateLoader(e)) {
                alert("Upload pdf files only!")
                e.preventDefault();
            } else {
                $("#<%: this.ID %>_idFileName").html(<%: this.ID %>_viewPSDListModel.getFileInfo(e));
            }
        },
        cekValidateLoader: function (e) {
            var result = false;
            $.map(e.files, function (file) {
                if (file.validationErrors) {
                    if (file.validationErrors.length > 0) {
                        result = true;
                    }
                }
            });
            return result
        },
        getFileInfo: function (e) {
            console.log(e);
            return $.map(e.files, function (file) {
                var info = file.name;

                //// File size is not available in all browsers
                //if (file.size > 0) {
                //    info += " (" + Math.ceil(file.size / 1024) + " KB)";
                //}
                return info;
            }).join(", ");
        }
    });
    function <%: this.ID %>_gridEdit(dataItem) {
        var that = $("#<%: this.ID %>_grdPSDLIst").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem + "']")
        $("#<%: this.ID %>_grdPSDLIst").getKendoGrid().editRow($(that));
    }

    function <%: this.ID %>_showDetails(e) {
        $('#<%: this.ID %>_uploaderDocCR').click();
    }

    function <%: this.ID %>_onViewPSDApproverClick(appID) {
        <%: this.ID %>_viewPSDListModel.set("k2State", "Read Details");

        $("#<%: this.ID %>_popupApproval").prevUntil("k-window-titlebar").find(".k-window-title").html("Project Scope Definition - Details");

        $.ajax({
            url: _webApiUrl + "PSDApprovals/" + appID,
            type: "GET",
            success: function (iPromData) {
                IpromK2PSDApprovalControlDetails_viewModel.set("psdRequestID", iPromData.id);
                IpromK2PSDApprovalControlDetails_Init(iPromData, "#", false, false);
            },
            error: function (jqXHR) {
            }
        });


        IpromK2PSDApprovalControlDetails_viewModel.set("onCloseEvent", function (e) { <%: this.ID %>_popupApproval.close(); });
        $(".form-group.form-group-sm").hide();
        <%: this.ID %>_popupApproval.center().open();
    }

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewPSDListModel);

        $("#<%: this.ID %>_popupApproval").kendoWindow({
            title: "Approval",
            modal: true,
            visible: false,
            resizable: false,
            width: "80%",
            height: "95%",
            activate: function () {
                // lookup bind

            }
        });
        <%: this.ID %>_popupApproval = $("#<%: this.ID %>_popupApproval").data("kendoWindow");

        <%: this.ID %>_popUpAttachments = $("#<%: this.ID %>_popUpAttachments").kendoWindow({
            title: "Add Attachment",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        $("#<%: this.ID %>_uploaderDocCR").kendoUpload({
            async: {
                saveUrl:  <%: this.ID %>_viewPSDListModel.saveHandler,
                autoUpload: false
            },
            multiple: false,
            success:  <%: this.ID %>_viewPSDListModel.onSuccessAtthment,
            upload:  <%: this.ID %>_viewPSDListModel.onUploadAttachment,
            error: <%: this.ID %>_viewPSDListModel.onErrorAttachment,
            select: <%: this.ID %>_viewPSDListModel.onSelectAttachment,
            validation: {
                allowedExtensions: [".pdf"]
            }
        });
    });

</script>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div id="<%: this.ID %>_grdPSDLIst" data-role="grid" data-bind="source: dsPSDListSource, events: { edit: editPSDList, save: savePSDList }"
                    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ProjectScopeDefinition))
            { %>
            data-toolbar= "[{name:'create', text:'Add PSD'}]"
            <% } %>
            data-columns="[
            { field: 'fileNamePsd', title: 'File PSD', width: 300, template: kendo.template($('#<%: this.ID %>_fileNamePsd-template').html()) },
            { field: 'remarks', title: 'Description', width: 300 },
            { field: 'submittedDate', title: 'Submitted Date', width: 150, format: '{0: dd MMM yyyy HH:mm:ss}'},
            { field: 'completionDate', title: 'Completion Date', width: 150, format: '{0: dd MMM yyyy HH:mm:ss}'},
            { field: 'approvalStatus', title: 'Status', width: 150, template: kendo.template($('#<%: this.ID %>_approvalStatus-template').html())},
            ]" 
            data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editPSDList").html())}'
            data-pageable="true"
            ></div>
            <script type="text/x-kendo-template" id="<%: this.ID %>_fileNamePsd-template">
                #= data.refDocId ? '<a href="<%:DocServer%>getdoc.aspx?id=' + data.refDocId + '&op=n" target="_blank">' + data.fileNamePsd + '</a>' : '-' #
            </script>
            <script type="text/x-kendo-template" id="<%: this.ID %>_approvalStatus-template">
                #= data.approvalStatus ? '<a href="\#" onclick="<%: this.ID %>_onViewPSDApproverClick(\''+data.id+'\');return false;">' + data.approvalStatus + '</a>' : "-" # #= (data.approvalStatus == 'NeedRevision' && data.createdBy == _currNTUserID) ? '<a href="\#" onclick="<%: this.ID %>_gridEdit(\''+data.uid+'\');return false;">Edit</a>' : '' #
            </script>
    </div>

    <div id="<%: this.ID %>_popupApproval">
        <uc1:IpromK2PSDApprovalControl runat="server" ID="IpromK2PSDApprovalControlDetails" />
    </div>

    <uc1:UploadControl runat="server" ID="UploadControl" />
    <div id="<%: this.ID %>_popUpAttachments">
        <input type="file" id="<%: this.ID %>_uploaderDocCR" name="uploaderDocMaster" />
    </div>
</div>
<script id="<%: this.ID %>_editPSDList" type="text/x-kendo-tmpl">
    <div style="padding:10px">            
        <table style="width: 620px;">
            <tr>
                <td style="width: 85px; padding-top: 1px;">File Name</td>
<%--                <td id="<%: this.ID %>_tdFileNameId"><input name="fileName" id="<%: this.ID %>_fileNameId" type="file" /></td>--%>
                <td style="width: 5px; padding-top: 1px;">:</td>
                <td id="<%: this.ID %>_tdFileNameId"><span id="<%: this.ID %>_idFileName"> #= data.refDocId ? '<a href="<%:DocServer%>getdoc.aspx?id=' + data.refDocId + '&op=n" target="_blank">' + data.fileNamePsd + '</a>' : '-' #</span>
                    <div data-role="button" onclick="<%: this.ID %>_showDetails()">
                        <span class="k-icon k-i-clip-45"></span>Upload
                    </div>                
                </td>
            </tr>
            <tr>
               <td style="padding-top: 1px; vertical-align: top;">Description</td>
               <td style="padding-top: 1px; vertical-align: top;">:</td>
               <td><textarea placeholder="Description" required name="remarks" /></textarea></td>
            </tr>
            # if (data.id > 0) {#
            <tr>
               <td style="padding-top: 1px; vertical-align: top;">Remarks Rivise</td>
               <td style="padding-top: 1px; vertical-align: top;">:</td>
               <td><textarea placeholder="Remark" id="<%: this.ID %>_remarksRivise" /></textarea></td>
            </tr>
            # } #
        </table>
    </div>
</script>
<style>
      #<%: this.ID %>_tdFileNameId .k-clear-selected, #<%: this.ID %>_tdFileNameId .k-upload-selected {
        display: none !important;
      }

</style>

