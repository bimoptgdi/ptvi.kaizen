﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataInputDsgrProdControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DataInputDsgrProdControl" %>
<script>
    var <%: this.ID %>_docId = "0";
    var <%: this.ID %>_docType = "";
    var <%: this.ID %>_docBadgeNo = "";

    var <%: this.ID %>_viewDesignerProdModel = new kendo.observable({
        no: "",
        title: "",
        dsDesignerProdSource: new kendo.data.DataSource({
            autoSync: true,
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTypeOfDrawingLIst"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdTypeOfDrawingLIst"), false);
                if (e.type == "update")
                {
                    <%: this.ID %>_viewDesignerProdModel.otherUpdate();
                }
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "masterDesignerProductionId",
                    fields: {
                        designerType: { editable: false },
                        typeOfDrawing: { editable: false, type: "number" },
                        estimateHours: { editable: false },
                        text: { editable: false },
                        numberOfDrawing: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    async: false,
                    url: function (data) {
                        return _webApiUrl + "ProjectDocumentDrawingTypes/getDrawingActiveFromProjectPosition/" + <%: this.ID %>_docId + "?docType=" +<%: this.ID %>_docType + "&projectId=" + _projectId + "&badgeNo=" + <%: this.ID %>_docBadgeNo;
                    },
                    dataType: "json"
                },
                update: {
                    async: false,
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "ProjectDocumentDrawingTypes/updateByMasterDrawingTypeId/" + data.masterDesignerProductionId;
                    },
                    type: "PUT"
                }
            }
        }),
        dsComplexitySource: new kendo.data.DataSource({
            autoSync: true,
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTypeOfDrawingLIst"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdTypeOfDrawingLIst"), false);
                if (e.type == "update")
                {
                    <%: this.ID %>_viewDesignerProdModel.otherUpdate();
                }
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "masterComplexityDCId",
                    fields: {
                        complexityValue: { type: "number" },
                        category: { editable: false },
                        weight: { editable: false, type: "number" },
                        complexityValue: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    async: false,
                    url: function (data) {
                        return _webApiUrl + "ProjectDocumentComplexities/getComplexityFromProjectDocId/" + <%: this.ID %>_docId;
                    },
                    dataType: "json"
                },
                update: {
                    async: false,
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "ProjectDocumentComplexities/updateByMasterComplexity/" + data.masterComplexityDCId;
                    },
                    type: "PUT"
                }
            }
        }),
        onEdit: function (e) {
            aaa = e;
            if (!e.model.text && !e.model.estimateHours)
            {
                $("#<%: this.ID %>_grdTypeOfDrawingLIst").data("kendoGrid").closeCell();
            }
       },
        onSave: function (e) {
        },
        otherUpdate: function (e) { },
        onChangeParticipation: function (e) {
            $.ajax({
                url: "<%: WebApiUrl %>" + "ProjectDocuments/updateParticipationValue?docId=" + <%: this.ID %>_docId + "&participationValue=" + <%: this.ID %>_viewDesignerProdModel.participationValue,
                type: "POST",
                async: false,
                success: function (result) {
                    if (result.length > 0)
                        status = $.trim(result[0].text);
                },
                error: function (error) {
                }
            });

            this.otherUpdate();
        },
        participationValue: 0
    });

    function <%: this.ID %>_numberOfDrawingEditTemplate(container, options) {
        $('<input name="numberOfDrawing" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                spinners : false
            });
    }

    function <%: this.ID %>_ComplexityValueEditTemplate(container, options) {
        $('<input name="numberOfDrawing" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                spinners : false
            });
    }

    function <%: this.ID %>_bind() {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewDesignerProdModel); 

    }

    $(document).ready(function () {
    });

</script>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div>
            <table style="width: 100%;">
                <tr style="font-size: 17px; font-weight: bold;">
                    <td style="width: 40px;">No</td>
                    <td style="width: 400px;">: <span data-bind="text: no"></span></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr style="font-size: 17px; font-weight: bold;">
                    <td>Title</td>
                    <td>: <span data-bind="text: title"></span></td>
                    <td style="width: 350px;"><div style="float: right">Participation (%): 
                        <input data-role="numerictextbox"
                            data-format="n"
                            data-min="0"
                            data-max="100"                             
                            data-bind="value: participationValue, events: { change: onChangeParticipation }" style="width: 130px">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <span style="font-weight: bold;">Drawing Type per Group Designer</span>
        <div id="<%: this.ID %>_grdTypeOfDrawingLIst" 
            data-role="grid"
    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours))
    { %>
            data-editable= "incell"
    <% } else { %>
            data-editable= "false"
    <% } %>
            data-selectable="true"
            data-bind="source: dsDesignerProdSource, events: { edit: onEdit, save: onSave }"
            data-columns="[
                        { 
                            field: 'typeOfDrawing', title: 'Type', width: 40
                        },
                        { 
                            field: 'estimateHours', title: 'Estimate Hour(s)', width: 80, 
                            template: kendo.template($('#<%: this.ID %>_estimateHours-template').html())
                        },
                        { 
                            field: 'text', title: 'Drawing Name', width: 300, 
                            template: kendo.template($('#<%: this.ID %>_DrawingName-template').html())
                        },
                        { 
                            field: 'numberOfDrawing', title: 'Number Of Drawing', width: 80, 
                            template: kendo.template($('#<%: this.ID %>_numberOfDrawing-template').html()),
                            editor: <%: this.ID %>_numberOfDrawingEditTemplate,
                            attributes: { style: 'text-align: right; vertical-align: top !important;' }
                        }
                        ]"></div>
<script id="<%: this.ID %>_DrawingName-template" type="text/x-kendo-template">
        <div>#= data.text ? data.text : "-" #</div>
</script>
<script id="<%: this.ID %>_estimateHours-template" type="text/x-kendo-template">
        <div>#= data.estimateHours ? data.estimateHours : "0" #</div>
</script>
<script id="<%: this.ID %>_numberOfDrawing-template" type="text/x-kendo-template">
        <div>#= data.numberOfDrawing ? data.numberOfDrawing : 0 #</div>
</script>
        <span style="font-weight: bold;">Complexity</span>
        <div id="<%: this.ID %>_grdComplexityLIst" 
            data-role="grid"
    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours))
    { %>
            data-editable= "incell"
    <% } else { %>
            data-editable= "false"
    <% } %>
            data-selectable="true"
            data-bind="source: dsComplexitySource, events: { edit: onEdit, save: onSave }"
            data-columns="[
                        { 
                            field: 'category', title: 'Complexity Category', width: 300, 
                            template: kendo.template($('#<%: this.ID %>_ComplexityCategory-template').html())
                        },
                        { 
                            field: 'weight', title: 'Weight', width: 80, 
                            template: kendo.template($('#<%: this.ID %>_ComplexityWeight-template').html())
                        },
                        { 
                            field: 'complexityValue', title: 'Complexity Value', width: 80, 
                            template: kendo.template($('#<%: this.ID %>_ComplexityValue-template').html()),
                            editor: <%: this.ID %>_ComplexityValueEditTemplate,
                            attributes: { style: 'text-align: right; vertical-align: top !important;' }
                        }
                        ]"></div>
    </div>
</div>
<script id="<%: this.ID %>_ComplexityCategory-template" type="text/x-kendo-template">
        <div>#= data.category ? data.category : "-" #</div>
</script>
<script id="<%: this.ID %>_ComplexityWeight-template" type="text/x-kendo-template">
        <div>#= data.weight ? data.weight : "0" #</div>
</script>
<script id="<%: this.ID %>_ComplexityValue-template" type="text/x-kendo-template">
        <div>#= data.complexityValue ? data.complexityValue : "0" #</div>
</script>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .engMaterialsContainer{
        width: 400px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }
</style>

