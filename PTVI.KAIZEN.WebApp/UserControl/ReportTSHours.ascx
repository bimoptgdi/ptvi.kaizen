﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportTSHours.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportTSHours" %>
<script>
    var <%: this.ID %>_chart;
    var <%: this.ID %>_reportWorkhoursCModel = kendo.observable({
        titleChart: "Workhours Consumption",
        valueYear: new Date().getFullYear(),
        area: null,
        totalTS: 0,
        dsWorkhoursCons: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
                
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        area: {type: "string"},
                        workHours: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var year = <%: this.ID %>_reportWorkhoursCModel.valueYear ? <%: this.ID %>_reportWorkhoursCModel.valueYear : 0;
                        var area = <%: this.ID %>_reportWorkhoursCModel.area;
                        return _webApiUrl + "timesheet/ReportTSHours/" + year + "?area=" + area;
                    },
                    dataType: "json",
                },
                    parameterMap: function (data, type) {
                        if (type == "read") {

                        }

                    }
            },
            pageSize: 10,
            sort: [{ field: "area", dir: "asc" }]
        }),
        onChange: function (e) {
            <%: this.ID %>_reportWorkhoursCModel.valueYear = this.valueYear;
            <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read();
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").options.title.text = ""
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").refresh();
            <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read().then(function () {
                var dt = <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.data();
                var totalCountTS = 0;
                $.each(dt, function (key, value) {
                    totalCountTS = totalCountTS + value.countTS;
                })
                <%: this.ID %>_reportWorkhoursCModel.totalTS = totalCountTS;
                $("#<%: this.ID %>_totalTS").html("Total Technical Support: " + totalCountTS);
            });
        },
        onChangeArea: function (e) {
            <%: this.ID %>_reportWorkhoursCModel.area = kendo.stringify(this.area);
            <%: this.ID %>_reportWorkhoursCModel.valueYear = this.valueYear;
            <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read();
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").options.title.text = ""
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").refresh();
            <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read().then(function () {
                var dt = <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.data();
                var totalCountTS = 0;
                $.each(dt, function (key, value) {
                    totalCountTS = totalCountTS + value.countTS;
                })
                <%: this.ID %>_reportWorkhoursCModel.totalTS = totalCountTS;
                $("#<%: this.ID %>_totalTS").html("Total Technical Support: " + totalCountTS);
            });
        },
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
        dsArea: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        areaDesc: { type: "string" },
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "areaInvolveds/areaInvolvedInActiveTS/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
        }),
    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_reportBudgetPerformance"), <%: this.ID %>_reportWorkhoursCModel);
        $("#<%: this.ID %>_workhoursCons").kendoChart({
            title: {
                text: ""
            },
            legend: {
                position: "bottom",
                visible: false
            },
            dataSource: <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons,
            transitions: false,
            seriesColors: ["#943432", "#6D8B2E"],
            series: [{
                name: "No of TS",
                type: "column",
                stack: "area",
                field: "countTS",
                labels: { visible: true, position: "above" }
            }, {
                name: "Hours",
                type: "line",
                stack: "area",
                axis: "hours",
                field: "workHoursTS",
                labels: { visible: true, position: "center"
                }
            }, 
            ],
            valueAxes: [{
                color: "#943432",
            }, {
                name: "hours",
                color: "#6D8B2E",
            }, ],
            categoryAxis: {
                field: "area",
                axisCrossingValues: [0, 10]
            },
            legend: {
                position: "bottom"
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= kendo.format('{0:N0}', value) #"
            }
        });

        <%: this.ID %>_chart = $("#<%: this.ID %>_workhoursCons").data("kendoChart");
        <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read().then(function () {
            var dt = <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.data();
            var totalCountTS = 0;
            $.each(dt, function (key, value) {
                totalCountTS = totalCountTS + value.countTS;
            })
            <%: this.ID %>_reportWorkhoursCModel.totalTS = totalCountTS;
            $("#<%: this.ID %>_totalTS").html("Total Technical Support: " + totalCountTS);
        });
    });
</script>

<div id="<%: this.ID %>_reportBudgetPerformance">
    <div style="display:inline-block">
    Year: <input data-role="combobox"
            data-placeholder="Select Year"                            
            data-text-field="year"
            data-value-field="year"
            data-bind="value: valueYear, source: dsYearProject,
                        events: {
                            change: onChange
                        }"
            style="width: 250px" />
    </div>
    <div style="display:inline-block;vertical-align:middle">
        <div style="display:inline-block;text-align: left;vertical-align:top; height:30px; width:50px;"> Area:</div>
        <div style="display:inline-block; width:550px; ">
            <select data-role="multiselect"
            data-value-primitive="true"
            data-placeholder="Select Area"                            
            data-text-field="areaDesc"
            data-value-field="id"
            data-bind="value: area, source: dsArea,
                        events: {
                            change: onChangeArea
                        }"
            style="display:inline-block;vertical-align: bottom;width: 500px" ></select>
            </div>
        </div>
    <br />
    <br />
    <br />
    <div id="<%: this.ID %>_totalTS" style="width: 1000px;"></div>
    <div id="<%: this.ID %>_workhoursCons" style="width: 1000px; height: 400px;"></div>
</div>
