﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainProjectFormControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.MainProjectFormControl" %>

<script>
    common_getAllStatusIprom();
    var <%: this.ID %>_viewMainProjectModel = kendo.observable({
        dsListProjectDocumentsEWP: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListMainProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListMainProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        progress: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    //url: _webApiUrl + "ProjectDocuments/listPDByProjectId/" + _projectId,
                    url: _webApiUrl + "Project/listSumaryProjectDetailEWP/" + _projectId,
                    dataType: "json"
                }
            },
            pageSize: 10
        }),
        dsListProjectDocumentsMRLIst: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        prPlanDate: { type: "date" },
                        prActualDate: { type: "date" },
                        poPlanDate: { type: "date" },
                        poActualDate: { type: "date" },
                        onSitePlanDate: { type: "date" },
                        onSiteActualDate: { type: "date" },
                        rfqPlanDate: { type: "date" },
                        rfqActualDate: { type: "date" }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "Project/listSumaryProjectDetailMRLIst/" + _projectId,
                    dataType: "json"
                }
            },
            pageSize: 10
        }),
        dsListProjectDocumentsOther: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListMainProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListMainProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        progress: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    //url: _webApiUrl + "ProjectDocuments/listPDByProjectId/" + _projectId,
                    url: _webApiUrl + "Project/listSumaryProjectDetailOther/" + _projectId,
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        })
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_MainProjectContent"), <%: this.ID %>_viewMainProjectModel);
<%--        <%: this.ID %>_viewMainProjectModel.dsListProjectDocuments.read();--%>
    });

</script>
<div id="<%: this.ID %>_MainProjectContent"><br />
    <div class="headerSubProject" style="font-size: 16px;font-weight: bold;">Summary Engineer Task</div>
    <div id="<%: this.ID %>_grdListMainProjectEWP" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-filterable="true"
        data-columns="[
            {
                field: 'task', title: 'Task',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                template: '#: task ? task : \'-\'#',
                width: 200
            },
            {
                field: 'by', title: 'PIC',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                template: '#: by ? by : \'-\'#',
                width: 110
            },
            {
                title: 'Plan Date', 
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                columns: [
                            {
                                field: 'planStartDate', title: 'Start',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: planStartDate ? kendo.toString(planStartDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            },
                            {
                                field: 'planFinishDate', title: 'Finish',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: planFinishDate ? kendo.toString(planFinishDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            }]
            },
            {
                title: 'Actual Date', 
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                columns: [
                            {
                                field: 'actualStartDate', title: 'Start',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: actualStartDate ? kendo.toString(actualStartDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            },
                            {
                                field: 'actualFinishDate', title: 'Finish',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: actualFinishDate ? kendo.toString(actualFinishDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            }]
            },
<%--            {
                field: 'status', title: 'Status',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                template: '#:status ? common_getStatusIpromDesc(status) : \'\'#',
                width: 70
            },--%>
            {
                field: 'documentTrafic', title: ' ',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; vertical-align: top !important;' },
                template: '#if (documentTrafic) { #<img class=\'logo\' src=\'images/#:common_getTrafficImageUrl(documentTrafic)#\' style=\'padding-top: 0px;\'>#}#',
                filterable: false, 
                width: 40
            }
        ]"
        data-bind="source: dsListProjectDocumentsEWP"
        data-pageable="{ buttonCount: 5, pageSizes: [10,20,50,100,'all'],}">
    </div><br />
    <div class="headerSubProject" style="font-size: 16px;font-weight: bold;">Summary MR List</div>

    <script type="text/x-kendo-template" id="<%: this.ID %>_prDate-template">
        <div class="subColumnTitle">Plan</div>
        <div>#= data.prPlanDate ? kendo.toString(data.prPlanDate,"dd MMM yyyy") : "-" #</div>
        <div class="subColumnTitle">Actual</div>
        <div>#= data.prActualDate ? kendo.toString(data.prActualDate,"dd MMM yyyy") : "-" #</div>
    </script>

    <script type="text/x-kendo-template" id="<%: this.ID %>_poPlanDate-template">
        <div class="subColumnTitle">Plan</div>
        <div>#= data.poPlanDate ? kendo.toString(data.poPlanDate,"dd MMM yyyy") : "-" #</div>
        <div class="subColumnTitle">Actual</div>
        <div>#= data.poActualDate ? kendo.toString(data.poActualDate,"dd MMM yyyy") : "-" #</div>
    </script>

    <script type="text/x-kendo-template" id="<%: this.ID %>_onSitePlanDate-template">
        <div class="subColumnTitle">Plan</div>
        <div>#= data.onSitePlanDate ? kendo.toString(data.onSitePlanDate,"dd MMM yyyy") : "-" #</div>
        <div class="subColumnTitle">Actual</div>
        <div>#= data.onSiteActualDate ? kendo.toString(data.onSiteActualDate,"dd MMM yyyy") : "-" #</div>
    </script>

    <script type="text/x-kendo-template" id="<%: this.ID %>_rfqPlanDate-template">
        <div class="subColumnTitle">Plan</div>
        <div>#= data.rfqPlanDate ? kendo.toString(data.rfqPlanDate,"dd MMM yyyy") : "-" #</div>
        <div class="subColumnTitle">Actual</div>
        <div>#= data.rfqActualDate ? kendo.toString(data.rfqActualDate,"dd MMM yyyy") : "-" #</div>
    </script>

    <div id="<%: this.ID %>_grdListMainProjectMRList" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-filterable="true"
        data-columns="[
            { field: 'mrNo', title: 'MR No', width: 100 },
            { field: 'materialDesc', title: 'MR Description', width: 80},
            { field: 'prPlanDate', title: 'PR Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_prDate-template').html())
            },
            { field: 'poPlanDate', title: 'PO Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_poPlanDate-template').html())
            },
            { field: 'onSitePlanDate', title: 'On Site Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_onSitePlanDate-template').html())
            },
            { field: 'rfqPlanDate', title: 'RFQ Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_rfqPlanDate-template').html())
            },
            { field: 'engineer', title: 'Engineer', width: 80 },
            {
                field: 'documentTrafic', title: ' ',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; vertical-align: top !important;' },
                template: '#if (documentTrafic) { #<img class=\'logo\' src=\'images/#:common_getTrafficImageUrl(documentTrafic)#\' style=\'padding-top: 0px;\'>#}#',
                filterable: false, 
                width: 40
            }

            ]" 
        data-bind="source: dsListProjectDocumentsMRLIst"
        data-pageable="{ buttonCount: 5, pageSizes: [10,20,50,100,'all'],}">
    </div><br />
    <div class="headerSubProject" style="font-size: 16px;font-weight: bold;">Summary Other Task</div>
    <div id="<%: this.ID %>_grdListMainProjectOther" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-filterable="true"
        data-columns="[
            {
                field: 'task', title: 'Task',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                template: '#: task ? task : \'-\'#',
                width: 200
            },
            {
                field: 'by', title: 'PIC',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                template: '#: by ? by : \'-\'#',
                width: 110
            },
            {
                title: 'Plan Date', 
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                columns: [
                            {
                                field: 'planStartDate', title: 'Start',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: planStartDate ? kendo.toString(planStartDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            },
                            {
                                field: 'planFinishDate', title: 'Finish',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: planFinishDate ? kendo.toString(planFinishDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            }]
            },
            {
                title: 'Actual Date', 
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                columns: [
                            {
                                field: 'actualStartDate', title: 'Start',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: planStartDate ? kendo.toString(planStartDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            },
                            {
                                field: 'actualFinishDate', title: 'Finish',
                                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                template: '#: actualFinishDate ? kendo.toString(actualFinishDate, \'dd MMM yyyy\') : \'-\'#',
                                width: 70
                            }]
            },
<%--            {
                field: 'status', title: 'Status',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                template: '#:status ? common_getStatusIpromDesc(status) : \'\'#',
                width: 70
            },--%>
            {
                field: 'documentTrafic', title: ' ',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; vertical-align: top !important;' },
                template: '#if (documentTrafic) { #<img class=\'logo\' src=\'images/#:common_getTrafficImageUrl(documentTrafic)#\' style=\'padding-top: 0px;\'>#}#',
                filterable: false, 
                width: 40
            }
        ]"
        data-bind="source: dsListProjectDocumentsOther"
        data-pageable="{ buttonCount: 5, pageSizes: [10,20,50,100,'all'],}">
    </div>
</div>
