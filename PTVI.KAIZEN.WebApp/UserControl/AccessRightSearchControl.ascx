﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessRightSearchControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.AccessRightSearchControl" %>
<script>
    var <%: this.ID %>_pickHandler;
    var ucAccessRight_grdAccess;
    var ucAccessRight_dataSource;
    
   function <%: this.ID %>_PopulateAccessRightData(filter, pickHandler) {
        <%: this.ID %>_pickHandler = pickHandler;
        var a = [];
        var dtGrdAccessright = $("#grdAccessRight").getKendoGrid().dataSource.data();
        for (var i = 0; i < dtGrdAccessright.length; i++) {
            a.push({ field: "OID", operator: "neq", value: dtGrdAccessright[i].ACCESSRIGHT.OID });
        }

        ucAccessRight_dataSource = new kendo.data.DataSource({
            transport: {
                read: { url: _webApiUrl + "Accessright", dataType: "json" }
            },
            pageSize: 10,
            sort: { field: "DESCRIPTION", dir: "asc" },
            schema: {
                model: {
                    id: "OID"//,
                    //fields: {
                    //    ACCESSDESCRIPTION: "ACCESSDESCRIPTION"
                    //}
                }
            }

        });

        ucAccessRight_dataSource.filter({ 'logic': 'and', 'filters': a });


        if ($("#ucAccessRight_grdAccess").data("kendoGrid") == null) {
            ucAccessRight_grdAccess = $("#ucAccessRight_grdAccess").kendoGrid({
                dataSource: ucAccessRight_dataSource,
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                reorderable: true,
                resizable: true,
                scrollable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                height: 400,
                edit: function (e) {
                    //console.log(e);
                },
                columns: [
                    {//*
                        command: [
                            { name: "pick", text: "Select", click: <%: this.ID %>_pickHandler }
                        ], title: "&nbsp;", width: "100px"
                    },
                    { field: "DESCRIPTION", title: "Access" }                    
                ],
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with"
                        }
                    }
                }
            }).data("kendoGrid");

        } else {
            ucAccessRight_grdAccess.setDataSource(ucAccessRight_dataSource);
        }

    }
</script>
<div id="ucAccessRightSearch" class="adrContent">
    <fieldset class="adrFieldset">
        <legend class="adrLegend">Access Right Search</legend>
        <div id="ucAccessRight_grdAccess"></div>
    </fieldset>

</div>
