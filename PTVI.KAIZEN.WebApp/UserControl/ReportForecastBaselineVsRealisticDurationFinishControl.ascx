﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportForecastBaselineVsRealisticDurationFinishControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportForecastBaselineVsRealisticDurationFinishControl" %>
<div id="<%: this.ID %>_reportForecastBaselineVsOptimisContainer">
    <div data-bind="visible:loaded" style="display:none;">
        <div style="font-weight:bold">
            <div style="text-align:center">Forecast Realistic Approach - Duration</div>
            <div style="text-align:center" data-bind="html:taskName"></div>
            <div style="text-align:center" >Position <span data-bind="html: position"></span></div>
            <div style="text-align:center" >Baseline Finish: <span data-bind="html: baselineFinishStr"></span></div>
            <div style="text-align:center" >Realistic Finish: <span data-bind="html: realisticFinishStr"></span></div>
            <div style="text-align:center" >Status: <span data-bind="html: proporsionalTimeVar"></span>&nbsp;<span data-bind="html: timeDiffStr"></span></div>
        </div><br /><br /><br />
        <div 
            style="height:600px"
            data-role="chart"
            data-transitions="false"
            data-chart-area="{background: 'lightgray'}"
            data-bind="source: dsForecastBaselineVsOptimis"
            data-series="[  {type: 'rangeColumn', fromField: 'baselineDurationInHours', toField: 'realisticDurationInHours', colorField: 'color'},
                            {type: 'line', field: 'baselineDurationInHours', name: 'Baseline Duration', color: 'yellow'},
                            {type: 'line', field: 'realisticDurationInHours', name: 'Realistic Duration', color: 'blue'}]",
            data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}">
        </div><br /><br />
        <div style="font-size:xx-small">
            <div data-role="grid" data-bind="source: dsForecastBaselineVsOptimis"  data-scrollable="false"
            data-columns="[{ template: '#= ++<%: this.ID %>_vm.record #', title:'No', width: '2%', attributes: {style: 'text-align: right;vertical-align:top'} }, 
                        { field: 'taskName', title: 'Project / Task Name', width: '14%', attributes: {style: 'vertical-align:top'} },
                        { title: 'Start Date', headerAttributes: {style: 'text-align: center'}, columns: [
                            { field: 'baselineStart', title: 'Baseline', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'estimatedStart', title: 'Estimated', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} }]
                        }, 
                        { title: 'Finish Date', headerAttributes: {style: 'text-align: center'}, columns: [
                            { field: 'baselineFinish', title: 'Baseline', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'optimisticFinish', title: 'Optimistic', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'realisticFinish', title: 'Realistic', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} }]
                        }, 
                        { title: 'Duration', headerAttributes: {style: 'text-align: center'}, columns: [
                            { field: 'baselineDurationInHours', title: 'Baseline', attributes: {style: 'text-align: right;vertical-align:top'}, width: '6%', format: '{0:n2}' },
                            { field: 'optimisticDurationInHours', title: 'Optimistic', attributes: {style: 'text-align: right;vertical-align:top'}, width: '6%', format: '{0:n2}' },
                            { field: 'realisticDurationInHours', title: 'Realistic', attributes: {style: 'text-align: right;vertical-align:top'}, width: '6%', format: '{0:n2}' }]
                        },                         
                        { field: 'contractor', title: 'Contractor', attributes: {style: 'vertical-align:top'} }]"></div>
            </div>
    </div>   
</div>
<div  id="<%: this.ID %>_errorMessageContainer">
    <div data-bind="visible: notFound" style="top: 60px;width: 400px;text-align: center;margin: auto;position: relative;" >
        <div style="border:1px solid red;color:red;font-size:x-large;" data-bind="html: ErrMsg"></div>
    </div>
</div>

<script>
    var <%: this.ID %>_vm = kendo.observable({
        taskName: null,
        position: null,
        dsForecastBaselineVsOptimis: null,
        loaded: false,
        record: 0
    });

    var <%: this.ID %>_ervm = kendo.observable({
        notFound: false,
        ErrMsg: "No Data Found"
    });

    function <%: this.ID %>_populate(projectShiftID, taskUid, callback) {
        $.ajax({
            url: webApiUrl + "iptreport/reportdata?reportName=IPT_FORECAST_OPTIMISVSPROPORSIONAL&projectshiftid=" + projectShiftID + "&taskUid=" + taskUid,
            success: function (result) {
                if (result != null) {
                    for (var i = 0; i < result.list.length; i++) {
                        result.list[i]['baselineDurationInHours'] = (result.list[i].baselineDuration / 10.0) / 60.0;
                        result.list[i]['optimisticDurationInHours'] = (result.list[i].optimisticDuration / 10.0) / 60.0;
                        result.list[i]['realisticDurationInHours'] = (result.list[i].realisticDuration / 10.0) / 60.0;

                        result.list[i]['color'] = result.list[i].realisticDurationVar == 'Delay' ? 'red' : 'green';
                    }

                    <%: this.ID %>_vm.set("taskName", result.taskName);
                    <%: this.ID %>_vm.set("position", kendo.toString(kendo.parseDate(result.position), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("proporsionalTimeVar", result.proporsionalTimeVar);
                    <%: this.ID %>_vm.set("proporsionalDurationVar", result.proporsionalDurationVar);
                    <%: this.ID %>_vm.set("baselineFinishStr", kendo.toString(kendo.parseDate(result.baselineFinish, 'yyyy-MM-ddTHH:mm'), 'dd MMM yyyy  HH:mm'));
                    <%: this.ID %>_vm.set("realisticFinishStr", kendo.toString(kendo.parseDate(result.realisticFinish, 'yyyy-MM-ddTHH:mm'), 'dd MMM yyyy  HH:mm'));

                    if (result.proporsionalDurationVar != "On Duration")
                        <%: this.ID %>_vm.set("timeDiffStr", kendo.toString((kendo.parseDate(result.baselineFinish, 'yyyy-MM-ddTHH:mm') - kendo.parseDate(result.realisticFinish, 'yyyy-MM-ddTHH:mm')) / 60000.0 / 60.0, "n2") + ' Hours');

                    <%: this.ID %>_vm.dsForecastBaselineVsOptimis = new kendo.data.DataSource({
                        data: result.list,
                        schema: {
                            model: {
                                fields: {
                                    baselineStart: { type: 'date' },
                                    estimatedStart: { type: 'date' },
                                    baselineFinish: { type: 'date' },
                                    optimisticFinish: { type: 'date' },
                                    realisticFinish: { type: 'date' },
                                    baselineDurationInHours: { type: 'number' },
                                    optimisticDurationInHours: { type: 'number' },
                                    realisticDurationInHours: { type: 'number' }
                                }
                            }
                        }
                    });
                    
                    <%: this.ID %>_vm.set("loaded", true);
                    <%: this.ID %>_ervm.set("notFound", false);
                    status = "loaded";
                    kendo.bind($("#<%: this.ID %>_reportForecastBaselineVsOptimisContainer"), <%: this.ID %>_vm);
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
                }

                else {
                    <%: this.ID %>_ervm.set("notFound", true);
                    status = "notfound";
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);                   
                }

                callback(status);
            },
            error: function (e) {

                var jsonResponse = e.responseJSON;
                //console.log(jsonResponse);
                <%: this.ID %>_ervm.set("ErrMsg", jsonResponse.ExceptionMessage);
                <%: this.ID %>_ervm.set("notFound", true);
                status = "Error";
                kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
            }
        });
    }

    function <%: this.ID %>_shortLabels(value) {
        return value;

        var result = "";
        while (value.length > 20) {
            var clipIndex = 20;
            while (value[clipIndex] != ' ')
                clipIndex--;

            result = result + (result != "" ? "\n" : "") + value.substring(0, clipIndex);

            value = value.substring(clipIndex);
        }
    
        result = result + (result != "" ? "\n" : "") + value;

        return result;
    }

</script>

