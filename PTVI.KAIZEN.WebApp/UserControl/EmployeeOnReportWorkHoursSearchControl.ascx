﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeOnReportWorkHoursSearchControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EmployeeOnReportWorkHoursSearchControl" %>
<script>
    var gridRole;

    var <%: this.ID %>_pickEmployeeOnProjectReportBudgetModel = kendo.observable({
        ds_employee_list: new kendo.data.DataSource({
           transport: {
               read: _webApiUrl + "timesheet/listEmployeeReportWorkinHours/1"
            },
            schema: {
                model: {
                    id: "roleLevel"
                }
            }
        
        }),
        detailInit: function (e) {
            var detailGrid = $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    requestStart: function () {
                        kendo.ui.progress($("#<%: this.ID %>_ucEmployeeOnProjectSearch"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("#<%: this.ID %>_ucEmployeeOnProjectSearch"), false);
                    },
                    data: e.data.detail,
                    sort: [{ field: "employeeid", dir: "asc" }],
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true
                },
                scrollable: false,
                sortable: false,
                pageable: false,
                columns: [
                    { template: "<input type='checkbox'  class='isCheckDetail' value='#=employeeid#'  />", width: "50px" },
                    { field: "employeeid", title: "Employee ID", width: 120 },
                    { field: "employeename", title: "Employee Name" }
                ],
                editable: "false"
            });
            detailGrid.on("change", ".isCheckDetail", function ()
            {
                $(e.masterRow).find(":checkbox.isCheckMaster").prop("checked", false);
            });

        },
        dataBound: function () {
            var grid = $("#<%: this.ID %>_EmployeeGrid").getKendoGrid();
            grid.expandRow(".k-master-row");
            $("a.k-icon.k-i-collapse").hide();
        },
        onSelect: function () {
            var result = [];

            var grid = $("#<%: this.ID %>_EmployeeGrid").data('kendoGrid');
            var allChildren = $(grid.element[0]).find('.isCheckDetail:checked')
            $.each(allChildren, function () {
                var detailRow = $(this).closest('tr');
                var table = $(this).closest('div.k-grid');
                var detailGrid = $(table).data('kendoGrid');
                var data = detailGrid.dataItem(detailRow);
                result.push({ employeeId: data.employeeid, employeeName : data.employeename });
                
            });
            return result;
        },
        pickEmployee: function (e) {
        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ucEmployeeOnProjectSearch"), <%: this.ID %>_pickEmployeeOnProjectReportBudgetModel);
        gridRole = $("#<%: this.ID %>_EmployeeGrid").getKendoGrid();
        gridRole.table.on("change", ".isCheckMaster", selectRowMaster);

    });

    function selectRowMaster() {
        var checkbox = $(this);
        var nextRow = checkbox.closest("tr").next(); // find the assosiated detail row
        //Note: the row should be expanded at least once as otherwhise there will be no child grid loaded      									
        if (nextRow.hasClass("k-detail-row")) {
            // and toggle the checkboxes
            nextRow.find(":checkbox")
                .prop("checked", checkbox.is(":checked"));
        }

    }
</script>
<div id="<%: this.ID %>_ucEmployeeOnProjectSearch" class="adrContent">
    <fieldset class="gdiFieldset">
        <legend class="gdiLegend">Employee Search</legend>
        <div id="<%: this.ID %>_EmployeeGrid" 
                    data-role="grid"
                    data-bind="source: ds_employee_list, events: { dataBound: dataBound }",
                    data-pageable="false",
                    data-scrollable="false",
                    data-selectable="true",
                    data-columns='[
                        { template:  kendo.template($("#<%: this.ID %>_chkTemplate").html()), width: "50px" },
                        { field: "roleName", title: "Group" }
                    ]'
                    data-detail-init="<%: this.ID %>_pickEmployeeOnProjectReportBudgetModel.detailInit"
                ></div>
        <script id="<%: this.ID %>_chkTemplate" type="text/x-kendo-tmpl">
            <input type='checkbox' class='isCheckMaster' />
        </script>
        <div style="float: right; margin-top: 12px;">
            <div
                data-role="button"
                data-bind="events: { click: pickEmployee }"
                class="k-button k-primary">
                Select
            </div>
        </div>

    </fieldset>

</div>
