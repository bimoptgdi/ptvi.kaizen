﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MRListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.MRListControl" %>
<%@ Register Src="~/UserControl/UploaderNewControl.ascx" TagPrefix="uc1" TagName="UploadControl" %>

<script>
    var <%: this.ID %>_viewMRListModel = new kendo.observable({
        dsMRListSource: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
                },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        prPlanDate: { type: "date", defaultValue: null },
                        prActualDate: { type: "date" },
                        poPlanDate: { type: "date", defaultValue: null },
                        poActualDate: { type: "date" },
                        onSitePlanDate: { type: "date", defaultValue: null },
                        onSiteActualDate: { type: "date" },
                        remark: { type: "string"},
                        rfqPlanDate: { type: "date" },
                        rfqActualDate: { type: "date" }
                    }
                }
            },
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "MaterialByEngineers/byProject/" + _projectId,
                    dataType: "json"
                },
                create: { url: "<%: WebApiUrl %>" + "MaterialByEngineers/AddMR/" + _projectId, type: "POST" },
                update: {
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "MaterialByEngineers/UpdateMR/" + data.id;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "MaterialByEngineers/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        data.prPlanDate = kendo.toString(data.prPlanDate, 'MM/dd/yyyy');
                        data.poPlanDate = kendo.toString(data.poPlanDate, 'MM/dd/yyyy');
                        data.onSitePlanDate = kendo.toString(data.onSitePlanDate, 'MM/dd/yyyy');
                        data.createdBy = _currNTUserID;
                        data.updatedBy = _currNTUserID;
                    }
                    return data;
                }
            },
            pageSize: 10
        }),
        editMRList: function (e) {
            if (e.model.isNew() && e.model.employeeId == null) {
                $(".k-grid-update").closest('.k-widget.k-window').find(".k-window-title").html("Add MR");
                $(".k-grid-update").html('<span class="k-icon k-i-check"></span>Save');
            }
            else
                $(".k-grid-update").closest('.k-widget.k-window').find(".k-window-title").html("Edit MR");

            if (_planStartDate) {
                $("#<%: this.ID %>_prPlanDate").getKendoDatePicker().min(_planStartDate);
                $("#<%: this.ID %>_poPlanDate").getKendoDatePicker().min(_planStartDate);
                $("#<%: this.ID %>_onSitePlanDate").getKendoDatePicker().min(_planStartDate);
            }

            if (_planFinishDate) {
                $("#<%: this.ID %>_prPlanDate").getKendoDatePicker().max(_planFinishDate);
                $("#<%: this.ID %>_poPlanDate").getKendoDatePicker().max(_planFinishDate);
                $("#<%: this.ID %>_onSitePlanDate").getKendoDatePicker().max(_planFinishDate);
            }
            var prPlanDate = $("#<%: this.ID %>_prPlanDate").getKendoDatePicker();
            var poPlanDate = $("#<%: this.ID %>_poPlanDate").getKendoDatePicker();
            var onSitePlanDate = $("#<%: this.ID %>_onSitePlanDate").getKendoDatePicker();
            prPlanDate.bind("change", function () {
                if (prPlanDate.value() != null)
                {
                    poPlanDate.enable(true);
                    var prDate = new Date(prPlanDate.value());
                    prDate.setDate(prDate.getDate() + 1);
                    if (prDate > _planFinishDate) prDate = _planFinishDate;
                    if (prDate < _planStartDate) prDate = _planStartDate;
                    poPlanDate.min(prDate);
                    poPlanDate.bind("change", function () {
                        if (poPlanDate.value() != null) {
                            onSitePlanDate.enable(true);
                            var poDate = new Date(poPlanDate.value());
                            poDate.setDate(poDate.getDate() + 1);
                            if (poDate > _planFinishDate) poDate = _planFinishDate;
                            if (poDate < _planStartDate) poDate = _planStartDate;
                            onSitePlanDate.min(poDate);
                        }
                     });
                 }
            })
            $(".k-edit-form-container").kendoTooltip({
                filter: "[name='mrNo']",
                position: "top",
                animation: {
                    open: {
                        effects: "fade:in",
                        duration: 500
                    },
                    close: {
                        effects: "fade:out",
                        duration: 500
                    },
                },
                content: function (e) {
                    var content = "Format MR, Ex: S20216E101<br\>"
                    + "S=>Capital, "
                    + "2=>area, "
                    + "02=>number, "
                    + "16=>tahun, "
                    + "E1=>no ewp,"
                    + "01=>nomor mr<br\>";
                    return content;
                }
            }).data("kendoTooltip");

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        }
    });


    function onSelectBillingFile(e) {
        var file = e.files[0];
        if (file.extension != ".xls" && file.extension != ".xlsx") {
            alert('Only excel file is accepted.');

            e.preventDefault();
        }
    }

    function onUploadBillingData(e) {

    }

    function onUploadBillingDataSuccess(e) {
        UploadControl_close();

        $("#<%: this.ID %>_grdMrLIst").getKendoGrid().dataSource.read();
    }

    function onUploadDataError(e) {
        _showDialogMessage("Error Message", "Upload Data Failed.<br/>Please check your input data!", "");
    }



    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewMRListModel); 
        $("#<%: this.ID %>_grdMrLIst .k-grid-upload").find('span').addClass("k-font-icon k-i-xls");
        $("#<%: this.ID %>_grdMrLIst .k-grid-upload").bind("click", function (e) {
            UploadControl_show("Upload Data MR", "Click button below or drag the excel file. Click <a href='template/uploadMRListTemplate.xlsx'>here</a> to get the excel form.",
                false, _webApiUrl + "MaterialByEngineers/upload/" + _projectId + "|" + _currNTUserID, onSelectBillingFile, onUploadBillingData, onUploadBillingDataSuccess, onUploadDataError);

            e.preventDefault();
        });
    });

</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_prDate-template">
    <div class="subColumnTitle">Plan</div>
    <div>#= data.prPlanDate ? kendo.toString(data.prPlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div>#= data.prActualDate ? kendo.toString(data.prActualDate,"dd MMM yyyy") : "-" #</div>
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_poPlanDate-template">
    <div class="subColumnTitle">Plan</div>
    <div>#= data.poPlanDate ? kendo.toString(data.poPlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div>#= data.poActualDate ? kendo.toString(data.poActualDate,"dd MMM yyyy") : "-" #</div>
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_onSitePlanDate-template">
    <div class="subColumnTitle">Plan</div>
    <div>#= data.onSitePlanDate ? kendo.toString(data.onSitePlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div>#= data.onSiteActualDate ? kendo.toString(data.onSiteActualDate,"dd MMM yyyy") : "-" #</div>
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_rfqPlanDate-template">
    <div class="subColumnTitle">Plan</div>
    <div>#= data.rfqPlanDate ? kendo.toString(data.rfqPlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div>#= data.rfqActualDate ? kendo.toString(data.rfqActualDate,"dd MMM yyyy") : "-" #</div>
</script>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div id="<%: this.ID %>_grdMrLIst" data-role="grid" data-bind="source: dsMRListSource, events: { edit: editMRList }"
            data-toolbar= "[
                    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP))
            { %>{name:'create', text:'Add MR'}, <% } %>{ name:'upload', text:'Upload Data (xlsx)' }
            ]"
            data-columns="[
            { field: 'mrNo', title: 'MR No', width: 100 },
            { field: 'materialDesc', title: 'MR Description', width: 80},
            { field: 'prPlanDate', title: 'PR Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_prDate-template').html())
            },
            { field: 'poPlanDate', title: 'PO Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_poPlanDate-template').html())
            },
            { field: 'onSitePlanDate', title: 'On Site Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_onSitePlanDate-template').html())
            },
            { field: 'rfqPlanDate', title: 'RFQ Date', width: 80,
              template: kendo.template($('#<%: this.ID %>_rfqPlanDate-template').html())
            }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP))
            { %>,
            { field: 'engineer', title: 'Engineer', width: 80 },
            { field: 'remark', title: 'Remark', width: 80, template: kendo.template($('#<%: this.ID %>_remark-template').html())},
            {
                command: [{
                    name: 'edit',
                    visible: function (e) {
                        if ((e.engineerId == _currBadgeNo) || (_currRole == 'ADM') || (_currRole == 'PM'))
                            return true;
                        else
                            return false;
                    }
                },
                {
                    name: 'destroy',
                    visible: function (e) {
                        if ((e.engineerId == _currBadgeNo) || (_currRole == 'ADM') || (_currRole == 'PM'))
                            return true;
                        else
                            return false;
                    }
                }
            ], width: 120

<%--                command: ['edit', 'destroy'], width: 120--%>
            }<% } %>
            ]" 
            data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editMRList").html())}'
            data-pageable="true"
            ></div>
    </div>
    <uc1:UploadControl runat="server" ID="UploadControl" />
</div>
<script id="<%: this.ID %>_remark-template" type="text/x-kendo-template">

    #= remark?remark.replace(/(\r?\n)/gi,"<br>"):"" #

</script>
<script id="<%: this.ID %>_editMRList" type="text/x-kendo-tmpl">
    <div style="padding:10px">            
        <table style="width:600px">
            <tr>
                <td style="width:145px">MR No<span style="color:red">*</span></td>
                <td><input required validationMessage="MR No is required" placeholder="MR No" name="mrNo" onfocus="this.value = this.value;" style="width:100%" /></td>
            </tr>
            <tr>
                <td>MR Description<span style="color:red">*</span></td>
                <td><input required validationMessage="MR Description is required" placeholder="MR Description" name="materialDesc" onfocus="this.value = this.value;" style="width:100%" /></td>
            </tr>
            <tr>
                <td>PR Plan Date<span style="color:red">*</span></td>
                <td><input data-role="datepicker" id="<%: this.ID %>_prPlanDate" data-format="dd MMM yyyy" name="prPlanDate" required validationMessage="PR Date Plan Date is required" placeholder="PR Date Plan Date" style="width:180px" /></td>
            </tr>
            <tr>
                <td>PO Plan Date<span style="color:red">*</span></td>
                <td><input data-role="datepicker" id="<%: this.ID %>_poPlanDate" data-format="dd MMM yyyy" name="poPlanDate" required validationMessage="PO Date Plan Date is required" placeholder="PO Date Plan Date" style="width:180px" /></td>
            </tr>
            <tr>
                <td>On Site Plan Date<span style="color:red">*</span></td>
                <td><input data-role="datepicker" id="<%: this.ID %>_onSitePlanDate" data-format="dd MMM yyyy" name="onSitePlanDate" required validationMessage="On Site Date Plan Date is required" placeholder="On Site Date Plan Date" style="width:180px" /></td>
            </tr>
                 <tr>
                <td>Remark</td>
               <td><textarea placeholder="REMARK" name="remark" onfocus="this.value = this.value;" /></td></textarea>
            </tr>
        </table>
    </div>
</script>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .engMaterialsContainer{
        width: 400px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }
</style>

