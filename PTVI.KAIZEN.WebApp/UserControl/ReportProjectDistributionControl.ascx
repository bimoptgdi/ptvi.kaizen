﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportProjectDistributionControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportProjectDistributionControl" %>
<%@ Register Src="~/UserControl/EmployeeOnProjectSearchControl.ascx" TagPrefix="uc1" TagName="EmployeeOnProjectSearchControl" %>

<script>
    var <%: this.ID %>_chart, <%: this.ID %>_popPickEmployee;
    var <%: this.ID %>_viewReportProjectDistributionModel = kendo.observable({
        dsProjectDistribution: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportProjectDistributionControl"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_ReportProjectDistributionControl"), false);
            },
            schema: {
                model: {
                    fields: {
                        totalProject: { type: "number" }
                    }
                },
                parse: function(e){
                    return e;
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var yearFrom = kendo.toString(<%: this.ID %>_viewReportProjectDistributionModel.valueYearFrom, "dd MMM yyyy");
                        var yearTo = kendo.toString(<%: this.ID %>_viewReportProjectDistributionModel.valueYearTo, "dd MMM yyyy");
                        return _webApiUrl + "dashboard/listProjectDistribution/1?emp=" + <%: this.ID %>_viewReportProjectDistributionModel.valueEmployee.join(";")+"&dateFrom="+ yearFrom + "&dateTo=" + yearTo;
                    },
                    dataType: "json"
                },
                parameterMap: function(data, type){
                }
            },
            //group: [
            //    { field: "roleId" },
            //],
            sort: [{ field: "roleId", dir: "asc" }]
        }),
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportProjectDistributionControl"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportProjectDistributionControl"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
        titleChart: "Report Distribution ",
        valueYear: new Date().getFullYear(),
        valueYearFrom: new Date(),
        valueYearTo: new Date(),
        dataEmployee: [],
        valueEmployee: [],
        dataResult: [],
        dataTargetDone: [],
        onPickEmployeeClick: function (e) {
            var that = this;
            EmployeeOnProjectSearch_pickEmployeeOnProjectModel.set("pickEmployee", function (e) {
                that.set("dataEmployee", EmployeeOnProjectSearch_pickEmployeeOnProjectModel.onSelect());
                var empId = [];
                $.each(that.dataEmployee, function (index, value) {
                    empId.push(value.employeeId);
                });
                that.set("valueEmployee", empId);
                <%: this.ID %>_popPickEmployee.close();
            });
            // show popup
            <%: this.ID %>_popPickEmployee.center().open();
        },
        onSearchClick: function () {
            this.dsProjectDistribution.read();
            <%: this.ID %>_chart.options.title.text = this.titleChart + kendo.toString(this.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(this.valueYearTo, "dd MMM yyyy");
            <%: this.ID %>_chart.refresh();
            <%: this.ID %>_chart.refresh();
        },
        onResetClick: function () {
            this.set("valueYear", new Date().getFullYear());
            this.set("valueEmployee", []);
        }

    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ReportProjectDistributionControl"), <%: this.ID %>_viewReportProjectDistributionModel);
        <%: this.ID %>_viewReportProjectDistributionModel.dsProjectDistribution.read();
        $("#<%: this.ID %>_projectDistribution").kendoChart({
            //theme: "silver",
            title: {
                text: <%: this.ID %>_viewReportProjectDistributionModel.titleChart, // + < %: this.ID %>_viewReportProjectDistributionModel.valueYear
            },
            legend: {
                position: "right",
                labels: {
                    //template: "#: text == '1: Total Project' ? 'Project Manager: Total Project ' : text == '2: Total Project' ? 'Project Engineer: Total Project' : text == '3: Total Project' ? 'Engineer: Total Project' : text == '4: Total Project' ? 'Designer: Total Project' : "
                    //+ "text == '1: Target Done' ? 'Project Manager - Target Done ' : text == '2: Target Done' ? 'Project Engineer - Target Done' : text == '3: Target Done' ? 'Engineer - Target Done' : text == '4: Target Done' ? 'Designer - Target Done' : "
                    //+ "text == '1: Done' ? 'Project Manager - Done ' : text == '2: Done' ? 'Project Engineer - Done' : text == '3: Done' ? 'Engineer - Done' : 'Designer - Done'#"
                }
            },
            dataSource: <%: this.ID %>_viewReportProjectDistributionModel.dsProjectDistribution,
            transitions: false,
            seriesColors: ["#943432", "#6D8B2E", "#376092", "#FFD455", "#376011", "#FFD411"],
            series: [
            //    {
            //    name: "Total Project",
            //    type: "column",
            //    stack: "true",
            //    field: "totalProject",
            //    labels: { visible: true },
            //},
            {
                name: "Project Manager",
                type: "column",
                stack: "true",
                field: "PM",
                labels: { visible: false },
            },
            {
                name: "Project Engineer",
                type: "column",
                stack: "true",
                field: "PE",
                labels: { visible: false },
            },
            {
                name: "Engineer",
                type: "column",
                stack: "true",
                field: "En",
                labels: { visible: false },
            },
            {
                name: "Designer",
                type: "column",
                stack: "true",
                field: "De",
                labels: { visible: false },
            },
            {
                name: "Target Done",
                type: "line",
                field: "targetDone",
                labels: { visible: false },

            },
            {
                name: "Done",
                type: "line",
                field: "done",
                labels: { visible: false },
            },
            ],
            categoryAxis: {
                labels: {rotation: -45},
                field: "employeeName"
            },
            tooltip: {
                visible: true,
                background: "green"
            },
        });

        <%: this.ID %>_popPickEmployee = $("#<%: this.ID %>_popPickEmployee").kendoWindow({
            title: "Operation List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");


        <%: this.ID %>_chart = $("#<%: this.ID %>_projectDistribution").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_ReportProjectDistributionControl">
    <div id="<%: this.ID %>_popPickEmployee">
       <uc1:EmployeeOnProjectSearchControl runat="server" ID="EmployeeOnProjectSearch" />
    </div>
    <fieldset>
        <legend>Search Criteria</legend>
        <table>
            <tr>
                <td>
                    Report Distribution Year
                </td>
                <td>
                    :
                </td>
                <td>
<%--                        <input data-role="combobox"
                            data-placeholder="Select Year"                            
                            data-text-field="year"
                            data-value-field="year"
                            data-bind="value: valueYear, source: dsYearProject"
                            style="width: 250px" /> - --%>
                            <input data-role="datepicker"
                            data-placeholder="From"                            
                            data-bind="value: valueYearFrom"
                                data-format="dd MMMM yyyy" 
                            style="width: 250px" />
                            - <input data-role="datepicker"
                            data-placeholder="To"                            
                            data-bind="value: valueYearTo"
                                data-format="dd MMMM yyyy" 
                            style="width: 250px" />
                </td>
            </tr>
                    <tr>
                <td>
                    Employee
                </td>
                <td>
                    :
                </td>
                <td><div style="width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_multiSelectEmployee" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Clik button to select Employee"
                        data-value-primitive="true"
                        data-text-field="employeeName"
                        data-value-field="employeeId"
                        data-readonly="true"
                        data-bind="value: valueEmployee, source: dataEmployee"></select></div>
                    <div data-role="button"
                        data-bind="events: { click: onPickEmployeeClick }"
                        style="display: inline-block;vertical-align: middle;"
                        class="k-button k-primary">
                        ...
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3"><br />
                    <div class="divCell" style="text-align:right">
                        <div
                            data-role="button"
                            data-bind="events: { click: onSearchClick }"
                            class="k-button k-primary">
                            <span class="k-icon k-i-filter"></span>
                            Search
                        </div>
                        <div
                            data-role="button"
                            data-bind="events: { click: onResetClick }"
                            class="k-button">
                            <span class="k-icon k-i-cancel"></span>
                            Reset
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset><br />
    <fieldset>
        <legend>Search Result</legend>
        <div id="<%: this.ID %>_projectDistribution" style="width: 100%; height: 400px;"></div>
    </fieldset>
</div>

