﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PickVendorsControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.PickVendorsControl" %>

<div id="<%: this.ID %>_PickVendorContent">
    <div>
        <div class="k-content">
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend">PLEASE SELECT VENDOR CODE</legend>
                <div>
                    <label class="labelForm">Vendor Code/Name</label>
                    <input id="<%: this.ID %>_vendorSearch" class="k-textbox" type="text" required 
                        placeholder="Input Vendor Code/Name here (min 4 characters)" style="width: 350px;"
                        data-bind="value: vendorCode, disabled: <%: this.ID %>_isContractorSearchDisabled" />
                    <div
                        data-role="button"
                        data-bind="events: { click: onSearchVendorClick }"
                        class="k-button" >
                        <span class="k-icon k-i-search"></span>
                    </div>
                </div>
            </fieldset>
        </div>
        <br/>
        <div id="<%: this.ID %>_VendorGrid" 
            data-role="grid"
            data-bind="source: ds_vendor_list, events: { change: onPickChange }",
            data-pageable="true",
            data-selectable="true",
            data-height="400",
            data-filterable="extra: false, operators: { string: { contains: 'Contains', startswith: 'Starts With' }",
            data-columns='[
                { field: "vendor_Code", title: "Code", width: "140px" },
                { field: "vendor_Name", title: "Name" },
                {
                    command: [
                        { text: "Select", click: <%: this.ID %>_pickVendorViewModel.pickHandlerVendor }
                    ], title: "&nbsp;", width: "100px"
                }
            ]'

        ></div>
        <input id="<%: this.ID %>_txtIdSelected" class="k-textbox" style="width: 100%;" type="hidden"/>
    </div>
    <div style="float:right;padding-top:20px;">
        <div data-role="button" class="k-button k-primary" data-bind="events: { click: onCloseSelectEvent }">Close</div>
    </div>
   
    <div style="clear:right;"></div>
    

</div>

<script>
    var <%: this.ID %>_pickVendorViewModel = kendo.observable({
        vendorCode: "",
        <%: this.ID %>_isContractorSearchDisabled: false,
        selectedVendor: {},
        listVendor: [],
        setListVendor: function(param) {
            this.set("listVendor", param);
        },
    
        ds_vendor_list: new kendo.data.DataSource({
           transport: {
               read: function(options) {
                   var lstEmp = <%: this.ID %>_pickVendorViewModel.get("listVendor");
                   options.success(lstEmp);
                }
            },
            pageSize: 20,
            schema: {
                model: {
                    id: "id"
                }
            }
        
        }),
        getListVendor: function() {
            
            return this.get("listVendor");
            
        },
        onPickChange: function (e) {
            this.set("selectedVendor", e.sender.dataItem(e.sender.select()));
        },
        onSelectEvent: function (e) {
            alert("");
        },
        
        onCloseSelectEvent: function (e) {

        },
        pickHandlerVendor: function (e) {
            
        },
        onSearchVendorClick: function (e) {
        
            var searchVendorCode = this.get("vendorCode");
            if (searchVendorCode && searchVendorCode.length >= 4) {

                var empApiUrl = "";
                var filter = "?$filter=substringof('"+ searchVendorCode +"',vendor_Code) or substringof(tolower('"+ searchVendorCode +"'),tolower(vendor_Name))";
                kendo.ui.progress($("#<%: this.ID %>_VendorGrid"), true);
                $.ajax({
                    url: _webOdataUrl + "VENDORs" + filter,
                    type: "GET",
                    success: function(data) {
                        //<%: this.ID %>_pickVendorViewModel.setListVendor([ { VENDORID: 6, FULL_NAME: "Heru" } ]);
                        <%: this.ID %>_pickVendorViewModel.setListVendor(data.value);
                        <%: this.ID %>_pickVendorViewModel.ds_vendor_list.read();

                    },
                    error: function (data) {

                        _showDialogMessage("Error", data, "");
                            
                    },
                    complete: function(jqXhr, textStatus) {
                        kendo.ui.progress($("#<%: this.ID %>_VendorGrid"), false);
                    }
                });
                
            } else {
                _showDialogMessage("Warning", "Please provide the vendor code with 4 characters minimum!", "");
            }
        
        }
    }); // View Model
        
    function <%: this.ID %>_DataBind() {
       
        kendo.bind($("#<%: this.ID %>_PickVendorContent"), <%: this.ID %>_pickVendorViewModel);
        
    }
    $(document).ready(function () {
        
       
    }); //doc ready
</script>