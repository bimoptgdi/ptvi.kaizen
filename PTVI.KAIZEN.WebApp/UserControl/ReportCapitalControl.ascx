﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportCapitalControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportCapitalControl" %>
<script>
    var <%: this.ID %>_chart;
    var <%: this.ID %>_reportCapitalProjectModel = kendo.observable({
        dsProjectDistribution: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportCapitalProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportCapitalProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        area: {type: "string"},
                        totalProject: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
//                        var year = < %: this.ID %>_reportCapitalProjectModel.valueYear ? < %: this.ID %>_reportCapitalProjectModel.valueYear : 0;
                        var yearFrom = kendo.toString(<%: this.ID %>_reportCapitalProjectModel.valueYearFrom, "dd MMM yyyy");
                        var yearTo = kendo.toString(<%: this.ID %>_reportCapitalProjectModel.valueYearTo, "dd MMM yyyy");
                        var area = <%: this.ID %>_reportCapitalProjectModel.area ? <%: this.ID %>_reportCapitalProjectModel.area : 0;
                        return _webApiUrl + "project/listCapitalProject/1?area=" + area + "&dateFrom=" + yearFrom + "&dateTo=" + yearTo;
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "area", dir: "asc" }]
        }),
        onChange: function() {
            this.dsProjectDistribution.read();
            <%: this.ID %>_chart.options.title.text = <%: this.ID %>_reportCapitalProjectModel.titleChart + "<br/>"
                + kendo.toString(this.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(this.valueYearTo, "dd MMM yyyy");
            <%: this.ID %>_chart.refresh();
        },
        titleChart: "Report Capital Project",
        valueYear: new Date().getFullYear(),
        valueYearFrom: new Date(),
        valueYearTo: new Date(),
        area: null,
    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ReportCapitalProject"), <%: this.ID %>_reportCapitalProjectModel);
        $("#<%: this.ID %>_projectCapital").kendoChart({
            title: {
                text: <%: this.ID %>_reportCapitalProjectModel.titleChart
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_reportCapitalProjectModel.dsProjectDistribution,
            transitions: false,
            seriesColors: ["#09094c", "Red", "Green", "Blue"],
            series: [{
                name: "Actual Done",
                type: "column",
                stack: "true",
                field: "actualDone",
            }, {
                name: "On Going",
                type: "column",
                stack: "true",
                field: "ongoing",
            }, {
                name: "Backlog",
                type: "column",
                stack: "true",
                field: "backLog",
            }, {
                name: "Plan Finish",
                type: "line",
                stack: "true",
                field: "target",
            }
            ],
            categoryAxis: {
                field: "area",
                labels: {
                    rotation: -45,
                }
            },
            tooltip: {
                visible: true,
            }
        });
        <%: this.ID %>_chart = $("#<%: this.ID %>_projectCapital").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_ReportCapitalProject">
    <div id="<%: this.ID %>_projectCapital" style="width: 500px; height: 400px;"></div>
</div>

