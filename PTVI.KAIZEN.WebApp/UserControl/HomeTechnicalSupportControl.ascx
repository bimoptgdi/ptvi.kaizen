﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeTechnicalSupportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.HomeTechnicalSupportControl" %>
<%@ Register Src="~/UserControl/AssignEngineerDesignerTechnicalSupportControl.ascx" TagPrefix="uc1" TagName="AssignEngineerDesignerControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>

<script type="text/x-kendo-template" id="<%: this.ID %>_selectedArea">
         
</script>
<script>
    var <%: this.ID %>_popAssignEngDes, <%: this.ID %>_popRequestBy; 
    var editModel;
    var dataItem,
        cancelDeselect = 0 ;
    var <%: this.ID %>_vm = kendo.observable({
        areaSelect: function (e) {
            editModel.dirty = true;
        },
        areaDeselect: function (e) {
            cancelDeselect = 0;
            if (this.SelectedArea.length <= 1) {
                dataItem = e.dataItem;
                alert("Area must not empty");
                cancelDeselect = 1;
            } else 
            editModel.dirty = true;
        },
        areaChange: function (e) {
            if(cancelDeselect==1)
                this.SelectedArea.push(dataItem);
        },
        SelectedArea: [],
        areaBound: function (e) {
            var areasjoin = [];
            for (var i = 0; i < editModel.technicalSupportAreas.length; i++) {
                var areainvolvedid = editModel.technicalSupportAreas[i].areaInvolvedId;
                var getval = this.dsAreaInvolved.get(areainvolvedid);
                areasjoin.push(getval);
            }
            this.set("SelectedArea", []);
            this.set("SelectedArea", areasjoin);
        },
        dsAreaInvolved: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "areaInvolveds/areaInvolvedInActive/TS"
            },
            serverFiltering: false,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: {},
                        areaCode: {},
                        areaName: {}
                    }
                }
            }
        }),
    });

    //var  < %: this.ID %>_selectedArea = areas;
    var <%: this.ID %>_viewModel = kendo.observable({
        selectedTabIndex: 0,


        dsListTechnical: new kendo.data.DataSource({
            pageSize: 10,
            requestStart: function () {
                kendo.ui.progress($("#grdListProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        requestDate: { type: "date" },
                        status: { type: "string" },
                        tsNo: { type: "string" },
                        tsDescription: { type: "string" },
                        requestDate: { type: "date" },
                        employeeId: { type: "string" },
                        sharedFolder: { type: "string" },
                        //requestBy: { type: "string" },
                        employeeName: { type: "string" },
                        employeeEmail: { type: "string" },
                        employeePosition: { type: "string" },
                        areaInvolvedId: { type: "string" },
                        //areaInvolvedAreaCode : { type: "string" }
                        engineerListText: { type: "string" },
                        designerListText: { type: "string" }
                    },
                    statusTechnical: function () {
                        //return common_getStatusIpromDesc(this.status);
                        return this.status;
                    }
                },
                parse: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        var engineerList = "";
                        var designerList = "";
                        if (response[i] !== undefined) {
                            for (var j = 0; j < response[i].engineerLists.length; j++) {
                                if (response[i].engineerLists[j] !== undefined)
                                engineerList = engineerList + response[i].engineerLists[j].employeeName+";";
                            }
                            for (var j = 0; j < response[i].designerLists.length; j++) {
                                if (response[i].designerLists[j] !== undefined)
                                    designerList = designerList + response[i].designerLists[j].employeeName + ";";
                            }
                        }
                        response[i].engineerListText = engineerList;
                        response[i].designerListText = designerList;

                    }
                    return response;
                }
            },

            transport: {
                read: {
                    url: function (data) {
                        <% if (IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.InputTechnicalSupport)) { %>
                        return _webApiUrl + "TechnicalSupport/listTechnicalSupport/" + _currBadgeNo + "?loptions=lOpen";
                        <% } else { %>
                        return _webApiUrl + "TechnicalSupport/listTechnicalSupport/1?loptions=lOpen";
                        <% } %>
                    },
                    dataType: "json"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "TechnicalSupport/updateTechnicalSupport/" + data.id;
                    },
                    type: "PUT"
                },
                parameterMap: function (data, type) {
                    if (type == "update") {
                        //data.technicalSupportAreas = [{}];

                        data.technicalSupportAreas = [];
                        var areasjoin = <%: this.ID %>_vm.SelectedArea;
                        for (var i = 0; i < areasjoin.length; i++) {
                            //if (data.technicalSupportAreas[i]) data.technicalSupportAreas[i].areaInvolvedId = areasjoin[i].id;
                            //else
                            data.technicalSupportAreas.push({ id: 0, areaInvolvedId: areasjoin[i].id });
                        }

                        data.userAssignments[0].employeeId = <%: this.ID %>_viewModel.userAssignments.employeeId;
                        data.userAssignments[0].employeeName = <%: this.ID %>_viewModel.userAssignments.employeeName;
                        data.userAssignments[0].employeeEmail = <%: this.ID %>_viewModel.userAssignments.employeeEmail;
                        data.userAssignments[0].employeePosition = <%: this.ID %>_viewModel.userAssignments.employeePosition;
                        data.userAssignments[0].project = [];
                        data.userAssignments[0].technicalsupport = [];
                        data.requestDate = kendo.toString(data.requestDate, 'yyyy-MM-dd');
                    }
                    return data;
                }
            },

//            sort: [{ field: "tsNo", dir: "asc" }]
        }),
        SelectedArea: [],
        userAssignments: [{}],

        detailInit: function (e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    pageSize: 10,
                    requestStart: function () {
                        kendo.ui.progress($("#grdListProject"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("#grdListProject"), false);
                    },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                employeeName: { editable: false },
                                engineerDesigner: { editable: false },
                                completedDate: { type: "date", validation: { required: false, max: new Date() } },
                                employeePosition: { type: "string" },
                                completePercentage: { type: "number", validation: { required: false, min: 0, max: 100 } }
                                //employeeStatus: { editable: false }
                            },
                            positionName: function () {
                                return common_getPositionByPositionID(this.employeePosition);
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: function (data) {
                                <% if (IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.InputTechnicalSupport)) { %>
                                return _webApiUrl + "TechnicalSupport/listEngineerDesignerByTechnicalSupportId/" + e.data.id + "?badgeNo=" + _currBadgeNo;
                                <% } else { %>
                                return _webApiUrl + "TechnicalSupport/listEngineerDesignerByTechnicalSupportId/" + e.data.id + "?badgeNo=1";
                                <% }  %>
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                return _webApiUrl + "TechnicalSupport/updateTechnicalSupportInteraction/" + data.id;
                            },
                            type: "PUT"
                        },
                        parameterMap: function (data, type) {
                            if (type == "create" || type == "update") {
                                data.completedDate = kendo.toString(data.completedDate, 'yyyy-MM-dd');

                            }
                            return data;
                        }
                    },
                    change: function (e) {
                        if (e.action == "sync") {
                            $("#<%: this.ID %>_grdListTechnicalSupport").getKendoGrid().dataSource.read();
                        }
                    }
                },
                scrollable: false,
                sortable: true,
                pageable: true,
                columns: [
                    { field: "employeeName", title: "Name", width: 70 },
                    { field: "engineerDesigner", title: "Engineer/Designer", width: 50 },
                    { field: "actionDescription", title: "Action Having Done/On Going", width: 150, template: '#= actionDescription ? actionDescription : \'-\' #' },
                    { field: "result", title: "Result", width: 50, template: '#= result ? result : \'-\' #' },
                    { field: "completePercentage", title: "%Complete", width: 30, template: '#: completePercentage ? completePercentage + \' %\' : \'-\'#' },
                    { field: "completedDate", title: "Complete Date", width: 50, template: '#= completedDate ? kendo.toString(completedDate,\'dd MMM yyyy\') : \'-\' #' },
                    {
                        command: [{
                            name: 'edit',
                            visible: function (e) {
                                if ((e.employeeId == _currBadgeNo) || (_currRole == "ADM"))
                                    return true;
                                else
                                    return false;
                            }
                        }],
                        width: 40
                    }
                ],
                editable: "inline"
            });
        },
        editTechnical: function (e) {
            var that = this;
            editModel = e.model;
            $("#<%: this.ID %>_employeeTmp").data("kendoAutoComplete").value("");

            var autocomplete = $("#<%: this.ID %>_employeeTmp").data("kendoAutoComplete");
            autocomplete.bind("change", function () {
                editModel.dirty = true;

                var that = <%: this.ID %>_viewModel;
                if (that.employeeSelected) {
                    if (!that.employeeSelected.employeeId) {
                        that.set("employeeSelected", null);
                    }
                }
                that.set("userAssignments.employeeId", that.employeeSelected ? that.employeeSelected.employeeId : null);
                that.set("userAssignments.employeeName", that.employeeSelected ? that.employeeSelected.full_Name : null);
                that.set("userAssignments.employeeEmail", that.employeeSelected ? that.employeeSelected.email : null);
                that.set("userAssignments.employeePosition", that.employeeSelected ? that.employeeSelected.positionId : null);

                $("#<%: this.ID %>_employeeId").val(that.employeeSelected ? that.employeeSelected.employeeId : null);
                $("#<%: this.ID %>_employeeName").val(that.employeeSelected ? that.employeeSelected.full_Name : null);
                $("#<%: this.ID %>_employeeEmail").val(that.employeeSelected ? that.employeeSelected.email : null);
                $("#<%: this.ID %>_employeePosition").val(that.employeeSelected ? that.employeeSelected.positionId : null);
            });

            <%--$("#<%: this.ID %>_btnRequestBySelectedName").on("click", function () {
                ListTechnical_PickEmployee_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                    var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                    $("#<%: this.ID %>_employeeId").val(pickedEmployee.id);
                    $("#<%: this.ID %>_employeePosition").val(pickedEmployee.positionId);
                    $("#<%: this.ID %>_employeeEmail").val(pickedEmployee.email);
                    $("#<%: this.ID %>_employeeName").val(pickedEmployee.full_Name);

                    <%: this.ID %>_viewModel.set("userAssignments.employeeId", pickedEmployee.id);
                    <%: this.ID %>_viewModel.set("userAssignments.employeeName", pickedEmployee.full_Name);
                    <%: this.ID %>_viewModel.set("userAssignments.employeeEmail", pickedEmployee.email);
                    <%: this.ID %>_viewModel.set("userAssignments.employeePosition", pickedEmployee.positionId);

                    editModel.dirty = true;
                    <%: this.ID %>_popRequestBy.close();
                });
                <%--ListTechnical_PickEmployee_pickVendorViewModel.set("onCloseSelectEvent", function (data) {
                    <%: this.ID %>_popRequestBy.close();
                });-- %>
                // show popup
                <%: this.ID %>_popRequestBy.center().open();
                ListTechnical_PickEmployee_DataBind();
            });--%>


            kendo.bind($("#areaInvolvedId"), <%: this.ID %>_vm);

            $("#<%: this.ID %>_employeeId").val(e.model.userAssignments[0].employeeId);
            $("#<%: this.ID %>_employeePosition").val(e.model.userAssignments[0].employeePosition);
            $("#<%: this.ID %>_employeeEmail").val(e.model.userAssignments[0].employeeEmail);
            $("#<%: this.ID %>_employeeName").val(e.model.userAssignments[0].employeeName);
            <%: this.ID %>_viewModel.set("userAssignments.employeeId", e.model.userAssignments[0].employeeId);
            <%: this.ID %>_viewModel.set("userAssignments.employeeName", e.model.userAssignments[0].employeeName);
            <%: this.ID %>_viewModel.set("userAssignments.employeeEmail", e.model.userAssignments[0].employeeEmail);
            <%: this.ID %>_viewModel.set("userAssignments.employeePosition", e.model.userAssignments[0].employeePosition);



            that.set("employeeSelected", {
                employeeId: e.model.userAssignments[0].employeeId,
                full_Name: e.model.userAssignments[0].employeeName,
                email: e.model.userAssignments[0].employeeEmail,
                positionId: e.model.userAssignments[0].employeePosition
            });

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(800).data("kendoWindow").center();
                $(".k-edit-buttons").width(780);
            }, 100);
        },

        dsStatusSource: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "lookup/findbytype/" + _statusIpromTS
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        }),

        openAssignEngineerDesigner: function (e) {
            AssignEngineerDesigner_technicalSupportId = e.data.id;
            kendo.bind($("#AssignEngineerDesigner_AssignEngineerDesignerContent"), AssignEngineerDesigner_viewAssignEngineerDesignerModel);
            AssignEngineerDesigner_viewAssignEngineerDesignerModel.dsTender();
            AssignEngineerDesigner_engineerSource.read();
            AssignEngineerDesigner_designerSource.read();
            <%: this.ID %>_popAssignEngDes.center().open();
            kendo.bind($("#AssignEngineerDesigner_AssignEngineerDesignerContent"), AssignEngineerDesigner_viewModel);
            AssignEngineerDesigner_viewModel.set("tsNo", e.data.tsNo);
            AssignEngineerDesigner_viewModel.set("tsDesc", e.data.tsDesc);

        },

        getEmployeeFromServer: function (options) {
            var searchBadgeNo = options.data.filter.filters[0].value;
            var result = [];
            if (searchBadgeNo.length >= 3) {
                //var filter = "?$filter=substringof('" + searchBadgeNo + "',employeeId) or substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var filter = "?$filter=substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var result = [];
                $.ajax({
                    url: _webOdataUrl + "EMPLOYEEs" + filter,
                    type: "GET",
                    async: false,
                    success: function (data) {
                        result = data.value;
                    },
                    error: function (data) {
                        //return [];
                    }
                });
            }
            return result;
        },
        dsEmployee: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                    options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        employeeSelected: null
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_HomeMyProjectControl"), <%: this.ID %>_viewModel);
        <%: this.ID %>_viewModel.dsListTechnical.read();
        <%: this.ID %>_popAssignEngDes = $("#<%: this.ID %>_popAssignEngDes").kendoWindow({
            title: "Assign Engineer/Designer",
            modal: true,
            visible: false,
            resizable: true,
            width: 950,
            activate: function () {
            },
            close: function () {
                $("#<%: this.ID %>_grdListTechnicalSupport").getKendoGrid().dataSource.read();
            }
        }).data("kendoWindow");

        <%: this.ID %>_popRequestBy = $("#<%: this.ID %>_popRequestBy").kendoWindow({
            title: "Pick Employee",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        $("#lOpen").addClass("k-state-active");
        $("[name='stGroupBtn']").on("click", function (e) {
            selectGroupBtn(e.currentTarget.id);
        })
    });
    function selectGroupBtn(btnId) {
        $("[name='stGroupBtn']").removeClass("k-state-active");
        $("#" + btnId).addClass("k-state-active");
        <%: this.ID %>_viewModel.dsListTechnical.options.transport.read.url = function () {
                        <% if (IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.InputTechnicalSupport)) { %>
                        return _webApiUrl + "TechnicalSupport/listTechnicalSupport/" + _currBadgeNo;
                        <% } else { %>
                        return _webApiUrl + "TechnicalSupport/listTechnicalSupport/1?loptions="+btnId;
                        <% } %>
        }
        <%: this.ID %>_viewModel.dsListTechnical.query({
            page: 1,
            pageSize: 10
        });
        <%: this.ID %>_viewModel.dsListTechnical.read().then(function () {
            $("#<%: this.ID %>_grdListTechnicalSupport").getKendoGrid().refresh();
        });
    }
</script>


<div id="<%: this.ID %>_HomeMyProjectControl">
    <div id="<%: this.ID %>_popAssignEngDes">
        <uc1:AssignEngineerDesignerControl runat="server" ID="AssignEngineerDesigner" />
    </div>
    <div id="<%: this.ID %>_popRequestBy">
        <uc1:PickEmployeesControl runat="server" id="ListTechnical_PickEmployee" />
    </div>
    
    <div id="<%: this.ID %>_grdListTechnicalSupport"
        data-role="grid"
        data-scrollable="true"
        data-sortable="true"
        data-columns="[
                { 
                    field: 'tsNo', title: 'Description', 
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_technicalSupportDetail-template').html()),
                    width: 200
                },
                { 
                    field: 'requestDate', title: 'Request', 
                    attributes: { style: 'vertical-align: top !important;' },
                    template: kendo.template($('#<%: this.ID %>_technicalSupportRequest-template').html()),
                    width: 100
                },
                {
                    field: 'requestDate', title: 'Date Request',
                    format: '{0:dd MMM yyyy}',
                    width: 40, hidden: true
                },                                
                { 
                    field: 'engineerListText', title: 'Engineer' ,
                    template: kendo.template($('#<%: this.ID %>_engineer-template').html()), 
                    width: 80
                }, 
                { 
                    field: 'designerListText', title: 'Designer',
                    template: kendo.template($('#<%: this.ID %>_designer-template').html()),
                    width: 80
                }, 
                { 
                    field: '', title: 'Request By',
                    template: kendo.template($('#<%: this.ID %>_requestby-template').html()), 
                    width: 80, hidden: true
                },
                { 
                    field: 'technicalSupportAreas', title: 'Area Involve',
                    template: kendo.template($('#<%: this.ID %>_areainvolved-template').html()), 
                    width: 150
                },                              
                                       
                { 
                    field: 'problemStatement', title: 'Problem Statement', 
                    width: 100
                },                               
                { 
                    field: 'status', title: 'Status',
                    template: '#=data.status ? common_getStatusIpromDesc(data.status):\'-\' #',
                    width: 70
                },
                { 
                    field: '', title: '',
                    template: kendo.template($('#<%: this.ID %>_assignBtn-template').html()),
                    width: 70
                }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.InputTechnicalSupport)){ %>,
                {
                command: ['edit'],
                width: 70
                }<% } %>
                
            ]"
        data-bind="source: dsListTechnical, events: { edit: editTechnical }"
        data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_grdTechnicalSupportEdit").html())}'
        data-detail-init="<%: this.ID %>_viewModel.detailInit"
        data-pageable="{ buttonCount: 5 }"
        data-filterable="true"
        data-toolbar='[{"template": kendo.template($("#<%: this.ID %>_StatusToggle").html())}]'>

    </div>
</div>
<script id="<%: this.ID %>_StatusToggle" type="text/x-kendo-template">
    <div id='lOpen' name='stGroupBtn' class='k-button'>
        List Open 
    </div>
    <div id='lCompleted' name='stGroupBtn' class='k-button'>
        List Completed 
    </div>
    <div id='lAll' name='stGroupBtn' class='k-button'>
        List All
    </div>
</script>
<script id="<%: this.ID %>_technicalSupportDetail-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        No:
    </div>
    <div>
        #= data.tsNo #
    </div>
    <div class='subColumnTitle'>
        Description:
    </div>
    <div> 
        #= data.tsDesc # 
    </div>
    #if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {#
        <div> 
            #if (data.sharedFolder) {#
                <a href="file://#= data.sharedFolder#"  target="_explorer.exe">Share Folder</a>
            #}#
        </div>
    #} else { #
        <div> 
            #if (data.sharedFolder) {#
                <div class='subColumnTitle'>
                        Share Folder
                </div>
                <div>
                    #= data.sharedFolder#
                </div>
            #}#
        </div>
    #} #
</script>
<script id="<%: this.ID %>_technicalSupportRequest-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Date:
    </div>
    <div>
        #= data.requestDate ? kendo.toString(data.requestDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        By:
    </div>
    <div>
        # for (var i = 0; i < userAssignments.length; i++) { #
            #= userAssignments[i].employeeName#
        # } #
    </div>
</script>
<script id="<%: this.ID %>_grdTechnicalSupportEdit" type="text/x-kendo-template">
    <div style="padding:10px">  
        <table style="width: 690px">
            <tr>
                <td style="width:170px">No</td>
                <td><input class="k-textbox" type="text" id="tsNo" name="tsNo"  placeholder="Technical Support No" readonly /></td>
            </tr>
            <tr>
                <td style="width:170px">Description</td>
                <td><input class="k-textbox" type="text" id="tsDesc" name="tsDesc" style="width: 520px;"  placeholder="Technical Support Description" required  /></td>
            </tr>
            <tr>
                <td style="width:170px">Share Folder</td>
                <td><input class="k-textbox" type="text" id="<%: this.ID %>_dtSharedFolder" name="Shared Folder" type="text" style="width:300px" data-bind="value: sharedFolder" /></td>
            </tr>
            <tr>
                <td style="width:170px">Request Date</td>
                <td><input type="date" <%--pattern="\d{1,2}/\d{1,2}/\d{4}" required="required"--%> data-role="datepicker"  data-format="dd MMM yyyy" id="<%: this.ID %>_requestDate" name="requestDate" required validationMessage="Request Date is required" placeholder="Please Input Request Date" style="width:180px" /></td>
            </tr>
            <tr>
                <td style="width:100px">Request By<span style="color:red">*</span></td>
                <td>
                    <input style="display:none;" disabled="disabled" id="<%: this.ID %>_employeeId" name="employeeId" class="k-textbox"  type="text" />
                    <input style="display:none;" disabled="disabled" id="<%: this.ID %>_employeeEmail" name="employeeEmail" class="k-textbox" " type="text" />
                    <input style="display:none;" disabled="disabled" id="<%: this.ID %>_employeePosition" name="employeePosition" class="k-textbox" " type="text" />
                    <input style="display:none;" disabled="disabled" id="<%: this.ID %>_employeeName" name="employeePosition" class="k-textbox" " type="text" />
                    <input id="<%: this.ID %>_employeeTmp" name="requester" data-role="autocomplete" 
                        data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                        placeholder="Name here (min 3 characters)" required validationMessage="Request by is required"
                        data-bind="source: dsEmployee, value: employeeSelected" />

                   <%-- <input id="<%: this.ID %>_employeeName" style="width: 300px" name="requestBy" class="k-textbox" type="text" disabled="disabled" placeholder="Vendor" required validationMessage="Vendor is required" />
                    <div
                        id="<%: this.ID %>_btnRequestBySelectedName" 
                        class="k-button">
                        ...
                    </div></td>--%>
            </tr>
            <tr>
            <td style="width:100px">Area Involved</td>
            <td>
            <select id="areaInvolvedId" name="areaInvolvedId" data-role="multiselect"
                   data-placeholder="Area Involved"
                   data-text-field="areaDesc"
                   data-value-field="id"
                   data-bind="
                   value: SelectedArea,
                   source: dsAreaInvolved, events:{select: areaSelect, dataBound: areaBound, deselect: areaDeselect, change: areaChange}"
                   style="width: 520px" validationMessage="Area Involved is required"></select>
            </td>
            </tr> 
            <tr>
                <td style="width:170px; vertical-align: top">Problem Statement</td>
                <td>
                    <textarea class="k-textbox" type="text" id="problemStatement" name="problemStatement" style="width: 520px; height: 100px"  placeholder="Problem Statement" required></textarea>
                </td>
            </tr>
       
            <tr>
                <td>Status</td>
                <td><input required="required" data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource, value: status" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
            </tr>   
            
        </table>
    </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_assignBtn-template">
        <div>
            <a href="\#" data-bind="events: { click: openAssignEngineerDesigner }">Assign Eng/Des</a>
            
        </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_engineer-template">
        <div>
            # for (var i = 0; i < engineerLists.length; i++) { #
             #= engineerLists[i].employeeName #;
            # } #

        </div>
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_designer-template">
        <div>
            # for (var i = 0; i < designerLists.length; i++) { #
             #= designerLists[i].employeeName #;
            # } #
        </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_areainvolved-template">
        <div>
            # var strList = ''; #
            # for (var i = 0; i < technicalSupportAreas.length; i++) { #
            # if (strList.indexOf(technicalSupportAreas[i].areaInvolved.areaDesc+';')<0) {#
            # strList = strList + technicalSupportAreas[i].areaInvolved.areaDesc+';'#
            #  } #
            # } #
            #= strList#
        </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_requestby-template">
        <div>
            # for (var i = 0; i < userAssignments.length; i++) { #
             #= userAssignments[i].employeeName#
            # } #
        </div>
</script>


<script type="text/x-kendo-template" id="<%: this.ID %>_completedDate-template">
        <div>
            # for (var i = 0; i < technicalSupportInteraction.length; i++) { #
            #:technicalSupportInteraction[i].completedDate ? kendo.toString(technicalSupportInteraction[i].completedDate,'dd MMM yyyy') : "-"#
            # } #
            
        </div>
    
</script>
<script>
    

</script>


