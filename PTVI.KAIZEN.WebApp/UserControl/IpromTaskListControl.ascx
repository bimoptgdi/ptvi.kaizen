﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IpromTaskListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.IpromTaskListControl" %>
<%@ Register Src="~/UserControl/IpromK2PSDApprovalControl.ascx" TagPrefix="uc1" TagName="IpromK2PSDApprovalControl" %>
<%@ Register Src="~/UserControl/IpromK2BudgetApprovalControl.ascx" TagPrefix="uc1" TagName="IpromK2BudgetApprovalControl" %>


<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>

<div id="<%: this.ID %>_TaskListContent" class="k-content wide">
    <div id="<%: this.ID %>_tabstrip">
        <ul>
            <li class="k-state-active">Need My Response PSD</li>
            <li class="k-state">Need My Project Budget Approval</li>
        </ul>
        <div>
            <iframe id="<%: this.ID %>_K2FramePSD" width="100%" height="350" frameborder="0"></iframe>
        </div>
        <div>
            <iframe id="<%: this.ID %>_K2FrameBudgetApproval" width="100%" height="350" frameborder="0"></iframe>
        </div>
    </div>
    <div id="<%: this.ID %>_popupApproval">
        <uc1:IpromK2PSDApprovalControl runat="server" ID="IpromK2PSDApprovalControl" />
    </div>
        <div id="<%: this.ID %>_popupBudgetApproval">
        <uc1:IpromK2BudgetApprovalControl runat="server" ID="IpromK2BudgetApprovalControl" />
    </div>

</div>
<script>
    var <%: this.ID %>_tabStrip;

    var <%: this.ID %>_viewModel = kendo.observable({
        submitState: "Submitted", //submitted or saved (as Draft)
        k2State: "",
        onSelect: function (e) {
            var text = $(e.item).children(".k-link").text();
        }
    });
    kendo.bind($("#<%: this.ID %>_TaskListContent"), <%: this.ID %>_viewModel);
    // PRODUCTION
    //$("#<%: this.ID %>_K2FramePSD").attr("src", "<%: K2PSDWorkListUrl %>&UserName=<%: NtUserID %>&ViewCallback=onK2ViewClick");
    // DEVELOPMENT
    $("#<%: this.ID %>_K2FramePSD").attr("src", "<%: K2PSDWorkListUrl %>&UserName=wnotifier&ViewCallback=onK2ViewClick");

    //$("#<%: this.ID %>_K2FrameBudgetApproval").attr("src", "<%: K2ProjectBudgetWorkListUrl %>&UserName=<%: NtUserID %>&ViewCallback=onK2ViewClick");
    // DEVELOPMENT
    $("#<%: this.ID %>_K2FrameBudgetApproval").attr("src", "<%: K2ProjectBudgetWorkListUrl %>&UserName=wnotifier&ViewCallback=onK2ViewClick");

    $("#<%: this.ID %>_popupApproval").kendoWindow({
        title: "Approval",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupApproval = $("#<%: this.ID %>_popupApproval").data("kendoWindow");


     $("#<%: this.ID %>_popupBudgetApproval").kendoWindow({
        title: "Approval",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupBudgetApproval = $("#<%: this.ID %>_popupBudgetApproval").data("kendoWindow");

    function onK2ViewClick(k2Data) {
        if (k2Data) {
            var appID = k2Data.instance.AppID;
            //var appID = 5;
            <%: this.ID %>_viewModel.set("k2State", k2Data.eventDisplayName);

            if ($("#<%: this.ID %>_tabstrip").data("kendoTabStrip").select().index() == 0) {
                $.ajax({
                    url: _webApiUrl + "PSDApprovals/" + appID,
                    type: "GET",
                    success: function (data) {
                        if (<%: this.ID %>_viewModel.get("k2State") != "Need Revision") {
                            <%: this.ID %>_onViewPSDApproverClick(data, true, false, "Approval")
                        } else if (<%: this.ID %>_viewModel.get("k2State") == "Need Revision") {
                            <%: this.ID %>_onViewPSDApproverClick(data, false, true, "Need Revision")
                        }
                    },
                    error: function (jqXHR) {
                    }
                });
            } else    if ($("#<%: this.ID %>_tabstrip").data("kendoTabStrip").select().index() == 1) {
                $.ajax({
                    url: _webApiUrl + "ProjectBudgetApproval/GetPROJECTBUDGETAPPROVAL/" + appID,
                    type: "GET",
                    success: function (data) {
                        if (<%: this.ID %>_viewModel.get("k2State") != "Need Revision") {
                            <%: this.ID %>_onViewBudgetApprovalClick(data, true, false, "Approval")
                        } else if (<%: this.ID %>_viewModel.get("k2State") == "Need Revision") {
                            <%: this.ID %>_onViewBudgetApprovalClick(data, false, true, "Need Revision")
                        }
                    },
                    error: function (jqXHR) {
                    }
                });
            }
        }
    }

    function <%: this.ID %>_onViewPSDApproverClick(iPromData, isApproval, isSaveSubmit, titleWindows) {
        $("#<%: this.ID %>_popupApproval").prevUntil("k-window-titlebar").find(".k-window-title").html("Project Scope Definition - " + titleWindows);

        IpromK2PSDApprovalControl_viewModel.set("onCloseEvent", function (e) { <%: this.ID %>_popupApproval.close(); document.getElementById("<%: this.ID %>_K2FramePSD").contentWindow.location.reload(); });
        IpromK2PSDApprovalControl_viewModel.set("psdRequestID", iPromData.id);

        <%: this.ID %>_popupApproval.center().open();
        IpromK2PSDApprovalControl_Init(iPromData, "IpromTaskListForm.aspx", isApproval, isSaveSubmit);
    }

     function <%: this.ID %>_onViewBudgetApprovalClick(iPromData, isApproval, isSaveSubmit, titleWindows) {
        $("#<%: this.ID %>_popupApproval").prevUntil("k-window-titlebar").find(".k-window-title").html("Project Budget Approval - " + titleWindows);

        IpromK2BudgetApprovalControl_viewModel.set("onCloseEvent", function (e) { <%: this.ID %>_popupBudgetApproval.close(); document.getElementById("<%: this.ID %>_K2FrameBudgetApproval").contentWindow.location.reload(); });
         IpromK2BudgetApprovalControl_viewModel.set("budgetApprovalRequestID", iPromData.id);

        <%: this.ID %>_popupBudgetApproval.center().open();
        IpromK2BudgetApprovalControl_Init(iPromData, "IpromTaskListForm.aspx", isApproval, isSaveSubmit);
    }

    // Document Ready
    $(document).ready(function () {
        <%: this.ID %>_tabStrip = $("#<%: this.ID %>_tabstrip").kendoTabStrip().data("kendoTabStrip");
    });
</script>
