﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploaderNewControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.UploaderNewControl" %>

<div id="<%: this.ID %>_window" style="display:none">
    <div id="<%: this.ID %>_label" style="margin:5px"></div>
    <input id="<%: this.ID %>_control" type="file" name="<%: this.ID %>_control" />
</div>
<script>
    var <%: this.ID %>_saveUrl;
    var <%: this.ID %>_multiple;
    var <%: this.ID %>_onUploadCallback;
    var <%: this.ID %>_onSuccessCallback;
    var <%: this.ID %>_onSelectFile;

    function <%: this.ID %>_show(title, label, multiple, saveUrl, onSelectFile, onUploadCallback, onSuccessCallback, onErrorCallback) {
        if ($("#<%: this.ID %>_window").getKendoWindow() == null){
            <%: this.ID %>_saveUrl = saveUrl;
            <%: this.ID %>_multiple = multiple;
            <%: this.ID %>_onUploadCallback = onUploadCallback;
            <%: this.ID %>_onSuccessCallback = onSuccessCallback; 
            <%: this.ID %>_onSelectFile = onSelectFile;

           $("#<%: this.ID %>_label").html(label);   

            $("#<%: this.ID %>_window").kendoWindow({
                title: title,
                activate: function(){
                    if ($("#<%: this.ID %>_control").getKendoUpload() == null){
                        $("#<%: this.ID %>_control").kendoUpload({
                            async:{
                                saveUrl: saveUrl,
                                removeUrl: "remove"
                            },
                            multiple: <%: this.ID %>_multiple,
                            select: <%: this.ID %>_onSelectFile,
                            upload: <%: this.ID %>_onUploadCallback,
                            success: <%: this.ID %>_onSuccessCallback,
                            error: onErrorCallback
                        });
                    }
                    else {
                        // clear history list
                        $("#<%: this.ID %>_window .k-upload-files").find("li").remove(); 
                    }    
                }        

            });

        }

        $("#<%: this.ID %>_window").getKendoWindow().center().open();
    }

    function <%: this.ID %>_close(){
        $("#<%: this.ID %>_window").getKendoWindow().center().close();
    }
</script>
