﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportWorkhoursConsumption.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportWorkhoursConsumption" %>
<script>
    var <%: this.ID %>_chart;
    var <%: this.ID %>_reportWorkhoursCModel = kendo.observable({
        titleChart: "Workhours Consumption",
        valueYear: new Date().getFullYear(),
        area: null,
        dsWorkhoursCons: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        area: {type: "string"},
                        workHours: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var year = <%: this.ID %>_reportWorkhoursCModel.valueYear ? <%: this.ID %>_reportWorkhoursCModel.valueYear : 0;
                        var area = <%: this.ID %>_reportWorkhoursCModel.area;
                        return _webApiUrl + "timesheet/ReportWorkhoursConsumption/" + year + "?area="+ area;
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "area", dir: "asc" }]
        }),
        onChange: function () {
            <%: this.ID %>_reportWorkhoursCModel.valueYear = this.valueYear;
            <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read();
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").options.title.text =
                <%: this.ID %>_reportWorkhoursCModel.titleChart + " " + <%: this.ID %>_reportWorkhoursCModel.valueYear;
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").refresh();
        },
        onChangeArea: function () {
            <%: this.ID %>_reportWorkhoursCModel.area = kendo.stringify(this.area);
            <%: this.ID %>_reportWorkhoursCModel.valueYear = this.valueYear;
            <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons.read();
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").options.title.text =
                <%: this.ID %>_reportWorkhoursCModel.titleChart + " " + <%: this.ID %>_reportWorkhoursCModel.valueYear;
            $("#<%: this.ID %>_workhoursCons").data("kendoChart").refresh();
        },
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
        dsArea: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        areaDesc: { type: "string" },
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "areaInvolveds/areaInvolvedInActive/TS";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
        }),
    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_reportBudgetPerformance"), <%: this.ID %>_reportWorkhoursCModel);
        $("#<%: this.ID %>_workhoursCons").kendoChart({
            title: {
                text: <%: this.ID %>_reportWorkhoursCModel.titleChart + " " + <%: this.ID %>_reportWorkhoursCModel.valueYear
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_reportWorkhoursCModel.dsWorkhoursCons,
            transitions: false,
            seriesColors: ["#943432", "#6D8B2E"],
            series: [{
                name: "Project",
                type: "column",
                stack: "area",
                field: "workHoursProject",
            }, {
                name: "Technical Support",
                type: "column",
                stack: "area",
                field: "workHoursTS",
            }

            ],
            valueAxis: {
                labels: {
                    template: "#= kendo.format('{0:N0}', value) #",
                    rotation: -45
                }
            },
            categoryAxis: {
                field: "area"
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= kendo.format('{0:N0}', value) #"
            }
        });

        <%: this.ID %>_chart = $("#<%: this.ID %>_workhoursCons").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_reportBudgetPerformance">
    <div style="display:inline-block">
    Year: <input data-role="combobox"
            data-placeholder="Select Year"                            
            data-text-field="year"
            data-value-field="year"
            data-bind="value: valueYear, source: dsYearProject,
                        events: {
                            change: onChange
                        }"
            style="width: 250px" />
    </div>
    <div style="display:inline-block;vertical-align:middle">
        <div style="display:inline-block;text-align: left;vertical-align:top; height:30px; width:50px;"> Area:</div>
        <div style="display:inline-block; width:550px; ">
            <select data-role="multiselect"
            data-value-primitive="true"
            data-placeholder="Select Area"                            
            data-text-field="areaDesc"
            data-value-field="id"
            data-bind="value: area, source: dsArea,
                        events: {
                            change: onChangeArea
                        }"
            style="display:inline-block;vertical-align: bottom;width: 500px" ></select>
            </div>
        </div>
    <div id="<%: this.ID %>_workhoursCons" style="width: 1000px; height: 400px;"></div>
</div>
