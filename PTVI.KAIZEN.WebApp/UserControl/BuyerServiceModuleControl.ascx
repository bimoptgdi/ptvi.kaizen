﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyerServiceModuleControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.BuyerServiceControl" %>
<%@ Register Src="~/UserControl/BuyerMaterialListControl.ascx" TagPrefix="uc1" TagName="BuyerMaterialListControl" %>
    <div id="BuyerServiceForm">
        <div class="k-content wide">
            <div>
                <div id="BuyerServiceTabStrip">
                    <ul>
                        <li>Buyer Material List</li>
                    </ul>
                    <div>
                        <uc1:BuyerMaterialListControl runat="server" ID="BuyerMaterialListControl1" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        var <%: this.ID %>_BuyerSeviceVM = kendo.observable({
            selectedTabIndex: 0
        });


            // Tab control
        var BuyerServiceTabStrip = $("#BuyerServiceTabStrip").kendoTabStrip({
                tabPosition: "top",
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
            });
        BuyerServiceTabStrip = $("#BuyerServiceTabStrip").getKendoTabStrip();
        BuyerServiceTabStrip.select(0);

            // --Tab control
        kendo.bind($("#BuyerServiceForm"), <%: this.ID %>_BuyerSeviceVM);
//        OnDocumentReady = function () {
//        }

    </script>
