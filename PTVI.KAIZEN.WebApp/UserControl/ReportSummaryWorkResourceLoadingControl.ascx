﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportSummaryWorkResourceLoadingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportSummaryWorkResourceLoadingControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
   var monthNames = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var <%: this.ID %>_ReportResoucesLoadingGrid = [], <%: this.ID %>_actualCostFormatData = [], currYear = new Date().getFullYear();
    var <%: this.ID %>_ResoucesLoadingReportModel = kendo.observable ({
        paramYear: currYear,
        listYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            }
        }),
        onChangeParamYear: function (e) {
            this.dsGetMonthWeekOfYear()
            this.generateGriReportResoucesLoading();
        },
        dsResoucesLoadingReport: new kendo.data.DataSource({
            requestStart: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), true);
                kendo.ui.progress($("#ReportResoucesLoadingForm"), true);
            },
            requestEnd: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), false);
                kendo.ui.progress($("#ReportResoucesLoadingForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "Report/SummaryWorkResourcesLoadingReport/" +  <%: this.ID %>_ResoucesLoadingReportModel.paramYear;
                    },
                    async: false
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dataGetMonthWeekOfYear: {},
        dsGetMonthWeekOfYear: function () {
            $.ajax({
                url: "<%: WebApiUrl %>" + "report/GetMonthWeekOfYear/" + <%: this.ID %>_ResoucesLoadingReportModel.paramYear,
                type: "GET",
                async: false,
                success: function (data) {
                    <%: this.ID %>_ResoucesLoadingReportModel.set("dataGetMonthWeekOfYear", data);
                },
                error: function (error) {
                }
            });
        },
        onDataBound: function (e) {
            var data = e.sender._data;
            $.each(data, function (i, row) {
                if (row.level == 0) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#C00000");
                } else if (row.level == 1) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#1F497D");
                } else if (row.level == 2) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#E26B0A");
                }
            });
        },
        dsResoucesLoadingReportReformat: function () {
            var dataReformat = [];

            this.dsResoucesLoadingReport.read();

            var dataOri = this.dsResoucesLoadingReport.data();
            
            //generateColumnGrid
            <%: this.ID %>_ReportResoucesLoadingGrid = [];
            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "pic", title: "Engineer / Designer", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 250,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "typeGroupName", title: " ",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });


            //loop grid column dsGetMonthWeekOfYear
            if (this.dataGetMonthWeekOfYear.totalWeekOfYear > 0) {
                var weekly = 0;
                for (var j = 0; j < this.dataGetMonthWeekOfYear.listTotalWeekOfMonths.length; j++) {
                    columnComplexity = [];
                    for (var k = 0; k < this.dataGetMonthWeekOfYear.listTotalWeekOfMonths[j].totalWeek; k++) {
                        //console.log(weekly.toString());
                        columnComplexity.push({
                            field: "ResoucesLoading_" + (++weekly),
                            title: kendo.toString(weekly, "n0"),
                            template: "#=(data.ResoucesLoading_" + weekly + " != null ? kendo.toString(data.ResoucesLoading_" + weekly + ", 'n2') : '')#",
                            headerAttributes: {
                                "class": "table-header-cell",
                                style: "text-align: center;"
                            }, width: 50,
                            attributes: { style: "text-align: right; vertical-align: top !important;" },
                        });
                    }
                   <%: this.ID %>_ReportResoucesLoadingGrid.push({
                       title: monthNames[this.dataGetMonthWeekOfYear.listTotalWeekOfMonths[j].month],
                       headerAttributes: {
                           "class": "table-header-cell",
                           style: "text-align: center; vertical-align: middle !important;"
                       },
                       columns: columnComplexity
                   });
                }

            }

            //Generate Data
            for (var i = 0; i < dataOri.length; i++) {
                // new object for insert DataReformat
                var ResoucesLoadingData = {};

                ResoucesLoadingData["pic"] = dataOri[i].name;
                <%: this.ID %>_actualCostFormatData["pic"] = { type: "string" };

                ResoucesLoadingData["picId"] = dataOri[i].employeeId;
                <%: this.ID %>_actualCostFormatData["picId"] = { type: "string" };

                ResoucesLoadingData["typeGroupName"] = dataOri[i].typeGroupName;
                <%: this.ID %>_actualCostFormatData["typeGroupName"] = { type: "string" };

                //generate type of resourceLoadingDetail
                if (this.dataGetMonthWeekOfYear.totalWeekOfYear > 0) {
                    for (var j = 1; j <= this.dataGetMonthWeekOfYear.totalWeekOfYear; j++) {
                        ResoucesLoadingData["ResoucesLoading_" + j] = dataOri[i]["week" + j];
                    }
                }

                dataReformat[i] = ResoucesLoadingData;
            }
            return dataReformat;
         },
         generateGriReportResoucesLoading: function () {
            var that = this;
            var dataGrid = that.dsResoucesLoadingReportReformat();
            var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
            var dataSource = new kendo.data.DataSource({
                data: dataGrid,
                schema: {
                    model: {
                        id: "id",
                        fields: <%: this.ID %>_actualCostFormatData
                    }
                },
            });

            if ($(idGrid).getKendoGrid() != null) {
                $(idGrid).getKendoGrid().destroy();
                $(idGrid).empty();
            }

            if ($(idGrid).getKendoGrid() == null) {
                $(idGrid).kendoGrid({
                    toolbar: [{ template: kendo.template($("#toolbarTemplate").html()) }], // ["excel"],
                    dataSource: dataSource,
                    dataBound: <%: this.ID %>_ResoucesLoadingReportModel.onDataBound,
                    columns: <%: this.ID %>_ReportResoucesLoadingGrid,
                    scrollable: true,
                    excelExport: function (e) {
                        var sheet = e.workbook.sheets[0];
                        var rowIndex = 2, cell = 0;
                        var sheet = e.workbook.sheets[0];
                        $.each(e.data, function (key, value) {
                            if (rowIndex > 1) {
                                var row = sheet.rows[rowIndex];
 
                                if (value.level == 0) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#C00000";
                                        row.cells[cellIndex].bold = true;
                                    }
                                } else if (value.level == 1) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#1F497D";
                                    }
                                } else if (value.level == 2) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#E26B0A";
                                    }
                                }
                            }
                            rowIndex++;
                        });
                        for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                            var row = sheet.rows[rowIndex];
                            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                if (!isNaN(row.cells[cellIndex].value)) {
                                    row.cells[cellIndex].format = "#,##0.00_;"
                                }
                                if (rowIndex == 0 || rowIndex == 1) {
                                    sheet.rows[rowIndex].cells[cellIndex].vAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].hAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].bold = true;
                                }
                            }
                            if (row.type == "header") {
                                row.height = 30;
                            }
                        }

                    }
                });
            }

            $("#exportExcel").on("click", function () {
                var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
                var grid = $(idGrid).getKendoGrid();
                var column = grid.columns;
                $.each(column, function (key, value) {
                    value.title = value.title.replace("</br>", "\r");
                })
                grid.saveAsExcel();
            })

        }
    });
    $(document).ready(function () {
        <%: this.ID %>_ResoucesLoadingReportModel.dsGetMonthWeekOfYear();
        kendo.bind($("#<%: this.ID %>_EwpDrawingControl"), <%: this.ID %>_ResoucesLoadingReportModel);
        <%: this.ID %>_ResoucesLoadingReportModel.generateGriReportResoucesLoading();
        <%--$("#<%: this.ID %>_gridDrawingReport").data("kendoGrid").resize();--%>
    });
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>deliverableNo-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableNo#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>projectType-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#">#=data.projectType#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableName-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableName#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>estimateHours-headerTemplate">
    <div style="text-align: center;">Estimate<br>Hours</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>planDurationWeek-headerTemplate">
    <div style="text-align: center;">Plan<br>Duration<br>(Week)</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>actualDurationWeek-headerTemplate">
    <div style="text-align: center;">Actual<br>Duration<br>(Week)</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>startWeek-headerTemplate">
    <div style="text-align: center;">Start<br>Week</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>endWeek-headerTemplate">
    <div style="text-align: center;">End<br>Week</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>weeklyDistributedWeight-headerTemplate">
    <div style="text-align: center;">Weekly<br>Distributed<br>Weight</div>
</script>  

<div id="<%: this.ID %>_EwpDrawingControl">
    <div>
        <span>Year: </span>
        <input data-role="dropdownlist"
               data-placeholder="Select Year"
               data-value-primitive="true"
               data-text-field="text"
               data-value-field="value"
               data-bind=" source: listYear, value: paramYear, events: { change: onChangeParamYear }"
               style="width: 100px" />
    </div>
    <div id="<%: this.ID %>_grdDrawingReportControl" style="height: 450px"
         data-filterable= "true">
    </div>
</div>