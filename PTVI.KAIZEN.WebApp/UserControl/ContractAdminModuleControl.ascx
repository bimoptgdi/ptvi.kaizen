﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractAdminModuleControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ConstructionServiceModuleControl" %>
<%@ Register Src="~/UserControl/TenderOutProcessControl.ascx" TagPrefix="uc1" TagName="TenderOutProcessControl" %>
<%@ Register Src="~/UserControl/TenderProgressMonitoringControl.ascx" TagPrefix="uc1" TagName="TenderProgressMonitoringControl" %>
<div id="EngineeringServiceForm">
    <div class="k-content wide">
        <div>
            <div id="<%: this.ID %>_tabContractAdminModuleControl">
                <ul>
                    <li>Tender Out Process</li>
                    <li>Tender Progress Monitoring</li>
                </ul>
                <div>
                    <uc1:TenderOutProcessControl runat="server" ID="TenderOutProcess" />
                </div>
                <div>
                    <uc1:TenderProgressMonitoringControl runat="server" ID="TenderProgressMonitoring" />
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var <%: this.ID %>_viewMainContractAdminModel = kendo.observable({
        selectedTabIndex: 0
    });

    function onSelectTabstripContractAdmin(e) {
        var indexTab = $(e.item).index();

        if (indexTab == 1) {
            kendo.ui.progress($("#EngineeringServiceForm"), true);

            TenderProgressMonitoring_viewTenderProgressMonitoringModel.dsTenderProgressMonitoring.read();
            kendo.ui.progress($("#EngineeringServiceForm"), false);
        }
    }

    // Tab control
    var <%: this.ID %>_tabContractAdminModuleControl = $("#<%: this.ID %>_tabContractAdminModuleControl").kendoTabStrip({
        select: onSelectTabstripContractAdmin,
        tabPosition: "top",
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
    });
    <%: this.ID %>_tabContractAdminModuleControl = $("#<%: this.ID %>_tabContractAdminModuleControl").getKendoTabStrip();
    <%: this.ID %>_tabContractAdminModuleControl.select(0);

    kendo.bind($("#<%: this.ID %>_tabContractAdminModuleControl"), <%: this.ID %>_viewMainContractAdminModel);
</script>
