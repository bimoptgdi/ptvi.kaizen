﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataInputFinalResultControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DataInputFinalResultControl" %>
<script>
    var <%: this.ID %>_docId = "0";
    var <%: this.ID %>_docType = "";
    var <%: this.ID %>_docBadgeNo = "";

    var <%: this.ID %>_viewFinalResultModel = new kendo.observable({
        no: "",
        title: "",
        dsFinalResultSource: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTypeOfDrawingLIst"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdTypeOfDrawingLIst"), false);
                if (e.type == "update") {
                    <%: this.ID %>_viewFinalResultModel.otherUpdate();
                }
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "masterFinalResultId",
                    fields: {
                        masterFinalResultDesc: { editable: false },
                        defaultValuePercentage: { editable: false }
                    }
                }
            },
            transport: {
                read: {
                    async: false,
                    url: function () {
                        return _webApiUrl + "ProjectDocumentFinalResults/getFinalResultFromProjectDocId/" + <%: this.ID %>_docId + "?docType=" +<%: this.ID %>_docType + "&projectId=" + _projectId + "&badgeNo=" + <%: this.ID %>_docBadgeNo;
                    },
                    dataType: "json"
                },
                update: {
                    async: false,
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "ProjectDocumentFinalResults/updateByMasterFinalResult/" + data.masterFinalResultId;
                    },
                    type: "PUT"
                }
            }
        }),
        onEdit: function (e) {
        },
        onSave: function (e) {
            aaa = e;
        },
        otherUpdate: function (e) { },

        remarksActualResults: "",
        remarksActualResultsTmp: "",
        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours)) { %> isEnabledRemarksActualResults: true, <% } else { %> isEnabledRemarksActualResults: false, <% } %>
        onSaveRemarksActualResultsClick: function () {
            var requestData = {
                id: <%: this.ID %>_docId,
                remarksActualResults: <%: this.ID %>_viewFinalResultModel.remarksActualResults
            };
            $.ajax({
                url: _webApiUrl + "ProjectDocuments/UpdateRemarkProjectDocument/remarksActualResults",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewFinalResultModel.set("remarksActualResultsTmp", data.remarksActualResults);
                    _showDialogMessage("Remarks Actual Results", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
        onCancelRemarksActualResultsClick: function (e) {
            <%: this.ID %>_viewFinalResultModel.set("remarksActualResults", <%: this.ID %>_viewFinalResultModel.remarksActualResultsTmp);
        },

        remarksDesignKpi: "",
        remarksDesignKpiTmp: "",
        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours)) { %> isEnabledRemarksDesignKpi: true, <% } else { %> isEnabledRemarksDesignKpi: false, <% } %>
        onSaveRemarksDesignKPIClick: function () {
            var requestData = {
                id: <%: this.ID %>_docId,
                remarksDesignKpi: <%: this.ID %>_viewFinalResultModel.remarksDesignKpi
            };
            $.ajax({
                url: _webApiUrl + "ProjectDocuments/UpdateRemarkProjectDocument/remarksDesignKpi",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewFinalResultModel.set("remarksDesignKpiTmp", data.remarksDesignKpi);
                    _showDialogMessage("Remarks Design KPI", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
        onCancelRemarksDesignKPIClick: function (e) {
            <%: this.ID %>_viewFinalResultModel.set("remarksDesignKpi", <%: this.ID %>_viewFinalResultModel.remarksDesignKpiTmp);
        }
    });

    function <%: this.ID %>_finalResultValueEditTemplate(container, options) {
        var dataList = options.model.lookupFinalResultValues.toJSON();
        $('<input name="finalResultValue" data-value-primitive="true" data-text-field="text" data-value-field="value" data-bind="value: ' + options.field + '" required="required" />')
            .appendTo(container)
            .kendoComboBox({
                autoBind: false,
                dataSource: {
                    data: dataList
                }
            });
        $('<span class="k-invalid-msg" data-for="finalResultValue"></span>').appendTo(container);
    }

    function <%: this.ID %>_bind() {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewFinalResultModel); 

    }

    $(document).ready(function () {
    });

</script>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div>
            <table style="width: 100%;">
                <tr style="font-size: 17px; font-weight: bold;">
                    <td style="width: 40px;">No</td>
                    <td style="width: 400px;">: <span data-bind="text: no"></span></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr style="font-size: 17px; font-weight: bold;">
                    <td>Title</td>
                    <td>: <span data-bind="text: title"></span></td>
                    <td style="width: 350px;">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div id="<%: this.ID %>_grdTypeOfDrawingLIst" 
            data-role="grid"
    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours))
    { %>
            data-editable= "inline"
    <% } else { %>
            data-editable= "false"
    <% } %>
            data-selectable="true"
            data-bind="source: dsFinalResultSource, events: { edit: onEdit, save: onSave }"
            data-columns="[
                        { 
                            field: 'masterFinalResultDesc', title: 'Description', width: 400, 
                            template: kendo.template($('#<%: this.ID %>_DrawingName-template').html())
                        },
                        { 
                            field: 'defaultValuePercentage', title: 'Value', width: 90, 
                            template: '#=data.defaultValuePercentage ? kendo.toString(data.defaultValuePercentage, \'n2\') : \'-\'# %',
                            attributes: { style: 'text-align: right; vertical-align: top !important;' }
                        },
                        { 
                            field: 'finalResultValue', title: 'Action Value', width: 160, 
                            template: kendo.template($('#<%: this.ID %>_numberOfDrawing-template').html()),
                            editor: <%: this.ID %>_finalResultValueEditTemplate,
                            attributes: { style: 'text-align: right; vertical-align: top !important;' }
                        },
                        {
                            command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}}], width: 180 
                        }
                        ]"></div>
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Remarks Actual Results</legend>
                    <textarea data-bind="value: remarksActualResults, enabled: isEnabledRemarksActualResults" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.DCWeightHours))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveRemarksActualResultsClick }, visible: isEnabledRemarksActualResults"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelRemarksActualResultsClick }, visible: isEnabledRemarksActualResults"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Remarks Design KPI</legend>
                    <textarea data-bind="value: remarksDesignKpi, enabled: isEnabledRemarksDesignKpi" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.DCWeightHours))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveRemarksDesignKPIClick }, visible: isEnabledRemarksDesignKpi"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelRemarksDesignKPIClick }, visible: isEnabledRemarksDesignKpi"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
<script id="<%: this.ID %>_DrawingName-template" type="text/x-kendo-template">
    <div>#= data.masterFinalResultDesc ? data.masterFinalResultDesc : "-" #</div>
</script>
<script id="<%: this.ID %>_numberOfDrawing-template" type="text/x-kendo-template">
    <div>
        # if (data.finalResultValue) { #
            # var dataLookup = Enumerable.From(data.lookupFinalResultValues).Where("$.value == '" + data.finalResultValue + "'").ToArray() #
            #= dataLookup.length > 0 ? dataLookup[0].text : " - "#
        # } else { #
            -
        # } #
    </div>
</script>
        
    </div>
</div>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .engMaterialsContainer{
        width: 400px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }
</style>

