﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~BUDGETListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.BUDGETListControl" %>
<%@ Register Src="~/UserControl/RequestBUDGETControl.ascx" TagPrefix="uc1" TagName="RequestBUDGETControl" %>


<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    var <%: this.ID %>_popEditor;
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        
        dsEWR: new kendo.data.DataSource({
            schema: {
                model: {
                    id: "OID",
                    fields: {
                        SUBJECT: { type: "string" },
                        AREA: { type: "string" },
                        DATEISSUED: { type: "date" },
                        ACCCODE: { type: "string" },
                        NETWORKNO: { type: "string" },
                        BUDGETALLOC: { type: "number" },
                        DATECOMPLETION: { type: "date" }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "EWRREQUESTs/FindAllEWRRequest/dw",
                    dataType: "json"
                },
                update: {
                    url: "",
                    dataType: "json"
                },
                create: {
                    url: "",
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            },
            pageSize: 10
        })
    });
    function <%: this.ID %>_showEditor(e) {
        var pickedData = this.dataItem($(e.currentTarget).closest("tr"));
        // show popup
        <%: this.ID %>_popEditor.center().open();
        RequestBUDGETControl_Init(pickedData, "BUDGETEngineeringForm.aspx");
        RequestBUDGETControl_viewModel.set("onCloseUpdateClick", function (e) {
            <%: this.ID %>_popEditor.close();
        });
    }
    // Document Ready
    $(document).ready(function () {
        //<%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#RequestContent"), <%: this.ID %>_viewModel);
        <%: this.ID %>_popEditor = $("#<%: this.ID %>_popEditor").kendoWindow({
            title: "BUDGET Editor",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

    }); //doc ready

</script>
<div id="RequestContent" class="k-content wide">

    <div data-role="grid"
        data-bind="source: dsEWR"
        data-editable="{mode:'popup', confirmation:true}"
        data-pageable="{buttonCount: 5}"
        data-filterable="{
                extra: false,
                operators: {
                    string: {
                        contains: 'Contains',
                        startswith: 'Starts with'
                    }
                }
            }"
        data-columns="[
                { 'field': 'SUBJECT', 'title': 'Subject' },
                { 'field': 'AREA', 'title': 'Area' },
                { 'field': 'DATEISSUED', 'title': 'Issued', 'format': '{0:dd MMM yyyy}' },
                { 'field': 'ACCCODE', 'title': 'Account Code' },
                { 'field': 'NETWORKNO', 'title': 'Network No' },
                { 'field': 'BUDGETALLOC', 'title': 'Budget Alloc.', 'format': '{0:n}' },
                { 'field': 'DATECOMPLETION', 'title': 'Completion', 'format': '{0:dd MMM yyyy}' }
<%--                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ProjectBudgetApproval))
                { %>
                ,
                { command: { text: 'Edit', click: <%: this.ID %>_showEditor }, title: ' ', width: '180px' }
                <% } %>--%>
            ]">
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
<div id="<%: this.ID %>_popEditor">
    <uc1:RequestBUDGETControl runat="server" ID="RequestBUDGETControl" />
</div>