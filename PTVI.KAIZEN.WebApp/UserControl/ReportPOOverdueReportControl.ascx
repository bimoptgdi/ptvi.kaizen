﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportPOOverdueReportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportPOOverdueReportControl" %>
<div id="<%: this.ID %>_ReportPOOverdueReportContent">
     <div>
       <fieldset data-bind="invisible: searchOption">
            <legend>Search Criteria</legend>
                <div class="divRow">
                    <div class="divCell"><span class="textLabel">Project No</span><br />
                        <input class="k-textbox" type="text" data-role="textbox" name="projectNo" data-bind="value: projectNo" style="width: 250px" placeholder="Project No" /></div><br />
                    <div class="divCell"><span class="textLabel">Project Desc.</span><br />
                        <input class="k-textbox" type="text" data-role="textbox" name="projectDesc" data-bind="value: projectDesc" style="width: 90%" placeholder="Project Description" /></div><br />
                    <div class="divCell"><span class="textLabel">Project Manager</span><br />
                        <select id="<%: this.ID %>_msPM" 
                            style="width: 400px;"
                            data-role="multiselect"
                            data-placeholder="Project Managers"
                            data-value-primitive="true"
                            data-text-field="employeeName"
                            data-value-field="employeeId"
                            data-readonly="true"
                            data-bind="value: valuePM, source: dataPM"></select>
                    </div><br />
                    <div class="divCell"><span class="textLabel">PO Overdue</span><br />
                        <input type="checkbox" name="overDue" data-bind="checked: overDue" value="bike"/>Option to display Overdue PO
                    </div><br />
                    <div class="divCell" style="text-align:right">
                        <div
                            data-role="button"
                            data-bind="events: { click: onSearchClick }"
                            class="k-button k-primary">
                            <span class="k-icon k-i-filter"></span>
                            Search
                        </div>
                        <div
                            data-role="button"
                            data-bind="events: { click: onResetClick }"
                            class="k-button">
                            <span class="k-icon k-i-cancel"></span>
                            Reset
                        </div></div>
                </div>
        </fieldset>
    </div>
    <div data-bind="visible: searchOption" >
        <div style="text-align: center; font-size: 16px; font-weight: bold;">PO Overdue Report<br /><br /><br /></div>
        <div id="<%: this.ID %>_grdReportPOOverdueReport" 
            data-toolbar= "[{ name: 'excelExport', text: 'Export to Excel'}, { name:'generatePDF', text: 'Generate PDF' }]", 
            data-role="grid"
            data-no-records="true"
            data-sortable="true"
            data-editable="false"
            data-scrollable="false"
            data-columns="[
                { field: 'projectNo', title: 'Project No', width: 250, template: kendo.template($('#<%: this.ID %>_project-template').html()) },
                { field: 'projectDesc', title: 'Project Desc', width: 180, hidden: true },
                { field: 'projectManager', title: 'Project Manager', width: 130 },
                { field: 'mrNo', title: 'MR No', template: '#: data.mrNo ? data.mrNo : \'-\'#', width: 110 },
                { field: 'poNo', title: 'PO', template: kendo.template($('#<%: this.ID %>_po-template').html()), width: 110 },
                { field: 'poItemNo', title: 'Item No', template: '#: data.poItemNo ? data.poItemNo : \'-\'#', width: 110, hidden: true },
                { field: 'poDate', title: 'PO Date', template: kendo.template($('#<%: this.ID %>_poDate-template').html()), width: 110 },
                { field: 'poRaisedDate', title: 'PO Actual Date', template: kendo.template($('#<%: this.ID %>_poRaisedDate-template').html()), width: 110, hidden: true },
                { field: 'orderedQuantity', title: 'Quantity', template: kendo.template($('#<%: this.ID %>_quantity-template').html()), width: 110 },
                { field: 'receivedQuantity', title: 'Qty Received', width: 110, hidden: true },
                { field: 'etaSiteDate', title: 'Delivery', template: kendo.template($('#<%: this.ID %>_etaSiteDate-template').html()), width: 110 },
                { field: 'actualDeliveryDate', title: 'Actual Delivery', template: kendo.template($('#<%: this.ID %>_actualDeliveryDate-template').html()), hidden: true, width: 110 },
                { field: 'buyers', title: 'Buyer', width: 160 }
            ]"
            data-bind="source: dsReportPOOverdueReport">
        </div>
    <div style="float: right;">Exported at [<span id="currentDate"></span>]</div>
    <script type="text/x-kendo-template" id="<%: this.ID %>_project-template">
            <div class='subColumnTitle'>
                No:
            </div>
            <div> 
                #= data.projectNo # 
            </div>
            <div class='subColumnTitle'>
                Description:
            </div>
            <div> 
                #= data.projectDesc # 
            </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_po-template">
            <div class='subColumnTitle'>
                No:
            </div>
            <div> 
                #= data.poNo ? data.poNo : '-' # 
            </div>
            <div class='subColumnTitle'>
                Item No:
            </div>
            <div> 
                #: data.poItemNo ? data.poItemNo : '-'#
            </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_poDate-template">
            <div class='subColumnTitle'>
                Plan:
            </div>
            <div> 
                #=(data.poDate ? kendo.toString(data.poDate, "dd MMM yyyy") : "-")#
            </div>
            <div class='subColumnTitle'>
                Actual:
            </div>
            <div> 
                #=(data.poRaisedDate ? kendo.toString(data.poRaisedDate, "dd MMM yyyy") : "-")#
            </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_quantity-template">
            <div class='subColumnTitle'>
                Ordered:
            </div>
            <div> 
                #=(data.orderedQuantity ? kendo.toString(data.orderedQuantity, "n3") : "0")#
            </div>
            <div class='subColumnTitle'>
                Received:
            </div>
            <div> 
                #=(data.receivedQuantity ? kendo.toString(data.receivedQuantity, "n3") : "0")#
            </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_poRaisedDate-template">
        #=(data.poRaisedDate ? kendo.toString(data.poRaisedDate, "dd MMM yyyy") : "-")#
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_etaSiteDate-template">
            <div class='subColumnTitle'>
                Estimated Date:
            </div>
            <div> 
                #=(data.etaSiteDate ? kendo.toString(data.etaSiteDate, "dd MMM yyyy") : "-")#
            </div>
            <div class='subColumnTitle'>
                Actual Date:
            </div>
            <div> 
                #=(data.actualDeliveryDate ? kendo.toString(data.actualDeliveryDate, "dd MMM yyyy") : "-")#
            </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_actualDeliveryDate-template">
        #=(data.actualDeliveryDate ? kendo.toString(data.actualDeliveryDate, "dd MMM yyyy") : "-")#
    </script>
    <script>
        $("#currentDate").text(kendo.toString(new Date(), "dd MMM yyyy HH:mm:ss"));

        var <%: this.ID %>_viewModelPOOverdue = new kendo.observable({
            searchOption: false,
            printOption: false,
            projectNo: "",
            projectDesc: "",
            pmParam: "",
            pmParamName: "",
            overDue: false,
            dsReportPOOverdueReport: new kendo.data.DataSource({
                requestStart: function () {
                    kendo.ui.progress($("#<%: this.ID %>_grdReportPOOverdueReport"), true);
                },
                requestEnd: function () {
                    kendo.ui.progress($("#<%: this.ID %>_grdReportPOOverdueReport"), false);
                    window.status = 'ready_to_print';
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            poDate: { type: "date" },
                            poRaisedDate: { type: "date" },
                            etaSiteDate: { type: "date" },
                            actualDeliveryDate: { type: "date" },
                            orderedQuantity: { type: "number" },
                            receivedQuantity: { type: "number" }
                        }
                    }
                },
                transport: {
                    read: {
                        async: false,
                        url: function (data) {
                            return _webApiUrl + "Report/POOverdueReport/?projectNo=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.projectNo) + "&projectDesc=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.projectDesc) + "&pmParam=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.valuePM.join("|")) + "&overDue=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.overDue)
                        },
                        dataType: "json"
                    }
                },
                sort: [{ field: "docType", dir: "asc" }]
            }),
            dataPM: new kendo.data.DataSource({
                requestStart: function () {
                    kendo.ui.progress($("#<%: this.ID %>_grdReportPOOverdueReport"), true);
                },
                requestEnd: function () {
                    kendo.ui.progress($("#<%: this.ID %>_grdReportPOOverdueReport"), false);
                },
                transport: {
                    async: false,
                    read: {
                        dataType: "json",
                        url: _webApiUrl + "v_projectEmployeeRole/listEmployeeByRoleId/1"
                    },
                    parameterMap: function (options, operation) {
                    }
                },
                schema: {
                    model: {
                        id: "employeeId"

                    },
                },
                //sort: [{ field: "employeeName", dir: "asc" }],
                batch: false
            }),
            valuePM: [],
            generateExcel: function (e) {
                var currentDate = kendo.toString(new Date, "- dd MMM yyyy - HH_mm_ss");
                var rows = [
                    {
                        cells: [
                            {
                                value: "Report PO Overdue",
                                fontSize: 20,
                                textAlign: "center",
                                verticalAlign: "center",
                                background: "#60b5ff",
                                color: "#ffffff"
                            }
                        ],
                        type: "header",
                        height: 70
                    },
                    {
                        cells: [
                            { value: "Project No", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Project Desc", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Project Manager", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "MR No", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "PO No", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Item No", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "PO Plan Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "PO Actual Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Quantity Ordered", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Quantity Received", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Delivery Estimate Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Delivery Actual Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                            { value: "Buyer", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" }
                        ],
                        type: "header"
                    }];

                var dataRow = this.dsReportPOOverdueReport;

                dataRow.fetch(function () {
                    var data = this.data();
                    for (var i = 0; i < data.length; i++) {
                        //push single row for every record
                        rows.push({
                            cells: [
                              { value: data[i].projectNo },
                              { value: data[i].projectDesc },
                              { value: data[i].projectManager },
                              { value: data[i].mrNo },
                              { value: data[i].poNo },
                              { value: data[i].poItemNo },
                              { value: data[i].poDate, format: "dd MMM yyyy" },
                              { value: data[i].poRaisedDate, format: "dd MMM yyyy" },
                              { value: data[i].orderedQuantity },
                              { value: data[i].receivedQuantity },
                              { value: data[i].etaSiteDate, format: "dd MMM yyyy" },
                              { value: data[i].actualDeliveryDate, format: "dd MMM yyyy" },
                              { value: data[i].buyers }
                            ]
                        })
                    }
                    var workbook = new kendo.ooxml.Workbook({
                        sheets: [
                          {
                              columns: [
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { width: 110, autoWidth: false },
                                { width: 110, autoWidth: false },
                                { autoWidth: true },
                                { autoWidth: true },
                                { width: 110, autoWidth: false },
                                { width: 110, autoWidth: false },
                                { autoWidth: true }
                              ],
                              // Title of the sheet
                              title: "Reports",
                              // Rows of the sheet
                              rows: rows,
                              frozenRows: 2,
                              mergedCells: ["A1:M1"]
                          }
                        ]
                    });

                    //sheet.rows.splice(0, 0, { cells: myHeaders, type: "header", height: 70 });

                    kendo.saveAs({
                        dataURI: workbook.toDataURL(),
                        fileName: "Report PO Overdue " + currentDate + ".xlsx",
                        proxyURL: "<%:WebApiUrl%>" + "exportproxy"
                    });
                });
            },
            onSearchClick: function (e) {
                //this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + encodeURIComponent(this.projectNo) + "&projectDesc=" + encodeURIComponent(this.projectDesc) + "&pmParam=" + encodeURIComponent(this.valuePM.join("|")) + "&overDue=" + encodeURIComponent(this.overDue.join("|"));
                //this.viewDetailsSource.read();
                if (<%: this.ID %>_viewModelPOOverdue.projectNo || <%: this.ID %>_viewModelPOOverdue.projectDesc ||
                    <%: this.ID %>_viewModelPOOverdue.valuePM.length != 0) {
                    var urlOpenWindow = _webAppUrl + "ReportPOOverdueReportPrint.aspx?_Search=true&_No=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.projectNo) + "&_Desc=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.projectDesc) + "&_PM=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.valuePM.join("|")) + "&_OverDue=" + encodeURIComponent(<%: this.ID %>_viewModelPOOverdue.overDue);
                    window.open(urlOpenWindow);
                }
                else {
                    $('<div/>').kendoAlert({
                        title: 'Search Request',
                        modal: true,
                        content: "<div>Search Criteria can't be empty</div>",
                        actions: [{ text: "OK" }]
                    }).data('kendoAlert').open();
                }
            },
            createpdf: function (e) {
                //this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + encodeURIComponent(this.projectNo) + "&projectDesc=" + encodeURIComponent(this.projectDesc) + "&pmParam=" + encodeURIComponent(this.valuePM.join("|")) + "&overDue=" + encodeURIComponent(this.overDue.join("|"));
                //this.viewDetailsSource.read();

                //var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--window-status%20ready_to_print%20%20" + urlPDF + "/";
                var urlPDF = encodeURIComponent(_webAppUrl.replace(/\//g, "|") + "ReportPOOverdueReportPrint.aspx?_Print=true&_Search=true&_No=" + <%: this.ID %>_viewModelPOOverdue.projectNo + "&_Desc=" + <%: this.ID %>_viewModelPOOverdue.projectDesc + "&_PM=" + <%: this.ID %>_viewModelPOOverdue.valuePM.join("|") + "&_OverDue=" + <%: this.ID %>_viewModelPOOverdue.overDue);
                var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--no-stop-slow-scripts%20--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20" + urlPDF + "/";

                window.open(pdfUrl);
            },
            onResetClick: function (e) {
                this.set("projectNo", "");
                this.set("projectDesc", "");
                this.set("valuePM", []);
                this.set("overDue", false);
                this.viewDetailsSource.options.transport.read.url = _webApiUrl + "Report/POOverdueReport/?projectNo=" + this.projectNo + "&projectDesc=" + this.projectDesc + "&pmParam=" + this.valuePM + "&overDue=" + this.overDue;
                this.viewDetailsSource.read();
            }
        });

        var _Print = "<%:Request["_Print"]%>",
            _Search = "<%:Request["_Search"]%>",
            _No = "<%:Request["_No"]%>",
            _Desc = "<%:Request["_Desc"]%>",
            _PM = "<%:Request["_PM"]%>",
            _OverDue = "<%:Request["_OverDue"]%>";
        if (_Print) {
            <%: this.ID %>_viewModelPOOverdue.set("printOption", true);
        }    
        if (_Search) {
            <%: this.ID %>_viewModelPOOverdue.set("searchOption", true);
        }    
        if (_No) {
            <%: this.ID %>_viewModelPOOverdue.set("projectNo", _No);
        }    
        if (_Desc) {
            <%: this.ID %>_viewModelPOOverdue.set("projectDesc", _Desc);
        }
        if (_PM) {
            <%: this.ID %>_viewModelPOOverdue.set("valuePM", _PM.split("|"));
        }
        if (_OverDue) {
            <%: this.ID %>_viewModelPOOverdue.set("overDue", _OverDue);
        }

    <%: this.ID %>_popPickEngineer = $("#<%: this.ID %>_popPickEngineer").kendoWindow({
        title: "Timesheets Engineer",
        modal: true,
        visible: false,
        resizable: false,
        width: 800,
        height: 450
    }).data("kendoWindow");

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ReportPOOverdueReportContent"), <%: this.ID %>_viewModelPOOverdue);
        if (<%: this.ID %>_viewModelPOOverdue.printOption) {
            $(".k-grid-toolbar").hide();
            $("#<%: this.ID %>_grdReportPOOverdueReport .k-grid-header").css("padding", "0 !important");
            $("#<%: this.ID %>_grdReportPOOverdueReport .k-grid-content").css("overflow-y", "visible");
        }
        else {
            $(".k-grid-generatePDF").find('span').addClass("k-font-icon k-i-pdf");
            $(".k-grid-generatePDF").find('span').css({ "vertical-align": "text-top", "margin-left": "-.3rem", "margin-right": ".3rem" });
            $(".k-grid-generatePDF").bind("click", function (e) {
                <%: this.ID %>_viewModelPOOverdue.createpdf();
            });

            $("#<%: this.ID %>_grdReportPOOverdueReport .k-grid-excelExport").find('span').addClass("k-font-icon k-i-xls");
            $("#<%: this.ID %>_grdReportPOOverdueReport .k-grid-excelExport").bind("click", function (e) {
                <%: this.ID %>_viewModelPOOverdue.generateExcel();
            });
        }
    });
    </script>

</div>