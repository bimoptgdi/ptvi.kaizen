﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDeliverableExControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportDeliverableExControl" %>
<div id="viewDetailsPanel">
    <div>
        <fieldset>
            <legend>Search</legend>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 100px">Project No</div>
                <div style="display: inline-block;padding-left: 20px">
                    <input id="<%: this.ID %>_projectNo" 
                        type= "text" 
                        class= "k-textbox" 
                        data-bind="value: projectNo"/>
                </div>
            </div>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 100px">Description</div>
                <div style="display: inline-block;padding-left: 20px">
                    <input id="<%: this.ID %>_description" 
                        type= "text" 
                        class= "k-textbox" 
                        data-bind="value: description"
                        style="width: 250px"/>
                </div>
            </div>
<%--                <div style="padding-bottom: 10px">
                <div style="display: inline-block; width: 100px">Status:</div> 
                <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_msStatus" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Select Status"
                        data-value-primitive="true"
                        data-text-field="text"
                        data-value-field="value"
                        data-readonly="true"
                        data-bind="value: status, source: dataStatus"></select>
                </div>
            </div>--%>
            <br />
            <div id="<%: this.ID %>_btnSearch" 
                data-role="button"
                data-bind="events: {click: searchClick}"
                >Search
            </div>
            <div id="<%: this.ID %>_btnReset" 
                data-role="button"
                data-bind="events: {click: resetClick}"
                >Reset
            </div>

        </fieldset>
    </div>
        <fieldset>
            <legend>Result</legend>
    <div id="<%: this.ID %>_grdViewTSDetail" data-role="grid" style="width: 750px"
        data-bind="source: viewDetailsSource, events: { excelExport: excelExport}" 
        data-filterable= "true"
        data-height="450"
        data-toolbar="['excel']"
        data-excel="{
                allPages: true,
            }"
        data-columns="[ 
            {field: 'projectNo', title: 'Project No', width: 150},
            {field: 'desc', title: 'Description', width: 250},
            {field: 'projectCategoryText', title: 'Project Category', width: 150},
            {field: 'projectManager', title: 'Project Manager', width: 150},
            {field: 'projectEngineer', title: 'Project Engineer', width: 150},
            {field: 'pcText', title: 'Project Controller', width: 150},
            {field: 'poText', title: 'Project Officer', width: 150},
            {field: 'mcText', title: 'Material Coordinator', width: 150},
            {field: 'uaText', title: 'Engineers', width: 150},
            {field: 'totalCost', title: 'Total Budget', width: 150, format:'{0:n2}', attributes:{style:'text-align:right; vertical-align: top !important;overflow: visible;'}, template: kendo.template($('#<%: this.ID %>_budget-template').html())},
            {field: 'actualCost', title: 'ETC', width: 200, format:'{0:n2}', attributes:{style:'text-align:left;'}, template: kendo.template($('#<%: this.ID %>_etc-template').html())},
            {field: 'assignedCost', title: 'Assigned Cost', width: 150, format:'{0:n2}', attributes:{style:'text-align:right;'}},
            {field: 'remainingBudget', title: 'Remaining Budget', width: 150, format:'{0:n2}', attributes:{style:'text-align:right;'}},
            {field: 'scope', title: 'Scope', width: 100},
            {field: 'benefit', title: 'Objective', width: 100},
            {field: 'accomplishment', title: 'Accomplishment', width: 100},
            {field: 'details', title: 'Next Plan', width: 100},
            {field: 'contractor', title: 'Contractor', width: 100},
            {field: 'manageKeyIssue', title: 'Issue/Risk', width: 100},
            {field: 'doc', title: 'Doc', width: 70},
        ]"
        data-pageable="true"
        ></div>                
    </fieldset>
</div>
<style>
.tooltip {
        position: relative;
        display: inline-block;
        text-align: center;
        margin: 0 0 0 4px;
        cursor: pointer;
    }
    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        color: #fff;
        border-radius: 6px;
        width: 200px;
        text-align: left;
        padding-left: 10px;
        padding-right: 10px;
        background-color: #808080;
        padding-bottom: 10px;
             
        /* Position the tooltip text - see examples below! */
        position: absolute;
        top: -5px;
        right: 105%; 
        z-index: 999999;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .tooltip:hover {
        color: #fff;
        padding-right: 1px;
        padding-top: 1px;
        box-sizing: padding-box;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        -webkit-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        transition: all 0.3s linear;
        color: #fff;
        border: none;
    }
    .tooltip .tooltiptext::after {
        content: " ";
        position: absolute;
        top: 50%;
        left: 100%; /* To the right of the tooltip */
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent transparent transparent #808080;
    }
    .tooltip_wrapper {
        display: inline-block;
        width: 30px;
    }
    .hrStyle {
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -moz-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -ms-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -o-linear-gradient(left, #808080, #ffffff, #808080);
    }
</style>
<script type="text/x-kendo-template" id="<%: this.ID %>_budget-template">

# var etcCost = actualCost + commitment + forecast; #    
# var budgetDisplay = kendo.toString(totalCost ? totalCost : 0, "n2"); #
<div class='tooltip'>
    # if (etcCost > totalCost) { #
        <div style="color:red;">
            #: budgetDisplay #
        </div>
        <div class='tooltiptext'>
            <div class='popup-title'>Over Budget</div>
            <hr class='hrStyle' />ETC > Budget
        </div>
    # } else { #
        <div style="color:green;">
            #: budgetDisplay #
        </div>
        <div class='tooltiptext'>
            <div class='popup-title'>In Budget</div>
            <hr class='hrStyle' />Budget > ETC
        </div>
    # } #
    
</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_etc-template">
# var actualDisplay = kendo.toString(actualCost ? actualCost : 0, "n2"); #
# var commitmentDisplay = kendo.toString(commitment ? commitment : 0, "n2"); #
# var forecastDisplay = kendo.toString(forecast ? forecast : 0, "n2"); #

<div>
    <b>Actual Cost</b>: #: actualDisplay #
        <br/>
    <b>Commitment</b>: #: commitmentDisplay #
        <br/>
    <b>Service Commitment</b>: #: forecastDisplay #
</div>
</script>

<script id="<%: this.ID %>_dateCreated" type="text/x-kendo-template">
    <div> #= createdDate? kendo.toString(kendo.parseDate(createdDate), "dd MMM yyyy"):'-' #</div>
</script>

<script>
    <%: this.ID %>_popPickEngineer = $("#<%: this.ID %>_popPickEngineer").kendoWindow({
        title: "Timesheets Engineer",
        modal: true,
        visible: false,
        resizable: false,
        width: 800,
        height: 450
    }).data("kendoWindow");

    var <%: this.ID %>_viewDetailsSource = new kendo.data.DataSource({
        transport: {
            read    : { dataType: "json",
                url: _webApiUrl + "project/listDeliverableEx/" + _currNTUserID + "?projectNo=" + "&description=" + "&status="
                },
            parameterMap: function (options, operation) {
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    projectNo: { type: "string" },
                    desc: { type: "string" },
                    projectManager: { type: "string" },
                    projectEngineer: { type: "string" },
                    projectController: { type: "string" },
                    totalCost: { type: "number" },
                    actualCost: { type: "number" },
                    commitment: { type: "number" },
                    forecast: { type: "number" },
                    assignedCost: { type: "number" },
                    remainingBudget: { type: "number" },
                    scope: { type: "string" },
                    benefit: { type: "string" },
                    accomplishment: { type: "string" },
                    details: { type: "string" },
                    contractor: { type: "string" },
                    manageKeyIssue: { type: "string" },
                    engineer: { type: "string" },
                    docNo: { type: "string" },
                    docTitle: { type: "string" },
                    planStartDate: { type: "date" },
                    planFinishDate: { type: "date" },
                    actualStartDate: { type: "date" },
                    actualFinishDate: { type: "date" },
                    status: { type: "string" },
                    statusDesc: { type: "string" },
                    remark: { type: "string" },
                },
            },
            parse: function (response) {
                for (var i = 0; i < response.length; i++) {
                    var str = response[i].pcText;
                    var res = str.replace(/,/g, "<br/>");
                    response[i].pcText = res;
                    str = response[i].uaText;
                    res = str.replace(/,/g, "<br/>");
                    response[i].uaText = res;
                }
                return response;
            }

        },
        pageSize: 10,
        batch: false
    });

    var <%: this.ID %>_dataStatus = new kendo.data.DataSource({
        requestStart: function () {
            kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), true);
        },
        requestEnd: function () {
            kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), false);
        },
        transport: {
            read    : { dataType: "json",
                url: _webApiUrl + "Lookup/FindByType/statusIprom"
                },
            parameterMap: function (options, operation) {
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    value : { type: "string" },
                    text : { type: "string" },
                },
            },
        },
        pageSize: 10,
        batch: false
    });

    var <%: this.ID %>_vmPopViewDetails = new kendo.observable({
        projectNo: "",
        description: "",
        status: "",
        viewDetailsSource: <%: this.ID %>_viewDetailsSource,
        dataStatus: <%: this.ID %>_dataStatus,
        dataEmployee: [],
        valueEmployee: [],
        excelExport: function (e) {
            var data = e.data.viewDetailsSource.data();
            var gridColumns = e.sender.columns;
            var sheet = e.workbook.sheets[0];
            var visibleGridColumns = [];
            var columnTemplates = [];
            var dataItem;
            var elem = document.createElement('div');

            // Get a list of visible columns
            for (var i = 0; i < gridColumns.length; i++) {
                if (!gridColumns[i].hidden) {
                    visibleGridColumns.push(gridColumns[i]);
                }
            }

            // Create a collection of the column templates, together with the current column index
            for (var i = 0; i < visibleGridColumns.length; i++) {
                if (visibleGridColumns[i].template) {
                    columnTemplates.push({ cellIndex: i, template: kendo.template(visibleGridColumns[i].template) });
                }
            }

            var sheet = e.workbook.sheets[0];
            for (var i = 1; i < sheet.rows.length; i++) {
                var row = sheet.rows[i];
                var dataItem = data[i - 1];

                var str = sheet.rows[i].cells[5].value;
                var res = str.replace(/<br\/>/g, "\n");
                sheet.rows[i].cells[5].value = res;
                str = sheet.rows[i].cells[6].value;
                res = str.replace(/<br\/>/g, "\n");
                sheet.rows[i].cells[6].value = res;
                str = sheet.rows[i].cells[7].value;
                res = str.replace(/<br\/>/g, "\n");
                sheet.rows[i].cells[7].value = res;
                str = sheet.rows[i].cells[8].value;
                res = str.replace(/<br\/>/g, "\n");
                sheet.rows[i].cells[8].value = res;
                sheet.rows[i].cells[9].format = "#,##0.0_);-#,##0.0;0.0;"
                sheet.rows[i].cells[10].format = "#,##0.0_);-#,##0.0;0.0;"
                sheet.rows[i].cells[11].format = "#,##0.0_);-#,##0.0;0.0;"
                sheet.rows[i].cells[12].format = "#,##0.0_);-#,##0.0;0.0;"
                sheet.rows[i].cells[13].format = "#,##0.0_);-#,##0.0;0.0;"

                for (var j = 0; j < columnTemplates.length; j++) {
                    var columnTemplate = columnTemplates[j];
                    // Generate the template content for the current cell.
                    elem.innerHTML = columnTemplate.template(dataItem);
                    if (row.cells[columnTemplate.cellIndex] != undefined)
                        // Output the text content of the templated cell into the exported cell.

                        if (columnTemplate.cellIndex == 10) {
                            if (dataItem.actualCost == null && dataItem.commitment == null && dataItem.forecast == null)
                                row.cells[columnTemplate.cellIndex].value = "";
                            else
                                row.cells[columnTemplate.cellIndex].value = ('    ' + $.trim(elem.textContent)) || ('    ' + $.trim(elem.innerText)) || "";
                        }
                }
            }
        },
        searchClick: function (e) {
            var status = status ? this.status.join(","):"";
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "project/listDeliverableEx/" +
                _currNTUserID + "?projectNo=" + this.projectNo + "&description=" + this.description + "&status=" + status
            this.viewDetailsSource.read();
        },
        resetClick: function (e) {
<%--            $("#<%: this.ID %>_projectNo").val("");  
            $("#<%: this.ID %>_description").val("");  
            $("#<%: this.ID %>_msStatus").getKendoMultiSelect().value([]);  --%>

            this.set("projectNo", "");
            this.set("description", "");
            this.set("status", []);
            var status = this.status.join(",");
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "project/listDeliverableEx/" +
                _currNTUserID + "?projectNo=" + this.projectNo + "&description=" + this.description + "&status=" + status
            this.viewDetailsSource.read();
        },

    });

    kendo.bind($("#viewDetailsPanel"), <%: this.ID %>_vmPopViewDetails); 

</script>
