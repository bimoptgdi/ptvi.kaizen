﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EngineeringServiceControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EngineeringService" %>
<%@ Register Src="~/UserControl/AssignEngineerDesignerControl.ascx" TagPrefix="uc1" TagName="AssignEngineerDesignerControl" %>
<%@ Register Src="~/UserControl/DocumentMaterialControl.ascx" TagPrefix="uc1" TagName="DocumentMaterialControl" %>
<%@ Register Src="~/UserControl/DesignerDrawingControl.ascx" TagPrefix="uc1" TagName="DesignerDrawingControl" %>
   
    <div id="EngineeringServiceForm">
        <div class="k-content wide">
            <div>
                <div id="EngineeringServiceTabStrip">
                    <ul>
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignEngineer) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignDesigner))
                    { %>
                        <li>Assign Engineer and Designer</li>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.EngineerMaterialReport) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity))
                    { %>
                        <li>Documents & Material</li>
                     <% }%>


                    <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageDesignDoc))
                    { %>
                        <li>Designer Drawing</li>
                     <% }%>

                     

                    </ul>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignEngineer) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignDesigner))
                    { %>
                    <div>
                        <uc1:AssignEngineerDesignerControl runat="server" ID="AssignEngineerDesigner" />
                    </div>
                     <% }%>
                    
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.EngineerMaterialReport))
                    { %>
                    <div>
                        <uc1:DocumentMaterialControl runat="server" ID="DocumentMaterial" />
                    </div>
                     <% }%>  

                    <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageDesignDoc))
                    { %>
                    <div>
                        <uc1:DesignerDrawingControl runat="server" ID="DesignerDrawingControl1" />
                    </div>
                    <% }%>

                </div>
            </div>
        </div>
    </div>

    <script>

        var <%: this.ID %>_EngineeringVM = kendo.observable({
            selectedTabIndex: 0
        });


            // Tab control
            var EngineeringServiceTabStrip = $("#EngineeringServiceTabStrip").kendoTabStrip({
                tabPosition: "top",
                activate: <%: this.ID %>_onActivate,
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
            });
            EngineeringServiceTabStrip = $("#EngineeringServiceTabStrip").getKendoTabStrip();
            EngineeringServiceTabStrip.select(0);

            function <%: this.ID %>_onActivate(e) {
                if (e.item.textContent == "Designer Drawing")
                DesignerDrawingControl1_designerDwgSource.read()
            }

            // --Tab control
            kendo.bind($("#EngineeringServiceForm"), <%: this.ID %>_EngineeringVM);
//        OnDocumentReady = function () {
//        }

    </script>
