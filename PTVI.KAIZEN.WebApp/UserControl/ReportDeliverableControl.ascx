﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDeliverableControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportDeliverable" %>

<div id="viewDetailsPanel">
        <div>
            <fieldset>
                <legend>Search</legend>
                <div>
                    <div style="display: inline-block;padding-bottom: 20px; width: 100px">Project No</div>
                    <div style="display: inline-block;padding-left: 20px">
                        <input id="<%: this.ID %>_projectNo" 
                            type= "text" 
                            class= "k-textbox" 
                            data-bind="value: projectNo"/>
                    </div>
                </div>
                <div>
                    <div style="display: inline-block;padding-bottom: 20px; width: 100px">Description</div>
                    <div style="display: inline-block;padding-left: 20px">
                        <input id="<%: this.ID %>_description" 
                            type= "text" 
                            class= "k-textbox" 
                            data-bind="value: description"
                            style="width: 250px"/>
                    </div>
                </div>
                <div style="padding-bottom: 10px">
                    <div style="display: inline-block; width: 100px">Status:</div> 
                    <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                        <select id="<%: this.ID %>_msStatus" 
                            style="width: 400px;"
                            data-role="multiselect"
                            data-placeholder="Select Status"
                            data-value-primitive="true"
                            data-text-field="text"
                            data-value-field="value"
                            data-readonly="true"
                            data-bind="value: status, source: dataStatus"></select>
                    </div>
                </div>
                <br />
                <div id="<%: this.ID %>_btnSearch" 
                    data-role="button"
                    data-bind="events: {click: searchClick}"
                    >Search
                </div>
                <div id="<%: this.ID %>_btnReset" 
                    data-role="button"
                    data-bind="events: {click: resetClick}"
                    >Reset
                </div>

            </fieldset>
        </div>
            <fieldset>
                <legend>Result</legend>
                <div style="overflow-x:scroll" >
        <div id="<%: this.ID %>_grdViewTSDetail" data-role="grid" data-bind="source: viewDetailsSource" style="width:auto"
            data-filterable= "true"
            data-toolbar="['excel']"
            data-excel="{
                    allPages: true
                }"
            data-columns="[ 
                {field: 'projectNo', title: 'Project No', width: 150},
                {field: 'desc', title: 'Description', width: 200},
                {field: 'engineer', title: 'Engineer Name', width: 150},
                {field: 'designer', title: 'Designer Name', width: 150},
                {field: 'docNo', title: 'Doc. No.', width: 200},
                {field: 'docTitle', title: 'Doc. Title', width: 150},
                {field: 'planStartDate', title: 'Plan Start Date', width: 150, template: '#= planStartDate? kendo.toString(kendo.parseDate(planStartDate), \'dd MMM yyyy\'):\'\' #'},
                {field: 'planFinishDate', title: 'Plan Finish Date', width: 150, template: '#= planFinishDate? kendo.toString(kendo.parseDate(planFinishDate), \'dd MMM yyyy\'):\'\' #'},
                {field: 'actualStartDate', title: 'Actual Start Date', width: 150, template: '#= actualStartDate? kendo.toString(kendo.parseDate(actualStartDate), \'dd MMM yyyy\'):\'\' #'},
                {field: 'actualFinishDate', title: 'Actual Finish Date', width: 150, template: '#= actualFinishDate? kendo.toString(kendo.parseDate(actualFinishDate), \'dd MMM yyyy\'):\'\' #'},
                {field: 'statusDesc', title: 'Status', width: 80, template: '#= statusDesc?statusDesc:\'\' #'},
                {field: 'remark', title: 'Remark', width: 200},
            ]"
            data-pageable="true"
            data-sortable= "true"
            ></div>                
        </div>
        </fieldset>
    </div>

<script id="<%: this.ID %>_dateCreated" type="text/x-kendo-template">
    <div> #= createdDate? kendo.toString(kendo.parseDate(createdDate), "dd MMM yyyy"):'-' #</div>
</script>

<script>
        <%: this.ID %>_popPickEngineer = $("#<%: this.ID %>_popPickEngineer").kendoWindow({
            title: "Timesheets Engineer",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

    var <%: this.ID %>_viewDetailsSource = new kendo.data.DataSource({
            transport: {
                read    : { dataType: "json",
                    url: _webApiUrl + "project/listDeliverable/" + _currNTUserID + "?projectNo=" + "&description=" + "&status="
                    },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectNo : { type: "string" },
                        engineer : { type: "string" },
                        desc : { type: "string" },
                        docNo: { type: "string" },
                        docTitle: { type: "string" },
                        engineer: { type: "string" },
                        designer: { type: "string" },
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        status: { type: "string" },
                        statusDesc: { type: "string" },
                        remark: { type: "string" },
                    },
                },
            },
            sort: [{ field: "docNo", dir: "asc" }, { field: "engineer", dir: "asc" }, { field: "designer", dir: "asc" }, ],
            pageSize: 10,
            batch: false
    });

    var <%: this.ID %>_dataStatus = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), false);
            },
            transport: {
                read    : { dataType: "json",
                    url: _webApiUrl + "Lookup/FindByType/statusIprom"
                    },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        value : { type: "string" },
                        text : { type: "string" },
                    },
                },
            },
        pageSize: 10,
        batch: false
    });

    var <%: this.ID %>_vmPopViewDetails = new kendo.observable({
        projectNo: "",
        description: "",
        status: [],
        viewDetailsSource: <%: this.ID %>_viewDetailsSource,
        dataStatus: <%: this.ID %>_dataStatus,
        dataEmployee: [],
        valueEmployee: [],
        searchClick: function (e) {
            var status = status!="" ? this.status.join(","):"";
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "project/listDeliverable/" +
                _currNTUserID + "?projectNo=" + this.projectNo + "&description=" + this.description + "&status=" + status;
            this.viewDetailsSource.read();
        },
        resetClick: function (e) {
<%--            $("#<%: this.ID %>_projectNo").val("");  
            $("#<%: this.ID %>_description").val("");  
            $("#<%: this.ID %>_msStatus").getKendoMultiSelect().value([]);  --%>

            this.set("projectNo", "");
            this.set("description", "");
            this.set("status", []);
            var status = this.status.join(",");
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "project/listDeliverable/" +
                _currNTUserID + "?projectNo=" + this.projectNo + "&description=" + this.description + "&status=" + status
            this.viewDetailsSource.read();
        },

    });

    kendo.bind($("#viewDetailsPanel"), <%: this.ID %>_vmPopViewDetails); 

    </script>