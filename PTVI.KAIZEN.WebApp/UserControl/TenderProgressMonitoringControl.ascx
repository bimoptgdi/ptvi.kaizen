﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TenderProgressMonitoringControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.TenderProgressMonitoringControl" %>
<%@ Register Src="~/UserControl/PickVendorsControl.ascx" TagPrefix="uc1" TagName="PickVendorsControl" %>

<script>
    common_getAllStatusIprom();
    var <%: this.ID %>_popInputContractNo, <%: this.ID %>_popVendor;
    var <%: this.ID %>_viewTenderProgressMonitoringModel = kendo.observable({
        dsTenderProgressMonitoring: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListMainProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListMainProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        tenderNo: { editable: false },
                        docNoSummary: { editable: false }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "Tenders/listTenderMonitoringByProjectId/" + _projectId,
                    dataType: "json"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "Tenders/updateTender/" + data.id;
                    },
                    type: "PUT",
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var getTender = <%: this.ID %>_viewTenderProgressMonitoringModel.dsTenderProgressMonitoring.read()
                        }
                    }
                }
            },
            pageSize: 10,
            sort: [{ field: "tenderNo", dir: "asc" }]
        }),
        detailInit: function (e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    requestStart: function () {
                        kendo.ui.progress($("#grdListMainProject"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("#grdListMainProject"), false);
                    },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                tenderNo: { editable: false },
                                docNoSummary: { editable: false }
                            }
                        }
                    },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                processText: { editable: false },
                                processBy: { editable: false },
                                planFinishDate: { type: "date", defaultValue: null, validation: { required: { message: "Plan Date is required." }<% if (_planStartDate != "null"){ %>, min: _planStartDate<%}%><% if (_planFinishDate != "null"){ %>, max: _planFinishDate <%}%> } },
                                actualFinishDate: { type: "date" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: function (data) {
                                return _webApiUrl + "TenderProgresses/listTenderProgressByTenderId/" + e.data.id;
                            },
                            dataType: "json"
                        },
                        update: {
                            url: function (data) {
                                return _webApiUrl + "TenderProgresses/updateTenderProgresses/" + data.id;
                            },
                            type: "PUT",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    var data = jqXHR.responseJSON;
                                    if (data.contractAward == data.processType) {
                                        var getTender = <%: this.ID %>_viewTenderProgressMonitoringModel.dsTenderProgressMonitoring.get(data.tenderId);
                                        var currenRow = $("#<%: this.ID %>_grdTenderProgressMonitoringControl").getKendoGrid().table.find("tr[data-uid='" + getTender.uid + "']");
                                        if (data.actualFinishDate != null) {
                                            $(currenRow).find(".k-grid-edit").show();
                                        } else {
                                            $(currenRow).find(".k-grid-edit").hide();
                                        }
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, type) {
                            if (type == "create" || type == "update") {
                                data.planStartDate = kendo.toString(data.planStartDate, 'yyyy-MM-dd');
                                data.planFinishDate = kendo.toString(data.planFinishDate, 'yyyy-MM-dd');
                                data.actualStartDate = kendo.toString(data.actualStartDate, 'yyyy-MM-dd');
                                data.actualFinishDate = kendo.toString(data.actualFinishDate, 'yyyy-MM-dd');
                            }
                            return data;
                        }
                    },
                    sort: [{ field: "tenderNo", dir: "asc" }],
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 10
                },
                scrollable: false,
                sortable: true,
                pageable: true,
                columns: [
                    { field: "processText", title: "Process", width: 200 },
                    { field: "processBy", title: "By", width: 120 },
                    { field: "planFinishDate", title: "Plan Date", width: 95, template: '#= planFinishDate ? kendo.toString(planFinishDate,\'dd MMM yyyy\') : \'-\' #' },
                    { field: "actualFinishDate", title: "Actual Date", width: 95, template: '#= actualFinishDate ? kendo.toString(actualFinishDate,\'dd MMM yyyy\') : \'-\' #' },
                    { field: "status", title: "Status", width: 120, editor: <%: this.ID %>_categoryDropDownStatus, template: '#:common_getStatusIpromDesc(status)#' },
                    { title: " ", template: "#if (trafficStatus) { #<img class='logo' src='images/#:common_getTrafficImageUrl(trafficStatus)#' style='padding-top: 0px;'>#}#", width: 40 }
                    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageTender))
                    { %>,
                    {
                        command: ['edit'],
                        width: 70
                    }<% }%>
                ],
                editable: "inline"
            });
        },
        editProgress: function (e) {

            $("#<%: this.ID %>_btnVendorSelectedName").on("click", function () {
                TenderProgress_PickVendors_pickVendorViewModel.set("pickHandlerVendor", function (e) {
                    var pickedVendor = (this.dataItem($(e.currentTarget).closest("tr")));
                    $("#<%: this.ID %>_vendorSelectedId").val(pickedVendor.id);
                    $("#<%: this.ID %>_vendorSelectedCode").val(pickedVendor.vendor_Code);
                    $("#<%: this.ID %>_vendorSelectedName").val(pickedVendor.vendor_Name);

                    <%: this.ID %>_popVendor.close();
                });
                TenderProgress_PickVendors_pickVendorViewModel.set("onCloseSelectEvent", function (data) {
                    <%: this.ID %>_popVendor.close();
                });
                // show popup
                <%: this.ID %>_popVendor.center().open();
                TenderProgress_PickVendors_DataBind();
            });

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        saveProgress: function (e) {
            e.model.set("vendorSelectedId", $("#<%: this.ID %>_vendorSelectedId").val());
            e.model.set("vendorSelectedCode", $("#<%: this.ID %>_vendorSelectedCode").val());
            e.model.set("vendorSelectedName", $("#<%: this.ID %>_vendorSelectedName").val());

            var isValid = true;
            if (!e.model.get("contractNo")) {
                _showDialogMessage("Contract No", "Please fill all the required input", "");
                isValid = false;
            }
            else if (!e.model.get("vendorSelectedName")) {
                _showDialogMessage("Vendor", "Please select the Vendor!", "");
                isValid = false;
            }

            if (!isValid)
                e.preventDefault();
        },
        dataBound: function () {
            var grid = $("#<%: this.ID %>_grdTenderProgressMonitoringControl").getKendoGrid();
            var data = grid.dataSource.data();
            $.each(data, function (i, row) {
                if (row.isEnabledSelectVendor == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + row.uid + "']");
                    $(currenRow).find(".k-grid-edit").hide();
                }
            });
        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_TenderProgressMonitoringControl"), <%: this.ID %>_viewTenderProgressMonitoringModel);
        //<%: this.ID %>_viewTenderProgressMonitoringModel.dsTenderProgressMonitoring.read();

        <%: this.ID %>_popVendor = $("#<%: this.ID %>_popVendor").kendoWindow({
            title: "Sponsor List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        <%: this.ID %>_popInputContractNo = $("#<%: this.ID %>_popInputContractNo").kendoWindow({
            title: "Input Tender No",
            modal: true,
            visible: false,
            resizable: false,
            width: 400,
            height: 100
        }).data("kendoWindow");
    });

    function <%: this.ID %>_categoryDropDownStatus(container, options) {
        $('<input required name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                optionLabel: "Select Status...",
                dataTextField: "text",
                dataValueField: "value",
                valuePrimitive: true,
                dataSource: {
                    transport: {
                        read: {
                            url: function () {
                                return _webApiUrl + "lookup/findbytype/statusIprom";
                            }
                        }
                    },
                    sort: { field: "value", dir: "asc" }
                }
            });
    }
</script>

<script id="<%: this.ID %>_grdTenderProgressMonitoringEdit" type="text/x-kendo-tmpl">
    <div style="padding:10px">  
        <table style="width: 600px">
            <tr>
                <td style="width:100px">Tender No</td>
                <td><input name="tenderNo" placeholder="Please Input" class="k-textbox" style="width:131px" disabled /></td>
            </tr>   
            <tr>
                <td style="width:100px">Contract No<span style="color:red">*</span></td>
                <td><input name="contractNo" placeholder="Please Input" class="k-textbox" style="width:131px" required validationMessage="Contract No is required" /></td>
            </tr>   
            <tr>
                <td style="width:100px">Vendor<span style="color:red">*</span></td>
                <td><input id="<%: this.ID %>_vendorSelectedId" name="vendorSelectedId" class="k-textbox" style="display:none;" type="text" />
                    <input id="<%: this.ID %>_vendorSelectedCode" name="vendorSelectedCode" class="k-textbox" style="display:none;" type="text" />
                    <input id="<%: this.ID %>_vendorSelectedName" style="width: 400px" name="vendorSelectedName" class="k-textbox" type="text" disabled="disabled" placeholder="Vendor" required validationMessage="Vendor is required" />
                    <div
                        id="<%: this.ID %>_btnVendorSelectedName" 
                        class="k-button">
                        ...
                    </div></td>
            </tr>   
        </table>
    </div>
</script>

<div id="<%: this.ID %>_TenderProgressMonitoringControl">
    <div class="headerSubProject">EWP Tender List</div>
    <div id="<%: this.ID %>_grdTenderProgressMonitoringControl" 
        data-role="grid"
        data-sortable="true"
        data-columns="[
            {
                field: 'tenderNo', title: 'Tender No',
                width: 110
            },
            {
                field: 'docNoSummary', title: 'Description',
                width: 250
            },
            {
                field: 'vendorSelectedName', title: 'Selected Vendor',
                width: 180
            },
            {
                field: 'contractNo', title: 'Contract No',
                width: 180
            }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageTender))
                    { %>,
            {
                command: ['edit'],
                width: 70
            }<% }%>
        ]"
        data-bind="source: dsTenderProgressMonitoring, events: { edit: editProgress, save: saveProgress, dataBound: dataBound }"
        data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_grdTenderProgressMonitoringEdit").html())}'
        data-detail-init="<%: this.ID %>_viewTenderProgressMonitoringModel.detailInit"
        data-pageable="{ buttonCount: 5 }">
    </div>
    <div id="<%: this.ID %>_popVendor">
        <uc1:PickVendorsControl runat="server" id="TenderProgress_PickVendors" />
    </div>
</div>

