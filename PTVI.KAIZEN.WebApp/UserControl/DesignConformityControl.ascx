﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DesignConformityControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DesignConformity" %>
<%@ Register Src="~/UserControl/EWPWeightHoursControl.ascx" TagPrefix="uc1" TagName="DCWeightHoursControl1" %>
<%@ Register Src="~/UserControl/DataInputFinalResultControl.ascx" TagPrefix="uc1" TagName="DCDataInputFinalResultControl" %>
<script type="text/x-kendo-template" id="<%: this.ID %>_addDDTemplate">
        <div class="projectDocContainerDesConf" >
            <div class="labelProjectDoc">Milestone<span style="color:red">*</span></div>
             <input name="Phase" required="required" id="<%: this.ID %>_ddphase" data-role="dropdownlist"
                   data-placeholder="Select Milestone"
                   data-value-primitive="true"
                   data-text-field="nameText"
                   data-value-field="id"
                   data-bind="source: ProjectManagementModuleSource, value: keyDeliverablesId "
            style="width: 250px" />
        </div>

    <div class="projectDocContainerDesConf" >
           <div class="labelProjectDoc" >DC. No.</div><input name="DC. No" required="true" class="inputProjectDoc k-textbox" type="text" id="<%: this.ID %>_docNo" data-placeholder="select DocNo" data-value-primitive="true" data-bind="value: docNo" readonly/>
        </div>

 
 <div class="projectDocContainerDesConf" >
    <div class="labelProjectDoc">DC. Title<span style="color:red">*</span></div>
    <div  class="labelProjectDoc" style="margin: 0px; padding-left:0px;width:300px; vertical-align:top">
        <div id='<%: this.ID %>_dcTitlePrefixDiv' style='display: inline-block; width: 300px'><input class='k-textbox' id='<%: this.ID %>_dcTitlePrefix' style='display: inline-block;width: 300px'></input></div>
        <div id='<%: this.ID %>_dcTitleStrip' style='display: inline-block;'> - </div> 
        <div id='<%: this.ID %>_dcTitlePrefix2Div' style='display: inline-block; width: 50px'><input class='k-textbox' id='<%: this.ID %>_dcTitlePrefix2' style='display: inline-block; width: 50px' disabled /></div>
        <div id='<%: this.ID %>_dcTitlePrefix3Div' style='display: inline-block; width: 100px'><input class='k-textbox' id='<%: this.ID %>_dcTitlePrefix3' style='display: inline-block; width: 100px' disabled /></div>
        <input style='display: inline-block' name="DC. Title" required='true' class="inputProjectDoc k-textbox" style="width: 200px" type="text" id="<%: this.ID %>_docTitle" data-bind="value: docTitle"/>
    </div>
</div>

    
 <div class="projectDocContainerDesConf" >
            <div class="labelProjectDoc">Doc. No Ref</div>
            <div class="labelProjectDoc" style= "margin: 0px; padding-left:0px;">
                <select id="<%: this.ID %>_ddDocNo" data-role="multiselect"
                       data-placeholder="select DocNo"
                       data-value-primitive="false"
                       data-text-field="docNo"
                       data-value-field="id"
                       data-bind="source: ProjectDocSource, value: projectDocumentReferenceList"
                style="width: 250px" />
        </div>
    </div>

    <div class="projectDocContainerDesConf" >
            <div class="labelProjectDoc">Doc. Title Ref</div>
            <div class="labelProjectDoc"  style= "margin: 0px; padding-left:0px;">
                <select id="<%: this.ID %>_ddDocTitle" data-role="multiselect"
                       data-placeholder="Doc Title Reference"
                       data-value-primitive="false"
                       data-text-field="docTitle"
                       data-value-field="id"
                       data-bind="source: ProjectDocSource, , value: projectDocumentReferenceList , enabled: isEnabledRefEwpTitle "
                style="width: 250px"></select>
            </div>
<%--            <div class="labelProjectDoc" style="height: 15px;width: 330px; margin-top: 10px;margin-bottom: 10px;vertical-align: middle;"><span id="<%: this.ID %>_ddDocTitleSpan" data-role : "multiselect"></span></div>
            <input name="Project Doc." required="required" class=" k-textbox" style="width: 300px; display: none" type="text" id="<%: this.ID %>_ddDocTitle" data-bind="value: projectDocumentReference" readonly/>--%>
        </div>

    <div class="projectDocContainerDesConf" >
            <div class="labelProjectDoc">Discipline<span style="color:red">*</span></div>
            <div class="labelProjectDoc" style= "margin: 0px; padding-left:0px;">
                <input name="Discipline" required='required' id="<%: this.ID %>_ddlDiscipline" data-role="dropdownlist"
                   data-placeholder="select discipline"
                   data-value-primitive="true"
                   data-text-field="text"
                   data-value-field="value"
                   data-bind="
                              source: DisciplineSource,
                              value: discipline,
                            "
            style="width: 250px" />
        </div>
    </div>

    <%--        <div class="" >
            <div class="labelProjectDoc">Drawing No.</div><div class="labelProjectDoc">
            <input name="Drawing No." class="k-textbox" style="width: 100px" type="text" id="drawingNo" data-bind="value: drawingNo" required="required"/></div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Drawing Title</div><div class="labelProjectDoc">
            <input name="Drawing Title" class="k-textbox" style="width: 250px" type="text" id="drawingTitle" data-bind="value: drawingTitle" required="required"/></div>
        </div>--%>
 <div class="projectDocContainerDesConf" >
            <div class="labelProjectDoc">Plan Start Date<span style="color:red">*</span></div>
                <input name="Plan Start Date" required="required" data-format="dd MMM yyyy" id="<%: this.ID %>_ddDtPlanStartDate" data-role="datepicker"
                     placeholder="Plan Start Date" style="width:180px" data-bind="value: planStartDate"/>
            </div>
 <div class="projectDocContainerDesConf" >
            <div class="labelProjectDoc">Plan Finish Date<span style="color:red">*</span></div>
                <input name="Plan Finish Date" data-format="dd MMM yyyy" required="required" id="<%: this.ID %>_ddDtPlanFinishDate" data-role="datepicker" 
                    placeholder="Plan Finish Date" style="width:180px" data-bind="value: planFinishDate"/>
        </div>
<div class="projectDocContainerDesConf" id="actualStartContainer" >
            <div class="labelProjectDoc">Actual Start Date</div>
             <input id="<%: this.ID %>_ddDtActualStartDate" data-role="datepicker"
             data-bind="value: actualStartDate" data-format="dd MMM yyyy"/>
        </div>

<div class="projectDocContainerDesConf" id="actualFinishContainer" >
    <div class="labelProjectDoc">Actual Finish Date</div>
    <input id="<%: this.ID %>_ddDtActualFinishDate" data-role="datepicker" 
    data-bind="value: actualFinishDate" data-format="dd MMM yyyy" />
</div>
<%--<div class="projectDocContainerDesConf" >
    <div class="labelProjectDoc">Status</div><div class="labelProjectDoc"><input name="Status" required='required' id="cbStatus" data-role="combobox"
            data-placeholder="select Status"
            data-value-primitive="true"
            data-text-field="text"
            data-value-field="value"
            data-bind="
                        source: StatusSource,
                        value: status,
                    "
    style="width: 250px" /></div>
</div>--%>
<div class="projectDocContainerDesConf" >
<div class="labelProjectDoc">Remark</div>
    <div class="labelProjectDoc" style= "margin: 0px; padding-left:0px;">    
        <textarea class=" k-textbox textArea" name="remark"/></textarea>
    </div>
</div>
</script>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 200px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
        margin-top: 0px;
        margin-bottom: 0px;
        padding-top: 3px;
        vertical-align: top;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .projectDocContainerDesConf{
        padding-bottom: 10px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }

div.k-edit-form-container
{
    width: auto;
}
</style>


<div id="<%: this.ID %>_FormContent">
    <div>
        <div id="<%: this.ID %>_grdDocuments" 
            data-role="grid" 
            data-bind="source: DesignerDwgSource, events: { edit: editProjectDoc, dataBound: onDataBound, save: saveDoc }"
            data-columns="[
            {
               field: 'keyDeliverablesName', title: 'Milestone',
               width: 95
            },
            { 
                field: 'docNo', title: 'Doc No & Title', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_document-template').html()),
                width: 200
            },
            { 
                field: 'projectDocumentReference', title: 'Project Document Reference', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_design-template').html()),
                width: 200
            },
            {   
                field:'disciplineName()', title: 'Discipline & Drawing', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_diciplineDrawing-template').html()),
                width: 150 
            },            
              { 
                field: 'plan', title: 'Planned', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_planned-template').html()),
                width: 100
            },
            { 
                field: 'actual', title: 'Actual', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_actual-template').html()),
                width: 100
            },
            { 
                field: 'status', title: 'Status', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_status-template').html()),
                width: 100
            },
            {
                field:'documentByName', title: 'Engineer & Remark', filterable: false,
                attributes: { style: 'vertical-align: top !important;' }, 
                template: kendo.template($('#<%: this.ID %>_designerRemark-template').html())               
            }
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity))
            { %>,
            {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}},{name:'destroy'}] }
            <% } %>
            ]" 
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity))
            { %>
            data-toolbar= "[{name: 'create', text: 'Add New Design Conformity'}]",
            <% } %>
            data-editable= "{mode: 'popup', confirmation:false, window:{width: 500, title: 'Design Conformity', modal: true}, template:kendo.template($('#<%: this.ID %>_addDDTemplate').html()) }",
            data-filterable="true"
            data-pageable="true", 
            data-sortable="true"
            ></div>
 
    </div>
    <div id="<%: this.ID %>_popWeightPopUp">
        <uc1:DCWeightHoursControl1 runat="server" ID="DCWeightHours" />
    </div>
    <div id="<%: this.ID %>_popFinalResultPopUp">
        <uc1:DCDataInputFinalResultControl runat="server" ID="DCDataInputFinalResultControl" />
    </div>
</div>
<script id="<%: this.ID %>_phase-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        keyDeliverables:
    </div>
    <div>
        #= data. name #
    </div>  
</script>
<script id="<%: this.ID %>_document-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        No:
    </div>
    <div>
        #= data.docNo #
    </div>
   <div class='subColumnTitle'>
       Title:
    </div>
    <div>
        #= data.docTitle.substring(0,2) == "\#\#"? data.docTitle.substring(2):data.docTitle #
    </div>
</script>
<script id="<%: this.ID %>_design-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Doc No:
    </div>
    <div>
        # if (data.projectDocumentReferences) { #
            # for (var i = 0; i < data.projectDocumentReferences.length; i++) { #
                #: data.projectDocumentReferences[i].projectDocRef.docNo #;
            # } #
        # } #
    </div>
   <div class='subColumnTitle'>
       Doc Title:
    </div>
    <div>
        # if (data.projectDocumentReferences) { #
            # for (var i = 0; i < data.projectDocumentReferences.length; i++) { #
                #: data.projectDocumentReferences[i].projectDocRef.docTitleText #;
            # } #
        # } #
    </div>
</script>
<script id="<%: this.ID %>_diciplineDrawing-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Discipline:
    </div>
    <div>
        #= data.disciplineName() ? data.disciplineName() : '-' #
    </div>
    <div class='subColumnTitle'>
        Number of Drawing:
    </div>
    <div> 
        #= data.drawingCount ? data.drawingCount : '-'# 
    </div>
</script>
<script id="<%: this.ID %>_planned-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.planStartDate ? kendo.toString(data.planStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.planFinishDate ? kendo.toString(data.planFinishDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Rev:
    </div>
    <div> 
        #= data.revision ? data.revision : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_actual-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.actualStartDate ? kendo.toString(data.actualStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.actualFinishDate ? kendo.toString(data.actualFinishDate,'dd MMM yyyy') : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_status-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Status:
    </div>
    <div> 
        #= data.statusText #  
    </div>
    <div class='subColumnTitle'>
        Hours(s):
    </div>
    <div> 
    <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DCWeightHours))
    { %>
        <a href="\#" data-bind="events: { click: openWeightPopUpEdit }">#: data.weightHours ? data.weightHours : "N/A" #</a>
    <% } else { %>
        #: data.weightHours ? data.weightHours : "N/A" #
    <% } %>
    </div>
    <div class='subColumnTitle'>
        Parameter:
    </div>
    <div> 
    <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignerProduction))
    { %>
        <a href="\#" data-bind="events: { click: openFinalResult }">#= data.parameterValue ? kendo.toString(data.parameterValue, "n") : "N/A"#</a>
    <% } else { %>
        #: data.parameterValue ? data.parameterValue : "N/A" #
    <% } %>
    </div>
</script>
<script id="<%: this.ID %>_designerRemark-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Engineer:
    </div>
    <div> 
        #= data.documentByName ? data.documentByName : '-'#  
    </div>
    <div class='subColumnTitle'>
        Remark:
    </div>
    <div> 
        #= data.remark ? data.remark : '-'#
    </div>
</script>

<script id="<%: this.ID %>_remark-template" type="text/x-kendo-template">
    #= remark?remark.replace(/(\r?\n)/gi,"<br>"):"" #
</script>
<script>
    var <%: this.ID %>_popWeightPopUp, <%: this.ID %>_popFinalResultPopUp;
    var curEngPos;
    var <%: this.ID %>_designerDwgSource = new kendo.data.DataSource({
            transport: {
                create: {
                    type: "POST", dataType: "json",
                    url: _webApiUrl + "ProjectDocuments/postProjectDocumentObjectBadgeNo/" + _currBadgeNo
                    },
                read    : { dataType: "json",
                    url: _webApiUrl + "ProjectDocuments/listPDByProjectIdDocType/" + _projectId + "?docType=DC"
                    },
                update: {
                    type: "PUT", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "ProjectDocuments/putProjectDocumentObject/" + e.id;
                    }

                },
                destroy: {
                    type: "DELETE", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "ProjectDocuments/" + e.id;
                    }

                },
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        var ret = $("#<%: this.ID %>_ddDocNo").getKendoMultiSelect().value();
                        console.log(ret);
                        var _projectDocumentReferences = [];
                        if (ret.length > 0) {
                            for (var i = 0; i < ret.length; i++) {
                                _projectDocumentReferences.push({ projectDocRefId: ret[i] });
                            }
                        }

                        options.cutOfDate = kendo.toString(options.cutOfDate, 'MM/dd/yyyy');
                        options.planStartDate = kendo.toString(options.planStartDate, 'MM/dd/yyyy');
                        options.planFinishDate = kendo.toString(options.planFinishDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.actualStartDate = kendo.toString(options.actualStartDate, 'MM/dd/yyyy');;
                        options.actualFinishDate = kendo.toString(options.actualFinishDate, 'MM/dd/yyyy');
                        options.projectId = _projectId;
                        options.projectDocumentReferences = _projectDocumentReferences;
                        options.documentById = _currBadgeNo;
                        options.documentByName = common_getEmployeeSName(_currBadgeNo);
                        options.documentByEmail = common_getEmployeeSEmail(_currBadgeNo);
                        options.docType = "DC";
                        options.docNo = $("#<%: this.ID %>_docNo").val();
                        options.docTitle = "##"+$("#<%: this.ID %>_dcTitlePrefix").val().trim() + " - " +
                            $("#<%: this.ID %>_dcTitlePrefix2").val().trim() + " " +
                            $("#<%: this.ID %>_dcTitlePrefix3").val().trim() + " " + options.docTitle;
                        options.createdBy = _currNTUserID;
                        options.updatedBy = _currNTUserID;
                        options.projectDocumentWeightHours = [];
                        return options;
                    }
                    if (operation == "update") {
                      var ret = $("#<%: this.ID %>_ddDocNo").getKendoMultiSelect().value();
                        console.log(ret);
                        var _projectDocumentReferences = [];
                        if (ret.length > 0) {
                            for (var i = 0; i < ret.length; i++) {
                                _projectDocumentReferences.push({ projectDocRefId: ret[i] });
                            }
                        }
                        //options.status = 
                        options.cutOfDate = kendo.toString(options.cutOfDate, 'MM/dd/yyyy');
                        options.planStartDate = kendo.toString(options.planStartDate, 'MM/dd/yyyy');
                        options.planFinishDate = kendo.toString(options.planFinishDate, 'MM/dd/yyyy');
                        options.actualStartDate = kendo.toString(options.actualStartDate, 'MM/dd/yyyy');
                        options.actualFinishDate = kendo.toString(options.actualFinishDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.project = null;
                        options.projectId = _projectId;
                        options.projectDocumentReferences = _projectDocumentReferences;
                        options.assignmentType = "DC";
                        options.docNo = $("#<%: this.ID %>_docNo").val();
                        if (!$("#<%: this.ID %>_dcTitlePrefix").is(':hidden')) {
                            options.docTitle = "##" + $("#<%: this.ID %>_dcTitlePrefix").val().trim() + " - " +
                                $("#<%: this.ID %>_dcTitlePrefix2").val().trim() + " " +
                                $("#<%: this.ID %>_dcTitlePrefix3").val().trim() + " " + $("#<%: this.ID %>_docTitle").val().trim();
                        }
                        options.updatedBy = _currNTUserID;
                        options.projectDocumentWeightHours = [];
                        return options;
                    }
                }
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        docType: { type: "string" },
                        docDesc: { type: "string" },
                        docNo: { type: "string" },
                        docTitle: { type: "string" },
                        drawingNo: { type: "string" },
                        drawingTitle: { type: "string" },
                        drawingCount: { type: "number" },
                       // projectDocumentReferences: { type: "object" },
                        cutOfDate: { type: "date", defaultValue: null },
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null},
                        actualFinishDate: { type: "date", defaultValue: null},
                        disciplinedrawingNo: { type: "string" },
                        status: { type: "string" },
                        remark: { type: "string" },
                        discipline: { type: "string" },
                        statusdrawingNo: { type: "string" },
                        remarkdrawingNo: { type: "string" },
                        executordrawingNo: { type: "string" },
                        executorAssignmentdrawingNo: { type: "string" },
                        progress: { type: "number" },
                        documentById: { type: "string" },
                        documentByName: { type: "string" },
                        documentByEmail: { type: "string" },
                        documentTrafic: { type: "string" },
                        revision: { type: "number" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                    disciplineName: function () {
                        if (this.discipline)
                            return getDiscipline(this.discipline);
                        else return "";
                    }
                },
                parse: function (d) {
                    if (d.length) {
                        $.each(d, function (key, value) {
                            if (value.status)
                                value.statusText = getStatus(value.status);

                            $.each(value.projectDocumentReferences, function (key2, value2) {
                                if (value2.projectDocRef) {
                                    var docTitle = value2.projectDocRef.docTitle;
                                    var t1 = docTitle != null ? docTitle.split("-") : "";
                                    var docTitle1 = t1[0];
                                    var docTitle2 = "";
                                    var disciplineID = "";
                                    var docTitle3 = "";
                                    if (t1[1]) {
                                        var t2Temp = t1[1];
                                        var t2 = t2Temp.split(" ");
                                        docTitle2 = value2.projectDocRef.ewpDocType;
                                        disciplineID = value2.projectDocRef.position;
                                        t2.shift();
                                        if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                                        docTitle3 = t2.join(" ");
                                        value2.projectDocRef.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                                    } else {
                                        value2.projectDocRef.docTitleText = value2.projectDocRef.docTitle;
                                    }
                                }
                            });
                        });
                    } else {
                        $.each(d.projectDocumentReferences, function (key2, value2) {
                            if (value2.projectDocRef) {
                                var docTitle = value2.projectDocRef.docTitle;
                                var t1 = docTitle.split("-");
                                var docTitle1 = t1[0];
                                var docTitle2 = "";
                                var disciplineID = "";
                                var docTitle3 = "";
                                if (t1[1]) {
                                    var t2Temp = t1[1];
                                    var t2 = t2Temp.split(" ");
                                    docTitle2 = value2.projectDocRef.ewpDocType;
                                    disciplineID = value2.projectDocRef.position;
                                    t2.shift();
                                    if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                                    docTitle3 = t2.join(" ");
                                    value2.projectDocRef.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                                } else {
                                    value2.projectDocRef.docTitleText = value2.projectDocRef.docTitle;
                                }
                            }
                        });
                        if (d.status)
                            statusText = getStatus(d.status);
                    }

                    if (d == "Employee ID Exist") {
                     //   <%: this.ID %>_designerDwgSource.read();
                        alert("Process failed: "+d);
                        return 0;
                    } else{
                        return d;
                       // <%: this.ID %>_designerDwgSource.read();
                    }
                },
            },
        pageSize: 10,
        change: function (e) {
        }
        });

    var <%: this.ID %>_projectDocSource = new kendo.data.DataSource({
        transport: {
            read: {
                async: false,
                dataType: "json",
                url: _webApiUrl + "ProjectDocuments/listPDByProjectIdDocType/" + _projectId + "?docType=EWP"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    docNo: { type: "string" },
                    docTitle: { type: "string" },
                    planStartDate: { type: "date" },
                    planFinishDate: { type: "date" },
                    actualStartDate: { type: "date" },
                    actualFinishDate: { type: "date" }
                }
            },
            parse: function (response) {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].planFinishDate) {
                        var planFinsihDateEwp = kendo.parseDate(response[i].planFinishDate, "yyyy-MM-ddTHH:mm:ss");
                        planFinsihDateEwp.setDate(planFinsihDateEwp.getDate() - 5);
                        response[i].planFinishDateEwp = planFinsihDateEwp;
                    }
                    var docTitle = response[i].docTitle;
                    var t1 = docTitle != null ? docTitle.split("-") : "";
                    //$("#docTitle1").val(t1[0]);
                    if (t1[1]) {
                        var t2Temp = t1[1];
                        var t2 = t2Temp.split(" ");
                      //  $("#docTitle2").getKendoDropDownList().value(t2[0]);
                        var t2Text = t2[0] == _useReportCode ? curEngPos + "R" : curEngPos;
                        t2.shift();
                        if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                        var dt3 = t2.join(" ");
                        //$("#docTitle3").val(t2.join(" "));
                        response[i].docTitle = t1[0] + " - " + response[i].ewpDocType + " - " + response[i].position + " - " + dt3;
                    } else {
                    //    $("#docTitle1").width(500);
                    //    $("#docTitle1").prop("disabled", false);
                    //    $("#docTitle2").getKendoDropDownList().value("");
                    //    $("#docTitle3").val("");
                    //    $("#docTitle2").closest(".k-widget").hide();
                    //    $("#docTitle1stStrip").hide();
                    //    $("#docTitle3").hide();
                    //    $("#disciplineTitle").hide();
                    }

                }
                return response;
            }
        }
    });

    var <%: this.ID %>_projectManagementModuleSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: _webApiUrl + "keydeliverables/listKeyDByProjectIdActive/" + _projectId,
                //url: _webApiUrl + "KeyDeliverables/listKDByProjectId/" + _projectId,
                dataType: "json"
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null },
                        weightPercentage: { type: "number" }
                    }
                },
                parse: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        if (response[i].planFinishDate) {
                            var planFinsihDateDc = kendo.parseDate(response[i].planFinishDate, "yyyy-MM-ddTHH:mm:ss");
                            planFinsihDateDc.setDate(planFinsihDateDc.getDate() - 5);
                            response[i].planFinishDateDc = planFinsihDateDc;
                        }
                    }
                    return response;
                }

            }
        }

    });

     var <%: this.ID %>_discipline = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/discipline"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_engineeringSource = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                cache: false,
                url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1?assignmentType=PD|TSD&PrID=" + _projectId
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    employeeId: { type: "string" },
                    employeeName: { type: "string" },
                    employeeEmail: { type: "string" }
                }
            }
        }
    });

    var <%: this.ID %>_UserDiscipline;
    var <%: this.ID %>_viewModel = new kendo.observable({
        isEnabledRefEwpTitle: false,
        planStartDate: null,
        planFinishDate: null,
        projectDocumentReferenceList: [],
        documentById: "",
        documentByName: "",
        documentByEmail: "",
        DesignerDwgSource: <%: this.ID %>_designerDwgSource,
        DisciplineSource: <%: this.ID %>_discipline,
        ProjectManagementModuleSource: <%: this.ID %>_projectManagementModuleSource,
        EngineeringSource:<%: this.ID %>_engineeringSource,
        ProjectDocSource:<%: this.ID %>_projectDocSource,
        saveDoc: <%: this.ID %>_saveDoc,
        StatusSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: _webApiUrl + "lookup/findbytype/statusEngineer"
                },
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        text: { type: "string" },
                        value: { type: "string" },
                    }
                }
            }
        }),
        saveDoc: <%: this.ID %>_saveDoc,
        editProjectDoc: <%: this.ID %>_EditProjectDocument,
        openDiscipline: OpenDiscipline,
        engineerChange: EngineerChange,
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();

            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if ((_roleInProject == "engineer") && (_currBadgeNo != model.documentById)) {
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();
                }
            });
        },
        openWeightPopUpEdit: function (e) {
            DCWeightHours_localData = [];
            this.openWeightPopUp(e);
        },
        openWeightPopUp: function (e) {
            $("#<%: this.ID %>_popWeightPopUp").prevUntil("k-window-titlebar").find(".k-window-title").html(_projectNo + " - " + _projectDesc);

            DCWeightHours_viewWeightHoursModel.set("no", e ? e.data.docNo : "-");
            DCWeightHours_viewWeightHoursModel.set("title", e ? e.data.docTitle : "-");
            DCWeightHours_docId = e ? e.data.id : 0;
            DCWeightHours_viewWeightHoursModel.set("visibleTitle", e ? true : false);

            DCWeightHours_docType = "DC";
            DCWeightHours_bind();
            kendo.bind($("#DCWeightHours_ConstructionMonitoringReportContent"), DCWeightHours_viewWeightHoursModel);
            DCWeightHours_viewWeightHoursModel.dsTreeListWeightListSource.read();
            DCWeightHours_viewWeightHoursModel.set("otherUpdate",
                function (data) {
                    kendo.ui.progress($("#DCWeightHours_grdWeightLIst"), true);
                    
                    if (DCWeightHours_docId) {
                        $.ajax({
                            url: "<%:WebApiUrl%>" + "ProjectDocumentWeightHours/totalWeightByDocID/" + DCWeightHours_docId,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                var dataEwp = <%: this.ID %>_designerDwgSource.get(DCWeightHours_docId);
                                dataEwp.set("weightHours", data)
                                kendo.ui.progress($("#DCWeightHours_grdWeightLIst"), false);
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#DCWeightHours_grdWeightLIst"), false);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#DCWeightHours_grdWeightLIst"), false);
                    }
                });
            <%: this.ID %>_popWeightPopUp.center().open();

        },
        openFinalResult: function (e) {
            DCDataInputFinalResultControl_viewFinalResultModel.set("no", e.data.docNo);
            DCDataInputFinalResultControl_viewFinalResultModel.set("title", e.data.docTitle);
            DCDataInputFinalResultControl_viewFinalResultModel.set("participationValue", e.data.participationValue)
            DCDataInputFinalResultControl_docId = e.data.id;
            DCDataInputFinalResultControl_docType = "DC";
            DCDataInputFinalResultControl_docBadgeNo = e.data.documentById;
            DCDataInputFinalResultControl_bind();

            $.ajax({
                url: "<%:WebApiUrl%>" + "ProjectDocuments/getCustomProjectDocumentByOid/" + DCDataInputFinalResultControl_docId,
                async: false,
                type: "GET",
                success: function (data) {
                    DCDataInputFinalResultControl_viewFinalResultModel.set("remarksActualResults", data.remarksActualResults);
                    DCDataInputFinalResultControl_viewFinalResultModel.set("remarksActualResultsTmp", data.remarksActualResults);
                    DCDataInputFinalResultControl_viewFinalResultModel.set("remarksDesignKpi", data.remarksDesignKpi);
                    DCDataInputFinalResultControl_viewFinalResultModel.set("remarksDesignKpiTmp", data.remarksDesignKpi);
                },
                error: function (jqXHR) {
                }
            });

            kendo.bind($("#DCDataInputFinalResultControl_ConstructionMonitoringReportContent"), DCDataInputFinalResultControl_viewFinalResultModel);

            DCDataInputFinalResultControl_viewFinalResultModel.dsFinalResultSource.read();
            DCDataInputFinalResultControl_viewFinalResultModel.set("otherUpdate",
                function (data) {
                    kendo.ui.progress($("#DCDataInputFinalResultControl_grdTypeOfDrawingLIst"), true);
                    
                    if (DCDataInputFinalResultControl_docId) {
                        $.ajax({
                            url: "<%:WebApiUrl%>" + "ProjectDocumentFinalResults/totalFinalResultByDocID/" + DCDataInputFinalResultControl_docId,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                var dataDWG = <%: this.ID %>_designerDwgSource.get(DCDataInputFinalResultControl_docId);
                                console.log(dataDWG);
                                dataDWG.set("parameterValue", data)
                                kendo.ui.progress($("#DCDataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#DCDataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#DCDataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                    }
                });
            $("#<%: this.ID %>_popFinalResultPopUp").prevUntil("k-window-titlebar").find(".k-window-title").html(_projectNo + " - " + _projectDesc + " (Final Result)");
            <%: this.ID %>_popFinalResultPopUp.center().open();
        }
    });
 
    function <%: this.ID %>_saveDoc(e) {
        if ((e.model.planFinishDate < e.model.planStartDate) && (e.model.planFinishDate!=null)) {
                e.preventDefault();
                alert("Plan Finish Date cannot start earlier than Plan start date");
            }
        if ((e.model.actualFinishDate < e.model.actualStartDate) && (e.model.actualFinishDate != null)) {
                e.preventDefault();
                alert("Actual Finish Date cannot start earlier than Actual start date");
            }
        }

    function  <%: this.ID %>_EngineerChange(e) {
        e.data.documentById = e.sender.dataSource.data()[e.sender.selectedIndex].employeeId;
        e.data.documentByName = e.sender.dataSource.data()[e.sender.selectedIndex].employeeName;
        e.data.documentByEmail = e.sender.dataSource.data()[e.sender.selectedIndex].employeeEmail;
        e.sender.dataSource._data[e.sender.selectedIndex].dirty = true;
        <%: this.ID %>_viewModel.set("documentById", e.data.documentById);
        <%: this.ID %>_viewModel.set("documentByName", e.data.documentByName);
        <%: this.ID %>_viewModel.set("documentByEmail", e.data.documentByEmail);
    }

    function <%: this.ID %>_procEditProjectDocument(e, d, pos) {
        var dataItm = e;
        var pDNoRefList = [], pDTitleRefList = [];
        if (dataItm.model.projectDocumentReferences) {
            if (dataItm.model.projectDocumentReferences.length > 0) {
                for (var j = 0; j < dataItm.model.projectDocumentReferences.length; j++) {
                    if (dataItm.model.projectDocumentReferences[j].projectDocRef) {
                        pDNoRefList.push(dataItm.model.projectDocumentReferences[j].projectDocRef.id);
                        pDTitleRefList.push(dataItm.model.projectDocumentReferences[j].projectDocRef.docTitle);
                    }
                }
            }
        }
        //dataItm.model.set("projectDocumentReferenceList", pDNoRefList);
        //console.log("pDNoRefList");
        //console.log(pDNoRefList);
        <%: this.ID %>_viewModel.projectDocumentReferenceList = pDNoRefList;
        $("#<%: this.ID %>_ddDocNo").getKendoMultiSelect().value(pDNoRefList);
        $("#<%: this.ID %>_ddDocTitle").getKendoMultiSelect().value(pDNoRefList);
        $("#<%: this.ID %>_ddphase").on("change", function (e) {
            $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().value(null);
            $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().value(null);
            var multi = $("#<%: this.ID %>_ddphase").getKendoDropDownList(),
                multiDataItems = multi.dataItem();

            if (multiDataItems.planStartDate != null || multiDataItems.planFinishDate != null)
            {
                if (multiDataItems.planStartDate != null) {
                    $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().min(multiDataItems.planStartDate);
                    if (multiDataItems.planFinishDate != null) {
                        $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().max(multiDataItems.planFinishDate);
                        $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().max(multiDataItems.planFinishDate);
                    }
                };
            } else {
                if (_planFinishDate) {
                    $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().max(_planFinishDate);
                    $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().max(_planFinishDate);
                }
            }
        });

        $("#<%: this.ID %>_ddDocNo").on("change", function (e) {

            //var dataItem = this.dataItem(e.item.index());
            var multi = $("#<%: this.ID %>_ddDocNo").getKendoMultiSelect(),
                multiDataItems = multi.dataItems(),
                 SelectedDocTitle = [];
            for (var i = 0; i < multiDataItems.length; i += 1) {
                var currentDocNo = multiDataItems[i];
                SelectedDocTitle.push(
                    currentDocNo.docTitle
                )
            }

            $("#<%: this.ID %>_ddDocTitleSpan").text(SelectedDocTitle.join());

            //        $("#<%: this.ID %>_ddDocTitle").val(dataItm.model.docTitle);
            dataItm.model.docTitle = dataItm.model.docTitle;
            dataItm.model.dirty = true;
        });
        if (e.model.isNew()) {
            $("#<%: this.ID %>_ddDtActualStartDateContainer").hide();
            $("#<%: this.ID %>_ddDtActualFinishDateContainer").hide();
            $("#<%: this.ID %>_ddUcEngineeringContainer").hide();
            $("#<%: this.ID %>_ddRdExecutorContainer").hide();
            if (pos) {
                $("#<%: this.ID %>_docNo").val(_projectNo + "-" +<%: this.ID %>_UserDiscipline.pos + "DCXXX");
                $("#<%: this.ID %>_dcTitlePrefix").val(_projectDesc);
                $("#<%: this.ID %>_dcTitlePrefix2").val(" DC ");
                $("#<%: this.ID %>_dcTitlePrefix3").val(" " +<%: this.ID %>_UserDiscipline.posName + " ");
            }
            else {
                $("#<%: this.ID %>_docNo").val("");
                $("#<%: this.ID %>_dcTitlePrefix").val(_projectDesc);
                $("#<%: this.ID %>_dcTitlePrefix2").val(" DC");
                $("#<%: this.ID %>_dcTitlePrefix3").val(" ## ");
            }
            <%: this.ID %>_discipline.read().then(function(e){
                var discId = $.grep(<%: this.ID %>_discipline.data(), function (e) {
                    return e.text === <%: this.ID %>_UserDiscipline.posName;
                });
            $("#<%: this.ID %>_ddlDiscipline").getKendoDropDownList().value(discId[0].value);
            })
            $(".k-window-title").html("Add");
            $("#actualStartContainer").hide();
            $("#actualFinishContainer").hide();
            $("#autoInfo").remove();
            $('<br/><div class="labelProjectDoc">&nbsp;</div><sup id="autoInfo" style="color: red">XXX will be replaced with sequence number</sup>').insertAfter($("#<%: this.ID %>_docNo"));

        } else {
            $("#<%: this.ID %>_ddDtActualStartDateContainer").show();
            $("#<%: this.ID %>_ddDtActualFinishDateContainer").show();
            $("#<%: this.ID %>_ddUcEngineeringContainer").show();
            $("#<%: this.ID %>_ddRdExecutorContainer").show();
            $(".k-window-title").html("Edit");
            $("#<%: this.ID %>_ddDocTitleSpan").text(pDTitleRefList.join());
            $("#<%: this.ID %>_ddDocTitle").val(e.model.docTitle);

            $("#<%: this.ID %>_disciplineText").val(getDiscipline(e.model.discipline));
            $("#<%: this.ID %>_disciplineTextSpan").text(getDiscipline(e.model.discipline));
            
            var m = e.model.docTitle.split("-");
            var prf = m[0];
            var prf2 ="";
            var prf3 = "";
            var prf4 = "";
            if (prf.substring(0, 2) == "##") {
                prf = prf.substring(2);
                prf2 = m[1].split(" ")[1];
                prf3 = m[1].split(" ")[2];
                for (mm = 3; mm < m[1].split(" ").length; mm++) {
                    var prf4 = prf4 + " " + m[1].split(" ")[mm];
                }
                $("#<%: this.ID %>_dcTitlePrefix").show();
                $("#<%: this.ID %>_dcTitlePrefix2").show();
                $("#<%: this.ID %>_dcTitlePrefix3").show();
                $("#<%: this.ID %>_dcTitleStrip").show();
                $("#<%: this.ID %>_dcTitlePrefix").val(prf);
                $("#<%: this.ID %>_dcTitlePrefix2").val(prf2);
                $("#<%: this.ID %>_dcTitlePrefix3").val(prf3);
                $("#<%: this.ID %>_docTitle").val(prf4);
            }
            else { //dc title lama
                $("#<%: this.ID %>_dcTitlePrefixDiv").hide();
                $("#<%: this.ID %>_dcTitlePrefix2Div").hide();
                $("#<%: this.ID %>_dcTitlePrefix3Div").hide();
                $("#<%: this.ID %>_dcTitleStrip").hide();
                
            }
        }

        if (_planStartDate) {
            $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().min(_planStartDate);
            $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().min(_planStartDate);
        }

        if (_planFinishDate) {
            $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().max(_planFinishDate);
            $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().max(_planFinishDate);
        }

        $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().bind("change", function () {
            $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().min(this.value());
        });

        $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().bind("change", function () {
            $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().max(this.value());
        });

        if (e.model.planFinishDate)
            $("#<%: this.ID %>_ddDtPlanStartDate").getKendoDatePicker().max(e.model.planFinishDate);
        if (e.model.planStartDate)
            $("#<%: this.ID %>_ddDtPlanFinishDate").getKendoDatePicker().min(e.model.planStartDate);

        if (e.model.id) {
          $("#<%: this.ID %>_ddDtActualStartDate").getKendoDatePicker().bind("change", function () {
                $("#<%: this.ID %>_ddDtActualFinishDate").getKendoDatePicker().min(this.value());
            });

            $("#<%: this.ID %>_ddDtActualFinishDate").getKendoDatePicker().bind("change", function () {
                $("#<%: this.ID %>_ddDtActualStartDate").getKendoDatePicker().max(this.value());
            });

            if (e.model.actualFinishDate)
                $("#<%: this.ID %>_ddDtActualStartDate").getKendoDatePicker().max(e.model.actualFinishDate);
            if (e.model.actualStartDate)
                $("#<%: this.ID %>_ddDtActualFinishDate").getKendoDatePicker().min(e.model.actualStartDate);
        }
        //if (this.ProjectDocSource.data().length == 0) this.ProjectDocSource.read();
        //if (e.model.docNo) {
        //}
    }

    function <%: this.ID %>_getCurEngPosition(e) {
        var pos = null;
        var that = e;
        if (e.model.isNew()) {
            $.ajax({
                url: "<%: WebApiUrl %>" + "ProjectDocuments/getEngineerPosInProject/" + _projectId + "?badgeNo=" + _currBadgeNo,
                type: "GET",
                async: false,
                success: function (result) {
                    if (!result) {
                        _showDialogMessage("Error Message", "You are not listed as engineer<br/>Please add yourself as engineer in Assign Engineer and Designer Tab", "");
                        $('#<%: this.ID %>_grdDocuments').data("kendoGrid").cancelChanges();
                    } else {
                        <%: this.ID %>_UserDiscipline = result;
                        <%: this.ID %>_procEditProjectDocument(e, that, result);
                        curEngPos = result.pos;
                        return result;
                    }
                },
                error: function (error) {
                }
            });
        } else {
            <%: this.ID %>_procEditProjectDocument(e, that, null)
        }

    }

    function <%: this.ID %>_EditProjectDocument(e) {
        var pos = <%: this.ID %>_getCurEngPosition(e);

    }
    function  <%: this.ID %>_getStatusEngineer(stat) {
        var status = "";
        $.ajax({
            url: "<%: WebApiUrl %>" + "lookup/FindByTypeAndValue/statusEngineer|" + stat,
            type: "GET",
            async: false,
            success: function (result) {
                if (result.length > 0)
                    status = $.trim(result[0].text);
            },
            error: function (error) {
            }
        });
        return status;
    }

    function  <%: this.ID %>_OpenDiscipline(e) {
        var listContainer = e.sender.list.closest(".k-list-container");
        listContainer.width(listContainer.width() + kendo.support.scrollbar());
    }

    $(document).ready(function () {

        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel);

        <%: this.ID %>_viewModel.ProjectManagementModuleSource.read();

        <%: this.ID %>_popWeightPopUp = $("#<%: this.ID %>_popWeightPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 900,
            height: 500
        }).data("kendoWindow");

        <%: this.ID %>_popFinalResultPopUp = $("#<%: this.ID %>_popFinalResultPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc + " (Final Result)",
            modal: true,
            visible: false,
            resizable: false,
            width: 900,
            height: 500
        }).data("kendoWindow");

    });
</script>