﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportToDateProgressSummaryControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportToDateProgressSummaryControl" %>
<div id="<%: this.ID %>_reportToDateProgressSummaryContainer">
    <div >
        <div style="font-weight:bold">
            <div style="text-align: center">To Date Progress Summary Chart</div>
            <div style="text-align: center" data-bind="html:taskName"></div>
            <div style="text-align: center">Position <span data-bind="html: position"></span></div>
        </div>
        <br />
        <br />
        <br />
        <div style="height:600px"            
            data-role="chart"
            data-value-axis="[{
                    max: 100,
                    majorTicks: {size: 10, width: 2}, 
                    minorTicks: {size: 5, width: 2, visible: true}
            }]"
            data-transitions="false"
            data-bind="source: dsToDateProgressSummary"
            data-series="[{type: 'rangeColumn', fromField: 'baselinePctComplete', colorField: 'color', toField: 'actualPctComplete', labels: {visible: true, template:'Test', from: {template: '#=value.from# %'}, to: {template: '#=value.to# %'}}},
                        {type: 'line', field: 'baselinePctComplete', name: 'Baseline % Complete', color: 'yellow'},
                        {type: 'line', field: 'actualPctComplete', name: 'Actual % Complete', color: 'blue'}]"
            data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}"
            data-chart-area="{background: 'lightgray'}">
        </div>
        <br />
        <br />
        <div data-role="grid" data-bind="source: dsToDateProgressSummary" data-scrollable="false"
            data-columns="[{ template: '#= ++<%: this.ID %>_vm.record #', title:'No', width: '5%', attributes: {style: 'text-align: right'} }, 
                        { field: 'taskName', title: 'Project / Task Name', width: '40%' },
                        { title: '% Complete', headerAttributes: {style: 'text-align: center'} , columns: [
                            { field: 'actualPctComplete', title: 'Actual', attributes: {style: 'text-align: right'} },
                            { field: 'baselinePctComplete', title: 'Baseline', attributes: {style: 'text-align: right'} },
                            { field: 'aheadeOrDelay', title: 'Ahead / Delay', width: '120px',  attributes: {style: 'text-align: right'} }]
                        },
                        { field: 'durationHour', title: 'Duration (hour)', format: '{0:n2}', attributes: {style: 'text-align: right'} },
                        { field: 'totalSlackHour', hidden: true, title: 'Total Slack (Hours)', template: '#: totalSlackHour == null ? \'NA\' : kendo.toString(totalSlackHour, \'n2\') #', attributes: {style: 'text-align: right'} },
                        { field: 'freeSlackHour', title: 'Free Slack (Hours)', width: '100px', template: '#: freeSlackHour == null ? \'NA\' : kendo.toString(freeSlackHour, \'n2\') #', attributes: {style: 'text-align: right'} },
                        { field: 'optimisDiff', title: 'Potential Ahead / Delay Optimist (Hours)', width: '150px', format: '{0:n2}', attributes: {style: 'text-align: right'} },
                        { field: 'contractor', title: 'Contractor' }]">
        </div>
    </div>
</div>
<div  id="<%: this.ID %>_errorMessageContainer">
    <div data-bind="visible: notFound" style="top: 60px;width: 400px;text-align: center;margin: auto;position: relative;" >
        <div style="border:1px solid red;color:red;font-size:x-large;" data-bind="html: ErrMsg"></div>
    </div>
</div>

<script>
    var <%: this.ID %>_vm = kendo.observable({
        taskName: null,
        position: null,
        dsToDateProgressSummary: null,
        loaded:false,
        record: 0
    });
    
    var <%: this.ID %>_ervm = kendo.observable({
        notFound: false,
        ErrMsg: "No Data Found"
    });

    function <%: this.ID %>_populate(projectShiftID, taskUid, callback) {
        $.ajax({
            url: webApiUrl + "iptreport/reportdata?reportName=IPT_PROGRESS_TODATE_SUMMARY&projectshiftid=" + projectShiftID + "&taskUid=" + taskUid,
            success: function (result) {
                if (result != null) {
                    <%: this.ID %>_vm.set("taskName", result.taskName);
                    <%: this.ID %>_vm.set("position", kendo.toString(kendo.parseDate(result.position), 'dd MMM yyyy  hh:mm tt'));


                    if (result != null) {
                        for (var i = 0; i < result.list.length; i++) {
                            result.list[i]["color"] = result.list[i].aheadeOrDelay < 0 ? 'red' : 'green';
                            result.list[i]["durationHour"] = result.list[i].duration / 60 / 10;
                            result.list[i]["totalSlackHour"] = result.list[i].totalSlack == null ? null : result.list[i].totalSlack / 60 / 10;
                            result.list[i]["freeSlackHour"] = result.list[i].freeSlack == null ? null : result.list[i].freeSlack / 60 / 10;
                        }
                    }

                    <%: this.ID %>_vm.dsToDateProgressSummary = new kendo.data.DataSource({
                        data: result.list,
                        filter: { field: "actualPctComplete", operator: "neq", value: 100 }
                    });
                    
                    <%: this.ID %>_vm.set("loaded", true);
                    <%: this.ID %>_ervm.set("notFound", false);
                    status = "loaded";
                    kendo.bind($("#<%: this.ID %>_reportToDateProgressSummaryContainer"), <%: this.ID %>_vm);
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
                }

                else {
                    <%: this.ID %>_ervm.set("notFound", true);
                    status = "notfound";
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);                   
                }

                callback(status);
            },
            error: function (e) {
                var jsonResponse = e.responseJSON;
                //console.log(jsonResponse);
                <%: this.ID %>_ervm.set("ErrMsg", jsonResponse.ExceptionMessage);
                <%: this.ID %>_ervm.set("notFound", true);
                status = "Error";
                kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
            }
        });
    }

    function <%: this.ID %>_shortLabels(value) {
        return value;

        var result = "";
        while (value.length > 20) {
            var clipIndex = 20;
            while (value[clipIndex] != ' ')
                clipIndex--;

            result = result + (result != "" ? "\n" : "") + value.substring(0, clipIndex);

            value = value.substring(clipIndex);
        }
    
        result = result + (result != "" ? "\n" : "") + value;

        return result;
    }

</script>
<style>
.k-grid  .k-grid-header  .k-header  .k-link {
height: auto;
}
  
.k-grid  .k-grid-header  .k-header {
    white-space: normal;
}
</style>
