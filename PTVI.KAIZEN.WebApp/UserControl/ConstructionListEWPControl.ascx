﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConstructionListEWPControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ConstructionListEWPControl" %>
<%@ Register Src="~/UserControl/ConstructionMonitoringReportControl.ascx" TagPrefix="uc1" TagName="ConstructionMonitoringReportControl" %>


<script type="text/x-kendo-template" id="editButtonTemplate">
<a class="k-button k-button-icontext k-grid-activate"><span class="k-icon k-i-edit"></span>Reactivate</a>
</script>


<script type="text/x-kendo-template" id="radio-template">
    <div class="radioItemExecutor">
        <input class="k-radio" type="radio" name="rdExecutor" data-bind="attr:{name: tname, id: value, value: value}, checked:executor"/>
        <label class="k-radio-label" for="#: value #" data-bind="text: text">
        </label>
    </div>
</script>

<style>
    .labelProjectDoc {
        width: 100px;
        display: inline-block;
        padding-left: 10px;
    }

    .radioTemplate {
        width: 300px;
        display: inline-block;
        padding-left: 10px;
    }

    .radioItemExecutor {
        width: 85px;
        display: inline-block;
        padding-left: 10px;
    }

    .inputProjectDoc {
        width: 200px;
    }

    .projectDocContainer {
        width: 700px;
    }

    .textArea {
        width: 300px;
        height: 50px;
    }
</style>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div id="<%: this.ID %>_grdDocuments" data-role="grid" data-bind="source: ProjectDocSource, events: { dataBound: onDataBound}" 
            data-columns="[
            { 
                field: 'docNo', title: 'EWP Compliance No', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_document-template').html()),
                width: 200
            },
            { 
                field: 'planStartDate', title: 'Planned', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_planned-template').html()),
                width: 100, filterable: true
            },
            { 
                field: 'actualStartDate', title: 'Actual', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_actual-template').html()),
                width: 100, filterable: true
            },
            {field:'statusText', title: 'Status', template:'#= statusName() #', filterable: true, filterable: { ui: <%: this.ID %>_statusFilter } },
            {
                field:'documentByName', title: 'Engineer & Executor',
                attributes: { style: 'vertical-align: top !important;' }, filterable: true, 
                template: kendo.template($('#<%: this.ID %>_engineerExecutor-template').html())               
            },
            {field:'remark', title: 'Remark', filterable: true, template: kendo.template($('#<%: this.ID %>_remark-template').html())},
            {title: 'No', width: 125, filterable: false, template: kendo.template($('#<%: this.ID %>_No-template').html())}
            ]" 
            data-editable= false, 
            data-filterable="true"
            data-pageable="true", 
            data-sortable="true"
            ></div>
 
    </div>
    <div id="<%: this.ID %>_popConstructionPopUp">
        <uc1:ConstructionMonitoringReportControl runat="server" ID="ConstructionMonitoringReport" />
    </div>
</div>
<script id="<%: this.ID %>_document-template" type="text/x-kendo-template">

    <div class='subColumnTitle'>
        No:
    </div>
    <div>
        #= data.docNo #
    </div>
    <div class='subColumnTitle'>
            Title:
    </div>
    <div> 
        #= data.docTitleText # 
    </div>
    #if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {#
        <div> 
            #if (data.sharedFolder) {#
                <a href="file://#= data.sharedFolder#"  target="_explorer.exe">Share Folder</a>
            #}#
        </div>
    #} else { #
        <div> 
            #if (data.sharedFolder) {#
                <div class='subColumnTitle'>
                        Share Folder
                </div>
                <div>
                    #= data.sharedFolder#
                </div>
            #}#
        </div>
    #} #
</script>
<script id="<%: this.ID %>_planned-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.planStartDate ? kendo.toString(data.planStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.planFinishDate ? kendo.toString(data.planFinishDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Rev:
    </div>
    <div> 
        #= data.revision ? data.revision : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_remark-template" type="text/x-kendo-template">

    #= remark?remark.replace(/(\r?\n)/gi,"<br>"):"" #

</script>

<script id="<%: this.ID %>_No-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        PMO:
    </div>
    <div> 
        #= (data.pmoNo ? '<a href="\#" data-bind="events: { click: openConstructionMonitoringReportControl }">' + data.pmoNo + '</a>' : '-') # 
    </div>
    <div class='subColumnTitle'>
        Tender:
    </div>
    <div> 
        #= (data.tenderNo ? data.tenderNo : '-') # 
    </div>
    <div class='subColumnTitle'>
        Contract:
    </div>
    <div> 
        #= (data.contractNo ? '<a href="\#" data-bind="events: { click: openConstructionMonitoringReportControl }">' + data.contractNo + '</a>' : '-') # 
    </div>
</script>

<script id="<%: this.ID %>_actual-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.actualStartDate ? kendo.toString(data.actualStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.actualFinishDate ? kendo.toString(data.actualFinishDate,'dd MMM yyyy') : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_engineerExecutor-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Engineer:
    </div>
    <div> 
        #= data.documentByName #  
    </div>
    <div class='subColumnTitle'>
        Executor:
    </div>
    <div> 
        #= data.executor ? data.executor : '-' #
    </div>
</script>

<script>
    var <%: this.ID %>_popConstructionPopUp;

    var <%: this.ID %>_projectDocSource = new kendo.data.DataSource({
        requestStart: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), true);
        },
        requestEnd: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), false);
        },
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "ProjectDocuments/ConstructionListEWP/" + _projectId
            }
        },
        error: function (e) {
            _showDialogMessage("Error", e.xhr.responseText, "");
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    projectId: { type: "number" },
                    keyDeliverablesId: { type: "string" },
                    docType: { type: "string" },
                    docDesc: { type: "string" },
                    docNo: { type: "string" },
                    docTitle: { type: "string" },
                    docTitleText: { type: "string" },
                    drawingNo: { type: "string" },
                    drawingCount: { type: "number" },
                    cutOfDate: { type: "date", defaultValue: null },
                    planStartDate: { type: "date", validation: { required: true }, defaultValue: null },
                    planFinishDate: { type: "date", validation: { required: true }, defaultValue: null },
                    actualStartDate: { type: "date", validation: { required: true }, defaultValue: null },
                    actualFinishDate: { type: "date", validation: { required: true }, defaultValue: null },
                    disciplinedrawingNo: { type: "string" },
                    status: { type: "string", validation: { required: true } },
                    remark: { type: "string", validation: { required: true } },
                    discipline: { type: "string", validation: { required: true } },
                    statusdrawingNo: { type: "string" },
                    remarkdrawingNo: { type: "string" },
                    executordrawingNo: { type: "string" },
                    executorAssignmentdrawingNo: { type: "string" },
                    executor: { type: "string", validation: { required: true } },
                    progress: { type: "number" },
                    documentById: { type: "string", validation: { required: true } },
                    documentByName: { type: "string" },
                    documentByEmail: { type: "string" },
                    documentTrafic: { type: "string" },
                    revision: { type: "number" },
                    pmoNo: { type: "string" },
                    position: { type: "string" },
                    sharedFolder: { type: "string" },
                    ewpDocType: { type: "string" },
                    contractNo: { type: "string" },
                    tenderNo: { type: "string" },
                    createdDate: { type: "date" },
                    createdBy: { type: "string" },
                    updatedDate: { type: "date" },
                    updatedBy: { type: "string" },
                },
                statusName: function () {
                    if (this.status)
                        return <%: this.ID %>_getStatus(this.status);
                    else return "";
                },
                disciplineName: function () {
                    if (this.discipline)
                        return getDiscipline(this.discipline);
                    else return "";
                }
            },
            parse: function (d) {
                if (d.length) {
                    $.each(d, function (key, value) {
                        if (value.status)
                            value.statusText = <%: this.ID %>_getStatus(value.status);

                        var docTitle = value.docTitle;
                        var t1 = docTitle != null ? docTitle.split("-") : "";
                        var docTitle1 = t1[0];
                        var docTitle2 = "";
                        var disciplineID = "";
                        var docTitle3 ="";
                        if (t1[1]) {
                            var t2Temp = t1[1];
                            var t2 = t2Temp.split(" ");
                            docTitle2 = value.ewpDocType;
                            disciplineID = value.position;
                            t2.shift();
                            if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                            docTitle3 = t2.join(" ");
                            value.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                        } else {
                            value.docTitleText = value.docTitle;
                        }

                    });
                }

                var totEWP = 0, totEWPComp = 0;
//                if ($("#infoCountEWP")) $("#infoCountEWP").remove();
                if (d == "Duplicate Doc. No.") {
                    <%: this.ID %>_projectDocSource.read();
                    alert("Process failed: " + d);
                    return 0;
                } else {
                    if (d.constructor === Array) {
                        $.each(d, function (key, value) {
                            if (value.status == "S1") totEWPComp = totEWPComp + 1;
                            totEWP = totEWP + 1;
                        })
                        <%: this.ID %>_viewModel.set("totEWP", totEWP);
                        <%: this.ID %>_viewModel.set("totEWPComp", totEWPComp);
                        $("#<%: this.ID %>_grdDocuments .k-grid-pager").append("<div id='infoCountEWP'>&nbsp;Total EWP: " + <%: this.ID %>_viewModel.totEWP + " | Total EWP Completed: " +<%: this.ID %>_viewModel.totEWPComp + "</div>");
                    }
                    return d;
                    <%: this.ID %>_projectDocSource.read();
                }
            },
        },
        pageSize: 10,
        change: function (e) {
        }
    });

    var <%: this.ID %>_deliverable = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "keydeliverables/listkdbyprojectid/"+ _projectId,
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    name: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_discipline = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/discipline"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_status = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/statusiprom"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_executor = new kendo.data.DataSource({
        requestEnd: function (e) {
        },
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/executor"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_engineeringSource = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                cache: false,
                url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1?assignmentType=PE|TSE&PrID=" + _projectId
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    employeeId: { type: "string" },
                    employeeName: { type: "string" },
                    employeeEmail: { type: "string" }
                }
            }
        }
    });

    var <%: this.ID %>_title2Source = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                cache: false,
                url: _webApiUrl + "lookup/findbytype/ewpDocType"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_vmExecutor = new kendo.observable({
        ExecutorSource: <%: this.ID %>_executor,
        executor: "",
        tname: "rdExecutor",
//        rdExecutorChange: <%: this.ID %>_rdExecutorChange, 
//        $('input[name="rdExecutor"]').change(rdExecutorChange);

    });

    function <%: this.ID %>_winActivate() {
        $('input[name="rdExecutor"]').change(<%: this.ID %>_rdExecutorChange);
    }

    function <%: this.ID %>_rdExecutorChange() {
        if ($('input[name="rdExecutor"]:checked').val() == 'N/A') {
            $("#pmoNo").prop("required", false);
            $("#tenderNo").prop("required", false);
            $("#contractNo").prop("required", false);
            $("#visibleOnEditColumn").hide();
        }
        else if ($('input[name="rdExecutor"]:checked').val() == 'CS') {
            $("#pmoNo").prop("required", true);
            $("#tenderNo").prop("required", false);
            $("#contractNo").prop("required", false);
            $("#visibleOnEditColumn").show();
            $("#divPmoNo").show();
            $("#divPmContractNo").hide();
            $("#divTenderNo").hide();
        }
        else if ($('input[name="rdExecutor"]:checked').val() == 'CAS') {
            $("#pmoNo").prop("required", false);
            $("#tenderNo").prop("required", true);
            $("#contractNo").prop("required", true);
            $("#visibleOnEditColumn").show();
            $("#divPmoNo").hide();
            $("#divPmContractNo").show();
            $("#divTenderNo").show();
        }
    }

    function sharedFolderOption() {
        if ($("#dtActualFinishDate").getKendoDatePicker().value()) {
            $("#<%: this.ID %>_dtSharedFolderContainer").show();
            $("#<%: this.ID %>_dtSharedFolder").attr("required", true);
            //   dtSharedFolder
        } else {
            $("#<%: this.ID %>_dtSharedFolderContainer").hide();
            $("#<%: this.ID %>_dtSharedFolder").attr("required", false);
        }

    }

    function <%: this.ID %>_disciplineFilter(e) {
        setTimeout(function () {
            e.parent().prev().hide();
        });
        e.kendoDropDownList({
            dataSource: <%: this.ID %>_discipline,
            dataTextField: "text",
            dataValueField: "text",
            optionLabel: "- Select Status -"
        });
    }

    function <%: this.ID %>_statusFilter(e) {
        setTimeout(function () {
            e.parent().prev().hide();
        });
        e.kendoDropDownList({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        dataType: "json",
                        url: _webApiUrl + "lookup/findbytype/statusiprom"
                    },
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { type: "number" },
                            text: { type: "string" },
                            value: { type: "string" },
                        }
                    }
                }
            }),
            dataTextField: "text",
            dataValueField: "text",
            optionLabel: "- Select Status -"
        });
    }

    function <%: this.ID %>_getStatus(stat) {
        var status = "";
        $.ajax({
            url: "<%: WebApiUrl %>" + "lookup/FindByTypeAndValue/statusiprom|" + stat,
            type: "GET",
            async: false,
            success: function (result) {
                if (result.length > 0)
                    status = $.trim(result[0].text);
            },
            error: function (error) {
            }
        });
        return status;
    }

    function <%: this.ID %>_OpenDiscipline(e) {
        var listContainer = e.sender.list.closest(".k-list-container");
        listContainer.width(listContainer.width() + kendo.support.scrollbar());
    }

    var <%: this.ID %>_viewModel = new kendo.observable({
        planStartDate: null,
        planFinishDate: null,
        totEWP: 0,
        totEWPComp: 0,
        disciplineID: "",
        documentById: "",
        documentByName: "",
        documentByEmail: "",
        ProjectDocSource: <%: this.ID %>_projectDocSource,
        DisciplineSource: <%: this.ID %>_discipline,
        DeliverableSource: <%: this.ID %>_deliverable,
        EngineeringSource:<%: this.ID %>_engineeringSource,
        Title2Source: <%: this.ID %>_title2Source,
        StatusSource: <%: this.ID %>_status,
        openDiscipline: <%: this.ID %>_OpenDiscipline,
        visible: function (e) {
            return e.status === "S3" || e.status === "S5";
        },
        reactivate: function (e) {
            e.preventDefault();
            var tr = $(e.target).closest("tr");
            var data = this.dataItem(tr);
            data.status = null;
            data.dirty = true;
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            console.log(grid.dataSource.data());
            grid.dataSource.sync();
        },
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);
                if (_currRole != "ADM" && (_roleInProject == "engineer" && _currBadgeNo != model.documentById)) {
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();
                }

                if (model.status == "S5" || model.status == "S3") {
                    $(this).find(".k-grid-Reactivate").show();
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();

                }
            });
        },
        openConstructionMonitoringReportControl: function (e) {
            ConstructionMonitoringReport_tenderId = e.data.tenderId;
            kendo.bind($("#ConstructionMonitoringReport_ConstructionMonitoringReportContent"), ConstructionMonitoringReport_viewConstructionMonitoringReportModel);
            ConstructionMonitoringReport_viewConstructionMonitoringReportModel.dsTender();
            ConstructionMonitoringReport_viewConstructionMonitoringReportModel.refreshGrid();
            <%: this.ID %>_popConstructionPopUp.center().open();

        }
    });

    $(document).ready(function () {

        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel);

        <%: this.ID %>_popConstructionPopUp = $("#<%: this.ID %>_popConstructionPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 850,
            height: 500
        }).data("kendoWindow");
    });

</script>