﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EngineersOnTimesheetControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EngineersOnTimesheet" %>
<script>
    var gridRole;

    var <%: this.ID %>_pickEngineersOnTimesheetModel = kendo.observable({
            ds_engineer_list: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ucEngineerOnTimesheet"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ucEngineerOnTimesheet"), false);
            },
           transport: {
               read: _webApiUrl + "timesheet/listEngineerTimesheet/1"
            },
            schema: {
                model: {
                    id: "engineerid",
                    fields: {
                        engineername: { type: "text" }
                    }
                }
            }
        }),
        dataBound: function () {
            var grid = $("#<%: this.ID %>_EmployeeGrid").getKendoGrid();
            grid.expandRow(".k-master-row");
            $("a.k-icon.k-i-collapse").hide();
        },
        onSelect: function () {
            var result = [];

            var grid = $("#<%: this.ID %>_EmployeeGrid").data('kendoGrid');
            var allChildren = $(grid.element[0]).find('.isCheckMaster:checked')
            $.each(allChildren, function () {
                var detailRow = $(this).closest('tr');
                //var table = $(this).closest('div.k-grid');
                //var detailGrid = $(table).data('kendoGrid');
                var data = grid.dataItem(detailRow);
                result.push({ employeeId: data.engineerid, employeeName: data.engineername });
                
            });
            return result;
        },
        reloadGrid: function(valueEmployee) {
            var grid = $("#<%: this.ID %>_EmployeeGrid").data('kendoGrid');
            var allChildren = $(grid.element[0]).find('.isCheckMaster')
            var result = [];
            $.each(allChildren, function (key,value) {
                var detailRow = $(this).closest('tr');
                //var table = $(this).closest('div.k-grid');
                //var detailGrid = $(table).data('kendoGrid');
                var data = grid.dataItem(detailRow);
                if ($.inArray(data.engineerid,valueEmployee)>-1) {
                    result.push({ employeeId: data.engineerid, employeeName: data.engineername });
                    value.checked=true;
                } else
                    value.checked = false;
                 
                return result;
            });
        },
        pickEmployee: function (e) {
        }
    });

    function selectRowMaster() {
        var checkbox = $(this);
        var nextRow = checkbox.closest("tr").next(); // find the assosiated detail row
        //Note: the row should be expanded at least once as otherwhise there will be no child grid loaded      									
        if (nextRow.hasClass("k-detail-row")) {
            // and toggle the checkboxes
            nextRow.find(":checkbox")
                .prop("checked", checkbox.is(":checked"));
        }

    }
    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ucEngineerOnTimesheet"), <%: this.ID %>_pickEngineersOnTimesheetModel);
<%--        gridRole = $("#<%: this.ID %>_EmployeeGrid").getKendoGrid();
        gridRole.table.on("change", ".isCheckMaster", selectRowMaster);--%>

    });

</script>
<div id="<%: this.ID %>_ucEngineerOnTimesheet" class="adrContent">
    <fieldset class="gdiFieldset">
        <legend class="gdiLegend">Engineer Search</legend>
        <div id="<%: this.ID %>_EmployeeGrid" 
                    data-role="grid"
                    data-bind="source: ds_engineer_list, events: { dataBound: dataBound }",
                    data-pageable="false",
                    data-scrollable="false",
                    data-selectable="true",
                    data-columns='[
                        { template:  kendo.template($("#<%: this.ID %>_chkTemplate").html()), width: "50px" },
<%--                        { field: "engineerid", title: "Id" },--%>
                        { field: "engineername", title: "Name" }
                    ]'
                ></div>

        <script id="<%: this.ID %>_chkTemplate" type="text/x-kendo-tmpl">
            <input type='checkbox' class='isCheckMaster' />
        </script>

        <div style="float: right; margin-top: 12px;">
            <div
                data-role="button"
                data-bind="events: { click: pickEmployee }"
                class="k-button k-primary">
                Select
            </div>
        </div>

    </fieldset>

</div>
