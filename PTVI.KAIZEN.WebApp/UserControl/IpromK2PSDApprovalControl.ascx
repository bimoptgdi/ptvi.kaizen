﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IpromK2PSDApprovalControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.IpromK2PSDApprovalControl" %>
<%@ Register Src="~/UserControl/K2HistoryControl.ascx" TagPrefix="uc1" TagName="K2HistoryControl" %>

<div id="<%: this.ID %>_RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_iProMDetail">iProM Detail</legend>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 200px;" class="originatorLabel">
                        <span>Project No.</span>
                    </td>
                    <td style="width: 10px;">:
                    </td>
                    <td>
                        <span data-bind="text: requestData.project.projectNo"></span>
                    </td>
                </tr>
                <tr>
                    <td class="originatorLabel">
                        <span>Project Description</span>
                    </td>
                    <td>:
                    </td>
                    <td>
                        <span data-bind="text: requestData.project.projectDescription"></span>
                    </td>
                </tr>
                <tr>
                    <td class="originatorLabel">
                        <span>Project Type</span>
                    </td>
                    <td>:
                    </td>
                    <td>
                        <span data-bind="text: requestData.project.projectType"></span>
                    </td>
                </tr>
                <tr>
                    <td class="originatorLabel">
                        <span>Project Manager</span>
                    </td>
                    <td>:
                    </td>
                    <td>
                        <span data-bind="text: requestData.project.projectManagerName"></span>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_RequestDetail">Request Detail</legend>
            <table style="width: 100%;">
                <tr style="vertical-align: top;">
                    <td style="width: 200px;" class="originatorLabel">
                        <span>Description</span>
                    </td>
                    <td style="width: 10px;">:
                    </td>
                    <td>
                        <span data-bind="text: requestData.remarks, invisible: isSaveSubmit"></span>
                        <textarea id="<%: this.ID %>_requestDataRemarks" class="k-textbox" style="width: 100%;" data-bind="value: requestData.remarks, visible: isSaveSubmit" placeholder="Description"></textarea>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td class="originatorLabel">
                        <span>File</span>
                    </td>
                    <td>:
                    </td>
                    <td>
                        <span data-bind="source: requestData"  data-template="<%: this.ID %>_fileNamePsd-template"></span>
                        <div id="<%: this.ID %>_divFileNameId"><input name="fileName" id="<%: this.ID %>_fileNameId" type="file" /></div>

                    </td>
                </tr>                
            </table>
        </fieldset>
        <script type="x-kendo-template" id="<%: this.ID %>_fileNamePsd-template">
            #= data.refDocId ? '<a href="<%:DocServer%>getdoc.aspx?id=' + data.refDocId + '&op=n" target="_blank">' + data.fileNamePsd + '</a>' : '-' #
        </script>
    </div>

    <br />
    <uc1:K2HistoryControl runat="server" ID="K2HistoryControl" />
    <br />
    <div class="form-group form-group-sm">
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_Remark">Remark</legend>
            <textarea class="k-textbox" style="width: 100%;" data-bind="value: Remark" placeholder="Remark"></textarea>
        </fieldset>
    </div>
       
        
        <br />
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <div style="float: right;">
                    <div
                        data-role="button"
                        data-bind="events: { click: onApproveRequestClick }, visible: isApproval"
                        class="k-primary">
                        Approve
                            <span class="glyphicon glyphicon-ok"></span>
                    </div>
                </div>
                <div style="float: right;">&nbsp;</div>
                <div style="float: right;">
                    <div
                        data-role="button"
<%--                        data-bind="events: { click: onReviseRequestClick }, visible: isRiviseVisible">--%>
                        data-bind="events: { click: onReviseRequestClick }, visible: isApproval">
                        Revise
                            <span class="glyphicon glyphicon-remove"></span>
                    </div>
                </div>
                <div style="float: right;">&nbsp;</div>
                <div style="float: right;">
                    <div
                        data-role="button"
                        data-bind="events: { click: onRejectRequestClick }, visible: isApproval">
                        Reject
                            <span class="glyphicon glyphicon-remove"></span>
                    </div>
                </div>
                <div style="float: right;">&nbsp;</div>
                <div style="float: right;">
                    <div
                        data-role="button"
                        data-bind="events: { click: onCloseEvent }">
                        Close
                            <span class="glyphicon glyphicon-ban-circle"></span>
                    </div>
                </div>
                <div style="float: right;">&nbsp;</div>
                <div style="float: right;">
                    <div
                        data-role="button"
                        data-bind="events: { click: onSaveDraftClick }, visible: isSaveSubmit"
                        class="k-primary">
                        Save
                            <span class="glyphicon glyphicon-ok"></span>
                    </div>
                </div>
                <div style="float: right;">&nbsp;</div>
                <div style="float: right;">
                    <div
                        data-role="button"
                        data-bind="events: { click: onSubmitDraftClick }, visible: isSaveSubmit"
                        class="k-primary">
                        Revise
                            <span class="glyphicon glyphicon-ok"></span>
                    </div>
                </div>
            </div>
        </div>
        <br />
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        psdRequestID: -1,
        isApproval: false,
        isRiviseVisible: false,
        isSaveSubmit: false,
        parentForm: "",
        Remark: "",
        requestData: {},
        onValidationEntry: function () {
            var validatable = $("#<%: this.ID %>_RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validatable.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validatable.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }

            return isValid;
        },
        saveHandler: _webApiUrl + "upload/PostUploadPSD/1",
        onCloseEvent: function (e) {
        },
        responseState: "",
        onApproveRequestClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);
            if (confirm("Are you sure you want to approve this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "PSDApprovals/ApproveRequest/" + <%: this.ID %>_viewModel.get("psdRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        alert("This request is successfully approved");

                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_viewModel.onCloseEvent();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
            }
        },
        onRejectRequestClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);
            if (!<%: this.ID %>_viewModel.get("Remark")) {
                alert("Please insert remark before continue");
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                return;
            }
            if (confirm("Are you sure you want to reject this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "PSDApprovals/RejectRequest/" + <%: this.ID %>_viewModel.get("psdRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        alert("This request is successfully rejected");

                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_viewModel.onCloseEvent();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
            }
        },
        onReviseRequestClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);
            if (!<%: this.ID %>_viewModel.get("Remark")) {
                alert("Please insert remark before continue");
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                return;
            }
            if (confirm("Are you sure you want to revise this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "PSDApprovals/RequestNeedRevised/" + <%: this.ID %>_viewModel.get("psdRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        alert("This request is successfully revised");

                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_viewModel.onCloseEvent();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
            }
        },
        onSaveDraftClick: function () {
            if (confirm("Are you sure you save this request as Draft?")) {
                <%: this.ID %>_viewModel.set("responseState", "Saved");
                if ($("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles().length > 0) {
                    $("#<%: this.ID %>_fileNameId").getKendoUpload().upload();
                } else {
                    this.onUploadSuccess();
                }
            }
        },
        onSubmitDraftClick: function (e) {
            if (!<%: this.ID %>_viewModel.requestData.get("remarks")) {
                alert("Please Input Description!")
                $("#<%: this.ID %>_requestDataRemarks").focus();
                return;
            }
            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);

            if (<%: this.ID %>_viewModel.onValidationEntry()) {
                <%: this.ID %>_viewModel.set("responseState", "Revised");
                if (confirm("Are you sure you want to revise this request?")) {
                    if ($("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles().length > 0) {
                        $("#<%: this.ID %>_fileNameId").getKendoUpload().upload();
                    } else {
                        this.onUploadSuccess();
                    }
                }
            }

            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
        },
        onSelectAttachment: function (e) {
            if (<%: this.ID %>_viewModel.cekValidateLoader(e)) {
                alert("Upload pdf files only!")
                e.preventDefault();
            } 
        },
        cekValidateLoader: function (e) {
            var result = false;
            $.map(e.files, function (file) {
                if (file.validationErrors) {
                    if (file.validationErrors.length > 0) {
                        result = true;
                    }
                }
            });
            return result
        },
        onUploadSuccess: function (e) {
            if (<%: this.ID %>_viewModel.get("responseState") == "Saved") {
                var requestData = <%: this.ID %>_viewModel.requestData.toJSON();
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);
                $.ajax({
                    url: _webApiUrl + "PSDApprovals/" + <%: this.ID %>_viewModel.get("psdRequestID"),
                    type: "PUT",
                    data: requestData,
                    success: function (data) {
                        alert("Request has been saved");
                        <%: this.ID %>_viewModel.onCloseEvent();

                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                    }
                });
            } else if (<%: this.ID %>_viewModel.get("responseState") == "Revised") {

                var requestData = <%: this.ID %>_viewModel.requestData.toJSON();

                $.ajax({
                    url: _webApiUrl + "PSDApprovals/UpdateSubmitRevisi/" + <%: this.ID %>_viewModel.get("psdRequestID"),
                    type: "PUT",
                    data: requestData,
                    success: function (data) {
                        var request = {
                            Remark: <%: this.ID %>_viewModel.get("Remark")
                        }
                        $.ajax({
                            url: _webApiUrl + "PSDApprovals/RequestNeedRevised/" + <%: this.ID %>_viewModel.get("psdRequestID"),
                            type: "POST",
                            data: request,
                            success: function (data) {
                                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                                alert("This request is successfully revised");

                                <%: this.ID %>_viewModel.set("Remark", "");
                                <%: this.ID %>_viewModel.onCloseEvent();
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                                alert(jqXHR);
                            }
                        });

                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        alert(jqXHR);
                    }
                });
            }        //alert("This request is successfully " + <%: this.ID %>_viewModel.get("submitState"));
        },
        onUploadAtt: function (e) {

            e.data = { typeName: "psdApproval", typeDetail: "psdApproval", badgeNo: _currBadgeNo, typeId:  <%: this.ID %>_viewModel.get("psdRequestID") };
        },
        onErrorAttachment: function (e) {
            kendo.ui.progress($(".container-main"), false);
            var that = this;
            $("<div />").kendoDialog({
                title: false,
                closable: false,
                modal: true,
                content: "<div>File failed to upload to document center. But if there are another changes, they have been saved.</div><div>Please check your network connection and upload again.</div>",
                actions: [{
                    text: 'OK',
                    action: function (e) {
                        window.location = "<%: WebAppUrl%>IpromTaskListForm.aspx";
                        return true;
                    }
                }]
            }).data("kendoDialog").open();
        }
    });

    // Document Ready
    $(document).ready(function () {
        // Form Bind
        kendo.bind($("#<%: this.ID %>_RequestContent"), <%: this.ID %>_viewModel);

        if (!$("#<%: this.ID %>_fileNameId").getKendoUpload()) {
            $("#<%: this.ID %>_fileNameId").kendoUpload({
                async: {
                    saveUrl:  <%: this.ID %>_viewModel.saveHandler,
                    autoUpload: false
                },
                multiple: false,
                select: <%: this.ID %>_viewModel.onSelectAttachment,
                success:  <%: this.ID %>_viewModel.onUploadSuccess,
                upload:  <%: this.ID %>_viewModel.onUploadAtt,
                error:  <%: this.ID %>_viewModel.onErrorAttachment,
                validation: {
                    allowedExtensions: [".pdf"]
                }
            });
        }

    }); //doc ready

    function <%: this.ID %>_Init(dataRequest, parentForm, isApproval, isSaveSubmit) {

        if (isSaveSubmit)
            $("#<%: this.ID %>_divFileNameId").show();
        else
            $("#<%: this.ID %>_divFileNameId").hide();

        <%: this.ID %>_viewModel.set("Remark", "");
        <%: this.ID %>_viewModel.set("isApproval", isApproval);
        <%: this.ID %>_viewModel.set("isSaveSubmit", isSaveSubmit);

        if (dataRequest.k2CurrentActivityName == "PM Approval") {
            <%: this.ID %>_viewModel.set("isRiviseVisible", true);
        } else {
            <%: this.ID %>_viewModel.set("isRiviseVisible", false);
        }

        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        // jika dataRequest ada data nya maka update
        if (dataRequest) {
            var requestData =
            {
                id: dataRequest.id,
                //ewrNo: 
                project: {
                    projectNo: dataRequest.project.projectNo,
                    projectDescription: dataRequest.project.projectDescription,
                    projectManagerName: dataRequest.project.projectManagerName,
                    projectType: dataRequest.project.projectType
                },
                remarks: dataRequest.remarks,
                fileNamePsd: dataRequest.fileNamePsd,
                refDocId: dataRequest.refDocId,
                submittedDate: kendo.toString(dataRequest.submittedDate, 'yyyy-MM-dd'),
                approvalStatus: dataRequest.approvalStatus

            };

            <%: this.ID %>_viewModel.set("requestData", requestData);

            var histories = [];

            $.each(dataRequest.K2Histories, function (idx, elem) {
                if (elem.Status == "Completed") {
                    histories.push({
                        ActivityName: elem.ActivityName,
                        CreatedDate: kendo.toString(kendo.parseDate(elem.CreatedDate), "dd MMM yyyy hh:mm"),
                        Name: elem.Name,
                        ActionResult: elem.ActionResult,
                        Comment: elem.Comment
                    });
                }
            });
            K2HistoryControl_Init(histories);

        }
    }
</script>
<style>
    .originatorLabel{
        width: 110px;
        height: 23px;
        font-weight: bold;
    }

    #<%: this.ID %>_divFileNameId .k-clear-selected, #<%: this.ID %>_divFileNameId .k-upload-selected {
    display: none !important;
    }

</style>