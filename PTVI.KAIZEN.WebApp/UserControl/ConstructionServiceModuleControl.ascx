﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConstructionServiceModuleControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ConstructionServiceModuleControl" %>
<%@ Register Src="~/UserControl/IntegrateConstructionServiceControl.ascx" TagPrefix="uc1" TagName="IntegrateConstructionServiceControl" %>
<%--<%@ Register Src="~/UserControl/AssignExecutorEWPControl.ascx" TagPrefix="uc1" TagName="AssignExecutorEWPControl" %>--%>
<%@ Register Src="~/UserControl/ConstructionMonitoringListControl.ascx" TagPrefix="uc1" TagName="ConstructionMonitoringListControl" %>
<%@ Register Src="~/UserControl/ConstructionListEWPControl.ascx" TagPrefix="uc1" TagName="ConstructionListEWPControl1" %>

<div id="EngineeringServiceForm">
    <div class="k-content wide">
        <div>
            <div id="<%: this.ID %>_tabConstructionServiceModuleControl">
                <ul>
                    
<%--                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageEWPConstruction))
                    { %>
                    <li>Assign Executor for EWP</li>
                     <% }%>
--%>
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>
                    <li>Construction Service Report</li>
<%--                    <li>Contract List Report</li>--%>
                    <li>EWP List Construction</li>
                     <% }%>
                </ul>
<%--                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageEWPConstruction))
                    { %>
                <div>
                    <uc1:AssignExecutorEWPControl runat="server" ID="AssignExecutorEWP" />
                </div>
                     <% }%>--%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>
                <div>
                    <uc1:IntegrateConstructionServiceControl runat="server" ID="IntegrateConstructionService" />
                </div>
<%--                <div>
                    <uc1:ConstructionMonitoringListControl runat="server" ID="ConstructionMonitoringList" />
                </div>--%>
                <div>
                    <uc1:ConstructionListEWPControl1 runat="server" ID="ConstructionListEWPControl1" />
                </div>
                     <% }%>
            </div>
        </div>
    </div>
</div>

<script>

    var <%: this.ID %>_viewMainConstructionServiceModel = kendo.observable({
        selectedTabIndex: 0
    });


    // Tab control
    var <%: this.ID %>_tabConstructionServiceModuleControl = $("#<%: this.ID %>_tabConstructionServiceModuleControl").kendoTabStrip({
        tabPosition: "top",
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
    });
    <%: this.ID %>_tabConstructionServiceModuleControl = $("#<%: this.ID %>_tabConstructionServiceModuleControl").getKendoTabStrip();
    <%: this.ID %>_tabConstructionServiceModuleControl.select(0);

    kendo.bind($("#<%: this.ID %>_tabConstructionServiceModuleControl"), <%: this.ID %>_viewMainConstructionServiceModel);
</script>
