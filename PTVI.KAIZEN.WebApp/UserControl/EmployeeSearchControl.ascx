﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeSearchControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EmployeeSearchControl" %>
<script>
    var ucEmployeeSearch_pickHandler;
    var ucEmployeeSearch_grdEmployee;
    var ucEmployeeSearch_dataSource;

    function <%: this.ID %>_PopulateEmployeeData(filter, pickHandler) {
        ucEmployeeSearch_pickHandler = pickHandler;

        var ellipseparam = "<%: WebOdataUrl %>" + "EMPLOYEEs";
        ucEmployeeSearch_dataSource = new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            serverPaging: true,
            transport: {
                read: {
                    url: ellipseparam,
                    dataType: "json"
                },
                parameterMap: function (options, type) {
                    
                    if (type == "read") {
                        //console.log(data.filter.filters);
                        if (options.filter) {
                            options.filter.filters[0].value = options.filter.filters[0].value.toLowerCase();
                            options.filter.filters[0].field = "tolower(" + options.filter.filters[0].field + ")";
                        }
                    }

                    var newData = kendo.data.transports.odata.parameterMap(options, type);
                    delete newData.$format;
                    return newData;
                }
            },

            pageSize: 10,
            schema: {
                model: {
                    id: "employeeId",
                    fields: {

                        full_Name: "full_Name"
                    }
                },
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                }
            }

        });

        if (!ucEmployeeSearch_grdEmployee) {
            ucEmployeeSearch_grdEmployee = $("#ucEmployeeSearch_grdEmployee").kendoGrid({
                dataSource: ucEmployeeSearch_dataSource,
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                reorderable: true,
                resizable: true,                
                pageable: {
                    pageSizes: true,
                    buttonCount: 3
                },
                height: 400,
                columns: [
                    {
                        command: [
                            { name: "pick", text: "Select", click: ucEmployeeSearch_pickHandler }
                        ], title: "&nbsp;", width: "100px"
                    },
                    {
                        field: "employeeId", title: "Badge Number", width: "140px",
                        filterable: false
                    },
                    { field: "full_Name", title: "Name" }
                ],
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with"
                        }
                    }
                }
            }).data("kendoGrid");


        } else {
            // karena jarang ada perubahan, jika sudah search sekali ke adlist maka tidak usah search lagi
            //ucEmployeeSearch_grdEmployee.setDataSource(ucEmployeeSearch_dataSource);

        }

    }
</script>
<div id="ucEmployeeSearch" class="adrContent">
    <fieldset class="gdiFieldset">
        <legend class="gdiLegend">Employee Search</legend>
        <div id="ucEmployeeSearch_grdEmployee"></div>
    </fieldset>

</div>
