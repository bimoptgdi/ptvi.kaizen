﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectBudgetApprovalControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ProjectBudgetApprovalControl" %>
<%@ Register Src="~/UserControl/UploaderNewControl.ascx" TagPrefix="uc1" TagName="UploadControl" %>
<%@ Register Src="~/UserControl/IpromK2BudgetApprovalControl.ascx" TagPrefix="uc1" TagName="IpromK2BudgetApprovalControl" %>

<script id="editProjectBudgetApproval" type="text/x-kendo-template">
      <div style="padding:10px">            
        <table style="width: 620px;">
            <tr>
                <td style="width:100px">Budget Amount<span style="color:red">*</span></td>
                <td>
                    <input name="budgetAmount" required='true' validationMessage="Budget Amount is required" class="k-textbox" style="width: 250px" type="number" min="1" max="1000000000000000000" maxlength="300"/>
               </td>
            </tr>
            <tr>
                <td style="width: 85px; padding-top: 1px;">File Name<span style="color:red">*</span></td>#console.log(data)#
                <td id="<%: this.ID %>_tdFileNameId">
                    <%-- <input name="fileName" id="<%: this.ID %>_fileNameId" type="file" />--%>
                    <div id="<%: this.ID %>_tdFileNameText" style="display: inline-block"></div>
                    <div style="display: inline-block" data-role="button" onclick="showDetails()">
                        <span class="k-icon k-i-clip-45"></span>Upload
                    </div>                
                </td>
            </tr>
        </table>
    </div>            
</script>

    <div id="formProjectBudgetApproval">
    <div id="<%: this.ID %>_grdProjectBudgetApprovalControl"
                data-role="grid"
                data-bind="source: dsProjectBudgetApproval, events: {edit: onEditProjectBudgetApproval , save: onSaveProjectBudgetApproval}"
                data-filterable="true"
                data-pageable="buttonCount: 5"
                data-no-records="true"
                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ProjectBudgetApproval))
                { %>
                data-toolbar="[{name: 'create', text: 'Add Project Budget Approval'}]"
                <% } %>
                data-columns="[
                    { field: 'budgetAmount', title: 'Budget Amount', format: '{0:n0}',
                      width: 100,
                    },  
                    { field: 'fileName', title: 'File Name', width: 300, template: kendo.template($('#<%: this.ID %>_fileName-template').html()) },
                    { field: 'budgetStatus', title: 'Budget Status', width: 150, template: kendo.template($('#<%: this.ID %>_budgetStatus-template').html())},

                ]"
                data-mobile="true"
                data-editable= '{ "mode": "popup" , "template": kendo.template($("#editProjectBudgetApproval").html()), "createAt": "bottom" }'
        </div>
</div>

    <div id="<%: this.ID %>_popupBudgetApproval">
        <uc1:IpromK2BudgetApprovalControl runat="server" ID="IpromK2BudgetApprovalControlDetails" />
    </div>

<%--    <uc1:UploadControl runat="server" ID="UploadControl" />--%>
    <div id="<%: this.ID %>_popUpAttachments">
        <input type="file" id="<%: this.ID %>_fileNameId" name="fileNameId" required='true' validationMessage="File Name is required" />
    </div>

<style>
     #<%: this.ID %>_tdFileNameId .k-clear-selected, #<%: this.ID %>_tdFileNameId .k-upload-selected {
        display: none !important;
      }

      .k-clear-selected, .k-upload-selected {
        display: none !important;
      }
.tooltip {
        position: relative;
        display: inline-block;
        text-align: center;
        margin: 0 0 0 4px;
        cursor: pointer;
    }
    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        color: #fff;
        border-radius: 6px;
        width: 200px;
        text-align: left;
        padding-left: 10px;
        padding-right: 10px;
        background-color: #808080;
        padding-bottom: 10px;
             
        /* Position the tooltip text - see examples below! */
        position: absolute;
        top: -5px;
        right: 105%; 
        z-index: 999999;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .tooltip:hover {
        color: #fff;
        padding-right: 1px;
        padding-top: 1px;
        box-sizing: padding-box;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        -webkit-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        transition: all 0.3s linear;
        color: #fff;
        border: none;
    }
    .tooltip .tooltiptext::after {
        content: " ";
        position: absolute;
        top: 50%;
        left: 100%; /* To the right of the tooltip */
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent transparent transparent #808080;
    }
    .tooltip_wrapper {
        display: inline-block;
        width: 30px;
    }
    .hrStyle {
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -moz-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -ms-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -o-linear-gradient(left, #808080, #ffffff, #808080);
    }
</style>

<script type="text/x-kendo-template" id="<%: this.ID %>_approved-template">

# var approvedText = "In Progress"; #    
# if (data.reasonClosure == 'DEL') { approvedText = "Deleted" } else if (data.isApproved) { approvedText = "Approved" } #
<div>
    #: approvedText #
</div>

</script>
 <script type="text/x-kendo-template" id="<%: this.ID %>_fileName-template">
                #= data.refDocId ? '<a href="<%: DocServer%>getdoc.aspx?id=' + data.refDocId + '&op=n" target="_blank">' + data.fileName + '</a>' : '-' #
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_budgetStatus-template">
                #= data.budgetStatus ? '<a href="\#" onclick="<%: this.ID %>_onViewBudgetApproverClick(\''+data.id+'\');return false;">' + data.budgetStatus + '</a>' : "-" # #= data.budgetStatus == 'NeedRevision' ? '<a href="\#" onclick="<%: this.ID %>_gridEdit(\''+data.uid+'\');return false;">Edit</a>' : '' #
</script>

<script id="<%: this.ID %>_dateCreated" type="text/x-kendo-template">
    <div> #= createdDate? kendo.toString(kendo.parseDate(createdDate), "dd MMM yyyy"):'-' #</div>
</script>

<script type="text/javascript">
    var <%: this.ID %>_popupBudgetApproval;
    var <%: this.ID %>_projectBudgetApprovalModel = kendo.observable({
        dsProjectBudgetApproval: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#formProjectBudgetApproval"), true);
            },
            requestEnd: function (e) {
                if (e.type == "create" || e.type == "update") {
                    <%: this.ID %>_projectBudgetApprovalModel.set("curBudgetApprovalId", e.response.id);

                    if ($("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles().length > 0) {
                        $("#<%: this.ID %>_fileNameId").getKendoUpload().upload();
                        e.response.fileName = $("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles()[0].name;
                    } else {
                        $.ajax({
                            url: _webApiUrl + "ProjectBudgetApproval/registerToK2/" + <%: this.ID %>_projectBudgetApprovalModel.get("curBudgetApprovalId"),
                            type: "GET",
                            success: function (iPromData) {
                            },
                            error: function (jqXHR) {
                            }
                        });

                    }
                }
                kendo.ui.progress($("#formProjectBudgetApproval"), false);
            },
            transport: {
                read: {
                    url: function (e) {
                        return _webApiUrl + "ProjectBudgetApproval/getBudgetByProjectID/" + _projectId;
                    },
                    type: "GET"
                },
                create: { url: _webApiUrl + "ProjectBudgetApproval/addProjectBudgetApproval/1", type: "POST" },
                update: {
                    url: function (data) {
                        return _webApiUrl + "ProjectBudgetApproval/updateProjectBudgetApproval/" + data.id;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "ProjectBudgetApproval/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (options, operation) {
                    if (operation === "update" || operation === "create") {
                        //options.submittedDate = kendo.toString(options.submittedDate, "MM/dd/yyyy HH:mm:ssZ");
                        //options.createdDate = kendo.toString(options.createdDate, "MM/dd/yyyy HH:mm:ssZ");
                        options.project = null;
                        options.K2Histories = null;
                    }
                    return options;
                }
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number", defaultValue: _projectId },
                        budgetAmount: { type: "number", dafaultValue: 0, validation: { required: true, min: "0" } },
                        budgetStatus: { type: "string" },
                        fileName: { type: "string", validation: { required: true } },
                        documentId: { type: "number" }
                    }
                },
            },
        }),

        selectedFiles: [],
        saveHandler: _webApiUrl + "upload/PostUploadBudgetApproval/1",
        removeUrl: "",
        projectId: _projectId,
        fileTypeIndex: 0,
        selectedFileType: null, //dropdown file type
        curBudgetApprovalId: 0,
        onUploadAtt: function (e) {
            console.log("onUploadAttachment");
            e.data = { typeName: "BUDGETAPPROVAL", badgeNo: _currBadgeNo, typeId: <%: this.ID %>_projectBudgetApprovalModel.curBudgetApprovalId };
        },

        addFile: false,
        onAttachmentSelect: function (e) {
            if (<%: this.ID %>_projectBudgetApprovalModel.cekValidateLoader(e)) {
                alert("Upload pdf files only!")
                e.preventDefault();
            } else {
                $("#<%: this.ID %>_tdFileNameText").html(<%: this.ID %>_projectBudgetApprovalModel.getFileInfo(e));
            }
<%--           $("#<%: this.ID %>_tdFileNameText").html("");
            $("#<%: this.ID %>_tdFileNameText").html(<%: this.ID %>_projectBudgetApprovalModel.getFileInfo(e));--%>
        },
        cekValidateLoader: function (e) {
            var result = false;
            $.map(e.files, function (file) {
                if (file.validationErrors) {
                    if (file.validationErrors.length > 0) {
                        result = true;
                    }
                }
            });
            return result
        },
        onSuccessAtthment: function (e) {
            $.ajax({
                url: _webApiUrl + "ProjectBudgetApproval/registerToK2/" + <%: this.ID %>_projectBudgetApprovalModel.get("curBudgetApprovalId"),
                type: "GET",
                success: function (iPromData) {
                },
                error: function (jqXHR) {
                }
            });

            <%: this.ID %>_projectBudgetApprovalModel.dsProjectBudgetApproval.read();
        },
        onErrorAttachment: function (e) {
            <%--   kendo.ui.progress($(".container-main"), false);
            var that = this;
            $("<div />").kendoDialog({
                title: false,
                closable: false,
                modal: true,
                content: "<div>File failed to upload to document center. But if there are another changes, they have been saved.</div><div>Please check your network connection and upload again.</div>",
                actions: [{
                    text: 'OK',
                    action: function (e) {
                            window.location = "<%: WebAppUrl%>ProjectBudgetApprovalForm.aspx";
                        return true;
                    }
                }]
            }).data("kendoDialog").open();--%>
        },
        arrDelFiles: [],
        onEditProjectBudgetApproval: function (e) {
            var that = this;
            
            if (e.model.isNew() && e.model.employeeId == null) {
                $(".k-grid-update").closest('.k-widget.k-window').find(".k-window-title").html("Add Project Budget Approval");
                $(".k-grid-update").html('<span class="k-icon k-i-check"></span>Submit');
              if ($("#<%: this.ID %>_fileNameId").getKendoUpload()) $("#<%: this.ID %>_fileNameId").getKendoUpload().clearAllFiles();

            }
            else {
                $(".k-grid-update").closest('.k-widget.k-window').find(".k-window-title").html("Edit Project Budget Approval");
            }

            var editWindow = e.sender.editable.element.data("kendoWindow");
            editWindow.wrapper.css({ width: 700 });

            var editContainer = e.sender.editable.element.find(".k-edit-form-container");
            editContainer.css({ width: "100%" });
            e.model.dirty = true;
            if (e.model.id != "") {
                $("[name='PROJECTBUDGETAPPROVAL']").prop("disabled", true);
            }

            editWindow.center();
            $("#<%: this.ID %>_fileNameId").kendoUpload({
                multiple: false,
                async: {
                    saveUrl: <%: this.ID %>_projectBudgetApprovalModel.saveHandler,
                    autoUpload: false
                },
                error: <%: this.ID %>_projectBudgetApprovalModel.onErrorAttachment,
                select: <%: this.ID %>_projectBudgetApprovalModel.onAttachmentSelect,
                success: <%: this.ID %>_projectBudgetApprovalModel.onSuccessAtthment,
                upload: <%: this.ID %>_projectBudgetApprovalModel.onUploadAtt,
                validation: {
                    allowedExtensions: [".pdf"]
                }
            });
        },

        onSaveProjectBudgetApproval: function (e) {

            if (e.model.isNew() && e.model.employeeId == null) {
                
                if ($("#<%: this.ID %>_fileNameId").getKendoUpload().getFiles().length == 0) {
                    $("#<%: this.ID %>_tdFileNameId").append('<div class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" style="margin: 0.5em; display: block;" data-for="fileNameId" role="alert"><span class="k-icon k-i-warning"> </span>File Name is required<div class="k-callout k-callout-n"></div></div>');
                    //alert("Please Select Data Attachment!")
                    e.preventDefault()
                }
            }
        },

        onChange: function (e) {
            var that = this;
            if (that.value() == "ProjectBudgetApproval") {
                $(".trProjectBudgetApproval").show();
                $("#<%: this.ID %>_projectBudgetApproval").prop('required', true);
            }
            else {
                $(".trProjectBudgetApproval").hide();
                $("#<%: this.ID %>_projectBudgetApproval").prop('required', false);
            }
        },
        getFileInfo: function (e) {
            console.log(e);
            return $.map(e.files, function (file) {
                var info = file.name;

                //// File size is not available in all browsers
                //if (file.size > 0) {
                //    info += " (" + Math.ceil(file.size / 1024) + " KB)";
                //}
                return info;
            }).join(", ");
        }
    });

     function <%: this.ID %>_gridEdit(dataItem) {
        var that = $("#<%: this.ID %>_grdProjectBudgetApprovalControl").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem + "']")
        $("#<%: this.ID %>_grdProjectBudgetApprovalControl").getKendoGrid().editRow($(that));
    }

    function showDetails(e) {
        $('#<%: this.ID %>_fileNameId').click();
    }


      function <%: this.ID %>_onViewBudgetApproverClick(appID) {
        <%: this.ID %>_projectBudgetApprovalModel.set("k2State", "Read Details");

        $("#<%: this.ID %>_popupBudgetApproval").prevUntil("k-window-titlebar").find(".k-window-title").html("Project Sponsor Approval - Details");

        $.ajax({
            url: _webApiUrl + "ProjectBudgetApproval/" + appID,
            type: "GET",
            success: function (iPromData) {
                IpromK2BudgetApprovalControlDetails_viewModel.set("budgetRequestID", iPromData.id);
                IpromK2BudgetApprovalControlDetails_Init(iPromData, "#", false, false);
            },
            error: function (jqXHR) {
            }
        });


        IpromK2BudgetApprovalControlDetails_viewModel.set("onCloseEvent", function (e) { <%: this.ID %>_popupBudgetApproval.close(); });
        $(".form-group.form-group-sm").hide();
        <%: this.ID %>_popupBudgetApproval.center().open();
     }

<%--        function onChange() {
            var grid = $("#<%: this.ID %>_grdProjectBudgetApprovalControl").data("kendoGrid");
            grid.dataSource.read();
        }--%>


    $(document).ready(function () {
            <%: this.ID %>_projectBudgetApprovalModel.dsProjectBudgetApproval.read()
            kendo.bind($("#formProjectBudgetApproval"), <%: this.ID %>_projectBudgetApprovalModel);

          $("#<%: this.ID %>_popupBudgetApproval").kendoWindow({
            title: "Approval",
            modal: true,
            visible: false,
            resizable: false,
            width: "80%",
            height: "95%",
            activate: function () {
                // lookup bind

            }
        });
        <%: this.ID %>_popupBudgetApproval = $("#<%: this.ID %>_popupBudgetApproval").data("kendoWindow");

        <%: this.ID %>_popUpAttachments = $("#<%: this.ID %>_popUpAttachments").kendoWindow({
            title: "Add Attachment",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");
        });
    </script>
</asp:Content>