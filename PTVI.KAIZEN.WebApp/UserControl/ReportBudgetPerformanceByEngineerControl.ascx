﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportBudgetPerformanceByEngineerControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportBudgetPerformanceByEngineerControl" %>
<%@ Register Src="~/UserControl/EmployeeOnReportBudgetSearchControl.ascx" TagPrefix="uc1" TagName="EmployeeOnReportBudgetSearchControl" %>

<script>
    var <%: this.ID %>_chart, <%: this.ID %>_popPickEmployee;
    var <%: this.ID %>_viewReportProjectDistributionModel = kendo.observable({
        dsProjectDistribution: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#searchResultPanel"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#searchResultPanel"), false);
            },
            schema: {
                model: {
                    fields: {
                        engineers : {type: "string"},
                        estimationCost: { type: "number" },
                        actualCost: { type: "number" }
            }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var year = <%: this.ID %>_viewReportProjectDistributionModel.valueYear ? <%: this.ID %>_viewReportProjectDistributionModel.valueYear : 0;
                        return _webApiUrl + "dashboard/reportBudgetPerformancebyEngineer/" + year;
                    },
                    type: "POST",
//                    data: { engineers: <%: this.ID %>_viewReportProjectDistributionModel.valueEmployee.join(";") },
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type == "read") {
                        data.engineers = <%: this.ID %>_viewReportProjectDistributionModel.valueEmployee.join(";");
                        return data;
                    }
                }
            },
            sort: [{ field: "employeeId", dir: "asc" }]
        }),
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#searchResultPanel"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#searchResultPanel"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
        titleChart: "Report Budget Performance By Engineer ",
        valueYear: new Date().getFullYear(),
        dataEmployee: [],
        valueEmployee: [],
        onPickEmployeeClick: function (e) {
            var that = this;
            EmployeeOnReportBudgetSearch_pickEmployeeOnProjectReportBudgetModel.set("pickEmployee", function (e) {
                that.set("dataEmployee", EmployeeOnReportBudgetSearch_pickEmployeeOnProjectReportBudgetModel.onSelect());
                var empId = [];
                $.each(that.dataEmployee, function (index, value) {
                    empId.push(value.employeeId);
                });
                that.set("valueEmployee", empId);
                <%: this.ID %>_popPickEmployee.close();
            });
            // show popup
            <%: this.ID %>_popPickEmployee.center().open();
        },
        onSearchClick: function () {
            var that = this;
            this.dsProjectDistribution.read().then(function () {
                <%: this.ID %>_chart.options.title.text = that.titleChart + that.valueYear;
                var h = that.dsProjectDistribution.data().length * 20;
                if (h < 450) h = 450;
                $("#<%: this.ID %>_projectDistribution").height(h + 100);
                <%: this.ID %>_chart.refresh();
            });
        },
        onResetClick: function () {
            this.set("valueYear", new Date().getFullYear());
            this.set("valueEmployee", []);
        }

    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ReportProjectDistributionControl"), <%: this.ID %>_viewReportProjectDistributionModel);
        $("#<%: this.ID %>_projectDistribution").kendoChart({
            autoBind: false,
            //theme: "silver",
            title: {
                text: <%: this.ID %>_viewReportProjectDistributionModel.titleChart + <%: this.ID %>_viewReportProjectDistributionModel.valueYear
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_viewReportProjectDistributionModel.dsProjectDistribution,
            transitions: false,
            seriesColors: ["#943432", "#4F81BD"],
            seriesDefaults: {
                type: "bar"
            },
            series: [{ name: "Total Project Cost (USD)", field: "actualCost" }, { name: "Estimate Cost (USD)", field: "estimationCost" }],
            valueAxis: {
                labels: {
                    template: "#= kendo.format('{0:N0}', value) #",
                    rotation: -45
                }
            },
            categoryAxis: {
                field: "employeeName"
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= kendo.format('{0:N0}', value) #"
            }
        });

        <%: this.ID %>_popPickEmployee = $("#<%: this.ID %>_popPickEmployee").kendoWindow({
            title: "Operation List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");


        <%: this.ID %>_chart = $("#<%: this.ID %>_projectDistribution").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_ReportProjectDistributionControl">
    <div id="<%: this.ID %>_popPickEmployee">
       <uc1:EmployeeOnReportBudgetSearchControl runat="server" ID="EmployeeOnReportBudgetSearch" />
    </div>
    <fieldset>
        <legend>Search Criteria</legend>
        <table>
            <tr>
                <td>
                    Report Budget Year
                </td>
                <td>
                    <input data-role="combobox"
                            data-placeholder="Select Year"                            
                            data-text-field="year"
                            data-value-field="year"
                            data-bind="value: valueYear, source: dsYearProject"
                            style="width: 250px" />
                </td>
            </tr>
                    <tr>
                <td>
                    Engineer Name
                </td>
                <td><div style="width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_multiSelectEmployee" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Clik button to select Employee"
                        data-value-primitive="true"
                        data-text-field="employeeName"
                        data-value-field="employeeId"
                        data-readonly="true"
                        data-bind="value: valueEmployee, source: dataEmployee"></select></div>
                    <div data-role="button"
                        data-bind="events: { click: onPickEmployeeClick }"
                        style="display: inline-block;vertical-align: middle;"
                        class="k-button k-primary">
                        ...
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3"><br />
                    <div class="divCell" style="text-align:right">
                        <div
                            data-role="button"
                            data-bind="events: { click: onSearchClick }"
                            class="k-button k-primary">
                            <span class="k-icon k-i-filter"></span>
                            Search
                        </div>
                        <div
                            data-role="button"
                            data-bind="events: { click: onResetClick }"
                            class="k-button">
                            <span class="k-icon k-i-cancel"></span>
                            Reset
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset><br />
    <fieldset id="searchResultPanel">
        <legend>Search Result</legend>
        <div id="<%: this.ID %>_projectDistribution" style="width: 100%; height: 450px"></div>
    </fieldset>
</div>

