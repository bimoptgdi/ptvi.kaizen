﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportTechnicalSupportMainControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportTechnicalSupportMainControl" %>
<%@ Register Src="~/UserControl/ReportTechnicalSupportByEngineerControl.ascx" TagPrefix="uc1" TagName="ReportTechnicalSupportByEngineerControl" %>
<%@ Register Src="~/UserControl/ReportTSHours.ascx" TagPrefix="uc1" TagName="ReportTSHours" %>
    <div id="OtherDepartmentForm">
        <div class="k-content wide">
            <div>
                <div id="SendRequestTabStrip">
                    <ul>
                        <li>by Engineer</li>
                        <li>by Area</li>
                    </ul>
                    <div>
                        <uc1:ReportTechnicalSupportByEngineerControl runat="server" ID="ReportTechnicalSupportByEngineerControl" />
                    </div>
                    <div>
                        <uc1:ReportTSHours runat="server" ID="ReportTSHours1" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        var <%: this.ID %>_OtherDepartmentVM = kendo.observable({
            selectedTabIndex: 0
        });

        var onShow = function (e) {
            SendRequestTabStrip = $("#SendRequestTabStrip").getKendoTabStrip();
            if (SendRequestTabStrip.value() == "Request List") {
                $("#RequestList_grdRequestList").getKendoGrid().dataSource.read();
                $("#RequestList_grdRequestList").getKendoGrid().refresh();

            }
        };

            // Tab control
        var SendRequestTabStrip = $("#SendRequestTabStrip").kendoTabStrip({
                tabPosition: "top",
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
                show: onShow
            });
        SendRequestTabStrip = $("#SendRequestTabStrip").getKendoTabStrip();
        SendRequestTabStrip.select(0);

            // --Tab control
        kendo.bind($("#OtherDepartmentForm"), <%: this.ID %>_OtherDepartmentVM);
//        OnDocumentReady = function () {
//        }

    </script>
