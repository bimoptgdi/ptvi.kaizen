﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportOperatingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportOperatingControl" %>
<script>
    var <%: this.ID %>_chart;
    var <%: this.ID %>_reportOperatingProjectModel = kendo.observable({
        dsProjectDistribution: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportOperatingProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportOperatingProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        area: {type: "string"},
                        totalProject: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
//                        var year = < %: this.ID %>_reportOperatingProjectModel.valueYear ? < %: this.ID %>_reportOperatingProjectModel.valueYear : 0;
                        var yearFrom = kendo.toString(<%: this.ID %>_reportOperatingProjectModel.valueYearFrom, "dd MMM yyyy");
                        var yearTo = kendo.toString(<%: this.ID %>_reportOperatingProjectModel.valueYearTo, "dd MMM yyyy");
                        var area = <%: this.ID %>_reportOperatingProjectModel.area ? <%: this.ID %>_reportOperatingProjectModel.area: 0;
                        return _webApiUrl + "project/listOperatingProject/1?area=" + area + "&dateFrom=" + yearFrom + "&dateTo=" + yearTo;
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "area", dir: "asc" }]
        }),
        onChange: function() {
            this.dsProjectDistribution.read();
            <%: this.ID %>_chart.options.title.text = <%: this.ID %>_reportOperatingProjectModel.titleChart + " "
                + kendo.toString(this.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(this.valueYearTo, "dd MMM yyyy");
            <%: this.ID %>_chart.refresh();
        },
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportOperatingProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportOperatingProject"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
        titleChart: "Report Operating Project",
        valueYear: new Date().getFullYear(),
        valueYearFrom: new Date(),
        valueYearTo: new Date(),
        area: null,
    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ReportOperatingProject"), <%: this.ID %>_reportOperatingProjectModel);
        $("#<%: this.ID %>_projectOperating").kendoChart({
            title: {
                text: <%: this.ID %>_reportOperatingProjectModel.titleChart
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_reportOperatingProjectModel.dsProjectDistribution,
            transitions: false,
            seriesColors: ["#09094c", "Red", "Green", "Blue"],
            series: [{
                name: "Actual Done",
                type: "column",
                stack: "true",
                field: "actualDone",
            }, {
                name: "On Going",
                type: "column",
                stack: "true",
                field: "ongoing",
            }, {
                name: "Backlog",
                type: "column",
                stack: "true",
                field: "backLog",
            }, {
                name: "Plan Finish",
                type: "line",
                stack: "true",
                field: "target",
            }
            ],
            categoryAxis: {
                field: "area",
                labels: {
                    rotation: -45,
                }
            },
            tooltip: {
                visible: true,
            }
        });

        <%: this.ID %>_chart = $("#<%: this.ID %>_projectOperating").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_ReportOperatingProject">
    <div id="<%: this.ID %>_projectOperating" style="width: 500px; height: 400px;"></div>
</div>

