﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OtherDepartmentTaskControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.OtherDepartmentTaskControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>

<script>
    common_getAllStatusIprom();
    var <%: this.ID %>_popPickPIC;
    var <%: this.ID %>_viewOtherDepartmentTaskModel = kendo.observable({
        dsListProjectDocuments: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListOtherDepartmentTask"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListOtherDepartmentTask"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planDate: { type: "date", defaultValue: null },
                        actualDate: { type: "date", defaultValue: null },
                        remark: { type: "string"}
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {

                    <% if (_currRoleType == "0" || _currRoleType == "1" || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.OtherDeptTask))
    { %>
                        return _webApiUrl + "OtherDepartmentTasks/listOtheDeptTaskByProjectId/" + _projectId;
                    <% }
    else
    { %>
                        return _webApiUrl + "OtherDepartmentTasks/listOtheDeptTaskByBadgeNo/" + "<%: BadgeNo %>";
                    <% } %>
                    },
                    dataType: "json"
                },
                create: {
                    url: function (data) {
                        return _webApiUrl + "OtherDepartmentTasks/deptTaskAdd/1";
                    },
                    type: "POST"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "OtherDepartmentTasks/deptTaskUpdate/" + data.id;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "OtherDepartmentTasks/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        data.departmentName = $("#departmentId").getKendoComboBox().dataItem().TABLE_DESC;
                        //data.departmentName = <%: this.ID %>_viewOtherDepartmentTaskModel.dsDepartmentSource.get(data.departmentId).TABLE_DESC;
                        data.planDate = kendo.toString(data.planDate, 'yyyy-MM-dd');
                        data.actualDate = kendo.toString(data.actualDate, 'yyyy-MM-dd');
                    }

                    if (type == "create") {
                        data.projectId = _projectId;
                    }
                    return data;
                }
            },
            pageSize: 10,
            sort: [{ field: "actualDate", dir: "asc" }, { field: "planDate", dir: "asc" }]
        }),
        onEdit: function (e) {
            //e.model.set("dirty", true);
            if (e.model.isNew()) {
                $(".k-window-title").html("Add Other Department Task");
                $(".k-grid-update").html('<span class="k-icon k-i-check"></span>Save');
            }
            else
                $(".k-window-title").html("Edit Other Department Task");

            var that = this;
            editModel = e.model;
            var autocomplete = $("#<%: this.ID %>_employeeTmp").data("kendoAutoComplete");
            if (autocomplete) {
                autocomplete.value("");

                autocomplete.bind("change", function () {
                    editModel.dirty = true;

                    if (that.employeeSelected) {
                        if (!that.employeeSelected.employeeId) {
                            that.set("employeeSelected", null);
                        }
                    }
                    editModel.set("picId", that.employeeSelected ? that.employeeSelected.employeeId : null);
                    editModel.set("picName", that.employeeSelected ? that.employeeSelected.full_Name : null);
                    editModel.set("picEmail", that.employeeSelected ? that.employeeSelected.email : null);
                });
            }

            if (_planStartDate) {
                $("#<%: this.ID %>_planDate").getKendoDatePicker().min(_planStartDate);
            }

            if (_planFinishDate) {
                $("#<%: this.ID %>_planDate").getKendoDatePicker().max(_planFinishDate);
            }

            that.set("employeeSelected", {
                employeeId: e.model.picId,
                full_Name: e.model.picName,
                email: e.model.picEmail
            });

            <%--$("#<%: this.ID %>_btnVendorSelectedName").on("click", function () {
                $("#<%: this.ID %>_popPickPIC_wnd_title.k-window-title").html("PIC List");

                otherDeptTask_PickPIC_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                    var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));

                    $("#<%: this.ID %>_picId").val(pickedEmployee.employeeId);
                    $("#<%: this.ID %>_picName").val(pickedEmployee.full_Name);
                    $("#<%: this.ID %>_picEmail").val(pickedEmployee.email);

                    <%: this.ID %>_popPickPIC.close();
                });
                otherDeptTask_PickPIC_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                    <%: this.ID %>_popPickPIC.close();
                });
                // show popup
                <%: this.ID %>_popPickPIC.center().open();
                otherDeptTask_PickPIC_DataBind();
            });--%>

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        onSave: function (e) {
<%--            e.model.set("picId", $("#<%: this.ID %>_picId").val());
            e.model.set("picName", $("#<%: this.ID %>_picName").val());
            e.model.set("picEmail", $("#<%: this.ID %>_picEmail").val());--%>

            var isValid = true;

            if (!e.model.get("picName")) {
                //_showDialogMessage("PIC", "Please select the PIC!", "");
                isValid = false;
            }

            if (!isValid)
                e.preventDefault();
        },
        dsStatusSource: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "lookup/findbytype/" + _statusIprom
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        }),
        dsDepartmentSource: new kendo.data.DataSource({
            transport: {
                read: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_DEPARTMENT_EX"
            },
            schema: {
                model: {
                    id: "TABLE_CODE",
                    fields: {
                        TABLE_DESC: "TABLE_DESC",
                        value: {},
                        text: {}
                    }
                }
            }
        }),
        getEmployeeFromServer: function (options) {
            var searchBadgeNo = options.data.filter.filters[0].value;
            var result = [];
            if (searchBadgeNo.length >= 3) {
                //var filter = "?$filter=substringof('" + searchBadgeNo + "',employeeId) or substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var filter = "?$filter=substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var result = [];
                $.ajax({
                    url: _webOdataUrl + "EMPLOYEEs" + filter,
                    type: "GET",
                    async: false,
                    success: function (data) {
                        result = data.value;
                    },
                    error: function (data) {
                        //return [];
                    }
                });
            }
            return result;
        },
        dsEmployee: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                    options.success(<%: this.ID %>_viewOtherDepartmentTaskModel.getEmployeeFromServer(options));
                }
            }
        }),
        employeeSelected: null
    });

    $(document).ready(function () {
        <%: this.ID %>_popPickPIC = $("#<%: this.ID %>_popPickPIC").kendoWindow({
            title: "PIC List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        kendo.bind($("#<%: this.ID %>_OtherDepartmentTaskContent"), <%: this.ID %>_viewOtherDepartmentTaskModel);
        <%: this.ID %>_viewOtherDepartmentTaskModel.dsListProjectDocuments.read();
    });

</script>

<div id="<%: this.ID %>_OtherDepartmentTaskContent">
    <div id="<%: this.ID %>_grdListOtherDepartmentTask"
        <% if (_currRoleType == "0" || _currRoleType == "1" || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.OtherDeptTask))
        { %>
        data-toolbar="[{'name': 'create', 'text': 'Add Task'}]"
        <% } %>
        data-role="grid"
        data-sortable="true"
        data-editable='{"mode" : "popup", "template": kendo.template($("#<%: this.ID %>_editGrdListOtherDepartmentTask").html()), "confirmation" : "Are you sure you want to delete this Department?"}'
        data-columns="[
        <% if (!(_currRoleType == "0" || _currRoleType == "1" || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.OtherDeptTask)))
        { %>
            {
                field: 'projectNo', title: 'Project No',
                headerAttributes: { style: 'textdepartmentNamealign: center; vertical-align: middle !important;' },
                width: 100
            },
            {
                field: 'projectDesc', title: 'Project Desc',
                headerAttributes: { style: 'textdepartmentNamealign: center; vertical-align: middle !important;' },
                width: 200
            },
        <% } %>
            {
                field: 'departmentName', title: 'Department',
                headerAttributes: { style: 'textdepartmentNamealign: center; vertical-align: middle !important;' },
                width: 200
            },
            {
                field: 'taskName', title: 'Task',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                width: 200
            },
            {
                field: 'picName', title: 'PIC',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                width: 110
            },
        <% if (_currRoleType == "0" || _currRoleType == "1" || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.OtherDeptTask))
        { %>
            {
                field: 'planDate', title: 'Plan',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; ' },
                template: '#: planDate ? kendo.toString(planDate, \'dd MMM yyyy\') : \'-\'#',
                width: 70
            },
            {
                field: 'actualDate', title: 'Actual',
                headerAttributes: { style: 'text-align: center; ' },
                attributes: { style: 'text-align: center; ' },
                template: kendo.template($('#<%: this.ID %>_actualDate-template').html()), 
                width: 70
            },
        <% }
        else
        { %>
            {
                title: 'Date',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: left; ' },
                template: kendo.template($('#<%: this.ID %>generalUserDate-template').html()), 
                width: 70
            },
        <% } %>
              {
                field: 'remark', title: 'Remark',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                width: 70
            },
            {
                field: 'status', title: 'Status',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; ' },
                template: '#:status ? common_getStatusIpromDesc(status) : \'\'#',
                width: 70
            },
            {
                title: ' ',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; ' },
                template: kendo.template($('#<%: this.ID %>_trafic-template').html()),
                width: 40
            },
            { 
                field: ' ', 
                command: ['edit'<% if (_currRoleType == "0" || _currRoleType == "1")
        { %>, 'destroy'], width: 150 <% }
        else
        { %>], width: 70  <% } %>
        
            }
        ]"
        data-bind="source: dsListProjectDocuments, events: { edit: onEdit, save: onSave }"
        data-pageable="{ buttonCount: 5 }">
    </div>
    <script type="text/x-kendo-template" id="<%: this.ID %>_actualDate-template">
        <div # if (kendo.toString(data.actualDate, 'yyyyMMdd') < kendo.toString(data.actualDate, 'yyyyMMdd')) { #
                 style="color:red"
            # } #>
            #: data.actualDate ? kendo.toString(data.actualDate, 'dd MMM yyyy') : '-'#
        </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_trafic-template">
        #if (data.trafic) { #
            <img class='logo' src='images/#:common_getTrafficImageUrl(data.trafic)#' style='padding-top: 0px;'>
        #}#
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>generalUserDate-template">
        <div>
            <div class='subTitleTemplate'>Plan:</div>
            <div>#:data.planDate ? kendo.toString(data.planDate,'dd MMM yyyy') : "-"#</div>
            <div class='subTitleTemplate'>Actual:</div>
            <div>#:data.actualDate ? kendo.toString(data.actualDate,'dd MMM yyyy') : "-"#</div>
        </div>
    </script>

    <script id="<%: this.ID %>_editGrdListOtherDepartmentTask" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
        <% if (_currRoleType == "0" || _currRoleType == "1" || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.OtherDeptTask))
        { %>
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Department<span style="color:red">*</span></td>
                    <td><input id="departmentId" name="departmentId" required='required' data-role="combobox"
                               data-value-primitive="true"
                               data-placeholder="Select Department"
                               data-text-field="TABLE_DESC"
                               data-value-field="TABLE_CODE"
                               data-bind="source: dsDepartmentSource" validationMessage="Department is required"
                        style="width: 350px" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Task<span style="color:red">*</span></td>
                    <td><input class="k-textbox" required validationMessage="Task is required" placeholder="Please Input Task" name="taskName" onfocus="this.value = this.value;" style="width:100%" /></td>
                </tr>
                <tr>
                    <td style="width:100px">PIC<span style="color:red">*</span></td>
                    <td>
                        <input id="<%: this.ID %>_picId" name="picId" class="k-textbox" type="text" style="display:none;" />
                        <input id="<%: this.ID %>_picName" name="picName" class="k-textbox" type="text" style="display:none;" />
                        <input id="<%: this.ID %>_picEmail" name="picEmail" class="k-textbox" type="text" disabled="disabled" style="display:none;" />
                        <input id="<%: this.ID %>_employeeTmp" name="requester" data-role="autocomplete" 
                            data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                            placeholder="Name here (min 3 characters)" required validationMessage="PIC is required"
                            data-bind="source: dsEmployee, value: employeeSelected" />
                        <%--<div style="display:none;" 
                            id="<%: this.ID %>_btnVendorSelectedName" 
                            class="k-button">
                            ...
                        </div>--%>
                    </td>
                </tr>
                <tr>
                    <td>Plan Date<span style="color:red">*</span></td>
                    <td><input data-role="datepicker" id="<%: this.ID %>_planDate" data-format="dd MMM yyyy" name="planDate" required validationMessage="Plan Date is required" placeholder="Please Input Plan Date" style="width:180px" /></td>
                </tr
                # if (data.id) { #
                <tr>
                    <td>Actual Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" name="actualDate" placeholder="Please Input Actual Date" style="width:180px" /></td>
                </tr>

                # } #
                 <tr>
                    <td style="width:100px">Remark<span style="color:red">*</span></td>
                    <td><textarea class="k-textbox" required validationMessage="Remark is required" placeholder="Please Input Remark" name="remark" onfocus="this.value = this.value;" style="width:100%"></textarea></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" style="width:100%" data-value-primitive="true" /></td>
                </tr>
            </table>
        <% }
        else
        { %>
            <table style="width:600px">
                <tr>
                    <td style="width:100px;height: 31px;">Project No</td>
                    <td><span>: #:data.projectNo#</span></td>
                </tr>
                <tr>
                    <td style="width:100px;height: 31px;">Project Desc</td>
                    <td><span>: #:data.projectDesc#</span></td>
                </tr>
                <tr>
                    <td style="width:100px;height: 31px;">Department</td>
                    <td><span>: #:data.departmentName#</span></td>
                </tr>
                <tr>
                    <td style="width:100px;height: 31px;">Task</td>
                    <td><span>: #:data.taskName#</span></td>
                </tr>
                <tr>
                    <td style="width:100px;height: 31px;">PIC</td>
                    <td><span>: #:data.picName#</span></td>
                </tr>
                <tr>
                    <td style="width:100px;height: 31px;">Plan Date</td>
                    <td><span>: #:data.planDate ? kendo.toString(data.planDate, 'dd MMM yyyy') : '-'#</span></td>
                </tr>
                # if (data.id) { #
                <tr>
                    <td>Actual Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" name="actualDate" placeholder="Please Input Actual Date" style="width:180px" /></td>
                </tr>
                # } #
                 <tr>
                    <td style="width:100px;height: 31px;">Remark</td>
                    <td><span>: #:data.remark#</span></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" style="width:100%" data-value-primitive="true" /></td>
                </tr>
            </table>
        <% } %>

        </div>
    </script>
    <div id="<%: this.ID %>_popPickPIC">
        <uc1:PickEmployeesControl runat="server" ID="otherDeptTask_PickPIC" />
    </div>
</div>
