﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EWRTaskListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EWRTaskListControl" %>
<%@ Register Src="~/UserControl/EWRK2ApprovalControl.ascx" TagPrefix="uc1" TagName="EWRK2ApprovalControl" %>
<%@ Register Src="~/UserControl/EWRMinutesOfMeetingControl.ascx" TagPrefix="uc1" TagName="EWRMinutesOfMeetingControl" %>
<%@ Register Src="~/UserControl/ReviseEWRControl.ascx" TagPrefix="uc1" TagName="ReviseEWRControl" %>
<%@ Register Src="~/UserControl/K2HistoryControl.ascx" TagPrefix="uc1" TagName="K2HistoryControl" %>





<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>

<div id="<%: this.ID %>_TaskListContent" class="k-content wide">
    <%--<div data-role="tabstrip"
        data-bind="events: { select: onSelect },
    visible: isVisible">--%>
    <div id="<%: this.ID %>_tabstrip">
        <ul>
            <li class="k-state-active">Need My Response
            </li>
            <li>My Request
            </li>
        </ul>
        <div>
            <iframe id="K2Frame" width="100%" height="300" frameborder="0"></iframe>
        </div>
        <div>
            <div data-role="grid"
                data-bind="source: dsEWR, events: { dataBound: dataBound }"
                data-editable="{mode:'popup', confirmation:true}"
                data-pageable="{buttonCount: 5}"
                data-resizable="true"
                data-filterable="{
                        extra: false,
                        operators: {
                            string: {
                                contains: 'Contains',
                                startswith: 'Starts with'
                            }
                        }
                    }"
                data-columns="[
                        { 'field': 'SUBJECT', 'title': 'Subject', 'template': <%: this.ID %>_viewModel.subjectTemplate },
                        { 'field': 'ASSIGNEDPM', 'title': 'Project Manager', 'template': <%: this.ID %>_viewModel.PMTemplate },
                        { 'field': 'PROJECTSPONSOR', 'title': 'Project Sponsor' },
                        { 'field': 'AREA', 'title': 'Area' },
                        { 'field': 'ACCCODE', 'title': 'Account Code' },
                        { 'field': 'NETWORKNO', 'title': 'Network No' },
                        { 'field': 'BUDGETALLOC', 'title': 'Budget Alloc.', 'format': '{0:c2}' },
                        { 'field': 'DATEISSUED', 'title': 'Date Issued', 'format': '{0:dd MMM yyyy}' },
                        { 'field': 'DATECOMPLETION', 'title': 'Date of Completion', 'format': '{0:dd MMM yyyy}' },
                        { 'field': '', 'title': 'MoM EWR No', 'template': <%: this.ID %>_viewModel.momEWRNoTemplate },
                        { 'field': 'STATUS', 'title': 'Status' }
                        <%--,
                        { command: { text: 'Edit', click: <%: this.ID %>_showDraftEditor }, title: ' ', width: '180px' }--%>
                       
                    ]">
            </div>
        </div>
    </div>
    <div id="<%: this.ID %>_popupApproval">
        <uc1:EWRK2ApprovalControl runat="server" ID="EWRK2ApprovalControl" />
        <br />
        <uc1:K2HistoryControl runat="server" ID="K2HistoryControl" />
        <br />
        <div class="form-group form-group-sm">
            <label class="labelForm">Remark</label>

            <%--<input id="inputComment" type="text" data-bind="value: Remark" class="form-control"/>--%>
            <textarea class="k-textbox" style="width: 400px;" data-bind="value: Remark" placeholder="Remark"></textarea>
        </div>
        
        
        <br />
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <div
                    data-role="button"
                    data-bind="events: { click: onApproveRequestClick }"
                    class="k-primary">
                    Approve
                        <span class="glyphicon glyphicon-ok"></span>
                </div>
                <div
                    data-role="button"
                    data-bind="events: { click: onReviseRequestClick }">
                    Revise
                        <span class="glyphicon glyphicon-remove"></span>
                </div>
                <div
                    data-role="button"
                    data-bind="events: { click: onRejectRequestClick }">
                    Reject
                        <span class="glyphicon glyphicon-remove"></span>
                </div>
                <div
                    data-role="button"
                    data-bind="events: { click: onCloseEvent }">
                    Close
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
        </div>
        <br />
    </div>
    <div id="<%: this.ID %>_popupEngineeringApproval">
        <uc1:EWRMinutesOfMeetingControl runat="server" ID="EWRMinutesOfMeetingControl" />
        <br />
        <div class="form-group form-group-sm">
            <label class="labelForm">Remark</label>

            <%--<input id="inputComment" type="text" data-bind="value: Remark" class="form-control"/>--%>
            <textarea class="k-textbox" style="width: 400px;" data-bind="value: Remark" placeholder="Remark"></textarea>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <div
                    data-role="button"
                    data-bind="events: { click: onEngineeringSubmitClick }"
                    class="k-primary">
                    Submit
                        <span class="glyphicon glyphicon-ok"></span>
                </div>
                <%--<div
                    data-role="button"
                    data-bind="events: { click: onEngineeringRejectClick }">
                    Reject
                        <span class="glyphicon glyphicon-remove"></span>
                </div>--%>
                <div
                    data-role="button"
                    data-bind="events: { click: onCloseEngineeringEvent }">
                    Close
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
        </div>
        <br />
    </div>
    <div id="<%: this.ID %>_popupOriginatorRevision">
        <uc1:ReviseEWRControl runat="server" id="ReviseEWRControl" />
        <br />
        <uc1:K2HistoryControl runat="server" ID="RevisionK2HistoryControl" />
        <br />
        <div class="form-group form-group-sm">
            <label class="labelForm">Remark</label>
            <textarea class="k-textbox" style="width: 400px;" data-bind="value: Remark" placeholder="Remark"></textarea>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <div
                    data-role="button"
                    data-bind="events: { click: onOriginatorSubmitClick }"
                    class="k-primary">
                    Submit
                        <span class="glyphicon glyphicon-ok"></span>
                </div>
                <div
                    data-role="button"
                    data-bind="events: { click: onCloseRevisionEvent }">
                    Close
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
        </div>
        <br />
    </div>
    <div id="<%: this.ID %>_popupOriginatorDraft">
        <uc1:ReviseEWRControl runat="server" id="ReviseEWRDraftControl" />
        <br />
        <div class="form-group form-group-sm">
            <label class="labelForm">Remark</label>
            <textarea class="k-textbox" style="width: 400px;" data-bind="value: Remark" placeholder="Remark"></textarea>
        </div>
        <br />
        <div>
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onCloseDraftEvent }">
                    Close
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onSaveDraftClick }"
                    class="k-primary">
                    Save as Draft
                        <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
            <div style="float: right;">&nbsp;</div>
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onSubmitDraftClick }"
                    class="k-primary">
                    Submit
                        <span class="glyphicon glyphicon-ok"></span>
                </div>
            </div>
        </div>
        
    </div>

    <div id="<%: this.ID %>_popupViewEWRRequest">
        <uc1:ReviseEWRControl runat="server" ID="ViewEWRControl" />
        <br />
        <uc1:K2HistoryControl runat="server" ID="ViewK2HistoryControl" />
        <br />
        <div>
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onCloseViewEvent }">
                    Close
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
            &nbsp;
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onPrintPDFEvent }, visible: isPrintPDFVisible"
                    class="k-primary">
                    Print to PDF
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
        </div>
        <br />
    </div>
</div>
<style>
    .fieldlist {     gin: 0 0 -1 m;
         ding: 0;
    }
        
         e

    st li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
<script>
    var <%: this.ID %>_tabStrip;

    var <%: this.ID %>_viewModel = kendo.observable({
        isVisible: true,
        ewrRequestID: -1,
        submitState: "Submitted", //submitted or saved (as Draft)
        k2State: "",
        onSelect: function (e) {
            var text = $(e.item).children(".k-link").text();
            //console.log("event :: select (" + text + ")");
        },
        Remark: "",
        isPrintPDFVisible: false,
        dsEWR: new kendo.data.DataSource({
            schema: {
                model: {
                    id: "OID",
                    fields: {
                        SUBJECT: { type: "string" },
                        AREA: { type: "string" },
                        DATEISSUED: { type: "date" },
                        ACCCODE: { type: "string" },
                        NETWORKNO: { type: "string" },
                        BUDGETALLOC: { type: "number" },
                        DATECOMPLETION: { type: "date" }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "EWRREQUESTs/FindEWRRequestByBN/<%: BadgeNo %>",
                    dataType: "json"
                },
                update: {
                    url: "",
                    dataType: "json"
                },
                create: {
                    url: "",
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            },
            pageSize: 10
        }),
        dataBound: function (e) {
            var grid = e.sender;
            var dataItem;
            grid.tbody.find("tr[role='row']").each(function () {
                dataItem = grid.dataItem(this);
                //console.log(dataItem);
                // Show Print Button
                if (dataItem.STATUS != "DRAFT") {

                    $(this).find(".k-grid-Edit").hide();
                    //$(".k-grid-CancelRequest").find("span").addClass("k-icon k-i-print");
                } else {

                    $(this).find(".k-grid-Edit").show();
                }

            });

        },
        onApproveRequestClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_popupApproval"), true);
            //if (!ApprovalControl_viewModel.reservationModel.get("Comment")) {
            //    alert("Please insert purpose before continue");
            //    kendo.ui.progress($("#popupApproval"), false);
            //    return;
            //}
            if (confirm("Are you sure you want to approve this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "EWRREQUESTs/ApproveRequest/" + <%: this.ID %>_viewModel.get("ewrRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {

                        kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);

                        alert("This request is successfully approved");
                        
                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_popupApproval.close();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);

                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);
            }
        },
        onRejectRequestClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_popupApproval"), true);
            if (!<%: this.ID %>_viewModel.get("Remark")) {
                alert("Please insert remark before continue");
                kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);
                return;
            }
            if (confirm("Are you sure you want to reject this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "EWRREQUESTs/RejectRequest/" + <%: this.ID %>_viewModel.get("ewrRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {

                        kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);

                        alert("This request is successfully rejected");
                        
                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_popupApproval.close();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);

                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);
            }
        },
        onReviseRequestClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_popupApproval"), true);
            if (!<%: this.ID %>_viewModel.get("Remark")) {
                alert("Please insert remark before continue");
                kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);
                return;
            }
            if (confirm("Are you sure you want to revise this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "EWRREQUESTs/RequestNeedRevised/" + <%: this.ID %>_viewModel.get("ewrRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {

                        kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);

                        alert("This request is successfully revised");
                        
                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_popupApproval.close();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);

                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_popupApproval"), false);
            }
        },
        onEngineeringSubmitClick: function (e) {
            
            if (<%: this.ID %>_viewModel.get("k2State") == "EngineeringL2Approval") {

                kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), true);

                if (confirm("Are you sure you want to approve this request?")) {
                    var request = {
                        Remark: <%: this.ID %>_viewModel.get("Remark")
                    }

                    $.ajax({
                        url: _webApiUrl + "EWRREQUESTs/ApproveRequest/" + <%: this.ID %>_viewModel.get("ewrRequestID"),
                        type: "POST",
                        data: request,
                        success: function (data) {

                            kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);

                            alert("This request is successfully approved");
                            
                            <%: this.ID %>_viewModel.set("Remark", "");
                            <%: this.ID %>_popupEngineeringApproval.close();
                        },
                        error: function (jqXHR) {
                            kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);

                            alert(jqXHR);
                        }
                    });
                } else {
                    kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);
                }
            } else {
                if (EWRMinutesOfMeetingControl_viewModel.onValidationEntry()) {
                    kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), true);
                    //var typeOfProjectType = [];
                    //var otherDesc = EWRMinutesOfMeetingControl_viewModel.get("projectTypeOther");
                    //$.each(EWRMinutesOfMeetingControl_viewModel.itemsProjectType, function (idx, el) {
                        
                    //    if (el == "other") {
                    //        typeOfProjectType.push({ TYPE: "projecttype", VALUE: el, DESCRIPTION: EWRMinutesOfMeetingControl_viewModel.get("projectTypeOther") });
                    //    } else {
                    //        typeOfProjectType.push({ TYPE: "projecttype", VALUE: el });
                    //    }

                    //});
                    
                    var momData = EWRMinutesOfMeetingControl_viewModel.populateData();
                   
                    $.ajax({
                        url: _webApiUrl + "momewrs/SubmitMOMRequest/1",
                        type: "POST",
                        data: momData,
                        success: function (data) {

                            kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);
                            <%--if (<%: this.ID %>_kendoUploadButton) {
                                <%: this.ID %>_kendoUploadButton.click();
                            } else {--%>
                            //_showDialogMessage("Submit Request", "Request has been submitted", "");
                            alert("Request has been submitted");
                            //}
                            EWRMinutesOfMeetingControl_viewModel.onClearEvent();
                            
                            <%: this.ID %>_viewModel.set("Remark", "");
                            <%: this.ID %>_popupEngineeringApproval.close();

                            location.reload();
                        },
                        error: function (jqXHR) {
                            if (jqXHR.status == 400) {

                                aaa = jqXHR;
                                if (jqXHR.responseJSON == "duplicateNo") {
                                    _showDialogMessage("Error Message", "Cannot Duplicate Project No!", "");
                                } else {
                                    _showDialogMessage("Error Message", "Cannot Duplicate Project Title!", "");
                                }

                            } else {
                                _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                            }

                            kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);
                        }
                    });
                }
            }
            
        },
        onEngineeringRejectClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), true);
            if (!<%: this.ID %>_viewModel.get("Remark")) {
                alert("Please insert remark before continue");
                kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);
                return;
            }
            if (confirm("Are you sure you want to reject this request?")) {
                var request = {
                    Remark: <%: this.ID %>_viewModel.get("Remark")
                }

                $.ajax({
                    url: _webApiUrl + "EWRREQUESTs/RejectRequest/" + <%: this.ID %>_viewModel.get("ewrRequestID"),
                    type: "POST",
                    data: request,
                    success: function (data) {

                        kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);

                        alert("This request is successfully rejected");
                        
                        <%: this.ID %>_viewModel.set("Remark", "");
                        <%: this.ID %>_popupEngineeringApproval.close();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);

                        alert(jqXHR);
                    }
                });
            } else {
                kendo.ui.progress($("#<%: this.ID %>_popupEngineeringApproval"), false);
            }
        },
        onOriginatorSubmitClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), true);
            if (!<%: this.ID %>_viewModel.get("Remark")) {
                alert("Please insert remark before continue");
                kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);
                return;
            }
            if (ReviseEWRControl_viewModel.onValidationEntry()) {
                if (ReviseEWRControl_viewModel.onValidateOther()) {
                    if (confirm("Are you sure you want to submit this request?")) {
                
                        var requestData = ReviseEWRControl_viewModel.populateData();
                        requestData.Remark = <%: this.ID %>_viewModel.get("Remark");
                                
                        $.ajax({
                            url: _webApiUrl + "EWRREQUESTs/ReviseRequest/" + <%: this.ID %>_viewModel.get("ewrRequestID"),
                            type: "POST",
                            data: requestData,
                            success: function (data) {

                                if (ReviseEWRControl_kendoUploadButton) {
                                    ReviseEWRControl_kendoUploadButton.click();
                                } else {
                                    //_showDialogMessage("Submit Request", "Request has been submitted", "");
                                    alert("This request is successfully revised");
                                    kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);
                                    <%: this.ID %>_popupOriginatorRevision.close();
                                }

                                <%: this.ID %>_viewModel.set("Remark", "");
                        
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);

                                alert(jqXHR);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);
                    }
                } else {
                    kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);
                }
            } else {
                kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);
            }
            
        },
        subjectTemplate: function (dataItem) {
            if (dataItem) {
                var result = "";
                if (dataItem.STATUS == "DRAFT") {
                    result = "<a href='#' onclick='<%: this.ID %>_showDraftEditorSubject(" + dataItem.OID + ");'>" + (dataItem.SUBJECT ? dataItem.SUBJECT : "<i>(Edit Draft)</i>") + "</a>";
                } else {
                    result = "<a href='#' onclick='<%: this.ID %>_openViewControl(" + dataItem.OID + ");'>" + (dataItem.SUBJECT ? dataItem.SUBJECT : "-") + "</a>";
                }
                
                return result;
            } 
            return "";
        },
        PMTemplate: function (dataItem) {
            if (dataItem && dataItem.EWRMOM) {
                return "<span>" + (dataItem.EWRMOM.PROJECTMANAGER ? dataItem.EWRMOM.PROJECTMANAGER : "-") + "</span>";
            }
            return "-";
        },
        momEWRNoTemplate: function (dataItem) {
            if (dataItem && dataItem.EWRMOM) {
                return "<span>" + (dataItem.EWRMOM.MOMEWRNO ? dataItem.EWRMOM.MOMEWRNO : "-")  + "</span>";
            } 
            return "-";
        },
        onCloseEvent: function (e) {
            <%: this.ID %>_popupApproval.close();
        },
        onCloseEngineeringEvent: function (e) {
            <%: this.ID %>_popupEngineeringApproval.close();
        },
        onCloseRevisionEvent: function (e) {
            <%: this.ID %>_popupOriginatorRevision.close();
        },
        onSubmitDraftClick: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_popupOriginatorDraft"), true);
            
            if (ReviseEWRDraftControl_viewModel.onValidationEntry()) {
                if (ReviseEWRDraftControl_viewModel.onValidateOther()) {
                    if (confirm("Are you sure you want to submit this request?")) {
                
                        var requestData = ReviseEWRDraftControl_viewModel.populateData();
                        requestData.Remark = <%: this.ID %>_viewModel.get("Remark");
                                
                        $.ajax({
                            url: _webApiUrl + "EWRREQUESTs/SubmitEWRDraftRequest/" + requestData.OID,
                            type: "POST",
                            data: requestData,
                            success: function (data) {
                                <%: this.ID %>_viewModel.set("submitState", "Submitted");
                                if (ReviseEWRDraftControl_kendoUploadButton) {
                                    ReviseEWRDraftControl_kendoUploadButton.click();
                                } else {
                                    //_showDialogMessage("Submit Request", "Request has been submitted", "");
                                    alert("This request is successfully Submitted");
                                    kendo.ui.progress($("#<%: this.ID %>_popupOriginatorDraft"), false);
                                    <%: this.ID %>_popupOriginatorDraft.close();

                                    ReviseEWRDraftControl_viewModel.onClearEvent();

                                    <%: this.ID %>_viewModel.dsEWR.read();
                                }

                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#<%: this.ID %>_popupOriginatorDraft"), false);

                                alert(jqXHR);
                            }
                        });
                    }
                } 
            }
            
            kendo.ui.progress($("#<%: this.ID %>_popupOriginatorDraft"), false);

        },
        onSaveDraftClick: function () {
            if (confirm("Are you sure you save this request as Draft?")) {
                //
                var requestData = ReviseEWRDraftControl_viewModel.populateData();
                    
                kendo.ui.progress($("#ReviseEWRDraftControl_RequestContent"), true);
                $.ajax({
                    url: _webApiUrl + "ewrrequests/SaveEWRRequestAsDraft/0",
                    type: "POST",
                    data: requestData,
                    success: function (data) {
                            
                        <%: this.ID %>_viewModel.set("ewrOID", data.OID);
                        <%: this.ID %>_viewModel.set("submitState", "Saved");
                        if (ReviseEWRDraftControl_kendoUploadButton) {
                            ReviseEWRDraftControl_kendoUploadButton.click();
                        } else {
                            //_showDialogMessage("Submit Request", "Request has been submitted", "");
                            alert("Request has been save as Draft");
                            //location.reload();
                            <%: this.ID %>_popupOriginatorDraft.close();
                        }

                        kendo.ui.progress($("#ReviseEWRDraftControl_RequestContent"), false);
                        
                        ReviseEWRDraftControl_viewModel.onClearEvent();

                        <%: this.ID %>_viewModel.dsEWR.read();
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#ReviseEWRDraftControl_RequestContent"), false);
                        
                        _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                    }
                });
            }
        },
        onCloseDraftEvent: function () {
           
            ReviseEWRDraftControl_viewModel.onClearEvent();
            <%: this.ID %>_popupOriginatorDraft.close();
        },
        onCloseViewEvent: function () {
           
            ViewEWRControl_viewModel.onClearEvent();
            <%: this.ID %>_popupViewEWRRequest.close();
        },
        onPrintPDFEvent: function () {
            var url = "<%: WebAppUrl.Replace("/", "|") %>" + "TemplateEWRDetailForm" + ".aspx?EWRID=" + <%: this.ID %>_viewModel.get("ewrRequestID");

            url = encodeURIComponent(url);
            var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20" + url + "/";
            
            window.open(pdfUrl);
        }
    });
    kendo.bind($("#<%: this.ID %>_TaskListContent"), <%: this.ID %>_viewModel);
    // PRODUCTION
    $("#K2Frame").attr("src", "<%: K2WorkListUrl %>&UserName=<%: NtUserID %>&ViewCallback=onK2ViewClick");
    // DEVELOPMENT
    //$("#K2Frame").attr("src", "<%: K2WorkListUrl %>&UserName=wnotifier&ViewCallback=onK2ViewClick");

    $("#<%: this.ID %>_popupApproval").kendoWindow({
        title: "Approval",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupApproval = $("#<%: this.ID %>_popupApproval").data("kendoWindow");

    $("#<%: this.ID %>_popupEngineeringApproval").kendoWindow({
        title: "Minutes Of Meeting",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupEngineeringApproval = $("#<%: this.ID %>_popupEngineeringApproval").data("kendoWindow");

    $("#<%: this.ID %>_popupOriginatorRevision").kendoWindow({
        title: "Originator Revision",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupOriginatorRevision = $("#<%: this.ID %>_popupOriginatorRevision").data("kendoWindow");

    $("#<%: this.ID %>_popupOriginatorDraft").kendoWindow({
        title: "Originator Draft",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupOriginatorDraft = $("#<%: this.ID %>_popupOriginatorDraft").data("kendoWindow");

    
    $("#<%: this.ID %>_popupViewEWRRequest").kendoWindow({
        title: "View EWR Request",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupViewEWRRequest = $("#<%: this.ID %>_popupViewEWRRequest").data("kendoWindow");

    function onK2ViewClick(k2Data) {
        //console.log(k2Data);
        if (k2Data) {
            var appID = k2Data.instance.AppID;
            <%: this.ID %>_viewModel.set("ewrRequestID", appID);
            <%: this.ID %>_viewModel.set("k2State", k2Data.instance._State);

            
            <%: this.ID %>_viewModel.set("Remark", "");

            //console.log(k2Data.instance._State);
            $.ajax({
                url: _webApiUrl + "EWRREQUESTs/FindEWRByID/" + appID,
                type: "GET",
                success: function (data) {
                    
                    //if (k2Data.instance._State == "L2Approval") {
                    //    onViewApproverClick(data);
                    //} else
                    if (k2Data.instance._State == "Project+Sponsor+Approval") {
                        onViewApproverClick(data);
                    } else if (k2Data.instance._State == "EngineeringL2Approval") {
                        onViewApproverClick(data)

                    } else if (k2Data.instance._State == "SecretaryApproval") {
                        onViewEngineeringClick(data);
                    } else if (k2Data.instance._State == "Need+Revision") {
                        onViewRevisionClick(data);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    function <%: this.ID %>_showEditor(e) {
        var pickedData = this.dataItem($(e.currentTarget).closest("tr"));
        // show popup
        <%: this.ID %>_popEditor.center().open();
        EWRK2ApprovalControl_Init(pickedData, "EWREngineeringForm.aspx", false);
        EWRK2ApprovalControl_viewModel.set("onCloseUpdateClick", function (e) {
            <%: this.ID %>_popEditor.close();
        });
    }

    function <%: this.ID %>_showDraftEditor(e) {
        var pickedData = this.dataItem($(e.currentTarget).closest("tr"));
        <%: this.ID %>_popupOriginatorDraft.center().open();
        ReviseEWRDraftControl_Init(pickedData, "EWRTaskListForm.aspx");
        
    }
    function <%: this.ID %>_showDraftEditorSubject(appID) {
       
        <%: this.ID %>_popupOriginatorDraft.center().open();

        $.ajax({
            url: _webApiUrl + "EWRREQUESTs/FindEWRByID/" + appID,
            type: "GET",
            success: function (data) {
                ReviseEWRDraftControl_Init(data, "EWRTaskListForm.aspx");
               
            },
            error: function (jqXHR) {
            }
        });
        
    }
    function onViewApproverClick(ewrData) {
        <%: this.ID %>_popupApproval.center().open();
        EWRK2ApprovalControl_Init(ewrData, "EWRTaskListForm.aspx", true);
        var histories = [];
        
        $.each(ewrData.K2Histories, function (idx, elem) {
            if (elem.Status == "Completed") {
                histories.push({
                    ActivityName: elem.ActivityName,
                    CreatedDate: kendo.toString(kendo.parseDate(elem.CreatedDate), "dd MMM yyyy hh:mm"),
                    Name: elem.Name,
                    ActionResult: elem.ActionResult,
                    Comment: elem.Comment
                });
            }
        });
        K2HistoryControl_Init(histories);
        //ApprovalControl_DataBindDefaultForm(reservationOID);

    }

    function <%: this.ID %>_openViewControl(appID) {
        <%: this.ID %>_popupViewEWRRequest.center().open();
        <%: this.ID %>_viewModel.set("ewrRequestID", appID);
        $.ajax({
            url: _webApiUrl + "EWRREQUESTs/FindEWRByID/" + appID,
            type: "GET",
            success: function (data) {
                if (data && data.K2Histories) {
                    var histories = data.K2Histories;
                    var result = $.grep(histories, function (el, idx) {
                        return el.ActivityName == "Operating Project Commitee" && el.Status == "Completed"
                    });
                    //console.log("TEST");
                    //console.log(result);
                    if (result.length > 0) {
                        <%: this.ID %>_viewModel.set("isPrintPDFVisible", true);
                    } else {
                        <%: this.ID %>_viewModel.set("isPrintPDFVisible", false);
                    }
                        
                }
                ViewEWRControl_InitViewOnly(data, "EWRTaskListForm.aspx");
                var histories = [];

                $.each(data.K2Histories, function (idx, elem) {
                    if (elem.Status == "Completed") {
                        histories.push({
                            ActivityName: elem.ActivityName,
                            CreatedDate: kendo.toString(kendo.parseDate(elem.CreatedDate), "dd MMM yyyy hh:mm"),
                            Name: elem.Name,
                            ActionResult: elem.ActionResult,
                            Comment: elem.Comment
                        });
                    }
                });
                ViewK2HistoryControl_Init(histories);
            },
            error: function (jqXHR) {
            }
        });
        
    }

    function onViewEngineeringClick(ewrData) {
        <%: this.ID %>_popupEngineeringApproval.center().open();
        EWRMinutesOfMeetingControl_Init(ewrData, "EWRTaskListForm.aspx", false);
    }

    function onViewEngineeringL2Click(ewrData) {
        <%: this.ID %>_popupEngineeringApproval.center().open();
        EWRMinutesOfMeetingControl_Init(ewrData, "EWRTaskListForm.aspx", true);
    }

    function onViewRevisionClick(ewrData) {
        <%: this.ID %>_popupOriginatorRevision.center().open();
        ReviseEWRControl_Init(ewrData, "EWRTaskListForm.aspx");
        
        var histories = [];

        $.each(ewrData.K2Histories, function (idx, elem) {
            if (elem.Status == "Completed") {
                histories.push({
                    ActivityName: elem.ActivityName,
                    CreatedDate: kendo.toString(kendo.parseDate(elem.CreatedDate), "dd MMM yyyy hh:mm"),
                    Name: elem.Name,
                    ActionResult: elem.ActionResult,
                    Comment: elem.Comment
                });
            }
        });
        RevisionK2HistoryControl_Init(histories);
    }

    function ReviseEWRControl_onUploadSuccess(e) {
        
        var tmp = ReviseEWRControl_viewModel.get("totalFilesToUpload");
        ReviseEWRControl_viewModel.set("totalFilesToUpload", (tmp - 1));
        
        if (ReviseEWRControl_viewModel.get("totalFilesToUpload") == 0) {
            alert("This request is successfully revised");
            kendo.ui.progress($("#<%: this.ID %>_popupOriginatorRevision"), false);
            <%: this.ID %>_popupOriginatorRevision.close();
        }
        
        ReviseEWRControl_uploaderFile.clearAllFiles();
    }

    function ReviseEWRDraftControl_onUploadSuccess(e) {
        
        var tmp = ReviseEWRDraftControl_viewModel.get("totalFilesToUpload");
        ReviseEWRDraftControl_viewModel.set("totalFilesToUpload", (tmp - 1));
        
        if (ReviseEWRDraftControl_viewModel.get("totalFilesToUpload") == 0) {
            alert("This request is successfully " + <%: this.ID %>_viewModel.get("submitState"));
            kendo.ui.progress($("#<%: this.ID %>_popupOriginatorDraft"), false);
            <%: this.ID %>_popupOriginatorDraft.close();
        }

        ReviseEWRDraftControl_uploaderFile.clearAllFiles();

        ReviseEWRDraftControl_viewModel.onClearEvent();
        
        <%: this.ID %>_viewModel.dsEWR.read();
    }

    // Document Ready
    $(document).ready(function () {
        <%: this.ID %>_tabStrip = $("#<%: this.ID %>_tabstrip").kendoTabStrip().data("kendoTabStrip");
                        
        //<%: this.ID %>_tabStrip.select("li:last");  // Select by jQuery selector
       <%-- $("#<%: this.ID %>_tabstrip").on("click", ".k-button", function () {
            <%: this.ID %>_tabStrip.select(2);   // Select by index
        });--%>

    });

    function <%: this.ID %>_Init(showNeedMyResponse) {
        
        if (!showNeedMyResponse) {
           <%: this.ID %>_tabStrip.select("li:last");  // Select by jQuery selector
        } else {
           <%: this.ID %>_tabStrip.select("li:first");  // Select by jQuery selector
        }
        
    }
</script>
