﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EwpPerformanceReportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EwpPerformanceReportControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
    var <%: this.ID %>_ewpReportControlGrid = [], <%: this.ID %>_actualCostFormatData = [], currYear = new Date().getFullYear();
    var <%: this.ID %>_viewEwpReportModel = kendo.observable({
        paramYear: currYear,
        listYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            }
        }),
        onChangeParam: function (e) {
            this.dsEwpReport.read();
        },
        dsEwpReport: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        level: { type: 'number' },
                        deliverableWeightHour: { type: 'number' },
                        deliverableWeightPercent: { type: 'number' },
                        factoredWeight: { type: 'number' },
                        finalResult: { type: 'number' },
                        performance: { type: 'number' },
                        achievement: { type: 'number' },
                        planStartDate: { type: 'date' },
                        planFinishDate: { type: 'date' },
                        actualStartDate: { type: 'date' },
                        actualFinishDate: { type: 'date' }
                    }
                }
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/EwpPerformanceReport?paramYear=" + <%: this.ID %>_viewEwpReportModel.paramYear;
                    },
                    async: false
                }
            }
        }),
        onDataBound: function (e) {
            //console.log("aurel");
            //console.log(this);
            //console.log(e.sender._data);
            var data = e.sender._data;
            $.each(data, function (i, row) {
                if (row.level == 0) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#C00000");
                } else if (row.level == 1) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#1F497D");
                } else if (row.level == 2) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#E26B0A");
                }
            });
        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_EwpReportControl"), <%: this.ID %>_viewEwpReportModel);
        $("#<%: this.ID %>_gridReport").data("kendoGrid").resize();
    });

</script>

<script type="text/x-kendo-template" id="<%: this.ID %>projectNo-template">
    #: data.level == 0 ? data.projectNo : ""#
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableNo-template">
    <div style="padding-left: #:data.level * 8#px;">#=data.deliverableNo#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableName-template">
    <div style="padding-left: #:data.level * 8#px;">#=data.deliverableName#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableNo-headerTemplate">
    <div style="text-align: center;">Project No.<br>EWP/DWG No.</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>projectType-headerTemplate">
    <div style="text-align: center;">Project<br>Type</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>factoredWeight-headerTemplate">
    <div style="text-align: center;">Factore<br>Weight</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>finalResult-headerTemplate">
    <div style="text-align: center;">Final<br>Result</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>performance-headerTemplate">
    <div style="text-align: center;">Perfor<br>mance</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>achievement-headerTemplate">
    <div style="text-align: center;">Achieve<br>ment</div>
</script>
<div id="<%: this.ID %>_EwpReportControl">
     <div>
         <span>Year: </span>
         <input data-role="dropdownlist"
                data-placeholder="Select Year"
                data-value-primitive="true"
                data-text-field="text"
                data-value-field="value"
                data-bind=" source: listYear, value: paramYear, events: { change: onChangeParam }"
                style="width: 100px" />
         </div>
    <div id="<%: this.ID %>_gridReport" style="height: 400px;" data-role="grid" data-bind="source: dsEwpReport, events: { dataBound: onDataBound }" 
    <%--data-scrollable="false"--%>
    data-filterable= "true"
    data-columns="[
        {
            field: 'projectNo', title: 'Project No', width: 110, hidden: true,
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'},
            template: kendo.template($('#<%: this.ID %>projectNo-template').html())
        },
        { 
            field: 'deliverableNo', title: 'EWP/DWG No', width: 150, 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'},
            headerTemplate: kendo.template($('#<%: this.ID %>deliverableNo-headerTemplate').html()),
            template: kendo.template($('#<%: this.ID %>deliverableNo-template').html())
        },
        { 
            field: 'deliverableName', title: 'Project Name / Enginer Designer Deliverable', width: 400, 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'},
            template: kendo.template($('#<%: this.ID %>deliverableName-template').html())
        },
        { 
            field: 'projectType', title: 'Project Type', width: 90, 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'},
            headerTemplate: kendo.template($('#<%: this.ID %>projectType-headerTemplate').html())
        },
        { 
            field: 'pic', title: 'Engineer / Designer', width: 200, headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'} },
        { 
            field: 'category', title: 'Category', headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'} 
        },
        { 
            title: 'Plan Date', headerAttributes: {style: 'text-align: center'}, columns: [
            { 
                field: 'planStartDate', title: 'Start', format: '{0: dd MMM yyyy}', width: 90, 
                headerAttributes: {style: 'text-align: center'}, attributes: {style: 'text-align: center;vertical-align:top'} 
            },
            { 
                field: 'planFinishDate', title: 'Finish', format: '{0: dd MMM yyyy}', width: 90, 
                headerAttributes: {style: 'text-align: center'}, attributes: {style: 'text-align: center;vertical-align:top'} 
            } ]
        }, 
        { 
            title: 'Actual Date', headerAttributes: {style: 'text-align: center'}, columns: [
            { 
                field: 'actualStartDate', title: 'Start', format: '{0: dd MMM yyyy}', width: 90, 
                headerAttributes: {style: 'text-align: center'}, attributes: {style: 'text-align: center;vertical-align:top'} 
            },
            { 
                field: 'actualFinishDate', title: 'Finish', format: '{0: dd MMM yyyy}', width: 90, 
                headerAttributes: {style: 'text-align: center'}, attributes: {style: 'text-align: center;vertical-align:top'} 
            } ]
        }, 
        { 
            title: 'Deliverable Weight', headerAttributes: {style: 'text-align: center'}, columns: [
            { 
                field: 'deliverableWeightHour', title: 'Hour', width: 90, 
                template: '#:data.level == 0 ? \'\': kendo.toString(data.deliverableWeightHour ? data.deliverableWeightHour : 0, \'n2\')#', 
                headerAttributes: {style: 'text-align: center'}, attributes: {style: 'text-align: center;vertical-align:top'} 
            },
            { 
                field: 'deliverableWeightPercent', title: '(%)', width: 90,
                template: '#:data.level == 0 ? \'\': kendo.toString(data.deliverableWeightPercent ? data.deliverableWeightPercent : 0, \'n2\') + \'%\'#', 
                headerAttributes: {style: 'text-align: center'}, attributes: {style: 'text-align: center;vertical-align:top'} 
            } ]
        }, 
        { 
            field: 'factoredWeight', title: 'Factored Weight', width: 90, filterable: false,
            template: '#:data.level == 0 ? \'\': kendo.toString(data.factoredWeight ? data.factoredWeight : 0, \'n2\') + \'%\'#', 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'text-align: right; vertical-align:top'},
            headerTemplate: kendo.template($('#<%: this.ID %>factoredWeight-headerTemplate').html())
        },
        { 
            field: 'finalResult', title: 'Final Result', width: 90, filterable: false,
            template: '#:data.level == 0 ? \'\': kendo.toString(data.finalResult ? data.finalResult : 0, \'n3\')#', 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'text-align: right; vertical-align:top'},
            headerTemplate: kendo.template($('#<%: this.ID %>finalResult-headerTemplate').html())
        },
        { 
            field: 'performance', title: 'Performance', width: 90, filterable: false,
            template: '#:data.level == 0 ? \'\': kendo.toString(data.performance ? data.performance : 0, \'n2\')#', 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'text-align: right; vertical-align:top'},
            headerTemplate: kendo.template($('#<%: this.ID %>performance-headerTemplate').html())
        },
        { 
            field: 'achievement', title: 'Achievement', width: 90, filterable: false,
            template: '#:data.level == 0 ? \'\': kendo.toString(data.achievement ? data.achievement : 0, \'n1\')#', 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'text-align: right; vertical-align:top'},
            headerTemplate: kendo.template($('#<%: this.ID %>achievement-headerTemplate').html())
        },
        { 
            field: 'remark', title: 'Remark', width: 300, 
            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'vertical-align:top'} 
        }
        ]">
    </div>
</div>
