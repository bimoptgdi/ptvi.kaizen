﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploaderWithTypeDetailControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.UploaderWithTypeDetailControl" %>

<script id="Reject for Revision">

    var urlSupportingDocumentUpload = "";
    var kendoUploadButton;
    var ucUploader_uploaderFile;
    var ucUploader_selectedFileName;
    var cboTypeDetail;

    $(document).ready(function () {
        
    });

    function ucUploader_uploaderInit(oid, type, badgeNo, url_type) {
        $("#txtOidReqAdd").val(oid);
        if (!cboTypeDetail) {
            cboTypeDetail = $("#cboTypeDetail").kendoComboBox({
                dataValueField: "VALUE",
                dataTextField: "TEXT",
                //dataTextField : "vatext",
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: { url: url_type, dataType: "json" }
                    },
                    sort: { field: "OID", dir: "ASC" }
                }),
                change: function () {
                    ucUploader_uploaderFile.enable();
                }
            });
        }
        else {
            $("#cboTypeDetail").data("kendoComboBox").value("");
        }

        if (!ucUploader_uploaderFile) {
            ucUploader_uploaderFile = $("#ucUploader_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload/PostUploadNew/1",
                    removeUrl: "remove",
                    //batch: false,
                    autoUpload: false
                },
                //showFileList: false,
                multiple: false,
                select: function (e) {
                    var files = e.files;

                    if ($("#cboTypeDetail").val() == "CRITIC" || $("#cboTypeDetail").val() == "MSDS" || $("#cboTypeDetail").val() == "CATALOG") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf") {
                                _showErrorMessage("Upload Error", "For Criticality Form, MSDS and Catalog Reference, only pdf files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "CHEMICAL") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf" && value.extension.toLowerCase() != ".doc" && value.extension.toLowerCase() != ".docx") {
                                _showErrorMessage("Upload Error", "For Chemical Form, only pdf and word files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "COMMUNICATION") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".msg") {
                                _showErrorMessage("Upload Error", "For Communication Document, only msg files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                },
                success: function (e) {
                    $("#lblInfo").text("Upload complete.");
                },
                error: function (e) {
                    var files = e.files;
                    var err = e.XMLHttpRequest;

                    if (e.operation == "upload") {
                        _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
                    }

                },
                localization: {
                    select: 'Browse...'
                },
                upload: function (e) {
                    if ($("#cboTypeDetail").val() == "") {
                        _showErrorMessage("Error message", "Please select Type before upload a file.", "", 2, "");
                        e.preventDefault();
                    }
                    else {
                        e.data = { typeId: $("#txtOidReqAdd").val(), typeName: type, badgeNo: badgeNo, typeDetail: $("#cboTypeDetail").val() };
                    }
                }
            }).data("kendoUpload");
        }
        

        if (ucUploader_uploaderFile) {
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
            $(".k-upload.k-header").addClass("k-upload-empty");
            $(".k-upload-button").removeClass("k-state-focused");
            $("#lblInfo").text("");
            $(".k-upload-files.k-reset").find("li").parent().remove();
        }

        ucUploader_uploaderFile.disable();
        cboTypeDetail.data("kendoComboBox").enable(true);
    }

    function ucUploader_uploaderInitCustoms(oid, type, badgeNo, url_type, itemsButtons) {
        $("#txtOidReqAdd").val(oid);
        
        if (!cboTypeDetail) {
            cboTypeDetail = $("#cboTypeDetail").kendoComboBox({
                dataValueField: "VALUE",
                dataTextField: "TEXT",
                //dataTextField : "vatext",
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: { url: url_type, dataType: "json" }
                    },
                    sort: { field: "OID", dir: "ASC" }
                }),
                change: function () {
                    ucUploader_uploaderFile.enable();
                }
            });
        }
        else {
            var typeDS = new kendo.data.DataSource({
                transport: {
                    read: { url: url_type, dataType: "json" }
                },
                sort: { field: "OID", dir: "ASC" }

            });

            cboTypeDetail.data("kendoComboBox").setDataSource(typeDS);

            $("#cboTypeDetail").data("kendoComboBox").value("");
        }

        if (!ucUploader_uploaderFile) {
            ucUploader_uploaderFile = $("#ucUploader_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload/PostUploadNew/1",
                    removeUrl: "remove",
                    //batch: false,
                    autoUpload: false
                },
                //showFileList: false,
                multiple: false,
                select: function (e) {
                    var files = e.files;

                    if ($("#cboTypeDetail").val() == "CRITIC" || $("#cboTypeDetail").val() == "MSDS" || $("#cboTypeDetail").val() == "CATALOG") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf") {
                                _showErrorMessage("Upload Error", "For Criticality Form, MSDS and Catalog Reference, only pdf files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "CHEMICAL") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf" && value.extension.toLowerCase() != ".doc" && value.extension.toLowerCase() != ".docx") {
                                _showErrorMessage("Upload Error", "For Chemical Form, only pdf and word files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "COMMUNICATION") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".msg") {
                                _showErrorMessage("Upload Error", "For Communication Document, only msg files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                },
                success: function (e) {
                    var itsItemsButtons = itemsButtons.split("|");
                    $.each(itsItemsButtons, function (index, value) {
                        $("#" + value).prop("disabled", false);
                    });

                    $("#lblInfo").text("Upload complete.");
                },
                error: function (e) {
                    var files = e.files;
                    var err = e.XMLHttpRequest;

                    var itsItemsButtons = itemsButtons.split("|");
                    $.each(itsItemsButtons, function (index, value) {
                        $("#" + value).prop("disabled", false);
                    });


                    if (e.operation == "upload") {
                        _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
                    }
                },
                localization: {
                    select: 'Browse...'
                },
                upload: function (e) {
                    var itsItemsButtons = itemsButtons.split("|");
                    $.each(itsItemsButtons, function (index, value) {
                        $("#" + value).prop("disabled", true);
                    });


                    if ($("#cboTypeDetail").val() == "") {
                        _showErrorMessage("Error message", "Please select Type before upload a file.", "", 2, "");
                        e.preventDefault();
                    }
                    else {
                        e.data = { typeId: $("#txtOidReqAdd").val(), typeName: type, badgeNo: badgeNo, typeDetail: $("#cboTypeDetail").val() };
                    }
                }
            }).data("kendoUpload");
        }


        if (ucUploader_uploaderFile) {
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
            $(".k-upload.k-header").addClass("k-upload-empty");
            $(".k-upload-button").removeClass("k-state-focused");
            $("#lblInfo").text("");
            //$(".k-upload-files.k-reset").find("li").parent().remove();
            $(".k-upload-files.k-reset").find("li").remove();
        }

        ucUploader_uploaderFile.disable();
        cboTypeDetail.data("kendoComboBox").enable(true);
    }

    function ucUploader_uploaderInitEdit(oid, typeId, type, badgeNo, url_type, attachmentType) {

        $("#txtOidReqAdd").val(typeId);

        if (!cboTypeDetail) {
            cboTypeDetail = $("#cboTypeDetail").kendoComboBox({
                dataValueField: "VALUE",
                dataTextField: "TEXT",
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: { url: url_type, dataType: "json" }
                    },
                    sort: { field: "OID", dir: "ASC" }
                })
            });
        }
        else {
            $("#cboTypeDetail").data("kendoComboBox").value("");
        }

        if (!ucUploader_uploaderFile) {
            ucUploader_uploaderFile = $("#ucUploader_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload/PostUploadNew/1",
                    removeUrl: "remove",
                    //batch: false,
                    autoUpload: false
                },
                //showFileList: false,
                multiple: false,
                select: function (e) {
                    var files = e.files;

                    if ($("#cboTypeDetail").val() == "CRITIC" || $("#cboTypeDetail").val() == "MSDS" || $("#cboTypeDetail").val() == "CATALOG") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf") {
                                _showErrorMessage("Upload Error", "For Criticality Form, MSDS and Catalog Reference, only pdf files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "CHEMICAL") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf" && value.extension.toLowerCase() != ".doc" && value.extension.toLowerCase() != ".docx") {
                                _showErrorMessage("Upload Error", "For Chemical Form, only pdf and word files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "COMMUNICATION") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".msg") {
                                _showErrorMessage("Upload Error", "For Communication Document, only msg files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                },
                success: function (e) {
                    $("#lblInfo").text("Upload complete.");
                },
                error: function (e) {
                    var files = e.files;
                    var err = e.XMLHttpRequest;

                    if (e.operation == "upload") {
                        _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
                    }
                },
                localization: {
                    select: 'Browse...'
                },
                upload: function (e) {
                    if ($("#cboTypeDetail").val() == "") {
                        _showErrorMessage("Error message", "Please select Type before upload a file.", "", 2, "");
                        e.preventDefault();
                    }
                    else {
                        e.data = { typeId: typeId, typeName: type, badgeNo: badgeNo, typeDetail: $("#cboTypeDetail").val(), oid: oid, action: "EDIT" };
                    }
                }
            }).data("kendoUpload");
        }

        if (ucUploader_uploaderFile) {            
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
            $(".k-upload.k-header").addClass("k-upload-empty");
            $(".k-upload-button").removeClass("k-state-focused");
            $("#lblInfo").text("");
            $(".k-upload-files.k-reset").find("li").parent().remove();
            //$(".k-upload-files.k-reset").find("li").remove();
        }

        ucUploader_uploaderFile.enable();
        cboTypeDetail.data("kendoComboBox").value(attachmentType);
        cboTypeDetail.data("kendoComboBox").enable(false);
    }

    function ucUploader_uploaderInitReviseRespond(revisionID, oid, type, badgeNo, url_type, itemsButtons) {
        $("#txtOidReqAdd").val(oid);
        
        if (!cboTypeDetail) {
            cboTypeDetail = $("#cboTypeDetail").kendoComboBox({
                dataValueField: "VALUE",
                dataTextField: "TEXT",
                //dataTextField : "vatext",
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: { url: url_type, dataType: "json" }
                    },
                    sort: { field: "OID", dir: "ASC" }
                }),
                change: function () {
                    ucUploader_uploaderFile.enable();
                }
            });
        }
        else {
            var typeDS = new kendo.data.DataSource({
                transport: {
                    read: { url: url_type, dataType: "json" }
                },
                sort: { field: "OID", dir: "ASC" }

            });

            cboTypeDetail.data("kendoComboBox").setDataSource(typeDS);

            $("#cboTypeDetail").data("kendoComboBox").value("");
        }

        if (!ucUploader_uploaderFile) {
            ucUploader_uploaderFile = $("#ucUploader_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload/PostUploadRevise/1",
                    removeUrl: "remove",
                    //batch: false,
                    autoUpload: false
                },
                //showFileList: false,
                multiple: false,
                select: function (e) {
                    var files = e.files;

                    if ($("#cboTypeDetail").val() == "CRITIC" || $("#cboTypeDetail").val() == "MSDS" || $("#cboTypeDetail").val() == "CATALOG") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf") {
                                _showErrorMessage("Upload Error", "For Criticality Form, MSDS and Catalog Reference, only pdf files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "CHEMICAL") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".pdf" && value.extension.toLowerCase() != ".doc" && value.extension.toLowerCase() != ".docx") {
                                _showErrorMessage("Upload Error", "For Chemical Form, only pdf and word files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                    if ($("#cboTypeDetail").val() == "COMMUNICATION") {
                        $.each(files, function (index, value) {
                            if (value.extension.toLowerCase() != ".msg") {
                                _showErrorMessage("Upload Error", "For Communication Document, only msg files can be uploaded.", "", 3, "");
                                e.preventDefault();
                            }
                        });
                    }

                },
                success: function (e) {
                    var itsItemsButtons = itemsButtons.split("|");
                    $.each(itsItemsButtons, function (index, value) {
                        $("#" + value).prop("disabled", false);
                    });

                    $("#lblInfo").text("Upload complete.");
                },
                error: function (e) {
                    var files = e.files;
                    var err = e.XMLHttpRequest;

                    var itsItemsButtons = itemsButtons.split("|");
                    $.each(itsItemsButtons, function (index, value) {
                        $("#" + value).prop("disabled", false);
                    });


                    if (e.operation == "upload") {
                        _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
                    }
                },
                localization: {
                    select: 'Browse...'
                },
                upload: function (e) {
                    var itsItemsButtons = itemsButtons.split("|");
                    $.each(itsItemsButtons, function (index, value) {
                        $("#" + value).prop("disabled", true);
                    });


                    if ($("#cboTypeDetail").val() == "") {
                        _showErrorMessage("Error message", "Please select Type before upload a file.", "", 2, "");
                        e.preventDefault();
                    }
                    else {
                        e.data = { revisionId: revisionID, typeId: $("#txtOidReqAdd").val(), typeName: type, badgeNo: badgeNo, typeDetail: $("#cboTypeDetail").val() };
                    }
                }
            }).data("kendoUpload");
        }


        if (ucUploader_uploaderFile) {
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
            $(".k-upload.k-header").addClass("k-upload-empty");
            $(".k-upload-button").removeClass("k-state-focused");
            $("#lblInfo").text("");
            //$(".k-upload-files.k-reset").find("li").parent().remove();
            $(".k-upload-files.k-reset").find("li").remove();
        }

        ucUploader_uploaderFile.disable();
        cboTypeDetail.data("kendoComboBox").value("");
        cboTypeDetail.data("kendoComboBox").enable(true);
    }

</script>
<div id="ucUploaderWithTypeDetail" style="margin: 3px; padding: 10px; border: solid 1px #007e7a;">
    <fieldset class="adrFieldset">
        <legend class="adrLegend">Upload Document</legend>
        <div>
            <label class="labelForm" style="width: 30%;">Type</label>
            <input id="cboTypeDetail" style="width: 60%;"/>
        </div>       
        <div>
            <input type="file" id="ucUploader_uploaderFile" name="ucUploader_uploaderFile" />
        </div>
        <div id="info" style="margin-bottom: 10px">
            <label id="lblInfo" class="labelForm"></label>
        </div>
        <div  style="display:none">
            <input id="txtOidReqAdd" name="txtOidReqAdd" class="k-textbox"/>
        </div>
        
    </fieldset>


</div>