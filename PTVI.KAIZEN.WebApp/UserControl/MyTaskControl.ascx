﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyTaskControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.MyTask" %>

<div id="<%: this.ID %>_FormContent">
    <div id="<%: this.ID %>_KeyDeliverables">
        <div>
            <div id="<%: this.ID %>_KeyDeliverablesGrid" 
                data-role="grid"
                data-bind="source: dsMyTask, events: { dataBound: dataBound, excelExport: excelExport }",
                data-pageable="true",
                data-selectable="true",
                data-height="200",
                data-filterable="true",
                data-sortable="true",
                data-toolbar= "['excel']",
                data-excel= "{ allPages: true, fileName: 'MyTask.xlsx'}",
                data-columns="[
                    { 
                        field: 'pnoName', title: 'Project No. / Name ', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        field: 'tipeName', title: 'Type', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100, template: '#: tipeName #'
                    },
                    { 
                        field: 'desc', title: 'Description', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        title: 'Plan Date', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100,
                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                        columns: [
                                    {
                                        field: 'psd', title: 'Start',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: psd ? kendo.toString(kendo.parseDate(psd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    },
                                    {
                                        field: 'pfd', title: 'Finish',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: pfd ? kendo.toString(kendo.parseDate(pfd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    }]
                    },
                    { 
                        title: 'Actual Date', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100,
                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                        columns: [
                                    {
                                        field: 'asd', title: 'Start',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: asd ? kendo.toString(kendo.parseDate(asd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    },
                                    {
                                        field: 'afd', title: 'Finish',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: afd ? kendo.toString(kendo.parseDate(afd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    }]

                    },
                    { 
                        field: 'pic', title: 'PIC', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        field: 'statusName', title: 'Status', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        title: 'Updated', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100,
                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                        columns: [
                                    { 
                                        field: 'updatedByName', title: 'By', 
                                        attributes: { style: 'vertical-align: top !important;' },
                                        width: 80
                                    },
                                    { 
                                        field: 'updatedDate', title: 'Date', 
                                        attributes: { style: 'vertical-align: top !important;' },
                                        width: 70,template: '#: updatedDate ? kendo.toString(kendo.parseDate(updatedDate,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#'
                                    },
                                 ]
                    },
                        {   
                           width: 70, command: {text: 'Edit', name: 'editMyTask', click: <%: this.ID %>_vm.editMyTask }
                        }
                ]"
            ></div>
        </div>
    </div>

</div>
<div id="windowTask" style="display:none"></div>

<script type="text/x-kendo-template" id="kdTemplate">
    <div id='##<%: this.ID %>_taskFormData'>
        <div style='display: inline-block; width: 170px;'>Project Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.projectName # </div><br>

        <div style='display: inline-block; width: 170px;'>Key Deliverable Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.name # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Start Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.planStartDate ? kendo.toString(kendo.parseDate(taskData.planStartDate,'yyyy-MM-dd'), 'dd MMM yyyy') : ''  # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'>  #= taskData.planFinishDate ? kendo.toString(kendo.parseDate(taskData.planFinishDate,'yyyy-MM-dd'), 'dd MMM yyyy') : '' # </div><br>

        <div style='display: inline-block; width: 170px;'>Actual Start Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualStart' /></div><br>

        <div style='display: inline-block; width: 170px;'>Actual Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualFinish' /></div><br>

        <div style='display: inline-block; width: 170px;'>Status</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.statusName # </div><br>

        <div style='display: inline-block; width: 170px;'>Remark</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><textarea id='taRemark' class='k-textbox' style='width: 250px; height: 50px ' > #= taskData.remark ? taskData.remark : '' # </textarea> </div><br>

        <div style='display: inline-block; width: 170px;'>Weight Percentage</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px; width: 270px;'> #= taskData.weightPercentage # </div><br>

        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnHoldTask' class='k-button'>Hold Task</div></div>
        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnCancelTask' class='k-button'>Cancel Task</div></div>
        <div style='display: inline-block;padding-right: 20px'>&nbsp;</div>
        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnSave' class='k-button'>Save</div></div>
        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnCancel' class='k-button'>Cancel</div></div>
    </div>
</script>

<script type="text/x-kendo-template" id="ewpTemplate">
    <div id='##<%: this.ID %>_taskFormData'>
        <div style='display: inline-block; width: 170px;'>Project Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.projectName # </div><br>

        <div style='display: inline-block; width: 170px;'>Doc. No. / Title</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docNo # / #= taskData.title # </div><br>

        <div style='display: inline-block; width: 170px;'>PIC ID</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docById # </div><br>
        <div style='display: inline-block; width: 170px;'>PIC Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docByName # </div><br>
        <div style='display: inline-block; width: 170px;'>PIC Email</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docByEmail # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Start Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.planStartDate ? kendo.toString(kendo.parseDate(taskData.planStartDate,'yyyy-MM-dd'), 'dd MMM yyyy') : ''  # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'>  #= taskData.planFinishDate ? kendo.toString(kendo.parseDate(taskData.planFinishDate,'yyyy-MM-dd'), 'dd MMM yyyy') : '' # </div><br>

        <div style='display: inline-block; width: 170px;'>Actual Start Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualStart' /></div><br>

        <div style='display: inline-block; width: 170px;'>Actual Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualFinish' /></div><br>

        <div style='display: inline-block; width: 170px;'>Status</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.statusName # </div><br>

        <div style='display: inline-block; width: 170px; vertical-align: top'>Remark</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px; width: 370px;'><textarea id='taRemark' class='k-textbox' style='width: 350px; height: 100px ' > #= taskData.remark ? taskData.remark : '' # </textarea> </div><br>


        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnSave' class='k-button'>Save</div></div>
        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnCancel' class='k-button'>Cancel</div></div>
    </div>
</script>

<script type="text/x-kendo-template" id="dwgTemplate">
    <div id='##<%: this.ID %>_taskFormData'>
        <div style='display: inline-block; width: 170px;'>Project Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.projectName # </div><br>

        <div style='display: inline-block; width: 170px;'>Doc. No. / Title</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docNo # / #= taskData.title # </div><br>

        <div style='display: inline-block; width: 170px;'>PIC ID</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docById # </div><br>
        <div style='display: inline-block; width: 170px;'>PIC Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docByName # </div><br>
        <div style='display: inline-block; width: 170px;'>PIC Email</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.docByEmail # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Start Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.planStartDate ? kendo.toString(kendo.parseDate(taskData.planStartDate,'yyyy-MM-dd'), 'dd MMM yyyy') : ''  # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'>  #= taskData.planFinishDate ? kendo.toString(kendo.parseDate(taskData.planFinishDate,'yyyy-MM-dd'), 'dd MMM yyyy') : '' # </div><br>

        <div style='display: inline-block; width: 170px;'>Actual Start Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualStart' /></div><br>

        <div style='display: inline-block; width: 170px;'>Actual Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualFinish' /></div><br>

        <div style='display: inline-block; width: 170px;'>Status</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.statusName # </div><br>

        <div style='display: inline-block; width: 170px; vertical-align: top'>Remark</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px; width: 370px;'><textarea id='taRemark' class='k-textbox' style='width: 350px; height: 100px ' > #= taskData.remark ? taskData.remark : '' # </textarea> </div><br>


        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnSave' class='k-button'>Save</div></div>
        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnCancel' class='k-button'>Cancel</div></div>
    </div>
</script>

<script type="text/x-kendo-template" id="coTemplate">
    <div id='##<%: this.ID %>_taskFormData'>
        <div style='display: inline-block; width: 170px;'>Project Name</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.projectName # </div><br>

        <div style='display: inline-block; width: 170px;'>Role</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.roleName # </div><br>

        <div style='display: inline-block; width: 170px;'>Plan Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'>  #= taskData.planFinishDate ? kendo.toString(kendo.parseDate(taskData.planFinishDate,'yyyy-MM-dd'), 'dd MMM yyyy') : '' # </div><br>

        <div style='display: inline-block; width: 170px;'>Actual Finish Date</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'><input id='##<%: this.ID %>_taskDateActualFinish' /></div><br>

        <div style='display: inline-block; width: 170px;'>Status</div>
        <div style='display: inline-block; margin-bottom: 10px; margin-right: 20px'> #= taskData.statusName # </div><br>

        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnSave' class='k-button'>Save</div></div>
        <div style='display: inline-block'><div id='##<%: this.ID %>_taskBtnCancel' class='k-button'>Cancel</div></div>
    </div>
</script>

 <script>
     var taskData = {};

     $("#windowTask").kendoWindow({
         title: "Task ",
         content: {
             template: ""
         }
     });
     var dtWindow = $("#windowTask").getKendoWindow();

     function showTaskData(dataRow, data)
     {
         //kendo window

         //form task
         var taskType = dataRow.tipe.replace(/\s/g, "");
         var winTitle = "";
         if (taskType == "KeyDeliverable") {
             dtWindow.options.content.template = kendo.template($('#kdTemplate').html());
             winTitle = "Key Deliverables";
         }
         else if (taskType == "EWP") {
             dtWindow.options.content.template = kendo.template($('#ewpTemplate').html());
             winTitle = "EWP Compliance";
         }
         else if (taskType == "DWG"){
             dtWindow.options.content.template = kendo.template($('#dwgTemplate').html());
             winTitle = "Drawing";
         }
         else if (taskType == "DC") {
             dtWindow.options.content.template = kendo.template($('#dwgTemplate').html());
             winTitle = "Design Conformity";
         }
         else if (taskType == "CloseOutTask") {
             dtWindow.options.content.template = kendo.template($('#coTemplate').html());
             winTitle = "Project Close Out";
         }
    dtWindow.center().open();
         dtWindow.refresh();
         
         dtWindow.title("Task " + winTitle);
         
         //bind form
         //bind save event
     }

     function editMyTask(e) {
         var dataRow = this.dataItem($(e.currentTarget).closest("tr"));
         var taskType = dataRow.tipe.replace(/\s/g, "");
         taskData = {};
         kendo.ui.progress($("#<%: this.ID %>_KeyDeliverablesGrid"), true);
         $.ajax({
             url: _webApiUrl + "myTask/getTaskData/" + dataRow.id+"?taskType="+taskType,
             type: "GET",
             async: false,
             success: function (data) {
                kendo.ui.progress($("#<%: this.ID %>_KeyDeliverablesGrid"), false);
                 taskData = data;
                 showTaskData(dataRow, data);
                 $("#<%: this.ID %>_taskDateActualStart").kendoDatePicker({
                     value: kendo.parseDate(taskData.actualStartDate, 'yyyy-MM-dd'),
                     format: 'dd MMM yyyy'
                 });
                 $("#<%: this.ID %>_taskDateActualFinish").kendoDatePicker({
                     value: kendo.parseDate(taskData.actualFinishDate, 'yyyy-MM-dd'),
                     format: 'dd MMM yyyy'
                 });

                 $("#<%: this.ID %>_taskBtnCancel").click(function () {
                     dtWindow.close();
                 });

                 $("#<%: this.ID %>_taskBtnHoldTask").click(function () {
                         $.ajax({
                             url: _webApiUrl + "myTask/putTaskData/" + dataRow.id,
                             type: "PUT",
                             async: false,
                             dataType: 'json',
                             data: {
                                 taskType: taskType, status: "S5"
                             },
                             success: function (data) {
                                 <%: this.ID %>_vm.dsMyTask.read();
                            //     <%: this.ID %>_vm.ds_Deliverable.read();
                                 //$("#<%: this.ID %>_KeyDeliverablesGrid").getKendoGrid().refresh();

                                 dtWindow.close();
                             }
                         });
                 });

                 $("#<%: this.ID %>_taskBtnCancelTask").click(function () {
                         $.ajax({
                             url: _webApiUrl + "myTask/putTaskData/" + dataRow.id,
                             type: "PUT",
                             async: false,
                             dataType: 'json',
                             data: {
                                 taskType: taskType, status: "S3"
                             },
                             success: function (data) {
                                 <%: this.ID %>_vm.dsMyTask.read();
                       //          <%: this.ID %>_vm.ds_Deliverable.read();
                                 //$("#<%: this.ID %>_KeyDeliverablesGrid").getKendoGrid().refresh();

                                 dtWindow.close();
                             }
                         });
                 });

                 $("#<%: this.ID %>_taskBtnSave").click(function (e) {
                     if ((taskType=="CloseOutTask" && $("#<%: this.ID %>_taskDateActualFinish").getKendoDatePicker().value()==null) || 
                         (($("#<%: this.ID %>_taskDateActualFinish").getKendoDatePicker().value() == null) && ($("#<%: this.ID %>_taskDateActualStart").getKendoDatePicker().value() == null))) {
                         alert("Actual Start / Finish must not empty");
                         return 0;
                     }
                     var status = "";
                     var remark = "";
                     if ($("#taRemark").val() != "") remark = $("#taRemark").val();
                     if ($("#<%: this.ID %>_taskDateActualFinish").getKendoDatePicker().value() != null) status = "S1";

                         $.ajax({
                             url: _webApiUrl + "myTask/putTaskData/" + dataRow.id,
                             type: "PUT",
                             async: false,
                             dataType: 'json',
                             data: {
                                 taskType: taskType, actualStartDate: $("#<%: this.ID %>_taskDateActualStart").getKendoDatePicker().value() != null ? $("#<%: this.ID %>_taskDateActualStart").getKendoDatePicker().value().toJSON() : null,
                                 actualFinishDate: $("#<%: this.ID %>_taskDateActualFinish").getKendoDatePicker().value() != null ? $("#<%: this.ID %>_taskDateActualFinish").getKendoDatePicker().value().toJSON() : null, status: status,
                                 remark: remark
                             },
                             success: function (data) {
                                 <%: this.ID %>_vm.dsMyTask.read();
                      //           <%: this.ID %>_vm.ds_Deliverable.read();
                                 //$("#<%: this.ID %>_KeyDeliverablesGrid").getKendoGrid().refresh();

                                 dtWindow.close();
                             }
                         });
                 });

             },
             error: function (data) {
                 //return [];
             }
         });

     }

     function existInArray(t,ar) {
         for (var i = 0; i < ar.length; i++) {
             if (ar[i] === t)
                 return i;
         }
         return -1;
     }

     function excelExport(e) {
         var sheet = e.workbook.sheets[0];
         var columns = sheet.columns;
         columns.forEach(function (column) {
             delete column.width;
             column.autoWidth = true;
         });
         var row = sheet.rows[0];
         row.cells[3].textAlign = "center";
         row.cells[4].textAlign = "center";
         row.cells[7].textAlign = "center";
         var ar = [3, 4, 5, 6, 10];
         for (var rowIndex = 2; rowIndex < sheet.rows.length; rowIndex++) {
             var row = sheet.rows[rowIndex];
             for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                 if (existInArray(cellIndex, ar)>=0) {
                     row.cells[cellIndex].value = kendo.toString(kendo.parseDate(row.cells[cellIndex].value, 'yyyy-MM-dd'), 'dd MMM yyyy');
                     row.cells[cellIndex].format = "dd-MMM-yyyy";
                 }
                     //if ([3, 4, 5, 6, 10].find(function (index) { return index === cellIndex })) {
                     //    row.cells[cellIndex].value = kendo.toString(kendo.parseDate(row.cells[cellIndex].value, 'yyyy-MM-dd'), 'dd MMM yyyy');
                     //    row.cells[cellIndex].format = "dd-MMM-yyyy";
                     //}
                 }
             }
     }

     var <%: this.ID %>_vm = kendo.observable({
         editMyTask: editMyTask,
         excelExport : excelExport,
         dataBound: function (e) {
             var grid = $("#<%: this.ID %>_KeyDeliverablesGrid").getKendoGrid();

             grid.tbody.find("tr[role='row']").each(function () {
                 var model = grid.dataItem(this);
                 _projectId = model.pId;
                 if (!model.editable) {
                     $(this).find(".k-grid-editMyTask").addClass("k-state-disabled");
                 }

                 if (model.tipe == "Close Out Task") {
                         $(this).find(".k-grid-editMyTask").hide();
                 }
             });
         },
        dsMyTask: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_KeyDeliverablesGrid"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_KeyDeliverablesGrid"), false);
            },
            transport: {
                read: {
                    url: _webApiUrl + "mytask/getmytask/" + _currBadgeNo,
                    dataType: "json"
                },
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        pNo: { type: "string" },
                        tipe: { type: "string" },
                        tipeName: { type: "string" },
                        name: { type: "string" },
                        pic: { type: "string" },
                        afd: { type: "date" },
                        pfd: { type: "date" },
                        asd: { type: "date" },
                        psd: { type: "date" },
                        status: { type: "number" },
                        statusName: { type: "string" },
                        updatedBy: { type: "string" },
                        updatedDate: { type: "date" },
                    }
                },
                parse: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        if (response[i].pic != null)
                        response[i].pic = response[i].pic.replace(/,/g, "\n");
                    }
                    
                  // <%: this.ID %>_vm.ds_Deliverable.data().push.apply(<%: this.ID %>_vm.ds_Deliverable.data(), response[0]);
                    return response;
                }
            },
            sort: [{ field: "psd", dir: "asc" }, { field: "pfd", dir: "asc" }]
        }),

        ds_Deliverable: new kendo.data.DataSource({
            pageSize: 10,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        pNo: { type: "string" },
                        tipe: { type: "string" },
                        name: { type: "string" },
                        pic: { type: "string" },
                        afd: { type: "date" },
                        pfd: { type: "date" },
                        asd: { type: "date" },
                        psd: { type: "date" },
                        status: { type: "number" },
                        statusName: { type: "string" },
                        updatedBy: { type: "string" },
                        updatedDate: { type: "date" },
                    }
                },
            },
            parse: function (response) {


            }
        }),

    }); // View Model
    <%: this.ID %>_vm.dsMyTask.read();
    function <%: this.ID %>_DataBind() {
       
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_vm);
    }
 </script>