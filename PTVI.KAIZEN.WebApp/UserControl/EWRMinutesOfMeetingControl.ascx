﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EWRMinutesOfMeetingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EWRMinutesOfMeetingControl" %>

<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<div id="RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Input Minutes Of Meeting</legend>
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend"></legend>
                <div>
                    <label class="labelFormWeight">Participants:<span style="color: red">*</span></label>
                    <div style="width: 400px; text-align: left;display:inline-block;">
                        <select
                            data-role="multiselect"
                            data-placeholder="Type Employee Name (min 3 char)"
                            data-text-field="FULL_NAME"
                            data-value-field="EMPLOYEEID"
                            data-auto-bind="false"
                            data-min-length=3
                            data-filter="contains"
                            data-bind="value: allParticipants, source: dsParticipant, enabled: isEnabled,
                                events: {
                                    filtering: autocompleteFiltering
                                }"
                        ></select>
                    </div>
                </div>
                <div>
                    <label class="labelFormWeight">Distributions:<span style="color: red">*</span></label>
                    <div style="width: 400px; text-align: left;display:inline-block;">
                        <select
                            data-role="multiselect"
                            data-placeholder="Type Employee Name (min 3 char)"
                            data-text-field="FULL_NAME"
                            data-value-field="EMPLOYEEID"
                            data-auto-bind="false"
                            data-min-length=3
                            data-filter="contains"
                            data-bind="value: allDistributions, source: dsDistribution, enabled: isEnabled,
                                events: {
                                    filtering: autocompleteFiltering
                                }"
                        ></select>
                    </div>
                </div>
                <div>
                    <label class="labelFormWeight">Date/Time<span style="color: red">*</span></label>
                    <input data-role="datetimepicker"
                       data-bind="value: momData.momDate, enabled: isEnabled">
                    
                </div>
                <div>
                    <label class="labelFormWeight">Venue<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.venue, enabled: isEnabled" placeholder="Venue" maxlength="200" required />
                </div>
                <div>
                    <label class="labelFormWeight">Minutes Recorded by<span style="color: red">*</span></label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.recordedBy, enabled: isEnabled" placeholder="Minutes Recorded by" required />--%>
                    <input data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-auto-bind="false"
                        data-filter="contains"
                        data-bind="value: momData.recordedBy,
                                    source: dsRecordedBy,
                                    events: {
                                        select: onMinutesRecordedBy,
                                        filtering: autocompleteFiltering
                                    },
                                    enabled: isEnabled"
                        style="width: 400px;"
                        required
                    />
                </div>
            </fieldset>
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend">Engineering Work Request Information</legend>
                <div>
                    <label class="labelFormWeight">Area</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.AREA" placeholder="Area" disabled />
                </div>
                <div>
                    <label class="labelFormWeight">Subject</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.SUBJECT" placeholder="Subject" disabled />
                </div>
                <div>
                    <label class="labelFormWeight">Budget Allocated</label>
                    <input data-role="numerictextbox"
                        data-format="c"
                        data-min="0"
                        data-bind="value: requestData.BUDGETALLOC"
                        style="width: 180px" disabled>
                </div>
                <div>
                    <label class="labelFormWeight">Account Code</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.ACCCODE" placeholder="Account Code" disabled />
                </div>
                <div>
                    <fieldset class="gdiFieldset">
                        <legend class="gdiLegend">Type of Requested Work</legend>
                        <div class="k-form-field" id="<%: this.ID %>_typeofrequestedwork">
                            <input type="checkbox" id="<%: this.ID %>_typeReq1" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="budgetestimate" disabled />
                            <label class="k-checkbox-label" for="<%: this.ID %>_typeReq1" style="font-size: 10pt; padding-right: 20px;">Budget Estimate</label>

                            <input type="checkbox" id="<%: this.ID %>_typeReq2" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="purchaseonly" disabled />
                            <label class="k-checkbox-label" for="<%: this.ID %>_typeReq2" style="font-size: 10pt; padding-right: 20px;">Purchase Only</label>

                            <input type="checkbox" id="<%: this.ID %>_typeReq3" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="projectmanagement" disabled />
                            <label class="k-checkbox-label" for="<%: this.ID %>_typeReq3" style="font-size: 10pt; padding-right: 20px;">Project Management (Capital Only)</label>

                            <br />
                            <input type="checkbox" id="<%: this.ID %>_typeReq4" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="engineeringwork" disabled />
                            <label class="k-checkbox-label" for="<%: this.ID %>_typeReq4" style="font-size: 10pt; padding-right: 20px;">Engineering Work Package (Design and Drafting)</label>

                            <br />
                            <input type="checkbox" id="<%: this.ID %>_typeReq5" data-bind="checked: itemsRequestedWork, events: { change: onOtherChange }" class="k-checkbox" value="other" disabled />
                            <label class="k-checkbox-label" for="<%: this.ID %>_typeReq5" style="font-size: 10pt; padding-right: 20px;">Other (Please Specify)</label>
                            <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: typeOfRequestedOther" placeholder="Other" disabled/>
                        </div>
                    </fieldset>
                    <%--<h5>Type of Requested Work</h5>
                    <br />
                    <ul class="fieldlist">
                        <li>
                            <input type="checkbox" id="typeReq1" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="budgetestimate" disabled />
                            <label class="k-checkbox-label labelForm" for="typeReq1" style="width:80%;">Budget Estimate</label>
                        </li>
                        <li>
                            <input type="checkbox" id="typeReq2" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="purchaseonly" disabled />
                            <label class="k-checkbox-label labelForm" for="typeReq2" style="width:80%;">Purchase Only</label>
                        </li>
                        <li>
                            <input type="checkbox" id="typeReq3" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="projectmanagement" disabled />
                            <label class="k-checkbox-label labelForm" for="typeReq3" style="width:80%;">Project Management (Capital Only)</label>
                        </li>
                        <li>
                            <input type="checkbox" id="typeReq4" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="engineeringwork" disabled />
                            <label class="k-checkbox-label labelForm" for="typeReq4" style="width:80%;">Engineering Work Package (Design and Drafting)</label>
                        </li>
                        <li>
                            <input type="checkbox" id="typeReq5" data-bind="checked: itemsRequestedWork, events: { change: onOtherChange }" class="k-checkbox" value="other" disabled />
                            <label class="k-checkbox-label labelForm" for="typeReq5">Other (Please Specify)</label>
                            <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: typeOfRequestedOther" placeholder="Other" disabled/>
                        </li>

                    </ul>--%>

                </div>
            </fieldset>
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend">Conclusion</legend>
                <div>
                    <label class="labelFormWeight">Background<span style="color: red">*</span></label>
                    <textarea class="k-textbox" rows="5" style="width: 400px;" data-bind="value: momData.background, enabled: isEnabled" placeholder="Background" required></textarea>
                </div>
                <div>
                    <label class="labelFormWeight">Scope Of Work<span style="color: red">*</span></label>
                    <textarea class="k-textbox" rows="5" style="width: 400px;" data-bind="value: momData.scopeOfWork, enabled: isEnabled" placeholder="Scope of Work" required></textarea>
                </div>
                <div>
                    <fieldset class="gdiFieldset">
                        <legend class="gdiLegend">Project Type</legend>
                        <div class="k-form-field" id="<%: this.ID %>_projecttype">
                            <input type="checkbox" id="projType1" data-bind="checked: itemsProjectType, enabled: isEnabled" class="k-checkbox" value="engineeringworkrequest">
                            <label class="k-checkbox-label" for="projType1" style="font-size: 10pt; padding-right: 20px;">Engineering Work Request</label>

                            <input type="checkbox" id="projType2" data-bind="checked: itemsProjectType, enabled: isEnabled" class="k-checkbox" value="technicalassistance">
                            <label class="k-checkbox-label" for="projType2" style="font-size: 10pt; padding-right: 20px;">Technical Assistance</label>
                            
                            <br />
                            <input type="checkbox" id="projType3" data-bind="checked: itemsProjectType, events: { change: onOtherChange }, enabled: isEnabled" class="k-checkbox" value="other">
                            <label class="k-checkbox-label" for="projType3" style="font-size: 10pt; padding-right: 20px;">Others</label>
                            <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: projectTypeOther, enabled: isOtherChecked" maxlength="500" placeholder="Other"/>
                        </div>
                    </fieldset>
                    <%--<h5>Project Type</h5>
                    <br />
                    <ul class="fieldlist">
                        <li>
                            <input type="checkbox" id="projType1" data-bind="checked: itemsProjectType, enabled: isEnabled" class="k-checkbox" value="engineeringworkrequest">
                            <label class="k-checkbox-label labelForm" for="projType1" style="width:80%;">Engineering Work Request</label>
                        </li>
                        <li>
                            <input type="checkbox" id="projType2" data-bind="checked: itemsProjectType, enabled: isEnabled" class="k-checkbox" value="technicalassistance">
                            <label class="k-checkbox-label labelForm" for="projType2" style="width:80%;">Technical Assistance</label>
                        </li>
                        <li>
                            <input type="checkbox" id="projType3" data-bind="checked: itemsProjectType, events: { change: onOtherChange }, enabled: isEnabled" class="k-checkbox" value="other">
                            <label class="k-checkbox-label labelForm" for="projType3" style="width:80%;">Others</label>
                            <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: projectTypeOther, enabled: isOtherChecked" maxlength="500" placeholder="Other"/>

                        </li>
                    </ul>--%>
                </div>
                <div>
                    <label class="labelFormWeight">EWR No</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.ewrNo, enabled: isEnabled" maxlength="50" placeholder="EWR No" />
                </div>
                <div>
                    <label class="labelFormWeight">Project Title<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.projectTitle, enabled: isEnabled" maxlength="200" placeholder="Project Title" required />
                </div>
                <div>
                    <label class="labelFormWeight">Project Manager<span style="color: red">*</span></label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.projectManager, enabled: isEnabled" placeholder="Project Manager" required />--%>
                    <input data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-auto-bind="false"
                        data-filter="contains"
                        data-bind="value: momData.projectManager,
                                    source: dsProjectManager,
                                    enabled: isEnabled,
                                    events: {
                                        select: onProjectManagerSelect,
                                        filtering: autocompleteFiltering
                                    }"
                        style="width: 400px;"
                        required
                    />
                </div>
                <div>
                    <label class="labelFormWeight">Project Deliverables<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.projectDeliverable, enabled: isEnabled" maxlength="50" placeholder="Project Deliverables" required />
                </div>
                <div>
                    <label class="labelFormWeight">Action Time Line<span style="color: red">*</span></label>
                    <input data-role="datepicker" data-format="dd MMM yyyy" data-bind="value: momData.actionTimeLine, enabled: isEnabled" placeholder="Action Time Line" />
                </div>
                <div>
                    <label class="labelFormWeight">Operational Contact Person<span style="color: red">*</span></label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.OPERATIONALCP" placeholder="Sponsor Contact Person" disabled />--%>
                    <input id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-filter="contains"
                        data-auto-bind="false"
                        data-bind="value: requestData.OPERATIONALCP,
                                    source: dsOperationalCP,
                                    
                                    events: {
                                        select: onOperationalCPSelected,
                                        close: onOperationalCPClose,
                                        filtering: autocompleteFiltering
                                    }"
                        style="width: 400px;"
                        required data-required-msg="Operational Contact Person is required"
                    />
                </div>
                <div>
                    <label class="labelFormWeight">Operational Contact Person 2</label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.OPERATIONALCP2" placeholder="Operational Contact Person" disabled />--%>
                    <input id="<%: this.ID %>_operationalCP2" name="<%: this.ID %>_operationalCP2" data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-filter="contains"
                        data-auto-bind="false"
                        data-bind="value: requestData.OPERATIONALCP2,
                                    source: dsOperationalCP2,
                                    events: {
                                        select: onOperationalCP2Selected,
                                        close: onOperationalCP2Close,
                                        filtering: autocompleteFiltering
                                    }"
                        style="width: 400px;"
                        
                    />
                </div>
                <div>
                    <label class="labelFormWeight">Data Required From Sponsor<span style="color: red">*</span></label>
                    <textarea class="k-textbox" rows="4" style="width: 400px;" data-bind="value: momData.dataRequired, enabled: isEnabled" placeholder="Data Required" required ></textarea>
                </div>
            </fieldset>
        </fieldset>
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>

<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        ewrID: 0,
        isReadonly: true,
        parentForm: "",
        itemsProjectType: [],
        itemsRequestedWork: [],
        allParticipants: [],
        allDistributions: [],
        isOtherChecked: false,
        projectTypeOther: "",
        typeOfRequestedOther: "",
        recordedByEmail: "",
        recordedByBN: "",
        projectManagerEmail: "",
        projectManagerBN: "",
        operationalCPFullName: "",
        operationalCP2FullName: "",
        requestData: new common_ewrRequest(),
        momData: new common_ewrMom(),
        onOtherChange: function (e) {

            this.set("isOtherChecked", !this.get("isOtherChecked"));
            if (!this.get("isOtherChecked")) {
                this.set("projectTypeOther", "");
            }

        },
        isEnabled: true,
        onValidationEntry: function () {
            var validator = $("#RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validator.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validator.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }

            // cek jika pilih other di project type, maka descriptionnya harus diisi
            var isProjectTypeValid = true;
            $.each(this.itemsProjectType, function (idx, el) {
                if (el == "other") {
                    if (!<%: this.ID %>_viewModel.get("projectTypeOther")) {
                        isProjectTypeValid = false;
                    }
                }
            });
            if (!isProjectTypeValid) {
                _showDialogMessage("Required Message", "Please fill Other's description in Project Type", "");
                return false;
            }

            // cek jika pilih other di requested work, maka descriptionnya harus diisi
            var isRequestedWorkValid = true;
            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    if (!<%: this.ID %>_viewModel.get("typeOfRequestedOther")) {
                        isRequestedWorkValid = false;
                    }
                }
            });
            if (!isRequestedWorkValid) {
                _showDialogMessage("Required Message", "Please fill Other's description in Type of Requested Work", "");
                return false;
            }

            return isValid;
        },
        populateData: function () {
            //console.log("Populate Data");
            var typeOfProjectType = [];
            var otherDesc = this.projectTypeOther;
            $.each(this.itemsProjectType, function (idx, el) {
                
                if (el == "other") {
                    typeOfProjectType.push({ TYPE: "itemsProjectType", VALUE: el, DESCRIPTION: otherDesc });
                } else {
                    typeOfProjectType.push({ TYPE: "itemsProjectType", VALUE: el });
                }
            });
            //console.log(typeOfProjectType);
            var allParticipants = [];
            $.each(this.allParticipants, function (idx, el) {
                allParticipants.push(el.EMPLOYEEID + "-" + el.FULL_NAME + "-" + el.EMAIL);
            });
            //console.log(allParticipants);
            var allDistributions = [];
            $.each(this.allDistributions, function (idx, el) {
                allDistributions.push(el.EMPLOYEEID + "-" + el.FULL_NAME + "-" + el.EMAIL);
            });
            
            var momData =
                {
                    OID: "-1",
                    EWRREQUESTID: this.ewrID,
                    PARTICIPANTS: allParticipants.join(","),
                    DISTRIBUTIONS: allDistributions.join(","),
                    MOMDATE: kendo.toString(this.momData.momDate, 'yyyy-MM-dd HH:mm:ss'),//kendo.toString(this.requestData.dateIssued, 'yyyy-MM-dd'),
                    VENUE: this.momData.venue,
                    RECORDEDBY: this.momData.recordedBy,
                    RECORDEDBYBN: this.recordedByBN,
                    RECORDEDBYEMAIL: this.recordedByEmail,
                    BACKGROUND: this.momData.background,
                    SCOPEOFWORK: this.momData.scopeOfWork,
                    PROJECTTITLE: this.momData.projectTitle,
                    PROJECTMANAGER: this.momData.projectManager,
                    PROJECTMANAGERBN: this.projectManagerBN,
                    PROJECTMANAGEREMAIL: this.projectManagerEmail,
                    PROJECTDELIVERABLE: this.momData.projectDeliverable,
                    ACTIONTIMELINE: kendo.toString(this.momData.actionTimeLine, 'yyyy-MM-dd'),
                    OPERATIONALCPNAME: this.get("operationalCPFullName"),
                    OPERATIONALCPEMAIL: this.requestData.OPERATIONALCPEMAIL,
                    OPERATIONALCPBN: this.requestData.OPERATIONALCPBN,
                    OPERATIONALCP2NAME: this.get("operationalCP2FullName"),
                    OPERATIONALCP2EMAIL: this.requestData.OPERATIONALCP2EMAIL,
                    OPERATIONALCP2BN: this.requestData.OPERATIONALCP2BN,
                    DATAREQUIRED: this.momData.dataRequired,
                    EWRNO: this.momData.ewrNo,
                    PROJECTTYPE: typeOfProjectType
                }
            //console.log(momData);
            //console.log("--Populate Data");
            return momData;
        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        onChangeParticipants: function() {

        },
        //dsParticipant: new kendo.data.DataSource({
        //    type: "odata",
        //    serverFiltering: true,
        //    transport: {
        //        read: {
        //            url: _webOdataUrl + "EmployeeOData",
        //            dataType: "json"
        //        }
               
        //    },
        //    schema: {ds
        //        data: function (data) {
        //            return data.value;
        //        },
        //        total: function (data) {
        //            return data["odata.count"];
        //        },
        //        model: {
        //            id: "EMPLOYEEID"
        //        }
        //    }
        //}),
        //dsDistribution: new kendo.data.DataSource({
        //    type: "odata",
        //    serverFiltering: true,
        //    transport: {
        //        read: {
        //            url: _webOdataUrl + "EmployeeOData",
        //            dataType: "json"
        //        }

        //    },
        //    schema: {
        //        data: function (data) {
        //            return data.value;
        //        },
        //        total: function (data) {
        //            return data["odata.count"];
        //        },
        //        model: {
        //            id: "EMPLOYEEID"
        //        }
        //    }
        //}),
        dsEmployee: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsParticipant: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsDistribution: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsRecordedBy: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsProjectManager: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsOperationalCP: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsOperationalCP2: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        autocompleteFiltering: function(e) {
            var filter = e.filter;
            
            if (filter) {
                if (!filter.value) {
                    //prevent filtering if the filter does not value
                    e.preventDefault();
                }
            } else {
                e.preventDefault();
            }
            
        },
        onMinutesRecordedBy: function (e) {
               
            <%: this.ID %>_viewModel.set("recordedByBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("recordedByEmail", e.dataItem.EMAIL);
        },
        onProjectManagerSelect: function (e) {
               
            <%: this.ID %>_viewModel.set("projectManagerBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("projectManagerEmail", e.dataItem.EMAIL);
        },
        onClearEvent: function (e) {
            this.requestData.set("ewrNo", null);
            this.requestData.set("subject", null);
            this.requestData.set("area", null);
            this.requestData.set("dateIssued", null);
            this.requestData.set("accCode", null);
            this.requestData.set("networkNo", null);
            this.requestData.set("budgetAlloc", null);
            this.requestData.set("dateCompletion", null);
            this.requestData.set("problemStatement", null);
            this.requestData.set("objective", null);
            this.requestData.set("assignedPM", null);
            this.requestData.set("projectSponsor", null);
            this.requestData.set("sponsorLocation", null);
            this.requestData.set("sponsorPhoneNo", null);
            this.requestData.set("operationalCP", null);
            this.requestData.set("projectTitle", null);
            this.requestData.set("status", null);

        },
        onOperationalCPSelected: function (e) {
            <%: this.ID %>_viewModel.set("operationalCPFullName", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCPBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCPEMAIL", e.dataItem.EMAIL);
            
        },
        onOperationalCPClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onOperationalCP2Selected: function (e) {

            <%: this.ID %>_viewModel.set("operationalCP2FullName", common_toTitleCase(e.dataItem.FULL_NAME));

            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCP2BN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCP2EMAIL", e.dataItem.EMAIL);
            
        },
        onOperationalCP2Close: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
    });

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    function <%: this.ID %>_initForm(ewrID) {
        <%: this.ID %>_viewModel.set("ewrID", ewrID);
    }

    function <%: this.ID %>_Init(dataRequest, parentForm, isReadOnly) {
        <%: this.ID %>_viewModel.set("ewrID", dataRequest.OID); 
        
        <%: this.ID %>_viewModel.set("isEnabled", !isReadOnly);
        
        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        <%: this.ID %>_viewModel.set("requestData", dataRequest);
        <%: this.ID %>_viewModel.set("operationalCPFullName", dataRequest.OPERATIONALCP);
        <%: this.ID %>_viewModel.set("operationalCP2FullName", dataRequest.OPERATIONALCP2);
        $.ajax({
            url: _webApiUrl + "momewrs/FindMomEwrByRequest/" + dataRequest.OID,
            type: "GET",
            success: function (result) {
                if (result) {
                    var momData = {
                        OID: result.OID,
                        ewrID: result.EWRREQUESTID,
                        distribution: result.DISTRIBUTION,
                        momDate: result.MOMDATE,//kendo.toString(this.requestData.dateIssued, 'yyyy-MM-dd'),
                        venue: result.VENUE,
                        recordedBy: result.RECORDEDBY,
                        background: result.BACKGROUND,
                        scopeOfWork: result.SCOPEOFWORK,
                        projectTitle: result.PROJECTTITLE,
                        projectManager: result.PROJECTMANAGER,
                        projectDeliverable: result.PROJECTDELIVERABLE,
                        actionTimeLine: result.ACTIONTIMELINE,
                        //operationalCP: result.OPERATIONALCP,
                        //operationalCPBN: result.OPERATIONALCPBN,
                        //operationalCPEmail: result.OPERATIONALCPEMAIL,
                        //operationalCP2: result.OPERATIONALCP2,
                        //operationalCP2BN: result.OPERATIONALCP2BN,
                        //operationalCP2Email: result.OPERATIONALCP2EMAIL,
                        dataRequired: result.DATAREQUIRED,
                        ewrNo: result.EWRNO,
                        typeOfProjectType: result.PROJECTTYPE
                    }
                    <%: this.ID %>_viewModel.set("momData", momData);

                    var itemsProjectType = [];
                    $.each(result.EWRREQUESTDETAILs, function (idx, elem) {
                        itemsProjectType.push(elem.VALUE);
                        if (elem.VALUE == "other") <%: this.ID %>_viewModel.set("projectTypeOther", elem.DESCRIPTION);
                    });
                    <%: this.ID %>_viewModel.set("itemsProjectType", itemsProjectType);
                }
                
            },
            error: function (jqXHR) {
            }
        });

        var requestedWork = [];
        $.each(dataRequest.TYPEOFREQUESTEDWORK, function (idx, elem) {
            requestedWork.push(elem.VALUE);
            if (elem.VALUE == "other") <%: this.ID %>_viewModel.set("typeOfRequestedOther", elem.DESCRIPTION);
        });
        <%: this.ID %>_viewModel.set("itemsRequestedWork", requestedWork);

        // User Participants
        var userParticipants = [];
        $.each(dataRequest.USERPARTICIPANTS, function (idx, elem) {
            userParticipants.push({ full_Name: elem.EMPLOYEENAME, employeeId: elem.EMPLOYEEID });
            
        });
        <%: this.ID %>_viewModel.set("allParticipants", userParticipants);

        // User Distributions
        var userDistributions = [];
        $.each(dataRequest.USERDISTRIBUTIONS, function (idx, elem) {
            userDistributions.push({ full_Name: elem.EMPLOYEENAME, employeeId: elem.EMPLOYEEID });
            
        });
        <%: this.ID %>_viewModel.set("allDistributions", userDistributions);

        // set is other to false if readonly = true
        if (isReadOnly) {
            <%: this.ID %>_viewModel.set("isOtherChecked", false);
        }
        
    }
    // Document Ready
    $(document).ready(function () {
        <%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#RequestContent"), <%: this.ID %>_viewModel);

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        //<%: this.ID %>_viewModel.onClearEvent()
        
        <%: this.ID %>_viewModel.requestData.set("dateIssued", new Date());

    }); //doc ready

</script>
