﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportWorkingHoursMainControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportWorkingHoursMainControl" %>
<%@ Register Src="~/UserControl/ReportWorkingHourByEngineerControl.ascx" TagPrefix="uc1" TagName="ReportWorkingHourByEngineerControl" %>
<%@ Register Src="~/UserControl/ReportWorkhoursConsumption.ascx" TagPrefix="uc1" TagName="ReportWorkhoursConsumption" %>
    <div id="OtherDepartmentForm">
        <div class="k-content wide">
            <div>
                <div id="SendRequestTabStrip">
                    <ul>
                        <li>by Engineer</li>
                        <li>by Area</li>
                    </ul>
                    <div>
                        <uc1:ReportWorkingHourByEngineerControl runat="server" ID="ReportWorkingHourByEngineerControl" />
                    </div>
                    <div>
                        <uc1:ReportWorkhoursConsumption runat="server" ID="ReportWorkhoursConsumption1" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        var <%: this.ID %>_OtherDepartmentVM = kendo.observable({
            selectedTabIndex: 0
        });

        var onShow = function (e) {
            SendRequestTabStrip = $("#SendRequestTabStrip").getKendoTabStrip();
            if (SendRequestTabStrip.value() == "Request List") {
                $("#RequestList_grdRequestList").getKendoGrid().dataSource.read();
                $("#RequestList_grdRequestList").getKendoGrid().refresh();

            }
        };

            // Tab control
        var SendRequestTabStrip = $("#SendRequestTabStrip").kendoTabStrip({
                tabPosition: "top",
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
                show: onShow
            });
        SendRequestTabStrip = $("#SendRequestTabStrip").getKendoTabStrip();
        SendRequestTabStrip.select(0);

            // --Tab control
        kendo.bind($("#OtherDepartmentForm"), <%: this.ID %>_OtherDepartmentVM);
//        OnDocumentReady = function () {
//        }

    </script>
