﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace PTVI.iPROM.WebApp.UserControl {
    
    
    public partial class EWRTaskListControl {
        
        /// <summary>
        /// EWRK2ApprovalControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.EWRK2ApprovalControl EWRK2ApprovalControl;
        
        /// <summary>
        /// K2HistoryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.K2HistoryControl K2HistoryControl;
        
        /// <summary>
        /// EWRMinutesOfMeetingControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.EWRMinutesOfMeetingControl EWRMinutesOfMeetingControl;
        
        /// <summary>
        /// ReviseEWRControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.ReviseEWRControl ReviseEWRControl;
        
        /// <summary>
        /// RevisionK2HistoryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.K2HistoryControl RevisionK2HistoryControl;
        
        /// <summary>
        /// ReviseEWRDraftControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.ReviseEWRControl ReviseEWRDraftControl;
        
        /// <summary>
        /// ViewEWRControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.ReviseEWRControl ViewEWRControl;
        
        /// <summary>
        /// ViewK2HistoryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PTVI.iPROM.WebApp.UserControl.K2HistoryControl ViewK2HistoryControl;
    }
}
