﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignExecutorEWPControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.AssignExecutorEWPControl" %>
<script>

    var <%: this.ID %>_viewAssignExecutorEWPModel = kendo.observable({
        editExecutor: function (e) {
            $("#executor").val(e.model.executorAssignment);
             $("#planStartDate").val(kendo.toString(e.model.planStartDate,"dd MMM yyyy"));
             $("#planFinishDate").val(kendo.toString(e.model.planFinishDate, "dd MMM yyyy"));
             if (e.model.executorAssignment != "") {
                 var ddExecAssignment = $("#ddExecAssignment").getKendoDropDownList();
                 ddExecAssignment.select(function (dataItem) {
                     return dataItem.text === "other";
                 });
             }
        },
        dsAssignExecutorEWP: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListMainProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListMainProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        docNo: { editable: false },
                        docTitle: { editable: false },
                        documentByName: { editable: false },
                        project: { editable: false },
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                    }
                },
                parse: function (e) {
                    for (var i = 0; i < e.length; i++) {
                        if ((e[i].executorAssignment != "") && (e[i].executorAssignmentText == "-"))
                            e[i].executorAssignmentText = e[i].executorAssignment;
                    }
                    return e;
                },
            },
            transport: {
                read: {
                    url: _webApiUrl + "ProjectDocuments/listPDAssignExecutorforEWP/" + _projectId,
                    dataType: "json"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "ProjectDocuments/PDAssignExecutorforEWPUpdate/" + data.id;
                    },
                    type: "PUT"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        aaa = data;
                        if (data.executorAssignment == "other") {
                            data.planStartDate = $("#planStartDate").val();
                            data.planFinishDate = $("#planFinishDate").val();
                            data.executorAssignment = $("#executor").val();
                        }
                        data.planStartDate = kendo.toString(data.planStartDate, 'yyyy-MM-dd');
                        data.planFinishDate = kendo.toString(data.planFinishDate, 'yyyy-MM-dd');
                        data.actualStartDate = kendo.toString(data.actualStartDate, 'yyyy-MM-dd');
                        data.actualFinishDate = kendo.toString(data.actualFinishDate, 'yyyy-MM-dd');
                    } else
                    {

                    }
                    return data;
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dataBound: function () {
            var grid = $("#<%: this.ID %>_grdAssignExecutorEWPControl").getKendoGrid();
            var data = grid.dataSource.data();
            $.each(data, function (i, row) {
                //hide jika sudah mempunyai nomor tender
                if (row.isTender == true) {
                    var currenRow = grid.table.find("tr[data-uid='" + row.uid + "']");
                    $(currenRow).find(".k-grid-edit").hide();
                }
            });
        }
    });

    function <%: this.ID %>_executorAssignmentDropDownEditor(container, options) {
        $('<input id="ddExecAssignment" required name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                optionLabel: "Select Executor...",
                valuePrimitive: true,
                autoBind: false,
                dataTextField: "text",
                dataValueField: "value",
                dataSource: {
                    requestEnd: function (e) {

                    },
                    transport: {
                        read: _webApiUrl + "lookup/findbytype/executorEWP"
                    }
                },
                change: function (e) {
                    if (this.selectedIndex>0)
                    if (this.dataSource.data()[this.selectedIndex-1].value == "other") {
                        var otherExecutorEWP = $("#otherExecutorEWP").getKendoWindow();
                        otherExecutorEWP.center().open();
                    }
                },
                sort: { field: "TEXT", dir: "asc" }
            });
    }

    $(document).ready(function () {
        $("#otherExecutorEWP").kendoWindow({
            modal: true,
            width: 400
        });
        kendo.bind($("#<%: this.ID %>_AssignExecutorEWPControl"), <%: this.ID %>_viewAssignExecutorEWPModel);
        <%: this.ID %>_viewAssignExecutorEWPModel.dsAssignExecutorEWP.read();
    });

    function saveExecutorEWP() {
        if ($("#executor").val()=="") {
            alert("Executor Assignnment is Empty");
            return 0;
        }
        var start = kendo.parseDate($("#planStartDate").val(), "dd MMM yyyy");
        var end = kendo.parseDate($("#planFinishDate").val(), "dd MMM yyyy");
        if ((start == null) || (end == null)) {
            alert("Plan Start Date Or Plan Finish Date is Empty");
            return 0;
        }
        if (start > end) {
            alert("Plan Start Date Exceed Plan Finish Date");
            return 0;
        }

            $("#<%: this.ID %>_grdAssignExecutorEWPControl").getKendoGrid().saveChanges();
            var otherExecutorEWP = $("#otherExecutorEWP").getKendoWindow();
            $("#executor").val("");
            $("#planStartDate").val(null);
            $("#planFinishDate").val(null);
            otherExecutorEWP.center().close();
    }
</script>
<div style="display:none" id="otherExecutorEWP">
    <div style="padding-bottom:10px">
        <label for="executor" style="display: inline-block; width: 150px">Executor Assignment</label>
        <input id="executor" style="width: 220px" required="required" type="text" class="k-textbox"/>
    </div>
    <div style="padding-bottom:10px">
        <label for="planStartDate" style="display: inline-block; width: 150px">Plan Start Date</label>
        <input id="planStartDate" style="width: 200px" name="Plan Start Date" required="required" type="date" data-role="datepicker" 
            data-format= "dd MMM yyyy"/>
    </div>
    <div style="padding-bottom:10px">
        <label for="planFinishDate" style="display: inline-block; width: 150px">Plan Finish Date</label>
        <input id="planFinishDate" style="width: 200px" name="Plan Finish Date" required="required" type="date" data-role="datepicker"
            data-format= "dd MMM yyyy"/>
    </div>
    <button class="k-button" onclick="saveExecutorEWP()" style="width: 100px">Save
     </button>
</div>

<div id="<%: this.ID %>_AssignExecutorEWPControl">

    <div class="headerSubProject">Construction Services EWP List</div>
    <div id="<%: this.ID %>_grdAssignExecutorEWPControl" 
        data-role="grid"
        data-sortable="true"
        data-editable="inline"
        data-columns="[
            {
                field: 'docNo', title: 'Doc No',
                attributes: {style: 'height: 40px'},
                width: 70
            },
            {
                field: 'docTitle', title: 'Doc Title',
                width: 250
            },
            {
                field: 'project', title: 'Project Manager',
                template: '#:project.projectManagerName#',
                width: 110
            },
            {
                field: 'documentByName', title: 'Engineer',
                width: 110
            },
            {
                field: 'executorAssignment', title: 'Executor',
                editor: <%: this.ID %>_executorAssignmentDropDownEditor,
                template: '#:data.executorAssignmentText#',
                width: 110
            }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageEWPConstruction))
            { %>,
            {
                command: ['edit'],
                width: 100
            }<% }%>
        ]"
        data-bind="source: dsAssignExecutorEWP, events: { edit: editExecutor, dataBound: dataBound }"
        data-pageable="{ buttonCount: 5 }">
    </div>
</div>