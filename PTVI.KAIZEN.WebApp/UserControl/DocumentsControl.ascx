﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentsControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DocumentsControl" %>
<%@ Register Src="~/UserControl/UploaderNewControl.ascx" TagPrefix="uc1" TagName="UploadControlDocument" %>
<%@ Register Src="~/UserControl/ConstructionMonitoringReportControl.ascx" TagPrefix="uc1" TagName="ConstructionMonitoringReportControl" %>
<%@ Register Src="~/UserControl/EWPWeightHoursControl.ascx" TagPrefix="uc1" TagName="EWPWeightHoursControl1" %>


<script type="text/x-kendo-template" id="editButtonTemplate">
<a class="k-button k-button-icontext k-grid-activate"><span class="k-icon k-i-edit"></span>Reactivate</a>
</script>


<script type="text/x-kendo-template" id="radio-template">
    <div class="radioItemExecutor">
        <input class="k-radio" type="radio" name="rdExecutor" data-bind="attr:{name: tname, id: value, value: value}, checked:executor"/>
        <label class="k-radio-label" for="#: value #" data-bind="text: text">
        </label>
    </div>
</script>

<script type="text/x-kendo-template" id="addPDTemplate">
    <div class="projectDocContainer" id="projectDocContainer">
        <div style="width:700px">
            <div class="labelProjectDoc">Deliverable</div><div class="labelProjectDoc">
            <input name="Deliverable" required='required' id="cbPDDeliverable" data-role="dropdownlist"
                   data-placeholder="Select Deliverable"
                   data-value-primitive="true"
                   data-text-field="nameText"
                   data-value-field="id"
                   data-placeholder="Select Deliverable"
                   data-bind="
                              source: DeliverableSource,
                              value: keyDeliverablesId,
                            "
            style="width: 350px" /></div>
        </div>

        </div>
        <div style="width:700px">
           <div class="labelProjectDoc" style="vertical-align: top; padding-top: 10px" >Doc. No.</div><div class="labelProjectDoc" style="width:500px">
            <input name="Doc. No" required="true" class="inputProjectDoc k-textbox" type="text" id="docNo" data-bind="value: docNo" disabled /></div>
        </div>
        <div style="width:700px" >
            <div class="labelProjectDoc" style="vertical-align: top; display: inline-block;">Doc. Title</div>
            <div style="display: inline-block; width: 400px; px;padding-left: 10px">
                <div style="width: 300px; display: inline-block" >
                  <input name="Doc. Title" required='true' class="k-textbox" style="width: 300px; display: inline-block" type="text" id="docTitle1" disabled/>
                </div>
                <div id="docTitle1stStrip" style="width: 10px; display: inline-block" >
                  -
                </div><br>
                <div style="width: 160px; display: inline-block" >
                 <input id="docTitle2"  style="width: 160px;display: inline-block" data-role="dropdownlist"
                   data-placeholder="Doc. Type"
                   data-value-primitive="true"
                   data-text-field="text"
                   data-value-field="value"
                   data-bind="value: docTitle,
                              source: Title2Source,"
                   />
                </div>    
                <div style="width: 100px; display: inline-block" >
                    <input id="disciplineTitle"  style="width: 100px;display: inline-block" class="k-textbox" type="text" placeholder="Discipline" readonly/>
                </div>
                <div style="width: 120px; display: inline-block" >
                    <input id="docTitle3"  style="width: 120px;display: inline-block" class="k-textbox" type="text" placeholder="Title" />
                </div>
            </div>
        </div>
        <div style="width: 700px; display: inline-block">
            <div style="width: 300px; display: inline-block">
                <div class="" >
                    <div class="labelProjectDoc">Plan Start Date</div><div class="labelProjectDoc"><input name="Plan Start Date" required='true' id="dtPlanStartDate" data-role="datepicker"
                     data-bind="value: planStartDate" data-format="dd MMM yyyy"/></div>
                </div>
                <div class="" >
                    <div class="labelProjectDoc">Plan Finish Date</div><div class="labelProjectDoc"><input name="Plan Finish Date" required='true' id="dtPlanFinishDate" data-role="datepicker" 
                    data-bind="value: planFinishDate" data-format="dd MMM yyyy"/></div>
                </div>
            </div>
            <div style="width: 300px; display: inline-block;padding-left: 30px">
                <div class="" id="dtActualStartDateContainer">
                    <div class="labelProjectDoc">Actual Start Date</div><div class="labelProjectDoc"><input id="dtActualStartDate" data-role="datepicker"
                     data-bind="value: actualStartDate" data-format="dd MMM yyyy"/></div>
                </div>
                <div class="" id="dtActualFinishDateContainer">
                    <div class="labelProjectDoc">Actual Finish Date</div><div class="labelProjectDoc"><input id="dtActualFinishDate" data-role="datepicker" 
                    data-bind="value: actualFinishDate" data-format="dd MMM yyyy"/></div>
                </div>
            </div>
        </div>

<%--        <div class="" >
            <div class="labelProjectDoc">Status</div><div class="labelProjectDoc"><input name="Status" required='required' id="cbStatus" data-role="combobox"
                   data-placeholder="select Status"
                   data-value-primitive="true"
                   data-text-field="text"
                   data-value-field="value"
                   data-bind="
                              source: StatusSource,
                              value: status,
                            "
            style="width: 250px" /></div>
        </div>--%>
        <div class="" >
            <div class="labelProjectDoc">Remark</div><div class="labelProjectDoc"><textarea class=" k-textbox textArea" id="remark"
                       data-bind="value: remark"/></textarea></div>
        </div>
        <div class="" id="ucEngineeringContainer" >
            <div class="labelProjectDoc">Engineer</div><div class="labelProjectDoc"><input id="cbEngineer" data-role="dropdownlist"
                   data-value-primitive="true"
                   data-text-field="employeeName"
                   data-value-field="employeeId"
                   data-bind="
                              source: EngineeringSource,
                              value: documentById,
                            "
            style="width: 250px" #= (_roleInProject != 'projectManager' && _roleInProject != 'projectEngineer' && _roleInProject != 'PC' && _roleInProject != 'otherPositionProject' && _currRole != "ADM") ? 'disabled' : '' #/>
            </div>
        </div>
        <div id="rdExecutorContainer">
            <div class="labelProjectDoc">Executor</div>
            <div class="radioTemplate" id="rdGroupExecutor" data-bind="source:ExecutorSource" data-template="radio-template"></div>
        </div>
    <div id="visibleOnEditColumn">
        <div class="" id="divPmoNo">
            <div class="labelProjectDoc">PMO No.</div><div class="labelProjectDoc">
                  <input id="pmoNo" name="PMO No" class="k-textbox" style="width: 120px;display: inline-block" data-bind="value: pmoNo" type="text"/>
            </div>
        </div>
        <div class="" id="divPmContractNo">
            <div class="labelProjectDoc">Contract No.</div><div class="labelProjectDoc">
                  <input id="contractNo" name="Contract No" class="k-textbox" style="width: 120px;display: inline-block" data-bind="value: contractNo" type="text"/>
            </div>
        </div>
        <div class="" id="divTenderNo">
            <div class="labelProjectDoc">Tender No.</div><div class="labelProjectDoc">
                  <input id="tenderNo" name="Tender No" class="k-textbox" style="width: 120px;display: inline-block" data-bind="value: tenderNo" type="text"/>
            </div>
        </div>
    </div>
        <div class="" id="<%: this.ID %>_dtSharedFolderContainer">
            <div class="labelProjectDoc">Share Folder</div><div class="labelProjectDoc"><input id="<%: this.ID %>_dtSharedFolder" name="Shared Folder" class="inputProjectDoc k-textbox" type="text" style="width:300px" placeholder="ShareFolderURL: \\\\NETAPP8\\SHARES " data-bind="value: sharedFolder" /></div>
        </div>

    <div>
</script>
<style>
    .labelProjectDoc {
        width: 100px;
        display: inline-block;
        padding-left: 10px;
    }

    .radioTemplate {
        width: 300px;
        display: inline-block;
        padding-left: 10px;
    }

    .radioItemExecutor {
        width: 85px;
        display: inline-block;
        padding-left: 10px;
    }

    .inputProjectDoc {
        width: 200px;
    }

    .projectDocContainer {
        width: 700px;
    }

    .textArea {
        width: 300px;
        height: 50px;
    }
</style>

<div id="<%: this.ID %>_FormContent">
      <uc1:UploadControlDocument runat="server" ID="UploadControlDocument" />
    <div>
        <div id="<%: this.ID %>_grdDocuments" data-role="grid" data-bind="source: ProjectDocSource, events: {
    edit: editProjectDoc,
    dataBound: onDataBound,
    save: saveDoc, excelExport: excelDocument
}" 
            data-columns="[
            { 
                field: 'docNo', title: 'EWP Compliance No', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_document-template').html()),
                width: 200
            },
            { 
                field: 'planStartDate', title: 'Planned', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_planned-template').html()),
                width: 100, filterable: true
            },
            { 
                field: 'actualStartDate', title: 'Actual', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_actual-template').html()),
                width: 100, filterable: true
            },
            {field:'statusText', title: 'Status', template:'#= statusName() #', filterable: true, filterable: { ui: <%: this.ID %>_statusFilter } },
            {
                field:'documentByName', title: 'Engineer & Executor',
                attributes: { style: 'vertical-align: top !important;' }, filterable: true, 
                template: kendo.template($('#<%: this.ID %>_engineerExecutor-template').html())               
            },
            {field:'remark', title: 'Remark', filterable: true, template: kendo.template($('#<%: this.ID %>_remark-template').html())},
            {
                title: 'Hours(s)', width: 65, filterable: false, 
                template: kendo.template($('#<%: this.ID %>_Weight-template').html()),
                attributes: { style: 'text-align: right; vertical-align: top !important;' }
            }

            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP))
            { %>,
            {command: [{text:'Reactivate', click: <%: this.ID %>_viewModel.reactivate, visible:<%: this.ID %>_viewModel.visible},{name:'edit', text:{edit: 'Edit', update: 'Save'}},{name:'destroy'}] }
            <% } %>
            ]" 
            data-editable= "{mode: 'popup', confirmation:true, window:{ width: 700, title: 'Project Documents', modal: true, activate: <%: this.ID %>_winActivate}, template:kendo.template($('#addPDTemplate').html()) }", 
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP))
            { %>
            data-toolbar= "[{name:'create', text:'Add New EWP Compliance'}, { name:'upload', text:'Upload Data (xlsx)' }, {name:'excelExport', text:'Export to Excel'}]", 
            <% } %>
            data-filterable="true"
            data-pageable="true", 
            data-sortable="true"
            ></div>
 
    </div>
    <div id="<%: this.ID %>_popWeightPopUp">
        <uc1:EWPWeightHoursControl1 runat="server" ID="EWPWeightHours" />
    </div>
</div>
<script id="<%: this.ID %>_document-template" type="text/x-kendo-template">

    <div class='subColumnTitle'>
        No:
    </div>
    <div>
        #= data.docNo #
    </div>
    <div class='subColumnTitle'>
            Title:
    </div>
    <div> 
        #= data.docTitleText # 
    </div>
    #if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {#
        <div> 
            #if (data.sharedFolder) {#
                <a href="file://#= data.sharedFolder#"  target="_explorer.exe">Share Folder</a>
            #}#
        </div>
    #} else { #
        <div> 
            #if (data.sharedFolder) {#
                <div class='subColumnTitle'>
                        Share Folder
                </div>
                <div>
                    #= data.sharedFolder#
                </div>
            #}#
        </div>
    #} #
</script>
<script id="<%: this.ID %>_planned-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.planStartDate ? kendo.toString(data.planStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.planFinishDate ? kendo.toString(data.planFinishDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Rev:
    </div>
    <div> 
        #= data.revision ? data.revision : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_remark-template" type="text/x-kendo-template">

    #= remark?remark.replace(/(\r?\n)/gi,"<br>"):"" #

</script>

<script id="<%: this.ID %>_Weight-template" type="text/x-kendo-template">
    <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours))
    { %>
        <a href="\#" data-bind="events: { click: openWeightPopUpEdit }">#: data.weightHours ? data.weightHours : "N/A" #</a>
    <% } else { %>
        #: data.weightHours ? data.weightHours : "N/A" #
    <% } %>
</script>

<script id="<%: this.ID %>_actual-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.actualStartDate ? kendo.toString(data.actualStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.actualFinishDate ? kendo.toString(data.actualFinishDate,'dd MMM yyyy') : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_engineerExecutor-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Engineer:
    </div>
    <div> 
        #= data.documentByName #  
    </div>
    <div class='subColumnTitle'>
        Executor:
    </div>
    <div> 
        #= data.executor ? data.executor : '-' #
    </div>
</script>

<script>
    var <%: this.ID %>_popWeightPopUp;
    var <%: this.ID %>_actionWoPopOut = 0;
    var <%: this.ID %>_projectDocSource = new kendo.data.DataSource({
        requestStart: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), true);
        },
        requestEnd: function (e) {
            kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), false);
        },
        transport: {
            create: {
                type: "POST", dataType: "json",
                url: _webApiUrl + "ProjectDocuments/postProjectDocumentObjectBadgeNo/" + _currBadgeNo
            },
            read: {
                dataType: "json",
                url: _webApiUrl + "ProjectDocuments/listPDByProjectIdDocType/" + _projectId + "?docType=EWP"
            },
            update: {
                type: "PUT", dataType: "json",
                url: function (e) {
                    return _webApiUrl + "ProjectDocuments/putProjectDocumentObject/" + e.id;
                }

            },
            destroy: {
                type: "DELETE", dataType: "json",
                url: function (e) {
                    return _webApiUrl + "ProjectDocuments/" + e.id;
                }

            },
            parameterMap: function (options, operation) {
                if (operation == "create") {
                    options.planStartDate = kendo.toString(options.planStartDate, 'MM/dd/yyyy');
                    options.planFinishDate = kendo.toString(options.planFinishDate, 'MM/dd/yyyy');
                    options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                    options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                    options.actualStartDate = null;
                    options.actualFinishDate = null;
                    options.projectId = _projectId;
                    options.documentById = _currBadgeNo;
                    options.documentByName = common_getEmployeeSName(_currBadgeNo);
                    options.documentByEmail = common_getEmployeeSEmail(_currBadgeNo);
                    options.docType = "EWP";
                    options.createdBy = _currNTUserID;
                    options.updatedBy = _currNTUserID;
                    options.cutOfDate = kendo.toString(options.cutOfDate, 'MM/dd/yyyy');
                    options.project = {};
                    options.tender = {};
                    options.docNo = $("#docNo").val();
                    options.docTitle = $("#docTitle1").val() + "-" + $("#docTitle2").getKendoDropDownList().value() + " " +<%: this.ID %>_viewModel.disciplineID + " " + $("#docTitle3").val();
                    options.projectDocumentWeightHours = EWPWeightHours_localData ? EWPWeightHours_localData : [];
                    return options;
                }


                if (operation == "update") {
                    options.planStartDate = kendo.toString(options.planStartDate, 'MM/dd/yyyy');
                    options.planFinishDate = kendo.toString(options.planFinishDate, 'MM/dd/yyyy');
                    options.actualStartDate = kendo.toString(options.actualStartDate, 'MM/dd/yyyy');
                    options.actualFinishDate = kendo.toString(options.actualFinishDate, 'MM/dd/yyyy');
                    options.executor =  <%: this.ID %>_vmExecutor.executor;
                    options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                    options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                 if((<%: this.ID %>_actionWoPopOut == 0)){
                     options.projectId = _projectId;
                     options.assignmentType = "EWP";
                     options.updatedBy = _currNTUserID;
                     options.cutOfDate = kendo.toString(options.cutOfDate, 'MM/dd/yyyy');
                     options.project = {};
                     options.tender = {};
                     var ddl = $("#cbEngineer").getKendoDropDownList();
                    if (ddl && ddl.selectedIndex > -1) {
                        options.documentByName = ddl.dataSource.data()[ddl.selectedIndex].employeeName;
                        options.documentByEmail = ddl.dataSource.data()[ddl.selectedIndex].employeeEmail;
                    }
                    //                    options.sharedFolder = options.SharedFolder;
                    options.docNo = $("#docNo").val();
                    if (($('#docTitle2').getKendoDropDownList().value() != null) || ($('#docTitle3').val()!=null))
                        options.docTitle = $("#docTitle1").val() + "-" + $("#docTitle2").getKendoDropDownList().value() + " " +<%: this.ID %>_viewModel.disciplineID + " " + $("#docTitle3").val();
                    else
                        options.docTitle = $("#docTitle1").val();
                     }
                    options.projectDocumentWeightHours = EWPWeightHours_localData ? EWPWeightHours_localData : [];
                    return options;
                }
            }
        },
        error: function (e) {
            //TODO: handle the errors
            _showDialogMessage("Error", e.xhr.responseText, "");
            //alert(e.xhr.responseText);
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    projectId: { type: "number" },
                    keyDeliverablesId: { type: "string" },
                    docType: { type: "string" },
                    docDesc: { type: "string" },
                    docNo: { type: "string" },
                    docTitle: { type: "string" },
                    docTitleText: { type: "string" },
                    drawingNo: { type: "string" },
                    drawingCount: { type: "number" },
                    cutOfDate: { type: "date", defaultValue: null },
                    planStartDate: { type: "date", validation: { required: true }, defaultValue: null },
                    planFinishDate: { type: "date", validation: { required: true }, defaultValue: null },
                    actualStartDate: { type: "date", validation: { required: true }, defaultValue: null },
                    actualFinishDate: { type: "date", validation: { required: true }, defaultValue: null },
                    disciplinedrawingNo: { type: "string" },
                    status: { type: "string", validation: { required: true } },
                    remark: { type: "string", validation: { required: true } },
                    discipline: { type: "string", validation: { required: true } },
                    statusdrawingNo: { type: "string" },
                    remarkdrawingNo: { type: "string" },
                    executordrawingNo: { type: "string" },
                    executorAssignmentdrawingNo: { type: "string" },
                    executor: { type: "string", validation: { required: true } },
                    progress: { type: "number" },
                    documentById: { type: "string", validation: { required: true } },
                    documentByName: { type: "string" },
                    documentByEmail: { type: "string" },
                    documentTrafic: { type: "string" },
                    revision: { type: "number" },
                    pmoNo: { type: "string" },
                    position: { type: "string" },
                    sharedFolder: { type: "string" },
                    ewpDocType: { type: "string" },
                    contractNo: { type: "string" },
                    tenderNo: { type: "string" },
                    weightHours: { type: "number" },
                    createdDate: { type: "date" },
                    createdBy: { type: "string" },
                    updatedDate: { type: "date" },
                    updatedBy: { type: "string" },
                },
                statusName: function () {
                    if (this.status)
                        return getStatus(this.status);
                    else return "";
                },
                disciplineName: function () {
                    if (this.discipline)
                        return getDiscipline(this.discipline);
                    else return "";
                }
            },
            parse: function (d) {
                if (d.length) {
                    $.each(d, function (key, value) {
                        if (value.status)
                            value.statusText = getStatus(value.status);

                        var docTitle = value.docTitle;
                        var t1 = docTitle != null ? docTitle.split("-") : "";
                        var docTitle1 = t1[0];
                        var docTitle2 = "";
                        var disciplineID = "";
                        var docTitle3 ="";
                        if (t1[1]) {
                            var t2Temp = t1[1];
                            var t2 = t2Temp.split(" ");
                            docTitle2 = value.ewpDocType;
                            disciplineID = value.position;
                            t2.shift();
                            if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                            docTitle3 = t2.join(" ");
                            value.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                        } else {
                            value.docTitleText = value.docTitle;
                        }

                    });
                } else {
                    var docTitle = d.docTitle;
                    if (docTitle) {
                        var t1 = docTitle.split("-");
                        var docTitle1 = t1[0];
                        var docTitle2 = "";
                        var disciplineID = "";
                        var docTitle3 = "";
                        if (t1[1]) {
                            var t2Temp = t1[1];
                            var t2 = t2Temp.split(" ");
                            docTitle2 = d.ewpDocType;
                            disciplineID = d.position;
                            t2.shift();
                            if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                            docTitle3 = t2.join(" ");
                            d.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                        } else {
                            d.docTitleText = d.docTitle;
                        }

                        statusText = getStatus(d.status);
                    }
                }

                var totEWP = 0, totEWPComp = 0;
//                if ($("#infoCountEWP")) $("#infoCountEWP").remove();
                if (d == "Duplicate Doc. No.") {
                    <%: this.ID %>_projectDocSource.read();
                    alert("Process failed: " + d);
                    return 0;
                } else {
                    if (d.constructor === Array) {
                        $.each(d, function (key, value) {
                            if (value.status == "S1") totEWPComp = totEWPComp + 1;
                            totEWP = totEWP + 1;
                        })
                        <%: this.ID %>_viewModel.set("totEWP", totEWP);
                        <%: this.ID %>_viewModel.set("totEWPComp", totEWPComp);
                        $("#<%: this.ID %>_grdDocuments .k-grid-pager").append("<div id='infoCountEWP'>&nbsp;Total EWP: " + <%: this.ID %>_viewModel.totEWP + " | Total EWP Completed: " +<%: this.ID %>_viewModel.totEWPComp + "</div>");
                    }
                    return d;
                    <%: this.ID %>_projectDocSource.read();
                }
            },
        },
        pageSize: 10,
        change: function (e) {
        }
    });

    var <%: this.ID %>_deliverable = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "keydeliverables/listKeyDByProjectIdActive/" + _projectId,
                //url: _webApiUrl + "keydeliverables/listkdbyprojectid/" + _projectId,
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    name: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_discipline = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/discipline"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_status = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/statusiprom"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_executor = new kendo.data.DataSource({
        requestEnd: function (e) {
        },
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/executor"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_engineeringSource = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                cache: false,
                url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1?assignmentType=PE|TSE&PrID=" + _projectId
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    employeeId: { type: "string" },
                    employeeName: { type: "string" },
                    employeeEmail: { type: "string" }
                }
            }
        }
    });

    var <%: this.ID %>_title2Source = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                cache: false,
                url: _webApiUrl + "lookup/findbytype/ewpDocType"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_vmExecutor = new kendo.observable({
        ExecutorSource: <%: this.ID %>_executor,
        executor: "",
        tname: "rdExecutor",
//        rdExecutorChange: <%: this.ID %>_rdExecutorChange, 
//        $('input[name="rdExecutor"]').change(rdExecutorChange);

    });



    function <%: this.ID %>_saveDoc(e) {
        if (<%: this.ID %>_vmExecutor.executor != "") e.model.dirty = true;
        if ((e.model.planFinishDate < e.model.planStartDate) && (e.model.planFinishDate != null)) {
            e.preventDefault();
            //                e.sender.dataSource.cancelChanges();
            alert("Plan Finish Date cannot start earlier than plan start date");
        }
        if ((e.model.actualFinishDate < e.model.actualStartDate) && (e.model.actualFinishDate != null)) {
            e.preventDefault();
            //                e.sender.dataSource.cancelChanges();
            alert("Actual Finish Date cannot start earlier than Actual start date");
        }
    }

    function EngineerChange(e) {
        e.data.documentById = e.sender.dataSource.data()[e.sender.selectedIndex].employeeId;
        e.data.documentByName = e.sender.dataSource.data()[e.sender.selectedIndex].employeeName;
        e.data.documentByEmail = e.sender.dataSource.data()[e.sender.selectedIndex].employeeEmail;
        e.sender.dataSource._data[e.sender.selectedIndex].dirty = true;
        <%: this.ID %>_viewModel.set("documentById", e.data.documentById);
        <%: this.ID %>_viewModel.set("documentByName", e.data.documentByName);
        <%: this.ID %>_viewModel.set("documentByEmail", e.data.documentByEmail);
    }


    function getCurEngPosition(e) {
        var pos = null;
        var that = e;
        if (e.model.isNew()) {
            $.ajax({
                url: "<%: WebApiUrl %>" + "projectdocuments/getEngineerPosInProject/" + _projectId + "?badgeno=" + _currBadgeNo,
                type: "GET",
                async: false,
                success: function (result) {
                    //result = pos;
                    if (!result) {
                        _showDialogMessage("Error Message", "You are not listed as engineer<br/>Please add yourself as engineer in Assign Engineer and Designer Tab", "");
                        $('#<%: this.ID %>_grdDocuments').data("kendoGrid").cancelChanges();
                    } else {
                        procEditProjectDocument(e, that, result)
                        return result;
                    }
                },
                error: function (error) {
                }
            });
        } else {
            procEditProjectDocument(e, that, null)
        }
    }

    function <%: this.ID %>_editProjectDocument(e) {
        //prepare input weight data
        EWPWeightHours_localData = [];
        var pos = getCurEngPosition(e);
    }

    function procEditProjectDocument(e, d, pos) {
        //var pos = getCurEngPosition();
      //  $("#cbPDDeliverable").getKendoDropDownList().refresh();
        kendo.bind($("#rdExecutorContainer"), <%: this.ID %>_vmExecutor);
        //        $('input[name="rdExecutor"]').change(<%: this.ID %>_rdExecutorChange);
        if (pos)
            $("#disciplineTitle").val(pos.posName);
        else
            $("#disciplineTitle").val("");
        $("#docTitle2").getKendoDropDownList().bind("change", function () {
            var dtItem = $("#docTitle2").getKendoDropDownList().dataItem();

                <%: this.ID %>_viewModel.set("disciplineID", dtItem.value == _useReportCode ? pos1 = pos.pos + "R" : pos1 = pos.pos);               
            });

        $("#cbPDDeliverable").getKendoDropDownList().bind("change", function () {
            var dtItem = $("#cbPDDeliverable").getKendoDropDownList().dataItem();
            if (dtItem.planStartDate != null)
                $("#dtPlanStartDate").getKendoDatePicker().setOptions({
                    min: dtItem.planStartDate,
                    max: dtItem.planFinishDate
                });
            if (dtItem.planFinishDate != null)
                $("#dtPlanFinishDate").getKendoDatePicker().setOptions({
                    min: dtItem.planStartDate,
                    max: dtItem.planFinishDate
                });
        });

        if (e.model.keyDeliverablesId){
            var dtItem = <%: this.ID %>_viewModel.DeliverableSource.get(e.model.keyDeliverablesId);
            if (dtItem.planStartDate != null)
                $("#dtPlanStartDate").getKendoDatePicker().setOptions({
                    min: dtItem.planStartDate,
                    max: dtItem.planFinishDate
                });
            if (dtItem.planFinishDate != null)
                $("#dtPlanFinishDate").getKendoDatePicker().setOptions({
                    min: dtItem.planStartDate,
                    max: dtItem.planFinishDate
                });
        }

        $(".k-edit-form-container").kendoTooltip({
            filter: "[name='Doc. No']",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format EWP No:<br\>"
                + "a. SIDB200216-E1, "
                + "b. SIDB200216-E2, "
                + "c. SIDB200216-E3<br\> ";
                return content;
            }
        }).data("kendoTooltip");

        if (e.model.isNew()) {
            $("#dtActualStartDateContainer").hide();
            $("#dtActualFinishDateContainer").hide();
            $("#ucEngineeringContainer").hide();
            $("#rdExecutorContainer").hide(); 
            $("#visibleOnEditColumn").hide();
            $(".k-window-title").html("Add")
            $("#docNo").val(_projectNo + "-" +  "AUTO");
            $("#docTitle1").val(_projectDesc);
            //$("#docTitle2").getKendoComboBox().value(pos.posName);
            $("#autoInfo").remove();
            $('<sup id="autoInfo" style="color: red">AUTO will be replaced with sequence number</sup>').insertAfter($("#docNo"));

            //prepare input weight
            $(".k-edit-buttons").prepend('<a class="k-button" onClick="<%: this.ID %>_viewModel.openWeightPopUp()" style="float: left"><span class="k-icon k-i-grid-layout"></span> Input Weight</a>');
        } else {
            $("#dtActualStartDateContainer").show();
            $("#dtActualFinishDateContainer").show();
            $("#ucEngineeringContainer").show();
            $("#rdExecutorContainer").show();
            $("#visibleOnEditColumn").show();
            $(".k-window-title").html("Edit");
            var docTitle = e.model.docTitle;
            var t1 = docTitle != null ? docTitle.split("-") : "";
            $("#docTitle1").val(t1[0]);
            if (t1[1]) {
                var t2Temp = t1[1];
                var t2 = t2Temp.split(" ");
                $("#docTitle2").getKendoDropDownList().value(t2[0]);
                if (pos)
                    <%: this.ID %>_viewModel.set("disciplineID", t2[0] == _useReportCode ? pos.pos + "R" : pos.pos);    
                else
                    $("#disciplineTitle").val(t2[1]);
                t2.shift();
                if (pos)
                { if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();}
                    else
                        t2.shift();
                $("#docTitle3").val(t2.join(" "));
            } else {
                $("#docTitle1").width(500);
                $("#docTitle1").prop("disabled", false);
                $("#docTitle2").getKendoDropDownList().value("");
                $("#docTitle3").val("");
                $("#docTitle2").closest(".k-widget").hide();
                $("#docTitle1stStrip").hide();
                $("#docTitle3").hide();
                $("#disciplineTitle").hide();
            }
            $(".k-edit-buttons").prepend('<a id="<%: this.ID %>_btnCancel" class="k-button" style="float: left">Cancel EWP</a>');
            $(".k-edit-buttons").prepend('<a id="<%: this.ID %>_btnHold" class="k-button" style="float: left">Hold EWP</a>');
            $("#<%: this.ID %>_btnCancel").on("click", function () {
                e.model.status = "S3";
                e.model.dirty = true;
                e.model.cutOfDate = kendo.toString(e.model.cutOfDate, 'MM/dd/yyyy');
                e.model.project = {};
                e.model.tender = {};
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
                grid.dataSource.sync();
                grid.refresh();
            });
            $("#<%: this.ID %>_btnHold").on("click", function () {
                e.model.status = "S5";
                e.model.dirty = true;
                e.model.cutOfDate = kendo.toString(e.model.cutOfDate, 'MM/dd/yyyy');
                e.model.project = {};
                e.model.tender = {};
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
                grid.dataSource.sync();
                grid.refresh();

            });
            $("#dtActualFinishDate").getKendoDatePicker().bind("change", function () {
                sharedFolderOption();
            });
        }
        sharedFolderOption();

        if (_planStartDate) {
            $("#dtPlanStartDate").getKendoDatePicker().min(_planStartDate);
            $("#dtPlanFinishDate").getKendoDatePicker().min(_planStartDate);
        }

        if (_planFinishDate) {
            $("#dtPlanStartDate").getKendoDatePicker().max(_planFinishDate);
            $("#dtPlanFinishDate").getKendoDatePicker().max(_planFinishDate);
        }

        $("#dtPlanStartDate").getKendoDatePicker().bind("change", function () {
            $("#dtPlanFinishDate").getKendoDatePicker().min(this.value());
        });

        $("#dtPlanFinishDate").getKendoDatePicker().bind("change", function () {
            $("#dtPlanStartDate").getKendoDatePicker().max(this.value());
        });

        if (e.model.planFinishDate)
            $("#dtPlanStartDate").getKendoDatePicker().max(e.model.planFinishDate);
        if (e.model.planStartDate)
            $("#dtPlanFinishDate").getKendoDatePicker().min(e.model.planStartDate);

        if (e.model.id) {
            $("#dtActualStartDate").getKendoDatePicker().bind("change", function () {
                $("#dtActualFinishDate").getKendoDatePicker().min(this.value());
            });

            $("#dtActualFinishDate").getKendoDatePicker().bind("change", function () {
                $("#dtActualStartDate").getKendoDatePicker().max(this.value());
            });

            if (e.model.actualFinishDate)
                $("#dtActualStartDate").getKendoDatePicker().max(e.model.actualFinishDate);
            if (e.model.actualStartDate)
                $("#dtActualFinishDate").getKendoDatePicker().min(e.model.actualStartDate);
        }

        //        $("#rdExecutor[value=" + e.model.executor + "]").prop('checked', true);
        <%: this.ID %>_vmExecutor.set("executor", e.model.executor);
        if (!e.model.executor) {
            $("#visibleOnEditColumn").hide();
        }
        else if (e.model.executor == 'N/A') {
            document.getElementById("N/A").checked = true;

            $("#pmoNo").prop("required", false);
            $("#tenderNo").prop("required", false);
            $("#contractNo").prop("required", false);
            $("#visibleOnEditColumn").hide();
        }
        else if (e.model.executor == 'CS') {
            $("#CS").prop("checked", true);
            $("#pmoNo").prop("required", true);
            $("#tenderNo").prop("required", false);
            $("#contractNo").prop("required", false);
            $("#visibleOnEditColumn").show();
            $("#divPmoNo").show();
            $("#divPmContractNo").hide();
            $("#divTenderNo").hide();
        }
        else if (e.model.executor == 'CAS') {
            $("#CAS").prop("checked", true);
            $("#pmoNo").prop("required", false);
            $("#tenderNo").prop("required", true);
            $("#contractNo").prop("required", true);
            $("#visibleOnEditColumn").show();
            $("#divPmoNo").hide();
            $("#divPmContractNo").show();
            $("#divTenderNo").show();
        }

        $("#cbEngineer").getKendoDropDownList().dataSource.read();
        $(".k-edit-form-container").kendoTooltip({
            filter: "#Documents1_dtSharedFolder",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format Share Folder, e.g.: \\\\netapp8\\folder";
                return content;
            }
        }).data("kendoTooltip");
    }

    function <%: this.ID %>_winActivate() {
        $('input[name="rdExecutor"]').change(<%: this.ID %>_rdExecutorChange);
    }

    function <%: this.ID %>_rdExecutorChange() {
        if ($('input[name="rdExecutor"]:checked').val() == 'N/A') {
            $("#pmoNo").prop("required", false);
            $("#tenderNo").prop("required", false);
            $("#contractNo").prop("required", false);
            $("#visibleOnEditColumn").hide();
        }
        else if ($('input[name="rdExecutor"]:checked').val() == 'CS') {
            $("#pmoNo").prop("required", true);
            $("#tenderNo").prop("required", false);
            $("#contractNo").prop("required", false);
            $("#visibleOnEditColumn").show();
            $("#divPmoNo").show();
            $("#divPmContractNo").hide();
            $("#divTenderNo").hide();
        }
        else if ($('input[name="rdExecutor"]:checked').val() == 'CAS') {
            $("#pmoNo").prop("required", false);
            $("#tenderNo").prop("required", true);
            $("#contractNo").prop("required", true);
            $("#visibleOnEditColumn").show();
            $("#divPmoNo").hide();
            $("#divPmContractNo").show();
            $("#divTenderNo").show();
        }
    }

    function sharedFolderOption() {
        if ($("#dtActualFinishDate").getKendoDatePicker().value()) {
            $("#<%: this.ID %>_dtSharedFolderContainer").show();
            $("#<%: this.ID %>_dtSharedFolder").attr("required", true);
            //   dtSharedFolder
        } else {
            $("#<%: this.ID %>_dtSharedFolderContainer").hide();
            $("#<%: this.ID %>_dtSharedFolder").attr("required", false);
        }

    }

    function <%: this.ID %>_disciplineFilter(e) {
        setTimeout(function () {
            e.parent().prev().hide();
        });
        e.kendoDropDownList({
            dataSource: <%: this.ID %>_discipline,
            dataTextField: "text",
            dataValueField: "text",
            optionLabel: "- Select Status -"
        });
    }

    function <%: this.ID %>_statusFilter(e) {
        setTimeout(function () {
            e.parent().prev().hide();
        });
        e.kendoDropDownList({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        dataType: "json",
                        url: _webApiUrl + "lookup/findbytype/statusiprom"
                    },
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { type: "number" },
                            text: { type: "string" },
                            value: { type: "string" },
                        }
                    }
                }
            }),
            dataTextField: "text",
            dataValueField: "text",
            optionLabel: "- Select Status -"
        });
    }

    function getStatus(stat) {
        var status = "";
        $.ajax({
            url: "<%: WebApiUrl %>" + "lookup/FindByTypeAndValue/statusiprom|" + stat,
            type: "GET",
            async: false,
            success: function (result) {
                if (result.length > 0)
                    status = $.trim(result[0].text);
            },
            error: function (error) {
            }
        });
        return status;
    }

    function OpenDiscipline(e) {
        var listContainer = e.sender.list.closest(".k-list-container");
        listContainer.width(listContainer.width() + kendo.support.scrollbar());
    }

    var <%: this.ID %>_viewModel = new kendo.observable({
        planStartDate: null,
        planFinishDate: null,
        totEWP: 0,
        totEWPComp: 0,
        disciplineID: "",
        excelDocument: function (e) {
            aaa = e;
            var dataRow = this.ProjectDocSource;

            var rows = [{
                cells: [
                    { value: "Document No", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Document Title", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Discipline", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "PM", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Engineer", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Plan Start Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Plan Finish Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Actual Start Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Actual Finish Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Status", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                    { value: "Remark", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" }
                ]
            }];

            dataRow.fetch(function () {
                var data = this.data();
                for (var i = 0; i < data.length; i++) {
                    //push single row for every record
                    rows.push({
                        cells: [
                          { value: data[i].docNo },
                          { value: data[i].docTitle },
                          { value: data[i].disciplineName() },
                          { value: data[i].PMName },
                          { value: data[i].documentByName },
                          //{ value: data[i].planStartDate, format: "yyyy/MM/dd" },
                          { value: data[i].planStartDate, format: "dd MMM yyyy" },
                          { value: data[i].planFinishDate, format: "dd MMM yyyy" },
                          { value: data[i].actualStartDate, format: "dd MMM yyyy" },
                          { value: data[i].actualFinishDate, format: "dd MMM yyyy" },
                          { value: data[i].statusName() },
                          { value: data[i].remark }
                        ]
                    })
                }
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                      {
                          columns: [
                            // Column settings (width)
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { width: 110, autoWidth: false },
                            { width: 110, autoWidth: false },
                            { width: 110, autoWidth: false },
                            { width: 110, autoWidth: false },
                            { autoWidth: true },
                            { autoWidth: true }
                          ],
                          // Title of the sheet
                          title: "Reports",
                          // Rows of the sheet
                          rows: rows
                      }
                    ]
                });
                //save the file as Excel file with extension xlsx
                kendo.saveAs({
                    dataURI: workbook.toDataURL(),
                    fileName: "Document Ewp.xlsx",
                    proxyURL: "<%:WebApiUrl%>" + "exportproxy"
                });
            });

        },
        documentById: "",
        documentByName: "",
        documentByEmail: "",
        ProjectDocSource: <%: this.ID %>_projectDocSource,
        DisciplineSource: <%: this.ID %>_discipline,
        DeliverableSource: <%: this.ID %>_deliverable,
        EngineeringSource:<%: this.ID %>_engineeringSource,
        Title2Source: <%: this.ID %>_title2Source,
        saveDoc: <%: this.ID %>_saveDoc,
        StatusSource: <%: this.ID %>_status,
        editProjectDoc: <%: this.ID %>_editProjectDocument,
        openDiscipline: OpenDiscipline,
        visible: function (e) {
            return e.status === "S3" || e.status === "S5";
        },
        reactivate: function (e) {
            e.preventDefault();
            <%: this.ID %>_actionWoPopOut = 1;
            var tr = $(e.target).closest("tr");
            var data = this.dataItem(tr);
            data.status = null;
            data.dirty = true;
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            console.log(grid.dataSource.data());
            grid.dataSource.sync();
        },
        engineerChange: EngineerChange,
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);
                if (_currRole != "ADM" && (_roleInProject == "engineer" && _currBadgeNo != model.documentById)) {
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();
                }

                if (model.status == "S5" || model.status == "S3") {
                    $(this).find(".k-grid-Reactivate").show();
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();

                }
            });
        },
        openWeightPopUpEdit: function (e) {
            EWPWeightHours_localData = [];
            this.openWeightPopUp(e);
        },
        openWeightPopUp: function (e) {
            $("#<%: this.ID %>_popWeightPopUp").prevUntil("k-window-titlebar").find(".k-window-title").html(_projectNo + " - " + _projectDesc);

            EWPWeightHours_viewWeightHoursModel.set("no", e ? e.data.docNo : "-");
            EWPWeightHours_viewWeightHoursModel.set("title", e ? e.data.docTitleText : "-");
            EWPWeightHours_docId = e ? e.data.id : 0;
            EWPWeightHours_viewWeightHoursModel.set("visibleTitle", e ? true : false);

            EWPWeightHours_docType = "EWP";
            EWPWeightHours_bind();
            kendo.bind($("#EWPWeightHours_ConstructionMonitoringReportContent"), EWPWeightHours_viewWeightHoursModel);
            EWPWeightHours_viewWeightHoursModel.dsTreeListWeightListSource.read();
            EWPWeightHours_viewWeightHoursModel.set("otherUpdate",
                function (data) {
                    kendo.ui.progress($("#EWPWeightHours_grdWeightLIst"), true);
                    
                    if (EWPWeightHours_docId) {
                        $.ajax({
                            url: "<%:WebApiUrl%>" + "ProjectDocumentWeightHours/totalWeightByDocID/" + EWPWeightHours_docId,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                var dataEwp = <%: this.ID %>_projectDocSource.get(EWPWeightHours_docId);
                                dataEwp.set("weightHours", data)
                                kendo.ui.progress($("#EWPWeightHours_grdWeightLIst"), false);
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#EWPWeightHours_grdWeightLIst"), false);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#EWPWeightHours_grdWeightLIst"), false);
                    }
                });
            <%: this.ID %>_popWeightPopUp.center().open();

        }
    });



        function onSelectDocumentFile(e) {
            var file = e.files[0];
            if (file.extension != ".xls" && file.extension != ".xlsx") {
                alert('Only excel file is accepted.');

                e.preventDefault();
            }
        }

        function onUploadDocumentData(e) {

        }

        function onUploadDocumentDataSuccess(e) {
            UploadControlDocument_close();

            <%: this.ID %>_viewModel.ProjectDocSource.read();
        }

    function onUploadDataError(e) {
        _showDialogMessage("Error Message", "Upload Data Failed.<br/>Please check your input data!", "");
    }

    $(document).ready(function () {

        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel);

        <%: this.ID %>_viewModel.DeliverableSource.read();

        $("#<%: this.ID %>_grdDocuments .k-grid-upload").find('span').addClass("k-font-icon k-i-xls");
        $("#<%: this.ID %>_grdDocuments .k-grid-upload").bind("click", function (e) {
            UploadControlDocument_show("Upload Data Document Engineer", "Click button below or drag the excel file. Click <a href='template/uploadEngineeringServiceTemplate.xlsx'>here</a> to get the excel form.",
                false, _webApiUrl + "ProjectDocuments/upload/" + _projectId + "|" + _currNTUserID + "|EWP", onSelectDocumentFile, onUploadDocumentData, onUploadDocumentDataSuccess, onUploadDataError);

            e.preventDefault();
        });

        $("#<%: this.ID %>_grdDocuments .k-grid-excelExport").find('span').addClass("k-font-icon k-i-xls");
        $("#<%: this.ID %>_grdDocuments .k-grid-excelExport").bind("click", function (e) {
            <%: this.ID %>_viewModel.excelDocument();
        });

        <%: this.ID %>_popWeightPopUp = $("#<%: this.ID %>_popWeightPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 900,
            height: 500
        }).data("kendoWindow");
    });

</script>