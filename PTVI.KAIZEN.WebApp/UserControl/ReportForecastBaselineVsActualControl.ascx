﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportForecastBaselineVsActualControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportForecastBaselineVsActualControl" %>
<div id="<%: this.ID %>_reportForecastBaselineVsActualContainer">
    <div>
    <div data-bind="visible:loaded" style="display:none;font-weight:bold">
        <div style="text-align:center">Forecast based on actual and baseline - Time (Finish)</div>
        <div style="text-align:center" data-bind="html:taskName"></div>
        <div style="text-align:center" >Position <span data-bind="html: position"></span></div>
        <div style="text-align:center" >FINISH: <span data-bind="html: timeVar"></span></div>
    </div>
    <div 
        data-role="chart"
        data-bind="source: dsForecastBaselineVsActual"
        data-chart-area="{background: 'lightgray'}"
        data-series="[  {type: 'rangeColumn', fromField: 'baselineFinish', toField: 'estimatedFinish', colorField: 'color'},
                        {type: 'line', field: 'baselineFinish', name: 'Baseline'},
                        {type: 'line', field: 'estimatedFinish', name: 'Estimated'}]",
        data-value-axis="{labels:{template: '#= kendo.toString(new Date(value), \'dd MMM yyyy  HH:mm\') #'}}"
        data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}">
    </div>
    </div><br /><hr /><br />
    <div>
    <div data-bind="visible:loaded" style="display:none;font-weight:bold">
        <div style="text-align:center">Forecast based on actual and baseline - Duration</div>
        <div style="text-align:center" data-bind="html:taskName"></div>
        <div style="text-align:center" >Position <span data-bind="html: position"></span></div>
        <div style="text-align:center" >FINISH: <span data-bind="html: durationVar"></span></div>
    </div>
    <div 
        data-role="chart"
        data-bind="source: dsForecastBaselineVsActual"
        data-series="[  {type: 'rangeColumn', fromField: 'baselineDurationInHours',  colorField: 'color', toField: 'estimatedDurationInHours', labels: {visible: true, template:'Test', from: {template: '#=value.from# hours'}, to: {template: '#=value.to# hours' }}},
                        {type: 'line', field: 'baselineDurationInHours', name: 'Baseline'},
                        {type: 'line', field: 'estimatedDurationInHours', name: 'Estimated'}]"
        data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}">
    </div>

    </div>
</div>

<script>
    var <%: this.ID %>_vm = kendo.observable({
        taskName: null,
        position: null,
        timeVar: null,
        durationVar: null,
        dsForecastBaselineVsActual: null,
        loaded:false
    });

    function <%: this.ID %>_populate(projectShiftID, taskUid, callback) {
        $.ajax({
            url: webApiUrl + "iptreport/reportdata?reportName=IPT_FORECAST_BASELINEVSACTUAL&projectshiftid=" + projectShiftID + "&taskUid=" + taskUid,
            success: function (result) {
                if (result != null) {
                    for (var i = 0; i < result.list.length; i++) {
                        result.list[i]['baselineDurationInHours'] = (result.list[i].baselineDuration / 10.0) / 60.0;
                        result.list[i]['estimatedDurationInHours'] = (result.list[i].estimatedDuration / 10.0) / 60.0;

                        result.list[i]['color'] = result.list[i].durationVar == 'Delay' ? 'red' : 'green';
                    }

                    <%: this.ID %>_vm.set("taskName", result.taskName);
                    <%: this.ID %>_vm.set("position", kendo.toString(kendo.parseDate(result.position), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("timeVar", result.timeVar);
                    <%: this.ID %>_vm.set("durationVar", result.durationVar);
                    <%: this.ID %>_vm.dsForecastBaselineVsActual = new kendo.data.DataSource({
                        data: result.list,
                        schema: {
                            model: {
                                fields: {
                                    baselineStart: { type: 'date' },
                                    baselineFinish: { type: 'date' },
                                    estimatedStart: { type: 'date' },
                                    estimatedFinish: { type: 'date' }
                                }
                            }
                        }
                    });

                    <%: this.ID %>_vm.set("loaded", true);

                    kendo.bind($("#<%: this.ID %>_reportForecastBaselineVsActualContainer"), <%: this.ID %>_vm);
                }

                callback();
            }
        });
    }

    function <%: this.ID %>_shortLabels(value) {
        var result = "";
        while (value.length > 20) {
            var clipIndex = 20;
            while (value[clipIndex] != ' ')
                clipIndex--;

            result = result + (result != "" ? "\n" : "") + value.substring(0, clipIndex);

            value = value.substring(clipIndex);
        }
    
        result = result + (result != "" ? "\n" : "") + value;

        return result;
    }

</script>

