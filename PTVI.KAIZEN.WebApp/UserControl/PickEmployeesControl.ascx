﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PickEmployeesControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.PickEmployeesControl" %>

<div id="<%: this.ID %>_PickEmployeeContent">
    <div>
        <div class="k-content">
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend">PLEASE SELECT EMPLOYEE TYPE</legend>
                <div style="float:left;">
                    <input type="radio" name="<%: this.ID %>_employee" id="<%: this.ID %>_employeevale" 
                        class="k-radio" value="vale"
                        data-bind="click: <%: this.ID %>_onRadioValeClicked, checked: employeeTypeSelected, disabled: chkValeDisabled">
                    <label class="k-radio-label" for="<%: this.ID %>_employeevale">Vale Employee</label>
                </div>
                <div style="float:left;padding-left:30px;">
                    <input type="radio" name="<%: this.ID %>_employee" id="<%: this.ID %>_employeecontractor" 
                        class="k-radio" value="contractor"
                        data-bind="click: <%: this.ID %>_onRadioContractorClicked, checked: employeeTypeSelected, disabled: chkContractorDisabled">
                    <label class="k-radio-label" for="<%: this.ID %>_employeecontractor">Contractor</label>
                </div>
                <div style="clear: both;">
                </div>
                <br />
                <div>
                    <label class="labelForm" id="<%: this.ID %>_lblBadgeNo">Badge No/Name</label>
                    <input id="<%: this.ID %>_employeeSearch" class="k-textbox" type="text" required 
                        placeholder="Input Badge No/Name here (min 4 characters)" style="width: 325px;"
                        data-bind="value: employeeBadgeNo, disabled: <%: this.ID %>_isContractorSearchDisabled" />
                    <div
                        data-role="button"
                        data-bind="events: { click: onSearchEmployeeClick }"
                        class="k-button" >
                        <span class="k-icon k-i-search"></span>
                    </div>
                </div>
            </fieldset>
        </div>
        <br/>
        <div id="<%: this.ID %>_EmployeeGrid" 
            data-role="grid"
            data-bind="source: ds_employee_list, events: { change: onPickChange }",
            data-pageable="true",
            data-selectable="true",
            data-height="400",
            data-filterable="extra: false, operators: { string: { contains: 'Contains', startswith: 'Starts With' }",
            data-columns='[
                <%--{ template:  kendo.template($("#chkTemplate").html()), width: "55px" },--%>
                { field: "employeeId", title: "Badge Number", width: "140px" },
                { field: "full_Name", title: "Name" },
                {
                    command: [
                        { text: "Select", click: <%: this.ID %>_pickEmployeeViewModel.pickHandlerEmployee }
                    ], title: "&nbsp;", width: "100px"
                }
            ]'

        ></div>
        <input id="<%: this.ID %>_txtIdSelected" class="k-textbox" style="width: 100%;" type="hidden"/>
    </div>
    <div style="float:right;padding-top:20px;">
        <div data-role="button" class="k-button k-primary" data-bind="events: { click: onCloseSelectEvent }">Close</div>
    </div>
   
    <div style="clear:right;"></div>
    

</div>

<script>
    var <%: this.ID %>_pickEmployeeViewModel = kendo.observable({
        employeeBadgeNo: "",
        <%: this.ID %>_isContractorSearchDisabled: false,
        <%: this.ID %>_onRadioContractorClicked: function (e) {
            this.set("<%: this.ID %>_isContractorSearchDisabled", false);
            $("#<%: this.ID %>_lblBadgeNo").text("Badge No/Name")
            $("#<%: this.ID %>_employeeSearch").attr("placeholder", "Input Badge No here (min 4 characters)");
        },
        <%: this.ID %>_onRadioValeClicked: function (e) {
            this.set("<%: this.ID %>_isContractorSearchDisabled", false);
            $("#<%: this.ID %>_lblBadgeNo").text("Badge No/Name")
            $("#<%: this.ID %>_employeeSearch").attr("placeholder", "Input Badge No/Name here (min 4 characters)");
        },
        employeeTypeSelected: "vale",
        selectedEmployee: {},
        listEmployee: [],
        chkValeDisabled: false,
        chkContractorDisabled: false,
        setListEmployee: function(param) {
            this.set("listEmployee", param);
        },
    
        ds_employee_list: new kendo.data.DataSource({
           transport: {
               read: function(options) {
                   var lstEmp = <%: this.ID %>_pickEmployeeViewModel.get("listEmployee");
                   options.success(lstEmp);
                }
            },
            pageSize: 20,
            schema: {
                model: {
                    id: "employeeId"
                }
            }
        
        }),
        getListEmployee: function() {
            
            return this.get("listEmployee");
            
        },
        onPickChange: function (e) {
            this.set("selectedEmployee", e.sender.dataItem(e.sender.select()));
        },
        onSelectEvent: function (e) {
            alert("");
        },
        
        onCloseSelectEvent: function (e) {

        },
        pickHandlerEmployee: function (e) {
            
        },
        onSearchEmployeeClick: function (e) {
        
            var searchBadgeNo = this.get("employeeBadgeNo");
            if (searchBadgeNo && searchBadgeNo.length >= 4) {

                var empApiUrl = "";
                if (this.get("employeeTypeSelected") == "vale") {
                    var filter = "?$filter=substringof('"+ searchBadgeNo +"',employeeId) or substringof(tolower('"+ searchBadgeNo +"'),tolower(full_Name))";
                    kendo.ui.progress($("#<%: this.ID %>_PickEmployeeContent"), true);
                    //kendo.ui.progress($("#<%: this.ID %>_EmployeeGrid"), true);
                    
                    $.ajax({
                        url: _webOdataUrl + "EMPLOYEEsOnly" + filter,
                        type: "GET",
                        success: function(data) {
                            //<%: this.ID %>_pickEmployeeViewModel.setListEmployee([ { EMPLOYEEID: 6, FULL_NAME: "Heru" } ]);
                            <%: this.ID %>_pickEmployeeViewModel.setListEmployee(data.value);
                            <%: this.ID %>_pickEmployeeViewModel.ds_employee_list.read();

                        },
                        error: function (data) {

                            _showDialogMessage("Error", data, "");
                            
                        },
                        complete: function(jqXhr, textStatus) {
                            kendo.ui.progress($("#<%: this.ID %>_PickEmployeeContent"), false);
                            //kendo.ui.progress($("#<%: this.ID %>_EmployeeGrid"), false);
                        }
                    });
                } 
                else 
                {
                    
                    //kendo.ui.progress($("#<%: this.ID %>_EmployeeGrid"), true);
                    $.ajax({
                        url: _webApiUrl + "user/FindContractorByBN/" + searchBadgeNo,
                        type: "GET",
                        success: function(data) {
                            
                            var formattedList = [];
                            $.each(data, function(index, value){
                                formattedList.push({ 
                                    employeeId: value.BADGENO,
                                    full_Name: value.EMPLOYEE_NAME,
                                    contractNumber: value.CONTRACTNUMBER,
                                    vendorId: value.VENDORID,
                                    vendorName: value.VENDORNAME,
                                    sponsorName: value.contractData ? value.contractData.Sponsor_Name : null,
                                    contractPm: value.contractData ? value.contractData.ContractProjectManager : null,
                                    contractDesc: value.contractData ? value.contractData.ContractDescription : null,
                                    contractNo:value.contractData ?  value.contractData.ContractNo : null
                                });
                            });
                            <%: this.ID %>_pickEmployeeViewModel.setListEmployee(formattedList);
                            <%: this.ID %>_pickEmployeeViewModel.ds_employee_list.read();
                            
                        },
                        error: function (data) {
                            _showDialogMessage("Error", data, "");
                            
                        },
                        complete: function(jqXhr, textStatus) {
                            kendo.ui.progress($("#<%: this.ID %>_EmployeeGrid"), false);
                        }
                    });
                }
                
            } else {
                _showDialogMessage("Warning", "Please provide the badge number with 4 characters minimum!", "");
            }
        
        }
    }); // View Model
        
    function <%: this.ID %>_DataBind() {
       
        kendo.bind($("#<%: this.ID %>_PickEmployeeContent"), <%: this.ID %>_pickEmployeeViewModel);
        
    }
    function <%: this.ID %>_Init(employeetype) {
        // OPTIONAL INIT
        <%: this.ID %>_pickEmployeeViewModel.set("employeeTypeSelected", employeetype);

        if (employeetype == "contractor") {
            <%: this.ID %>_pickEmployeeViewModel.set("chkValeDisabled", true);
        }
        if (employeetype == "vale") {
            <%: this.ID %>_pickEmployeeViewModel.set("chkContractorDisabled", true);
        }
    }
    $(document).ready(function () {
        
        $("#<%: this.ID %>_employeeSearch").focus();
    }); //doc ready
</script>