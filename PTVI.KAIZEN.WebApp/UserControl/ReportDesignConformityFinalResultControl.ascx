﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDesignConformityFinalResultControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportDesignConformityFinalResultControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
    var <%: this.ID %>_ReportFinalResultGrid = [], <%: this.ID %>_actualCostFormatData = [], currYear = new Date().getFullYear();
    var <%: this.ID %>_finalResultReportModel = kendo.observable ({
        paramYear: currYear,
        listYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            }
        }),
        onChangeParamYear: function (e) {
            this.dsMasterFinalResultByYear.read()
            this.generateGriReportFinalResult();
        },
        dsFinalResultReport: new kendo.data.DataSource({
            requestStart: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), true);
                kendo.ui.progress($("#ReportFinalResultForm"), true);
            },
            requestEnd: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), false);
                kendo.ui.progress($("#ReportFinalResultForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "Report/designConformityFinalResult/" +  <%: this.ID %>_finalResultReportModel.paramYear;
                    },
                    async: false
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dsMasterFinalResultByYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#ReportFinalResultForm"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#ReportFinalResultForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "masterDCFinalResult/getByYear/" +  <%: this.ID %>_finalResultReportModel.paramYear;
                    },
                    async: false
                }
            },
        }),
        onDataBound: function (e) {
            var data = e.sender._data;
            $.each(data, function (i, row) {
                if (row.level == 0) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#C00000");
                } else if (row.level == 1) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#1F497D");
                } else if (row.level == 2) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#E26B0A");
                }
            });
        },
        dsFinalResultReportReformat: function () {
            var dataReformat = [];

            this.dsFinalResultReport.read();

            var dataOri = this.dsFinalResultReport.data();
            
            //generateColumnGrid
            <%: this.ID %>_ReportFinalResultGrid = [];
            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "deliverableNo", title: "Project No", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>deliverableNo-template').html()),
                width: 200,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });
            
            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "projectType", title: "CAP/OP", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>projectType-template').html()),
                width: 80,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "deliverableName", title: "Project Name / Designer Deliverable", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>deliverableName-template').html()),
                width: 300,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "pic", title: "Engineer / Designer", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 180,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                title: "Plan Date",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [{
                    field: "planStartDate", title: "Start",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                },
                {
                    field: "planFinishDate", title: "Finish",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                }]
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                title: "Actual Date",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [{
                    field: "actualStartDate", title: "Start",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                },
                {
                    field: "actualFinishDate", title: "Finish",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                }]
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "remarksActualResults", title: "Actual Results", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 250,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "remarksDesignKpi", title: "Design Kpi", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 250,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "category", title: "Category", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            //loop grid column dsMasterFinalResultByYear
            if (this.dsMasterFinalResultByYear.data().length > 0) {

                for (var j = 0; j < this.dsMasterFinalResultByYear.data().length; j++) {
                    var columnComplexity = [];
                    columnComplexity.push({
                        field: "finalResult_" + this.dsMasterFinalResultByYear.data()[j].id,
                        title: this.dsMasterFinalResultByYear.data()[j].resultValue + " %",
                        template: "#=data.level ? (data.finalResult_" + this.dsMasterFinalResultByYear.data()[j].id + " ? data.finalResult_" + this.dsMasterFinalResultByYear.data()[j].id + " : '-') : ''#",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center;"
                        }, width: 150,
                        attributes: { style: "vertical-align: top !important;" },
                    });
                    <%: this.ID %>_ReportFinalResultGrid.push({
                        title: this.dsMasterFinalResultByYear.data()[j].description,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        columns: columnComplexity
                    });
                }

            }

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "finalResult", title: "Final Result", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "#=data.level ? (data.finalResult ? kendo.toString(data.finalResult, 'n2') : '-') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "performance", title: "Performance", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "#=data.level ? (data.performance ? kendo.toString(data.performance, 'n2') : '-') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "factoredWeight", title: "Factored Weight", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "#=data.level ? (data.factoredWeight ? kendo.toString(data.factoredWeight, 'n2') : '-') : ''#",
                headerTemplate: kendo.template($('#<%: this.ID %>factoredWeight-headerTemplate').html()),
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportFinalResultGrid.push({
                field: "achievement", title: "Achievement", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "#=data.level ? (data.achievement ? kendo.toString(data.achievement, 'n2') : '-') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });


            //Generate Data
            for (var i = 0; i < dataOri.length; i++) {
                // new object for insert DataReformat
                var finalResultData = {};

                finalResultData["deliverableNo"] = dataOri[i].deliverableNo;
                <%: this.ID %>_actualCostFormatData["deliverableNo"] = { type: "string" };

                finalResultData["projectType"] = dataOri[i].projectType;
                <%: this.ID %>_actualCostFormatData["projectType"] = { type: "string" };

                finalResultData["deliverableName"] = dataOri[i].deliverableName;
                <%: this.ID %>_actualCostFormatData["deliverableName"] = { type: "string" };

                finalResultData["pic"] = dataOri[i].pic;
                <%: this.ID %>_actualCostFormatData["pic"] = { type: "string" };

                finalResultData["category"] = dataOri[i].category;
                <%: this.ID %>_actualCostFormatData["category"] = { type: "string" };

                finalResultData["participation"] = dataOri[i].participation;
                <%: this.ID %>_actualCostFormatData["participation"] = { type: "number" };

                finalResultData["planStartDate"] = kendo.parseDate(dataOri[i].planStartDate);
                <%: this.ID %>_actualCostFormatData["planStartDate"] = { type: "date" };

                finalResultData["planFinishDate"] = kendo.parseDate(dataOri[i].planFinishDate);
                <%: this.ID %>_actualCostFormatData["planFinishDate"] = { type: "date" };

                finalResultData["actualStartDate"] = kendo.parseDate(dataOri[i].actualStartDate);
                <%: this.ID %>_actualCostFormatData["actualStartDate"] = { type: "date" };

                finalResultData["actualFinishDate"] = kendo.parseDate(dataOri[i].actualFinishDate);
                <%: this.ID %>_actualCostFormatData["actualFinishDate"] = { type: "date" };

                finalResultData["remarksActualResults"] = dataOri[i].remarksActualResults;
                <%: this.ID %>_actualCostFormatData["remarksActualResults"] = { type: "string" };

                finalResultData["remarksDesignKpi"] = dataOri[i].remarksDesignKpi;
                <%: this.ID %>_actualCostFormatData["remarksDesignKpi"] = { type: "string" };

                finalResultData["level"] = dataOri[i].level;
                <%: this.ID %>_actualCostFormatData["level"] = { type: "number" };

                //generate type of projectDocumentFinalResult
                if (dataOri[i].projectDocumentFinalResult != null) {
                    for (var j = 0; j < dataOri[i].projectDocumentFinalResult.length; j++) {
                        finalResultData["finalResult_" + dataOri[i].projectDocumentFinalResult[j].masterFinalResultId] = dataOri[i].projectDocumentFinalResult[j].finalResultText;
                    }
                }

                finalResultData["finalResult"] = dataOri[i].finalResult;
                <%: this.ID %>_actualCostFormatData["finalResult"] = { type: "number" };

                finalResultData["performance"] = dataOri[i].performance;
                <%: this.ID %>_actualCostFormatData["performance"] = { type: "number" };

                finalResultData["factoredWeight"] = dataOri[i].factoredWeight;
                <%: this.ID %>_actualCostFormatData["factoredWeight"] = { type: "number" };

                finalResultData["achievement"] = dataOri[i].achievement;
                <%: this.ID %>_actualCostFormatData["achievement"] = { type: "number" };

                dataReformat[i] = finalResultData;
            }
            return dataReformat;
         },
         generateGriReportFinalResult: function () {
            var that = this;
            var dataGrid = that.dsFinalResultReportReformat();
            var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
            var dataSource = new kendo.data.DataSource({
                data: dataGrid,
                schema: {
                    model: {
                        id: "id",
                        fields: <%: this.ID %>_actualCostFormatData
                    }
                },
            });

            if ($(idGrid).getKendoGrid() != null) {
                $(idGrid).getKendoGrid().destroy();
                $(idGrid).empty();
            }

            if ($(idGrid).getKendoGrid() == null) {
                $(idGrid).kendoGrid({
                    toolbar: [{ template: kendo.template($("#toolbarTemplate").html()) }], // ["excel"],
                    dataSource: dataSource,
                    dataBound: <%: this.ID %>_finalResultReportModel.onDataBound,
                    columns: <%: this.ID %>_ReportFinalResultGrid,
                    scrollable: true,
                    excelExport: function (e) {
                        var sheet = e.workbook.sheets[0];
                        var rowIndex = 2, cell = 0;
                        var sheet = e.workbook.sheets[0];
                        $.each(e.data, function (key, value) {
                            if (rowIndex > 1) {
                                var row = sheet.rows[rowIndex];
 
                                row.cells[0].value = value.level == 0 ? row.cells[0].value : lpad('', (value.level * 4), ' ') + row.cells[0].value;
                                row.cells[2].value = value.level == 0 ? row.cells[2].value : lpad('', (value.level * 4), ' ') + row.cells[2].value;
                                if (value.level == 0) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#C00000";
                                        row.cells[cellIndex].bold = true;
                                    }
                                } else if (value.level == 1) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#1F497D";
                                    }
                                } else if (value.level == 2) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#E26B0A";
                                    }
                                }
                            }
                            rowIndex++;
                        });
                        for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                            var row = sheet.rows[rowIndex];
                            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                if (!isNaN(row.cells[cellIndex].value)) {
                                    row.cells[cellIndex].format = "#,##0.00_;"
                                }
                                if (rowIndex == 0 || rowIndex == 1) {
                                    sheet.rows[rowIndex].cells[cellIndex].vAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].hAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].bold = true;
                                }
                            }
                            if (row.type == "header") {
                                row.height = 30;
                            }
                        }

                    }
                });
            }

            $("#exportExcel").on("click", function () {
                var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
                var grid = $(idGrid).getKendoGrid();
                var column = grid.columns;
                $.each(column, function (key, value) {
                    value.title = value.title.replace("</br>", "\r");
                })
                grid.saveAsExcel();
            })

        }
    });
    $(document).ready(function () {
        <%: this.ID %>_finalResultReportModel.dsMasterFinalResultByYear.read();
        kendo.bind($("#<%: this.ID %>_EwpDrawingControl"), <%: this.ID %>_finalResultReportModel);
        <%: this.ID %>_finalResultReportModel.generateGriReportFinalResult();
        <%--$("#<%: this.ID %>_gridDrawingReport").data("kendoGrid").resize();--%>
    });
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>deliverableNo-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableNo#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>projectType-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#">#=data.projectType#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableName-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableName#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>factoredWeight-headerTemplate">
    <div style="text-align: center;">Factor<br>Weight</div>
</script>

<div id="<%: this.ID %>_EwpDrawingControl">
    <div>
        <span>Year: </span>
        <input data-role="dropdownlist"
               data-placeholder="Select Year"
               data-value-primitive="true"
               data-text-field="text"
               data-value-field="value"
               data-bind=" source: listYear, value: paramYear, events: { change: onChangeParamYear }"
               style="width: 100px" />
    </div>
    <div id="<%: this.ID %>_grdDrawingReportControl" style="height: 450px"
         data-filterable= "true">
    </div>
</div>