﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimesheetControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.TimesheetControl" %>
<%@ Register Src="~/UserControl/ReportTimesheetDetail.ascx" TagPrefix="uc1" TagName="ReportTimesheetDetail" %>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
</style>

<script type="text/x-kendo-template" id="hdrChbTSTemplate">
    <input type='checkbox' id='header-chbTS' class='k-checkbox checkboxHTimesheet'><label class='k-checkbox-label' for='header-chbTS'></label>
</script>
<script type="text/x-kendo-template" id="chbTSTemplate">
    <div>
     <input type='checkbox' id='chb#= newId#' class='k-checkbox checkboxTimesheet'  />
     <label class='k-checkbox-label' for='chb#= newId#'></label>
    </div>
</script>

<div style="display:none" id="popUpInputTaskTS">
        <div id="<%: this.ID %>_grdInputTask" data-role="grid" data-bind="source: InputTaskSource"  style="width: 750px"
            data-filterable= "true"
            data-columns="[
            {headerTemplate: kendo.template($('#hdrChbTSTemplate').html()),
            template: kendo.template($('#chbTSTemplate').html()), width: 50},
            {field: 'detailInformation.no', title: 'Proj/TS No.', width: 180, template:'<div>#= detailInformation?detailInformation.no:\'&nbsp;\' # </div>'},
            {field: 'detailInformation.desc', title: 'Description',width: 150, template:'<div>#= detailInformation?detailInformation.desc:\'&nbsp;\' # </div>'},
            {field: 'task', title: 'Task', width: 150, template:'<div>#= task?task:\'&nbsp;\' # </div>'},
            {field: 'planDate', title: 'Plan Date', width: 90, template:'<div>#= planDate ? kendo.toString(planDate,\'dd MMM yyyy\') : \'-\' # </div>'},
            {field: 'planHours', title: 'Plan Hours', width: 110, template:'<div>#= planHours ? planHours : \'-\' # </div>'},
            ]"
               data-pageable="true"
            ></div>                
            <input id="btnShowTask" 
                data-role="button" 
                data-bind="events: {click: viewTask}"
             value="View" class="k-button" style="width:70px"/>    
</div>

<div style="display:none" id="popViewTSDetails">
    <uc1:ReportTimesheetDetail runat="server" ID="ReportTimesheetDetail" />
</div>


<div style="display:none" id="popUpOHActTS">
    <div style="margin-bottom: 3px">
        <label for="listActivity" class="labelProjectDoc">Other Activity</label>
        <input id="listActivity" data-role="dropdownlist" data-option-label="Select Activity" data-bind="source: dsOHActivity, events: { change: OHAChange }" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" required="required"/> 
    </div>
    <div style="margin-bottom: 3px">
        <label for="OHActDesc" class="labelProjectDoc">Description</label>
        <span class="k-dropdown"><textarea id="OHActDesc" data-bind="value: OHActDesc, events: { focus: OHAChange }" class="k-textbox" required="required" style="width: 500px; height: 100px"></textarea></span>
    </div>
    <div style="margin-bottom: 3px">
        <label for="OHActSpentHr" class="labelProjectDoc">Spent Hours</label>
        <span class="k-dropdown">
    <%--        <input type="number" id="OHActSpentHr" data-bind="value: OHActSpentHr, events: { focus: OHAChange }" class="k-textbox" min="0" required="required" style="width: 100px"/>--%>
            <input id="OHActSpentHr" type="text"
                           data-min="0"
                           data-max="10"
                           required
                           style="width: 260px;"
                data-bind="value: OHActSpentHr"
                    />
        </span>
    </div>
    <hr />
    <div style="text-align:right">
    <input id="btnOHASave" data-role="button"
        value="Save" class="k-button" style="width:80px"/>    
    <input id="btnOHACancel" data-role="button"
        value="Cancel" class="k-button" style="width:80px"/>    
    </div>
</div>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div>Date :
            <input id="weeklyPick" style="width:210px" >
            <div id="btnViewTimesheet" data-role="button" value="View" class="k-button" style="width:70px" data-bind="events: { click: viewTS }" >View</div>    
        </div><br />
        <div>(Every Monday to Sunday)</div><br />
        <div id="btnAddOHActTS" data-role="button"
            value="Add Other Activity" class="k-button" style="width:200px" data-bind="events: { click: OHAClick }">Add Other Activity</div>    
        <div id="btnInputTaskTS" data-role="button"
            value="Input Tasks" class="k-button" style="width:120px" data-bind="events: { click: inputTaskClick }">Input Tasks</div>    
        <div id="btnViewReportDetail" data-role="button"
            value="Report Timesheet Details" class="k-button" style="width:120px" data-bind="events: { click: viewTimesheetsDetails }">TS Detail</div>    
        

        <div id="<%: this.ID %>_grdTimesheet" 
            data-role="grid" data-bind="source: TimesheetSource, events:{ dataBound: tsDataBound }"  style="width: 1050px"
            data-filterable= "true"
            data-columns="[
            {field: 'projectNo', title: 'Proj. No.', width: 110, template: '<div>#= data.projectNo ? data.projectNo :\'-\'#</div>'},
            {field: 'descriptionOha', title: 'Description',width: 150, template: '<div>#= data.descriptionOha ? data.descriptionOha:data.description#</div>'},
            {field: 'task', title: 'Task', width: 150, template: '<div>#= data.task ? data.task:\'-\'#</div>'},
            {field: 'planDate', title: 'Plan Date', width: 90, template: '<div>#= kendo.toString(data.planDate, \'dd MMM yyyy\')!=\'01 Jan 0001\'?kendo.toString(data.planDate, \'dd MMM yyyy\'):\'-\'#</div>'},
            {field: 'actualDate', title: 'Actual Date', width: 90, template: '<div>#= data.actualDate ? kendo.toString(data.actualDate, \'dd MMM yyyy\')!=\'01 Jan 0001\'?kendo.toString(data.actualDate, \'dd MMM yyyy\'):\'-\' : \'-\'#</div>'},
            {field: 'planHours', title: 'Plan Hours', width: 90, template: '<div>#= data.planHours?data.planHours:\'-\'#</div>'},
            {field: 'spentHours', title: 'Spent Hours', width: 90, template: '<div class=\'editSpentHours\'>#= data.spentHours?data.spentHours:\'-\'#</div>'},
            {field: 'weekHours', title: 'Hrs this Week', width: 90, template: '<div>#= data.weekHours?data.weekHours:\'-\'#</div>'},
            {field: 'status', title: 'Status', width: 110, template: '<div>#= data.status ? common_getStatusIpromDesc(data.status):\'-\'#</div>'},
            {command:['edit',{text: 'Hide', name:'delete', iconClass: 'k-icon k-i-close'}], width: 110},
<%--            {command:['edit',{text: 'Hide', name:'delete', iconClass: 'k-icon k-i-close'}], width: 110},--%>
            ]" data-pageable="true",
            data-editable="inline"
            ></div>
    </div>
</div>
<script>
    var defWeek;

    function editOnSpentHours() {
        $(".editSpetnHours").closest('td').on("click", function () {

            $("#<%: this.ID %>_grdTimesheet").getKendoGrid().editRow($(this));
        });
    }

    var <%: this.ID %>_weeklyListSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTimesheet"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTimesheet"), false);
                editOnSpentHours();
            },
            transport: {
                read    : { dataType: "json",
                    url: _webApiUrl + "weeklies/weeklyList/1"
                    },
            },
            schema: {
                parse: function (e) {
                    defWeek = e.defWeek;
                    $("#weeklyPick").getKendoDropDownList().value(defWeek);
                    <%: this.ID %>_inputTaskSource.transport.options.read.url = _webApiUrl + "timesheet/listInputTask/1?userID=" + _currNTUserID + "&badgeNo=" + _currBadgeNo + "&wId=" + $("#weeklyPick").getKendoDropDownList().value();
                    <%: this.ID %>_timesheetSource.transport.options.read.url = _webApiUrl + "timesheet/pickTimesheetByUserIdWid/1?userID=" + _currNTUserID + "&wId=" + $("#weeklyPick").getKendoDropDownList().value();
                    <%: this.ID %>_viewModel.set("defWeek", defWeek);
                    <%: this.ID %>_timesheetSource.read();

                    return e.Wl
                },
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        enddate: { type: "date" },
                        month: { type: "number" },
                        startdate: { type: "date" },
                        week: { type: "number" },
                        year: { type: "number" },
                        title: { type: "string" },
                    },
                },
            },
    });

    var <%: this.ID %>_inputTaskSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdInputTask"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdInputTask"), false);
            },
            transport: {
                read    : { dataType: "json",
                    url: ""
                    },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        docType: { type: "string" },
                        docDesc: { type: "string" },
                        docNo: { type: "string" },
                        docTitle: { type: "string" },
                        drawingNo: { type: "string" },
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        planDate: { type: "date" },
                        planHours: { type: "number" },
                        disciplinedrawingNo: { type: "string" },
                        status: { type: "string" },
                        remark: { type: "string" },
                        discipline: { type: "string" },
                        statusdrawingNo: { type: "string" },
                        remarkdrawingNo: { type: "string" },
                        executordrawingNo: { type: "string" },
                        executorAssignmentdrawingNo: { type: "string" },
                        progress: { type: "number" },
                        documentById: { type: "string" },
                        documentByName: { type: "string" },
                        documentByEmail: { type: "string" },
                        documentTrafic: { type: "string" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                },
            },
        pageSize: 10,
        batch: false
    });
    var <%: this.ID %>_timesheetSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTimesheet"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTimesheet"), false);
            },
            transport: {
                read    : { dataType: "json", type: "GET",
                    url: ""
                    },
                create: {
                    dataType: "json", type: "POST",
                    url: _webApiUrl + "timesheet/inputTask/1"
                },
                update: {
                    dataType: "json", type: "PUT",
                    url: function(e){
                        return _webApiUrl + "timesheet/updateTimesheet/"+e.id
                        }
                },
                destroy: {
                    dataType: "json", type: "DELETE",
                    url: function(e){
                        return _webApiUrl + "timesheet/deleteTimesheet/"+e.id
                    }
                },
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        return options;
                    }
                    if (operation == "update") {
                        options.hours = options.spentHours;
                        options.project = {};
                        options.projectDocument = {};
                        options.technicalSupportInteractionObject = {};
                        options.technicalSupportObject = {};
                        options.technicalSupportId != 0 ? options.technicalSupportId : null;
                        options.technicalSupportUserActionId != 0 ? options.technicalSupportUserActionId : null;
                        return options;
                    }
                }
            },
            schema: {
                model: {
                        id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        projectNo: { type: "string", editable: false },
                        descriptionOha: { type: "string", editable: false },
                        overheadActivity: { type: "string" },
                        task: { type: "string", editable: false },
                        weeklyId: {type: "number"},
                        planDate: { type: "date", editable: false  },
                        actualDate: { type: "date", editable: false  },
                        spentHours: { type: "number" },
                        planHours: { type: "number", editable: false  },
                        weekHours: { type: "number", editable: false  },
                        status: { type: "string", editable: false  },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                        },
                },
            },
        pageSize: 10,
        batch: false,
        confirmation: true
    });

    $("#weeklyPick").kendoDropDownList({
        dataSource: <%: this.ID %>_weeklyListSource, 
        value: defWeek,
        valuePrimitive: true,
        optionlabel:"Select Weekly",
        dataTextField: "title",
        dataValueField: "id",
        change: function () {
            <%: this.ID %>_inputTaskSource.transport.options.read.url = _webApiUrl + "timesheet/listInputTask/1?userID=" + _currNTUserID + "&badgeNo=" + _currBadgeNo + "&wId=" + $("#weeklyPick").getKendoDropDownList().value();
            <%: this.ID %>_timesheetSource.transport.options.read.url = _webApiUrl + "timesheet/pickTimesheetByUserIdWid/1?userID=" + _currNTUserID + "&wId=" + $("#weeklyPick").getKendoDropDownList().value();
        }
    });

    $("#popUpInputTaskTS").kendoWindow({
        title: "Input Task",
        modal: true,
        width: 800,
    });

    $("#popViewTSDetails").kendoWindow({
        title: "Timesheets Details",
        modal: true,
//        width: 100%,
    });

    $("#popUpOHActTS").kendoWindow({
        title: "Add Other Activity",
        modal: true,
        width: 800,
    });

    var checkedTSLists = {};
    function <%: this.ID %>_gridClick() {
        var checked = this.checked,
        row = $(this).closest("tr"),
        grid = $("#<%: this.ID %>_grdInputTask").data("kendoGrid"),
        dataItem = grid.dataItem(row);
        checkedTSLists[dataItem.newId] = checked;
        if (checked) {
            //-select the row
            row.addClass("k-state-selected");
        } else {
            //-remove selection
            row.removeClass("k-state-selected");
        }
        $.each(checkedTSLists, function (key, value) {
            if (checkedTSLists[key] == true) {
                $("#btnShowTask").removeClass("k-state-disabled");
                $("#btnShowTask").removeAttr("disabled");
                $("#btnShowTask").removeAttr("aria-disabled");
                return false;
            }
            $("#btnShowTask").addClass("k-state-disabled");
            $("#btnShowTask").attr("disabled", "disabled");
            $("#btnShowTask").attr("aria-disabled", true);
            $('.checkboxHTimesheet')[0].checked = false;
        })

    }

    function <%: this.ID %>_gridHClick() {
        var cbh = this;
        $('.checkboxTimesheet').each(function (idx, item) {
        var checked = this.checked,
        row = $(item).closest("tr"),
        grid = $("#<%: this.ID %>_grdInputTask").data("kendoGrid"),
        dataItem = grid.dataItem(row);
            if (cbh.checked==true) {
                //-select the row
                row.addClass("k-state-selected");
                item.checked = true;
                $("#btnShowTask").removeClass("k-state-disabled");
                $("#btnShowTask").removeAttr("disabled");
                $("#btnShowTask").removeAttr("aria-disabled");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
                item.checked = false;
                $("#btnShowTask").addClass("k-state-disabled");
                $("#btnShowTask").attr("disabled", "disabled");
                $("#btnShowTask").attr("aria-disabled", true);
            }
            checkedTSLists[dataItem.newId] = item.checked;
        })
    }

    function <%: this.ID %>_clickUpdate() {
        var grid = $("#<%: this.ID %>_grdInputTask").getKendoGrid();
        var data = grid.dataSource.data();
        var ts;
        var gridTS = $("#<%: this.ID %>_grdTimesheet").data("kendoGrid");
        for (var i = 0; i < data.length; i++) {
            if (checkedTSLists[data[i].newId]) {
                //ts = {
                //    id: 0, projectId: data[i].projectId, technicalSupportId: data[i].technicalSupportId, projectDocumentId: data[i].docType ? data[i].id : null,
                //    description: data[i].docDesc, createdBy: _currNTUserID, spentHours: data[i].planHours,
                //    updatedBy: _currNTUserID, project: {}, task: data[i].task, planDate: kendo.toString(data[i].planDate, \'dd MMM yyyy\'),
                //    actualDate: kendo.toString(data[i].actualDate, \'dd MMM yyyy\'), projectNo: null, weeklyId: $("#weeklyPick").getKendoDropDownList().value(),
                //    hours: data[i].planHours, planHours: data[i].planHours, weekHours: null, descriptionOha: null
                //}
                ts = {
                    id: 0,
                    projectId: data[i].projectId, projectDocumentId: data[i].projectDocumentId,
                    projectNo: data[i].detailInformation.no, description: data[i].detailInformation.desc,
                    technicalSupportId: data[i].technicalSupportId, technicalSupportUserActionId: data[i].technicalSupportUserActionId,
                    task: data[i].task,
                    status: data[i].status,
                    planDate: kendo.toString(data[i].planDate, 'dd MMM yyyy'), spentHours: data[i].planHours,
                    weeklyId: $("#weeklyPick").getKendoDropDownList().value(),
                    createdBy: _currNTUserID, updatedBy: _currNTUserID
                };
                gridTS.dataSource.add(ts);
                ts = {};
            }
        }
        checkedTSLists = {};
        gridTS.dataSource.sync();
        $("#btnShowTask").addClass("k-state-disabled");
        $("#btnShowTask").attr("disabled", "disabled");
        $("#btnShowTask").attr("aria-disabled", true);

        editOnSpentHours();

        $("#popUpInputTaskTS").getKendoWindow().close();

    }

    function <%: this.ID %>_clickCancelOHA() {
        ts = {};
        $(".k-tooltip-validation").remove();
        $("#listActivity").getKendoDropDownList().value(null);
        <%: this.ID %>_vmOHActivity.set("OHActDesc", null);
        <%: this.ID %>_vmOHActivity.set("OHActSpentHr", null);
        $("#btnShowTask").addClass("k-state-disabled");
        $("#btnShowTask").attr("disabled", "disabled");
        $("#btnShowTask").attr("aria-disabled", true);
        $("#popUpOHActTS").getKendoWindow().close();
    }

    function <%: this.ID %>_clickSaveOHA() {
        var valid=true

        if (!$('#listActivity')[0].checkValidity()) {
            $('#listActivity').after('<div class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" style="margin: 0.5em; display: block;" data-for="listActivity" role="alert"><span class="k-icon k-i-warning"></span>Other Activity is required<div class="k-callout k-callout-n"></div></div>');
            valid = false;
        };
        if (!$('#OHActDesc')[0].checkValidity()) {
            $('#OHActDesc').after('<div class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" style="margin: 0.5em; display: block;" data-for="OHActDesc" role="alert"><span class="k-icon k-i-warning"></span>Description is required<div class="k-callout k-callout-n"></div></div>');
            valid = false;
        };
        if (!$('#OHActSpentHr')[0].checkValidity()) {
            $('#OHActSpentHr').after('<div class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" style="margin: 0.5em; display: block;" data-for="OHActSpentHr" role="alert"><span class="k-icon k-i-warning"></span>Spent Hours is required<div class="k-callout k-callout-n"></div></div>');
            valid = false;
        }
        if (valid) {
            var ts;
            var gridTS = $("#<%: this.ID %>_grdTimesheet").data("kendoGrid");
            ts = {
                id: 0, projectId: null, overheadActivity: $("#listActivity").getKendoDropDownList().value(), spentHours: null,
                description: <%: this.ID %>_vmOHActivity.OHActDesc, task: $("#listActivity").getKendoDropDownList().text(),
                weekHours:<%: this.ID %>_vmOHActivity.OHActSpentHr, createdBy: _currNTUserID, updatedBy: _currNTUserID,
                project: {}, task: null, planDate: null, actualDate: null, planHours: null, projectNo: null, descriptionOha: null,
                weeklyId: $("#weeklyPick").getKendoDropDownList().value(), hours: <%: this.ID %>_vmOHActivity.OHActSpentHr
            }
            gridTS.dataSource.add(ts);
            gridTS.dataSource.sync();
            ts = {};
            $(".k-tooltip-validation").remove();
            $("#listActivity").getKendoDropDownList().value(null);
            <%: this.ID %>_vmOHActivity.set("OHActDesc", "");
            <%: this.ID %>_vmOHActivity.set("OHActSpentHr", null);
            gridTS.refresh();
            $("#btnShowTask").addClass("k-state-disabled");
            $("#btnShowTask").attr("disabled", "disabled");
            $("#btnShowTask").attr("aria-disabled", true);
            $("#popUpOHActTS").getKendoWindow().close();
        }
    }

    var <%: this.ID %>_ohaSource = new kendo.data.DataSource({
        transport: {
            read    : { dataType: "json",
                url: _webApiUrl + "lookup/findbytype/overheadTimesheet"
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        },

    })


    var <%: this.ID %>_vmPopInputTask = new kendo.observable({
        InputTaskSource: <%: this.ID %>_inputTaskSource,
        viewTask: <%: this.ID %>_clickUpdate
    });


    var <%: this.ID %>_vmOHActivity = new kendo.observable({
        dsOHActivity: <%: this.ID %>_ohaSource,
        listActivity:null,
        OHActDesc:null,
        OHActSpentHr: null,
        OHAChange: function () {
            $(".k-tooltip-validation").remove();
        }
    });

    function timesheetInputTaskClick(e) {
        kendo.bind($("#popUpInputTaskTS"), <%: this.ID %>_vmPopInputTask); 
        $("#popUpInputTaskTS").getKendoWindow().center().open();
        var grid = $("#<%: this.ID %>_grdInputTask").data("kendoGrid");
        grid.table.on("click", ".checkboxTimesheet", <%: this.ID %>_gridClick);
        $('#header-chbTS').click(<%: this.ID %>_gridHClick);
        grid.dataSource.read();
    }

    function timesheetOHAClick(e) {
        kendo.bind($("#popUpOHActTS"), <%: this.ID %>_vmOHActivity); 
        $("#popUpOHActTS").getKendoWindow().center().open();
        $("#OHActSpentHr").kendoNumericTextBox();
    }

    function viewTimesheetsDetails(e) {
        $("#popViewTSDetails").getKendoWindow().center().open();
    }

    var <%: this.ID %>_viewModel = new kendo.observable({
        TimesheetSource: <%: this.ID %>_timesheetSource,
        dsWeeklyListSource: <%: this.ID %>_weeklyListSource,
        inputTaskClick: timesheetInputTaskClick,
        viewTimesheetsDetails: viewTimesheetsDetails, 
        OHAClick: timesheetOHAClick,
        defWeek: null,
        tsDataBound: function (e) {
            var grid = e.sender;
            var gridRows = grid.tbody.find("tr");
            for (var i = 0; i < gridRows.length; i++) {
                var row = $(gridRows[i]);
                var dataItem = grid.dataItem(row);
                //perform your custom check
                if ((dataItem.projectNo) && (dataItem.weekHours || dataItem.weekHours == 0))
                    row.find(".k-grid-delete").html("<span class='k-icon k-i-close'></span>Hide");
                else
                    row.find(".k-grid-delete").html("<span class='k-icon k-i-close'></span>Hide");

            }
            editOnSpentHours();
        },
        hideTimeSheet: function (e) {
            e.preventDefault();
            var tr = $(e.target).closest("tr");
            var data = this.dataItem(tr);
            // if (e.currentTarget.text == "Delete") {
            //                if (confirm("Delete Data?") == true) {
            var gridTS = $("#<%: this.ID %>_grdTimesheet").data("kendoGrid");
            //                    gridTS.dataSource.remove(data);
            //                    gridTS.dataSource.sync();
            //              };
            //            } else {            };
        },
        viewTS: function () {
            var gridTS = $("#<%: this.ID %>_grdTimesheet").data("kendoGrid");
            gridTS.dataSource.read();
        }

    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel);
        $("#btnOHASave").kendoButton({
            enable: true,
            click: <%: this.ID %>_clickSaveOHA
        });


        $("#btnOHACancel").kendoButton({
            enable: true,
            click: <%: this.ID %>_clickCancelOHA
        });

        editOnSpentHours();

    });
    </script>
