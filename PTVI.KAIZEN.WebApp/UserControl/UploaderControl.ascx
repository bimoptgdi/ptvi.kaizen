﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploaderControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.UploaderControl" %>

<script>
    
    //var urlSupportingDocumentUpload = "";
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_uploaderFile;
    var <%: this.ID %>_selectedFileName;
    var <%: this.ID %>_successHandlerUploader;

    var <%: this.ID %>_oid, <%: this.ID %>_type, <%: this.ID %>_badgeNo;

    $(document).ready(function () {
        
    });

    function <%: this.ID %>_onSuccess(e) {
        $("#lblInfo").text("Upload complete.");
        //_showMessage("Attachment", "Upload success", 1, "");
    }

    function <%: this.ID %>_onSelect(e) {
        var files = e.files;
        
    }

    function <%: this.ID %>_onSelectHideButton(e) {

        var files = e.files;
        setTimeout(function () {
            <%: this.ID %>_kendoUploadButton = $(".k-upload-selected");
            <%: this.ID %>_kendoUploadButton.hide();
        }, 1);
    }

    function <%: this.ID %>_onSelectExcelOnly(e) {
        var files = e.files;
        
        $.each(files, function (index, value) {
            <%: this.ID %>_selectedFileName = value.name;
            if (value.extension.toLowerCase() != ".xls" && value.extension.toLowerCase() != ".xlsx") {
                _showErrorMessage("Upload Error", "Only excel files can be uploaded.", "", 3, "");
                e.preventDefault();
            }
        });
        
    }

    function <%: this.ID %>_onUpload(e) {
        e.data = { typeId: <%: this.ID %>_oid, typeName: <%: this.ID %>_type, badgeNo: <%: this.ID %>_badgeNo };
    }

    function <%: this.ID %>_onError(e) {
        // Array with information about the uploaded files
        var files = e.files;
        var err = e.XMLHttpRequest;

        if (e.operation == "upload") {
            _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
        }
    }

    //===
    function <%: this.ID %>_uploaderInit(oid, type, badgeNo) {

        <%: this.ID %>_oid = oid;
        <%: this.ID %>_type = type;
        <%: this.ID %>_badgeNo = badgeNo;

        if (!<%: this.ID %>_uploaderFile) {
            <%: this.ID %>_uploaderFile = $("#<%: this.ID %>_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload",
                    removeUrl: "remove",
                    batch: true,
                    autoUpload: false

                },
                multiple: true,
                select: <%: this.ID %>_onSelect,
                upload: <%: this.ID %>_onSelect,
                success: <%: this.ID %>_onSuccess,
                error: <%: this.ID %>_onError,
                localization: {
                    select: 'Browse...'
                },
                upload: <%: this.ID %>_onUpload
            }).data("kendoUpload");
        }

    }
   
    function <%: this.ID %>_uploaderCopyOnlyInit(oid, type, badgeNo) {

        <%: this.ID %>_oid = oid;
        <%: this.ID %>_type = type;
        <%: this.ID %>_badgeNo = badgeNo;

        if (!<%: this.ID %>_uploaderFile) {
            <%: this.ID %>_uploaderFile = $("#<%: this.ID %>_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload/CopyAttachmentFile/" + oid,
                    removeUrl: "remove",
                    batch: true,
                    autoUpload: false

                },
                multiple: true,
                select: <%: this.ID %>_onSelect,
                upload: <%: this.ID %>_onSelect,
                success: <%: this.ID %>_onSuccess,
                error: <%: this.ID %>_onError,
                localization: {
                    select: 'Browse...'
                },
                upload: <%: this.ID %>_onUpload
            }).data("kendoUpload");
        }

    }

    function <%: this.ID %>_uploaderInitRejectDoc(oid, type, badgeNo) {

        <%: this.ID %>_oid = oid;
        <%: this.ID %>_type = type;
        <%: this.ID %>_badgeNo = badgeNo;

        if (!<%: this.ID %>_uploaderFile) {
            <%: this.ID %>_uploaderFile = $("#<%: this.ID %>_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload/PostUploadRejectDoc/x",
                    removeUrl: "remove",
                    batch: true,
                    autoUpload: false

                },
                multiple: true,
                select: <%: this.ID %>_onSelect,
                upload: <%: this.ID %>_onSelect,
                success: <%: this.ID %>_onSuccess,
                error: <%: this.ID %>_onError,
                localization: {
                    select: 'Browse...'
                },
                upload: <%: this.ID %>_onUpload
            }).data("kendoUpload");
        }

    }

    function <%: this.ID %>_uploaderInitHideUploadButton(oid, type, badgeNo) {

        <%: this.ID %>_oid = oid;
        <%: this.ID %>_type = type;
        <%: this.ID %>_badgeNo = badgeNo;

        if (!<%: this.ID %>_uploaderFile) {
            <%: this.ID %>_uploaderFile = $("#<%: this.ID %>_uploaderFile").kendoUpload({
                async: {
                    saveUrl: _webApiUrl + "upload",
                    removeUrl: "remove",
                    batch: true,
                    autoUpload: false

                },
                multiple: true,
                select: <%: this.ID %>_onSelectHideButton,
                upload: <%: this.ID %>_onSelectHideButton,
                success: <%: this.ID %>_onSuccess,
                error: <%: this.ID %>_onError,
                localization: {
                    select: 'Browse...'
                },
                upload: <%: this.ID %>_onUpload
            }).data("kendoUpload");
        }

    }
    //===
    
</script>
<div style="margin: 3px; padding: 10px; border: solid 1px #007e7a;">
    <fieldset class="adrFieldset">
        <legend class="adrLegend">Upload Document</legend>
        <div id="info" style="margin-bottom: 10px">
            <label id="lblInfo" class="labelForm"></label>
        </div>
        <div>
            <input type="file" id="<%: this.ID %>_uploaderFile" name="<%: this.ID %>_uploaderFile" />
        </div>
       
    </fieldset>


</div>
