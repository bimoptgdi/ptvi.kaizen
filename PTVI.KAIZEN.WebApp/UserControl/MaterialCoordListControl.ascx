﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaterialCoordListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.MaterialCoordListControl" %>
<div id="MaterialsCoordWindow" style="display: none">
    <div class="inputProjectDoc" >
        <label class="labelProjectDoc">PR Plan Date</label> <input id="mccDtPRPlanDate" data-role="datepicker" data-bind="value: emDtPRPlanDate"/>
    </div>
    <hr/>
    <div class="inputProjectDoc" style="text-align: right; display:block">
        <div data-role="button"
                data-bind="events: { click: onUpdateClick }"
        >Update</div>    
        <div data-role="button"
                data-bind="events: { click: onCancelClick }"
        >Cancel</div>    
    </div>
</div>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .engMaterialsContainer{
        width: 400px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }
</style>

<script type="text/x-kendo-template" id="TraficMCCStatusTemplate">
    <div style='text-align: center'> 
        #= documentTraficPR ? 
            "<img class='logo' src='images/" + common_getTrafficImageUrl(documentTraficPR)+ "' style='padding-top: 0px;' title='"+common_getTrafficHint(documentTraficPR)+"'>"
        :
            "&nbsp;" 
        #
    </div>
</script>

<script type="text/x-kendo-template" id="MCCPRDateTemplate">
    <div class="subColumnTitle">Planned</div>
    <div>#= prPlanDate?kendo.toString(prPlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div #= prIssuedDate>prPlanDate ? "style='color:red'":"" #>#= prIssuedDate?kendo.toString(prIssuedDate,"dd MMM yyyy") : "-" #</div>
</script>

<script type="text/x-kendo-template" id="hdrChbMCCTemplate">
    <input type='checkbox' id='header-chb' class='k-checkbox checkboxHEngMaterial'><label class='k-checkbox-label' for='header-chb'></label>
</script>

<script type="text/x-kendo-template" id="chbMCCTemplate">
#= prIssuedDate? "&nbsp;" :
    "<div>"+
     "<input type='checkbox' id='chb"+ id +"' class='k-checkbox checkboxEngMaterial' />"+
     "<label class='k-checkbox-label' for='chb"+ id +"'></label>"+
    "</div>" #
</script>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div id="lastUpdateText"></div>
        <div id="<%: this.ID %>_grdDocuments" data-role="grid" data-bind="source: EngMaterialSource, events: { dataBound: onDataBound }"  style="width: 1050px"
            data-filterable="true"
            data-pageable="true"
            data-columns="[
<%--            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.MaterialStatusReport))
                        { %>{headerTemplate: kendo.template($('#hdrChbMCCTemplate').html()), 
             template: kendo.template($('#chbMCCTemplate').html()), width: 100},<% } %>--%>
            {field:'material', title: 'Stock Code', template:'<div>#= material ? material : \'-\' # </div>', width: 100},
            {field: 'materialDesc',title: 'Description', template:'<div>#= materialDesc ? materialDesc : \'-\' # </div>', width: 100},
            {field: 'mrNo', title: 'MR.No.', template:'<div>#= mrNo ? mrNo : \'-\' # </div>', width: 100},
<%--            {title: 'Item', template:'<div>#= lineItem ? lineItem : \'-\' #</div>', width: 100},--%>
            {title: 'Qty', template:'<div>#= orderQuantity ? orderQuantity : \'-\' #</div>', width: 40},
            {title: 'Unit', template:'<div>#= uom ? uom : \'-\' #</div>', width: 40},
<%--            {title: 'S/C', template:'<div>#= material ? material : \'-\' #</div>', width: 80},--%>
            {title: 'Network', template:'<div>#= network ? network : \'-\' #</div>', width: 80},
            {title: 'PR', 
            columns: [{ 
                        title: 'Item',
                        template:'<div>#= prItmNo?prItmNo:\'-\' #</div>', width: 40
                        },{ 
                        title: 'Date',
                        template:kendo.template($('#MCCPRDateTemplate').html()), width: 90
                        },{
                        title: 'PR. No.',
                        template: '<div>#= prMPNo?prMPNo:\'-\' #</div>', width: 80
                        }],
            headerAttributes: {
      	            style: 'font-weight: bold;text-align: center;'
    	        }
            },
            {title: 'Reservation', 
            columns: [{ 
                        title: 'Res. No',
                        template:'<div>#= reservation?reservation:\'-\' #</div>', width: 100,
                        },{
                        title: 'Qty.',
                        template: '<div>#= orderQuantity?orderQuantity:\'-\' #</div>', width: 40
                        },{
                        title: 'Engineer',
                        template: '<div>#= engineer?engineer:\'-\' #</div>', width: 120
                        }],
            headerAttributes: {
      	            style: 'font-weight: bold;text-align: center;'
    	        }
            },
            {title: 'Status', filterable: false, 
             template: kendo.template($('#TraficMCCStatusTemplate').html()), width: 40, 
            }]"
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.MaterialStatusReport))
                        { %>data-editable= "{mode: 'popup', window:{width:400, title: 'Engineering Material', modal: true}}", <% } %>
            data-toolbar= ""
            ></div>
<%--             <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.MaterialStatusReport))
                        { %><input id="btnEMCoordUpdate" data-role="button"
                    data-bind="events: { click: onClick }"
             value="Edit" class="k-button" style="width:70px" disabled/><% } %>--%>

    </div>
</div>

<script>
    $("#MaterialsCoordWindow").kendoWindow({
        width: 350,
        title: "Update Material"
    });
    //ini buat apa? bikin error di ie
    //class obj {

    //}

    var <%: this.ID %>_engMaterialSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), false);
            },
            transport: {
                read    : { dataType: "json",
                    url: _webApiUrl + "materialcomponents/GetByPrIDforPS/" + _projectId
                    },
                update: {
                    type: "PUT", 
                    url: function (e) {
                        return _webApiUrl + "materialcomponents/updateEngMaterial/1";
                    }

                },
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        return options;
                    }
                    if (operation == "update") {
                        for (i = 0; i < options.models.length; i++) {
                            options.models[i].materialPlan[0].prPlanDate = kendo.toString(<%: this.ID %>_emPopupModel.emDtPRPlanDate, 'MM/dd/yyyy');
                            options.models[i].materialPlan[0].prPlanBy = _currNTUserID;
                            options.models[i].rfqDate = null;
                            options.models[i].planOnSite = null;
                            options.models[i].poPlanDate = null;
                            options.models[i].poRaisedDate = null;
                            options.models[i].prIssuedDate = null;
                            options.models[i].prPlanDate = null;
                            options.models[i].shipmentEndDate = null;
                            options.models[i].etaSiteDateMSt = null;
                            options.models[i].updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.models[i].createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.models[i].updatedBy = _currNTUserID;
                            options.models[i].setMaterialPlan = options.models[i].materialPlan;
                            checkedIdsMCC = [];
                            var checkboxHEngMaterial = $('.checkboxHEngMaterial');
                            checkboxHEngMaterial[0].checked = false;
                        }
                        return {"":options.models};
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        engineer : { type: "string" },
                        ewpNo : { type: "string" },
                        lineItem : { type: "string" },
                        material : { type: "string" },
                        materialDesc : { type: "string" },
                        mrNo: { type: "string" },
                        network : { type: "string" },
                        networkActivity : { type: "string" },
                        orderQuantity : { type: "number" },
                        planOnSite: { type: "date" },
                        poItemNo : { type: "string" },
                        poNo : { type: "string" },
                        pritemNo : { type: "string" },
                        prNo : { type: "string" },
                        projectDocumentId : { type: "number" },
                        projectNo : { type: "string" },
                        receivedQuantity : { type: "number" },
                        reservation : { type: "string" },
                        reservationItemNo : { type: "string" },
                        uom : { type: "string" },
                        wbsNo : { type: "string" },
                        withdrawnQuantity : { type: "number" },
                        prIssuedDate: { type: "date" },
                        poRaisedDate: { type: "date" },
                        shipmentEndDate: { type: "date" },
                        prPlanDate: { type: "date" },
                        rfqDate: { type: "date" },
                        poPlanDate: { type: "date" },
                        prMPNo: { type: "string" },
                        prItmNo: { type: "string" },
                        orderedQuantityMS: { type: "number" },
                        receivedQuantityMS: { type: "number" },
                        etaSiteDateMSt: { type: "date" },
                        actualDeliveryDateMSt: { type: "date" },
                        trafficMethodMSt: { type: "string" },
                        LocationMSt: { type: "string" },
                        officerNameMp: { type: "string" },
                        LocationMSt: { type: "string" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_projectDocSource.read();
                        alert("Process failed: "+d);
                        return 0;
                    } else{
                        return d;
                        <%: this.ID %>_projectDocSource.read();
                    }
                },
            },
        pageSize: 10,
        batch: true,
        change: function (e) {
        }
        });

    var <%: this.ID %>_emPopupModel = new kendo.observable({
        emDtPRPlanDate: null,
        emDtRFQDate: null,
        emDtPOPlanDate: null,
        onUpdateClick: function () {
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            var data = grid.dataSource.data();
            for (var i = 0; i < data.length; i++) {
                if (checkedIdsMCC[data[i].id]) {
                    if (data[i].materialPlan.length > 0) {
                        data[i].materialPlan.prPlanDate = <%: this.ID %>_emPopupModel.emDtPRPlanDate;
                        data[i].materialPlan.prPlanBy = _currNTUserID;
                        data[i].materialPlan.rfqBy = _currNTUserID;
                        data[i].materialPlan.poPlanBy = _currNTUserID;
                        data[i].dirty = true;
                        data[i].setMaterialPlan = data[i].materialPlan;
                        data[i].updatedBy = _currNTUserID;
                    } else
                    {
                        data[i].materialPlan.push({
                            prPlanDate: <%: this.ID %>_emPopupModel.emDtPRPlanDate,
                            prPlanBy: _currNTUserID,
                            rfqBy: _currNTUserID,
                            poPlanBy: _currNTUserID,
                            createdBy: _currNTUserID,
                            updatedBy: _currNTUserID
                    });
                    data[i].dirty = true;
                    data[i].setMaterialPlan = data[i].materialPlan;
                    data[i].updatedBy = _currNTUserID;
                }
                }
            }
            grid.dataSource.sync();
            var MaterialsCoordWindow = $("#MaterialsCoordWindow").getKendoWindow();
            MaterialsCoordWindow.close();
        },
        onCancelClick: function () {
            $("#mccDtPRPlanDate").val(null);
            <%: this.ID %>_emPopupModel.emDtPRPlanDate = null;
            var MaterialsCoordWindow = $("#MaterialsCoordWindow").getKendoWindow();
            MaterialsCoordWindow.close();
        }
     });

    var <%: this.ID %>_viewModel = new kendo.observable({
            emDtPRPlanDate: null,
            emDtRFQDate: null,
            emDtPOPlanDate: null,
            documentById: "",
            documentByName : "",
            documentByEmail: "",
            EngMaterialSource: <%: this.ID %>_engMaterialSource,
            editProjectDoc: EditProjectDocument,
            onClick: <%: this.ID %>_clickUpdate,
            onDataBound: function (e) {
                var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
                var view = grid.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (checkedIdsMCC[view[i].id]) {
                        //this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        //.addClass("k-state-selected")
                        //.find(".checkbox")
                        //.attr("checked", "checked");
                    }
                }

            }
    });

    var checkedIdsMCC = [];

    function <%: this.ID %>_gridClick() {
        var checked = this.checked,
        row = $(this).closest("tr"),
        grid = $("#<%: this.ID %>_grdDocuments").data("kendoGrid"),
        dataItem = grid.dataItem(row);
        checkedIdsMCC[dataItem.id] = checked;
        if (checked) {
            //-select the row
            row.addClass("k-state-selected");
        } else {
            //-remove selection
            row.removeClass("k-state-selected");
        }
        $.each(checkedIdsMCC, function (key, value) {
            if (checkedIdsMCC[key] == true) {
                $("#btnEMCoordUpdate").removeClass("k-state-disabled");
                $("#btnEMCoordUpdate").removeAttr("disabled");
                $("#btnEMCoordUpdate").removeAttr("aria-disabled");
                return false;
            }
            $("#btnEMCoordUpdate").addClass("k-state-disabled");
            $("#btnBMLUpdate").attr("disabled", "disabled");
            $("#btnBMLUpdate").attr("aria-disabled", true);
            $('.checkboxHEngMaterial')[0].checked = false;
        })
        $("#btnEMCoordUpdate").kendoButton({
            enable: true,
            click: <%: this.ID %>_clickUpdate
        });
    }

    function <%: this.ID %>_gridHClick() {
        var cbh = this;
        $('.checkboxEngMaterial').each(function (idx, item) {
        var checked = this.checked,
        row = $(item).closest("tr"),
        grid = $("#<%: this.ID %>_grdDocuments").data("kendoGrid"),
        dataItem = grid.dataItem(row);
            if (cbh.checked==true) {
                //-select the row
                item.checked = true;
                row.addClass("k-state-selected");
                $("#btnEMCoordUpdate").removeClass("k-state-disabled");
                $("#btnEMCoordUpdate").removeAttr("disabled");
                $("#btnEMCoordUpdate").removeAttr("aria-disabled");
            } else {
                //-remove selection
                item.checked = false;
                row.removeClass("k-state-selected");
                $("#btnEMCoordUpdate").addClass("k-state-disabled");
                $("#btnBMLUpdate").attr("disabled", "disabled");
                $("#btnBMLUpdate").attr("aria-disabled", true);
            }
            checkedIdsMCC[dataItem.id] = item.checked;
        })

        $("#btnEMCoordUpdate").kendoButton({
            enable: true,
            click: <%: this.ID %>_clickUpdate
        });
    }

    function <%: this.ID %>_clickUpdate() {
        kendo.bind($("#MaterialsCoordWindow"), <%: this.ID %>_emPopupModel);
        var MaterialsCoordWindow = $("#MaterialsCoordWindow").getKendoWindow();
        MaterialsCoordWindow.center().open();
    }

    function EngineerChange(e) {
        e.data.documentById = e.sender.dataSource.data()[e.sender.selectedIndex].employeeId;
        e.data.documentByName = e.sender.dataSource.data()[e.sender.selectedIndex].employeeName;
        e.data.documentByEmail = e.sender.dataSource.data()[e.sender.selectedIndex].employeeEmail;
        e.sender.dataSource._data[e.sender.selectedIndex].dirty = true;
        <%: this.ID %>_viewModel.set("documentById", e.data.documentById);
        <%: this.ID %>_viewModel.set("documentByName", e.data.documentByName);
        <%: this.ID %>_viewModel.set("documentByEmail", e.data.documentByEmail);
    }

    function EditProjectDocument(e) {
        if (e.model.isNew()) {
            $("#dtActualStartDateContainer").hide();
            $("#dtActualFinishDateContainer").hide();
            $("#ucEngineeringContainer").hide();
            $("#rdExecutorContainer").hide();
            $(".k-window-title").html("Add")
        } else {
            $("#dtActualStartDateContainer").show();
            $("#dtActualFinishDateContainer").show();
            $("#ucEngineeringContainer").show();
            $("#rdExecutorContainer").show();
            $(".k-window-title").html("Edit")
        }

    }


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel); 
        var grid = $("#<%: this.ID %>_grdDocuments").data("kendoGrid");
        grid.table.on("click", ".checkboxEngMaterial", <%: this.ID %>_gridClick);
        $('#header-chb').click(<%: this.ID %>_gridHClick);
        $("#lastUpdateText").html("<div style='color: orange'>Last Update: " + (!lastUpdateMaterial(_projectId) ? "-" : lastUpdateMaterial(_projectId) != null ? lastUpdateMaterial(_projectId) : "-") + "</div>");
    });

</script>