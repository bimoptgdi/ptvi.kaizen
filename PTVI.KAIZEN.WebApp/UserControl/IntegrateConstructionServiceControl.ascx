﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IntegrateConstructionServiceControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.IntegrateConstructionServiceControl" %>
<%@ Register Src="~/UserControl/ConstructionMonitoringReportControl.ascx" TagPrefix="uc1" TagName="ConstructionMonitoringReportViewControl" %>

<script>
    var <%: this.ID %>_popConstructionMonitoringReport;
    var <%: this.ID %>_viewIntegrateConstructionServiceModel = kendo.observable({
        dsIntegrateConstructionService: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListMainProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListMainProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        rbd: { type: "date" },
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        estimationDays: { type: "number" },
                        actualDays: { type: "number" },
                        estimationCost: { type: "number" },
                        actualCost: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "ConstructionServices/csByProjectNo/" + _projectId,
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "ewpNo", dir: "asc" }]
        }),
        openConstructionMonitoringReportControl: function (e) {
            ConstructionMonitoringReportView_tenderId = e.data.tenderId;
            kendo.bind($("#ConstructionMonitoringReportView_ConstructionMonitoringReportContent"), ConstructionMonitoringReportView_viewConstructionMonitoringReportModel);
            ConstructionMonitoringReportView_viewConstructionMonitoringReportModel.dsTender();

            <%: this.ID %>_popConstructionMonitoringReport.center().open();

        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_MainIntegrateConstructionServiceControl"), <%: this.ID %>_viewIntegrateConstructionServiceModel);
        <%: this.ID %>_viewIntegrateConstructionServiceModel.dsIntegrateConstructionService.read();

        <%: this.ID %>_popConstructionMonitoringReport = $("#<%: this.ID %>_popConstructionMonitoringReport").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 850,
            height: 500
        }).data("kendoWindow");

    });

</script>

<div id="<%: this.ID %>_MainIntegrateConstructionServiceControl">
    <div id="<%: this.ID %>_popConstructionMonitoringReport">
        <uc1:ConstructionMonitoringReportViewControl runat="server" ID="ConstructionMonitoringReportView" />
    </div>
    <div class="headerSubProject">Construction Service With EWP</div>
    <div id="<%: this.ID %>_grdIntegrateConstructionServiceControl" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-filterable="true"
        data-columns="[
            {
                field: 'ewpNo', title: 'EWP No',
                template: kendo.template($('#ewpIntegrateConstructionService-template').html()),
                width: 110
            },
            {
                field: 'projectManager', title: 'Project Manager',
                width: 110
            },
            {
                field: 'engineer', title: 'Engineer',
                width: 110
            },
            {
                field: 'pmoNo', title: 'PMO No',
                width: 110
            },
            {
                field: 'rbd', title: 'RBD',
                <%--format: '{0:dd MMM yyyy}' ,--%>
                template: '#:rbd?kendo.toString(rbd, \'dd MMM yyyy\'):\'-\'#',
                width: 110
            },
            {
                field: 'status', title: 'Status',
                width: 110
            },
            {
                title: 'Plan Date',
                template: kendo.template($('#planDateIntegrateConstructionService-template').html()),
                width: 130
            },
            {
                field: 'estimationCost', title: 'Cost',
                template: kendo.template($('#estimationCostIntegrateConstructionService-template').html()),
                width: 110
            },
            {
                field: 'executor', title: 'Executor',
                template: kendo.template($('#<%: this.ID %>_executorName-template').html()), 
                width: 110
            },
            {
                title: ' ',
                template: '#if (trafficStatus) { #<img class=\'logo\' src=\'images/#:common_getTrafficImageUrl(trafficStatus)#\' style=\'padding-top: 0px;\'>#}#',
                width: 50
            },
        ]"
        data-bind="source: dsIntegrateConstructionService"
        data-pageable="{ buttonCount: 5 }">
    </div><br />

    <script>
    var <%: this.ID %>_popConstructionMonitoringReport;
    var <%: this.ID %>_viewConstructionServiceWithOutEwpModel = kendo.observable({
        dsConstructionService: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdConstructionServiceWithOutEwp"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdConstructionServiceWithOutEwp"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        rbd: { type: "date" },
                        planStartDate: { type: "date" },
                        planFinishDate: { type: "date" },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        estimationDays: { type: "number" },
                        actualDays: { type: "number" },
                        estimationCost: { type: "number" },
                        actualCost: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "ConstructionServices/cswithOutEwp/" + _projectId,
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "ewpNo", dir: "asc" }]
        }),
        openConstructionMonitoringReportControl: function (e) {
            ConstructionMonitoringReportView_tenderId = e.data.tenderId;
            kendo.bind($("#ConstructionMonitoringReportView_ConstructionMonitoringReportContent"), ConstructionMonitoringReportView_viewConstructionMonitoringReportModel);
            ConstructionMonitoringReportView_viewConstructionMonitoringReportModel.dsTender();

            <%: this.ID %>_popConstructionMonitoringReport.center().open();
        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ConstructionServiceWithOutEwp"), <%: this.ID %>_viewConstructionServiceWithOutEwpModel);
        <%: this.ID %>_viewConstructionServiceWithOutEwpModel.dsConstructionService.read();

        <%: this.ID %>_popConstructionMonitoringReport = $("#<%: this.ID %>_popConstructionMonitoringReport").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 850,
            height: 500
        }).data("kendoWindow");

    });

</script>


<div id="<%: this.ID %>_ConstructionServiceWithOutEwp">
    <div class="headerSubProject">Construction Service Without EWP</div>
    <div id="<%: this.ID %>_grdConstructionServiceWithOutEwp" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-filterable="true"
        data-columns="[
            {
                 field: 'ewpNo', title: 'EWP No',
                template: kendo.template($('#ewpIntegrateConstructionService-template').html()),
                width: 110
            },
            {
                field: 'projectManager', title: 'Project Manager',
                width: 110
            },
            {
                field: 'engineer', title: 'Engineer',
                width: 110
            },
            {
                field: 'pmoNo', title: 'PMO No',
                width: 110
            },
            {
                field: 'rbd', title: 'RBD',
                <%--format: '{0:dd MMM yyyy}' ,--%>
                template: '#:rbd?kendo.toString(rbd, \'dd MMM yyyy\'):\'-\'#',
                width: 110
            },
            {
                field: 'status', title: 'Status',
                width: 110
            },
            {
                title: 'Plan Date',
                template: kendo.template($('#planDateIntegrateConstructionService-template').html()),
                width: 130
            },
            {
                field: 'estimationCost', title: 'Cost',
                template: kendo.template($('#estimationCostIntegrateConstructionService-template').html()),
                width: 110
            },
            {
                field: 'executor', title: 'Executor',
                template: kendo.template($('#<%: this.ID %>_executor-template').html()), 
                width: 110
            },
            {
                title: ' ',
                template: '#if (trafficStatus) { #<img class=\'logo\' src=\'images/#:common_getTrafficImageUrl(trafficStatus)#\' style=\'padding-top: 0px;\'>#}#',
                width: 50
            },
        ]"
        data-bind="source: dsConstructionService"
        data-pageable="{ buttonCount: 5 }">
    </div>
</div>

  <script type="text/x-kendo-template" id="ewpIntegrateConstructionService-template">
        <div>
            No: #:ewpNo?ewpNo:"-"# </br>
               #:ewpTitle?ewpTitle:"-"#
        </div>
    </script>
   <script type="text/x-kendo-template" id="<%: this.ID %>_executorName-template">
        <div>
            # if (data.executor) {#
                # if (data.tenderId) { #
                    <a href="\#" data-bind="events: { click: openConstructionMonitoringReportControl }">#: data.executor.name #</a>
                # } else { #
                    #: data.executor.name #
                # } #
            # } else { #
                -
            # } #
        </div>
</script>
    <script type="text/x-kendo-template" id="planDateIntegrateConstructionService-template">
        <div>
            Start: #:planStartDate?kendo.toString(planStartDate, 'dd MMM yyyy'):"-"# </br>
            Estimation: #:estimationDays?estimationDays:"-"# days</br>
            Finish: #:planFinishDate?kendo.toString(planFinishDate, 'dd MMM yyyy'):"-"#
        </div>
    </script>
    <script type="text/x-kendo-template" id="estimationCostIntegrateConstructionService-template">
        <div>
            Est.: #:estimationCost?kendo.toString(estimationCost, "n2"):"-"# </br>
            Act.: #:actualCost?kendo.toString(actualCost, "n2"):"-"# 
        </div>
    </script>

    
<script type="text/x-kendo-template" id="<%: this.ID %>_executor-template">
        <div>
            # if (data.executor) {#
                # if (data.tenderId) { #
                    <a href="\#" data-bind="events: { click: openConstructionMonitoringReportControl }">#: data.executor.name #</a>
                # } else { #
                    #: data.executor.name #
                # } #
            # } else { #
                -
            # } #
        </div>
</script>

 <script type="text/x-kendo-template" id="ewpIntegrateConstructionService-template">
        <div>
            No: #:ewpNo?ewpNo:"-"# </br>
                #:ewpTitle?ewpTitle:"-"#
        </div>
    </script>

<script type="text/x-kendo-template" id="planDateIntegrateConstructionService-template">
        <div>
            Start: #:planStartDate?kendo.toString(planStartDate, 'dd MMM yyyy'):"-"# </br>
            Estimation: #:estimationDays?estimationDays:"-"# days</br>
            Finish: #:planFinishDate?kendo.toString(planFinishDate, 'dd MMM yyyy'):"-"#
        </div>
    </script>
    <script type="text/x-kendo-template" id="estimationCostIntegrateConstructionService-template">
        <div>
            Est.: #:estimationCost?kendo.toString(estimationCost, "n2"):"-"# </br>
            Act.: #:actualCost?kendo.toString(actualCost, "n2"):"-"# 
        </div>
    </script>
</div>
