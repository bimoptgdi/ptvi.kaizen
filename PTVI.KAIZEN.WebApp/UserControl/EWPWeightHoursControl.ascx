﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EWPWeightHoursControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EWPWeightHoursControl" %>
<script>
    var <%: this.ID %>_docId = "0";
    var <%: this.ID %>_docType = "";
    var <%: this.ID %>_localData = [];

    function getIndexByMasterWeightHoursId(id) {
        var l = <%: this.ID %>_localData.length;

        for (var j = 0; j < l; j++) {
            if (<%: this.ID %>_localData[j].masterWeightHoursId == id) {
                return j;
            }
        }
        return null;
    }

    var <%: this.ID %>_viewWeightHoursModel = new kendo.observable({
        no: "",
        title: "",
        visibleTitle: false,
        dsTreeListWeightListSource: new kendo.data.TreeListDataSource({
            autoSync: true,
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdWeightLIst"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdWeightLIst"), false);
                if (e.type == "update")
                {
                    <%: this.ID %>_viewWeightHoursModel.otherUpdate();
                }
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "masterWeightHoursId",
                    parentId: "parentId",
                    fields: {
                        //masterWeightHoursDesc: { editable: false },
                        estimateHours: { editable: false },
                        actualHours: { type: "number" }
                    },
                    expanded: true
                }
            },
            transport: {
                <%--read: {
                    async: false,
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "ProjectDocumentWeightHours/getByDocIdWithMasterWeight/" + <%: this.ID %>_docId + "?engineerType=" + <%: this.ID %>_docType;
                    },
                    dataType: "json"
                },
                update: {
                    async: false,
                    url: function (data) {
                        return "<%: WebApiUrl %>" + "ProjectDocumentWeightHours/updateByMasterWeightHoursIdId/" + data.masterWeightHoursId;
                    },
                    type: "PUT"
                }--%>
                read: function (options) {
                    if (<%: this.ID %>_localData.length == 0) {
                       $.ajax({
                            async: false,
                            type: 'GET',
                            url: "<%: WebApiUrl %>" + "ProjectDocumentWeightHours/getByDocIdWithMasterWeight/" + <%: this.ID %>_docId + "?engineerType=" + <%: this.ID %>_docType,
                            success: function(result) {
                                options.success(result);
                                <%: this.ID %>_localData = result;
                            },
                            error: function(result) {
                                options.error(result);
                            }
                        });
                    } else {
                        options.success(<%: this.ID %>_localData);
                    }
                },
                update: function (options) {
                    if (<%: this.ID %>_docId == 0) {
                        <%: this.ID %>_localData[getIndexByMasterWeightHoursId(options.data.masterWeightHoursId)] = options.data;
                        // on success
                        options.success();
                    } else {
                        $.ajax({
                            async: false,
                            type: 'PUT',
                            url: "<%: WebApiUrl %>" + "ProjectDocumentWeightHours/updateByMasterWeightHoursIdId/" + options.data.masterWeightHoursId,
                            //dataType: "jsonp",
                            data: JSON.parse(JSON.stringify(options.data)),
                            success: function (result) {
                                options.success(result);
                            },
                            error: function (result) {
                                options.error(result);
                            }
                        });
                    }
                }
            }
        }),
        onEdit: function (e) {
            if (($(e.sender.current().find("input")[0]).attr("name") == 'masterWeightHoursDesc' && !e.model.isEditable) || e.model.isParentGroup)
            {
                $("#<%: this.ID %>_grdWeightLIst").data("kendoTreeList").closeCell();
            }

            //var treeList = $("#<%: this.ID %>_grdWeightLIst").data("kendoTreeList").closeCell();
            //if (!e.model.isNew()) {
            //    // Disable the editor of the "id" column when editing data items
            //    var numeric = e.container.find("input[name=masterWeightHoursDesc]").    data("kendoNumericTextBox");
            //    numeric.enable(false);
            //}
        },
        onSave: function (e) {

<%--            e.model.actualHours = e.values.actualHours;
            e.model.masterWeightHoursDesc = e.values.masterWeightHoursDesc;
            <%: this.ID %>_viewWeightHoursModel.dsTreeListWeightListSource.sync();--%>
            //this.otherUpdate();
        },
        otherUpdate: function (e) { }
    });

    function <%: this.ID %>_actualHoursEditTemplate(container, options) {
        $('<input name="actualHours" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                spinners : false
            });
    }

    function <%: this.ID %>_bind() {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewWeightHoursModel); 

    }

    $(document).ready(function () {
    });

</script>

<div id="<%: this.ID %>_FormContent">
    <div>
        <div style="display: none;" data-bind="visible: visibleTitle">
            <table>
                <tr style="font-size: 17px; font-weight: bold;">
                    <td>No</td>
                    <td>: <span data-bind="text: no"></span></td>
                </tr>
                <tr style="font-size: 17px; font-weight: bold;">
                    <td>Title</td>
                    <td>: <span data-bind="text: title"></span></td>
                </tr>
            </table>
        </div>
        <div id="<%: this.ID %>_grdWeightLIst" 
            data-role="treelist"
    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours))
    { %>
            data-editable= "incell"
    <% } else { %>
            data-editable= "false"
    <% } %>
            data-selectable="true"
            data-bind="source: dsTreeListWeightListSource, events: { edit: onEdit, save: onSave }"
            data-columns="[
                        { field: 'masterWeightHoursDesc', title: 'Description', width: 300 },
                        { 
                            field: 'estimateHours', title: 'Estimation', width: 80, 
                            template: kendo.template($('#<%: this.ID %>_estimateHours-template').html()),
                            attributes: { style: 'text-align: right; vertical-align: top !important;' }
                        },
                        { 
                            field: 'actualHours', title: 'Hour(s)', width: 80, 
                            template: kendo.template($('#<%: this.ID %>_actualHours-template').html()),
                            editor: <%: this.ID %>_actualHoursEditTemplate,
                            attributes: { style: 'text-align: right; vertical-align: top !important;' }
                        }
                        ]"></div>
    </div>
</div>
<script id="<%: this.ID %>_estimateHours-template" type="text/x-kendo-template">
    # if (!data.isParentGroup) { #
        <div>#= data.estimateHours ? data.estimateHours : "-" #</div>
    # } #
</script>
<script id="<%: this.ID %>_actualHours-template" type="text/x-kendo-template">
    # if (!data.isParentGroup) { #
        <div>#= data.actualHours ? data.actualHours : 0 #</div>
    # } #
</script>

