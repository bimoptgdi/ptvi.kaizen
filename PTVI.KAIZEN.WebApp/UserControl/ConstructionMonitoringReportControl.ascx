﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConstructionMonitoringReportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ConstructionMonitoringReportControl" %>
<%@ Register Src="~/UserControl/PickMaterialControl.ascx" TagPrefix="uc1" TagName="PickMaterialComponentControl" %>

<script>
    common_getAllStatusIprom();
    var <%: this.ID %>_tenderId = "", <%: this.ID %>_popMaterialComponent;
    var <%: this.ID %>_viewConstructionMonitoringReportModel = kendo.observable({
        contractNo: ": -",
        vendorSelectedName: ": -",
        docNoSummary: ": -",
        dsTender: function () {
            kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), true);
            var that = this;
            $.ajax({
                url: _webApiUrl + "Tenders/ConstructionMonitoringReportListByTenderId/" +<%: this.ID %>_tenderId,
                type: "GET",
                success: function (data) {
                    that.set("contractNo", ": " + (data.contractNo ? data.contractNo : "-"));
                    that.set("pmoNo", ": " + (data.pmoNo ? data.pmoNo : "-"));

                    if (data.contractNo)
                        $("#<%: this.ID %>_divContractNo").show();
                    else
                        $("#<%: this.ID %>_divContractNo").hide();

                    if (data.pmoNo)
                        $("#<%: this.ID %>_divPmoNo").show();
                    else
                        $("#<%: this.ID %>_divPmoNo").hide();

                    that.set("vendorSelectedName", ": " + (data.vendorSelectedName ? data.vendorSelectedName : "-"));
                    that.set("docNoSummary", ": " + (data.docNoSummary ? data.docNoSummary : "-"));
                    kendo.ui.progress($("#<%: this.ID %>_ContractHead"), false);
                },
                error: function (jqXHR) {
                    kendo.ui.progress($("#<%: this.ID %>_ContractHead"), false);
                }
            });
        },
        dsStatusSource: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "lookup/findbytype/" + _statusIprom
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        }),
        dsListConstructionMonitoringReportManPower: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderManPowers/TenderManPowersByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                },
                create: {
                    url: function (data) {
                        return _webApiUrl + "TenderManPowers/TenderManPowerSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "TenderManPowers/TenderManPowerSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "TenderManPowers/" + data.id
                    },
                    type: "DELETE"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        editManPower: function (e) {
            if (e.model.isNew()) {
                $(".k-window-title").html("Add Man Power");
                $(".k-grid-update").html("<span class='k-icon k-i-check'></span>Save");
            }
            else
                $(".k-window-title").html("Edit Man Power");

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        dsListConstructionMonitoringReportEquipment: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportEquipment"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportEquipment"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderEquipments/TenderEquipmentsByTenderId/" + <%: this.ID %>_tenderId;
                    },
                    dataType: "json"
                },
                create: {
                    url: function (data) {
                        return _webApiUrl + "TenderEquipments/TenderEquipmentSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "TenderEquipments/TenderEquipmentSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "TenderEquipments/" + data.id
                    },
                    type: "DELETE"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        editEquipment: function (e) {
            if (e.model.isNew()) {
                $(".k-window-title").html("Add Equipment");
                $(".k-grid-update").html("<span class='k-icon k-i-check'></span>Save");
            }
            else
                $(".k-window-title").html("Edit Equipment");

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        dsListConstructionMonitoringReportMaterial: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportMaterial"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportMaterial"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        handover: { defaultValue: true, type: "boolean" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderMaterials/TenderMaterialsByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                },
                create: {
                    url: function (data) {
                        return _webApiUrl + "TenderMaterials/TenderMaterialSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "TenderMaterials/TenderMaterialSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "TenderMaterials/" + data.id
                    },
                    type: "DELETE"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        editMaterial: function (e) {
            if (e.model.isNew()) {
                $(".k-window-title").html("Add Material");
                $(".k-grid-update").html("<span class='k-icon k-i-check'></span>Save");
            }
            else
                $(".k-window-title").html("Edit Material");

            $("#<%: this.ID %>_btnTenderMaterial").on("click", function () {
                PickMaterialComponent_pickMaterialViewModel.set("projectId", _projectId);
                PickMaterialComponent_pickMaterialViewModel.set("pickHandlerMaterial", function (e) {
                    var pickedMaterial = (this.dataItem($(e.currentTarget).closest("tr")));

                    $("#<%: this.ID %>_TenderMaterialId").val(pickedMaterial.id);
                    $("#<%: this.ID %>_TenderMaterialMrNo").val(pickedMaterial.mrNo ? pickedMaterial.mrNo : '-');
                    $("#<%: this.ID %>_TenderMaterialPoNo").val(pickedMaterial.poNo ? pickedMaterial.poNo + '/' + pickedMaterial.poItemNo : '-');
                    $("#<%: this.ID %>_TenderMaterialReservation").val(pickedMaterial.reservation && pickedMaterial.reservation != '0' ? pickedMaterial.reservation + '/' + pickedMaterial.reservationItemNo : '-');
                    $("#<%: this.ID %>_TenderMaterialMaterialDesc").val(pickedMaterial.materialDesc);

                    <%: this.ID %>_popMaterialComponent.close();
                });
                PickMaterialComponent_pickMaterialViewModel.set("onCloseSelectEvent", function (data) {
                    <%: this.ID %>_popMaterialComponent.close();
                });
                // show popup
                <%: this.ID %>_popMaterialComponent.center().open();
                PickMaterialComponent_DataBind();
            });

            //$("input[name='handover']").kendoMobileSwitch({
            //    onLabel: "Yes",
            //    offLabel: "No"
            //});

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        saveMaterial: function (e) {
            e.model.set("materialComponentId", $("#<%: this.ID %>_TenderMaterialId").val());

            var isValid = true;
            if (!e.model.get("materialComponentId")) {
                _showDialogMessage("Vendor", "Please select the Material!", "");
                isValid = false;
            }

            if (!isValid)
                e.preventDefault();
        },
        dsListConstructionMonitoringReportInstallation: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportInstallation"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportInstallation"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderInstallations/TenderInstallationsByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                },
                create: {
                    url: function (data) {
                        return _webApiUrl + "TenderInstallations/TenderInstallationSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "TenderInstallations/TenderInstallationSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "TenderInstallations/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        data.planStartDate = kendo.toString(data.planStartDate, 'yyyy-MM-dd');
                        data.planFinishDate = kendo.toString(data.planFinishDate, 'yyyy-MM-dd');
                        data.actualStartDate = kendo.toString(data.actualStartDate, 'yyyy-MM-dd');
                        data.actualFinishDate = kendo.toString(data.actualFinishDate, 'yyyy-MM-dd');
                    }
                    return data;
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        editInstallation: function (e) {
            if (e.model.isNew()) {
                $(".k-window-title").html("Add Installation");
                $(".k-grid-update").html("<span class='k-icon k-i-check'></span>Save");
            }
            else
                $(".k-window-title").html("Edit Installation");

            if (_planStartDate) {
                $("#<%: this.ID %>_planStartDateInstallation").getKendoDatePicker().min(_planStartDate);
                $("#<%: this.ID %>_planFinishDateInstallation").getKendoDatePicker().min(_planStartDate);
            }

            if (_planFinishDate) {
                $("#<%: this.ID %>_planStartDateInstallation").getKendoDatePicker().max(_planFinishDate);
                $("#<%: this.ID %>_planFinishDateInstallation").getKendoDatePicker().max(_planFinishDate);
            }

            if (e.model.planFinishDate)
                $("#<%: this.ID %>_planStartDateInstallation").getKendoDatePicker().max(e.model.planFinishDate);
            if (e.model.planStartDate)
                $("#<%: this.ID %>_planFinishDateInstallation").getKendoDatePicker().min(e.model.planStartDate);
            if (e.model.actualFinishDate)
                $("#<%: this.ID %>_actualStartDateInstallation").getKendoDatePicker().max(e.model.actualFinishDate);
            if (e.model.actualStartDate)
                $("#<%: this.ID %>_actualFinishDateInstallation").getKendoDatePicker().min(e.model.actualStartDate);

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        dsListConstructionMonitoringReportPunchlist: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportPunchlist"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportPunchlist"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderPunchlists/TenderPunchlistsByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                },
                create: {
                    url: function (data) {
                        return _webApiUrl + "TenderPunchlists/TenderPunchlistSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                update: {
                    url: function (data) {
                        return _webApiUrl + "TenderPunchlists/TenderPunchlistSubmit/" + <%: this.ID %>_tenderId;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "TenderPunchlists/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        data.planStartDate = kendo.toString(data.planStartDate, 'yyyy-MM-dd');
                        data.planFinishDate = kendo.toString(data.planFinishDate, 'yyyy-MM-dd');
                        data.actualStartDate = kendo.toString(data.actualStartDate, 'yyyy-MM-dd');
                        data.actualFinishDate = kendo.toString(data.actualFinishDate, 'yyyy-MM-dd');
                    }
                    return data;
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        editPunchlist: function (e) {
            if (e.model.isNew()) {
                $(".k-window-title").html("Add Punchlist");
                $(".k-grid-update").html("<span class='k-icon k-i-check'></span>Save");
            }
            else
                $(".k-window-title").html("Edit Punchlist");

            if (_planStartDate) {
                $("#<%: this.ID %>_planStartDatePunchlist").getKendoDatePicker().min(_planStartDate);
                $("#<%: this.ID %>_planFinishDatePunchlist").getKendoDatePicker().min(_planStartDate);
            }

            if (_planFinishDate) {
                $("#<%: this.ID %>_planStartDatePunchlist").getKendoDatePicker().max(_planFinishDate);
                $("#<%: this.ID %>_planFinishDatePunchlist").getKendoDatePicker().max(_planFinishDate);
            }

            if (e.model.planFinishDate)
                $("#<%: this.ID %>_planStartDatePunchlist").getKendoDatePicker().max(e.model.planFinishDate);
            if (e.model.planStartDate)
                $("#<%: this.ID %>_planFinishDatePunchlist").getKendoDatePicker().min(e.model.planStartDate);
            if (e.model.actualFinishDate)
                $("#<%: this.ID %>_actualStartDatePunchlist").getKendoDatePicker().max(e.model.actualFinishDate);
            if (e.model.actualStartDate)
                $("#<%: this.ID %>_actualFinishDatePunchlist").getKendoDatePicker().min(e.model.actualStartDate);

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        refreshGrid: function (e) {
             <%: this.ID %>_viewConstructionMonitoringReportModel.dsListConstructionMonitoringReportManPower.read();
             <%: this.ID %>_viewConstructionMonitoringReportModel.dsListConstructionMonitoringReportEquipment.read();
             <%: this.ID %>_viewConstructionMonitoringReportModel.dsListConstructionMonitoringReportMaterial.read();
             <%: this.ID %>_viewConstructionMonitoringReportModel.dsListConstructionMonitoringReportInstallation.read();
             <%: this.ID %>_viewConstructionMonitoringReportModel.dsListConstructionMonitoringReportPunchlist.read();

        }
    });

    function onTenderMaterialHandoverActiveChange(e) {
        if ($(".k-edit-form-container").length > 0)
            return 0;

        var active = e.checked;
        if (confirm('Are you sure to ' + (active ? 'handover' : 'takeover') + ' this material?')) {
            var grid = $("#<%: this.ID %>_grdListConstructionMonitoringReportMaterial").getKendoGrid();
            aaa = e;
            var item = grid.dataItem($(e.sender.element.parent().parent().parent().parent()));
            item.set('handover', active);

            $("#<%: this.ID %>_grdListConstructionMonitoringReportMaterial").getKendoGrid().dataSource.sync();
        }
        else {
            e.sender.value(!active); // undo
        }
    };
    $(document).ready(function () {
        <%: this.ID %>_popMaterialComponent = $("#<%: this.ID %>_popMaterialComponent").kendoWindow({
            title: "Sponsor List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        $("#cpLegend").kendoTooltip({
            filter: ".tooltip_wrapper",
            position: "right",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "<div class='popup-content'>"
                        + "<div class='popup-title'>Hint</div>"
                        + "<hr class='hrStyle' />"
                        + "<div>Commissioning, Fabrication, etc"
                        + "</div>";
                        + "</div>"

                return content;
            }
        }).data("kendoTooltip");
    });

</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_tenderPlan-template">
    <div>
        <div class='subTitleTemplate'>Start:</div>
        <div>#:data.planStartDate ? kendo.toString(data.planStartDate,'dd MMM yyyy') : "-"#</div>
        <div class='subTitleTemplate'>Finish:</div>
        <div>#:data.planFinishDate ? kendo.toString(data.planFinishDate,'dd MMM yyyy') : "-"#</div>
    </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_tenderActual-template">
    <div>
        <div class='subTitleTemplate'>Start:</div>
        <div>#:data.actualStartDate ? kendo.toString(data.actualStartDate,'dd MMM yyyy') : "-"#</div>
        <div class='subTitleTemplate'>Finish:</div>
        <div>#:data.actualFinishDate ? kendo.toString(data.actualFinishDate,'dd MMM yyyy') : "-"#</div>
    </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_completePercentage-template">
    <div>
        <div class='subTitleTemplate'>Percentage:</div>
        <div>#: data.completePercentage ? data.completePercentage + ' %' : '-'#</div>
        <div class='subTitleTemplate'>Status:</div>
        <div>#:data.issueStatus ? common_getStatusIpromDesc(data.issueStatus) : '-'#</div>
    </div>
</script>

<div id="<%: this.ID %>_ConstructionMonitoringReportContent">
    <div id="<%: this.ID %>_popMaterialComponent">
        <uc1:PickMaterialComponentControl runat="server" id="PickMaterialComponent" />
    </div>
    <div id="<%: this.ID %>_ContractHead">
        <div id="<%: this.ID %>_divContractNo">
            <label class="labelForm">Contract No</label>
            <span data-bind="text: contractNo"></span>
        </div>
        <div id="<%: this.ID %>_divPmoNo">
            <label class="labelForm">PMO No</label>
            <span data-bind="text: pmoNo"></span>
        </div>
<%--        <div>
            <label class="labelForm">Contractor Name</label>
            <span data-bind="text: vendorSelectedName"></span>
        </div>--%>
        <div>
            <label class="labelForm">EWP No</label>
            <span data-bind="text: docNoSummary"></span>
        </div>
    </div>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Man Power</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportManPower" 
                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                { %>data-toolbar='[{name:"create", text:"Add"}]'<% } %>
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        field: 'position', title: 'Position',
                        width: 110
                    },
                    {
                        field: 'quantity', title: 'Qty',
                        width: 40
                    },
                    {
                        field: 'qualification', title: 'Qualification',
                        width: 170
                    },
                    {
                        field: 'license', title: 'License',
                        width: 110
                    },
                    {
                        title: 'Status',
                        template: '#:common_getStatusIpromDesc(status)#',
                        width: 75
                    }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>,
                    {
                        field: 'command', 
                        command: ['edit', 'destroy'],
                        width: 150
                    }<% } %>
                ]"
                data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editGrdManPower-editTemplate").html())}'
                data-bind="source: dsListConstructionMonitoringReportManPower, events: { edit: editManPower }"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdManPower-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Position<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Position is required" placeholder="Please Input Position" name="position" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Quantity<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="number" required validationMessage="Quantity is required" placeholder="Please Input Quantity" name="quantity" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Qualification<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Qualification is required" placeholder="Please Input Qualification" name="qualification" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">License</td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" placeholder="Please Input License" name="license" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Equipment</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportEquipment" 
                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                { %>data-toolbar='[{name:"create", text:"Add"}]'<% } %>
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        field: 'equipment', title: 'Equipment',
                        width: 300
                    },
                    {
                        field: 'quantity', title: 'Qty',
                        width: 40
                    },
                    {
                        title: 'Status',
                        template: '#:common_getStatusIpromDesc(status)#',
                        width: 75
                    }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>,
                    {
                        field: 'command', 
                        command: ['edit', 'destroy'],
                        width: 150
                    }<% } %>
                ]"
                data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editGrdEquipment-editTemplate").html())}'
                data-bind="source: dsListConstructionMonitoringReportEquipment, events: { edit: editEquipment }"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdEquipment-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Equipment<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Equipment is required" placeholder="Please Input Equipment" name="equipment" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Quantity<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="number" required validationMessage="Quantity is required" placeholder="Please Input Quantity" name="quantity" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Material</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportMaterial" 
                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                { %>data-toolbar='[{name:"create", text:"Add"}]'<% } %>
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        title: 'No',
                        template: kendo.template($('#<%: this.ID %>_tenderMaterialNo-template').html()),
                        width: 100
                    },
                    {
                        title: 'Material',
                        template: '#: data.materialDesc ? data.materialDesc + \' %\' : \'-\'#',
                        width: 165
                    },
                    {
                        title: 'Qty',
                        template: kendo.template($('#<%: this.ID %>_tenderMaterialQty-template').html()),
                        width: 100
                    },
                    {
                        title: 'Qty\'s Location', hidden: true,
                        template: '#:data.remainingLocation#',
                        width: 110
                    },
                    {
                        title: 'Status',
                        template: kendo.template($('#<%: this.ID %>_tenderMaterialHandover-template').html()),
                        width: 80
                    }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>,
                    {
                        field: 'command', 
                        command: ['edit', 'destroy'],
                        width: 150
                    }<% } %>
                ]"
                data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editGrdMaterial-editTemplate").html())}'
                data-bind="source: dsListConstructionMonitoringReportMaterial, events: { edit: editMaterial, save: saveMaterial }"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>

    <script type="text/x-kendo-template" id="<%: this.ID %>_tenderMaterialNo-template">
        <div>
            # if (data.mrNo) {#
            <div class='subTitleTemplate'>MR No:</div>
            <div>#:data.mrNo#</div>
            #}#
            # if (data.poNo) {#
            <div class='subTitleTemplate'>PO No:</div>
            <div>#:data.poNo + '/' + data.poItemNo#</div>
            #}#
            # if (data.reservation && data.reservation != '0') {#
            <div class='subTitleTemplate'>Reservation:</div>
            <div>#:data.reservation + '/' + data.reservationItemNo#</div>
            #}#
        </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_tenderMaterialQty-template">
        <div>
            <div class='subTitleTemplate'>Quantity:</div>
            <div>#:data.usedQuantity ? data.usedQuantity : "-"#</div>
            <div class='subTitleTemplate'>Used:</div>
            <div>#:data.usedQuantity ? (data.remainingQuantity ? data.usedQuantity - data.remainingQuantity : data.usedQuantity) : "-"#</div>
            <div class='subTitleTemplate'>Remaining:</div>
            <div>#:data.remainingQuantity ? data.remainingQuantity : "-"#</div>
        </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_tenderMaterialHandover-template">
        <div>
            <div class='subTitleTemplate'>Status:</div>
            <div>#:data.status ? data.status : '-'#</div>
            <div class='subTitleTemplate'>Handover:</div>
            <input type="checkbox" disabled="disabled" #: handover ? "checked" : "" # /> Handover

            <!--input type="checkbox" data-role="switch" data-checked="#: handover ? true : false #" data-on-label="Yes" data-off-label="No" data-change="onTenderMaterialHandoverActiveChange" /-->
        </div>
    </script>

    <script id="<%: this.ID %>_editGrdMaterial-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Material<span style="color:red">*</span></td>
                    <td><input id="<%: this.ID %>_TenderMaterialId" name="id" class="k-textbox" style="display:none;" type="text" />
                        <input id="<%: this.ID %>_TenderMaterialMaterialDesc" style="width: 400px" name="materialDesc" class="k-textbox" type="text" disabled="disabled" />
                        <div
                            id="<%: this.ID %>_btnTenderMaterial" 
                            class="k-button">
                            ...
                        </div></td>
                </tr>   
                <tr>
                    <td style="width:100px">MR No</td>
                    <td>
                        <input id="<%: this.ID %>_TenderMaterialMrNo" style="width: 200px" name="mrNo" class="k-textbox" type="text" disabled="disabled" placeholder="Material" required validationMessage="Material is required" />
                   </td>
                </tr>
                <tr>
                    <td style="width:100px">PO No</td>
                    <td>
                        <input id="<%: this.ID %>_TenderMaterialPoNo" style="width: 200px" name="poNo" class="k-textbox" type="text" disabled="disabled" />
                   </td>
                </tr>
                <tr>
                    <td style="width:100px">Reservation No</td>
                    <td>
                        <input id="<%: this.ID %>_TenderMaterialReservation" style="width: 200px" name="reservation" class="k-textbox" type="text" disabled="disabled" />
                   </td>
                </tr>
                <tr>
                    <td style="width:100px">Handover</td>
                    <td>
                        <input type="checkbox" name="handover" />
                   </td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div id="cpLegend">
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Construction Process <span class='tooltip_wrapper'><i class='info-pop'>i</i></span></legend>
            
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportInstallation" 
                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                { %>data-toolbar='[{name:"create", text:"Add"}]'<% } %>
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        title: 'Task',
                        template: '#:data.task#',
                        width: 110
                    },
                    {
                        title: 'Weight',
                        template: '#: data.weightPercentage ? data.weightPercentage + \' %\' : \'-\'#',
                        width: 60
                    },
                    {
                        title: 'Plan Date',
                        template: kendo.template($('#<%: this.ID %>_tenderPlan-template').html()),
                        width: 80
                    },
                    {
                        title: 'Actual Date',
                        template: kendo.template($('#<%: this.ID %>_tenderActual-template').html()),
                        width: 80
                    },
                    {
                        title: 'Complete',
                        template: kendo.template($('#<%: this.ID %>_completePercentage-template').html()),
                        width: 70
                    },
                    {
                        title: 'Issue',
                        template: '#: data.issue ? data.issue : \'-\'#',
                        width: 130
                    }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>,
                    {
                        field: 'command', 
                        command: ['edit', 'destroy'],
                        width: 155
                    }<% } %>
                ]"
                data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editGrdInstallation-editTemplate").html())}'
                data-bind="source: dsListConstructionMonitoringReportInstallation, events: { edit: editInstallation }"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdInstallation-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Task<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Task is required" placeholder="Please Input Task" name="task" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Weight (%)</td>
                    <td><input class="k-textbox" type="number" placeholder="Please Input Weight" name="weightPercentage" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planStartDateInstallation" placeholder="Plan Start Date" name="planStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planFinishDateInstallation" placeholder="Plan Finish Date" name="planFinishDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualStartDateInstallation" placeholder="Actual Start Date" name="actualStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualFinishDateInstallation" placeholder="Actual Finish Date" name="actualFinishDate" onfocus/></td>
                </tr>
                <tr>
                    <td style="width:100px">Complete (%)<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="number" placeholder="Please Input Complete" name="completePercentage" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Issue</td>
                    <td>
                        <textarea name="issue" class="k-textbox" style="height: 60px; width: 100%;" maxlength="2000" ></textarea></td>
                </tr>
                <tr>
                    <td style="width:100px">Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="issueStatus" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Punchlist</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportPunchlist" 
                <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                { %>data-toolbar='[{name:"create", text:"Add"}]'<% } %>
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        field: 'punchlist', title: 'Punchlist',
                        width: 110
                    },
                    {
                        title: 'Plan Date',
                        template: kendo.template($('#<%: this.ID %>_tenderPlan-template').html()),
                        width: 80
                    },
                    {
                        title: 'Actual Date',
                        template: kendo.template($('#<%: this.ID %>_tenderActual-template').html()),
                        width: 80
                    },
                    {
                        title: 'Complete',
                        template: kendo.template($('#<%: this.ID %>_completePercentage-template').html()),
                        width: 70
                    },
                    {
                        title: 'Issue',
                        template: '#: data.issue ? data.issue : \'-\'#',
                        width: 140
                    }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ConstructionMonitoring))
                    { %>,
                    {
                        field: 'command', 
                        command: ['edit', 'destroy'],
                        width: 150
                    }<% } %>
                ]"
                data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editGrdPunchlist-editTemplate").html())}'
                data-bind="source: dsListConstructionMonitoringReportPunchlist, events: { edit: editPunchlist }"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdPunchlist-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Punchlist<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Punchlist is required" placeholder="Please Input Punchlist" name="punchlist" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planStartDatePunchlist" placeholder="Plan Start Date" name="planStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planFinishDatePunchlist" placeholder="Plan Finish Date" name="planFinishDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualStartDatePunchlist" placeholder="Actual Start Date" name="actualStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualFinishDatePunchlist" placeholder="Actual Finish Date" name="actualFinishDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Complete (%)</td>
                    <td><input class="k-textbox" type="number" placeholder="Please Input Complete" name="completePercentage" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Issue</td>
                    <td>
                        <textarea name="issue" class="k-textbox" style="height: 60px; width: 100%;" maxlength="2000" ></textarea></td>
                </tr>
                <tr>
                    <td style="width:100px">Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="issueStatus" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
</div>
    <style>
        .info-pop {
            position: relative;
            font-size: 1rem;
            width: 20px;
            height: 20px;
            display: inline-block;
            text-align: center;
            line-height: 20px;
            background-color:#808080;
            margin: 0 0 0 4px;
            cursor: pointer;
            color:#fff;
            padding-right:1px;
            padding-top:1px;
            box-sizing:padding-box;
            font-family:"Monotype Corsiva" !important;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            -webkit-transition: all 0.3s linear;
            -o-transition: all 0.3s linear;
            -moz-transition: all 0.3s linear;
            transition: all 0.3s linear;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

            .info-pop:hover {
                background-color: #137dbb;
                color: #fff;
                border:none
            }

        .k-tooltip {
            background: #808080 !important;
            border-color: #808080 !important;
        }

        .k-callout-w {
            border-right-color: #808080 !important;
        }

        .k-callout {
            position: absolute;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 6px;
            border-color: transparent;
            pointer-events: none;
        }

        .hrStyle {
            border: 0;
            height: 1px;
            background-image: -webkit-linear-gradient(left, #808080, #ffffff, #808080);
            background-image: -moz-linear-gradient(left, #808080, #ffffff, #808080);
            background-image: -ms-linear-gradient(left, #808080, #ffffff, #808080);
            background-image: -o-linear-gradient(left, #808080, #ffffff, #808080);
        }

        .popup-title {
            text-align: center;
            padding-top: 5px;
            font-weight: 700;
        }
        .tooltip_wrapper {
            display: inline-block;
            width: 30px;
        }
        .popup-content {
            width: 250px;
            text-align: left;
            padding-left: 10px;
            padding-right: 10px;
            background-color: #808080;
            padding-bottom: 10px;
        }
        .km-switch {
            margin-top: .5em !important;
            display: inherit !important;
        }
    </style>
