﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TemplateEWRDetailControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.TemplateEWRDetailControl" %>
<span style="font-family:Arial;font-size:16px;padding-top:5px;padding-bottom:5px">EWR Detail</span>   

<div id="<%: this.ID %>_Content" style="width:100%;font-family:Arial">
	<div style="width:100%;font-family:Arial">
	<table width="700px" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="7" align="left" height="30" class="company">PT.Vale Indonesia, Tbk</td>
		</tr>
		
		<tr>
			<td class="title" colspan="7" align="center">Minutes Of Meeting (<span data-bind="text: dsEWR.EWRMOM.MOMEWRNO"></span>)</td>
		</tr>
		
        <tr style="height:25px">
			<td colspan="2" width="20%" class="form">Participants</td>
			<td colspan="2" width="20%" class="form">Distribution</td>
			<td colspan="2" class="form">Date :</td>
			<!-- <td class="wborderB form" >:</td> -->
			<td width="50%"><b><span data-bind="text: momDate"></span></b></td>
		</tr>
		<tr style="height:25px">
			<td rowspan="3" colspan="2"><span data-bind="text: participants"></span></td>
			<td rowspan="3" colspan="2"><span data-bind="text: distributions"></span></td>
			<td colspan="2" class="form">Time :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td width="50%"><b><span data-bind="text: momTime"></span></b></td>
		</tr>
		<tr style="height:25px">
			<td colspan="2" class="form">Venue :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td width="50%"><b><span data-bind="text: dsEWR.EWRMOM.VENUE"></span></b></td>
		</tr>
		<tr style="height:25px">
			<td colspan="2" class="form">Minutes Recorded by :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td width="50%"><b><span data-bind="text: dsEWR.EWRMOM.RECORDEDBY"></span></b></td>
		</tr>
		
		<tr>
			<td class="title" colspan="7" align="center">Engineering Work Request Information</td>
		</tr>
		
		<tr>
			<td colspan="3" class="form">Area :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.AREA"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Subject :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.SUBJECT"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Budget Allocated :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.BUDGETALLOC"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Account Code :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.ACCCODE"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Type of Requested Work :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><span data-bind="text: typeOfRequestedWork"></span></td>
		</tr>
		
		<tr>
			<td class="title" colspan="7" align="center">Conclutions :</td>
		</tr>
		
		<tr>
			<td colspan="3" class="form">Background :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.EWRMOM.BACKGROUND"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Scope of Work :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.EWRMOM.SCOPEOFWORK"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Project Type :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: projectType"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">EWR No :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.EWRMOM.EWRNO"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Project Title :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.EWRMOM.PROJECTTITLE"></span></b></td>
		</tr>
		
		<tr>
			<td colspan="3" class="form">Project Sponsor :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.PROJECTSPONSOR"></span></b></td>
		</tr>
		
		<tr>
			<td colspan="3" class="form">Project Deriverables :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.EWRMOM.PROJECTDELIVERABLE"></span></b></td>
		</tr>
		
		<tr>
			<td colspan="3" class="form">Action Timeline :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: actionTimeLine"></span></b></td>
		</tr>
		
		<tr>
			<td colspan="3" class="form">Operation Contact Person :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.OPERATIONALCP"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Operation Contact Person 2 :</td>
			<!-- <td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.OPERATIONALCP2"></span></b></td>
		</tr>
		<tr>
			<td colspan="3" class="form">Data Required from Sponsor :</td>
			<!-- td class="wborderB form">:</td> -->
			<td colspan="4"><b><span data-bind="text: dsEWR.EWRMOM.DATAREQUIRED"></span></b></td>
		</tr>
		
	</table>
</div>

<style>
	
	table {
		border-collapse: collapse;
		padding:10px;
		}
	tr {
		height:25px;
		font-size:13px;
		}
	
	td {
		padding:1px 5px;
		border:1px solid #333;
		valign:top;
		}
	
	.title {
		height:80;
		font-size:14px;
		background-color:#007e7a;
		font-weight:bold;
		color:white;
		}
		
	.company {
		font-weight:bold;
		background-color:#007e7a;
		color:white;
		}

	.wborderR {
		border-right-width:0px !important;
		}
		
	.wborderB {
		border-width:1px 1px 1px 0px !important;
		}
	
	.wborderL {
		border-left-width:0px !important;
		}
		
	.form {
		background-color:#cae6b4;
		color:#007e7a;
		font-weight:bold;
		}
</style>
<script>
    var <%: this.ID %>_viewModel = new kendo.observable({
        dsEWR: {},
        typeOfRequestedWork: "",
        categoryOfWork: "",
        projectType: "",
        dateIssued: "",
        dateCompletion: "",
        participants: "-",
        distributions: "",
        momDate: "",
        momTime: "",
        actionTimeLine: ""
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_Content"), <%: this.ID %>_viewModel);

        //<%: this.ID %>_viewModel.dataSource.read();
        
    });

    function <%: this.ID %>_Init(ewrID) {
        //kendo.ui.progress($("#<%: this.ID %>_Content"), true);
        $.ajax({
            url: _webApiUrl + "EWRREQUESTs/FindEWRByID/" + ewrID,
            type: "GET",
            success: function (data) {
                if (data) {

                    //kendo.parseDate("2016-08-09T05:28:46Z", "yyyy-MM-ddTHH:mm:sszzz")
                    if (data.EWRMOM) {
                        if (data.EWRMOM.MOMDATE) {
                            <%: this.ID %>_viewModel.set("momDate", kendo.toString(kendo.parseDate(data.EWRMOM.MOMDATE), "dd MMM yyyy"));
                            <%: this.ID %>_viewModel.set("momTime", kendo.toString(kendo.parseDate(data.EWRMOM.MOMDATE), "HH:mm:ss"));
                        }
                        if (data.EWRMOM.ACTIONTIMELINE) {
                            <%: this.ID %>_viewModel.set("actionTimeLine", kendo.toString(kendo.parseDate(data.EWRMOM.ACTIONTIMELINE), "dd MMM yyyy"));

                        }
                    }
                    
                    <%: this.ID %>_viewModel.set("dsEWR", data);
                    
                    if (data.TYPEOFREQUESTEDWORK.length > 0) {
                        var typeOfRequestedWork = [];
                        $.each(data.TYPEOFREQUESTEDWORK, function (idx, elem) {
                            
                            if (elem.VALUE == "budgetestimate") {
                                typeOfRequestedWork.push("Budget Estimate");
                            } else if (elem.VALUE == "purchaseonly") {
                                typeOfRequestedWork.push("Purchase Only");
                            } else if (elem.VALUE == "projectmanagement") {
                                typeOfRequestedWork.push("Project Management (Capital Only)");
                            } else if (elem.VALUE == "engineeringwork") {
                                typeOfRequestedWork.push("Engineering Work Package (Design and Drafting)");
                            } else if (elem.VALUE == "other") {
                                typeOfRequestedWork.push("Other: " + elem.DESCRIPTION);
                            }
                            
                        });
                        <%: this.ID %>_viewModel.set("typeOfRequestedWork", typeOfRequestedWork.join(", "));
                    }

                    <%--if (data.CATEGORYOFWORK.length > 0) {
                        var categoryOfWork = [];
                        $.each(data.CATEGORYOFWORK, function (idx, elem) {
                            if (elem.VALUE == "safetyhazard") {
                                categoryOfWork.push("Safety/Health Hazard");
                            } else if (elem.VALUE == "productionloss") {
                                categoryOfWork.push("Production Loss < 1 Year Payback (Please Justify)");
                            } else if (elem.VALUE == "capitalplan") {
                                categoryOfWork.push("Capital Plan");
                            } else if (elem.VALUE == "environmental") {
                                categoryOfWork.push("Environmental");
                            } else if (elem.VALUE == "generalimprovement") {
                                categoryOfWork.push("General Improvement and Others");
                            }
                            
                        });
                        <%: this.ID %>_viewModel.set("categoryOfWork", categoryOfWork.join(", "));
                    }--%>
                    
                    if (data.PROJECTTYPE && data.PROJECTTYPE.length > 0) {
                        var projectType = [];
                        $.each(data.PROJECTTYPE, function (idx, elem) {
                            if (elem.VALUE == "engineeringworkrequest") {
                                projectType.push("Engineering Work Request");
                            } else if (elem.VALUE == "technicalassistance") {
                                projectType.push("Technical Assistance");
                            } else if (elem.VALUE == "other") {
                                projectType.push("Other: " + elem.DESCRIPTION);
                            }
                            
                        });
                        <%: this.ID %>_viewModel.set("projectType", projectType.join(", "));
                    }
                    //<%: this.ID %>_viewModel.set("participants", "A");
                    if (data.USERPARTICIPANTS && data.USERPARTICIPANTS.length > 0) {
                        var participants = [];
                        $.each(data.USERPARTICIPANTS, function (idx, elem) {
                            participants.push(elem.EMPLOYEENAME);
                        });
                        <%: this.ID %>_viewModel.set("participants", participants.join(", "));
                    }
                    
                    if (data.USERDISTRIBUTIONS && data.USERDISTRIBUTIONS.length > 0) {
                        var distributions = [];
                        $.each(data.USERDISTRIBUTIONS, function (idx, elem) {
                            distributions.push(elem.EMPLOYEENAME);
                        });
                        <%: this.ID %>_viewModel.set("distributions", distributions.join(", "));
                    }

                    <%: this.ID %>_viewModel.set("dateIssued", kendo.toString(kendo.parseDate(data.DATEISSUED), "dd MMM yyyy"));
                    <%: this.ID %>_viewModel.set("dateCompletion", kendo.toString(kendo.parseDate(data.DATECOMPLETION), "dd MMM yyyy"));
                    
                }
                
                //kendo.ui.progress($("#<%: this.ID %>_Content"), false);

            },
            error: function (jqXHR) {
                //kendo.ui.progress($("#<%: this.ID %>_Content"), false);
            }
        })
    }
    
</script>