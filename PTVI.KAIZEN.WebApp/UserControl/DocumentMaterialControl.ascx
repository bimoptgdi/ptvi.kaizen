﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentMaterialControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DocumentMaterial" %>
<%@ Register Src="~/UserControl/DocumentsControl.ascx" TagPrefix="uc1" TagName="DocumentsControl" %>
<%@ Register Src="~/UserControl/MaterialsControl.ascx" TagPrefix="uc1" TagName="MaterialsControl" %>
<%@ Register Src="~/UserControl/MRListControl.ascx" TagPrefix="uc1" TagName="MRListControl" %>
<%@ Register Src="~/UserControl/DesignConformityControl.ascx" TagPrefix="uc1" TagName="DesignConformityControl" %>

<style>
    .FormTitlePanel{
        border: 1px solid black;
        border-radius: 4px 4px 0 0;
        color: #fff;
        padding: .65em .92em;
        display: inline-block;
        border-bottom-width: 0;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 0px;
        padding-bottom: 7px;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #ccc;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        top: 1px;
    }
    .FormContentPanel{
        top: 10px;
        border-radius: 0px 4px 0 0;
        border: 1px solid black;
        border-top-width: 1;
        margin-top: 0px;
        color: #333;
        padding: .65em .92em;
        display: inline-block;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #fff;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
    }
</style>
     
    <div id="DocumentsMaterialForm">
        <div class="k-content wide">
            <div>
                <div id="DocumentsMaterialTabStrip">
                    <ul>
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP))
                    { %>
                        <li>EWP Compliance</li>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.EngineerMaterialReport))
                    { %>
                        <li>MR List</li>
                        <li>Material</li>
                       
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity))
                    { %>
                      <li>Design Conformity</li>

                    <% }%>

                    </ul>

                     
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ManageDocEWP))
                    { %>
                    <div>
                        <uc1:DocumentsControl runat="server" ID="Documents1" />
                    </div>
                     <% }%>

                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.EngineerMaterialReport))
                    { %>
                    <div>
                        <uc1:MRListControl runat="server" ID="MRListControl1" />
                    </div>
                    <div>
                        <uc1:MaterialsControl runat="server" ID="MaterialsControl1" />
                    </div>
                     <% }%>
                    
                     <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity) || IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignConformity))
                    { %>
                     <div>
                        <uc1:DesignConformityControl runat="server" ID="DesignConformity1" />
                    </div>
                    <% }%>
                 

                </div>
            </div>
        </div>
    </div>

    <script>

        var <%: this.ID %>_DocumentsMaterialVM = kendo.observable({
            selectedTabIndex: 0
        });


            // Tab control
        var DocumentsMaterialTabStrip = $("#DocumentsMaterialTabStrip").kendoTabStrip({
            tabPosition: "top",
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
        });
        DocumentsMaterialTabStrip = $("#DocumentsMaterialTabStrip").getKendoTabStrip();
        DocumentsMaterialTabStrip.select(0);
            // --Tab control
        kendo.bind($("#DocumentsMaterialForm"), <%: this.ID %>_DocumentsMaterialVM);
//        OnDocumentReady = function () {
//        }
        //_projectDocSource
    </script>


