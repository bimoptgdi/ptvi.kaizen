﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportForecastBaselineVsActualTimeFinishControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportForecastBaselineVsActualTimeFinishControl" %>
<div id="<%: this.ID %>_reportForecastBaselineVsActualContainer">
    <div data-bind="visible:loaded" style="display:none;">
        <div style="font-weight:bold">
            <div style="text-align:center">Forecast based on actual and baseline - Time (Finish)</div>
            <div style="text-align:center" data-bind="html:taskName"></div>
            <div style="text-align:center" >Position <span data-bind="html: position"></span></div>
            <div style="text-align:center" >Baseline FInish: <span data-bind="html: baselineFinishStr"></span></div>
            <div style="text-align:center" >Estimated Finish: <span data-bind="html: estimatedFinishStr"></span></div>
            <div style="text-align:center" >Status: <span data-bind="html: timeVar"></span>&nbsp;<span data-bind="html: timeDiffStr"></span></div>
        </div><br /><br /><br />
        <div style="height:600px"     
            data-role="chart"
            data-transitions="false"
            data-chart-area="{background: 'lightgray'}"
            data-bind="source: dsForecastBaselineVsActual"
            data-series="[  {type: 'rangeColumn', fromField: 'baselineFinish', toField: 'estimatedFinish', colorField: 'color'},
                            {type: 'line', field: 'baselineFinish', name: 'Baseline', color: 'yellow'},
                            {type: 'line', field: 'estimatedFinish', name: 'Estimated', color: 'blue'}]",
            data-value-axis="{labels:{template: '#= kendo.toString(new Date(value), \'dd MMM\') #'}}"
            data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}">
        </div><br /><br />
        <div style="font-size:xx-small">
            <div data-role="grid" data-bind="source: dsForecastBaselineVsActual" 
                data-scrollable="false"
            data-columns="[{ template: '#= ++<%: this.ID %>_vm.record #', title:'No', width: '2%', attributes: {style: 'text-align: right;vertical-align:top'} }, 
                        { field: 'taskName', title: 'Project / Task Name', width: '14%', attributes: {style: 'vertical-align:top'} },
                        { title: 'Start Date', headerAttributes: {style: 'text-align: center'}, columns: [
                            { field: 'baselineStart', title: 'Baseline', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'estimatedStart', title: 'Estimated', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'actualStart', title: 'Actual', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} }]
                        }, 
                        { title: 'Finish Date', headerAttributes: {style: 'text-align: center'}, columns: [
                            { field: 'baselineFinish', title: 'Baseline', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'estimatedFinish', title: 'Estimated', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} },
                            { field: 'actualFinish', title: 'Actual', format: '{0: dd MMM yyyy  HH:mm}', attributes: {style: 'text-align: center;vertical-align:top'} }]
                        }, 
                        { title: 'Duration', headerAttributes: {style: 'text-align: center'}, columns: [
                            { field: 'baselineDurationInHours', title: 'Baseline', attributes: {style: 'text-align: right;vertical-align:top'}, width: '6%', format: '{0:n2}' },
                            { field: 'estimatedDurationInHours', title: 'Estimated', attributes: {style: 'text-align: right;vertical-align:top'}, width: '6%', format: '{0:n2}' },
                            { field: 'actualDurationInHours', title: 'Actual', attributes: {style: 'text-align: right;vertical-align:top'}, width: '6%', format: '{0:n2}' }]
                        },                         
                        { field: 'timeVar', title: 'Time VAR', attributes: {style: 'vertical-align:top'} },
                        { field: 'contractor', title: 'Contractor', attributes: {style: 'vertical-align:top'} }]"></div>
            </div>
    </div>   
</div>
<div  id="<%: this.ID %>_errorMessageContainer">
    <div data-bind="visible: notFound" style="top: 60px;width: 400px;text-align: center;margin: auto;position: relative;" >
        <div style="border:1px solid red;color:red;font-size:x-large;" data-bind="html: ErrMsg"></div>
    </div>
</div>

<script>
    var <%: this.ID %>_vm = kendo.observable({
        taskName: null,
        position: null,
        timeVar: null,
        durationVar: null,
        baselineFinishStr: null,
        estimatedFinishStr: null,
        timeDiffStr: null,
        dsForecastBaselineVsActual: null,
        loaded: false,
        record: 0
    });

    var <%: this.ID %>_ervm = kendo.observable({
        notFound: false,
        ErrMsg: "No Data Found"
    });

    function <%: this.ID %>_populate(projectShiftID, taskUid, callback) {
        $.ajax({
            url: webApiUrl + "iptreport/reportdata?reportName=IPT_FORECAST_BASELINEVSACTUAL&projectshiftid=" + projectShiftID + "&taskUid=" + taskUid,
            success: function (result) {
                if (result != null) {
                    for (var i = 0; i < result.list.length; i++) {
                        result.list[i]['baselineDurationInHours'] = (result.list[i].baselineDuration / 10.0) / 60.0;
                        result.list[i]['actualDurationInHours'] = (result.list[i].actualDuration / 10.0) / 60.0;
                        result.list[i]['estimatedDurationInHours'] = (result.list[i].estimatedDuration / 10.0) / 60.0;

                        result.list[i]['color'] = result.list[i].timeVar == 'Delay' ? 'red' : 'green';
                    }

                    <%: this.ID %>_vm.set("taskName", result.taskName);
                    <%: this.ID %>_vm.set("position", kendo.toString(kendo.parseDate(result.position), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("timeVar", result.timeVar);
                    <%: this.ID %>_vm.set("durationVar", result.durationVar);
                    <%: this.ID %>_vm.set("baselineFinishStr", kendo.toString(kendo.parseDate(result.baselineFinish, 'yyyy-MM-ddTHH:mm'), 'dd MMM yyyy  HH:mm'));
                    <%: this.ID %>_vm.set("estimatedFinishStr", kendo.toString(kendo.parseDate(result.estimatedFinish, 'yyyy-MM-ddTHH:mm'), 'dd MMM yyyy  HH:mm'));

                    if (result.timeVar != "On Schedule")
                        <%: this.ID %>_vm.set("timeDiffStr", kendo.toString((kendo.parseDate(result.baselineFinish, 'yyyy-MM-ddTHH:mm') - kendo.parseDate(result.estimatedFinish)) / 60000.0 / 60.0, "n2") + ' Hours');

                    <%: this.ID %>_vm.dsForecastBaselineVsActual = new kendo.data.DataSource({
                        data: result.list,
                        schema: {
                            model: {
                                fields: {
                                    baselineStart: { type: 'date' },
                                    baselineFinish: { type: 'date' },
                                    estimatedStart: { type: 'date' },
                                    estimatedFinish: { type: 'date' },
                                    actualStart: { type: 'date' },
                                    actualFinish: { type: 'date' },
                                    baselineDurationInHours: { type: 'number' },
                                    actualDurationInHours: { type: 'number' },
                                    estimatedDurationInHours: { type: 'number' }
                                }
                            }
                        }
                    });
                    
                    <%: this.ID %>_vm.set("loaded", true);
                    <%: this.ID %>_ervm.set("notFound", false);
                    status = "loaded";
                    kendo.bind($("#<%: this.ID %>_reportForecastBaselineVsActualContainer"), <%: this.ID %>_vm);
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
                }

                else {
                    <%: this.ID %>_ervm.set("notFound", true);
                    status = "notfound";
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);                   
                }

                callback(status);
            },
            error: function (e) {

                var jsonResponse = e.responseJSON;
                //console.log(jsonResponse);
                <%: this.ID %>_ervm.set("ErrMsg", jsonResponse.ExceptionMessage);
                <%: this.ID %>_ervm.set("notFound", true);
                status = "Error";
                kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
            }
        });
    }

    function <%: this.ID %>_shortLabels(value) {
        return value;

        var result = "";
        while (value.length > 20) {
            var clipIndex = 20;
            while (value[clipIndex] != ' ')
                clipIndex--;

            result = result + (result != "" ? "\n" : "") + value.substring(0, clipIndex);

            value = value.substring(clipIndex);
        }
    
        result = result + (result != "" ? "\n" : "") + value;

        return result;
    }

</script>

