﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintShiftTargetControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.PrintShiftTargetControl" %>
<script type="text/javascript">
    var <%: this.ID %>_now = kendo.parseDate('<%: DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") %>');

    var <%: this.ID %>_vm = kendo.observable({
        projectShiftID: null,
        dsProjectShift: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "shift/activeProjectShifts/" + _projectId
            },
            schema: {
                model: {
                    id: "id"
                },
                parse: function (responses) {
                    for (var j=0; j<responses.length;j++)
                    {
                        var item = responses[j];

                        if (item.shiftTargetHasGenerated) 
                            item["enabled"] = true;
                        else{
                            var items = Enumerable.From(responses).Where("$.shiftTargetHasGenerated == false").ToArray();
                            
                            for (var i = 0; i < items.length; i++) {
                                if (i == 0) 
                                    items[i]["enabled"] = true;
                                else
                                    items[i]["enabled"] = false;
                            }

                            break;
                        }
                    };

                    return responses;
                }
            }
        }),
        dsProgressCondition: [{ value: "DL", text: "Delay" }, { value: "AH", text: "Ahead / On Schedule" }],
        progressCondition: "DL",
        viewClick: function (e) {
            if (<%: this.ID %>_vm.projectShiftID == null)
            {
                alert('Please select project shift.');

                e.preventDefault();
                return false;
            }

            $("#<%: this.ID %>_reportContainer").attr('src', "LoadPage.aspx");

            $("#<%: this.ID %>_noPreview").fadeOut(300);
            $("#<%: this.ID %>_previewContainer").fadeIn(300);

            var di = <%: this.ID %>_vm.dsProjectShift.get(<%: this.ID %>_vm.projectShiftID);
            // temp
            <%--if (di.enabled == false && di.shift != "2") {
                $("#<%: this.ID %>_message").fadeIn(300);
                $("#<%: this.ID %>_reportContainer").fadeOut(300);

                e.preventDefault();
                return false;
            }
            else {
                $("#<%: this.ID %>_message").fadeOut(300);
                $("#<%: this.ID %>_reportContainer").fadeIn(300);
            }--%>
            
            $("#<%: this.ID %>_message").fadeOut(300);
            $("#<%: this.ID %>_reportContainer").fadeIn(300);

            // saving task
            $.ajax({
                url: _webApiUrl + "IPTProjectSchedule/StoreTaskInformationByShift?prjShiftID=" + <%: this.ID %>_vm.projectShiftID + "&progressCondition=" + <%: this.ID %>_vm.progressCondition,
                success: function (result) {
                    // generate shift target
                    var url = location.protocol + "//" +
                        location.hostname + "/" +
                        location.pathname.substring(1).split('/')[0] + "/" +
                                "ShiftTargetReportForm.aspx?ProjectShiftID=" + <%: this.ID %>_vm.projectShiftID;                   

                    url = url.replace(new RegExp('/', 'g'), '|');
                    url = encodeURIComponent(url);

                    var option = "--load-error-handling%20ignore --window-status%20ready_to_print";
                    var pdfUrl = _webApiUrlEllipse + "pdf/FromAddress/" + option + "%20%20" + url + "/";

                    $("#<%: this.ID %>_reportContainer").attr('src', pdfUrl);
                },
                error: function (xhr) {
                    //_showErrorMessage('Store task information Error', xhr.responseText, 3, '');
                    if (xhr.responseText.indexOf("Tasks not found.") >= 0)
                        _showMessage("Print Shift Target", "Task not found, please check the Project Shift Setup", 2, '');
                    else if (xhr.responseJSON)
                        _showErrorMessage('Print Shift Target', xhr.responseJSON.ExceptionMessage, xhr.responseText, 3, '');
                    else
                        _showErrorMessage('Print Shift Target', 'Failed when storing task by shift. Click show detail for more information.', xhr.responseText, 3, '');

                    $("#<%: this.ID %>_previewContainer").fadeOut(300);
                }
            });

            e.preventDefault();
            return false;
        },
        onDataBound: function (e) {
            // Default Shift untuk print shift target adalah 1 shift setelah shift yg saat ini berjalan
            var prjShiftList = <%:this.ID%>_vm.dsProjectShift.data();
            for (var i = prjShiftList.length - 1; i >= 0; i--) {
                ps = prjShiftList[i];

                if (kendo.parseDate(ps.startDate) <= <%: this.ID %>_now && kendo.parseDate(ps.endDate) >= <%: this.ID %>_now)
                {
                    <%: this.ID %>_vm.set("projectShiftID", i == prjShiftList.length - 1 ? ps.id : prjShiftList[i + 1]); // <-- 1 shift setelah shift yg saat ini berjalan
                    break;
                }
            }
        }
    });

    $(document).ready(function () {
        kendo.bind($("#PrintShifTargetForm"), <%: this.ID %>_vm);
    });

</script>
<script id="project-shift-template" type="text/x-kendo-template">
    #: id#: #:kendo.toString(kendo.parseDate(startDate), 'dd MMM yyyy  HH:mm:ss')# - #:kendo.toString(kendo.parseDate(endDate), 'dd MMM yyyy  HH:mm:ss')#
</script>
<div id="PrintShifTargetForm" style="position:relative">
    <fieldset class="gdiFieldset">
        <legend class="gdiLegend">Input Criteria</legend>
        <div>
            <div>
                <label class="labelForm">Shift<span style="color:red">*</span></label>
                <input 
                    data-role="combobox" 
                    id="<%: this.ID %>_shift" 
                    name="<%: this.ID %>_shift" 
                    data-bind="source: dsProjectShift, value: projectShiftID, events: { dataBound: onDataBound  }" 
                    data-value-field="id"
                    data-text-field="text"
                    data-value-primitive="true" 
                    data-filter = "contains"
                    required
                    placeholder="select shift" 
                    validationMessage="Shift is required"
                    style="width:350px"/>
            </div>
            <div>
                <label class="labelForm">Progress Condition<span style="color:red">*</span></label>
                <input 
                    data-role="dropdownlist" 
                    id="<%: this.ID %>_progressCondition" 
                    name="<%: this.ID %>_progressCondition" 
                    data-bind="source: dsProgressCondition, value: progressCondition" 
                    data-value-field="value"
                    data-text-field="text"
                    data-value-primitive="true"                     
                    required
                    placeholder="select progress condition" 
                    validationMessage="Progress condition is required"
                    style="width:350px"/>
            </div>
            <div>
                <label class="labelForm"></label>
                <div style="width:350px;display:inline-block"><button id="<%: this.ID %>_btnView" data-role="button" style="width:140px;float:right" data-bind="events: { click: viewClick }">View</button></div>                
            </div>
        </div>
    </fieldset><br />
    <fieldset id="<%: this.ID %>_previewContainer" class="gdiFieldset" style="min-height:100px;display:none">
        <legend class="gdiLegend">Preview</legend>
        <div id="<%: this.ID %>_noPreview" style="margin:auto;text-align:center">No Preview</div>
        <div id="<%: this.ID %>_message" style="color:red;text-align:center;width:100%;display:none">Shift target of previous shift is not generated yet, Shift Target must be generated sequentially.</div>
        <iframe id="<%: this.ID %>_reportContainer" style="width:100%;height:660px;display:none" src="LoadPage.aspx"></iframe>
    </fieldset>
</div>
