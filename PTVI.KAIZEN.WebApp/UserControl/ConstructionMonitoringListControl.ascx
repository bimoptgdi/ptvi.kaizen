﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConstructionMonitoringListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ConstructionMonitoringListControl" %>
<%@ Register Src="~/UserControl/ConstructionMonitoringReportControl.ascx" TagPrefix="uc1" TagName="ConstructionMonitoringReportControl" %>

<script>
    var <%: this.ID %>_popConstructionMonitoringReport;
    var <%: this.ID %>_viewConstructionMonitoringModel = kendo.observable({
        dsListConstructionMonitoring: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoring"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoring"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "tenders/ConstructionMonitoringList/" + _projectId,
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        openConstructionMonitoringReportControl: function (e) {
            ConstructionMonitoringReport_tenderId = e.data.id;
            kendo.bind($("#ConstructionMonitoringReport_ConstructionMonitoringReportContent"), ConstructionMonitoringReport_viewConstructionMonitoringReportModel);
            ConstructionMonitoringReport_viewConstructionMonitoringReportModel.dsTender();
            ConstructionMonitoringReport_viewConstructionMonitoringReportModel.refreshGrid();
            <%: this.ID %>_popConstructionMonitoringReport.center().open();

        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ConstructionMonitoringContent"), <%: this.ID %>_viewConstructionMonitoringModel);
        <%: this.ID %>_viewConstructionMonitoringModel.dsListConstructionMonitoring.read();

        <%: this.ID %>_popConstructionMonitoringReport = $("#<%: this.ID %>_popConstructionMonitoringReport").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 850,
            height: 500
        }).data("kendoWindow");

    });

</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_contractNo-template">
        <div>
            <a href="\#" data-bind="events: { click: openConstructionMonitoringReportControl }">#: contractNo #</a>
            
        </div>
</script>

<div id="<%: this.ID %>_ConstructionMonitoringContent">
    <div id="<%: this.ID %>_popConstructionMonitoringReport">
        <uc1:ConstructionMonitoringReportControl runat="server" ID="ConstructionMonitoringReport" />
    </div>
    <div class="headerSubProject">Contract List Report</div>
    <div id="<%: this.ID %>_grdListConstructionMonitoring" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-columns="[
            {
                field: 'contractNo', title: 'Contract No',
                template: kendo.template($('#<%: this.ID %>_contractNo-template').html()), 
                width: 50
            },
            {
                field: 'docNoSummary', title: 'EWP',
                width: 110
            },
            {
                field: 'vendorSelectedName', title: 'Vendor Name',
                width: 110
            },
            {
                field: 'contractTrafic', title: ' ',
                headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                attributes: { style: 'text-align: center; vertical-align: top !important;' },
                template: '#if (contractTrafic) { #<img class=\'logo\' src=\'images/#:common_getTrafficImageUrl(contractTrafic)#\' style=\'padding-top: 0px;\'>#}#',
                width: 40
            }
        ]"
        data-bind="source: dsListConstructionMonitoring"
        data-pageable="{ buttonCount: 10 }">
    </div>
</div>