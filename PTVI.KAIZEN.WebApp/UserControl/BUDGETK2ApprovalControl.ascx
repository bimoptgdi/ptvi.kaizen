﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BUDGETK2ApprovalControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.BUDGETK2ApprovalControl" %>

<div id="<%: this.ID %>_RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_inputProjectLegend">Budget Approval</legend>
                <div>
                    <label class="labelFormWeight">Project No</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: projectData.projectNo, enabled: isEnabled" maxlength="50" placeholder="Project No" />
                </div>
                <div>
                    <label class="labelFormWeight">Project Description<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: projectData.projectDescription, enabled: isEnabled" maxlength="200" placeholder="Project Description" required />
                </div>
                <div>
                    <label class="labelFormWeight">Project Manager<span style="color: red">*</span></label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.projectManager, enabled: isEnabled" placeholder="Project Manager" required />--%>
                    <input data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-auto-bind="false"
                        data-filter="contains"
                        data-bind="value: projectData.projectManager,
                                    source: dsProjectManager,
                                    enabled: isEnabled,
                                    events: {
                                        select: onProjectManagerSelect,
                                        filtering: autocompleteFiltering
                                    }"
                        style="width: 400px;"
                        required
                    />
                </div>
            <div>
                <label class="labelFormWeight">Budget Approval</label>
                <input data-role="numerictextbox"
                    data-format="c"
                    data-min="0"
                    data-bind="value: requestData.budgetAmount, enabled: isEnabled"
                    style="width: 180px">
            </div>
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Detail of Work Requirement</legend>
                
                    <div>
                        <label class="labelFormWeight">4. File Name</label>
                        <div data-role="grid" data-bind="source: supportingDocuments"
                            data-columns="[
                            { 
                                'field': 'FILENAME', 
                                'title': 'File Name', 'width': '50%', 
                                template: <%: this.ID %>_viewModel.supportingDocumentColumnTemplate
                            }
                            ]"
                            >

                        </div>
                    </div>
                    <%--<table>
                        <tr>
                            <td style="width: 30%"><label class="labelFormWeight">Data Supporting Document</label></td>
                            <td style="width: 50%">
                                <div data-role="grid" data-bind="source: supportingDocuments"
                                    data-columns="[
                                    { 
                                        'field': 'FILENAME', 
                                        'title': 'File Name', 'width': '100px', 
                                        template: <%: this.ID %>_viewModel.supportingDocumentColumnTemplate
                                    }
                                    ]"
                                    >

                                </div>

                            </td>
                        </tr>
                    </table>
                   --%>
                </fieldset>
               
            </div>
           
            
        </fieldset>
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        isReadonly: true,
        items: [],
        itemsRequestedWork: [],
        itemInitiate: [],
        supportingDocuments: [],
        isOtherChecked: false,
        otherDescription: "",
        projectManagerEmail: "",
        projectManagerBN: "",
        parentForm: "",
        requestData: new common_ewrRequest(),
        onOtherChange: function (e) {
            this.set("isOtherChecked", !this.get("isOtherChecked"));
            if (!this.get("isOtherChecked")) {
                this.set("otherDescription", "");
            }

        },
        isEnabled: true,
        onValidationEntry: function () {
            var validatable = $("#<%: this.ID %>_RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validatable.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validatable.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }

            return isValid;
        },
        
        dsEmployee: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {

                    var searchParam = options.data.filter.filters[0].value;
                    if (searchParam.length > 0) {
                        var result = [];
                        // if (this.get("selectedUser").length >= 3) {

                        //var filter = "?$filter=((EMPLOYEE_ID contains '*" + searchParam + "*') or lower(FULL_NAME) contains lower('*" + searchParam + "*')) and (LEVEL_INFO not contains '*L2*' and LEVEL_INFO not contains '*L3*' and LEVEL_INFO not contains '*L4*')";
                        var filter = "?$filter=(substringof('" + searchParam + "',EMPLOYEEID) or substringof(tolower('" + searchParam + "'),tolower(FULL_NAME)))";
                        //filter += " and (substringof('L2',LEVEL_INFO) eq true)";

                        var result = [];
                        $.ajax({
                            url: _webOdataUrl + "EmployeeOData" + filter,
                            type: "GET",
                            async: false,
                            success: function (data) {
                                //result = data.value;

                                options.success(data.value);
                            },
                            error: function (data) {
                                //return [];
                                //options.error(data.value);
                                options.error(data.value);
                            }
                        });
                    } else {
                        options.success([]);
                    }
                    // }

                }
            }
        }),
        onOperationalCPSelected: function (e) {
                
            <%: this.ID %>_viewModel.set("requestData.operationalCP", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.operationalCPEmail", e.dataItem.EMAIL);
            
        },
        onOperationalCPClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onProjectSponsorSelected: function(e) {
                
            <%: this.ID %>_viewModel.set("requestData.projectSponsorBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.projectSponsorEmail", e.dataItem.EMAIL);
        },
        onProjectSponsorClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        supportingDocumentColumnTemplate: function(dataItem) {
            var result = "<a href='" + "<%: DocServer %>" + "getdoc.aspx?id=" + dataItem.REFDOCID + "&op=n' target='_blank'>" + dataItem.FILENAME + "</a>";
            return result;
        },
        onClearEvent: function (e) {
            this.requestData.set("projectNo", null);
            this.requestData.set("projectDescription", null);
            this.requestData.set("projectManager", null);
            this.requestData.set("budgetApproval", null);
            this.requestData.set("fileName", null);
            this.set("otherDescription", "");
            this.set("isOtherChecked", false);
            this.set("items", []);
            this.set("itemsRequestedWork", []);
            this.set("itemInitiate", []);
        }
    });

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    // Document Ready
    $(document).ready(function () {
        <%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#<%: this.ID %>_RequestContent"), <%: this.ID %>_viewModel);

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        <%: this.ID %>_viewModel.onClearEvent()
        $("#<%: this.ID %>_RequestContent").kendoTooltip({
            filter: "#<%: this.ID %>_shareFolderDoc",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format Share Folder, e.g.: \\\\netapp8\\folder";
                return content;
            }
        }).data("kendoTooltip");

        <%: this.ID %>_viewModel.requestData.set("dateIssued", new Date());
    }); //doc ready

    function <%: this.ID %>_Init(dataRequest, parentForm, isReadOnly) {
        
        <%: this.ID %>_viewModel.set("isEnabled", !isReadOnly);
        <%: this.ID %>_viewModel.set("isOtherChecked", !isReadOnly);
        
        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        // jika dataRequest ada data nya maka update
        if (dataRequest) {
            var requestData =
            {
                id: dataRequest.OID,
                //ewrNo: 
                projectNo: dataRequest.PROJECTNO,
                projectDescription: dataRequest.PROJECTDESCRIPTION,
                projectManager: dataRequest.PROJECTMANAGER,
                budgetAmount: dataRequest.budgetAmount,
                fileName: dataRequest.FILENAME
            };

            <%: this.ID %>_viewModel.set("requestData", requestData);

            var arrReqWork = [];
            $.each(dataRequest.TYPEOFREQUESTEDWORK, function (idx, elem) {
                arrReqWork.push(elem.VALUE);
                if (elem.VALUE == "other") {
                    <%: this.ID %>_viewModel.set("otherDescription", elem.DESCRIPTION);
                    <%: this.ID %>_viewModel.set("isOtherChecked", true);
                }
            });
            <%: this.ID %>_viewModel.set("itemsRequestedWork", arrReqWork);

            var arrCat = [];
            $.each(dataRequest.CATEGORYOFWORK, function (idx, elem) {
                arrCat.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("items", arrCat);

            var arrInitiate = [];
            $.each(dataRequest.DETAILOFWORK, function (idx, elem) {
                arrInitiate.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("itemInitiate", arrInitiate);

            //var arrSupportingDoc = [];
            //$.each(dataRequest.SUPPORTINGDOCUMENTS, function (idx, elem) {
            //    arrSupportingDoc.push({ OID: elem.OID, FILENAME: elem.FILENAME });
            //    console.log(elem.FILENAME);
            //})
            <%: this.ID %>_viewModel.set("supportingDocuments", dataRequest.SUPPORTINGDOCUMENTS);

            $('#<%: this.ID %>_create').hide();
            $('#<%: this.ID %>_update').show();
        } else {
            $('#<%: this.ID %>_create').show();
            $('#<%: this.ID %>_update').hide();
        }

        // set is other to false if readonly = true
        <%: this.ID %>_viewModel.set("isOtherChecked", !isReadOnly);
    }
</script>