﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfficerVendorControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.PickMaterialControl" %>

<div id="<%: this.ID %>_OfficerVendorContent">
    <div>
        <div id="<%: this.ID %>_OfficerVendorGrid" 
            data-role="grid"
            data-bind="source: dsOfficerVendor"
            data-pageable="false"
            data-selectable="true"
            data-height="400"
            data-columns='[
                { field: "trafficOfficer", title: "Officer" },
                { field: "vendor", title: "Vendor" }
            ]'

        ></div>
    </div>
    <div style="float:right;padding-top:20px;">
        <div data-role="button" class="k-button k-primary" data-bind="events: { click: onCloseSelectEvent }">Close</div>

    </div>

    <div style="clear:right;"></div>
    

</div>

<script>
    var <%: this.ID %>_viewModel = kendo.observable({
        dsOfficerVendor: new kendo.data.DataSource({
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TrafficOfficerVendors"
                    },
                    dataType: "json"
                },
            },
            pageable: false,
            schema: {
                model: {
                    id: "id"
                }
            }

        }),
        onCloseSelectEvent: function (e) {
            alert("asdfasd")
        }
    }); // View Model
    function <%: this.ID %>_DataBind() {
        kendo.bind($("#<%: this.ID %>_OfficerVendorContent"), <%: this.ID %>_viewModel);
    }
        
    $(document).ready(function () {
       
    }); //doc ready
</script>