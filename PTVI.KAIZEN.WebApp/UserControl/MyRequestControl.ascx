﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyRequestControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.MyRequestControl" %>
<%@ Register Src="~/UserControl/ReviseEWRControl.ascx" TagPrefix="uc1" TagName="ReviseEWRControl" %>
<%@ Register Src="~/UserControl/K2HistoryControl.ascx" TagPrefix="uc1" TagName="K2HistoryControl" %>


<div id="<%: this.ID %>_Content" class="container">
    <div class="row">
        <div class="col">
            
            <fieldset data-bind="invisible: searchOption">
                <legend>Search</legend>
                <div class="container">
                    <div class="row">
                        <div class="col">
                                <div>
                                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Subject</div>
                                <div style="display: inline-block;padding-left: 20px">
                                    <input
                                        type= "text" 
                                        class= "k-textbox" 
                                        data-bind="value: subject"/>
                                </div>
                            </div>
                            <div>
                                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Account Code</div>
                                <div style="display: inline-block;padding-left: 20px">
                                    <input
                                        type= "text" 
                                        class= "k-textbox" 
                                        data-bind="value: accountCode"
                                        style="width: 250px"/>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div style="padding-bottom: 10px">
                                <div style="display: inline-block; width: 150px">Project Manager:</div> 
                                <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                                    <select
                                        style="width: 400px;"
                                        data-role="multiselect"
                                        data-placeholder="Project Managers"
                                        data-value-primitive="true"
                                        data-text-field="employeeName"
                                        data-value-field="employeeId"
                                        data-readonly="true"
                                        data-filter="contains"
                                        data-bind="value: valuePM, source: dataPM"></select>
                                </div>
                            </div>
                            <div style="padding-bottom: 10px">
                                <div style="display: inline-block; width: 150px">Project Sponsor:</div> 
                                <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                                    <select
                                        style="width: 400px;"
                                        data-role="multiselect"
                                        data-placeholder="Project Sponsors"
                                        data-value-primitive="true"
                                        data-text-field="FULL_NAME"
                                        data-value-field="EMPLOYEE_ID"
                                        data-auto-bind="false"
                                        data-min-length="3"
                                        data-readonly="true"
                                        data-filter="contains"
                                        data-bind="value: valuePS, source: dsEmployee"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div id="<%: this.ID %>_btnSearch" 
                    data-role="button"
                    data-bind="events: {click: searchClick}"
                    >Search
                </div>
                <div id="<%: this.ID %>_btnReset" 
                    data-role="button"
                    data-bind="events: {click: resetClick}"
                    >Reset
                </div>
                
            </fieldset>  
        </div>
    </div>
    <div class="row">
        <div class="col">
            <fieldset data-bind="invisible: searchOption">
                <legend>My EWR Request</legend>
                <div id="<%: this.ID %>_grdResult" data-role="grid"
                    data-bind="source: dsEWR"
                    data-editable="{mode:'popup', confirmation:true}"
                    data-pageable="{buttonCount: 5}"
                    data-resizable="true"
                    data-filterable="{
                            extra: false,
                            operators: {
                                string: {
                                    contains: 'Contains',
                                    startswith: 'Starts with'
                                }
                            }
                        }"
                    data-columns="[
                            { 'field': 'SUBJECT', 'title': 'Subject', 'template': <%: this.ID %>_viewModel.subjectTemplate },
                            { 'field': 'ASSIGNEDPM', 'title': 'Project Manager', 'template': <%: this.ID %>_viewModel.PMTemplate },
                            { 'field': 'PROJECTSPONSOR', 'title': 'Project Sponsor' },
                            { 'field': 'AREA', 'title': 'Area' },
                            { 'field': 'ACCCODE', 'title': 'Account Code' },
                            { 'field': 'NETWORKNO', 'title': 'Network No' },
                            { 'field': 'BUDGETALLOC', 'title': 'Budget Alloc.', 'format': '{0:c2}' },
                            { 'field': 'DATEISSUED', 'title': 'Issued', template: '#= kendo.toString(kendo.parseDate(DATECOMPLETION, \'yyyy-MM-dd\'), \'dd MMM yyyy\') #' },        
                            { 'field': 'DATECOMPLETION', 'title': 'Completion', template: '#= kendo.toString(kendo.parseDate(DATECOMPLETION, \'yyyy-MM-dd\'), \'dd MMM yyyy\') #' },
                            { 'field': '', 'title': 'MoM EWR No', 'template': <%: this.ID %>_viewModel.momEWRNoTemplate },
                            { 'field': 'STATUS', 'title': 'Status' }
                        ]">
                </div>
            </fieldset>
        </div>
    </div>
    <div id="<%: this.ID %>_popupViewEWRRequest">
        <uc1:ReviseEWRControl runat="server" ID="ViewEWRControl" />
        <br />
        <uc1:K2HistoryControl runat="server" ID="ViewK2HistoryControl" />
        <br />
        <div>
            <div style="float: right;">
                
                <div
                    data-role="button"
                    data-bind="events: { click: onCloseViewEvent }">
                    Close
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
            &nbsp;
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onPrintPDFEvent }, visible: isPrintPDFVisible"
                    class="k-primary">
                    Print to PDF
                        <span class="glyphicon glyphicon-ban-circle"></span>
                </div>
            </div>
            
        </div>
        <br />
    </div>
</div>
<script>
    // Variables
    var <%: this.ID %>_popupViewEWRRequest;

    var <%: this.ID %>_viewModel = kendo.observable({
        ewrRequestID: -1,
        isPrintPDFVisible: false,
        dsEWR: new kendo.data.DataSource({
            schema: {
                model: {
                    id: "OID",
                    fields: {
                        SUBJECT: { type: "string" },
                        AREA: { type: "string" },
                        DATEISSUED: { type: "date" },
                        ACCCODE: { type: "string" },
                        NETWORKNO: { type: "string" },
                        BUDGETALLOC: { type: "number" },
                        DATECOMPLETION: { type: "date" }
                    }
                }
            },
           <%-- transport: {
                read: {
                    url: _webApiUrl + "EWRREQUESTs/FindEWRRequestByBN/<%: BadgeNo %>",
                    dataType: "json"
                },
                update: {
                    url: "",
                    dataType: "json"
                },
                create: {
                    url: "",
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            },--%>
            pageSize: 10
        }),
        dataPM: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_Content"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_Content"), false);
            },
            transport: {
                read: {
                    dataType: "json",
                    url: _webApiUrl + "v_projectEmployeeRole/listEmployeeByRoleId/1"
                },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "employeeId",
                },
            },
            sort: [{ field: "employeeName", dir: "asc" }],
            pageSize: 10,
            batch: false
        }),
        dsEmployee: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }
            },  
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        searchClick: function() {
            
            <%: this.ID %>_viewModel.get("dsEWR").data([]);

            kendo.ui.progress($("#<%: this.ID %>_Content"), true);

            $.ajax({
                url: _webApiUrl + "EWRREQUESTs/FindDataByCriteria/dw",
                type: "POST",
                data: {
                    ACCCODE: <%: this.ID %>_viewModel.accountCode,
                    SUBJECT: <%: this.ID %>_viewModel.subject,
                    PROJECTMANAGER: <%: this.ID %>_viewModel.valuePM.join(';'),
                    PROJECTSPONSOR: <%: this.ID %>_viewModel.valuePS.join(';'),
                    REQUESTBY: "<%: BadgeNo %>"
                    //REQUESTDATEFROM: common_dateFormat(viewModelInquiry.inquiryModel.get("REQUESTDATEFROM"), "MM/dd/yyyy"),
                    //REQUESTDATETO: common_dateFormat(viewModelInquiry.inquiryModel.get("REQUESTDATETO"), "MM/dd/yyyy"),
                    //REQMTDATEFROM: common_dateFormat(viewModelInquiry.inquiryModel.get("REQMTDATEFROM"), "MM/dd/yyyy"),
                    //REQMTDATETO: common_dateFormat(viewModelInquiry.inquiryModel.get("REQMTDATETO"), "MM/dd/yyyy"),
                    //CHARGINGACCOUNT: viewModelInquiry.inquiryModel.get("CHARGINGACCOUNT"),
                    //REQUESTBY: userLoginData.badgeNo,
                    //STATUS: viewModelInquiry.get("selectedStatus"),
                    //APPROVER: viewModelInquiry.inquiryModel.get("APPROVER")
                },
                success: function (result) {
                    kendo.ui.progress($("#<%: this.ID %>_Content"), false);
                    if (result) {
                        
                        <%: this.ID %>_viewModel.get("dsEWR").data(result);

                    } else {
                        <%: this.ID %>_viewModel.get("dsEWR").data([]);
                    }
                    var grid = $("#<%: this.ID %>_grdResult").data("kendoGrid");
                    grid.dataSource.page(1);
                },
                error: function () {
                    alert("Failed to retrieve data from the server. Please try again.");
                    kendo.ui.progress($("#<%: this.ID %>_Content"), false);
                },
                complete: function (jqXhr, textStatus) {

                }
            });
        },
        resetClick: function() {
            <%: this.ID %>_viewModel.set("accountCode", "");
            <%: this.ID %>_viewModel.set("subject", "");
            <%: this.ID %>_viewModel.set("valuePM", []);
            <%: this.ID %>_viewModel.set("valuePS", []);
        },
        valuePM: [],
        valuePS: [],
        accountCode: "",
        subject: "",
        searchOption: false,
        subjectTemplate: function (dataItem) {
            if (dataItem) {
                var result = "<a href='#' onclick='<%: this.ID %>_openViewControl(" + dataItem.OID + ");'>" + (dataItem.SUBJECT ? dataItem.SUBJECT : "<i>(View Draft)</i>") + "</a>";
                return result;
            }
            return "";
        },
        PMTemplate: function (dataItem) {
            if (dataItem && dataItem.EWRMOM) {
                return "<span>" + (dataItem.EWRMOM.PROJECTMANAGER ? dataItem.EWRMOM.PROJECTMANAGER : "-") + "</span>";
            }
            return "-";
        },
        momEWRNoTemplate: function (dataItem) {
            if (dataItem && dataItem.EWRMOM) {
                return "<span>" + (dataItem.EWRMOM.MOMEWRNO ? dataItem.EWRMOM.MOMEWRNO : "-") + "</span>";
            }
            return "-";
        },
        onCloseViewEvent: function () {
           
            ViewEWRControl_viewModel.onClearEvent();
            <%: this.ID %>_popupViewEWRRequest.close();
        },
        onPrintPDFEvent: function () {
            var url = "<%: WebAppUrl.Replace("/", "|") %>" + "TemplateEWRDetailForm" + ".aspx?EWRID=" + <%: this.ID %>_viewModel.get("ewrRequestID");

            url = encodeURIComponent(url);
            var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20" + url + "/";

            window.open(pdfUrl);
        }
    });
    
    kendo.bind($("#<%: this.ID %>_Content"), <%: this.ID %>_viewModel);
    
    // Functions
    function <%: this.ID %>_openViewControl(appID) {
        <%: this.ID %>_popupViewEWRRequest.center().open();
        <%: this.ID %>_viewModel.set("ewrRequestID", appID);
        $.ajax({
            url: _webApiUrl + "EWRREQUESTs/FindEWRByID/" + appID,
            type: "GET",
            success: function (data) {
                if (data && data.K2Histories) {
                    var histories = data.K2Histories;
                    var result = $.grep(histories, function (el, idx) {
                        return el.ActivityName == "Operating Project Commitee" && el.Status == "Completed"
                    });
                    
                    if (result.length > 0) {
                        <%: this.ID %>_viewModel.set("isPrintPDFVisible", true);
                    } else {
                        <%: this.ID %>_viewModel.set("isPrintPDFVisible", false);
                    }
                        
                }
                ViewEWRControl_InitViewOnly(data, "EWRTaskListForm.aspx");
                var histories = [];

                $.each(data.K2Histories, function (idx, elem) {
                    if (elem.Status == "Completed") {
                        histories.push({
                            ActivityName: elem.ActivityName,
                            CreatedDate: kendo.toString(kendo.parseDate(elem.CreatedDate), "dd MMM yyyy hh:mm"),
                            Name: elem.Name,
                            ActionResult: elem.ActionResult,
                            Comment: elem.Comment
                        });
                    }
                });
                ViewK2HistoryControl_Init(histories);
            },
            error: function (jqXHR) {
            }
        });
        
    }


    // 
    $("#<%: this.ID %>_popupViewEWRRequest").kendoWindow({
        title: "View EWR Request",
        modal: true,
        visible: false,
        resizable: false,
        width: "80%",
        height: "95%",
        activate: function () {
            // lookup bind

        }
    });
    <%: this.ID %>_popupViewEWRRequest = $("#<%: this.ID %>_popupViewEWRRequest").data("kendoWindow");
</script>