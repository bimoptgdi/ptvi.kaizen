﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignEngineerDesignerTechnicalSupportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.AssignEngineerDesigner" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>
<script>
    var <%: this.ID %>_technicalSupportId = "",<%: this.ID %>_popMaterialComponent;
    var _technicalSupportId = <%: this.ID %>_technicalSupportId;
    var <%: this.ID %>_viewAssignEngineerDesignerModel = kendo.observable({
        tsNo: ": -",
        tsDesc: ": -",
        dsTender: function () {
            //kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), true);
            var that = this;
            $.ajax({
                url: _webApiUrl + "TechnicalSupport/TechnicalSupportListBytsId/" +<%: this.ID %>_technicalSupportId,
                type: "GET",
                success: function (data) {
                    that.set("tsNo", ": " + data.tsNo);
                    that.set("tsDesc", ": " + data.tsDesc);
                    kendo.ui.progress($("#<%: this.ID %>_ContractHead"), false);
                },
                error: function (jqXHR) {
                    kendo.ui.progress($("#<%: this.ID %>_ContractHead"), false);
                }
            });
        }

    });


</script>
<style>
    .FormTitlePanel{
        border: 1px solid black;
        border-radius: 4px 4px 0 0;
        color: #fff;
        padding: .65em .92em;
        display: inline-block;
        border-bottom-width: 0;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 0px;
        padding-bottom: 7px;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #ccc;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        top: 1px;
    }
    .FormContentPanel{
        top: 10px;
        border-radius: 0px 4px 0 0;
        border: 1px solid black;
        border-top-width: 1;
        margin-top: 0px;
        color: #333;
        padding: .65em .92em;
        display: inline-block;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #fff;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
    }
</style>
<script type="text/x-kendo-template" id="ckBoxTemplate">
    <input type="checkbox" #= isActive ? "checked='checked'" : "" # disabled />
</script>

<div style="display:none" id="popUpEmployee">
                <uc1:PickEmployeesControl runat="server" ID="PickEmployeesTS" />
</div>
<div id="<%: this.ID %>_AssignEngineerDesignerContent">
    <div id="<%: this.ID %>_ContractHead">
        <div>
            <label class="labelForm">Technical Support No</label>
            <span data-bind="text: tsNo"></span>
        </div>
        <div>
            <label class="labelForm">Technical Support Description</label>
            <span data-bind="text: tsDesc"></span>
        </div>
       
    </div>
    <div class="FormContentPanel">
        <fieldset>
                <legend>Engineer</legend>
                <div>
                    <div id="<%: this.ID %>_grdEngineer" data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
                        {title: 'Position', template: '#= positionName() #', width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75 },
                        {command: ['edit'] }
                        ]" 
                        data-editable= "inline", 
                        data-toolbar= "[{name: 'create', text: 'Add New Engineer'}]" 
                        data-pageable="{ buttonCount: 5 }"
                        data-role="grid" 
                        data-bind="source: EngineerSource, events:{save:saveEngineer}"
                        ></div>
                </div>
            </fieldset>
            <br />
            <fieldset>
                <legend>Designer</legend>
                <div>
                    <div id="<%: this.ID %>_grdDesigner"  data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
                        {title: 'Position', template: '#= positionName() #', width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75},
                        {command: ['edit'] }
                        ]" 
                        data-editable= "inline", 
                        data-toolbar= "[{name: 'create', text: 'Add New Designer'}]" 
                        data-pageable="{ buttonCount: 5 }"
                        data-role="grid" 
                        data-bind="source: DesignerSource, events:{save:saveDesigner}"
                        ></div>
                </div>
            </fieldset>

    </div>
</div>

    <script>
        $("#popUpEmployee").kendoWindow({
            title: "Pick Employee",
            modal: true,
            width: 700
        });
        var <%: this.ID %>_engineerSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdEngineer"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdEngineer"), false);
            },
            transport: {
                create: { url: _webApiUrl + "TechnicalSupport/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                read: {
                    url: function (e)
                    {
                        return _webApiUrl + "Technicalsupport?mode=GetByAssType&param=1&AssignmentType=TSE&tsID=" + <%:this.ID%>_technicalSupportId;
                    }

                        
                },
                update: {
                    type: "PUT", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "TechnicalSupport/PutUserAssignmentObject/" + e.id;
                    }

                },
                destroy: {
                    type: "DELETE", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "TechnicalSupport/delUserAssignmentObject/" + e.id;
                    }

                },
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        options.joinedDate = kendo.toString(options.joinedDate,'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.technicalSupportId = <%: this.ID %>_technicalSupportId;
                        options.isActive = true;
                        options.assignmentType = "TSE";
                        options.toTechnicalSupportInteraction = "A";
                        options.createdBy = _currNTUserID;
                        options.updatedBy = _currNTUserID;
                        options.isActive = true;
                        options.employeeId = <%: this.ID %>_viewModel.employeeId
                        options.employeeName = <%: this.ID %>_viewModel.employeeName;
                        options.employeeEmail = <%: this.ID %>_viewModel.employeeEmail;
                        options.employeePosition = <%: this.ID %>_viewModel.employeePosition;
                        return options;
                    }
                    if (operation == "update") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                         options.technicalSupportId = <%: this.ID %>_technicalSupportId;
                        options.assignmentType = "TSE";
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        technicalSupportId: { type: "number" },
                        assignmentType: { type: "string" },
                        toTechnicalSupportInteraction: { type: "string" },
                        employeeId: { type: "string" },
                        employeeName: { type: "string" },
                        employeeEmail: { type: "string" },
                        employeePosition: { type: "string" },
                        joinedDate: { type: "date" },
                        isActive: { type: "boolean" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                    positionName: function () {
                        return common_getPositionByPositionID(this.employeePosition);
                    }
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_engineerSource.read();
                        alert("Process failed: "+d);
                        return 0;
                    } else{
                        return d;
                        //< %: this.ID %>_engineerSource.read();
}
                },
            },
            pageSize: 10,
            change: <%: this.ID %>_change,
        });

        var <%: this.ID %>_designerSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdDesigner"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdDesigner"), false);
            },
            transport: {
                create: { url: _webApiUrl + "TechnicalSupport/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                read: {
                    url:  function (e){
                        return _webApiUrl + "TechnicalSupport/GetByAssType/1" + "?AssignmentType=TSD&tsID=" +<%: this.ID %>_technicalSupportId;
                    }, dataType: "json"
                },
                update: {
                    type: "PUT", dataType: "json",
                    url: function(e){
                        return _webApiUrl + "TechnicalSupport/PutUserAssignmentObject/" + e.id;
                        }
                },
                destroy: {
                    type: "DELETE", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "TechnicalSupport/delUserAssignmentObject/" + e.id;
                    }
                },
                //                update: { url: _webApiUrl + "UserAssignment", type: "PUT", dataType: "json" }
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.technicalSupportId = <%: this.ID %>_technicalSupportId;
                        options.isActive = true;
                        options.assignmentType = "TSD";
                        options.toTechnicalSupportInteraction = "A";
                        options.createdBy = _currNTUserID;
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                    if (operation == "update") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.technicalSupportId = <%: this.ID %>_technicalSupportId;
                        options.assignmentType = "TSD";
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        technicalSupportId: { type: "number" },
                        assignmentType: { type: "string" },
                        toTechnicalSupportInteraction: { type: "string" },
                        employeeId: { type: "string" },
                        employeeName: { type: "string" },
                        employeeEmail: { type: "string" },
                        employeePosition: { type: "string" },
                        joinedDate: { type: "date" },
                        isActive: { type: "boolean" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                    positionName: function () {
                        return common_getPositionByPositionID(this.employeePosition);
                    }
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_designerSource.read();
                        alert("Process failed: " + d);
                        return 0;
                    } else{
                        return d;
                        //< %: this.ID %>_designerSource.read();
                    }
                },
            },
            pageSize: 10,
            change: <%: this.ID %>_change,
        });

        var <%: this.ID %>_viewModel = new kendo.observable({
            EngineerSource: <%: this.ID %>_engineerSource,
            DesignerSource: <%: this.ID %>_designerSource,
            GetEmployee: <%: this.ID %>_getEmployee,
            tsNo: ": -",
            tsDesc: ": -",
            employeeId: "",
            employeeName: "",
            employeeEmail: "",
            employeePosition: "",
            EditUser: <%: this.ID %>_editUser,
            saveEngineer: <%: this.ID %>_saveEngineer,
            saveDesigner: <%: this.ID %>_saveDesigner
        });

        function <%: this.ID %>_saveEngineer(e) {
            if (e.model.employeeId == "") {
                _showDialogMessage("Required", "Name is empty", "");
                e.preventDefault();
            } else
                if (e.model.employeePosition == "") {
                    _showDialogMessage("Required", "Position is empty", "");
                    e.preventDefault();
                } else
                    if (e.model.joinedDate == null) {
                        _showDialogMessage("Required", "Join Date is empty", "");
                        e.preventDefault();
                    }
        }
        function <%: this.ID %>_saveDesigner(e) {
            if (e.model.employeeId == "") {
                _showDialogMessage("Required", "Name is empty", "");
                e.preventDefault();
            } else
                if (e.model.employeePosition == "") {
                    _showDialogMessage("Required", "Position is empty", "");
                    e.preventDefault();
                } else
                    if (e.model.joinedDate == null) {
                        _showDialogMessage("Required", "Join Date is empty", "");
                        e.preventDefault();
                    }
        }

        function <%: this.ID %>_change(e) {
            switch (e.action){
                case "add":
                    e.items[0].isActive = true;
                    break;
                case "edit":
                    e.items[0].isActive = true;
                    break;
                default:
            }
        }

        function <%: this.ID %>_editUser(e){
            if (!e.model.isNew()) {
//                $("#< %: this.ID %>_txtemployeeName").attr("disabled", true);
//                  $("#< %: this.ID %>_btnemployeeName").attr("disabled", true);
            }
        }

        function <%: this.ID %>_getEmployee(container, options) {
            var input, btn;
            if (options.model.id == 0) {
                input = $("<input class=' k-textbox' type='text' style='width:150px' disabled/>");
                input.attr("id", "<%: this.ID %>_txt" + options.field);
                input.appendTo(container);
                btn = $("<input type='button' class='k-button' style='min-width:30px' value='...'/>");
                btn.attr("id", "<%: this.ID %>_btn" + options.field);
                btn.appendTo(container);
//            var pickedEmp = (this.dataItem($(container.currentTarget).closest("tr")));
            } else {
                input = $("<div type='text' style='width:150px' disabled/>");
                input.attr("id", "<%: this.ID %>_txt" + options.field);
                input.appendTo(container);
                $("#<%: this.ID %>_txt" + options.field).html(options.model.employeeName);
            }

            $("#<%: this.ID %>_btn" + options.field).on("click", function () {
                var popUpEmployee = $("#popUpEmployee").getKendoWindow();
                popUpEmployee.center().open();

                PickEmployeesTS_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                    var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                    options.model.employeeId = pickedEmployee.id;
                    options.model.employeeName = pickedEmployee.full_Name;
                    options.model.employeeEmail = pickedEmployee.email;
                    options.model.employeePosition = pickedEmployee.positionId;
                    options.model.dirty = true;
                    $("#<%: this.ID %>_txt" + options.field).val(pickedEmployee.full_Name);
                    container.closest("td").next().html(common_getPositionByPositionID(pickedEmployee.positionId));
                    <%: this.ID %>_viewModel.employeeId = pickedEmployee.id;
                    <%: this.ID %>_viewModel.employeeName = pickedEmployee.full_Name;
                    <%: this.ID %>_viewModel.employeeEmail = pickedEmployee.email;
                    <%: this.ID %>_viewModel.employeePosition = pickedEmployee.positionId;

                    if (PickEmployeesTS_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                        $.ajax({
                            url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                            type: "GET",
                            async: false,
                            success: function (result) {
                                if (result.length != 0) {
                                    <%: this.ID %>_viewModel.employeeName = result[0].FULL_NAME;
                                    <%: this.ID %>_viewModel.employeeEmail = result[0].EMAIL;
                                    <%: this.ID %>_viewModel.employeePosition = result[0].POSITIONID;
                                }
                            },
                            error: function (data) {
                            }
                        });
                    }

                    popUpEmployee.close();
                });
                PickEmployeesTS_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                    popUpEmployee.close();
                });
                // show popup
                PickEmployeesTS_DataBind();

            })

        }

        //OnDocumentReady = function () {
        //}

    </script>




