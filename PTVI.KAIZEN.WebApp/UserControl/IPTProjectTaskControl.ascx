﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPTProjectTaskControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.IPTProjectTaskControl" %>
<div id="<%: this.ID  %>_container" >
    <div id="<%: this.ID  %>_treelist"
        data-height="550px"
        data-role="treelist"
        data-filterable="true"
        data-resizable="true"
        data-bind="source: dsProjectTask, events: {dataBound: onDataBound}}"
        data-auto-bind="false"
        data-selectable="row"
        data-columns="[
            { field: 'title', title: 'Task' },
            { field: 'start', title: 'Start Date', format: '{0: dd MMM yyyy  HH:mm}', width:'190px' },
            { field: 'end', title: 'Finish Date', format: '{0: dd MMM yyyy  HH:mm}', width:'190px' },
            { template:  kendo.template($('#<%: this.ID  %>_select-column').html()), width: '100px', filterable: false }
        ]" ></div>
</div>

<script id="<%: this.ID  %>_select-column" type="text/x-kendo-template">
    #if (<%: this.ID %>_vm.isSingleSelect == false) {#
        #if (summary == false && excluded == false) {#
            <input type='checkbox' class='checkbox-selection' onchange='<%: this.ID %>_onCheckBoxSelectionChanged(this)' id='select_#:originalId#' value='#:excluded#' />
        #}#
    #} else {#
        <button data-role="button" data-bind="events:{click:selectButtonClick}">#: <%: this.ID %>_vm.selectButtonText #</button>
    #}#
</script>


<script type="text/javascript">
    var <%: this.ID  %>_vm = kendo.observable({
        projectID: null,
        excludeTaskIDs: [],
        selectButtonClick: null,
        selectButtonText: "Select",
        isSingleSelect: false,
        dsProjectTask: new kendo.data.TreeListDataSource({
            transport: {
                read: {
                    url: function () {
                        return "<%: WebApiUrl %>" + "IPTProjectSchedule/GanttObjectByProjectID?projID=" + <%: this.ID  %>_vm.projectID;
                    }
                }
            },
            schema: {
                parse: function(responses){
                    for (var i = 0; i < responses.length; i++) {
                        var count = Enumerable.From(<%: this.ID  %>_vm.excludeTaskIDs)
                        .Count("$ == '" + responses[i].originalId + "'");

                        responses[i]["excluded"] = count > 0;

                        if (responses[i].percentComplete >= 100)
                            responses[i]["excluded"] = true;

                        responses[i]["selected"] = false;
                    }

                    return responses;
                },
                model: {
                    fields: {
                        end: { field: "end", type: "date" },
                        start: { field: "start", type: "date" }
                    },
                    expanded: true
                }
            }
        }),
        onDataBound: function (e) {
            var dataItems = this.dsProjectTask.data();
            for (var j = 0; j < dataItems.length; j++) {
                var dataItem = dataItems[j];
                var row = $("[data-uid='" + dataItem.uid + "']");

                var td = row.find("td").eq(0);

                $(td).prop("title", dataItem.title);
            }
        }
    });

    function <%: this.ID  %>_populate(projectID, excludeTaskIDs) {
        if (<%: this.ID  %>_vm.get("projectID") == null) {
            kendo.bind($("#<%: this.ID  %>_container"), <%: this.ID  %>_vm);
        }

        <%: this.ID  %>_vm.set("projectID", projectID);
        <%: this.ID  %>_vm.set("excludeTaskIDs", excludeTaskIDs);
            
        <%: this.ID  %>_vm.dsProjectTask.read();
    }

    function <%: this.ID  %>_populateSingleSelect(projectID, selectButtonText, hideColumns, onSelectClick) {
        if (<%: this.ID  %>_vm.get("projectID") == null) {
            kendo.bind($("#<%: this.ID  %>_container"), <%: this.ID  %>_vm);
        }

        <%: this.ID  %>_vm.set("projectID", projectID);
        <%: this.ID  %>_vm.set("isSingleSelect", true);
        <%: this.ID  %>_vm.set("selectButtonText", selectButtonText); 
        <%: this.ID  %>_vm.set("selectButtonClick", onSelectClick); 

        for (var i = 0; i < hideColumns.length; i++)
            $("#<%: this.ID  %>_treelist").getKendoTreeList().hideColumn(hideColumns[i]);

        <%: this.ID  %>_vm.dsProjectTask.read();
    }

    function <%: this.ID  %>_getSelectedTasks() {
        var data = $("#<%: this.ID  %>_treelist").getKendoTreeList().dataSource.data().toJSON();
        var selectedItems = Enumerable.From(data).Where('$.selected == true').ToArray();

        return selectedItems;
    }

    function <%: this.ID %>_onCheckBoxSelectionChanged(chk){
        var dataItem = $("#<%: this.ID  %>_treelist").getKendoTreeList().dataItem(
                        $(chk).closest('tr'));

        dataItem.set("selected", $(chk).prop('checked'));
    }

</script>
