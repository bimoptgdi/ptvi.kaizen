﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OtherDepartmentControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.OtherDepartmentControl" %>
<%@ Register Src="~/UserControl/OtherDepartmentTaskControl.ascx" TagPrefix="uc1" TagName="OtherDepartmentTask" %>
<%@ Register Src="~/UserControl/SendRequestControl.ascx" TagPrefix="uc1" TagName="SendRequest" %>
<%@ Register Src="~/UserControl/RequestListControl.ascx" TagPrefix="uc1" TagName="RequestList" %>
    <div id="OtherDepartmentForm">
        <div class="k-content wide">
            <div>
                <div id="SendRequestTabStrip">
                    <ul>
                        <li>Other Department Task</li>
                        <li>Send Request to Other User</li>
                        <li>Request List</li>
                    </ul>
                    <div>
                        <uc1:OtherDepartmentTask runat="server" ID="OtherDepartmentTask" />
                    </div>
                    <div>
                        <uc1:SendRequest runat="server" ID="SendRequest" />
                    </div>
                    <div>
                        <uc1:RequestList runat="server" ID="RequestList" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        var <%: this.ID %>_OtherDepartmentVM = kendo.observable({
            selectedTabIndex: 0
        });

        var onShow = function (e) {
            SendRequestTabStrip = $("#SendRequestTabStrip").getKendoTabStrip();
            if (SendRequestTabStrip.value() == "Request List") {
                $("#RequestList_grdRequestList").getKendoGrid().dataSource.read();
                $("#RequestList_grdRequestList").getKendoGrid().refresh();

            }
        };

            // Tab control
        var SendRequestTabStrip = $("#SendRequestTabStrip").kendoTabStrip({
                tabPosition: "top",
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
                show: onShow
            });
        SendRequestTabStrip = $("#SendRequestTabStrip").getKendoTabStrip();
        SendRequestTabStrip.select(0);

            // --Tab control
        kendo.bind($("#OtherDepartmentForm"), <%: this.ID %>_OtherDepartmentVM);
//        OnDocumentReady = function () {
//        }

    </script>
