﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EwpDrawingReportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EwpDrawingReportControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
    var <%: this.ID %>_ewpDrawingReportGrid = [], <%: this.ID %>_actualCostFormatData = [], currYear = new Date().getFullYear();
    var <%: this.ID %>_drawingReportModel = kendo.observable ({
        param: currYear,
        listYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            }
        }),
        onChangeParam: function (e) {
            this.dsComplexity.read();
            this.dsTypeOfDrawing.read();
            this.generateGriEwpDrawingReport();
        },
        dsDrawingReport: new kendo.data.DataSource({
            requestStart: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), true);
                kendo.ui.progress($("#EwpDrawingReportForm"), true);
            },
            requestEnd: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), false);
                kendo.ui.progress($("#EwpDrawingReportForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "Report/EwpDrawingReport/" +  <%: this.ID %>_drawingReportModel.param;
                    },
                    async: false
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dsTypeOfDrawing: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#EwpDrawingReportForm"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#EwpDrawingReportForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "masterDesignerDrafting/getTypeNumberOfDrawing/1"
                    },
                    async: false
                }
            },
        }),
        dsComplexity: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#EwpDrawingReportForm"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#EwpDrawingReportForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "masterDesignerComplexity/getByYear/" +  <%: this.ID %>_drawingReportModel.param;
                    },
                    async: false
                }
            },
        }),
        onDataBound: function (e) {
            var data = e.sender._data;
            $.each(data, function (i, row) {
                if (row.level == 0) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#C00000");
                } else if (row.level == 1) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#1F497D");
                } else if (row.level == 2) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#E26B0A");
                }
            });
        },
        dsDrawingReportReformat: function () {
            var dataReformat = [];

            this.dsDrawingReport.read();

            var dataOri = this.dsDrawingReport.data();
            
            //generateColumnGrid
            <%: this.ID %>_ewpDrawingReportGrid = [];
            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "deliverableNo", title: "Project No", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>deliverableNo-template').html()),
                width: 200,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "projectType", title: "CAP/OP", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>projectType-template').html()),
                width: 100,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "deliverableName", title: "Project Name / Designer Deliverable", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>deliverableName-template').html()),
                width: 300,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
                //locked: true
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "pic", title: "Engineer / Designer", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 180,
                //format: "{0:n2}",
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                title: "Plan Date",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [{
                    field: "planStartDate", title: "Start",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                },
                {
                    field: "planFinishDate", title: "Finish",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                }]
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                title: "Actual Date",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [{
                    field: "actualStartDate", title: "Start",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                },
                {
                    field: "actualFinishDate", title: "Finish",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                }]
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "category", title: "Category", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                //template: "#=data.actualCost ? kendo.toString(data.actualCost, 'n2') : '-'#",
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "participation", title: "Participation", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "# if (data.level) { # #=data.participation ? kendo.toString(data.participation, 'n0') : '0'# % # } #",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });


            //loop grid column Type Of Drawing
            if (this.dsTypeOfDrawing.data().length > 0) {
                var columnTypeOfDrawing = [];

                for (var j = 0; j < this.dsTypeOfDrawing.data().length; j++) {
                    columnTypeOfDrawing.push({
                        field: "typeOfDrawing_" + this.dsTypeOfDrawing.data()[j].typeOfDrawing,
                        title: kendo.toString(this.dsTypeOfDrawing.data()[j].typeOfDrawing, "n0"),
                        template: "# if (data.level) { # #=data.totalProduction ? data.typeOfDrawing_" + this.dsTypeOfDrawing.data()[j].typeOfDrawing + " ? data.typeOfDrawing_" + this.dsTypeOfDrawing.data()[j].typeOfDrawing + " : '0' : '' # # } #",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center;"
                        }, width: 50,
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                    });
                }
                <%: this.ID %>_ewpDrawingReportGrid.push({
                    title: "Type Of Drawing",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    columns: columnTypeOfDrawing
                });

            }

            //loop grid column Complexity
            if (this.dsComplexity.data().length > 0) {
                var columnComplexity = [];

                for (var j = 0; j < this.dsComplexity.data().length; j++) {
                    columnComplexity.push({
                        field: "complexity_" + this.dsComplexity.data()[j].id,
                        title: kendo.toString(this.dsComplexity.data()[j].category, "n0"),
                        template: "# if (data.level) { # #=data.totalProduction ? data.complexity_" + this.dsComplexity.data()[j].id + " ? data.complexity_" + this.dsComplexity.data()[j].id + " : '0' : '' # # } #",
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center;"
                        }, width: 170,
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                    });
                }
                <%: this.ID %>_ewpDrawingReportGrid.push({
                    title: "Complexity",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    columns: columnComplexity
                });

            }

            <%: this.ID %>_ewpDrawingReportGrid.push({
                field: "totalProduction", title: "Total Of Production", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "# if (data.level) { # #=data.totalProduction ? kendo.toString(data.totalProduction, 'n2') : '-'# # } #",
                headerTemplate: kendo.template($('#<%: this.ID %>totalProduction-headerTemplate').html()),
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });


            //Generate Data
            for (var i = 0; i < dataOri.length; i++) {
                // new object for insert DataReformat
                var ewpDrawingData = {};

                ewpDrawingData["deliverableNo"] = dataOri[i].deliverableNo;
                <%: this.ID %>_actualCostFormatData["deliverableNo"] = { type: "string" };

                ewpDrawingData["projectType"] = dataOri[i].projectType;
                <%: this.ID %>_actualCostFormatData["projectType"] = { type: "string" };

                ewpDrawingData["deliverableName"] = dataOri[i].deliverableName;
                <%: this.ID %>_actualCostFormatData["deliverableName"] = { type: "string" };

                ewpDrawingData["pic"] = dataOri[i].pic;
                <%: this.ID %>_actualCostFormatData["pic"] = { type: "string" };

                ewpDrawingData["category"] = dataOri[i].category;
                <%: this.ID %>_actualCostFormatData["category"] = { type: "string" };

                ewpDrawingData["planStartDate"] = kendo.parseDate(dataOri[i].planStartDate);
                <%: this.ID %>_actualCostFormatData["planStartDate"] = { type: "date" };

                ewpDrawingData["planFinishDate"] = kendo.parseDate(dataOri[i].planFinishDate);
                <%: this.ID %>_actualCostFormatData["planFinishDate"] = { type: "date" };

                ewpDrawingData["actualStartDate"] = kendo.parseDate(dataOri[i].actualStartDate);
                <%: this.ID %>_actualCostFormatData["actualStartDate"] = { type: "date" };

                ewpDrawingData["actualFinishDate"] = kendo.parseDate(dataOri[i].actualFinishDate);
                <%: this.ID %>_actualCostFormatData["actualFinishDate"] = { type: "date" };

                ewpDrawingData["participation"] = dataOri[i].participation;
                <%: this.ID %>_actualCostFormatData["participation"] = { type: "number" };

                ewpDrawingData["level"] = dataOri[i].level;
                <%: this.ID %>_actualCostFormatData["level"] = { type: "number" };

                //generate type of drawing & complexity
                if (dataOri[i].typeOfDrawing != null) {
                    for (var j = 0; j < dataOri[i].typeOfDrawing.length; j++) {
                        ewpDrawingData["typeOfDrawing_" + dataOri[i].typeOfDrawing[j].typeOfDrawing] = dataOri[i].typeOfDrawing[j].numberOfDrawing;
                    }
                }

                if (dataOri[i].complexity != null) {
                    for (var j = 0; j < dataOri[i].complexity.length; j++) {
                        ewpDrawingData["complexity_" + dataOri[i].complexity[j].masterComplexityDCId] = dataOri[i].complexity[j].complexityValue;
                    }
                }

                ewpDrawingData["totalProduction"] = dataOri[i].totalProduction;
                <%: this.ID %>_actualCostFormatData["totalProduction"] = { type: "number" };

                dataReformat[i] = ewpDrawingData;
            }
            return dataReformat;
         },
         generateGriEwpDrawingReport: function () {
            var that = this;
            var dataGrid = that.dsDrawingReportReformat();
            var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
            var dataSource = new kendo.data.DataSource({
                data: dataGrid,
                schema: {
                    model: {
                        id: "id",
                        fields: <%: this.ID %>_actualCostFormatData
                    }
                },
            });

            if ($(idGrid).getKendoGrid() != null) {
                $(idGrid).getKendoGrid().destroy();
                $(idGrid).empty();
            }

            if ($(idGrid).getKendoGrid() == null) {
                $(idGrid).kendoGrid({
                    toolbar: [{ template: kendo.template($("#toolbarTemplate").html()) }],
                    dataSource: dataSource,
                    dataBound: <%: this.ID %>_drawingReportModel.onDataBound,
                    columns: <%: this.ID %>_ewpDrawingReportGrid,
                    scrollable: true,
                    excelExport: function (e) {
                        var sheet = e.workbook.sheets[0];
                        var rowIndex = 2, cell = 0;
                        var sheet = e.workbook.sheets[0];
                        $.each(e.data, function (key, value) {
                            if (rowIndex > 1) {
                                var row = sheet.rows[rowIndex];

                                row.cells[0].value = value.level == 0 ? row.cells[0].value : lpad('', (value.level * 4), ' ') + row.cells[0].value;
                                row.cells[2].value = value.level == 0 ? row.cells[2].value : lpad('', (value.level * 4), ' ') + row.cells[2].value;

                                if (value.level == 0) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#C00000";
                                        row.cells[cellIndex].bold = true;
                                    }
                                } else if (value.level == 1) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#1F497D";
                                    }
                                } else if (value.level == 2) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#E26B0A";
                                    }
                                }
                            }
                            rowIndex++;
                        });
                        for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                            var row = sheet.rows[rowIndex];
                            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                if (!isNaN(row.cells[cellIndex].value)) {
                                    row.cells[cellIndex].format = "#,##0.00_;"
                                }
                                if (rowIndex == 0 || rowIndex == 1) {
                                    sheet.rows[rowIndex].cells[cellIndex].vAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].hAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].bold = true;
                                }
                            }
                            if (row.type == "header") {
                                row.height = 30;
                            }
                        }

                    }
                });
            }

            $("#exportExcel").on("click", function () {
                var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
                var grid = $(idGrid).getKendoGrid();
                var column = grid.columns;
                $.each(column, function (key, value) {
                    value.title = value.title.replace("</br>", "\r");
                })
                grid.saveAsExcel();
            })

        }
    });
    $(document).ready(function () {
        <%: this.ID %>_drawingReportModel.dsComplexity.read();
        <%: this.ID %>_drawingReportModel.dsTypeOfDrawing.read();
        kendo.bind($("#<%: this.ID %>_EwpDrawingControl"), <%: this.ID %>_drawingReportModel);
        <%: this.ID %>_drawingReportModel.generateGriEwpDrawingReport();
        <%--$("#<%: this.ID %>_gridDrawingReport").data("kendoGrid").resize();--%>
    });
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>deliverableNo-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableNo#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>projectType-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#">#=data.projectType#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableName-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableName#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>totalProduction-headerTemplate">
    <div style="text-align: center;">Total Of<br>Production</div>
</script>

<div id="<%: this.ID %>_EwpDrawingControl">
    <div>
        <span>Year: </span>
        <input data-role="dropdownlist"
               data-placeholder="Select Year"
               data-value-primitive="true"
               data-text-field="text"
               data-value-field="value"
               data-bind=" source: listYear, value: param, events: { change: onChangeParam }"
               style="width: 100px" />
    </div>
    <div id="<%: this.ID %>_grdDrawingReportControl" style="height: 450px"
         data-filterable= "true">
    </div>
</div>