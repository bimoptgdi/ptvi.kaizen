﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewAttachmentControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ViewAttachmentControl" %>
<script>
    var ucViewAttachment_grdAttachment, ucViewAttachment_dsAttachment;

    $(document).ready(function () {

    });

    function ucViewAttachment_init(docType, oid) {
        // Attachment
        ucViewAttachment_dsAttachment = new kendo.data.DataSource({
            transport: {
                read: {
                    url: _webApiUrl + "supportingdocument/findSuppDocByTypeNameAndID/" + (docType + "|" + oid),
                    dataType: "json"
                }
            },
            schema: {
                model: {
                    id: "OID"
                }
            },
            pageSize: 10
        });
        
        if (!ucViewAttachment_grdAttachment) {
            ucViewAttachment_grdAttachment = $("#ucViewAttachment_grdAttachment").kendoGrid({
                dataSource: ucViewAttachment_dsAttachment,
                columns: [
                    {
                        field: "FILENAME", title: "File Name",
                        template: function (dataItem) {
                            var result = "<a href='" + "<%: DocServer %>" + "getdoc.aspx?id=" + dataItem.REFDOCID + "&op=n' target='_blank'>" + dataItem.FILENAME.substring(0, 20) + "</a>";
                            return result;
                        }
                    },
                    {
                        field: "TYPEDETAIL", title: "Type"
                    }
                ],
                editable: {
                    mode: "popup",
                    confirmation: true
                }
            }).data("kendoGrid");
        } else {
            ucViewAttachment_grdAttachment.setDataSource(ucViewAttachment_dsAttachment);
        }
        
    }

    function ucViewAttachment_reset() {
        ucViewAttachment_grdAttachment.dataSource.data([]);
    }
</script>
<div id="ucViewAttachment" class="adrContent">
    <div id="ucViewAttachment_grdAttachment"></div>
</div>