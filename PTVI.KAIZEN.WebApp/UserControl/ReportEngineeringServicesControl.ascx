﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportEngineeringServicesControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportEngineeringServicesControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
    var <%: this.ID %>_ESReportControlGrid = [], <%: this.ID %>_actualCostFormatData = [],
        createDataPie = {},
        currMonth = (new Date().getMonth()),
        currYear = new Date().getFullYear(),
        //currYear = 2017,
        localDataESSource = [],
        startChartPie = 1,
        minMonth = 11;

    var <%: this.ID %>_viewESReportModel = kendo.observable({
        paramYear: currYear,
        paramBadge: "",
        isInvisible: true,
        dataSummaryTable: [],
        dataPieChart: {},
        listYear: new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridEWP"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridDC"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummary"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummaryGraphic"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridEWP"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridDC"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummary"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummaryGraphic"), false);
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            },
            filter: { field: "value", operator: "neq", value: currYear + 1 }
        }),
        onChangeParam: function (e) {
            currMonth = (new Date().getMonth());
            this.dsESReport.read();
            setMonth();
            setHeaderGrid(<%: this.ID %>_viewESReportModel.paramYear, currMonth)
            this.dsSummaryReport.read();
            this.set("isInvisible", this.paramBadge ? false : true);

        },
        dataES: new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    // on success
                    e.success(localDataESSource);
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            },
            schema: {
                model: {
                    id: "monthInt",
                    fields: {
                        year: { type: 'number' },
                        month: { type: 'number' },
                        monthName: { type: 'string' },
                        docId: { type: 'number' },
                        documentBy: { type: 'string' },
                        ewpCapCummPlan: { type: 'number' },
                        ewpOpCummPlan: { type: 'number' },
                        ewpMonthFactor: { type: 'number' },
                        ewpCapPlan: { type: 'number' },
                        ewpOpPlan: { type: 'number' },
                        ewpCapActual: { type: 'number' },
                        ewpOpActual: { type: 'number' },
                        ewpDeliverableWeightHour: { type: 'number' },
                        ewpTotalWeight: { type: 'number' },
                        ewpAvgWeightPlan: { type: 'number' },
                        ewpScorePlan: { type: 'number' },
                        ewpScoreCummPlan: { type: 'number' },
                        ewpNewScorePlan: { type: 'number' },
                        ewpNewScoreCummPlan: { type: 'number' },
                        ewpScoreActual: { type: 'number' },
                        ewpScoreCummActual: { type: 'number' },
                        ewpRatioActual: { type: 'number' },
                        ewpAdjScoreActual: { type: 'number' },
                        ewpAdjScoreCummActual: { type: 'number' },
                        ewpPercentActual: { type: 'number' },
                        ewpMinActual: { type: 'number' },
                        ewpTargetActual: { type: 'number' },
                        ewpMaxActual: { type: 'number' },
                        ewpMaxGraphic: { type: 'number' },
                        ewpCummMaximumGraphic: { type: 'number' },
                        ewpForecastGraphic: { type: 'number' },
                        ewpCummForecastGraphic: { type: 'number' },
                        dcCapCummPlan: { type: 'number' },
                        dcOpCummPlan: { type: 'number' },
                        dcMonthFactor: { type: 'number' },
                        dcCapPlan: { type: 'number' },
                        dcOpPlan: { type: 'number' },
                        dcCapActual: { type: 'number' },
                        dcOpActual: { type: 'number' },
                        dcCarryOver: { type: 'number' },
                        dcDeliverableWeightHour: { type: 'number' },
                        dcTotalWeightPlan: { type: 'number' },
                        dcAvgWeightPlan: { type: 'number' },
                        dcScorePlan: { type: 'number' },
                        dcScoreCummPlan: { type: 'number' },
                        dcNewScorePlan: { type: 'number' },
                        dcNewScoreCummPlan: { type: 'number' },
                        dcScoreActual: { type: 'number' },
                        dcCummActual: { type: 'number' },
                        dcRatioActual: { type: 'number' },
                        dcAdjustedActual: { type: 'number' },
                        dcAdjustedcummActual: { type: 'number' },
                        dcPercentActual: { type: 'number' },
                        dcMinActual: { type: 'number' },
                        dcTargetActual: { type: 'number' },
                        dcMaxActual: { type: 'number' },
                        dcMaxGraphic: { type: 'number' },
                        dcCummMaxGraphic: { type: 'number' },
                        dcForecastGraphic: { type: 'number' },
                        dcCummForecastGraphic: { type: 'number' },

                        summaryWeightEngPlan: { type: 'number' },
                        summaryScorePlan: { type: 'number' },
                        summaryScoreCummPlan: { type: 'number' },
                        summaryMaxPlan: { type: 'number' },
                        summaryMaxCummPlan: { type: 'number' },
                        summaryForecastPlan: { type: 'number' },
                        summaryForecastCummPlan: { type: 'number' },
                        summaryScoreActual: { type: 'number' },
                        summaryScoreCummActual: { type: 'number' },
                        summaryAdjScoreActual: { type: 'number' },
                        summaryAdjScoreCummActual: { type: 'number' },

                        summaryPlanOfTask: { type: 'number' },
                        summaryPlanCummOfTask: { type: 'number' },
                        summaryCompletedOfTask: { type: 'number' },
                        summaryCompletedCummOfTask: { type: 'number' },
                        summaryPercentageOfTask: { type: 'number' },
                        summaryLineOfTask: { type: 'number' }
                    }
                }
            },
            aggregate: [
                { field: "ewpCapPlan", aggregate: "sum" },
                { field: "ewpOpPlan", aggregate: "sum" },
                { field: "ewpCapActual", aggregate: "sum" },
                { field: "ewpOpActual", aggregate: "sum" },
                { field: "ewpTotalWeight", aggregate: "sum" },
                { field: "ewpAvgWeightPlan", aggregate: "sum" },
                { field: "ewpScorePlan", aggregate: "sum" },
                { field: "ewpNewScorePlan", aggregate: "sum" },
                { field: "ewpScoreCummActual", aggregate: "max" },
                { field: "ewpAdjScoreActual", aggregate: "sum" },
                { field: "ewpAdjScoreCummActual", aggregate: "max" },
                { field: "ewpMaxGraphic", aggregate: "sum" },
                { field: "dcCapPlan", aggregate: "sum" },
                { field: "dcOpPlan", aggregate: "sum" },
                { field: "dcCapActual", aggregate: "sum" },
                { field: "dcOpActual", aggregate: "sum" },
                { field: "dcTotalWeight", aggregate: "sum" },
                { field: "dcAvgWeightPlan", aggregate: "sum" },
                { field: "dcScorePlan", aggregate: "sum" },
                { field: "dcNewScorePlan", aggregate: "sum" },
                { field: "dcScoreCummActual", aggregate: "max" },
                { field: "dcAdjScoreActual", aggregate: "sum" },
                { field: "dcAdjScoreCummActual", aggregate: "max" },
                { field: "dcMaxGraphic", aggregate: "sum" },
                { field: "summaryWeightEngPlan", aggregate: "sum" },
                { field: "summaryScorePlan", aggregate: "sum" },
            ]

        }),
        dsESReport: new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridEWP"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridDC"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummary"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummaryGraphic"), true);
            },
            requestEnd: function (e) {
                setMonth();

                localDataESSource = e.response;

                 <%: this.ID %>_viewESReportModel.dataES.read();

                resetDataChartPie();

                if (localDataESSource.length > 0) {
                    //generate data chart EWP
                    startChartPie = 1;
                    for (var i = minMonth; i <= currMonth; i++) {
                        var plan = localDataESSource[i].ewpCapPlan + localDataESSource[i].ewpOpPlan;
                        var completed = localDataESSource[i].ewpCapActual + localDataESSource[i].ewpOpActual;
                        var completedPercent = plan == 0 ? 100 : (100 * completed / plan);
                        var outstanding = (plan - completed) < 0 ? 0 : (plan - completed);
                        var outstandingPercent = (100 * (plan - completed) / (plan == 0 ? 1 : plan));
                        createDataPie =
                            { 
                                monthName: localDataESSource[i].monthName, 
                                isVisible: true,
                                data: [
                                    { 
                                        text: "Completed", 
                                        value: completed,
                                        percent: completedPercent
                                    },
                                    { 
                                        text: "Outstanding", 
                                        value: outstanding,
                                        percent: outstandingPercent
                                    }]
                            };
                        <%: this.ID %>_viewESReportModel.dataPieChart.set("chartPieMonthEWP" + startChartPie++, createDataPie);
                    }

                    //generate Data Design conformity
                    startChartPie = 1;
                    for (var i = minMonth; i <= currMonth; i++) {
                        var plan = localDataESSource[i].dcCapPlan + localDataESSource[i].dcOpPlan;
                        var completed = localDataESSource[i].dcCapActual + localDataESSource[i].dcOpActual;
                        var completedPercent = plan == 0 ? 100 : (100 * completed / plan);
                        var outstanding = (plan - completed) < 0 ? 0 : (plan - completed);
                        var outstandingPercent = (100 * (plan - completed) / (plan == 0 ? 1 : plan));
                        createDataPie =
                            { 
                                monthName: localDataESSource[i].monthName, 
                                isVisible: true,
                                data: [
                                    { 
                                        text: "Completed", 
                                        value: completed,
                                        percent: completedPercent
                                    },
                                    { 
                                        text: "Outstanding", 
                                        value: outstanding,
                                        percent: outstandingPercent
                                    }]
                            };
                        <%: this.ID %>_viewESReportModel.dataPieChart.set("chartPieMonthDC" + startChartPie++, createDataPie);
                    }

                    //generate data chart and table summary                    
                    var summaryMaximum = { desc: "Maximum" }, summaryForecast = { desc: "Forecast" },
                        summaryActual = { desc: "Actual" };
                    $.each(localDataESSource, function (index, val) {
                        summaryMaximum["month" + val.month] = val.summaryMaxPlan ? val.summaryMaxPlan : 0;
                        summaryForecast["month" + val.month] = val.summaryForecastPlan ? val.summaryForecastPlan : 0;
                        summaryActual["month" + val.month] = val.summaryAdjScoreActual ? val.summaryAdjScoreActual : 0;
                    });
                    <%: this.ID %>_viewESReportModel.set("dataSummaryTable", [summaryMaximum, summaryForecast, summaryActual]);

                    <%: this.ID %>_viewESReportModel.set("dataTotalScoreTable", [
                        { no: "1", desc: "EWP Compliance", value: <%: this.ID %>_viewESReportModel.dataES.aggregates().ewpAdjScoreCummActual.max },
                        { no: "2", desc: "Design Conformity", value: <%: this.ID %>_viewESReportModel.dataES.aggregates().dcAdjScoreCummActual.max }]);
                  
                } else {
                    <%: this.ID %>_viewESReportModel.dataPieChart.set("dataSummaryTable", []);
                    <%: this.ID %>_viewESReportModel.set("dataTotalScoreTable", []);                  
                }

                kendo.ui.progress($("#<%: this.ID %>_ESReportGridEWP"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridDC"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummary"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummaryGraphic"), false);
            },
            schema: {
                model: {
                    id: "monthInt",
                    fields: {
                        year: { type: 'number' },
                        month: { type: 'number' },
                        monthName: { type: 'string' },
                        docId: { type: 'number' },
                        documentBy: { type: 'string' },
                        ewpCapCummPlan: { type: 'number' },
                        ewpOpCummPlan: { type: 'number' },
                        ewpMonthFactor: { type: 'number' },
                        ewpCapPlan: { type: 'number' },
                        ewpOpPlan: { type: 'number' },
                        ewpCapActual: { type: 'number' },
                        ewpOpActual: { type: 'number' },
                        ewpDeliverableWeightHour: { type: 'number' },
                        ewpTotalWeight: { type: 'number' },
                        ewpAvgWeightPlan: { type: 'number' },
                        ewpScorePlan: { type: 'number' },
                        ewpScoreCummPlan: { type: 'number' },
                        ewpNewScorePlan: { type: 'number' },
                        ewpNewScoreCummPlan: { type: 'number' },
                        ewpScoreActual: { type: 'number' },
                        ewpScoreCummActual: { type: 'number' },
                        ewpRatioActual: { type: 'number' },
                        ewpAdjScoreActual: { type: 'number' },
                        ewpAdjScoreCummActual: { type: 'number' },
                        ewpPercentActual: { type: 'number' },
                        ewpMinActual: { type: 'number' },
                        ewpTargetActual: { type: 'number' },
                        ewpMaxActual: { type: 'number' },
                        ewpMaxGraphic: { type: 'number' },
                        ewpCummMaximumGraphic: { type: 'number' },
                        ewpForecastGraphic: { type: 'number' },
                        ewpCummForecastGraphic: { type: 'number' },
                        dcCapCummPlan: { type: 'number' },
                        dcOpCummPlan: { type: 'number' },
                        dcMonthFactor: { type: 'number' },
                        dcCapPlan: { type: 'number' },
                        dcOpPlan: { type: 'number' },
                        dcCapActual: { type: 'number' },
                        dcOpActual: { type: 'number' },
                        dcCarryOver: { type: 'number' },
                        dcDeliverableWeightHour: { type: 'number' },
                        dcTotalWeightPlan: { type: 'number' },
                        dcAvgWeightPlan: { type: 'number' },
                        dcScorePlan: { type: 'number' },
                        dcScoreCummPlan: { type: 'number' },
                        dcNewScorePlan: { type: 'number' },
                        dcNewScoreCummPlan: { type: 'number' },
                        dcScoreActual: { type: 'number' },
                        dcCummActual: { type: 'number' },
                        dcRatioActual: { type: 'number' },
                        dcAdjustedActual: { type: 'number' },
                        dcAdjustedcummActual: { type: 'number' },
                        dcPercentActual: { type: 'number' },
                        dcMinActual: { type: 'number' },
                        dcTargetActual: { type: 'number' },
                        dcMaxActual: { type: 'number' },
                        dcMaxGraphic: { type: 'number' },
                        dcCummMaxGraphic: { type: 'number' },
                        dcForecastGraphic: { type: 'number' },
                        dcCummForecastGraphic: { type: 'number' },

                        summaryWeightEngPlan: { type: 'number' },
                        summaryScorePlan: { type: 'number' },
                        summaryScoreCummPlan: { type: 'number' },
                        summaryMaxPlan: { type: 'number' },
                        summaryMaxCummPlan: { type: 'number' },
                        summaryForecastPlan: { type: 'number' },
                        summaryForecastCummPlan: { type: 'number' },
                        summaryScoreActual: { type: 'number' },
                        summaryScoreCummActual: { type: 'number' },
                        summaryAdjScoreActual: { type: 'number' },
                        summaryAdjScoreCummActual: { type: 'number' },

                        summaryPlanOfTask: { type: 'number' },
                        summaryPlanCummOfTask: { type: 'number' },
                        summaryCompletedOfTask: { type: 'number' },
                        summaryCompletedCummOfTask: { type: 'number' },
                        summaryPercentageOfTask: { type: 'number' },
                        summaryLineOfTask: { type: 'number' }
                    }
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "ReportEngineeringService/engineeringServicesReport/" + <%: this.ID %>_viewESReportModel.paramYear;
                    },
                    async: false
                }
            },
            //group: [{ field: "category" }],
            aggregate: [
                { field: "ewpCapPlan", aggregate: "sum" },
                { field: "ewpOpPlan", aggregate: "sum" },
                { field: "ewpCapActual", aggregate: "sum" },
                { field: "ewpOpActual", aggregate: "sum" },
                { field: "ewpTotalWeight", aggregate: "sum" },
                { field: "ewpAvgWeightPlan", aggregate: "sum" },
                { field: "ewpScorePlan", aggregate: "sum" },
                { field: "ewpNewScorePlan", aggregate: "sum" },
                { field: "ewpScoreCummActual", aggregate: "max" },
                { field: "ewpAdjScoreActual", aggregate: "sum" },
                { field: "ewpAdjScoreCummActual", aggregate: "max" },
                { field: "ewpMaxGraphic", aggregate: "sum" },
                { field: "dcCapPlan", aggregate: "sum" },
                { field: "dcOpPlan", aggregate: "sum" },
                { field: "dcCapActual", aggregate: "sum" },
                { field: "dcOpActual", aggregate: "sum" },
                { field: "dcTotalWeight", aggregate: "sum" },
                { field: "dcAvgWeightPlan", aggregate: "sum" },
                { field: "dcScorePlan", aggregate: "sum" },
                { field: "dcNewScorePlan", aggregate: "sum" },
                { field: "dcScoreCummActual", aggregate: "max" },
                { field: "dcAdjScoreActual", aggregate: "sum" },
                { field: "dcAdjScoreCummActual", aggregate: "max" },
                { field: "dcMaxGraphic", aggregate: "sum" },
                { field: "summaryWeightEngPlan", aggregate: "sum" },
                { field: "summaryScorePlan", aggregate: "sum" },
            ]
        }),
        dsSummaryReport: new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridEWP"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridDC"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummary"), true);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummaryGraphic"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridEWP"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridDC"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummary"), false);
                kendo.ui.progress($("#<%: this.ID %>_ESReportGridSummaryGraphic"), false);
            },
            schema: {
                model: {
                    id: "projectType",
                    fields: {
                        ewpNumberOfProject: { type: 'number' },
                        ewpNumberOfPlan: { type: 'number' },
                        ewpNumberOfPlanByMonth: { type: 'number' },
                        ewpNumberOfActualByMonth: { type: 'number' },
                        ewpPercentageCompleteByMonth: { type: 'number' },
                        ewpPercentageComplete: { type: 'number' },
                        dcNumberOfProject: { type: 'number' },
                        dcNumberOfPlan: { type: 'number' },
                        dcNumberOfPlanByMonth: { type: 'number' },
                        dcNumberOfActualByMonth: { type: 'number' },
                        dcPercentageCompleteByMonth: { type: 'number' },
                        dcPercentageComplete: { type: 'number' }
                    }
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "ReportEngineeringService/summaryReport/" + <%: this.ID %>_viewESReportModel.paramYear + "?month=" + currMonth;
                    },
                    async: false
                }
            },
            //group: [{ field: "category" }],
            aggregate: [
                { field: "ewpNumberOfProject", aggregate: "sum" },
                { field: "ewpNumberOfPlan", aggregate: "sum" },
                { field: "ewpNumberOfPlanByMonth", aggregate: "sum" },
                { field: "ewpNumberOfActualByMonth", aggregate: "sum" },
                { field: "dcNumberOfProject", aggregate: "sum" },
                { field: "dcNumberOfPlan", aggregate: "sum" },
                { field: "dcNumberOfPlanByMonth", aggregate: "sum" },
                { field: "dcNumberOfActualByMonth", aggregate: "sum" }
            ]
        }),
        toggleSeriesChart: function (e) {
            e.preventDefault();
            var visual = e.visual;
            var opacity = e.show ? 0.8 : 1;
            visual.opacity(opacity);
        },
        visualSeriesChart: function (e) {
            return createColumn(e.rect, e.options.color);
        }
    });

    function resetDataChartPie() {
        startChartPie = 1;
        for (var i = 0; i <= 2; i++) {
            createDataPie =
                {
                    monthName: "-",
                    isVisible: false,
                    data: [
                        {
                            text: "Completed",
                            value: 0,
                            percent: 0
                        },
                        {
                            text: "Outstanding",
                            value: 0,
                            percent: 0
                        }]
                };
            <%: this.ID %>_viewESReportModel.dataPieChart.set("chartPieMonthEWP" + startChartPie++, createDataPie);
        }
        startChartPie = 1;
        for (var i = 0; i <= 2; i++) {
            createDataPie =
                {
                    monthName: "-",
                    isVisible: false,
                    data: [
                        {
                            text: "Completed",
                            value: 0,
                            percent: 0
                        },
                        {
                            text: "Outstanding",
                            value: 0,
                            percent: 0
                        }]
                };
            <%: this.ID %>_viewESReportModel.dataPieChart.set("chartPieMonthDC" + startChartPie++, createDataPie);
        }
    }

    function setHeaderGrid(valYear, valMonth) {
        var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
        var d = new Date(valYear, valMonth + 1, 0);
        $("span[name='span_ParamYear']").text(valYear)
        $("span[name='span_ParamMonth']").text(d.getDate() + ' ' + months[valMonth])

    }

    function setMonth() {
        minMonth = 9;
        if (<%: this.ID %>_viewESReportModel.paramYear == currYear) {
            minMonth = currMonth > 1 ? currMonth - 2 : 0;
        } else if (<%: this.ID %>_viewESReportModel.paramYear > currYear) {
            minMonth = 0;
            currMonth = -1;
        } else {
            currMonth = 11;
        }
    };

    $(document).ready(function () {
        resetDataChartPie();
        kendo.bind($("#<%: this.ID %>_ESReportControl"), <%: this.ID %>_viewESReportModel);
        setHeaderGrid(<%: this.ID %>_viewESReportModel.paramYear, currMonth)
        <%: this.ID %>_viewESReportModel.dsESReport.read();
        //$("#<%: this.ID %>_gridReport").data("kendoGrid").resize();
    });

</script>
<div id="<%: this.ID %>_ESReportControl">
    <div>
        <span>Year: </span>
        <input data-role="dropdownlist"
        data-placeholder="Select Deliverable"
        data-value-primitive="true"
        data-text-field="text"
        data-value-field="value"
        data-bind="source: listYear, value: paramYear, events: { change: onChangeParam }"
        style="width: 100px" />
    </div>
    <br />
    <div  class="k-content wide">
        <div data-role="tabstrip">
            <ul>
                <li class="k-state-active">EWP Compliance</li>
                <li>Design Conformity</li>
                <li>Summary Score and Task</li>
                <li>EWP Compliance Summary & DC Summary</li>
            </ul>

            <div style="background-color: #fff !important;">
                <div id="<%: this.ID %>_ESReportGridEWP" class="gridReport" style="height: 400px; width: 99% !important;" 
                    data-role="grid" 
                    data-bind="source: dataES" <%--, invisible: isInvisible--%>
                    <%--data-scrollable="false"--%>
                    data-no-records="true"
                    data-filterable= "false"
                    data-columns="[
                        { 
                            field: 'monthName', width: 80, locked: true, lockable: false,
                            headerTemplate: kendo.template($('#<%: this.ID %>_monthYear-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, 
                            footerTemplate: 'Total'
                        },
                        { 
                            title: 'EWP Compliance', headerAttributes: {style: 'text-align: center'}, columns: [
                            { 
                                field: 'ewpMonthFactor', headerTemplate: 'Month Factor', width: 65, format: '{0:n2}',
                                headerTemplate: kendo.template($('#<%: this.ID %>_ewpMonthFactor-headerTemplate').html()),
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                title: 'Plan EWP', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'ewpCapPlan', title: 'Capital', width: 65, format: '{0:n0}', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpCapPlan ? ewpCapPlan.sum : 0, \'n0\') #'
                                },
                                { 
                                    field: 'ewpOpPlan', title: 'Operating', width: 65, format: '{0:n0}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpOpPlan ? ewpOpPlan.sum : 0, \'n0\') #'
                                }
                                ]
                            },
                            { 
                                title: 'Actual EWP', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'ewpCapActual', title: 'Capital', width: 65, format: '{0:n0}', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpCapActual ? ewpCapActual.sum : 0, \'n0\') #'
                                },
                                { 
                                    field: 'ewpOpActual', title: 'Operating', width: 65, format: '{0:n0}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpOpActual ? ewpOpActual.sum : 0, \'n0\') #'
                                }
                                ]
                            },
                            { 
                                field: 'ewpTotalWeight', title: 'Total Weight', width: 65, 
                                template: '#:kendo.toString(data.ewpTotalWeight ? data.ewpTotalWeight : 0, \'n1\')#%', 
                                headerTemplate: kendo.template($('#<%: this.ID %>_ewpTotalWeight-headerTemplate').html()),
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpTotalWeight ? ewpTotalWeight.sum : 0, \'n1\') #%'
                            },
                            { 
                                title: 'Plan Score', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'ewpAvgWeightPlan', title: 'Avg Weight', width: 75, 
                                    template: '#:kendo.toString(data.ewpAvgWeightPlan ? data.ewpAvgWeightPlan : 0, \'n2\')#%', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpAvgWeightPlan ? ewpAvgWeightPlan.sum : 0, \'n2\') #%'
                                },
                                { 
                                    field: 'ewpScorePlan', title: 'Score', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpScorePlan ? ewpScorePlan.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'ewpScoreCummPlan', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                                },
                                { 
                                    field: 'ewpNewScorePlan', title: 'New Score', width: 70, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNewScorePlan ? ewpNewScorePlan.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'ewpNewScoreCummPlan', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                }
                                ]
                            },
                            { 
                                title: 'Actual Score', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'ewpScoreActual', title: 'Score', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpScoreCummActual', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['max'], footerTemplate: '#: kendo.toString(data.ewpScoreCummActual ? ewpScoreCummActual.max : 0, \'n2\') #'
                                },
                                { 
                                    field: 'ewpRatioActual', title: 'Ratio', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpAdjScoreActual', title: 'Adjusted', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpAdjScoreActual ? ewpAdjScoreActual.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'ewpAdjScoreCummActual', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['max'], footerTemplate: '#: kendo.toString(data.ewpAdjScoreCummActual ? ewpAdjScoreCummActual.max : 0, \'n2\') #'
                                },
                                { 
                                    field: 'ewpPercentActual', title: '%', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpMinActual', title: 'Min', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpTargetActual', title: 'Target', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpMaxActual', title: 'Max', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                ]
                            },
                            { 
                                title: 'GRAPHIC', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'ewpMaxGraphic', title: 'Max', width: 65, format: '{0:n2}', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpMaxGraphic ? ewpMaxGraphic.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'ewpCummMaxGraphic', title: 'cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpForecastGraphic', title: 'Forecast', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'ewpCummForecastGraphic', title: 'cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                }]
                            }]
                        }
                        ]">
                </div>
            
                <div>
                    <div style="width: 49%; display: inline-block"
                        data-role="chart"
                        data-title ="EWP Compliance"
                        data-legend="{ position: 'bottom', item: { visual: createLegendItem  }}"
                        data-bind="source: dataES"
                        data-panes: "[{ clip: false }]"
                        data-value-axis= "[{
                                name: 'barChart',
                                title: { text: ' ' }
                            },
                            {
                                name: 'lineChart',
                                position: 'right',
                                title: { text: 'Percent Actual' },
                                color: '#ec5e0a'
                            }]"
                        data-category-axis="{field: 'monthName', labels:{ rotation: 315 }, majorGridLines: { visible: false }, line: { visible: false }, axisCrossingValues: [0, 20] }"
                        data-tooltip= "{ visible: true, template: '#= data.series.name + \': \' + (data.series.axis == \'lineChart\' ? kendo.toString(data.value, \'n2\') : data.value)#' }" 
                        data-series="[  
                            { type:'column', field: 'ewpCapPlan', name:'Plan Capital', stack: 'Capital', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'column', field: 'ewpOpPlan', name:'Plan Operating', stack: 'Capital', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'column', field: 'ewpCapActual', name:'Actual Capital', stack: 'Operating', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'column', field: 'ewpOpActual', name:'Actual Operating', stack: 'Operating', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'line', style: 'smooth', field: 'ewpPercentActual', name:'Percent Actual', style:'smooth', axis: 'lineChart', color: '#ff1c1c' }, 
                            { type:'line', style: 'smooth', field: 'ewpTargetActual', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}, 
                            { type:'line', style: 'smooth', field: 'ewpMaxActual', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}, 
                            { type:'line', style: 'smooth', field: 'ewpMinActual', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}
                        ]"
                     >
                    </div>
                    <div style="width: 15%; display: inline-block; text-align: center; vertical-align: top;">
                        <div style="padding-top: 15px;">
                            <span data-bind="visible: dataPieChart.chartPieMonthEWP1.isVisible, text: dataPieChart.chartPieMonthEWP1.monthName"></span>
                        </div>
                        <div
                            data-role="chart"
                            data-legend="{ position: 'top' }"
                            data-bind="visible: dataPieChart.chartPieMonthEWP1.isVisible, source: dataPieChart.chartPieMonthEWP1.data"
                            data-tooltip= "{ visible: true, template: '#=dataItem.text + \': \' + kendo.toString(dataItem.percent, \'n2\')#%' }" 
                            data-series="[  
                                { type:'pie', field: 'value', categoryField:'text' }
                            ]"
                            style="height: 250px;"
                         >
                        </div>
                    </div>
                    <div style="width: 15%; display: inline-block; text-align: center; vertical-align: top;">
                        <div style="padding-top: 15px;">
                            <span data-bind="visible: dataPieChart.chartPieMonthEWP2.isVisible, text: dataPieChart.chartPieMonthEWP2.monthName"></span>
                        </div>
                        <div 
                            data-role="chart"
                            data-legend="{ position: 'top' }"
                            data-bind="visible: dataPieChart.chartPieMonthEWP2.isVisible, source: dataPieChart.chartPieMonthEWP2.data"
                            data-tooltip= "{ visible: true, template: '#=dataItem.text + \': \' + kendo.toString(dataItem.percent, \'n2\')#%' }" 
                            data-series="[  
                                { type:'pie', field: 'value', categoryField:'text' }
                            ]"
                            style="height: 250px;"
                         >
                        </div>
                    </div>
                    <div style="width: 15%; display: inline-block; text-align: center; vertical-align: top;">
                        <div style="padding-top: 15px;">
                            <span data-bind="visible: dataPieChart.chartPieMonthEWP3.isVisible, text: dataPieChart.chartPieMonthEWP3.monthName"></span>
                        </div>
                        <div
                            data-role="chart"
                            data-legend="{ position: 'top' }"
                            data-bind="visible: dataPieChart.chartPieMonthEWP3.isVisible, source: dataPieChart.chartPieMonthEWP3.data"
                            data-tooltip= "{ visible: true, template: '#=dataItem.text + \': \' + kendo.toString(dataItem.percent, \'n2\')#%' }" 
                            data-series="[  
                                { type:'pie', field: 'value', categoryField:'text' }
                            ]"
                            style="height: 250px;"
                         >
                        </div>
                    </div>
                </div>
            </div>
            <div style="background-color: #fff !important;">
                <div id="<%: this.ID %>_ESReportGridDC" class="gridReport" style="height: 400px; width: 99% !important;" 
                    data-role="grid" 
                    data-bind="source: dataES" <%--, invisible: isInvisible--%>
                    <%--data-scrollable="false"--%>
                    data-no-records="true"
                    data-filterable= "false"
                    data-columns="[
                        {
                            field: 'monthName', width: 80, locked: true, lockable: false,
                            headerTemplate: kendo.template($('#<%: this.ID %>_monthYear-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, 
                            footerTemplate: 'Total'
                        },
                        { 
                            title: 'Design Conformity', headerAttributes: {style: 'text-align: center'}, columns: [
                            { 
                                field: 'dcMonthFactor', headerTemplate: 'Month Factor', width: 65, format: '{0:n2}',
                                headerTemplate: kendo.template($('#<%: this.ID %>_ewpMonthFactor-headerTemplate').html()),
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                title: 'Plan DC', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'dcCapPlan', title: 'Capital', width: 65, format: '{0:n0}', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcCapPlan ? dcCapPlan.sum : 0, \'n0\') #'
                                },
                                { 
                                    field: 'dcOpPlan', title: 'Operating', width: 65, format: '{0:n0}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcOpPlan ? dcOpPlan.sum : 0, \'n0\') #'
                                }
                                ]
                            },
                            { 
                                title: 'Actual DC', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'dcCapActual', title: 'Capital', width: 65, format: '{0:n0}', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcCapActual ? dcCapActual.sum : 0, \'n0\') #'
                                },
                                { 
                                    field: 'dcOpActual', title: 'Operating', width: 65, format: '{0:n0}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcOpActual ? dcOpActual.sum : 0, \'n0\') #'
                                }
                                ]
                            },
                            { 
                                field: 'dcTotalWeight', title: 'Total Weight', width: 65, 
                                template: '#:kendo.toString(data.dcTotalWeight ? data.dcTotalWeight : 0, \'n1\')#%', 
                                headerTemplate: kendo.template($('#<%: this.ID %>_ewpTotalWeight-headerTemplate').html()),
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcTotalWeight ? dcTotalWeight.sum : 0, \'n1\') #%'
                            },
                            { 
                                title: 'Plan Score', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'dcAvgWeightPlan', title: 'Avg Weight', width: 75, 
                                    template: '#:kendo.toString(data.dcAvgWeightPlan ? data.dcAvgWeightPlan : 0, \'n2\')#%', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcAvgWeightPlan ? dcAvgWeightPlan.sum : 0, \'n2\') #%'
                                },
                                { 
                                    field: 'dcScorePlan', title: 'Score', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcScorePlan ? dcScorePlan.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'dcScoreCummPlan', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                                },
                                { 
                                    field: 'dcNewScorePlan', title: 'New Score', width: 75, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNewScorePlan ? dcNewScorePlan.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'dcNewScoreCummPlan', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                }
                                ]
                            },
                            { 
                                title: 'Actual Score', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'dcScoreActual', title: 'Score', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcScoreCummActual', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['max'], footerTemplate: '#: kendo.toString(data.dcScoreCummActual ? dcScoreCummActual.max : 0, \'n2\') #'
                                },
                                { 
                                    field: 'dcRatioActual', title: 'Ratio', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcAdjScoreActual', title: 'Adjusted', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcAdjScoreActual ? dcAdjScoreActual.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'dcAdjScoreCummActual', title: 'Cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['max'], footerTemplate: '#: kendo.toString(data.dcAdjScoreCummActual ? dcAdjScoreCummActual.max : 0, \'n2\') #'
                                },
                                { 
                                    field: 'dcPercentActual', title: '%', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcMinActual', title: 'Min', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcTargetActual', title: 'Target', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcMaxActual', title: 'Max', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                ]
                            },
                            { 
                                title: 'GRAPHIC', headerAttributes: {style: 'text-align: center'}, columns: [
                                { 
                                    field: 'dcMaxGraphic', title: 'Max', width: 65, format: '{0:n2}', 
                                    headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                    aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcMaxGraphic ? dcMaxGraphic.sum : 0, \'n2\') #'
                                },
                                { 
                                    field: 'dcCummMaxGraphic', title: 'cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcForecastGraphic', title: 'Forecast', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                },
                                { 
                                    field: 'dcCummForecastGraphic', title: 'cumm', width: 65, format: '{0:n2}',
                                    headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                                }]
                            }]
                        }
                        ]">
                </div>

                <div>
                    <div style="width: 49%; display: inline-block"
                        data-role="chart"
                        data-title ="DESIGN CONFORMITY"
                        data-legend="{ position: 'bottom', item: { visual: createLegendItem  }}"
                        data-bind="source: dataES"
                        data-panes: "[{ clip: false }]"
                        data-value-axis= "[{
                                name: 'barChart',
                                title: { text: ' ' }
                            },
                            {
                                name: 'lineChart',
                                position: 'right',
                                title: { text: 'Percent Actual' },
                                color: '#ec5e0a'
                            }]"
                        data-category-axis="{field: 'monthName', labels:{ rotation: 315 }, majorGridLines: { visible: false }, line: { visible: false }, axisCrossingValues: [0, 20] }"
                        data-tooltip= "{ visible: true, template: '#= data.series.name + \': \' + (data.series.axis == \'lineChart\' ? kendo.toString(data.value, \'n2\') : data.value)#' }" 
                        data-series="[  
                            { type:'column', field: 'dcCapPlan', name:'Plan Capital', stack: 'Capital', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'column', field: 'dcOpPlan', name:'Plan Operating', stack: 'Capital', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'column', field: 'dcCapActual', name:'Actual Capital', stack: 'Operating', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'column', field: 'dcOpActual', name:'Actual Operating', stack: 'Operating', axis: 'barChart', highlight: { toggle: <%: this.ID %>_viewESReportModel.toggleSeriesChart }, visual: <%: this.ID %>_viewESReportModel.visualSeriesChart }, 
                            { type:'line', style: 'smooth', field: 'dcPercentActual', name:'Percent Actual', style:'smooth', axis: 'lineChart', color: '#ff1c1c' }, 
                            { type:'line', style: 'smooth', field: 'dcTargetActual', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}, 
                            { type:'line', style: 'smooth', field: 'dcMaxActual', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}, 
                            { type:'line', style: 'smooth', field: 'dcMinActual', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}
                        ]"
                     >
                    </div>
                    <div style="width: 15%; display: inline-block; text-align: center; vertical-align: top;">
                        <div style="padding-top: 15px;">
                            <span data-bind="visible: dataPieChart.chartPieMonthDC1.isVisible, text: dataPieChart.chartPieMonthDC1.monthName"></span>
                        </div>
                        <div
                            data-role="chart"
                            data-legend="{ position: 'top' }"
                            data-bind="visible: dataPieChart.chartPieMonthDC1.isVisible, source: dataPieChart.chartPieMonthDC1.data"
                            data-tooltip= "{ visible: true, template: '#=dataItem.text + \': \' + kendo.toString(dataItem.percent, \'n2\')#%' }" 
                            data-series="[  
                                { type:'pie', field: 'value', categoryField:'text' }
                            ]"
                            style="height: 250px;"
                         >
                        </div>
                    </div>
                    <div style="width: 15%; display: inline-block; text-align: center; vertical-align: top;">
                        <div style="padding-top: 15px;">
                            <span data-bind="visible: dataPieChart.chartPieMonthDC2.isVisible, text: dataPieChart.chartPieMonthDC2.monthName"></span>
                        </div>
                        <div 
                            data-role="chart"
                            data-legend="{ position: 'top' }"
                            data-bind="visible: dataPieChart.chartPieMonthDC2.isVisible, source: dataPieChart.chartPieMonthDC2.data"
                            data-tooltip= "{ visible: true, template: '#=dataItem.text + \': \' + kendo.toString(dataItem.percent, \'n2\')#%' }" 
                            data-series="[  
                                { type:'pie', field: 'value', categoryField:'text' }
                            ]"
                            style="height: 250px;"
                         >
                        </div>
                    </div>
                    <div style="width: 15%; display: inline-block; text-align: center; vertical-align: top;">
                        <div style="padding-top: 15px;">
                            <span data-bind="visible: dataPieChart.chartPieMonthDC3.isVisible, text: dataPieChart.chartPieMonthDC3.monthName"></span>
                        </div>
                        <div
                            data-role="chart"
                            data-legend="{ position: 'top' }"
                            data-bind="visible: dataPieChart.chartPieMonthDC3.isVisible, source: dataPieChart.chartPieMonthDC3.data"
                            data-tooltip= "{ visible: true, template: '#=dataItem.text + \': \' + kendo.toString(dataItem.percent, \'n2\')#%' }" 
                            data-series="[  
                                { type:'pie', field: 'value', categoryField:'text' }
                            ]"
                            style="height: 250px;"
                         >
                        </div>
                    </div>
                </div>
            </div>
            <div style="background-color: #fff !important;">
                <div>
                    <span style="font-weight: bold;">SUMMARY - SCORE (<span name="span_ParamYear"></span>)</span>
                </div>
                <div id="<%: this.ID %>_ESReportGridSummary" class="gridReport" style="width: 99% !important;"
                    data-role="grid" 
                    data-bind="source: dataES" <%--, invisible: isInvisible--%>
                    data-scrollable="false"
                    data-no-records="true"
                    data-filterable= "false"
                    data-columns="[
                        {
                            field: 'monthName', title: 'Month', width: 100, lockable: false,
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport1'},
                            footerTemplate: 'Total'
                        },
                        { 
                            title: 'Plan Score', headerAttributes: {style: 'text-align: center'}, columns: [
                            { 
                                field: 'summaryWeightEngPlan', title: 'Weight/Eng', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                template: '#:kendo.toString(data.summaryWeightEngPlan ? data.summaryWeightEngPlan : 0, \'n2\')#%', 
                                aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.summaryWeightEngPlan ? summaryWeightEngPlan.sum : 0, \'n2\') #%'
                            },
                            { 
                                field: 'summaryScorePlan', title: 'Score', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                                aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.summaryScorePlan ? summaryScorePlan.sum : 0, \'n2\') #'
                            },
                            { 
                                field: 'summaryScoreCummPlan', title: 'Cumm', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryMaxPlan', title: 'Max', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryMaxCummPlan', title: 'Cumm Max', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryForecastPlan', title: 'Forecast', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryForecastCummPlan', title: 'Cumm Forecast', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            }]
                        },
                        { 
                            title: 'Actual Score', headerAttributes: {style: 'text-align: center'}, columns: [
                            { 
                                field: 'summaryScoreActual', title: 'Score', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryScoreCummActual', title: 'Cumm', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryAdjScoreActual', title: 'Adj Score', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryAdjScoreCummActual', title: 'Cumm', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}
                            }]
                        }
                        ]">
                </div>
                <br />
                <br />
                <div>
                    <span style="font-weight: bold;">SUMMARY - GRAPHIC (<span name="span_ParamYear"></span>)</span>
                </div>
                <div id="<%: this.ID %>_ESReportGridSummaryGraphic" class="gridReport" style="width: 99% !important;"
                    data-role="grid"    
                    data-bind="source: dataES" <%--, invisible: isInvisible--%>
                    data-scrollable="false"
                    data-no-records="true"
                    data-filterable= "false"
                    data-columns="[
                        {
                            field: 'monthName', title: 'Month', width: 100, lockable: false,
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport1'},
                        },
                        { 
                            title: 'NUMBER OF TASK', headerAttributes: {style: 'text-align: center'}, columns: [
                            { 
                                field: 'summaryPlanOfTask', title: 'PLAN', width: 75, format: '{0:n0}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryPlanCummOfTask', title: 'CUMM', width: 75, format: '{0:n0}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryCompletedOfTask', title: 'COMPLETED', width: 75, format: '{0:n0}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'summaryCompletedCummOfTask', title: 'CUMM', width: 75, format: '{0:n0}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                title: 'PERCENTAGE', width: 75,
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'},
                                template: '#: data.summaryPercentageOfTask != null ? kendo.toString(data.summaryPercentageOfTask, \'n2\') + \'%\' : \'-\' #'
                            },
                            { 
                                title: 'LINE', width: 75,
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'},
                                template: '#: data.summaryLineOfTask != null ? kendo.toString(data.summaryLineOfTask, \'n2\') + \'%\' : \'-\' #'
                            }]
                        }
                        ]">
                </div>
                <br />
                <br />
                <div style="width: 960px !important;">
                    <div style="width: 895px; float:right; display: inline-block"
                        data-role="chart"
                        data-title ="SUMMARY"
                        data-legend="{ visible: false }"
                        data-bind="source: dataES"
                        data-panes: "[{ clip: false }]"
                        data-value-axis= "[
                            {
                                name: 'lineChart',
                                position: 'right',
                                color: '#ec5e0a'
                            }]"
                        data-tooltip= "{ visible: true, template: '#= data.series.name + \': \' + (data.series.axis == \'lineChart\' ? kendo.toString(data.value, \'n2\') : data.value)#' }" 
                        data-series="[  
                            { type:'line', style: 'smooth', field: 'summaryPercentageOfTask', name:'Percent Actual', style:'smooth', axis: 'lineChart', color: '#ff1c1c' }, 
                            { type:'line', style: 'smooth', field: 'summaryLineOfTask', style:'smooth', axis: 'lineChart', dashType: 'dash', tooltip: { visible: false }, markers: { visible: false }}
                        ]"
                     >
                    </div>
                </div>
                <div style="width: 950px !important;">
                    <div class="gridReport" 
                        data-role="grid"    
                        data-bind="source: dataSummaryTable" <%--, invisible: isInvisible--%>
                        data-scrollable="false"
                        data-no-records="true"
                        data-filterable= "false"
                        data-columns="[
                            {
                                field: 'desc', title: ' ', width: 100, lockable: false,
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport1'},
                            },
                            { 
                                field: 'month1', title: 'Jan', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month2', title: 'Feb', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month3', title: 'Mar', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month4', title: 'Apr', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month5', title: 'Mei', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month6', title: 'Jun', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month7', title: 'Jul', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month8', title: 'Aug', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month9', title: 'Sep', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month10', title: 'Oct', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month11', title: 'Nov', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            },
                            { 
                                field: 'month12', title: 'Dec', width: 75, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            }
                            ]">
                    </div>
                    <div class="gridReport" 
                        data-role="grid"    
                        data-bind="source: dataTotalScoreTable" <%--, invisible: isInvisible--%>
                        data-scrollable="false"
                        data-no-records="true"
                        data-filterable= "false"
                        data-columns="[
                            {
                                field: 'no', title: 'NO', width: 100, 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport1'},
                            },
                            { 
                                field: 'desc', title: 'DESCRIPTION', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {style: 'text-align: left;'}
                            },
                            { 
                                field: 'value', title: 'SCORE', width: 150, format: '{0:n2}', 
                                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport2'}
                            }
                            ]">
                    </div>
                </div>
            </div>
            <div style="background-color: #fff !important; overflow: hidden;">
                <div>
                    <span style="font-weight: bold;">EWP COMPLIANCE</span>
                </div>    
                <div class="gridReport" style="width: 99% !important;"
                    data-role="grid"    
                    data-bind="source: dsSummaryReport" <%--, invisible: isInvisible--%>
                    data-scrollable="false"
                    data-no-records="true"
                    data-filterable= "false"
                    data-columns="[
                        {
                            field: 'projectType', title: 'PROJECT TYPE', width: 100, 
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: right'}, attributes: {class: 'columnReport1'},
                            footerTemplate: 'Total'
                        },
                        { 
                            field: 'ewpNumberOfProject',  width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_NumberOfProject-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNumberOfProject ? ewpNumberOfProject.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'ewpNumberOfPlan', width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_NumberOfPlanTask-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNumberOfPlan ? ewpNumberOfPlan.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'ewpNumberOfPlanByMonth', width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_TotalTaskMonth-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNumberOfPlanByMonth ? ewpNumberOfPlanByMonth.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'ewpNumberOfActualByMonth', width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_TotalCompletedMonth-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNumberOfActualByMonth ? ewpNumberOfActualByMonth.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'ewpPercentageCompleteByMonth', width: 150, format: '{0:n2}', template: '#: kendo.toString(data.ewpPercentageCompleteByMonth ? data.ewpPercentageCompleteByMonth : 0, \'n2\') #%',
                            headerTemplate: kendo.template($('#<%: this.ID %>_PercentageCompletedMonth-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNumberOfPlanByMonth ? ewpNumberOfPlanByMonth.sum ? 100 * (ewpNumberOfActualByMonth.sum / ewpNumberOfPlanByMonth.sum) : 0 : 0, \'n2\') #%'
                        },
                        { 
                            field: 'ewpPercentageComplete', width: 150, format: '{0:n2}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_PercentageCompleted-headerTemplate').html()), template: '#: kendo.toString(data.ewpPercentageComplete ? data.ewpPercentageComplete : 0, \'n2\') #%',
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.ewpNumberOfPlanByMonth ? ewpNumberOfPlan.sum ? 100 * (ewpNumberOfActualByMonth.sum / ewpNumberOfPlan.sum) : 0 : 0, \'n2\') #%'
                        }
                        ]">
                </div>
                <br />
                <br />
                <div>
                    <span style="font-weight: bold;">DESIGN CONFORMITY</span>
                </div>    
                <div class="gridReport" style="width: 99% !important;"
                    data-role="grid"    
                    data-bind="source: dsSummaryReport" <%--, invisible: isInvisible--%>
                    data-scrollable="false"
                    data-no-records="true"
                    data-filterable= "false"
                    data-columns="[
                        {
                            field: 'projectType', title: 'PROJECT TYPE', width: 100, 
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: right'}, attributes: {class: 'columnReport1'},
                            footerTemplate: 'Total'
                        },
                        { 
                            field: 'dcNumberOfProject',  width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_NumberOfProject-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNumberOfProject ? dcNumberOfProject.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'dcNumberOfPlan', width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_NumberOfPlanTask-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNumberOfPlan ? dcNumberOfPlan.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'dcNumberOfPlanByMonth', width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_TotalTaskMonth-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNumberOfPlanByMonth ? dcNumberOfPlanByMonth.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'dcNumberOfActualByMonth', width: 150, format: '{0:n0}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_TotalCompletedMonth-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNumberOfActualByMonth ? dcNumberOfActualByMonth.sum : 0, \'n0\') #'
                        },
                        { 
                            field: 'dcPercentageCompleteByMonth', width: 150, format: '{0:n2}', template: '#: kendo.toString(data.dcPercentageCompleteByMonth ? data.dcPercentageCompleteByMonth : 0, \'n2\') #%',
                            headerTemplate: kendo.template($('#<%: this.ID %>_PercentageCompletedMonth-headerTemplate').html()),
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNumberOfPlanByMonth ? dcNumberOfPlanByMonth.sum ? 100 * (dcNumberOfActualByMonth.sum / dcNumberOfPlanByMonth.sum) : 0 : 0, \'n2\') #%'
                        },
                        { 
                            field: 'dcPercentageComplete', width: 150, format: '{0:n2}', 
                            headerTemplate: kendo.template($('#<%: this.ID %>_PercentageCompleted-headerTemplate').html()), template: '#: kendo.toString(data.dcPercentageComplete ? data.dcPercentageComplete : 0, \'n2\') #%',
                            headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                            aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.dcNumberOfPlanByMonth ? dcNumberOfPlan.sum ? 100 * (dcNumberOfActualByMonth.sum / dcNumberOfPlan.sum) : 0 : 0, \'n2\') #%'
                        }
                        ]">
                </div>
            </div>
        </div>
    </div>
    <script type="text/x-kendo-template" id="<%: this.ID %>_monthYear-headerTemplate">
        <div style="text-align: center;"><span name="span_ParamYear">-</span><br>(Month)</div>
    </script>
   <script type="text/x-kendo-template" id="<%: this.ID %>_ewpMonthFactor-headerTemplate">
        <div style="text-align: center;">Month<br>Factor</div>
    </script>
   <script type="text/x-kendo-template" id="<%: this.ID %>_ewpTotalWeight-headerTemplate">
        <div style="text-align: center;">Total<br>Weight</div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_NumberOfProject-headerTemplate">
        <div style="text-align: center;">NUMBER OF<br>PROJECT<br><span name="span_ParamYear">-</span></div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_NumberOfPlanTask-headerTemplate">
        <div style="text-align: center;">NUMBER OF<br>PLAN TASK<br><span name="span_ParamYear">-</span></div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_TotalTaskMonth-headerTemplate">
        <div style="text-align: center;">TOTAL TASK<br>TARGET PER<br><span name="span_ParamMonth">-</span> <span name="span_ParamYear">-</span></div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_TotalCompletedMonth-headerTemplate">
        <div style="text-align: center;">TOTAL TASK<br>COMPLETED PER<br><span name="span_ParamMonth">-</span> <span name="span_ParamYear">-</span></div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_PercentageCompletedMonth-headerTemplate">
        <div style="text-align: center;">PERCENTAGE<br>COMPLETED PER<br><span name="span_ParamMonth">-</span> <span name="span_ParamYear">-</span></div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_PercentageCompleted-headerTemplate">
        <div style="text-align: center;">PERCENTAGE<br>COMPLETED<br><span name="span_ParamYear">-</span></div>
    </script>
<div>
    <script>
        var drawing = kendo.drawing;
        var geometry = kendo.geometry;
        function createColumn(rect, color) {
            var origin = rect.origin;
            var center = rect.center();
            var bottomRight = rect.bottomRight();
            var radiusX = rect.width() / 2;
            var radiusY = radiusX / 3;
            var gradient = new drawing.LinearGradient({
                stops: [{
                    offset: 0,
                    color: color
                }, {
                    offset: 0.5,
                    color: color,
                    opacity: 0.9
                }, {
                    offset: 0.5,
                    color: color,
                    opacity: 0.9
                }, {
                    offset: 1,
                    color: color
                }]
            });

            var path = new drawing.Path({
                    fill: gradient,
                    stroke: {
                        color: "none"
                    }
                }).moveTo(origin.x, origin.y)
                .lineTo(origin.x, bottomRight.y)
                .arc(180, 0, radiusX, radiusY, true)
                .lineTo(bottomRight.x, origin.y)
                .arc(0, 180, radiusX, radiusY);

            var topArcGeometry = new geometry.Arc([center.x, origin.y], {
                startAngle: 0,
                endAngle: 360,
                radiusX: radiusX,
                radiusY: radiusY                
            });

            var topArc = new drawing.Arc(topArcGeometry, {
                fill: {
                    color: color
                },
                stroke: {
                    color: "#ebebeb"
                }
            });
            var group = new drawing.Group();
            group.append(path, topArc);
            return group;
        }

        function createLegendItem(e) {
            if (!e.series.stack) {
                var color = e.options.markers.background;
                var labelColor = e.options.labels.color;
                var rect = new kendo.geometry.Rect([0, 0], [120, 50]);
                var layout = new kendo.drawing.Layout(rect, {
                    spacing: 5,
                    alignItems: "center",
                });

                var overlay = drawing.Path.fromRect(rect, {
                    fill: {
                        color: "#fff",
                        opacity: 0
                    },
                    stroke: {
                        color: "none"
                    },
                    cursor: "pointer"
                });

                var circleGeometry = new kendo.geometry.Circle([0, 10], 4);
                var circle = new kendo.drawing.Circle(circleGeometry, {
                    stroke: { color: "none" },
                    fill: { color: color }
                });
                var marker = circle;

                var label = new kendo.drawing.Text(e.series.name, [0, 0], {
                    fill: { color: labelColor }
                });

                layout.append(marker, label);
                layout.reflow();

                var group = new drawing.Group().append(layout, overlay);

                return layout;
            } else {

                var color = e.options.markers.background;
                var labelColor = e.options.labels.color;
                var rect = new geometry.Rect([0, 0], [120, 50]);
                var layout = new drawing.Layout(rect, {
                    spacing: 5,
                    alignItems: "center"
                });

                var overlay = drawing.Path.fromRect(rect, {
                    fill: {
                        color: "#fff",
                        opacity: 0
                    },
                    stroke: {
                        color: "none"
                    },
                    cursor: "pointer"
                });

                var column = createColumn(new geometry.Rect([0, 0], [15, 10]), color);
                var label = new drawing.Text(e.series.name, [0, 0], {
                    fill: {
                        color: labelColor
                    }
                })

                layout.append(column, label);
                layout.reflow();

                var group = new drawing.Group().append(layout, overlay);

                return layout;
            }
        }
    </script>
</div>
</div>
<style>
    .gridReport{
        display: inline-block;
        font-size: 11px;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .columnReport1 {
        text-align: right;
        vertical-align:top !important;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        font-size: 11px !important;
    }
    .columnReport2 {
        text-align: center;
        vertical-align:top !important;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        font-size: 11px !important;
    }
</style>
