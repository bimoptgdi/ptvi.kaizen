﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuyerMaterialListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.BuyerMaterialListControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>
<%@ Register Src="~/UserControl/OfficerVendorControl.ascx" TagPrefix="uc1" TagName="OfficerVendorControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<div id="buyerMaterialsWindow" style="display: none">
                <uc1:PickEmployeesControl runat="server" ID="PickEmployeesControl" />
</div>

<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .engMaterialsContainer{
        width: 400px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }
    .checkboxBMLMaterial{
        display: none;
    }
</style>

<script type="text/x-kendo-template" id="TraficBMLOSSTemplate">
    <div style='text-align: center'> 
        #= documentTraficETASite ? 
            "<img class='logo' src='images/" + common_getTrafficImageUrl(documentTraficETASite)+ "' style='padding-top: 0px;'>"
        :
            "-" 
        #
    </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_officer-template">
    <div> 
        <%--#= data.officerNameMp ? data.officerNameMp : '-' #--%>
        <a href="\#" data-bind="events: { click: openOfficerVendor }">View</a>
    </div>
</script>

<script type="text/x-kendo-template" id="BMLPRDateTemplate">
    <div class="subColumnTitle">Planned</div>
    <div>#= prPlanDate?kendo.toString(prPlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div #= prIssuedDate>prPlanDate ? "style='color:red'":"" #>#= prIssuedDate?kendo.toString(prIssuedDate,"dd MMM yyyy") : "-" #</div>
</script>
<script type="text/x-kendo-template" id="BMLOSDateTemplate">
    <div class="subColumnTitle">Planned</div>
    <div>#= planOnSite?kendo.toString(planOnSite,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">ETA</div>
    <div>#= etaSiteDateMSt?kendo.toString(etaSiteDateMSt,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div>#= shipmentEndDate?kendo.toString(shipmentEndDate,"dd MMM yyyy") : "-" #</div>
</script>
<script type="text/x-kendo-template" id="BMLPODateTemplate">
    <div class="subColumnTitle">Planned</div>
    <div>#= poPlanDate?kendo.toString(poPlanDate,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div #= poRaisedDate<poPlanDate ? "style='color:red'":"" #>#= poRaisedDate?kendo.toString(poRaisedDate,"dd MMM yyyy") : "-" #</div>
</script>
<script type="text/x-kendo-template" id="BMLQtyTemplate">
    <div class="subColumnTitle">Ordered</div>
    <div>#= orderedQuantityMS ?orderedQuantityMS : "-" #</div>
    <div class="subColumnTitle">Received</div>
    <div>#= receivedQuantityMS ? receivedQuantityMS : "-" #</div>
</script>
<script type="text/x-kendo-template" id="BMLDeliveryTemplate">
    <div class="subColumnTitle">Estimated</div>
    <div>#= etaSiteDateMSt?kendo.toString(etaSiteDateMSt,"dd MMM yyyy") : "-" #</div>
    <div class="subColumnTitle">Actual</div>
    <div>#= actualDeliveryDateMSt?kendo.toString(actualDeliveryDateMSt,"dd MMM yyyy") : "-" #</div>
</script>

<div id="<%: this.ID %>_FormContent" >
    <div >
        <div id="lastUpdateText"></div>
        <div id="<%: this.ID %>_grdDocuments" data-role="grid" data-bind="source: EngMaterialSource, events: { dataBound: onDataBound, excelExport: onExcelExport }"
            data-pageable="true"
            data-filterable= "true"
            data-toolbar= "[{ template: kendo.template($('\#toolbarTemplate').html()) }]"
            data-excel= "{
                'allPages': 'true'
            }"
            data-height="450"
            data-columns="[
<%--            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.BuyerMaterial))
                    { %>{headerTemplate: '<input id=\'header-chbBML\' type=checkbox class=\'k-checkbox checkboxHBMLMaterial\' /><label class=\'k-checkbox-label\' for=\'header-chbBML\'></label>', 
             template: '<div><input id=\'chbBML#= id#\' type=checkbox class=\'k-checkbox checkboxBMLMaterial\' /><label class=\'k-checkbox-label\' for=\'chbBML#= id#\'></label></div>', width: 50 },<% }%>--%>
            {field: 'mrNo', title: 'MR.No.', template:'<div>#= mrNo ? mrNo : \'-\' # </div>', width: 100, filterable: true, },
            {field: 'material', title: 'Stock Code', template:'<div>#= material ? material:\'-\' #</div>', width: 80},
            {field: 'materialDesc', title: 'Description', template:'<div>#= materialDesc ? materialDesc : \'-\' # </div>', width: 180},
<%--            {field: 'lineItem', title: 'Item', template:'<div>#= lineItem ? lineItem : \'-\' #</div>', width: 80},--%>
            {field: 'orderQuantity', title: 'Qty', template:'<div>#= orderQuantity ? orderQuantity : \'-\' #</div>', width: 50},
            {field: 'uom', title: 'Unit', template:'<div>#= uom ? uom : \'-\' #</div>', width: 80},
            {field: 'network', title: 'Network', template:'<div>#= network ? network : \'-\' #</div>', width: 80},
            {title: 'PR', 
            columns: [{ 
                        field: 'prItmNo', title: 'Item',
                        template:'<div>#= prItmNo ? prItmNo : \'-\' #</div>', width: 50
                        },{ 
                        field: 'prPlanDate', title: 'Date',
                        template:kendo.template($('#BMLPRDateTemplate').html()), width: 90,
                        attributes:{ style:'vertical-align: top' },
                        },{
                        field: 'prMPNo', title: 'PR. No.',
                        template: '<div>#= prMPNo ? prMPNo : \'-\' #</div>', width: 100
                        },{
                        field: 'engineer', title: 'Engineer',
                        template: '<div>#= engineer ? engineer : \'-\' #</div>', width: 160
                        }],
            headerAttributes: {
      	            style: 'font-weight: bold;text-align: center;'
    	        }
            },
            {field: 'rfqDate', title: 'RFQ Date', template:'<div>#= rfqDate?kendo.toString(rfqDate,\'dd MMM yyyy\') : \'-\' #</div>', width: 100},
            {title: 'PO', 
            columns: [{ 
                        field: 'poPlanDate', title: 'Date',
                        template:kendo.template($('#BMLPODateTemplate').html()), width: 90,
                        attributes:{ style:'vertical-align: top' },
                        },{
                        field: 'poNo', title: 'PO. No.',
                        template: '<div>#= poNo ? poNo : \'-\' #</div>', width: 100
                        },{
                        field: 'poItemNo', title: 'Item',
                        template: '<div>#= poItemNo ? poItemNo : \'-\' #</div>', width: 80
                        },{
                        field: 'orderedQuantityMS', title: 'Qty.',
                        template: kendo.template($('#BMLQtyTemplate').html()), width: 80,
                        attributes:{ style:'vertical-align: top' },
                        },{
                        field: 'etaSiteDateMSt', title: 'Delivery',
                        template: kendo.template($('#BMLDeliveryTemplate').html()), width: 90,
                        attributes:{ style:'vertical-align: top' },
                        },{
                        field: 'prGroupCode', title: 'Buyer',
                        template: '<div>#= prGroupCode?prGroupCode.concat(\' - \',prGroupDesc):\'-\' #</div>', width: 160,
                        attributes:{ style:'vertical-align: top' },
                        }],
            headerAttributes: {
      	            style: 'font-weight: bold;text-align: center;'
    	        }
            },
            {title: 'Traffic', 
            columns: [{ 
                        field: 'officerNameMp', title: 'Officer',
                        template: kendo.template($('#<%: this.ID %>_officer-template').html()), width: 90
                        },{
                        field: 'trafficMethodMSt', title: 'Method',
                        template: '<div> #= trafficMethodMSt ? trafficMethodMSt : \'-\' #</div>', width: 90
                        },{
                        field: 'planOnSite', title: 'On Site',
                        template:kendo.template($('#BMLOSDateTemplate').html()), width: 90
                        },{
                        field: 'LocationMSt', title: 'Location',
                        template: '<div> #= LocationMSt ? LocationMSt : \'-\' #</div>', width: 90
                        },{
                        title: 'Status', filterable: false, 
                        template: kendo.template($('#TraficBMLOSSTemplate').html()), width: 50
                        }],
            headerAttributes: {
      	            style: 'font-weight: bold;text-align: center;'
    	        }
            }
            ]" 
            data-editable= "{mode: 'popup', window:{width:400, title: 'Engineering Material', modal: true}}", 
            ></div>
<%--            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.BuyerMaterial))
                    { %><input id="btnBMLUpdate" data-role="button"
                    data-bind="events: { click: onClick }"
             value="Edit" class="k-button" style="width:70px" disabled/><% }%>
--%>
    </div>

    <div id="<%: this.ID %>_popOfficerVendor">
        <uc1:OfficerVendorControl runat="server" ID="OfficerVendor" />
    </div>

</div>

<script>
    $("#buyerMaterialsWindow").kendoWindow({
        width: 600,
        title: "Set Traffic Officer"
    });

    var <%: this.ID %>_popOfficerVendor;
    var <%: this.ID %>_engMaterialSource = new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdDocuments"), false);
            },
            transport: {
                read    : { dataType: "json",
                    url: _webApiUrl + "materialcomponents/GetByPrIDforBuyer/" + _projectId
                    },
                update: {
                    type: "PUT", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "materialcomponents/updateEngMaterial/1";
                    }

                },
                parameterMap: function (options, operation) {
                    if (operation == "update") {
                        for (i = 0; i < options.models.length; i++) {
                            options.models[i].materialPlan[0].prPlanDate = kendo.toString(<%: this.ID %>_emPopupModel.emDtPRPlanDate, 'MM/dd/yyyy');
                            options.models[i].materialPlan[0].rfqDate = kendo.toString(<%: this.ID %>_emPopupModel.emDtRFQDate, 'MM/dd/yyyy');
                            options.models[i].materialPlan[0].poPlanDate = kendo.toString(<%: this.ID %>_emPopupModel.emDtPOPlanDate, 'MM/dd/yyyy');
                            options.models[i].materialPlan[0].prPlanBy = _currNTUserID;
                            options.models[i].materialPlan[0].rfqBy = _currNTUserID;
                            options.models[i].materialPlan[0].poPlanBy = _currNTUserID;
                            options.models[i].setMaterialPlan = options.models[i].materialPlan;
                            options.models[i].rfqDate = null;
                            options.models[i].planOnSite = null;
                            options.models[i].poPlanDate = null;
                            options.models[i].poRaisedDate = null;
                            options.models[i].prIssuedDate = null;
                            options.models[i].prPlanDate = null;
                            options.models[i].etaSiteDateMSt = null;
                            options.models[i].shipmentEndDate = null;
                            options.models[i].createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.models[i].updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.models[i].updatedBy = _currNTUserID;
                        }
                        checkedBMLIds = [];
                        var checkboxHBMLMaterial = $('.checkboxHBMLMaterial');
                        checkboxHBMLMaterial[0].checked = false;
                        return { "": options.models };
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        engineer : { type: "string" },
                        ewpNo : { type: "string" },
                        lineItem : { type: "string" },
                        material : { type: "string" },
                        materialDesc : { type: "string" },
                        mrNo: { type: "string" },
                        network : { type: "string" },
                        networkActivity : { type: "string" },
                        orderQuantity : { type: "number" },
                        planOnSite: { type: "date" },
                        poItemNo : { type: "string" },
                        poNo : { type: "string" },
                        pritemNo : { type: "string" },
                        prNo : { type: "string" },
                        projectDocumentId : { type: "number" },
                        projectNo : { type: "string" },
                        receivedQuantity : { type: "number" },
                        reservation : { type: "string" },
                        reservationItemNo : { type: "string" },
                        uom : { type: "string" },
                        wbsNo : { type: "string" },
                        withdrawnQuantity : { type: "number" },
                        prIssuedDate: { type: "date" },
                        poRaisedDate: { type: "date" },
                        shipmentEndDate: { type: "date" },
                        prPlanDate: { type: "date" },
                        rfqDate: { type: "date" },
                        prMPNo: { type: "string" },
                        prItmNo: { type: "string" },
                        poPlanDate: { type: "date" },
                        prGroupCode: {type: "string"},
                        prGroupDesc: { type: "string" },
                        orderedQuantityMS: { type: "number" },
                        receivedQuantityMS: { type: "number" },
                        etaSiteDateMSt: { type: "date" },
                        actualDeliveryDateMSt: { type: "date" },
                        trafficMethodMSt: { type: "string" },
                        LocationMSt: { type: "string" },
                        officerNameMp: { type: "string" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                        },
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_projectDocSource.read();
                        alert("Process failed: "+d);
                        return 0;
                    } else{
                        return d;
                        <%: this.ID %>_projectDocSource.read();
                    }
                },
            },
        pageSize: 10,
        batch: true
        });

    var <%: this.ID %>_status = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/statusiprom"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_emPopupModel = new kendo.observable({
        emDtPRPlanDate: null,
        emDtRFQDate: null,
        emDtPOPlanDate: null,
        onUpdateClick: function () {
        },
        onCancelClick: function () {
        }
     });

    var <%: this.ID %>_viewModel = new kendo.observable({
        emDtPRPlanDate: null,
        emDtRFQDate: null,
        emDtPOPlanDate: null,
        documentById: "",
        documentByName: "",
        documentByEmail: "",
        EngMaterialSource: <%: this.ID %>_engMaterialSource,
        StatusSource: <%: this.ID %>_status,
        onClick: <%: this.ID %>_clickUpdate,
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            var view = grid.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedBMLIds[view[i].id]) {
                    //this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                    //.addClass("k-state-selected")
                    //.find(".checkbox")
                    //.attr("checked", "checked");
                }
            }
        },
        openOfficerVendor: function (e) {
            <%: this.ID %>_popOfficerVendor.center().open();
            OfficerVendor_DataBind();

            OfficerVendor_viewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popOfficerVendor.close();
            });

        },
        onExcelExport: function (e) {
            var data = e.data.EngMaterialSource.data();
            var sheet = e.workbook.sheets[0];
            for (var i = 0; i < sheet.rows[0].cells.length; i++) {
                    sheet.rows[0].cells[i].textAlign = "center";
            }

        }
    });

    var checkedBMLIds = {};

    function <%: this.ID %>_gridClick() {
        var checked = this.checked,
        row = $(this).closest("tr"),
        grid = $("#<%: this.ID %>_grdDocuments").data("kendoGrid"),
        dataItem = grid.dataItem(row);
        checkedBMLIds[dataItem.id] = checked;
        if (checked) {
            //-select the row
            row.addClass("k-state-selected");
        } else {
            //-remove selection
            row.removeClass("k-state-selected");
        }
        $.each(checkedBMLIds,function (key, value) {
            if (checkedBMLIds[key] == true) {
                $("#btnBMLUpdate").removeClass("k-state-disabled");
                $("#btnBMLUpdate").removeAttr("disabled");
                $("#btnBMLUpdate").removeAttr("aria-disabled");
                return false;
            }
            $("#btnBMLUpdate").addClass("k-state-disabled");
            $("#btnBMLUpdate").attr("disabled", "disabled");
            $("#btnBMLUpdate").attr("aria-disabled", true);
            $('.checkboxHBMLMaterial')[0].checked = false;
        })

        $("#btnBMLUpdate").kendoButton({
            enable: true,
            click: <%: this.ID %>_clickUpdate
        });
    }

    function <%: this.ID %>_gridHClick() {
        var cbh = this;
        $('.checkboxBMLMaterial').each(function (idx, item) {
        var checked = this.checked,
        row = $(item).closest("tr"),
        grid = $("#<%: this.ID %>_grdDocuments").data("kendoGrid"),
        dataItem = grid.dataItem(row);
            if (cbh.checked==true) {
                //-select the row
                row.addClass("k-state-selected");
                item.checked = true;
                $("#btnBMLUpdate").removeClass("k-state-disabled");
                $("#btnBMLUpdate").removeAttr("disabled");
                $("#btnBMLUpdate").removeAttr("aria-disabled");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
                item.checked = false;
                $("#btnBMLUpdate").addClass("k-state-disabled");
                $("#btnBMLUpdate").attr("disabled", "disabled");
                $("#btnBMLUpdate").attr("aria-disabled", true);
            }
            checkedBMLIds[dataItem.id] = item.checked;
        })

        $("#btnBMLUpdate").kendoButton({
            enable: true,
            click: <%: this.ID %>_clickUpdate
        });
    }

    function <%: this.ID %>_clickUpdate() {
        kendo.bind($("#buyerMaterialsWindow"), <%: this.ID %>_emPopupModel);
        var buyerMaterialsWindow = $("#buyerMaterialsWindow").getKendoWindow();

        PickEmployeesControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
            var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
            //options.model.employeeId = pickedEmployee.id;
            //options.model.employeeName = pickedEmployee.full_Name;
            //options.model.employeeEmail = pickedEmployee.email;
            //options.model.employeePosition = pickedEmployee.positionId;
            //options.model.dirty = true;

            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();
            var data = grid.dataSource.data();
            for (var i = 0; i < data.length; i++) {
                if (checkedBMLIds[data[i].id]) {
                    if (data[i].materialPlan.length > 0) {
                        data[i].materialPlan[0].trafficofficerid = pickedEmployee.id;
                        data[i].materialPlan[0].trafficofficername = pickedEmployee.full_Name;
                        data[i].materialPlan[0].trafficofficeremail = pickedEmployee.email;

                        if (PickEmployeesControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                            $.ajax({
                                url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                                type: "GET",
                                async: false,
                                success: function (result) {
                                    if (result.length != 0) {
                                        data[i].materialPlan[0].trafficofficername = result[0].FULL_NAME;
                                        data[i].materialPlan[0].trafficofficeremail = result[0].EMAIL;
                                    }
                                },
                                error: function (data) {
                                }
                            });
                        }

                        data[i].dirty = true;
                        data[i].setMaterialPlan = data[i].materialPlan;
                        data[i].updatedBy = _currNTUserID;
                    } else
                    {
                        data[i].materialPlan.push({
                            trafficofficerid: pickedEmployee.id,
                            trafficofficername: pickedEmployee.full_Name,
                            trafficofficeremail: pickedEmployee.email,
                            createdBy: _currNTUserID,
                            updatedBy: _currNTUserID
                    });
                    data[i].dirty = true;
                    data[i].setMaterialPlan = data[i].materialPlan;
                    data[i].updatedBy = _currNTUserID;
                }
                }
            }
            grid.dataSource.sync();
            buyerMaterialsWindow.close();
        });
        PickEmployeesControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
            buyerMaterialsWindow.close();
        });
        // show popup
        PickEmployeesControl_DataBind();

        var popUpEmployee = $("#popUpEmployee").getKendoWindow();
        buyerMaterialsWindow.center().open();

    }


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel); 
        var grid = $("#<%: this.ID %>_grdDocuments").data("kendoGrid");
        grid.table.on("click", ".checkboxBMLMaterial", <%: this.ID %>_gridClick);
        $('#header-chbBML').click(<%: this.ID %>_gridHClick);
        $("#lastUpdateText").html("<div style='color: orange'>Last Update: " + (!lastUpdateMaterial(_projectId) ? "-" : lastUpdateMaterial(_projectId) != null ? lastUpdateMaterial(_projectId) : "-") + "</div>");

        <%: this.ID %>_popOfficerVendor = $("#<%: this.ID %>_popOfficerVendor").kendoWindow({
            title: _projectNo + " - " + _projectDesc,
            modal: true,
            visible: false,
            resizable: false,
            width: 850,
            height: 500
        }).data("kendoWindow");

        $("#exportExcel").on("click", function () {
//            alert("test");
//            var idGrid = "#<%: this.ID %>_grdCostReportControl";
//            var grid = $(idGrid).getKendoGrid();
            var column = grid.columns;
            $.each(column, function (key, value) {
                if (value.columns != null) {
                    if (value.title == "PR") {
                        var b = { field: 'prIssuedDate', title: 'Actual Date' };
                        value.columns.splice(2, 0, b);
                    }
                    if (value.title == "PO") {
                        var b = { field: 'poRaisedDate', title: 'Actual Date' };
                        value.columns.splice(1, 0, b);
                        b = { field: 'receivedQuantityMS', title: 'Received Qty.' };
                        value.columns.splice(5, 0, b);
                        b = { field: 'actualDeliveryDateMSt', title: 'Actual Delivery' };
                        value.columns.splice(7, 0, b);
                    }
                    if (value.title == "Traffic") {
                        var b = { field: 'etaSiteDateMSt', title: 'On-Site ETA' };
                        value.columns.splice(3, 0, b);
                        b = { field: 'shipmentEndDate', title: 'Actual On-Site' };
                        value.columns.splice(4, 0, b);
                    }

                    $.each(value.columns, function (k, v) {
                        if (v.field == 'prPlanDate')
                            v.title = "Plan Date";
                        if (v.field == 'poPlanDate')
                            v.title = "Plan Date";
                        if (v.field == 'orderedQuantityMS')
                            v.title = "Ordered Qty.";
                        if (v.field == 'etaSiteDateMSt')
                            v.title = "Estimated Date";
                        if (v.field == 'planOnSite')
                            v.title = "On-Site Plan Date";

                    });
                }
                value.title = value.title.replace("</br>","\r");
            })
            grid.saveAsExcel();
            $.each(column, function (key, value) {
                if (value.columns != null) {
                    if (value.title == "PR") {
                        value.columns.splice(2, 1);
                    }
                    if (value.title == "PO") {
                        value.columns.splice(1, 1);
                        value.columns.splice(4, 1);
                        value.columns.splice(5, 1);
                    }
                    if (value.title == "Traffic") {
                        value.columns.splice(3, 1);
                        value.columns.splice(3, 1);
                    }
                }
            })
        })

    });

</script>