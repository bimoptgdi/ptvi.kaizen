﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportCloseOutControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportCloseOutControl" %>
<div id="<%: this.ID %>_viewDetailsPanel">
    <div>
        <fieldset data-bind="invisible: searchOption">
            <legend>Search Criteria</legend>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Project No</div>
                <div style="display: inline-block;padding-left: 20px">
                    <input id="<%: this.ID %>_projectNo" 
                        placeholder="Project No"
                        type= "text" 
                        class= "k-textbox" 
                        data-bind="value: projectNo"/>
                </div>
            </div>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Project Description</div>
                <div style="display: inline-block;padding-left: 20px">
                    <input id="<%: this.ID %>_description" 
                        placeholder="Project Description"
                        type= "text" 
                        class= "k-textbox" 
                        data-bind="value: description"
                        style="width: 250px"/>
                </div>
            </div>
            <div style="padding-bottom: 10px">
                <div style="display: inline-block; width: 150px">Project Manager</div> 
                <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_msPM" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Project Managers"
                        data-value-primitive="true"
                        data-text-field="employeeName"
                        data-value-field="employeeId"
                        data-readonly="true"
                        data-bind="value: valuePM, source: dataPM"></select>
                </div>
            </div>
            <div style="padding-bottom: 10px">
                <div style="display: inline-block; width: 150px">Reasons</div> 
                <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_msReasons" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Reasons"
                        data-value-primitive="true"
                        data-text-field="text"
                        data-value-field="value"
                        data-readonly="true"
                        data-bind="value: valueReasons, source: dataReasons"></select>
                </div>
            </div>
            <br />
            <div id="<%: this.ID %>_btnSearch" 
                data-role="button"
                data-bind="events: {click: searchClick}"
                >Search
            </div>
            <div id="<%: this.ID %>_btnReset" 
                data-role="button"
                data-bind="events: {click: resetClick}"
                >Reset
            </div>

        </fieldset>
    </div>
    <div data-bind="visible: searchOption" >
        <div style="text-align: center; font-size: 16px; font-weight: bold;">Report Close Out<br /><br /><br /></div>
        <div id="<%: this.ID %>_grdViewTSDetail" data-role="grid" style="width: 750px"
            data-bind="source: viewDetailsSource, events: { excelExport: excelExport, dataBound: onDataBound }" 
            data-filterable= "true"
            <%--data-toolbar="['excel']"--%>
            data-toolbar= "[{ name: 'excelExport', text: 'Export to Excel'}, { name:'generatePDF', text: 'Generate PDF' }]", 
            data-scrollable="false"
            data-excel="{
                    allPages: true,
                }"
            data-columns="[ 
        
                {field: 'projectNo', title: 'Project', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 175, template: kendo.template($('#<%: this.ID %>_projectNo-template').html())},
                {field: 'projectDesc', title: 'Description', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 250, hidden: true},
                {field: 'projectManager', title: 'Project Manager', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 200, template: kendo.template($('#<%: this.ID %>_projectManager-template').html())},
                {field: 'reason', title: 'Action', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 150, template: kendo.template($('#<%: this.ID %>_action-template').html()), hidden: true },
                {field: 'comment', title: 'Comment', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 150, hidden: true },
                {field: 'picRole', title: 'PIC', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 200, template: kendo.template($('#<%: this.ID %>_PIC-template').html()) },
                {field: 'picName', title: 'PIC Name', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 200, template: '#: data.picName ? data.picName : \'-\'#', hidden: true },
                {field: 'isApproved', title: 'Status', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 90, template: kendo.template($('#<%: this.ID %>_approved-template').html())},
                {field: 'planDate', title: 'Date', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 90, format: '{0:dd MMM yyyy}', template: kendo.template($('#<%: this.ID %>_date-template').html()) },
                {field: 'submittedDate', title: 'Submitted', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 100, hidden: true },
                {field: 'submittedBy', title: 'Submitted By', headerAttributes: { style: 'text-align: center; vertical-align: middle;' }, width: 130}
            
            ]"
            <%--data-pageable="true"--%>
            ></div>
            <div style="text-align: right;" data-bind="visible: printOption">Exported at [<span id="currentDate"></span>]</div>
        </div>                 
        <script id="<%: this.ID %>_projectNo-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                No:
            </div>
            <div> 
                #= data.projectNo # 
            </div>
            <div class='subColumnTitle'>
                Description:
            </div>
            <div> 
                #= data.projectDesc # 
            </div>
        </script>
        <script id="<%: this.ID %>_projectManager-template" type="text/x-kendo-template">
            <div> 
            #: data.projectManager #
            </div>
            <div> 
            <b>Action</b>
            </div>
            <div class='subColumnTitle'>
                Reason:
            </div>
            <div> 
                #: data.reason #
            </div>
            <div class='subColumnTitle'>
                Comment
            </div>
            <div> 
                #= data.comment # 
            </div>
        </script>
        <script id="<%: this.ID %>_action-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                Reason:
            </div>
            <div> 
                #: data.reason #
            </div>
            <div class='subColumnTitle'>
                Comment
            </div>
            <div> 
                #= data.comment # 
            </div>
        </script>
        <script id="<%: this.ID %>_PIC-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                Process:
            </div>
            <div> 
                #= data.picProcessName # 
            </div>
            <div class='subColumnTitle'>
                Name
            </div>
            <div> 
                #= data.picName # 
            </div>
        </script>
        <script id="<%: this.ID %>_date-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                Plan:
            </div>
            <div> 
                #: data.planDate ? kendo.toString(data.planDate, 'dd MMM yyyy') : '-' #
            </div>
            <div class='subColumnTitle'>
                Submitted
            </div>
            <div> 
                #= data.submittedDate ? kendo.toString(data.submittedDate, 'dd MMM yyyy') : '-' # 
            </div>
        </script>
</div>
<style>
.tooltip {
        position: relative;
        display: inline-block;
        text-align: center;
        margin: 0 0 0 4px;
        cursor: pointer;
    }
    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        color: #fff;
        border-radius: 6px;
        width: 200px;
        text-align: left;
        padding-left: 10px;
        padding-right: 10px;
        background-color: #808080;
        padding-bottom: 10px;
             
        /* Position the tooltip text - see examples below! */
        position: absolute;
        top: -5px;
        right: 105%; 
        z-index: 999999;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .tooltip:hover {
        color: #fff;
        padding-right: 1px;
        padding-top: 1px;
        box-sizing: padding-box;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        -webkit-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        transition: all 0.3s linear;
        color: #fff;
        border: none;
    }
    .tooltip .tooltiptext::after {
        content: " ";
        position: absolute;
        top: 50%;
        left: 100%; /* To the right of the tooltip */
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent transparent transparent #808080;
    }
    .tooltip_wrapper {
        display: inline-block;
        width: 30px;
    }
    .hrStyle {
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -moz-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -ms-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -o-linear-gradient(left, #808080, #ffffff, #808080);
    }
</style>
<script type="text/x-kendo-template" id="<%: this.ID %>_approved-template">

# var approvedText = "In Progress"; #    
# if (data.reasonClosure == 'DEL') { approvedText = "Deleted" } else if (data.submittedDate) { approvedText = "Completed" } #
<div>
    #: approvedText #
</div>

</script>

<script id="<%: this.ID %>_dateCreated" type="text/x-kendo-template">
    <div> #= createdDate? kendo.toString(kendo.parseDate(createdDate), "dd MMM yyyy"):'-' #</div>
</script>

<script>
    $("#currentDate").text(kendo.toString(new Date(), "dd MMM yyyy HH:mm:ss"));
    var <%: this.ID %>_vmPopViewDetails = new kendo.observable({
        searchOption: false,
        printOption: false,
        projectNo: "",
        description: "",
        onDataBound: function () {
            window.status = 'ready_to_print';
        },
        viewDetailsSource: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), false);
                window.status = 'ready_to_print';

            },
            transport: {
                read: {
                    async: false,
                    url: function (data) {
                        return _webApiUrl + "closeout/ReportCloseOut/?projectNo=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.projectNo) + "&description=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.description) + "&projectManager=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.valuePM.join("|")) + "&reason=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.valueReasons.join("|"))
                    },
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectNo: { type: "string" },
                        projectDesc: { type: "string" },
                        projectManager: { type: "string" },
                        reasonClosure: { type: "string" },
                        reason: { type: "string" },
                        comment: { type: "string" },
                        isApproved: { type: "boolean" },
                        planDate: { type: "date", format: "dd MMM yyyy" },
                        submittedDate: { type: "date" },
                        submittedBy: { type: "string" },
                        picRole: { type: "string" },
                        picName: { type: "string" }
                    }
                }
            },
            sort: [{ field: "projectNo", dir: "asc" }, { field: "seqCloseOut", dir: "asc" }],
            //pageSize: 10,
            batch: false
        }),
        dataReasons: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), false);
            },
            transport: {
                async: false,
                read: {
                    dataType: "json",
                    url: _webApiUrl + "Lookup/FindByType/closeOutReason"
                },
                parameterMap: function (options, operation) {
                }
            },
            filter: [
              { field: "value", operator: "neq", value: "DEL" }
            ],
            schema: {
                model: {
                    id: "id",
                    fields: {
                        value : { type: "string" },
                        text : { type: "string" },
                    },
                },
                parse: function (d) {
                    var m = [];
                    $.each(d, function (key, value) {
                        if (value.value != "DEL") {
                            m.push(value);
                        }
                    });
                    return m;
                },
            },

            sort: [{ field: "value", dir: "asc" }],
            //pageSize: 10,
            batch: false
        }),
        valueReasons: [],
        dataPM: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdViewTSDetail"), false);
            },
            transport: {
                async: false,
                read: {
                    dataType: "json",
                    url: _webApiUrl + "v_projectEmployeeRole/listEmployeeByRoleId/1"
                },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "employeeId",
                },
            },
            sort: [{ field: "employeeName", dir: "asc" }],
            batch: false
        }),
        valuePM: [],
        generateExcel: function (e) {
            var currentDate = kendo.toString(new Date, "- dd MMM yyyy - HH_mm_ss");
            var rows = [
                {
                    cells: [
                        {
                            value: "Report Close Out",
                            fontSize: 20,
                            textAlign: "center",
                            verticalAlign: "center",
                            background: "#60b5ff",
                            color: "#ffffff"
                        }
                    ],
                    type: "header",
                    height: 70
                },
                {
                    cells: [
                        { value: "Project No", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Project Desc", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Project Manager", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Reasons", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Comments", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Process", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "PIC", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Status", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Plan Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Submitted Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Submitted By", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" }
                    ],
                    type: "header"
                }];

            var dataRow = this.viewDetailsSource;

            dataRow.fetch(function () {
                var data = this.data();
                for (var i = 0; i < data.length; i++) {
                    //push single row for every record
                    rows.push({
                        cells: [
                          { value: data[i].projectNo },
                          { value: data[i].projectDesc },
                          { value: data[i].projectManager },
                          { value: data[i].reason },
                          { value: data[i].comment },
                          { value: data[i].picProcessName },
                          { value: data[i].picName },
                          { value: data[i].submittedDate ? "Completed" : "In Progress" },
                          { value: data[i].planDate, format: "dd MMM yyyy" },
                          { value: data[i].submittedDate, format: "dd MMM yyyy" },
                          { value: data[i].submittedBy }
                        ]
                    })
                }
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                      {
                          columns: [
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { width: 110, autoWidth: false },
                            { width: 110, autoWidth: false },
                            { autoWidth: true }
                          ],
                          // Title of the sheet
                          title: "Reports",
                          // Rows of the sheet
                          rows: rows,
                          frozenRows: 2,
                          mergedCells: ["A1:K1"]
                      }
                    ]
                });

                //sheet.rows.splice(0, 0, { cells: myHeaders, type: "header", height: 70 });

                kendo.saveAs({
                    dataURI: workbook.toDataURL(),
                    fileName: "Report Close Out " + currentDate + ".xlsx",
                    proxyURL: "<%:WebApiUrl%>" + "exportproxy"
                });
            });
        },
        searchClick: function (e) {
            //this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + encodeURIComponent(this.projectNo) + "&description=" + encodeURIComponent(this.description) + "&projectManager=" + encodeURIComponent(this.valuePM.join("|")) + "&reason=" + encodeURIComponent(this.valueReasons.join("|"));
            //this.viewDetailsSource.read();
            if (<%: this.ID %>_vmPopViewDetails.projectNo || <%: this.ID %>_vmPopViewDetails.description ||
                <%: this.ID %>_vmPopViewDetails.valuePM.length != 0 || <%: this.ID %>_vmPopViewDetails.valueReasons.length != 0) {
                var urlOpenWindow = _webAppUrl + "ReportCloseOutPrint.aspx?_Search=true&_No=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.projectNo) + "&_Desc=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.description) + "&_PM=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.valuePM.join("|")) + "&_Reasons=" + encodeURIComponent(<%: this.ID %>_vmPopViewDetails.valueReasons.join("|"));
                window.open(urlOpenWindow)
            }
            else {
                $('<div/>').kendoAlert({
                    title: 'Search Request',
                    modal: true,
                    content: "<div>Search Criteria can't be empty</div>",
                    actions: [{ text: "OK" }]
                }).data('kendoAlert').open();
            }
        },
        createpdf: function (e) {
            //this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + encodeURIComponent(this.projectNo) + "&description=" + encodeURIComponent(this.description) + "&projectManager=" + encodeURIComponent(this.valuePM.join("|")) + "&reason=" + encodeURIComponent(this.valueReasons.join("|"));
            //this.viewDetailsSource.read();

            var urlPDF = encodeURIComponent(_webAppUrl.replace(/\//g, "|") + "ReportCloseOutPrint.aspx?_Print=true&_Search=true&_No=" + <%: this.ID %>_vmPopViewDetails.projectNo + "&_Desc=" + <%: this.ID %>_vmPopViewDetails.description + "&_PM=" + <%: this.ID %>_vmPopViewDetails.valuePM.join("|") + "&_Reasons=" + <%: this.ID %>_vmPopViewDetails.valueReasons.join("|"));
            var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20" + urlPDF + "/";
            //var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--window-status%20ready_to_print%20%20" + urlPDF + "/";

            window.open(pdfUrl);
        },
        resetClick: function (e) {
            this.set("projectNo", "");
            this.set("description", "");
            this.set("valuePM", []);
            this.set("valueReasons", []);
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + this.projectNo + "&description=" + this.description + "&projectManager=" + this.valuePM + "&reason=" + this.valueReasons;
            this.viewDetailsSource.read();
        }

    });

    var _Print = "<%:Request["_Print"]%>",
        _Search = "<%:Request["_Search"]%>",
        _No = "<%:Request["_No"]%>",
        _Desc = "<%:Request["_Desc"]%>",
        _PM = "<%:Request["_PM"]%>",
        _Reasons = "<%:Request["_Reasons"]%>";
    if (_Print) {
        <%: this.ID %>_vmPopViewDetails.set("printOption", true);
    }    
    if (_Search) {
        <%: this.ID %>_vmPopViewDetails.set("searchOption", true);
    }    
    if (_No) {
        <%: this.ID %>_vmPopViewDetails.set("projectNo",_No);
    }    
    if (_Desc) {
        <%: this.ID %>_vmPopViewDetails.set("description",_Desc);
    }
    if (_PM) {
        <%: this.ID %>_vmPopViewDetails.set("valuePM", _PM.split("|"));
    }
    if (_Reasons) {
        <%: this.ID %>_vmPopViewDetails.set("valueReasons", _Reasons.split("|"));
    }

    <%: this.ID %>_popPickEngineer = $("#<%: this.ID %>_popPickEngineer").kendoWindow({
        title: "Timesheets Engineer",
        modal: true,
        visible: false,
        resizable: false,
        width: 800,
        height: 450
    }).data("kendoWindow");

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_viewDetailsPanel"), <%: this.ID %>_vmPopViewDetails);
        if (<%: this.ID %>_vmPopViewDetails.printOption) {
            $(".k-grid-toolbar").hide();
            $("#<%: this.ID %>_grdViewTSDetail .k-grid-header").css("padding", "0 !important");
            $("#<%: this.ID %>_grdViewTSDetail .k-grid-content").css("overflow-y", "visible");
        }
        else {
            $("#<%: this.ID %>_grdViewTSDetail .k-grid-generatePDF").find('span').addClass("k-font-icon k-i-pdf");
            $("#<%: this.ID %>_grdViewTSDetail .k-grid-generatePDF").find('span').css({ "vertical-align": "text-top", "margin-left": "-.3rem", "margin-right": ".3rem" });
            $("#<%: this.ID %>_grdViewTSDetail .k-grid-generatePDF").bind("click", function (e) {
                <%: this.ID %>_vmPopViewDetails.createpdf();
            });

            $("#<%: this.ID %>_grdViewTSDetail .k-grid-excelExport").find('span').addClass("k-font-icon k-i-xls");
            $("#<%: this.ID %>_grdViewTSDetail .k-grid-excelExport").bind("click", function (e) {
                <%: this.ID %>_vmPopViewDetails.generateExcel();
            });
        }
    });
</script>
