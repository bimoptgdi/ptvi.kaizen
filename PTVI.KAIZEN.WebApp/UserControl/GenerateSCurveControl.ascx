﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenerateSCurveControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.GenerateSCurveControl" %>
<div id="<%: this.ID %>_container">
    <div id="<%: this.ID %>_grid" data-role="grid" 
        data-bind="source: dsSCurveGeneration, events: {dataBound: onDataBound}"
        data-auto-bind="false"
        data-no-records="true"
        data-columns="[
            {field: 'projectShift.text', title: 'Shift'},
            {field: 'startDate', title: 'Started At', format: '{0: dd MMM yyyy HH:mm}', width: '130px'},
            {field: 'finishDate', title: 'Finished At', format: '{0: dd MMM yyyy HH:mm}', width: '130px'},
            {template: kendo.template($('#curve-generation-template').html()), title: 'Progress'}            
        ]">
    </div>
</div>
<script id="curve-generation-template"  type="text/x-kendo-template">
    #if (status == 'INPROGRESS') {#
        <div><img style='vertical-align:sub;margin-right: 3px' src='Content/Kendo/2017.1.118/Bootstrap/loading.gif' />Shift <span data-bind='text:shiftNoInProgress'></span> of #: totalShift # have been generated.</div>   
        <div class='progress' data-role='progressbar' data-type='chunk' data-min='0' data-max='#: totalShift #' data-chunk-count='#: totalShift #' data-bind='value: shiftNoInProgress' style='width:100%' ></div>
        <div class='k-button button-restart' title='Restart the S-Curve generation from shift 1'>Restart</div>
    #} else if (status == 'NEW'  || status == 'RESTART' || status == 'RETRY') {#
        <div style='text-align:center'>In Queue</div>
    #} else if (status == 'FAILED') {#
        <div>
            <div style='font-weight:bold'>Failed</div>
            <div>Error message:</div>
            <div style='font-size:small'>#: errorMessage #</div>
            <div><div class='k-button button-restart' title='Restart the S-Curve generation from shift 1'>Restart</div>&nbsp;<div class='k-button button-retry' title='Continue scurve generation from failed shift.' >Retry</div>
        </div>
    #} else if (status == 'COMPLETED' || status == 'EMAIL_FAILED') {#
        <div>
            <div style='font-weight:bold'>Completed</div>
            <div>
                <a class='k-button' href='SCurveReportForm.aspx?psId=#: estimationShiftID #&reporCode=ScurveActualAndForecastReportForm'>Go to report page</a>
                <div class='k-button button-restart' title='Restart the S-Curve generation from shift 1'>Restart</div>
            </div>
        </div>
    #}#        
</script>
<script type="text/javascript">
    var <%: this.ID %>_vm = kendo.observable({
        projectID: null,
        inProgressData: null,
        dsSCurveGeneration: new kendo.data.DataSource({
            transport: {
                read: {
                    url: function () {
                        return "<%: WebApiUrl %>" + "SCurveGenerator/ByProjectID/" + <%: this.ID  %>_vm.projectID
                    }
                }
            },              
            schema: {
                model: {
                    fields: {
                        startDate: { type: 'date' },
                        finishDate: { type: 'date' }
                    }
                }
            }
        }),
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grid").getKendoGrid();
            var data = grid.dataSource.data();

            var checkToStartTimer = <%: this.ID %>_vm.inProgressData == null;

            for (var i = 0; i < data.length; i++) {
                var tr = $("tr[data-uid='" + data[i].uid + "']")[0];
                if (data[i].status == 'INPROGRESS' || data[i].startDate == null) {
                    <%: this.ID %>_vm.inProgressData = data[i];
                    
                    kendo.bind($(tr), <%: this.ID %>_vm.inProgressData);
                }

                var btnRestart = $(tr).find('.button-restart').click(function (e) {
                    var dt = $("#<%: this.ID %>_grid").getKendoGrid().dataItem($(e.currentTarget).closest('tr'));

                    _showLiteLoadingIndicator("<%: this.ID %>_grid", true); 

                    $.ajax({
                        url: "<%: WebApiUrl %>" + "SCurveGenerator/SetStatus?id=" + dt.id + "&status=RESTART",
                        success: function (e) {
                            _showLiteLoadingIndicator("<%: this.ID %>_grid", false);

                            <%: this.ID %>_vm.inProgressData = null;
                            $("#<%: this.ID %>_grid").getKendoGrid().dataSource.read();
                        },
                        error: function (jqXHR) {
                            _showLiteLoadingIndicator("<%: this.ID %>_grid", false);

                            //console.log(jqXHR);
                            var errJson = jqXHR.responseJSON;
                            alert(errJson.ExceptionMessage);
                            //console.log(errJson);
                            //_showErrorMessage('Restart SCurve Failed', errJson
                        }
                    });
                });
                
                var btnRetry = $(tr).find('.button-retry').click(function (e) {
                    var dt = $("#<%: this.ID %>_grid").getKendoGrid().dataItem($(e.currentTarget).closest('tr'));
                    _showLiteLoadingIndicator("<%: this.ID %>_grid", true); 

                    $.ajax({
                        url: "<%: WebApiUrl %>" + "SCurveGenerator/SetStatus?id=" + dt.id + "&status=RETRY",
                        success: function (e) {
                            _showLiteLoadingIndicator("<%: this.ID %>_grid", false); 


                            <%: this.ID %>_vm.inProgressData = null;
                            $("#<%: this.ID %>_grid").getKendoGrid().dataSource.read();
                        },
                        error: function (jqXHR) {
                            _showLiteLoadingIndicator("<%: this.ID %>_grid", false); 

                            //console.log(jqXHR);
                            var errJson = jqXHR.responseJSON;
                            alert(errJson.ExceptionMessage);
                            //console.log(errJson);
                            //_showErrorMessage('Restart SCurve Failed', errJson
                        }
                    });
                });
            }

            if (checkToStartTimer == true && <%: this.ID %>_vm.inProgressData != null) {
                // start refresh data
                <%: this.ID %>_refreshData()
            }


        }
    });

    function <%: this.ID %>_populate(projectID) {
        if (<%: this.ID  %>_vm.get("projectID") == null) {
            kendo.bind($("#<%: this.ID  %>_container"), <%: this.ID  %>_vm);
        }

        <%: this.ID %>_vm.set("projectID", projectID);
        $("#<%: this.ID %>_grid").getKendoGrid().dataSource.read();
    }

    function <%: this.ID %>_refreshData() {
        setTimeout(function () {
            //console.log("fetching data for " + <%: this.ID %>_vm.inProgressData.id);

            $.ajax({
                url: "<%: WebApiUrl %>" + "SCurveGenerator/" + <%: this.ID %>_vm.inProgressData.id,
                success: function (result) {
                    <%: this.ID %>_vm.inProgressData.set("shiftNoInProgress", result.shiftNoInProgress);
                    <%: this.ID %>_vm.inProgressData.set("status", result.status);
                    <%: this.ID %>_vm.inProgressData.set("finishDate", result.finishDate);
                    <%: this.ID %>_vm.inProgressData.set("startDate", result.startDate);
                    
                    if (<%: this.ID %>_vm.inProgressData.get("status") == "INPROGRESS") {
                        <%: this.ID %>_refreshData();
                    }
                    else {
                        <%: this.ID %>_vm.inProgressData = null;
                        $("#<%: this.ID %>_grid").getKendoGrid().dataSource.read();
                    }
                }
            });
        }, 5000);
    }

</script>