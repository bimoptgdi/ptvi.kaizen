﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.RequestList" %>
    <div id="<%: this.ID %>_ContentForm" style="border: 1px solid #c5c5c5; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 10px">
        <div id="<%: this.ID %>_grdRequestList" data-role="grid" 
            data-bind="source: sendReqToUserSource"  
            data-editable= "{mode:'inline', confirmation:true}" 
            data-pageable= "{buttonCount: 5}"
            data-filterable = "{
                    extra: false,
                    operators: {
                        string: {
                            contains: 'Contains',
                            startswith: 'Starts with'
                        }
                    }
                }"
            data-columns="[
            {field: 'requestNo',title: 'Request No', width: 100, template:'<div>#= data.requestNo?data.requestNo:\'-\' # </div>'},
            {field: 'requestDate', title: 'Request Date',width: 100, template:'<div>#= data.requestDate?kendo.toString(data.requestDate,\'dd MMM yyyy\'):\'-\' # </div>'},
            {title: 'To',field: 'mailToName', width: 120, template:'<div>#= data.mailToName?data.mailToName:\'-\' # </div>' },
            {title: 'From', field: 'mailFromName', width: 120, template:'<div>#= data.mailFromName?data.mailFromName:\'-\' # </div>'},
            {field: 'mailSubject', title: 'Subject', width: 150, template:'<div>#= data.mailSubject?data.mailSubject:\'-\' # </div>'},
            {field: 'responseRequiredBy', title: 'Response Required By', width: 160, template:'<div>#= data.responseRequiredBy?kendo.toString(data.responseRequiredBy,\'dd MMM yyyy\'):\'-\' # </div>'},
            {field: 'priorityText', title: 'Priority', width: 70, template:'<div>#= data.priorityText?data.priorityText:\'-\' # </div>'},
            {field: 'status', title: 'Status', width: 80, template:'<div>#= data.status?toTitleCase(data.status):\'-\' # </div>', editor:dropDownStatus},
            {command:['edit'], width: 100}]"
            ></div>                
    </div>

<script>
    function dropDownStatus(container, options) {
        $('<input id="listStatus" required name="' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    autoBind: true,
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: {
                                        transport: {
                                            read: { url: _webApiUrl + "lookup/findbytype/requestListStatus", dataType: "json", type: "GET" }
                                        },
                                        schema: {
                                            model: {
                                                id: "id",
                                                fields: {
                                                    id: { type: "number" },
                                                    text: { type: "string" },
                                                    value: { type: "string" },
                                                }
                                            }
                                        }
                                    }
                                });
    }

    var <%: this.ID %>_dsSendReqToUser = new kendo.data.DataSource({
//        sort: { field: "requestNo", dir: "desc" },
        pageSize: 10,
        requestStart: function () {
            kendo.ui.progress($("#<%: this.ID %>_grdRequestList"), true);
        },
        requestEnd: function () {
            kendo.ui.progress($("#<%: this.ID %>_grdRequestList"), false);
        },
        transport: {
            read: {
                url: function (e) {
                    return _webApiUrl + "RequestToTheUsers/RequestList/" + _projectId;
                }, dataType: "json", type: "GET"
            },
            create: { url: _webApiUrl + "RequestToTheUsers/SendRequest/1", dataType: "json", type: "POST" },
            update: {
                url: function (e) {
                    return _webApiUrl + "RequestToTheUsers/updateRequest/" + e.id;
                }, dataType: "json", type: "PUT" },
            parameterMap: function (options, operation) {
                if (operation == "update") {
                    options.status = $("#listStatus")[0].value;
                    options.updatedBy = _currNTUserID;
                    options.requestDate = kendo.toString(options.requestDate, 'yyyy-MM-dd');
                    options.responseRequiredBy = kendo.toString(options.responseRequiredBy, 'yyyy-MM-dd');
                    console.log(options);
                    return options;
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    requestNo: { type: "string", editable: false },
                    requestDate: { type: "date", editable: false },
                    mailToId: { type: "string" },
                    mailToName: { type: "string", editable: false },
                    mailTo: { type: "string" },
                    mailFromId: { type: "string" },
                    mailFromName: { type: "string", editable: false },
                    mailFrom: { type: "string" },
                    mailSubject: { type: "string", editable: false },
                    mailBody: { type: "string" },
                    responseRequiredBy: { type: "date", editable: false },
                    priority: { type: "string" },
                    priorityText: { type: "string", editable: false },
                    status: { type: "string" },
                    createdBy: { type: "string" },
                    updatedBy: { type: "string" }
                }
            }
        }
    })

    var <%: this.ID %>_viewModel = new kendo.observable({
        sendReqToUserSource: <%: this.ID %>_dsSendReqToUser,
            })


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ContentForm"), <%: this.ID %>_viewModel);
    });
    </script>