﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportEWPResourceLoadingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportEWPResourceLoadingControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
   var monthNames = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var <%: this.ID %>_ReportResoucesLoadingGrid = [], <%: this.ID %>_actualCostFormatData = [], currYear = new Date().getFullYear();
    var <%: this.ID %>_ResoucesLoadingReportModel = kendo.observable ({
        paramYear: currYear,
        listYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_gridReport"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            }
        }),
        onChangeParamYear: function (e) {
            this.dsGetDataMonthWeekOfYear()
            this.generateGriReportResoucesLoading();
        },
        dsResoucesLoadingReport: new kendo.data.DataSource({
            requestStart: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), true);
                kendo.ui.progress($("#ReportResoucesLoadingForm"), true);
            },
            requestEnd: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), false);
                kendo.ui.progress($("#ReportResoucesLoadingForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "Report/EWPResourceLoadingReport/" +  <%: this.ID %>_ResoucesLoadingReportModel.paramYear;
                    },
                    async: false
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dsGetDataMonthWeekOfYear: function () {
            var result = {}, that = this;
            $.ajax({
                url: "<%: WebApiUrl %>" + "report/GetMonthWeekOfYear/" + <%: this.ID %>_ResoucesLoadingReportModel.paramYear,
                type: "GET",
                async: false,
                success: function (data) {
                    result = data
                    that.set("dsMonthWeekOfYear", data)
                },
                error: function (error) {
                }
            });
        },
        dsMonthWeekOfYear: {
            totalWeekOfYear: 0,
            totalWeek: 0,
            listTotalWeekOfMonths: []
        },
        onDataBound: function (e) {
            var data = e.sender._data;
            $.each(data, function (i, row) {
                if (row.level == 0) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#C00000");
                } else if (row.level == 1) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#1F497D");
                } else if (row.level == 2) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).css("color", "#E26B0A");
                }
            });
        },
        dsResoucesLoadingReportReformat: function () {
            var dataReformat = [];

            this.dsResoucesLoadingReport.read();

            var dataOri = this.dsResoucesLoadingReport.data();
            
            //generateColumnGrid
            <%: this.ID %>_ReportResoucesLoadingGrid = [];
            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "deliverableNo", title: "Project No", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>deliverableNo-template').html()),
                width: 200,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });
            
            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "projectType", title: "CAP/OP", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>projectType-template').html()),
                width: 80,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "deliverableName", title: "Project Name / Designer Deliverable", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: kendo.template($('#<%: this.ID %>deliverableName-template').html()),
                width: 300,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "pic", title: "Engineer / Designer", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 250,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                title: "Plan Date",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [{
                    field: "planStartDate", title: "Start",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                },
                {
                    field: "planFinishDate", title: "Finish",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                }]
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                title: "Actual Date",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [{
                    field: "actualStartDate", title: "Start",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                },
                {
                    field: "actualFinishDate", title: "Finish",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    format: "{0: dd MMM yyyy}",
                    width: 100,
                    attributes: { style: "text-align: center; vertical-align: top !important;" },
                    filterable: false
                }]
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "category", title: "Category", 
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false
            });


            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "estimateHours", title: "Estimate Hours",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                headerTemplate: kendo.template($('#<%: this.ID %>estimateHours-headerTemplate').html()),
                template: "#=data.level ? (data.estimateHours ? kendo.toString(data.estimateHours, 'n0') : '0') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "planDurationWeek", title: "Plan Duration (Week)",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "#=data.level ? (data.planDurationWeek ? kendo.toString(data.planDurationWeek, 'n1') : '0.0') : ''#",
                headerTemplate: kendo.template($('#<%: this.ID %>planDurationWeek-headerTemplate').html()),
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "actualDurationWeek", title: "Actual Duration (Week)",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                template: "#=data.level ? (data.actualDurationWeek ? kendo.toString(data.actualDurationWeek, 'n1') : '0.0') : ''#",
                headerTemplate: kendo.template($('#<%: this.ID %>actualDurationWeek-headerTemplate').html()),
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });
            var columnComplexity = [];

            columnComplexity.push({
                field: "startWeek", title: "startWeek",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 85,
                headerTemplate: kendo.template($('#<%: this.ID %>startWeek-headerTemplate').html()),
                template: "#=data.level ? (data.startWeek ? kendo.toString(data.startWeek, 'n0') : '0') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            columnComplexity.push({
                field: "endWeek", title: "endWeek",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 85,
                headerTemplate: kendo.template($('#<%: this.ID %>endWeek-headerTemplate').html()),
                template: "#=data.level ? (data.endWeek ? kendo.toString(data.endWeek, 'n0') : '0') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                title: <%: this.ID %>_ResoucesLoadingReportModel.paramYear.toString(),
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: columnComplexity
            });

            <%: this.ID %>_ReportResoucesLoadingGrid.push({
                field: "weeklyDistributedWeight", title: "Weekly Distributed Weight",
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 100,
                headerTemplate: kendo.template($('#<%: this.ID %>weeklyDistributedWeight-headerTemplate').html()),
                template: "#=data.level ? (data.weeklyDistributedWeight ? kendo.toString(data.weeklyDistributedWeight, 'n1') : '0.0') : ''#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            //loop grid column dsMonthWeekOfYear
            if (this.dsMonthWeekOfYear.totalWeekOfYear > 0) {
                var weekly = 0;
                for (var j = 0; j < this.dsMonthWeekOfYear.listTotalWeekOfMonths.length; j++) {
                    columnComplexity = [];
                    for (var k = 0; k < this.dsMonthWeekOfYear.listTotalWeekOfMonths[j].totalWeek; k++) {
                        //console.log(weekly.toString());
                        columnComplexity.push({
                            field: "ResoucesLoading_" + (++weekly),
                            title: kendo.toString(weekly, "n0"),
                            template: "#=data.level ? (data.ResoucesLoading_" + weekly + " ? kendo.toString(data.ResoucesLoading_" + weekly + ", 'n2') : '0') : ''#",
                            headerAttributes: {
                                "class": "table-header-cell",
                                style: "text-align: center;"
                            }, width: 50,
                            attributes: { style: "text-align: right; vertical-align: top !important;" },
                        });
                    }
                   <%: this.ID %>_ReportResoucesLoadingGrid.push({
                       title: monthNames[this.dsMonthWeekOfYear.listTotalWeekOfMonths[j].month],
                       headerAttributes: {
                           "class": "table-header-cell",
                           style: "text-align: center; vertical-align: middle !important;"
                       },
                       columns: columnComplexity
                   });
                }

            }

            //Generate Data
            for (var i = 0; i < dataOri.length; i++) {
                // new object for insert DataReformat
                var ResoucesLoadingData = {};

                ResoucesLoadingData["deliverableNo"] = dataOri[i].deliverableNo;
                <%: this.ID %>_actualCostFormatData["deliverableNo"] = { type: "string" };

                ResoucesLoadingData["projectType"] = dataOri[i].projectType;
                <%: this.ID %>_actualCostFormatData["projectType"] = { type: "string" };

                ResoucesLoadingData["deliverableName"] = dataOri[i].deliverableName;
                <%: this.ID %>_actualCostFormatData["deliverableName"] = { type: "string" };

                ResoucesLoadingData["pic"] = dataOri[i].pic;
                <%: this.ID %>_actualCostFormatData["pic"] = { type: "string" };

                ResoucesLoadingData["category"] = dataOri[i].category;
                <%: this.ID %>_actualCostFormatData["category"] = { type: "string" };

                ResoucesLoadingData["participation"] = dataOri[i].participation;
                <%: this.ID %>_actualCostFormatData["participation"] = { type: "number" };

                ResoucesLoadingData["planStartDate"] = kendo.parseDate(dataOri[i].planStartDate);
                <%: this.ID %>_actualCostFormatData["planStartDate"] = { type: "date" };

                ResoucesLoadingData["planFinishDate"] = kendo.parseDate(dataOri[i].planFinishDate);
                <%: this.ID %>_actualCostFormatData["planFinishDate"] = { type: "date" };

                ResoucesLoadingData["actualStartDate"] = kendo.parseDate(dataOri[i].actualStartDate);
                <%: this.ID %>_actualCostFormatData["actualStartDate"] = { type: "date" };

                ResoucesLoadingData["actualFinishDate"] = kendo.parseDate(dataOri[i].actualFinishDate);
                <%: this.ID %>_actualCostFormatData["actualFinishDate"] = { type: "date" };

                ResoucesLoadingData["level"] = dataOri[i].level;
                <%: this.ID %>_actualCostFormatData["level"] = { type: "number" };


                ResoucesLoadingData["estimateHours"] = dataOri[i].estimateHours;
                <%: this.ID %>_actualCostFormatData["estimateHours"] = { type: "number" };

                ResoucesLoadingData["planDurationWeek"] = dataOri[i].planDurationWeek;
                <%: this.ID %>_actualCostFormatData["planDurationWeek"] = { type: "number" };

                ResoucesLoadingData["actualDurationWeek"] = dataOri[i].actualDurationWeek;
                <%: this.ID %>_actualCostFormatData["actualDurationWeek"] = { type: "number" };

                ResoucesLoadingData["startWeek"] = dataOri[i].startWeek;
                <%: this.ID %>_actualCostFormatData["startWeek"] = { type: "number" };

                ResoucesLoadingData["endWeek"] = dataOri[i].endWeek;
                <%: this.ID %>_actualCostFormatData["endWeek"] = { type: "number" };

                ResoucesLoadingData["weeklyDistributedWeight"] = dataOri[i].weeklyDistributedWeight;
                <%: this.ID %>_actualCostFormatData["weeklyDistributedWeight"] = { type: "number" };

                //generate type of resourceLoadingDetail
                if (dataOri[i].resourceLoadingDetail != null) {
                    for (var j = 0; j < dataOri[i].resourceLoadingDetail.length; j++) {
                        ResoucesLoadingData["ResoucesLoading_" + dataOri[i].resourceLoadingDetail[j].week] = dataOri[i].resourceLoadingDetail[j].weight;
                    }
                }

                dataReformat[i] = ResoucesLoadingData;
            }
            return dataReformat;
         },
         generateGriReportResoucesLoading: function () {
            var that = this;
            var dataGrid = that.dsResoucesLoadingReportReformat();
            var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
            var dataSource = new kendo.data.DataSource({
                data: dataGrid,
                schema: {
                    model: {
                        id: "id",
                        fields: <%: this.ID %>_actualCostFormatData
                    }
                },
            });

            if ($(idGrid).getKendoGrid() != null) {
                $(idGrid).getKendoGrid().destroy();
                $(idGrid).empty();
            }

            if ($(idGrid).getKendoGrid() == null) {
                $(idGrid).kendoGrid({
                    toolbar: [{ template: kendo.template($("#toolbarTemplate").html()) }], // ["excel"],
                    dataSource: dataSource,
                    dataBound: <%: this.ID %>_ResoucesLoadingReportModel.onDataBound,
                    columns: <%: this.ID %>_ReportResoucesLoadingGrid,
                    scrollable: true,
                    excelExport: function (e) {
                        var sheet = e.workbook.sheets[0];
                        var rowIndex = 2, cell = 0;
                        var sheet = e.workbook.sheets[0];
                        $.each(e.data, function (key, value) {
                            if (rowIndex > 1) {
                                var row = sheet.rows[rowIndex];
 
                                row.cells[0].value = value.level == 0 ? row.cells[0].value : lpad('', (value.level * 4), ' ') + row.cells[0].value;
                                row.cells[2].value = value.level == 0 ? row.cells[2].value : lpad('', (value.level * 4), ' ') + row.cells[2].value;
                                if (value.level == 0) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#C00000";
                                        row.cells[cellIndex].bold = true;
                                    }
                                } else if (value.level == 1) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#1F497D";
                                    }
                                } else if (value.level == 2) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].color = "#E26B0A";
                                    }
                                }
                            }
                            rowIndex++;
                        });
                        for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                            var row = sheet.rows[rowIndex];
                            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                if (cellIndex >= 4 && cellIndex <= 7) {
                                    row.cells[cellIndex].format = "dd MMM yyyy"
                                } else {
                                    if (!isNaN(row.cells[cellIndex].value)) {
                                        row.cells[cellIndex].format = "#,##0.00_;"
                                    }
                                }
                                if (rowIndex == 0 || rowIndex == 1) {
                                    sheet.rows[rowIndex].cells[cellIndex].vAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].hAlign = "center";
                                    sheet.rows[rowIndex].cells[cellIndex].bold = true;
                                }
                            }
                            if (row.type == "header") {
                                row.height = 30;
                            }
                        }

                    }
                });
            }

            $("#exportExcel").on("click", function () {
                var idGrid = "#<%: this.ID %>_grdDrawingReportControl";
                var grid = $(idGrid).getKendoGrid();
                var column = grid.columns;
                $.each(column, function (key, value) {
                    value.title = value.title.replace("</br>", "\r");
                })
                grid.saveAsExcel();
            })

        }
    });
    $(document).ready(function () {
        <%: this.ID %>_ResoucesLoadingReportModel.dsGetDataMonthWeekOfYear();
        kendo.bind($("#<%: this.ID %>_EwpDrawingControl"), <%: this.ID %>_ResoucesLoadingReportModel);
        <%: this.ID %>_ResoucesLoadingReportModel.generateGriReportResoucesLoading();
        <%--$("#<%: this.ID %>_gridDrawingReport").data("kendoGrid").resize();--%>
    });
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>deliverableNo-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableNo#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>projectType-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#">#=data.projectType#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>deliverableName-template">
    <div style="#:(data.level == 0 ? "font-weight: bold; " : "")#padding-left: #:data.level * 8#px;">#=data.deliverableName#</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>estimateHours-headerTemplate">
    <div style="text-align: center;">Estimate<br>Hours</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>planDurationWeek-headerTemplate">
    <div style="text-align: center;">Plan<br>Duration<br>(Week)</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>actualDurationWeek-headerTemplate">
    <div style="text-align: center;">Actual<br>Duration<br>(Week)</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>startWeek-headerTemplate">
    <div style="text-align: center;">Start<br>Week</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>endWeek-headerTemplate">
    <div style="text-align: center;">End<br>Week</div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>weeklyDistributedWeight-headerTemplate">
    <div style="text-align: center;">Weekly<br>Distributed<br>Weight</div>
</script>  

<div id="<%: this.ID %>_EwpDrawingControl">
    <div>
        <span>Year: </span>
        <input data-role="dropdownlist"
               data-placeholder="Select Year"
               data-value-primitive="true"
               data-text-field="text"
               data-value-field="value"
               data-bind=" source: listYear, value: paramYear, events: { change: onChangeParamYear }"
               style="width: 100px" />
    </div>
    <div id="<%: this.ID %>_grdDrawingReportControl" style="height: 450px"
         data-filterable= "true">
    </div>
</div>

<!--
   data-series="[  {type: 'rangeColumn', fromField: 'baselineFinish', toField: 'estimatedFinish', colorField: 'color'},
                        {type: 'line', field: 'baselineFinish', name: 'Baseline'},
                        {type: 'line', field: 'estimatedFinish', name: 'Estimated'}]",
        data-value-axis="{labels:{template: '#= kendo.toString(new Date(value), \'dd MMM yyyy  HH:mm\') #'}}"
        data-category-axis="{field: 'taskName', labels:{rotation: 270, template: '#: <%: this.ID %>_shortLabels(value) #' }}"
!-->