﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportScurveActualAndForecastControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportScurveActualAndForecastControl" %>
<div id="<%: this.ID %>_reportScurveActualAndForecastContainer">
    <div data-bind="visible:loaded" style="display:none;">
        <div style="font-weight:bold">
            <div style="text-align:center">S-Curve Actual & Forecast</div>
            <div style="text-align:center" data-bind="html:reportTitle"></div>
            <div style="text-align:center" >Base : <span data-bind="html: baseDate"></span> </div>
            <div style="text-align:center" >Original : <span data-bind="html: originalDate"></span> (<span data-bind="html: originalDiff"></span>)</div>
            <div style="text-align:center" >optimis : <span data-bind="html: optimisDate"></span> (<span data-bind="html: optimisDiff"></span>)</div>
            <div style="text-align:center" >Realis : <span data-bind="html: realisDate"></span> (<span data-bind="html: realisDiff"></span>)</div>
            <br />
            <div style="text-align:center">% COMPLETE</div>
            <div style="text-align:center" >Baseline : <span data-bind="html: basePct"></span>%</div>
            <div style="text-align:center" >Actual : <span data-bind="html: actualPct"></span>%</div>
        </div><br /><br /><br />
        <div 
            style="height:400px"
            data-role="chart"
            data-transitions="false"
            data-bind="source: dsScurveActualAndForecast"
            data-series="[  {type: 'line', field: 'originalPctComplete', name: 'Original % Complete'},
                            {type: 'line', field: 'optimisticPctComplete', name: 'Optimistic % Complete', color: 'green'},
                            {type: 'line', field: 'realisticPctComplete', name: 'Realistic % Complete', color: 'red'},
                            {type: 'line', field: 'baselinePctComplete', name: 'Baseline % Complete', color: 'yellow'},
                            {type: 'line', field: 'actualPctComplete', name: 'Actual % Complete', color: 'blue'}]"
            data-category-axis="{field: 'shiftno', labels:{rotation: 0, template: '#: <%: this.ID %>_shortLabels(value) #' }}"
            data-chart-area="{background: 'lightgray'}"
                        data-value-axis="[{
                    max: 100,
                    majorTicks: {size: 10, width: 2}, 
                    minorTicks: {size: 5, width: 2, visible: true}
            }]">
        </div><br /><br />    
        <div style="font-size:xx-small">
            <div data-role="grid" data-bind="source: dsScurveActualAndForecast" data-scrollable="false"
            data-columns="[{ template: '#= ++<%: this.ID %>_vm.record #', title:'No', width: '2%', attributes: {style: 'text-align: right;vertical-align:top'} }, 
                        { field: 'taskName', title: 'Task Name', width: '20%', attributes: {style: 'vertical-align:top'} },
                        { field: 'shiftno', title: 'Shift No', width: '20%', attributes: {style: 'vertical-align:top'} },
                        { title: '% Complete', headerAttributes: {style: 'text-align: center'} , columns: [
                            { field: 'baselinePctComplete', title: 'Baseline', attributes: {style: 'text-align: right'}, format:'{0:n2}' },
                            { field: 'actualPctComplete', title: 'Actual', attributes: {style: 'text-align: right'}, format:'{0:n2}' },
                            { field: 'originalPctComplete', title: 'Original', attributes: {style: 'text-align: right'}, format:'{0:n2}' },
                            { field: 'optimisticPctComplete', title: 'Optimistic', attributes: {style: 'text-align: right'}, format:'{0:n2}' },
                            { field: 'realisticPctComplete', title: 'Realistic', attributes: {style: 'text-align: right'}, format:'{0:n2}' }]
                        }]"></div>
        </div>
    </div>
</div>
<div  id="<%: this.ID %>_errorMessageContainer">
    <div data-bind="visible: notFound" style="top: 60px;width: 400px;text-align: center;margin: auto;position: relative;" >
        <div style="border:1px solid red;color:red;font-size:x-large;" data-bind="html: ErrMsg"></div>
    </div>
</div>    

<script>
    var <%: this.ID %>_vm = kendo.observable({
        reportTitle: null,
        taskName: null,
        baseDate: null,
        originalDate: null,
        optimisDate: null,
        realisDate: null,
        originalDiff: null,
        optimisDiff: null,
        realisDiff: null,
        basePct: null,
        actualPct: null,
        dsScurveActualAndForecast: null,
        loaded: false,       
        record: 0
    });
    var <%: this.ID %>_ervm = kendo.observable({
        notFound: false,
        ErrMsg: "No Data Found"
    });
    var <%: this.ID %>_callback = null;

    function <%: this.ID %>_populate(projectShiftID, taskUid, callback) {
        <%: this.ID %>_callback = callback;

        $.ajax({
            url: webApiUrl + "iptreport/reportdata?reportName=IPT_SCURVE_ACTUAL_AND_FORECAST&projectshiftid=" + projectShiftID + "&taskUid=" + taskUid,
            success: function (result) {
                if (result != null) {
                    <%: this.ID %>_vm.set("reportTitle", result.taskName + (result.shiftNo ? ' (Estimation shift: ' + result.shiftNo + ')' : ''));
                    <%: this.ID %>_vm.set("taskName", result.taskName);
                    <%: this.ID %>_vm.set("baseDate", kendo.toString(kendo.parseDate(result.baseDate), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("originalDate", kendo.toString(kendo.parseDate(result.originalDate), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("optimisDate", kendo.toString(kendo.parseDate(result.optimisDate), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("realisDate", kendo.toString(kendo.parseDate(result.realisDate), 'dd MMM yyyy  hh:mm tt'));
                    <%: this.ID %>_vm.set("originalDiff", kendo.toString(result.originalDiff, "n2") + 'h');
                    <%: this.ID %>_vm.set("optimisDiff", kendo.toString(result.optimisDiff, "n2") + 'h');
                    <%: this.ID %>_vm.set("realisDiff", kendo.toString(result.realisDiff, "n2") + 'h');
                    <%: this.ID %>_vm.set("basePct", result.basePct);
                    <%: this.ID %>_vm.set("actualPct", result.actualPct);
                    <%: this.ID %>_vm.dsScurveActualAndForecast = new kendo.data.DataSource({
                        data: result.list,
                        schema: {
                            model: {
                                fields: {
                                    baselinePctComplete: { type: 'number' },
                                    actualPctComplete: { type: 'number' },
                                    originalPctComplete: { type: 'number' },
                                    optimisticPctComplete: { type: 'number' },
                                    realisticPctComplete: { type: 'number' }
                                }
                            }
                        }
                    });

                    <%: this.ID %>_vm.set("loaded", true);
                    <%: this.ID %>_ervm.set("notFound", false);

                    status = "loaded";

                    kendo.bind($("#<%: this.ID %>_reportScurveActualAndForecastContainer"), <%: this.ID %>_vm);
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);
                }
                else {
                    <%: this.ID %>_ervm.set("notFound", true);

                    status = "notfound";
                    kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);                   
                }

                <%: this.ID %>_callback(status);
            },
            error: function (e) {

                var jsonResponse = e.responseJSON;
                //console.log(jsonResponse);
                <%: this.ID %>_ervm.set("ErrMsg", jsonResponse.ExceptionMessage);
                <%: this.ID %>_ervm.set("notFound", true);
                status = "Error";
                kendo.bind($("#<%: this.ID %>_errorMessageContainer"), <%: this.ID %>_ervm);

                window.status = 'ready_to_print';
            }
        });
    }

    function <%: this.ID %>_shortLabels(value) {
        return value;

        var result = "";
        while (value.length > 20) {
            var clipIndex = 20;
            while (value[clipIndex] != ' ')
                clipIndex--;

            result = result + (result != "" ? "\n" : "") + value.substring(0, clipIndex);

            value = value.substring(clipIndex);
        }
    
        result = result + (result != "" ? "\n" : "") + value;

        return result;
    }

</script>

