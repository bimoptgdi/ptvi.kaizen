﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignPCMCControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.AssignPCMCControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>

<style>
    .disabled {
      pointer-events: none;
      opacity: 0.7;
      border: 1px solid red;
    }

    .FormTitlePanel{
        border: 1px solid black;
        border-radius: 4px 4px 0 0;
        color: #fff;
        padding: .65em .92em;
        display: inline-block;
        border-bottom-width: 0;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 0px;
        padding-bottom: 7px;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #ccc;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        top: 1px;
    }
    .FormContentPanel{
        top: 10px;
        border-radius: 0px 4px 0 0;
        border: 1px solid black;
        border-top-width: 1;
        margin-top: 0px;
        color: #333;
        padding: .65em .92em;
        display: inline-block;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #fff;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
    }
</style>
<script type="text/x-kendo-template" id="ckBoxTemplate">
    <input type="checkbox" #= isActive ? "checked='checked'" : "" # disabled  class="ckBox"/>
</script>
<div style="display:none" id="popUpEmployee">
                <uc1:PickEmployeesControl runat="server" ID="PickEmployeesControl" />
</div>

    <div id="<%: this.ID %>_FormContent">
        <div class="FormContentPanel">
            <p  style="color: red">Only one person can be set Active (Last added/updated personnel with "Active" checked)*</p>
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectController))
            { %>
            <fieldset>
                <legend>Project Controller(PC)</legend>
                <div>
                    <div id="<%: this.ID %>_grdPC" data-role="grid" 
                        data-bind="source: PCSource, events: { save: savePC, edit: editPC, dataBound: dataBoundPC }" data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
                        {title: 'Position', template: '#= positionName() #', width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75 }<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectController))
                        { %>,
                        {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}}] }<% } %>
                        ]" data-editable= "inline", data-pageable="true"<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectController))
                        { %>, data-toolbar= "[{name: 'create', text:'Add New PC'}]"<% } %>
                        ></div>
                </div>
            </fieldset>
            <% }%>
            
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignMaterialCoordinator))
            { %>
            <br />
            <fieldset>
                <legend>Material Coordinator(MC)</legend>
                <div>
                    <div id="<%: this.ID %>_grdMC" data-role="grid" 
                        data-bind="source: MCSource, events: { save: saveMC, edit: editMC, dataBound: dataBoundMC }" data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
                        {title: 'Position', template: '#= positionName() #', width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75}<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignMaterialCoordinator))
                        { %>,
                        {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}}] }<% } %>
                        ]" data-editable= "inline", data-pageable="true"<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignMaterialCoordinator))
                        { %>, data-toolbar= "[{name: 'create', text:'Add New MC'}]"<% } %>
                        ></div>
                </div>
            </fieldset>
            <% }%>
           
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectOfficer))
            { %>
            <br />
             <fieldset>
                <legend>Project Officer(PO)</legend>
                <div>
                    <div id="<%: this.ID %>_grdPO" data-role="grid" 
                        data-bind="source: POSource, events: { save: savePO, edit: editPO, dataBound: dataBoundPO }" data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
                        {title: 'Position', template: '#= positionName() #', width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75}<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectOfficer))
                        { %>,
                        {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}}] }<% } %>
                        ]" data-editable= "inline", data-pageable="true"<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignProjectOfficer))
                        { %>, data-toolbar= "[{name: 'create', text:'Add New PO'}]"<% } %>
                        ></div>
                </div>
            </fieldset>
            <% }%>
        </div>
    </div>

    <script>
        function cbClick(e){
            alert(e);
        }

        $("#popUpEmployee").kendoWindow({
            title: "Pick Employee",
            width: 700,
            activate: function() {
                $("#PickEmployeesControl_employeeSearch").select();
            }
        });


            var <%: this.ID %>_PCSource = new kendo.data.DataSource({
                requestStart: function (e) {
                    kendo.ui.progress($("#<%: this.ID %>_grdPC"), true);
                },
                requestEnd: function (e) {
                    if (e.type == "update" || e.type == "create")
                        <%: this.ID %>_viewModel.PCSource.read();
                    kendo.ui.progress($("#<%: this.ID %>_grdPC"), false);
                },
                transport: {
                    create: { url: _webApiUrl + "UserAssignment/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                    read: { url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1" + "?AssignmentType=PC&PrID=" + _projectId, dataType: "json" },
                    update: {
                        type: "PUT", dataType: "json",
                        url: function (e) {
                            return _webApiUrl + "UserAssignment/PutUserAssignmentObject/" + e.id;
                        }

                    },
                    parameterMap: function (options, operation) {
                        if (operation == "create") {
                            options.joinedDate = kendo.toString(options.joinedDate,'MM/dd/yyyy');
                            options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.projectId = _projectId;
                            options.isActive = true;
                            options.assignmentType = "PC";
                            options.createdBy = _currNTUserID;
                            options.updatedBy = _currNTUserID;
                            return options;
                        }
                        if (operation == "update") {
                            options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                            options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.projectId = _projectId;
                            options.assignmentType = "PC";
                            options.updatedBy = _currNTUserID;
                            return options;
                        }
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { type: "number" },
                            projectId: { type: "number" },
                            technicalSupportId: { type: "number" },
                            assignmentType: { type: "string" },
                            employeeId: { type: "string" },
                            employeeName: { type: "string" },
                            employeeEmail: { type: "string" },
                            employeePosition: { type: "string" },
                            joinedDate: { type: "date", defaultValue: null, validation: { <% if (_planStartDate != "null")
                            { %> min: _planStartDate, <% }
                            else if (_planFinishDate != "null")
                            { %> max: _planFinishDate <% } %> } } ,
                            isActive: { type: "boolean" },
                            createdDate: { type: "date" },
                            createdBy: { type: "string" },
                            updatedDate: { type: "date" },
                            updatedBy: { type: "string" },
                        },
                        positionName: function () {
                            return common_getPositionByPositionID(this.employeePosition);
                        }
                    },
                    parse: function (d) {
                        if (d == "Employee ID Exist") {
                            //                        <%: this.ID %>_engineerSource.read();
                            alert("Process failed: "+d);
                            return 0;
                        } else{
                            return d;
                            // < %: this.ID %>_engineerSource.read();
                        }
                    },
                },
                pageSize: 10,
                change: <%: this.ID %>_change,
                requestStart: <%: this.ID %>_requestStart
            });

            var <%: this.ID %>_MCSource = new kendo.data.DataSource({
                requestStart: function (e) {
                    kendo.ui.progress($("#<%: this.ID %>_grdMC"), true);
                },
                requestEnd: function (e) {
                    if (e.type == "update" || e.type == "create")
                        <%: this.ID %>_viewModel.MCSource.read();
                    kendo.ui.progress($("#<%: this.ID %>_grdMC"), false);
                },
                transport: {
                    create: { url: _webApiUrl + "UserAssignment/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                    read: { url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1" + "?AssignmentType=MC&PrID=" + _projectId, dataType: "json" },
                    update: {
                        type: "PUT", dataType: "json",
                        url: function(e){
                            return _webApiUrl + "UserAssignment/PutUserAssignmentObject/" + e.id;
                        }
                    },
                    //                update: { url: _webApiUrl + "UserAssignment", type: "PUT", dataType: "json" }
                    parameterMap: function (options, operation) {
                        if (operation == "create") {
                            options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                            options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.projectId = _projectId;
                            options.isActive = true;
                            options.assignmentType = "MC";
                            options.createdBy = _currNTUserID;
                            options.updatedBy = _currNTUserID;
                            return options;
                        }
                        if (operation == "update") {
                            options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                            options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.projectId = _projectId;
                            options.assignmentType = "MC";
                            options.updatedBy = _currNTUserID;
                            return options;
                        }
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { type: "number" },
                            projectId: { type: "number" },
                            technicalSupportId: { type: "number" },
                            assignmentType: { type: "string" },
                            employeeId: { type: "string" },
                            employeeName: { type: "string" },
                            employeeEmail: { type: "string" },
                            employeePosition: { type: "string" },
                            joinedDate: { type: "date", defaultValue: null, validation: {<% if (_planStartDate != "null")
                            { %> min: _planStartDate, <% }
                            else if (_planFinishDate != "null")
                            { %> max: _planFinishDate <% } %> } } ,
                            isActive: { type: "boolean" },
                            createdDate: { type: "date" },
                            createdBy: { type: "string" },
                            updatedDate: { type: "date" },
                            updatedBy: { type: "string" },
                        },
                        positionName: function () {
                            return common_getPositionByPositionID(this.employeePosition);
                        }
                    },
                    parse: function (d) {
                        if (d == "Employee ID Exist") {
                            //                        <%: this.ID %>_designerSource.read();
                            alert("Process failed: " + d);
                            return 0;
                        } else{
                            return d;
                            //                        < %: this.ID %>_designerSource.read();
                        }
                    },
                },
                pageSize: 10,
                change: <%: this.ID %>_change,
                requestStart: <%: this.ID %>_requestStart
            });

            var <%: this.ID %>_POSource = new kendo.data.DataSource({
                requestStart: function (e) {
                    kendo.ui.progress($("#<%: this.ID %>_grdPO"), true);
                },
                requestEnd: function (e) {
                    if (e.type == "update" || e.type == "create")
                        <%: this.ID %>_viewModel.POSource.read();
                    kendo.ui.progress($("#<%: this.ID %>_grdPO"), false);
                },
                transport: {
                    create: { url: _webApiUrl + "UserAssignment/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                    read: { url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1" + "?AssignmentType=PO&PrID=" + _projectId, dataType: "json" },
                    update: {
                        type: "PUT", dataType: "json",
                        url: function(e){
                            return _webApiUrl + "UserAssignment/PutUserAssignmentObject/" + e.id;
                        }
                    },
                    //                update: { url: _webApiUrl + "UserAssignment", type: "PUT", dataType: "json" }
                    parameterMap: function (options, operation) {
                        if (operation == "create") {
                            options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                            options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.projectId = _projectId;
                            options.isActive = true;
                            options.assignmentType = "PO";
                            options.createdBy = _currNTUserID;
                            options.updatedBy = _currNTUserID;
                            return options;
                        }
                        if (operation == "update") {
                            options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                            options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                            options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                            options.projectId = _projectId;
                            options.assignmentType = "PO";
                            options.updatedBy = _currNTUserID;
                            return options;
                        }
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { type: "number" },
                            projectId: { type: "number" },
                            technicalSupportId: { type: "number" },
                            assignmentType: { type: "string" },
                            employeeId: { type: "string" },
                            employeeName: { type: "string" },
                            employeeEmail: { type: "string" },
                            employeePosition: { type: "string" },
                            joinedDate: { type: "date", defaultValue: null, validation: {<% if (_planStartDate != "null")
                            { %> min: _planStartDate, <% }
                            else if (_planFinishDate != "null")
                            { %> max: _planFinishDate <% } %> } } ,
                            isActive: { type: "boolean" },
                            createdDate: { type: "date" },
                            createdBy: { type: "string" },
                            updatedDate: { type: "date" },
                            updatedBy: { type: "string" },
                        },
                        positionName: function () {
                            return common_getPositionByPositionID(this.employeePosition);
                        }
                    },
                    parse: function (d) {
                        if (d == "Employee ID Exist") {
                            //                        <%: this.ID %>_POSource.read();
                            alert("Process failed: " + d);
                            return 0;
                        } else{
                            return d;
                            //                        < %: this.ID %>_designerSource.read();
                        }
                    },
                },
                pageSize: 10,
                change: <%: this.ID %>_change,
                requestStart: <%: this.ID %>_requestStart
            });

            var <%: this.ID %>_viewModel = new kendo.observable({
                PCSource: <%: this.ID %>_PCSource,
                MCSource: <%: this.ID %>_MCSource,
                POSource: <%: this.ID %>_POSource,
                GetEmployee: <%: this.ID %>_getEmployee,
                EditUser: <%: this.ID %>_editUser,
                savePC: <%: this.ID %>_savePC,
                saveMC: <%: this.ID %>_saveMC,
                savePO: <%: this.ID %>_savePO,
                editPC: <%: this.ID %>_editPC,
                editMC: <%: this.ID %>_editMC,
                editPO: <%: this.ID %>_editPO,
                dataBoundPC: <%: this.ID %>_dataBoundPC,
                dataBoundMC: <%: this.ID %>_dataBoundMC,
                dataBoundPO: <%: this.ID %>_dataBoundPO,
            });

        function <%: this.ID %>_dataBoundPC(e) {
            var grid = $("#<%: this.ID %>_grdPC").getKendoGrid();
            var trs = grid.tbody.find('tr');
            trs.each(function (index, value) {
                var item = grid.dataItem($(this));
                {
                    if (item.isActive == true) {
                        $("#<%: this.ID %>_grdPC").find('.k-grid-add').addClass('disabled');
                        return false;
                    }
                }
                $("#<%: this.ID %>_grdPC").find('.k-grid-add').removeClass('disabled');
            });               
        }

        function <%: this.ID %>_dataBoundMC(e) {
            var grid = $("#<%: this.ID %>_grdMC").getKendoGrid();
            var trs = grid.tbody.find('tr');
            trs.each(function (index, value) {
                var item = grid.dataItem($(this));
                {
                    if (item.isActive == true) {
                        $("#<%: this.ID %>_grdMC").find('.k-grid-add').addClass('disabled');
                        return false;
                    }
                }
                $("#<%: this.ID %>_grdMC").find('.k-grid-add').removeClass('disabled');
            });               
        }
        function <%: this.ID %>_dataBoundPO(e) {
            var grid = $("#<%: this.ID %>_grdPO").getKendoGrid();
            var trs = grid.tbody.find('tr');
            trs.each(function (index, value) {
                var item = grid.dataItem($(this));
                {
                    if (item.isActive == true) {
                        $("#<%: this.ID %>_grdPO").find('.k-grid-add').addClass('disabled');
                        return false;
                    }
                }
                $("#<%: this.ID %>_grdPO").find('.k-grid-add').removeClass('disabled');
            });               
        }
        
        function <%: this.ID %>_editPC(e) {
                if (e.model.id > 0)
                    $(".k-grid-update").html("<span class='k-icon k-i-update'></span>Update");
            }


            function <%: this.ID %>_editMC(e) {
                if (e.model.id > 0)
                    $(".k-grid-update").html("<span class='k-icon k-i-update'></span>Update");
            }

            function <%: this.ID %>_editPO(e) {
                if (e.model.id > 0)
                    $(".k-grid-update").html("<span class='k-icon k-i-update'></span>Update");
            }


        function <%: this.ID %>_savePC(e) {
            if (e.model.employeeId == "") {
                _showDialogMessage("Required", "Name is empty", "");
                e.preventDefault();
            } else
                if (e.model.employeePosition == "") {
                    _showDialogMessage("Required", "Position is empty", "");
                    e.preventDefault();
                } else
                    if (e.model.joinedDate == null) {
                        _showDialogMessage("Required", "Join Date is empty", "");
                        e.preventDefault();
                    }
                var grid = $("#<%: this.ID %>_grdPC").getKendoGrid();
                var trs = grid.tbody.find('tr');
                trs.each(function (index, value) {
                    var item = grid.dataItem($(this));
                    {
                        if (item.isActive == true) {
                            $("#<%: this.ID %>_grdPC").find('.k-grid-add').addClass('disabled');
                            return false;
                        }
                    }
                    $("#<%: this.ID %>_grdPC").find('.k-grid-add').removeClass('disabled');
                });
                
            }

            function <%: this.ID %>_saveMC(e) {
                if (e.model.employeeId == "") {
                    _showDialogMessage("Required", "Name is empty", "");
                    e.preventDefault();
                } else
                    if (e.model.employeePosition == "") {
                        _showDialogMessage("Required", "Position is empty", "");
                        e.preventDefault();
                    } else
                        if (e.model.joinedDate == null) {
                            _showDialogMessage("Required", "Join Date is empty", "");
                            e.preventDefault();
                        }
                var grid = $("#<%: this.ID %>_grdMC").getKendoGrid();
                var trs = grid.tbody.find('tr');
                trs.each(function (index, value) {
                    var item = grid.dataItem($(this));
                    {
                        if (item.isActive == true) {
                            $("#<%: this.ID %>_grdMC").find('.k-grid-add').addClass('disabled');
                            return false;
                        }
                    }
                    $("#<%: this.ID %>_grdMC").find('.k-grid-add').removeClass('disabled');
                });
            }

            function <%: this.ID %>_savePO(e) {
                if (e.model.employeeId == "") {
                    _showDialogMessage("Required", "Name is empty", "");
                    e.preventDefault();
                } else
                    if (e.model.employeePosition == "") {
                        _showDialogMessage("Required", "Position is empty", "");
                        e.preventDefault();
                    } else
                        if (e.model.joinedDate == null) {
                            _showDialogMessage("Required", "Join Date is empty", "");
                            e.preventDefault();
                        }
                var grid = $("#<%: this.ID %>_grdPO").getKendoGrid();
                var trs = grid.tbody.find('tr');
                trs.each(function (index, value) {
                    var item = grid.dataItem($(this));
                    {
                        if (item.isActive == true) {
                            $("#<%: this.ID %>_grdPO").find('.k-grid-add').addClass('disabled');
                            return false;
                        }
                    }
                    $("#<%: this.ID %>_grdPO").find('.k-grid-add').removeClass('disabled');
                });
            }

            function <%: this.ID %>_change(e) {
                switch (e.action){
                    case "add":
                        e.items[0].isActive = true;
                        break;
                    case "edit":
                        e.items[0].isActive = true;
                        break;
                    default:
                }
            }

            function <%: this.ID %>_requestStart(e) {
                switch (e.type){
                    case "create":
                        break;
                    default:
                }
            }

            function <%: this.ID %>_editUser(e){
                if (!e.model.isNew()) {
                    //                $("#< %: this.ID %>_txtemployeeName").attr("disabled", true);
                    //                  $("#< %: this.ID %>_btnemployeeName").attr("disabled", true);
                }
            }

            function <%: this.ID %>_getEmployee(container, options) {
                var input, btn;
                if (options.model.id == 0) {
                    input = $("<input class=' k-textbox' type='text' style='width:150px' disabled/>");
                    input.attr("id", "<%: this.ID %>_txt" + options.field);
                    input.appendTo(container);
                    btn = $("<input type='button' class='k-button' style='min-width:30px' value='...'/>");
                    btn.attr("id", "<%: this.ID %>_btn" + options.field);
                    btn.appendTo(container);
                    //            var pickedEmp = (this.dataItem($(container.currentTarget).closest("tr")));
                } else {
                    input = $("<div type='text' style='width:150px' disabled/>");
                    input.attr("id", "<%: this.ID %>_txt" + options.field);
                    input.appendTo(container);
                    $("#<%: this.ID %>_txt" + options.field).html(options.model.employeeName);
                }

                $("#<%: this.ID %>_btn" + options.field).on("click", function () {
                    PickEmployeesControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                        var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                        options.model.employeeId = pickedEmployee.id;
                        options.model.employeeName = pickedEmployee.full_Name;
                        options.model.employeeEmail = pickedEmployee.email;
                        options.model.employeePosition = pickedEmployee.positionId;

                        if (PickEmployeesControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                            $.ajax({
                                url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                                type: "GET",
                                async: false,
                                success: function (result) {
                                    if (result.length != 0) {
                                        options.model.employeeName = result[0].FULL_NAME;
                                        options.model.employeeEmail = result[0].EMAIL;
                                        options.model.employeePosition = result[0].POSITIONID;
                                    }
                                },
                                error: function (data) {
                                }
                            });
                        }

                        options.model.dirty = true;
                        $("#<%: this.ID %>_txt" + options.field).val(pickedEmployee.full_Name);
                        container.closest("td").next().html(common_getPositionByPositionID(pickedEmployee.positionId));
                        //                    viewModel.requestData.set("operationRepId", pickedEmployee.employeeId);
                        //                    viewModel.requestData.set("operationRepName", pickedEmployee.full_Name);
                        //                    viewModel.requestData.set("operationRepEmail", pickedEmployee.email);
                        popUpEmployee.close();
                    });
                    PickEmployeesControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                        popUpEmployee.close();
                    });
                    // show popup
                    PickEmployeesControl_DataBind();

                    var popUpEmployee = $("#popUpEmployee").getKendoWindow();
                    popUpEmployee.center().open();
                    PickEmployeesControl_pickEmployeeViewModel.set("employeeBadgeNo", "");
                    $("#PickEmployeesControl_EmployeeGrid").getKendoGrid().dataSource.data([]);
                })

            }

        function cekMultiPC(e) {
            alert("test");
            alert(e);
        }
            OnDocumentReady = function () {
                kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel); 
            }

    </script>
