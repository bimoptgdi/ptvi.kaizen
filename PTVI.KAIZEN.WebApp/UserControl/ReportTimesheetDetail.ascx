﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportTimesheetDetail.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportTimesheetDetail" %>
<%@ Register Src="~/UserControl/EngineersOnTimesheetControl.ascx" TagPrefix="uc1" TagName="EngineersOnTimesheetControl" %>

<div id="<%: this.ID %>_popPickEngineer">
    <uc1:EngineersOnTimesheetControl runat="server" id="EngineersOnTimesheetControl" />
</div>

<div id="viewDetailsPanel">
        <div>
            <fieldset>
                <legend>Search</legend>
                <div style="padding-bottom: 10px">
                    <div>Week:</div>
                    <div style="padding-left: 20px">Start: 
                        <input id="startWeek" 
                            data-role="datepicker" 
                            data-bind="value: startWeek"
                            data-format="dd MMM yyyy"/> 
                    End: 
                        <input id="endWeek" 
                            data-role="datepicker" 
                            data-bind="value: endWeek"
                            data-format="dd MMM yyyy"
                            >
                    </div>
                </div>
                <div style="padding-bottom: 10px">
                    <div>Engineer Name:</div> 
                    <div style="padding-left: 20px">
                    <div style="width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_multiSelectEmployee" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Clik button to select Employee"
                        data-value-primitive="true"
                        data-text-field="engineername"
                        data-value-field="engineerid"
                        data-readonly="true"
                        data-bind="value: valueEmployee, source: ds_engineer_list"
                        ></select></div>
                    <div data-role="button"
                        data-bind="events: { click: onPickEmployeeClick }"
                        style="display: inline-block;vertical-align: middle;"
                        class="k-button k-primary">
                        ...
                    </div>

                    </div>
                </div>
                <br />
                <div id="<%: this.ID %>_btnSearch" 
                    data-role="button"
                    data-bind="events: {click: searchClick}"
                    >Search
                </div>
                <div id="<%: this.ID %>_btnReset" 
                    data-role="button"
                    data-bind="events: {click: resetClick}"
                    >Reset
                </div>

            </fieldset>
        </div>
            <fieldset>
                <legend>Result</legend>
        <div id="<%: this.ID %>_grdViewTSDetail" data-role="grid" data-bind="source: viewDetailsSource"  style="width: 750px"
            data-filterable= "true"
            data-toolbar="['excel']"
            data-excel="{
                    allPages: true
                }"
            data-columns="[ 
                {field: 'week', title: 'Week', width: 200, template: '#= week? week:\'-\' #', filterable: false},
                {field: 'engineerName', title: 'Engineer Name', template: '#= engineerName? engineerName:\'-\' #', filterable: false},
                {field: 'projectNo', title: 'Project No', width: 150},
                {field: 'projectTitle', title: 'Project Description'},
                {field: 'description', title: 'Task Description', width: 300},
                {field: 'spentHours', title: 'Hours', width: 80},
                {field: 'createdDate', title: 'Created Date', template: kendo.template($('#<%: this.ID %>_dateCreated').html()), width: 150},
            ]"
            data-pageable="true"
            ></div>                
        </fieldset>
    </div>

<script id="<%: this.ID %>_dateCreated" type="text/x-kendo-template">
    <div> #= createdDate? kendo.toString(kendo.parseDate(createdDate), "dd MMM yyyy"):'-' #</div>
</script>

<script>
        <%: this.ID %>_popPickEngineer = $("#<%: this.ID %>_popPickEngineer").kendoWindow({
            title: "Timesheets Engineer",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

    var <%: this.ID %>_viewDetailsSource = new kendo.data.DataSource({
            transport: {
                read    : { dataType: "json",
                    url: _webApiUrl + "timesheet/listDetailTimesheets/" + _currNTUserID + "?engName=" + "&weekStart=" + "&weekEnd="
                    },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        week: { type: "string" },
                        engineerName : { type: "string" },
                        projectNo : { type: "string" },
                        projectTitle : { type: "string" },
                        description : { type: "string" },
                        spentHours: { type: "number" },
                        createdDate: { type: "datetime" },
                    },
                },
            },
        pageSize: 10,
        batch: false
    });

    var <%: this.ID %>_ds_engineer_list = new kendo.data.DataSource({
        requestStart: function () {
            kendo.ui.progress($("#<%: this.ID %>_ucEngineerOnTimesheet"), true);
        },
        requestEnd: function () {
            kendo.ui.progress($("#<%: this.ID %>_ucEngineerOnTimesheet"), false);
        },
        transport: {
            read: _webApiUrl + "timesheet/listEngineerTimesheet/1"
        },
        schema: {
            model: {
                id: "engineerid",
                fields: {
                    engineername: { type: "text" }
                }
            }
        }
    });

    var <%: this.ID %>_vmPopViewDetails = new kendo.observable({
        startWeek: new Date(),
        endWeek: new Date(),
        engName: "",
        viewDetailsSource: <%: this.ID %>_viewDetailsSource,
        ds_engineer_list: <%: this.ID %>_ds_engineer_list,
        dataEmployee: [],
        valueEmployee: [],
        searchClick: function (e) {
            var startWeek = kendo.toString(this.startWeek, 'dd MMM yyyy');
            var endWeek = kendo.toString(this.endWeek, 'dd MMM yyyy');
            this.engName = this.valueEmployee.join();
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "timesheet/listDetailTimesheets/" +
                _currNTUserID + "?engName=" + this.engName + "&weekStart=" + startWeek + "&weekEnd=" + endWeek
            this.viewDetailsSource.read();
        },
        resetClick: function (e) {
            this.set("startWeek", new Date());
            this.set("endWeek", new Date());
            this.set("valueEmployee", []);
            var startWeek = kendo.toString(this.startWeek, 'dd MMM yyyy');
            var endWeek = kendo.toString(this.endWeek, 'dd MMM yyyy');
            this.engName = this.valueEmployee.join();
            this.viewDetailsSource.options.transport.read.url = _webApiUrl + "timesheet/listDetailTimesheets/" +
                _currNTUserID + "?engName=" + this.engName + "&weekStart=" + startWeek + "&weekEnd=" + endWeek
            this.viewDetailsSource.read();
        },
        msEmpSelect: function (e) {
            //var item = e.item;
            //var text = item.text();
            //var data = EngineersOnTimesheetControl_pickEngineersOnTimesheetModel.ds_engineer_list;
        },
        onPickEmployeeClick: function (e) {
            var that = this;
            
            EngineersOnTimesheetControl_pickEngineersOnTimesheetModel.set("pickEmployee", function (e) {
                that.set("dataEmployee", EngineersOnTimesheetControl_pickEngineersOnTimesheetModel.onSelect());
                var empId = [];
                $.each(that.dataEmployee, function (index, value) {
                    empId.push(value.employeeId);
                });
                that.set("valueEmployee", empId);
                <%: this.ID %>_popPickEngineer.close();
            });
            // show popup
            EngineersOnTimesheetControl_pickEngineersOnTimesheetModel.reloadGrid(this.valueEmployee);
            <%: this.ID %>_popPickEngineer.center().open();
        },

    });

    kendo.bind($("#viewDetailsPanel"), <%: this.ID %>_vmPopViewDetails); 
<%--    $("#<%: this.ID %>_multiSelectEmployee").getKendoMultiSelect().dataSource = EngineersOnTimesheetControl_pickEngineersOnTimesheetModel.ds_engineer_list;
    $("#<%: this.ID %>_multiSelectEmployee").getKendoMultiSelect().dataSource.read();--%>
    </script>