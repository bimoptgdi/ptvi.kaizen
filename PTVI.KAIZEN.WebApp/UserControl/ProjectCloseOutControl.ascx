﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectCloseOutControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ProjectCloseOutControl" %>
<%@ Import Namespace="PTVI.iPROM.WebApp" %>
<script>
    var <%:this.ID%>_viewModel = kendo.observable({
        allDatatoBind: null,
        disableChecked: function () {
            return this;
        },        
        roleFormTitle: "", //title form
        rid: 0,
        onChangeCboRole: function (e) {
            <%:this.ID%>_viewModel.set("rid", e.sender.value());
            if (<%:this.ID%>_viewModel.roleLoginerDefault.id != e.sender.value())
                <%:this.ID%>_viewModel.set("showRefreshFormBtn", true);
            else
                <%:this.ID%>_viewModel.set("showRefreshFormBtn", false);
        },
        onDataBoundCboRole: function (e) {
            if(<%:this.ID%>_viewModel.roleLoginerDefault !== null){
                e.sender.value(<%:this.ID%>_viewModel.roleLoginerDefault.id);
            }
        },
        btnChooseRole:function(){
            window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx?rid=" + <%:this.ID%>_viewModel.rid;
            <%:this.ID%>_viewModel.set("roleLoginerDefault",null);
        },
        dsLookupReasonBound: function(e){
            if (_roleInProject == "projectManager") {
            }
        },
        showRefreshFormBtn: false,

        //PM Finalize Rejection
        arrCOProgIdRejected: [],
        dsCOProgRejected: null,

        //vf3
        curCopId: 0, // current close out progresses id
        curRoleId: 0, // current role from default / choosen user login role
        loginerIsInCurrProj: false,
        loginerIsInCurrTask: false,
        rolesLoginer: [], // list role object of user login
        rolesNotSubmitted: [], // list role object of not submitted progresses yet
        rolesLoginerValid: [], // list role object of user login = not submitted progresses
        roleLoginerDefault: null, // object role of user login default or, choosen role (if has multi roles with the same sequence approval) 

        //vf2
        //rolesLoginUser: [],
        fileTypeChange: function (e) {
            <%:this.ID%>_viewModel.set("selectedFileType", e.sender.value());
        },
        fileTypeDataBound: function (e) {
            <%:this.ID%>_viewModel.set("selectedFileType", e.sender.value());
        },
        dsAttachmentListServ: [],
        currRoleText: "",
        dsAllCloseOut: new kendo.data.DataSource({
            requestEnd: function (e) {
                <%:this.ID%>_viewModel.set("curCopId", e.response[0].curCopId);
                <%:this.ID%>_viewModel.set("closeOutProgresses", e.response[0].closeOutProgresses);
                <%:this.ID%>_viewModel.set("allDatatoBind", e.response[0]);

                <%:this.ID%>_viewModel.set("loginerIsInCurrProj", <%:this.ID%>_viewModel.allDatatoBind.loginerIsInCurrProj);
                <%:this.ID%>_viewModel.set("loginerIsInCurrTask", <%:this.ID%>_viewModel.allDatatoBind.loginerIsInCurrTask);
                <%:this.ID%>_viewModel.set("rolesLoginer", <%:this.ID%>_viewModel.allDatatoBind.rolesLoginer);
                <%:this.ID%>_viewModel.set("rolesNotSubmitted", <%:this.ID%>_viewModel.allDatatoBind.rolesNotSubmitted);
                <%:this.ID%>_viewModel.set("roleLoginerDefault", <%:this.ID%>_viewModel.allDatatoBind.roleLoginerDefault);

                if (<%:this.ID%>_viewModel.allDatatoBind.rolesLoginerValid.length > 1) {
                    $("#<%:this.ID%>_divLoginRole").show();
                }
                <%:this.ID%>_viewModel.set("rolesLoginerValid", <%:this.ID%>_viewModel.allDatatoBind.rolesLoginerValid);

                <%:this.ID%>_viewModel.set("allDatatoBind.userLoginBadgeNo", _currBadgeNo);
                <%:this.ID%>_viewModel.set("allDatatoBind.userLoginEmail", _currEMail);
                <%:this.ID%>_viewModel.set("allDatatoBind.userLoginName", _currUserName);

                $("#curRoleTxt").text(<%:this.ID%>_viewModel.currRoleText);
                $.each(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses, function (index, value) {
                    value.planDate = kendo.parseDate(value.planDate);
                    //value.submitedDate = kendo.parseDate(value.submitedDate);
                    $.each(value.closeOutDocuments, function (index, value1) {
                        <%:this.ID%>_viewModel.dsAttachmentListServ.push({
                            id: value1.id,
                            closeoutProgressId: value.id,
                            closeoutDocumentId: value1.closeoutDocumentId,
                            fileType: value1.fileType,
                            fileName: value1.fileName,
                            refDocId: value1.refDocId,
                            description: value1.description,
                            isLocal: false
                        });

                    });
                });
                var convertCloseOutProgresses = $.each(e.response[0].closeOutProgresses, function (index, value) {
                    value.planDate = kendo.parseDate(value.planDate);
                    value.submitedDate = kendo.parseDate(value.submitedDate);
                });
                <%:this.ID%>_viewModel.dsCO2.data(convertCloseOutProgresses);
                <%:this.ID%>_viewModel.set("checkedRadioReason", <%:this.ID%>_viewModel.allDatatoBind.reasonClosureType);
                if ($.trim(<%:this.ID%>_viewModel.checkedRadioReason) == "" || <%:this.ID%>_viewModel.checkedRadioReason == null) {
                    <%:this.ID%>_viewModel.set("checkedRadioReason", <%:this.ID%>_viewModel.rbReasonDefault);
                }
                <%:this.ID%>_viewModel.dsAttachmentList.data(<%:this.ID%>_viewModel.dsAttachmentListServ);

                if (<%:this.ID%>_viewModel.roleLoginerDefault.id != 0) {
                    <%:this.ID%>_viewModel.set("dsListDocument", Enumerable.From(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses)
                                                                    .Where("$.closeOutRoleId == '" + <%:this.ID%>_viewModel.roleLoginerDefault.id + "'")
                                                                    .ToArray()[0].setupCloseOutDocuments);
                }

                <%:this.ID%>_viewModel.set("isApprove", e.response[0].isApproved);
                <%:this.ID%>_viewModel.set("fileTypeCount", <%:this.ID%>_viewModel.dsListDocument.length);
                if (<%:this.ID%>_viewModel.allDatatoBind.rolesLoginerValid.length < 1) {
                    <%:this.ID%>_allDisabedCloseOutProject();
                }

                $("#<%:this.ID%>_divBodyCloseOut").show();

                kendo.ui.progress($("#<%: this.ID %>_divContainer"), false);
            },
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_divContainer"), true);
            },
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "closeout/checkProjectCloseOut?param=" + _projectId + "&bn=" + _currBadgeNo + "&rid=" + "<%:string.IsNullOrEmpty(Request.QueryString["rid"])?"0": Request.QueryString["rid"]%>"
                }
            },
            schema: {
                model: {
                    id: "id"
                }
            }
        }),
        fileTypeCount: null,

        // top
        dsLookupReason: new kendo.data.DataSource({
            requestEnd: function (e) {
                <%:this.ID%>_viewModel.set("rbReasonDefault", e.response[0].VALUE);

            },
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "lookup/FindLookupByType/closeoutreason" // type = closeoutreason
                }
            },
            schema: {
                model: {
                    id: "OID"
                },
                parse: function (d) {
                    var m = [];
                    $.each(d, function (key, value) {
                        if (value.VALUE != "DEL") {
                            m.push(value);
                        }
                    });
                    return m;
                },
            }
        }),
        checkedRadioReason: "",
        rbReasonDefault: "",
        comment: "",

        // Attachment

        dsListDocument: [],
        cancelEditPlan: function (e) {
            e.sender.cancelChanges();
        },
        editPlan: function (e) {
            e.preventDefault();
        },
        grdPlanDataBound: function (e) {
            var curRole = <%:this.ID%>_viewModel.roleLoginerDefault.id;
            var coProggress = <%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses;
            strN = "<%=constantRoleDefi.PROJECT_MANAGER%>";
            strN = strN.replace(/\s+/g, '').toLowerCase();
            if ((curRole == "0") && ($.trim(_roleInProject.toLowerCase()) != strN)) {
                e.sender.hideColumn('23');
            }
            else {
                var role = Enumerable.From(coProggress).Where("$.closeOutRoleId ==  '" + curRole + "'").Select("$.closeOutRoleText").ToArray();
                if (($.trim(role) != "<%=constantRoleDefi.PROJECT_MANAGER%>") && ($.trim(_roleInProject.toLowerCase()) != strN)) {
                    e.sender.hideColumn('23');
                }
            }
            var grid = $("#tes").getKendoGrid();
            var na = 0;
            grid.tbody.find("tr[role='row']").each(function () {
                var role = Enumerable.From(coProggress).Where("$.closeOutRoleId ==  '" + curRole + "'").Select("$.closeOutRoleText").ToArray();
                var model = grid.dataItem(this);
                if ((model.submitedDate != null)) {
                    $(this).find(".k-grid-edit").hide();
                }
                if ($.trim(_roleInProject.toLowerCase()) != strN) { $(this).find(".k-grid-edit").hide() }
                else { $(this).find(".k-grid-edit").removeAttr('disabled'); };
                //if (model.submitedDate == null) {
                //    na = 1;
                //}
            });
            if (_roleInProject == "projectManager" && !<%:this.ID%>_viewModel.loginerIsInCurrTask) {

                $("#<%:this.ID%>_submit").hide();
                $("#<%:this.ID%>_approve").hide();
                $("#<%:this.ID%>_reject").hide();
                $("#<%:this.ID%>_close").hide();
            }
        },
        grdAttachmentDtBound: function (e) {
            var grd = e.sender;
            grd.tbody.find("tr .k-grid-delete").each(function () {
                var currentDataItem = grd.dataItem($(this).closest("tr"));

                //Check in the current dataItem if the row is deletable
                if (currentDataItem.closeoutProgressId != Enumerable.From(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses)
                                                            .Where("$.closeOutRoleId == '" + <%:this.ID%>_viewModel.allDatatoBind.roleLoginerDefault.id + "'")
                                                            .Select("$.id").ToArray()[0]) {
                    $(this).remove();
                }
            })
        },
        dsCO2: new kendo.data.DataSource({
            batch: false,
            data: [],
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number", editable: false },
                        closeOutRoleText: { type: 'string', editable: false },
                        closeOutRolePICName: { type: 'string', editable: false },
                        planDate: { type: 'date' },
                        submitedDate: {
                            type: 'date', editable: false
                        }
                    }
                }
            }
        }),
        dsSuppDoc: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "api/supportingdocument"
                }
            },
            schema: {
                model: {
                    id: "OID"
                }
            }
        }),

        onCLickRadio: function (e) {
            <%:this.ID%>_viewModel.set("allDatatoBind.reasonClosureType", $(e.currentTarget).val());
            <%:this.ID%>_viewModel.set("checkedRadioReason", <%:this.ID%>_viewModel.allDatatoBind.reasonClosureType);
        },
        onClickCheckBox: function (e) {
            $(e.currentTarget).val($(e.currentTarget).is(':checked'));
            $.each(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses, function (index, value) {
                $.each(value.closeOutCheckLists, function (index, value1) {
                    if (value1.id == $(e.currentTarget)[0].id) value1.set("isChecked", $(e.currentTarget).is(':checked'))
                });
            });
        },
        onClose: function (e) {
            e.preventDefault();
        },
        selectedReason: null,

        //attachment
        dsAttachmentType: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "closeout/getAttachmentType/1" //CloseOutRoleID = 1
                }
            },
            schema: {
                model: {
                    id: "id"
                }
            }
        }),

        attDescription: "",
        selectedFiles: [],
        saveHandler: _webApiUrl + "upload",
        removeUrl: "",
        projectId: _projectId,
        fileTypeIndex: 0,
        selectedFileType: null, //dropdown file type

        //uri save/submit
        uriSave: "<%: WebApiUrl %>" + "closeout/saveSubmitCloseOut?param=" + _projectId + "&isSubmit=false",
        uriSubmit: "<%: WebApiUrl %>" + "closeout/saveSubmitCloseOut?param=" + _projectId + "&isSubmit=true",
        uriSubmit2: "<%: WebApiUrl %>" + "closeout/submitAfterUploadSuccess/" + _projectId,
        uriSetDeleted: "<%: WebApiUrl %>" + "closeout/setDeletedCloseOut?param=" + _projectId + "&isDeleted=false",

        onUploadAtt: function (e) {
            kendo.ui.progress($(".container-main"), true);
            var dt = null;
            var dt1 = [];
            if (<%:this.ID%>_viewModel.dsAttachmentList != null) {
                dt = Enumerable.From(<%:this.ID%>_viewModel.dsAttachmentList.data())
                .Where("$.isLocal == true && $.fileName == '" + e.files[0].name + "'")
                .ToArray();
            }

            $.each(dt, function (index, value) {

                dt1.push({ closeoutDocumentId: value.closeoutDocumentId, fileType: value.fileType, fileName: value.fileName, description: value.description });
            });

            var strval = JSON.stringify(dt1);
            <%--var curCopId = Enumerable.From(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses)
                                                            .Where("$.closeOutRoleId == '" + <%:this.ID%>_viewModel.allDatatoBind.roleLoginerDefault.id + "'")
                                                            .Select(x=>x.id).ToArray()[0]--%>
            var curCopId = <%:this.ID%>_viewModel.curCopId;
            e.data = { typeName: "CLOSEOUT", badgeNo: _currBadgeNo, projectId: this.projectId, currProggId: curCopId, datas: strval };
        },

        addFile:false,
        onAttachmentSelect: function (e) {
            var that = this;

            var fileTypeID = <%:this.ID%>_viewModel.selectedFileType;
            $.each(e.files, function (index, value) {
                var docs = Enumerable.From(<%:this.ID%>_viewModel.dsListDocument)
                                .Where("$.id=='" + fileTypeID + "'")
                                .Select("$.documentType")
                                .ToArray();
                var docName = "";
                if (docs.length > 0) {
                    docName = docs[0];
                }

                // check file with same name is exist
                var fileList = <%:this.ID%>_viewModel.dsAttachmentList.data();
                var count = Enumerable.From(fileList)
                                .Count("$.fileName=='" + value.name + "'");

                if (count > 0) {
                    $(".k-clear-selected").hide();
                    $(".k-upload-selected").hide();

                    $("<div />").kendoDialog({
                        title: "Warning",
                        closable: false,
                        modal: true,
                        content: "There is a file with same file name.",
                        actions: [{
                            text: 'OK',
                            action: function (e) {
                                return true;
                            }
                        }]
                    }).data("kendoDialog").open();


                    return;

                }

                var cpId = Enumerable.From(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses)
                            .Where("$.closeOutRoleId == '" + <%:this.ID%>_viewModel.allDatatoBind.roleLoginerDefault.id + "'")
                            .Select("$.id").ToArray();


                <%:this.ID%>_viewModel.dsAttachmentList.add({
                    id: null,
                    closeoutProgressId: cpId[0],
                    closeoutDocumentId: fileTypeID,
                    fileType: docName,
                    fileName: value.name,
                    description: <%:this.ID%>_viewModel.attDescription,
                    isLocal: true
                });
                that.set("addFile", true);

                setTimeout(function () {
                    <%:this.ID%>_viewModel.set('attDescription', '');
                    $(".k-clear-selected").hide();
                    $(".k-upload-selected").hide();
                }, 100);
            });
        },

        btnSaveClick: false,
        btnSubmitClick: true,
        onSuccessAtthment: function (e) {            
            var that = this;
            if (e.operation == "upload" && that.btnSubmitClick == true) {
                 $.ajax({
                    url: that.uriSubmit2,
                    type: "PUT",
                    data: that.allDatatoBind.toJSON(),
                    success: function (data) {
                        kendo.ui.progress($(".container-main"), false);
                        $("<div />").kendoDialog({
                            title: false,
                            closable: false,
                            modal: true,
                            content: "The form has been submitted successfully.",
                            actions: [{
                                text: 'OK',
                                action: function (e) {
                                        window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                                    return true;
                                }
                            }]
                        }).data("kendoDialog").open();
                    }
                })
            }
            else if (e.operation == "upload" && that.btnSaveClick == true) {
                kendo.ui.progress($(".container-main"), false);
                $("<div />").kendoDialog({
                    title: false,
                    closable: false,
                    modal: true,
                    content: "The form has been saved successfully.",
                    actions: [{
                        text: 'OK',
                        action: function (e) {
                                window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                            return true;
                        }
                    }]
                }).data("kendoDialog").open();
            }
        },
        onErrorAttachment: function (e) {
            kendo.ui.progress($(".container-main"), false);
            var that = this;
            $("<div />").kendoDialog({
                title: false,
                closable: false,
                modal: true,
                content: "<div>File failed to upload to document center. But if there are another changes, they have been saved.</div><div>Please check your network connection and upload again.</div>",
                actions: [{
                    text: 'OK',
                    action: function (e) {
                            window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                        return true;
                    }
                }]
            }).data("kendoDialog").open();
        },
        arrDelFiles: [],
        onRemove: function (e) {
            if (!e.model.isLocal) {
                var closeOutDocumentId = e.model.id;
                var closeOutProgressId = e.model.closeoutProgressId;
                var arr = <%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses;
                var cod = Enumerable.From(arr)
                                .Where("$.id == '" + e.model.closeoutProgressId + "'")
                                .Select("$.closeOutDocuments").ToArray()[0];
                //arr.find(x => x.id === e.model.closeoutProgressId).closeOutDocuments;
                var doc = Enumerable.From(cod)
                                .Where("$.id == '" + e.model.id + "'")
                                .ToArray()[0];
                //cod.find(x=>x.id === e.model.id);

                var docNew = cod.filter(function (value) {
                    return value.id !== e.model.id;
                });

                //<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses.find(x => x.id === e.model.closeoutProgressId).set("closeOutDocuments", docNew);
                //<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses.Where("$.id == '" + e.model.closeoutProgressId + "'").ToArray()[0].set("closeOutDocuments", docNew);
                Enumerable.From(<%:this.ID%>_viewModel.allDatatoBind.closeOutProgresses).Where("$.id == '" + e.model.closeoutProgressId + "'").ToArray()[0].set("closeOutDocuments", docNew);
            }
        },
        dsAttachmentList: new kendo.data.DataSource({
            data: [],
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false },
                        closeoutProgressId: { type: "number", editable: false },
                        closeoutDocumentId: { type: "number", editable: false },
                        fileType: { type: "string", editable: false },
                        fileName: { type: "string", editable: false },
                        description: { type: "string", editable: false },
                        isLocal: { type: "boolean" }
                    }
                }
            }
        }),
        grdAttachmentDeleteRow: function (e) {

        },
        listCheckItem: [],
        isCheckedAll: null,
        isApprove: null,
        onEditGridPlandate: function (e) {

        },
        onSaveGridPlandate: function (e) {

        },
        onCancelGridPlanDate: function (e) {
            $("#tes").data("kendoGrid").cancelChanges();
        },
        onSaveChangesGridPlanDate: function () {

        },
        saveGrdPlan: function () {
            var that = this;
            var grdPlanDatas = this.dsCO2.data();
            $.each(grdPlanDatas, function (index, value) {
                $.each(that.allDatatoBind.closeOutProgresses, function (idx, val) {
                    if (val.closeOutRoleId == value.closeOutRoleId) {
                        val.planDate = value.planDate;
                        //val.submitedDate = value.submitedDate;
                    }
                })
            });
        },
        onSetDeleted: function (e) {
            if (confirm("Are you sure to set this project to Deleted?")) {
                kendo.ui.progress($(".container-main"), true);
                var that = this;
                var dt = that.allDatatoBind.toJSON();
                if (that.allDatatoBind.comments == null) {
                    alert("Comments must not empty!")
                    kendo.ui.progress($(".container-main"), false);
                } else {
                    $.ajax({
                        url: that.uriSetDeleted,
                        type: "PUT",
                        data: dt,
                        beforeSend: function () {
                            kendo.ui.progress($(".container-main"), true);
                        },
                        success: function (data) {
                            kendo.ui.progress($(".container-main"), false);
                            $("<div />").kendoDialog({
                                title: false,
                                closable: false,
                                modal: true,
                                content: "The form has been set to Deleted.",
                                actions: [{
                                    text: 'OK',
                                    action: function (e) {
                                        kendo.ui.progress($(".container-main"), true);
                                        window.location = "<%: WebAppUrl%>homeform.aspx";
                                        return true;
                                    }
                                }]
                            }).data("kendoDialog").open();
                        },
                        error: function (jqXHR) {
                            kendo.ui.progress($(".container-main"), false);

                        },
                        complete: function () {
                            kendo.ui.progress($(".container-main"), false);

                        }
                    });
                }
                <%--            e.preventDefault();
            that.set("btnSaveClick", true);
            that.set("btnSubmitClick", false);

            this.set('allDatatoBind.reasonClosureType', this.checkedRadioReason);

            this.saveGrdPlan();
            if (this.allDatatoBind.closeOutProgresses != null) {
                for (var i = 0; i < this.allDatatoBind.closeOutProgresses.length; i++) {
                    this.allDatatoBind.closeOutProgresses[i].planDate = kendo.toString(this.allDatatoBind.closeOutProgresses[i].planDate, "yyyy/MM/dd");
                }
            }

            var dt = that.allDatatoBind.toJSON();
            $.ajax({
                url: that.uriSave,
                type: "PUT",
                data: dt,
                beforeSend: function () {
                    kendo.ui.progress($(".container-main"), true);
                },
                success: function (data) {
                    if (that.addFile == true) {
                        $(".upload-attachment").getKendoUpload().upload();
                    }
                    else
                    {
                        kendo.ui.progress($(".container-main"), false);
                        $("<div />").kendoDialog({
                            title: false,
                            closable: false,
                            modal: true,
                            content: "The form has been saved successfully.",
                            actions: [{
                                text: 'OK',
                                action: function (e) {                                    
                                    window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                                    return true;
                                }
                            }]
                        }).data("kendoDialog").open();
                    }
                },
                error: function (jqXHR) {
                    kendo.ui.progress($(".container-main"), false);

                },
                complete: function () {
                    kendo.ui.progress($(".container-main"), false);

                }
            });
    --%>
            }
        },
        onSave: function (e) {
            kendo.ui.progress($(".container-main"), true);
            e.preventDefault();
            var that = this;
            that.set("btnSaveClick", true);
            that.set("btnSubmitClick", false);

            this.set('allDatatoBind.reasonClosureType', this.checkedRadioReason);

            this.saveGrdPlan();
            if (this.allDatatoBind.closeOutProgresses != null) {
                for (var i = 0; i < this.allDatatoBind.closeOutProgresses.length; i++) {
                    this.allDatatoBind.closeOutProgresses[i].planDate = kendo.toString(this.allDatatoBind.closeOutProgresses[i].planDate, "yyyy/MM/dd");
                    //this.allDatatoBind.closeOutProgresses[i].submitedDate = kendo.toString(this.allDatatoBind.closeOutProgresses[i].submitedDate, "yyyy/MM/dd");
                }
            }

            var dt = that.allDatatoBind.toJSON();
            $.ajax({
                url: that.uriSave,
                type: "PUT",
                data: dt,
                beforeSend: function () {
                    kendo.ui.progress($(".container-main"), true);
                },
                success: function (data) {
                    if (that.addFile == true) {
                        $(".upload-attachment").getKendoUpload().upload();
                    }
                    else
                    {
                        kendo.ui.progress($(".container-main"), false);
                        $("<div />").kendoDialog({
                            title: false,
                            closable: false,
                            modal: true,
                            content: "The form has been saved successfully.",
                            actions: [{
                                text: 'OK',
                                action: function (e) {                                    
                                    window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                                    return true;
                                }
                            }]
                        }).data("kendoDialog").open();
                    }
                },
                error: function (jqXHR) {
                    kendo.ui.progress($(".container-main"), false);

                },
                complete: function () {
                    kendo.ui.progress($(".container-main"), false);

                }
            });
        },

        //all attachment grid change
        arrAttChange: [],        

        onSubmit: function (e) {
            kendo.ui.progress($(".container-main"), true);
            e.preventDefault();
            var notices = [];
            var that = this;
            that.set("btnSubmitClick", true);
            that.set("btnSaveClick", false);
            var coProgg = this.allDatatoBind.closeOutProgresses;
            var coCheckList = Enumerable.From(coProgg)
                                .Where("$.closeOutRoleId == '" + that.allDatatoBind.roleLoginerDefault.id + "'")
                                .Select("$.closeOutCheckLists").ToArray()[0];
            this.saveGrdPlan();
            //Submit Rules:
            //1.	Reason for project closure harus diisi.
            //2.	Comment harus diisi.
            //3.	Semua checklist harus dicentang.
            //4.	Semua file type yang ada, harus ada file yang diupload.
            //5.	Semua plan date harus ada isinya.


            //#1 default is Completed

            //#2
            if (this.allDatatoBind.comments == "") {
                notices.push("<div>Comments field should be filled.</div>");
            }

            //#3
            var isAllCheckedForm = true;
            for (var i = 0; i < coCheckList.length; i++) {
                if (coCheckList[i].isChecked != true) {
                    isAllCheckedForm = false;
                    //notices.push("<div>All checkboxes must be checked.</div>");
                }
            }

            if (!isAllCheckedForm) {
                notices.push("<div>All checkboxes must be checked.</div>");
            }

            //#4

            $.each(that.dsListDocument, function (index, value) {
                var dtByType = null;
                var dt = that.dsAttachmentList.data();
                var type = value.documentType;
                dtByType = Enumerable.From(that.dsAttachmentList.data())
                          .Where("$.fileType == '" + type + "'")
                          .ToArray();

                if (dtByType.length == 0) {
                    notices.push("<div>Need to upload a file for type of " + type + " .</div>");
                }

            });

            //#5
            for (var i = 0 ; i < coProgg.length; i++) {
                if (!coProgg[i].planDate) {
                    notices.push("<div>Fill plan date for PIC - " + coProgg[i].closeOutRoleText + ".</div>");
                }
            }

            if (this.allDatatoBind.closeOutProgresses != null) {
                for (var i = 0; i < this.allDatatoBind.closeOutProgresses.length; i++) {
                    this.allDatatoBind.closeOutProgresses[i].planDate = kendo.toString(this.allDatatoBind.closeOutProgresses[i].planDate, "yyyy/MM/dd");
                }
            }

            if (notices.length > 0) {
                var contents = "<tr><td colspan= '2'>" + "Please complete the following rules :" + "</td></tr>";
                var no = 0;
                for (var i = 0; i < notices.length; i++) {
                    no++;
                    contents = contents + "<tr><td>" + no + ". </td>" + "<td>" + notices[i] + "</td></tr>";
                }

                $("<div />").kendoDialog({
                    title: "Submit Rules",
                    closable: false,
                    modal: true,
                    content: "<table>" + contents + "</table>",
                    actions: [{
                        text: 'OK',
                        action: function (e) {
                            kendo.ui.progress($(".container-main"), false);
                            return true;
                        }
                    }],
                }).data("kendoDialog").open();
                kendo.ui.progress($(".container-main"), false);

            }
            else {
                that.set("allDatatoBind.reasonClosureType", that.checkedRadioReason);
                //that.set("allDatatoBind.userLoginBadgeNo", _currBadgeNo);
                //that.set("allDatatoBind.userLoginEmail", _currEMail);
                //that.set("allDatatoBind.userLoginName", _currUserName);
                $("<div />").kendoDialog({
                    title: false,
                    closable: false,
                    modal: true,
                    content: "Are you sure you want to submit?",
                    actions: [{
                        text: 'OK',
                        action: function (e) {
                            kendo.ui.progress($("#<%: this.ID %>_divContainer"), true);

                            if (that.addFile == true) {
                                $.ajax({
                                    url: that.uriSave,
                                    type: "PUT",
                                    data: that.allDatatoBind.toJSON(),
                                    beforeSend: function () {
                                        kendo.ui.progress($(".container-main"), true);
                                    },
                                    success: function (data) {
                                        $(".upload-attachment").getKendoUpload().upload();
                                        kendo.ui.progress($("#<%: this.ID %>_divContainer"), false);
                                    },
                                    error: function (jqXHR) {
                                        kendo.ui.progress($("#<%: this.ID %>_divContainer"), false);
                                    }
                                })
                            }
                            else
                            {
                                $.ajax({
                                    url: that.uriSubmit,
                                    type: "PUT",
                                    data: that.allDatatoBind.toJSON(),
                                    beforeSend: function () {
                                        kendo.ui.progress($(".container-main"), true);
                                    },
                                    success: function (data) {
                                        kendo.ui.progress($("#<%: this.ID %>_divContainer"), false);
                                        $("<div />").kendoDialog({
                                            title: false,
                                            closable: false,
                                            modal: true,
                                            content: "The form has been submitted successfully.",
                                            actions: [{
                                                text: 'OK',
                                                action: function (e) {                                        
                                                        window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                                                    return true;
                                                }
                                            }]
                                        }).data("kendoDialog").open();
                                    },
                                    error: function (jqXHR) {
                                        kendo.ui.progress($("#<%: this.ID %>_divContainer"), false);

                                    },
                                    complete: function () {
                                        kendo.ui.progress($(".container-main"), false);
                                    }
                                })
                            }
                        }
                    },
                    {
                        text: 'Cancel',
                        action: function (e) {
                            return true;
                        }
                    }]
                 }).data("kendoDialog").open();
                }
        },
        validateForm2: function () {
            if (<%:this.ID%>_viewModel.isApprove)
            {
                approvedForm();
            }
            else
            {
                // user is in this project
                if (<%:this.ID%>_viewModel.loginerIsInCurrProj)
                {
                    strN = "<%=constantRoleDefi.PROJECT_MANAGER%>";
                    strN = strN.replace(/\s+/g, '').toLowerCase();
                    if ((<%:this.ID%>_viewModel.loginerIsInCurrTask) || ($.trim(_roleInProject.toLowerCase()) == strN))
                    {
                        $(".container-main").css("pointer-events", "visible");
                        //check default role for user that login in this task
                        //do function form for this default role                    
                    
                        switch ($.trim(<%:this.ID%>_viewModel.roleLoginerDefault.descript))
                        {
                            case "<%=constantRoleDefi.PROJECT_MANAGER%>":
                                projectManagerForm();

                                break;
                            case "<%=constantRoleDefi.LEAD_DESIGNER_C_M_ES%>":
                                leadDesignerFormMC();
                                break;
                            case "<%=constantRoleDefi.LEAD_DESIGNER_E_I_ES%>":
                                leadDesignerFormEI();
                                break;
                            case "<%=constantRoleDefi.MATERIAL_COORDINATOR%>":
                                materialCoordinatorForm();
                                break;
                            case "<%=constantRoleDefi.PROJECT_CONTROLLER%>":
                                projectControllerform();
                                break;
                            case "<%=constantRoleDefi.DOCUMENT_CONTROLLER%>":
                                documentControllerForm();
                                break;
                            case "<%=constantRoleDefi.PM_FINALIZE%>":
                                finalizeCloseOutForm();
                                break;
                        }
                    }
                    else roleNotInCurrentProgress();
                }
                else invalidRoleForm();
            }            
        },
        onApprove: function (e) {
            e.preventDefault();
            var that = this;
            $("<div />").kendoDialog({
                title: false,
                closable: false,
                modal: true,
                content: "Are you sure want to approve?",
                actions: [{
                    text: "OK",
                    action: function (e) {
                        var uri = "<%: WebApiUrl %>" + "closeout/approveRejectCloseOut?param=" + _projectId + "&isApproved=true";
                        $.ajax({
                            url: uri,
                            type: "PUT",
                            data: that.allDatatoBind.toJSON(),
                            success: function (data) {
                                $('.submit-close-btn').hide();
                                $('.attachment-class').hide();
                                //disable
                                $('.pro-man, .lea-des-mc, .lea-des-ei, .mat-coo, .pro-con, .doc-con').find('input, textarea, label').each(function () {
                                    $(this).attr('disabled', true);
                                });
                                window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                            },
                            error: function (jqXHR) {
                            }
                        });
                    }
                },
                {
                    text: 'CANCEL',
                    action: function (e) {
                        return true;
                    }
                }]
            }).data("kendoDialog").open();
        },
        onReject: function (e) {
            e.preventDefault();
            var uri = "<%: WebApiUrl %>" + "closeout/approveRejectCloseOut?param=" + _projectId + "&isApproved=false";
            var that = this;
            var dsReject = null;
            var id = that.curCopId;
            
            var dsReject = that.allDatatoBind.closeOutProgresses.filter(function (value) {
                return value.id !== id;
            });

            that.set('dsCOProgRejected', dsReject);

            $("<div />").kendoDialog({
                title: "Progress Rejection",
                closable: false,
                modal: true,
                content: '<table>' +
                            '<tr><td><div data-template="<%: this.ID %>_pm_reject_temp" data-bind="source: dsCOProgRejected"></td></tr>' +
                            '<tr><td><textarea data-bind="value:allDatatoBind.remarkRejectCLoseOut" rows="4" cols="50" style="width: 95%;resize: none;" placeholder="Put a remark here."></textarea></td></tr>' +
                        '</table>',
                open: function () {
                    kendo.bind($(this.wrapper), <%:this.ID%>_viewModel);
                },
                actions: [{
                    text: 'OK',
                    action: function (e) {
                        if (that.arrCOProgIdRejected.length > 0) {
                            if ($.trim(that.allDatatoBind.remarkRejectCLoseOut) == "") {
                                $("<div />").kendoDialog({
                                    title: false,
                                    closable: false,
                                    content: "A remark is needed.",
                                    modal: true,
                                    actions: [{
                                        text: "OK",
                                    }]
                                }).data("kendoDialog").open();
                                return false;
                            }
                            else {
                                //that.set("allDatatoBind.")
                                $.each(that.arrCOProgIdRejected, function (idx, el) {
                                    var arr = that.allDatatoBind.closeOutProgresses;
                                    var cop = Enumerable.From(arr)
                                                .Where("$.id == '" + parseInt(el) + "'")
                                                .ToArray()[0];
                                        //arr.find(x => x.id === parseInt(el));

                                    //that.allDatatoBind.closeOutProgresses.find(x => x.id === parseInt(el)).set("submitedDate", null);
                                    that.allDatatoBind.closeOutProgresses.Where("$.id == '" + parseInt(el) + "'").ToArray()[0].set("submitedDate", null);
                                });

                                $.ajax({
                                    url: uri,
                                    type: "PUT",
                                    data: that.allDatatoBind.toJSON(),
                                    success: function (data) {
                                        window.location = "<%: WebAppUrl%>ProjectCloseOutModuleForm.aspx";
                                    },
                                    error: function (jqXHR) {
                                    }
                                });
                                    return true;
                                }
                            }
                            else {
                                $("<div />").kendoDialog({
                                    title: false,
                                    closable: false,
                                    content: "Select the progress that will be rejected.",
                                    modal: true,
                                    actions: [{
                                        text: "OK",
                                    }]
                                }).data("kendoDialog").open();
                                return false;
                            }
                    }
                },
                {
                    text: 'CANCEL',
                    action: function (e) {
                        return true;
                    }
                }]
            }).data("kendoDialog").open();
        }
    });

    function getIndexById(id, dt) {
        var idx,
            l = dt.length;

        for (var j = 0; j < l; j++) {
            if (dt[j].id == id) {
                return j;
            }
        }
        return null;
    }

    function approvedForm() {
        $('.fin-clo').hide();
        $('.submit-close-btn').hide();
        $(".container-main").css("pointer-events", "none");
        $("<div />").kendoDialog({
            title: false,
            closable: false,
            modal: true,
            content: "This project had been approved.",
            actions: [{
                text: 'OK',
                action: function (e) {
                    return true;
                }
            }],
        }).data("kendoDialog").open();
    }

    function invalidRoleForm() {
        $("#<%:this.ID%>_divRoleFormTitle").hide();
        $('.fin-clo').hide();
        $('.submit-close-btn').hide();
        $("<div />").kendoDialog({
            title: false,
            closable: false,
            modal: true,
            content: "You are not in this project. Please check the another project that you are in it.",
            actions: [{
                text: 'OK',
                action: function (e) {
                    return true;
                }
            }],
        }).data("kendoDialog").open();
    }

    function roleNotInCurrentProgress() {
        $('.fin-clo').hide();
        $('.submit-close-btn').hide();
        $("<div />").kendoDialog({
            title: false,
            closable: false,
            modal: true,
            content: "You are not in current progress of this project.",
            actions: [{
                text: 'OK',
                action: function (e) {
                    return true;
                }
            }],
        }).data("kendoDialog").open();
    }

    function projectHadApproved() {
        //$(".container-main").hide();
        $('.fin-clo').hide();
        $('.submit-close-btn').hide();
        $(".container-main").css("pointer-events", "none");
        $("<div />").kendoDialog({
            title: false,
            closable: false,
            modal: true,
            content: "This project had been approved.",
            actions: [{
                text: 'OK',
                action: function (e) {

                    return true;
                }
            }],
        }).data("kendoDialog").open();
    }

    function projectManagerForm() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.PROJECT_MANAGER%>");
        if (<%:this.ID%>_viewModel.isApprove) {
            projectHadApproved();
        }
        else {
            $("#setDeletedBtn").show();
            //hide
            $('.lea-des-mc, .lea-des-ei, .mat-coo, .pro-con, .doc-con').hide();
            $('.fin-clo').hide();

            //disable
            $('.attachment-class').show();
            //enable
            $('.pro-man').find('input, textarea, label').each(function () {
                $(this).attr('disabled', false);
            });
        }
    }

    function leadDesignerFormMC() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.LEAD_DESIGNER_C_M_ES%>");
        if (<%:this.ID%>_viewModel.isApprove) {
            projectHadApproved();
        }
        else {
            //hide
            $('.mat-coo, .pro-con, .doc-con').hide();
            $('.fin-clo').hide();

            $('.attachment-class').show();

            //disable
            $('.pro-man, .lea-des-ei').find('input, textarea, label').each(function () {
                $(this).attr('disabled', true);
            });

            //enable
        }
    }
    function leadDesignerFormEI() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.LEAD_DESIGNER_E_I_ES%>");
        if (<%:this.ID%>_viewModel.isApprove) {
            projectHadApproved();
        }
        else {
            //hide
            $('.mat-coo, .pro-con, .doc-con').hide();
            $('.fin-clo').hide();

            $('.attachment-class').show();

            //disable
            $('.pro-man, .lea-des-mc').find('input, textarea, label').each(function () {
                $(this).attr('disabled', true);
            });

            //enable
        }
    }
    function materialCoordinatorForm() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.MATERIAL_COORDINATOR%>");
        if (<%:this.ID%>_viewModel.isApprove) {
            projectHadApproved();
        }
        else {
            //hide
            $('.pro-con, .doc-con').hide();
            $('.fin-clo').hide();

            $('.attachment-class').show();
            
            //disable
            $('.pro-man, .lea-des-mc, .lea-des-ei').find('input, textarea, label').each(function () {
                $(this).attr('disabled', true);
            });

            //enable
        }
    }

    function projectControllerform() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.PROJECT_CONTROLLER%>");
        if (<%:this.ID%>_viewModel.isApprove) {
            projectHadApproved();
        }
        else {
            //hide
            $('.doc-con').hide();
            $('.fin-clo').hide();
            $('.attachment-class').hide();
            //disable
            $('.pro-man, .lea-des-mc, .lea-des-ei, .mat-coo').find('input, textarea, label').each(function () {
                $(this).attr('disabled', true);
            });

            //enable
        }
    }

    function documentControllerForm() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.DOCUMENT_CONTROLLER%>");
        if (<%:this.ID%>_viewModel.isApprove) {
            projectHadApproved();
        }
        else {
            //hide
            $('.fin-clo').hide();
            $('.attachment-class').hide();
            //disable
            $('.pro-man, .lea-des-mc, .lea-des-ei, .mat-coo, .pro-con').find('input, textarea, label').each(function () {
                $(this).attr('disabled', true);
            });

            //enable
        }
    }

    function finalizeCloseOutForm() {
        <%:this.ID%>_viewModel.set("roleFormTitle", "<%=constantRoleDefi.PM_FINALIZE%>");
        //show
        if (<%:this.ID%>_viewModel.isApprove) {
            //$('.fin-clo').hide();
            projectHadApproved();
        }
        else
            $('.fin-clo').show();
        //hide
        $('.submit-close-btn').hide();
        $('.attachment-class').hide();
        //disable
        $('.pro-man, .lea-des-mc, .lea-des-ei, .mat-coo, .pro-con, .doc-con').find('input, textarea, label').each(function () {
            $(this).attr('disabled', true);
        });

        //enable
    }

    function <%:this.ID%>_allDisabedCloseOutProject() {
        $('#ProjectCloseOutControl_divContainer').find('*').each(function () {
            $(this).attr('disabled', true);
        });
        $(".attachment-class").hide();
        $(".divClearBoth").hide()
    }

    function correctForms() {
        <%:this.ID%>_viewModel.getRolesFromProject();
    }

    function reloadDataAfterSaveSubmit() {
        <%:this.ID%>_viewModel.dsAllCloseOut.read().then(function () {
            //<%:this.ID%>_viewModel.getRolesFromProject();

            //<%:this.ID%>_viewModel.getRoleForm();
            <%:this.ID%>_viewModel.validateForm2();
        });
    }



    $(document).ready(function () {
        if(_roleInProject != "projectManager"){ 
            $("#reasonTR").hide;
        } 


        $("#container-main").ajaxStart(function () {
            _showLoadingIndicator(true);
        });

        $("#container-main").ajaxComplete(function () {
            _showLoadingIndicator(false);
        });

        reloadDataAfterSaveSubmit();


        window.setTimeout(function () {
            $(".k-upload-button").click(function (e) {
                if ($.trim(<%:this.ID%>_viewModel.attDescription) == "") {
                    $("<div />").kendoDialog({
                        title: false,
                        closable: false,
                        modal: true,
                        content: "File description must not be empty. ",
                        actions: [{
                            text: 'OK',
                            action: function (e) {

                                return true;
                            }
                        }],
                    }).data("kendoDialog").open();
                    return false;
                }
            });


        }, 1);


        kendo.bind($("#<%: this.ID %>_divContainer"), <%:this.ID%>_viewModel);

        <%:this.ID%>_viewModel.dsLookupReason.bind("change", function (e) {
            window.setTimeout(function () {
                if (_roleInProject == "projectManager" && !<%:this.ID%>_viewModel.loginerIsInCurrTask) {
                    $("#<%:this.ID%>_divLoginRole").find('input, textarea, select').attr('disabled', 'disabled');
                    $("#<%:this.ID%>_pmPageR").find('input, textarea, select').attr('disabled', 'disabled');
                    $("#attachmentPane").find('input, textarea, select').attr('disabled', 'disabled');

                    $("#<%:this.ID%>_pmPageL").find('input, textarea, select').attr('disabled', 'disabled');
                    $("#<%:this.ID%>_pmPageR").find('input, textarea, select').attr('disabled', 'disabled');
                    $(".pro-man").find('input, textarea, select').attr('disabled', 'disabled');
                    $(".lea-des-mc").find('input, textarea, select').attr('disabled', 'disabled');
                    $(".lea-des-ei").find('input, textarea, select').attr('disabled', 'disabled');
                    $(".mat-coo").find('input, textarea, select').attr('disabled', 'disabled');
                    $(".pro-con").find('input, textarea, select').attr('disabled', 'disabled');
                    $(".doc-con").find('input, textarea, select').attr('disabled', 'disabled');
                    $("#attachmentPane").find('input, textarea, select').attr('disabled', 'disabled');

                    $("#<%:this.ID%>_save").attr('disabled', false);
                    $(".k-grid-edit").attr('disabled', false);

                }
            }, 10);
        }); 
    });

</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_pm_reject_temp">
    <tr>
        <td>            
            <input type="checkbox" style="vertical-align:middle;" value=#:id# data-bind="checked:arrCOProgIdRejected">
        </td>
        <td>&nbsp; </td>
        <td>
            <label>#=closeOutRoleText#</label>
        </td>
    </tr>
</script>

<script type="text/x-kendo-template" id="<%: this.ID %>_reasonProj_temp">
    <tr>
        <td>
            <input type="radio" name="rbReason" style="width:auto!important;margin:0!important;vertical-align:middle!important;" data-bind="events:{click: onCLickRadio}, checked:checkedRadioReason" value="#:VALUE#" )/>
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            <label>#=data.TEXT#</label>
        </td>
     </tr>  
</script>

<script type="text/x-kendo-template" id="<%:this.ID %>_checkListItem">
    <tr>
        <td>            
            <input type="checkbox" style="vertical-align:middle;" data-bind="events:{click: onClickCheckBox},checked:isChecked, disabled:isDisabled" id="#:id#">          
        </td>
        <td>&nbsp; </td>
        <td>
            <label>#=closeOutCheckListText#</label>
        </td>
    </tr>
</script>

<%--<script type="text/x-kendo-template" id="<%: this.ID %>_reasonProj_temp_lama">
     <div style="display:inline-block; padding-right: 50px;">
        <input type="radio" name="rbReason" style="width:auto!important;margin:0!important;vertical-align:middle!important;" data-bind="events:{click: onCLickRadio}, checked:checkedRadioReason" value="#:VALUE#" )/> #=data.TEXT#
     </div>    
</script>--%>


<div id="<%: this.ID %>_divContainer" class="container-page">
    
    <div class="container-main">        
        <div style="font-size:18px;padding:10px;float:right;" id="<%:this.ID%>_divRoleFormTitle">
            <%--<span id="curRoleTxt"></span>--%>
            <div>
                <span data-bind="text: roleFormTitle"></span>
            </div>
        </div>
        <div style="clear:both;" class="divClearBoth">&nbsp;</div>
        <div class="container-page" id="<%:this.ID%>_divLoginRole" style="display:none">
            <div class="sub-title-page">
                Login Role
            </div>
            <div class="content-page">
                <table>
                <tr>
                    <td style="width:5%;"><span>Login as</span>
                    </td>
                    <td style="width:5px;">:</td>
                    <td style="width:355px;">
                        <input data-role="dropdownlist"
                            data-value-primitive="true"
                            data-text-field="descript"
                            data-value-field="id"
                            data-bind="source: rolesLoginerValid, events: { change: onChangeCboRole, dataBound: onDataBoundCboRole }"
                            style="width: 350px;"
                        />
                    </td>
                    <td>
                        <div style="display:none;" data-bind="css: { refreshButton: showRefreshFormBtn }">
                            <a href="javascript:;" data-role="button"
                                data-bind='click: btnChooseRole'
                                data-icon='k-icon k-i-refresh-sm'
                            ><span>Refresh Form</span></a>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            </div>
        </div>
        <div style="clear:both;" class="divClearBoth">&nbsp;</div>
        <div class="sub-title-page">
            <span>Close Out</span>
        </div>
        
        <div class="content-page" id="<%:this.ID%>_divBodyCloseOut" style="display:none">
            <div id="<%:this.ID%>_pmPageL" style="display:inline-block; width: 500px;">
            <table class="pro-man spaceUnder">
                <tr>
                    <td style="width: 25%;">
                        <span>Project number</span>
                    </td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <span data-bind="text: allDatatoBind.projectNo"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>Project title</span>
                    </td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <span data-bind="text: allDatatoBind.projectTitle"></span>
                    </td>
                </tr>
                <tr id="reasonTR">
                    <td>
                        <span>Reason for project closure</span>
                    </td>
                    <td style="width: 1%;">:</td>
                    <td>
                        <div data-template="<%: this.ID %>_reasonProj_temp" data-bind="source: dsLookupReason"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <span>Comments</span>
                        <div>
                            <textarea data-bind="value: allDatatoBind.comments" style="width: 95%;"></textarea>
                        </div>
                    </td>
                </tr>
            </table>
                </div>
            <div id="<%:this.ID%>_pmPageR" style="display:inline-block; vertical-align: top;width: 500px;">
                <input type="button" class="k-button" style="display:none;width: 200px; text-align: center; font-size: 18px;" 
                    id="setDeletedBtn" data-bind="events: { click: onSetDeleted }" value="Set Deleted" />
            </div><br />
            <%--<div contenteditable="false" style="display: table;">                
                <div contentEditable="true" style="display:block; border: solid 1px #007e7a; min-height: 50px; width:800px;"></div>
            </div>--%>
            <div class="space-ver">&nbsp;</div>
            <div class="pro-man container-page">
                <div class="sub-title-page">
                    <span>PM Checklist</span>
                </div>
                <div class="content-page">
                    <div data-template="<%:this.ID %>_checkListItem" data-bind="source: allDatatoBind.closeOutProgresses[0].closeOutCheckLists"></div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div class="lea-des-mc container-page">
                <div class="sub-title-page">
                    <span>Lead Designer Mechanical Civil Checklist</span>
                </div>
                <div class="content-page">
                    <div data-template="<%:this.ID %>_checkListItem" data-bind="source: allDatatoBind.closeOutProgresses[1].closeOutCheckLists"></div>
                </div>
            </div>
             <div class="space-ver">&nbsp;</div>
            <div class="lea-des-ei container-page">
                <div class="sub-title-page">
                    <span>Lead Designer Electrical Instrument Checklist</span>
                </div>
                <div class="content-page">
                    <div data-template="<%:this.ID %>_checkListItem" data-bind="source: allDatatoBind.closeOutProgresses[2].closeOutCheckLists"></div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div class="mat-coo container-page">
                <div class="sub-title-page">
                    <span>Material Coordinator Checklist</span>
                </div>
                <div class="content-page">
                    <div data-template="<%:this.ID %>_checkListItem" data-bind="source: allDatatoBind.closeOutProgresses[3].closeOutCheckLists"></div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div class="pro-con container-page">
                <div class="sub-title-page">
                    <span>Project Controller Checklist</span>
                </div>
                <div class="content-page">
                    <div data-template="<%:this.ID %>_checkListItem" data-bind="source: allDatatoBind.closeOutProgresses[4].closeOutCheckLists"></div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div class="doc-con container-page">
                <div class="sub-title-page">
                    <span>Document Controller Checklist</span>
                </div>
                <div class="content-page">
                    <div data-template="<%:this.ID %>_checkListItem" data-bind="source: allDatatoBind.closeOutProgresses[5].closeOutCheckLists"></div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div id="attachmentPane" class="container-page">
                <div class="sub-title-page">
                    <span>Attachment</span>
                </div>
                <div class="content-page">
                    <table class="attachment-class" style="width: 100%;">
                        <tr>
                            <td style="width: 25%;">File Type
                            </td>
                            <td style="width: 1%;">:</td>
                            <td>
                                <input data-role="dropdownlist"
                                    data-value-primitive="true"
                                    data-text-field="documentType"
                                    data-value-field="id"
                                    data-index="0"
                                    data-bind="source: dsListDocument, events: { dataBound: fileTypeDataBound, change: fileTypeChange }"
                                    style="width: 30%;" />
                            </td>
                        </tr>
                        <tr>
                            <td>File Description
                            </td>
                            <td style="width: 1%;">:</td>
                            <td>
                                <input style="width: 70%;" data-bind="value: attDescription" />
                                <%--<div contentEditable="true" style="min-height: 50px;width:600px;border: solid 1px #007e7a;" data-bind="value:attDescription">
                                </div>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Select File
                            </td>
                            <td style="width: 1%;">:</td>
                            <td>
                                <input class="upload-attachment" name="closeOutFiles" type="file" data-role="upload"
                                    data-async="{ 
                                        autoUpload: false,
                                        batch: true,
                                        saveUrl: <%:this.ID%>_viewModel.saveHandler
                                    }"
                                    data-multiple="true"
                                    data-show-file-list="false"
                                    data-bind="events: {
                                    upload: onUploadAtt, select: onAttachmentSelect, error: onErrorAttachment, success: onSuccessAtthment
                                }" />
                            </td>
                        </tr>
                    </table>
                    <div class="space-ver"></div>
                    <div data-role="grid"
                        data-columns="[
                            {'field' : 'fileType', 'title' : 'File Type'},
                            {'field' : 'fileName', 'title' : 'File Name',
                                template: function (data) {
                                    if (data.refDocId) return '<a href=\'<%:DocServer%>' + 'getdoc.aspx?id=' + data.refDocId + '&op=n\' target=\'_blank\'>' + data.fileName + '</a>'; 
                                    return data.fileName;
                                }
                            },
                            {'field' : 'description', 'title' : 'File Description'},
                            {'field': 'btnDel',
                                'command': [{ name: 'destroy' }],
                                'title': '&nbsp;', width: '200px'
                            }
                            <%--{'field' : 'btnDel', 'command': {'name' : 'destroy', 'text' : '&nbsp;'}, width: '150px'}--%>
                        ]"
                        data-editable="popup",
                        data-bind="source: dsAttachmentList, events: { remove: onRemove, dataBound: grdAttachmentDtBound }">
                    </div>
                    <div class="space-ver"></div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div class="container-page">
                <div class="sub-title-page">
                    <span>Plan Date</span>
                </div>
                <div class="content-page">
                    <div id="tes" data-role="grid"
                        data-columns="[
                            {'field' : 'closeOutRoleText', 'title' : 'PIC'},
                            {'field' : 'closeOutRolePICName', 'title' : 'Name'},
                            {
                                field       : 'planDate',
                                title       : 'Plan Date',
                                format      : '{0:dd MMM yyyy}',
                               <%-- filterable: {
                                    ui: 'datetimepicker'
                                },--%>
                            },                        
                            {
                                field       : 'submitedDate',
                                title       : 'Actual Date',
                                format      : '{0:dd MMM yyyy}'
                            },                        
                            {'field': '23', command: [{ name: 'edit' }],
                            'title': '&nbsp;', width: '200px'
                            }
                        ]"
                        data-editable="inline"
                        data-auto-bind="false"
                        data-bind="source: dsCO2, events: { dataBound: grdPlanDataBound, cancel: cancelEditPlan }">
                    </div>
                </div>
            </div>
            <div class="space-ver">&nbsp;</div>
            <div class="submit-close-btn">
                <button id="<%:this.ID%>_save" class="k-button" data-bind="events: { click: onSave }">Save</button>
                <button id="<%:this.ID%>_submit" class="k-button" data-bind="events: { click: onSubmit }">Submit</button>
                <button id="<%:this.ID%>_close" class="k-button" data-bind="events: { click: onClose }">Close</button>
            </div>
            <div class="fin-clo">
                <button id="<%:this.ID%>_approve" class="k-button" data-bind="events: { click: onApprove }">Approve</button>
                <button id="<%:this.ID%>_reject" class="k-button" data-bind="events: { click: onReject }">Reject</button>
            </div>
        </div>
    </div>
</div>
<style>
    /*.container-main {
        pointer-events: none;
    }*/
    .refreshButton {
        display:block!important;
    }
    .container-page {
        margin-left: auto;
        margin-right: auto;
        border: none;
        overflow: hidden;
        height: 100%;
    }

    .sub-title-page {
        color: #fff;
        background-color: #007e7a;
        border: solid 1px #007e7a;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        padding: 10px;
    }

    .content-page {
        padding: 10px;
        border: solid 1px #007e7a;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    table {
        width: 100%;
    }

        table.spaceUnder td {
            padding-bottom: 0.5em;
        }

    .spaceVer {
        margin-bottom: 1em;
    }

    .submit-close-btn {
        text-align: right;
    }

        .submit-close-btn .k-button {
            width: 85px;
        }

    .fin-clo {
        text-align: right;
    }

        .fin-clo .k-button {
            width: 85px;
        }
</style>
