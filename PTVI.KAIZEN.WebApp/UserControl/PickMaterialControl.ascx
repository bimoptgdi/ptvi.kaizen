﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PickMaterialControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.PickMaterialControl" %>

<div id="<%: this.ID %>_PickMaterialContent">
    <div>
        <div id="<%: this.ID %>_MaterialGrid" 
            data-role="grid"
            data-bind="source: ds_material_list, events: { change: onPickChange }",
            data-pageable="true",
            data-selectable="true",
            data-height="400",
            data-filterable="extra: false, operators: { string: { contains: 'Contains', startswith: 'Starts With' }",
            data-columns='[
                { field: "materialDesc", title: "Material" },
                { field: "mrNo", width: 110, 
                  title: "MR No", width: "140px", template: "#=data.mrNo ? data.mrNo : \"-\" #" },
                { field: "reservation", width: 110, 
                  title: "Reservation", width: 110, 
                  template: "#=data.reservation && data.reservation != \"0\" ? data.reservation + \"/\" + data.reservationItemNo : \"-\" #" },
                { field: "poNo", width: 110, 
                  title: "PO", template: "#=data.poNo ? data.poNo + \"/\" + data.poItemNo : \"-\" #" },
                {
                    command: [
                        { text: "Select", click: <%: this.ID %>_pickMaterialViewModel.pickHandlerMaterial }
                    ], title: "&nbsp;", width: "100px"
                }
            ]'

        ></div>
        <input id="<%: this.ID %>_txtIdSelected" class="k-textbox" style="width: 100%;" type="hidden"/>
    </div>
    <div style="float:right;padding-top:20px;">
        <div data-role="button" class="k-button k-primary" data-bind="events: { click: onCloseSelectEvent }">Close</div>
    </div>
   
    <div style="clear:right;"></div>
    

</div>

<script>
    var <%: this.ID %>_pickMaterialViewModel = kendo.observable({
        projectId: "",
        selectedMaterial: {},
        listMaterial: [],
        setListMaterial: function(param) {
            this.set("listMaterial", param);
        },
    
        ds_material_list: new kendo.data.DataSource({
           transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "materialComponents/pickMaterialComponentByProjectId/" + <%: this.ID %>_pickMaterialViewModel.projectId
                    },
                    dataType: "json"
                },
            },
            pageSize: 20,
            schema: {
                model: {
                    id: "id"
                }
            }
        
        }),
        getListMaterial: function() {
            
            return this.get("listMaterial");
            
        },
        onPickChange: function (e) {
            this.set("selectedMaterial", e.sender.dataItem(e.sender.select()));
        },
        onSelectEvent: function (e) {
            alert("");
        },
        
        onCloseSelectEvent: function (e) {

        },
        pickHandlerMaterial: function (e) {
            
        }
    }); // View Model
        
    function <%: this.ID %>_DataBind() {
       
        kendo.bind($("#<%: this.ID %>_PickMaterialContent"), <%: this.ID %>_pickMaterialViewModel);
        
    }
    $(document).ready(function () {
        
       
    }); //doc ready
</script>