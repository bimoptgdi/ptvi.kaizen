﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConstructionMonitoringReportViewControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ConstructionMonitoringReportViewControl" %>

<script>
    common_getAllStatusIprom();
    var <%: this.ID %>_tenderId = "", <%: this.ID %>_popMaterialComponent;
    var <%: this.ID %>_viewConstructionMonitoringReportModel = kendo.observable({
        contractNo: ": -",
        vendorSelectedName: ": -",
        docNoSummary: ": -",
        dsTender: function () {
            kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), true);
            var that = this;
            $.ajax({
                url: _webApiUrl + "Tenders/ConstructionMonitoringReportListByTenderId/" +<%: this.ID %>_tenderId,
                type: "GET",
                success: function (data) {
                    that.set("contractNo", ": " + (data.contractNo ? data.contractNo : "-"));
                    that.set("vendorSelectedName", ": " + (data.vendorSelectedName ? data.vendorSelectedName : "-"));
                    that.set("docNoSummary", ": " + (data.docNoSummary ? data.docNoSummary : "-"));
                    kendo.ui.progress($("#<%: this.ID %>_ContractHead"), false);
                },
                error: function (jqXHR) {
                    kendo.ui.progress($("#<%: this.ID %>_ContractHead"), false);
                }
            });
        },
        dsStatusSource: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "lookup/findbytype/" + _statusIprom
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        }),
        dsListConstructionMonitoringReportManPower: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportManPower"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderManPowers/TenderManPowersByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsListConstructionMonitoringReportEquipment: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportEquipment"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportEquipment"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderEquipments/TenderEquipmentsByTenderId/" + <%: this.ID %>_tenderId;
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsListConstructionMonitoringReportMaterial: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportMaterial"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportMaterial"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        handover: { defaultValue: true, type: "boolean" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderMaterials/TenderMaterialsByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsListConstructionMonitoringReportInstallation: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportInstallation"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportInstallation"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderInstallations/TenderInstallationsByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsListConstructionMonitoringReportPunchlist: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportPunchlist"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListConstructionMonitoringReportPunchlist"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "TenderPunchlists/TenderPunchlistsByTenderId/" + <%: this.ID %>_tenderId
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        })
    });

    function onTenderMaterialHandoverActiveChange(e) {
        if ($(".k-edit-form-container").length > 0)
            return 0;

        var active = e.checked;
        if (confirm('Are you sure to ' + (active ? 'handover' : 'takeover') + ' this material?')) {
            var grid = $("#<%: this.ID %>_grdListConstructionMonitoringReportMaterial").getKendoGrid();
            aaa = e;
            var item = grid.dataItem($(e.sender.element.parent().parent().parent().parent()));
            item.set('handover', active);

            $("#<%: this.ID %>_grdListConstructionMonitoringReportMaterial").getKendoGrid().dataSource.sync();
        }
        else {
            e.sender.value(!active); // undo
        }
    };
    $(document).ready(function () {
        <%: this.ID %>_popMaterialComponent = $("#<%: this.ID %>_popMaterialComponent").kendoWindow({
            title: "Sponsor List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

    });

</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_tenderPlan-template">
    <div>
        <div class='subTitleTemplate'>Start:</div>
        <div>#:data.planStartDate ? kendo.toString(data.planStartDate,'dd MMM yyyy') : "-"#</div>
        <div class='subTitleTemplate'>Finish:</div>
        <div>#:data.planFinishDate ? kendo.toString(data.planFinishDate,'dd MMM yyyy') : "-"#</div>
    </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_tenderActual-template">
    <div>
        <div class='subTitleTemplate'>Start:</div>
        <div>#:data.actualStartDate ? kendo.toString(data.actualStartDate,'dd MMM yyyy') : "-"#</div>
        <div class='subTitleTemplate'>Finish:</div>
        <div>#:data.actualFinishDate ? kendo.toString(data.actualFinishDate,'dd MMM yyyy') : "-"#</div>
    </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_completePercentage-template">
    <div>
        <div class='subTitleTemplate'>Percentage:</div>
        <div>#: data.completePercentage ? data.completePercentage + ' %' : '-'#</div>
        <div class='subTitleTemplate'>Status:</div>
        <div>#:issueStatus ? common_getStatusIpromDesc(issueStatus) : '-'#</div>
    </div>
</script>

<div id="<%: this.ID %>_ConstructionMonitoringReportContent">
    <div id="<%: this.ID %>_ContractHead">
        <div>
            <label class="labelForm">Contract No</label>
            <span data-bind="text: contractNo"></span>
        </div>
        <div>
            <label class="labelForm">Contractor Name</label>
            <span data-bind="text: vendorSelectedName"></span>
        </div>
        <div>
            <label class="labelForm">EWP No</label>
            <span data-bind="text: docNoSummary"></span>
        </div>
    </div>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Man Power</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportManPower" 
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        field: 'position', title: 'Position',
                        width: 110
                    },
                    {
                        field: 'quantity', title: 'Qty',
                        width: 40
                    },
                    {
                        field: 'qualification', title: 'Qualification',
                        width: 170
                    },
                    {
                        field: 'license', title: 'License',
                        width: 110
                    },
                    {
                        title: 'Status',
                        template: '#:common_getStatusIpromDesc(status)#',
                        width: 75
                    }
                ]"
                data-editable="false"
                data-bind="source: dsListConstructionMonitoringReportManPower"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdManPower-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Position<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Position is required" placeholder="Please Input Position" name="position" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Quantity<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="number" required validationMessage="Quantity is required" placeholder="Please Input Quantity" name="quantity" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Qualification<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Qualification is required" placeholder="Please Input Qualification" name="qualification" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">License</td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" placeholder="Please Input License" name="license" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Equipment</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportEquipment" 
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        field: 'equipment', title: 'Equipment',
                        width: 300
                    },
                    {
                        field: 'quantity', title: 'Qty',
                        width: 40
                    },
                    {
                        title: 'Status',
                        template: '#:common_getStatusIpromDesc(status)#',
                        width: 75
                    }
                ]"
                data-editable="false"
                data-bind="source: dsListConstructionMonitoringReportEquipment"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdEquipment-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Equipment<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Equipment is required" placeholder="Please Input Equipment" name="equipment" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Quantity<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="number" required validationMessage="Quantity is required" placeholder="Please Input Quantity" name="quantity" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Material</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportMaterial" 
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        title: 'No',
                        template: kendo.template($('#<%: this.ID %>_tenderMaterialNo-template').html()),
                        width: 100
                    },
                    {
                        title: 'Material',
                        template: '#: data.materialDesc ? data.materialDesc + \' %\' : \'-\'#',
                        width: 165
                    },
                    {
                        title: 'Qty',
                        template: kendo.template($('#<%: this.ID %>_tenderMaterialQty-template').html()),
                        width: 100
                    },
                    {
                        title: 'Qty\'s Location', hidden: true,
                        template: '#:data.remainingLocation#',
                        width: 110
                    },
                    {
                        title: 'Status',
                        template: kendo.template($('#<%: this.ID %>_tenderMaterialHandover-template').html()),
                        width: 80
                    }
                ]"
                data-editable="false"
                data-bind="source: dsListConstructionMonitoringReportMaterial"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>

    <script type="text/x-kendo-template" id="<%: this.ID %>_tenderMaterialNo-template">
        <div>
            # if (data.mrNo) {#
            <div class='subTitleTemplate'>MR No:</div>
            <div>#:data.mrNo#</div>
            #}#
            # if (data.poNo) {#
            <div class='subTitleTemplate'>PO No:</div>
            <div>#:data.poNo + '/' + data.poItemNo#</div>
            #}#
            # if (data.reservation && data.reservation != '0') {#
            <div class='subTitleTemplate'>Reservation:</div>
            <div>#:data.reservation + '/' + data.reservationItemNo#</div>
            #}#
        </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_tenderMaterialQty-template">
        <div>
            <div class='subTitleTemplate'>Quantity:</div>
            <div>#:data.usedQuantity ? data.usedQuantity : "-"#</div>
            <div class='subTitleTemplate'>Used:</div>
            <div>#:data.usedQuantity ? (data.remainingQuantity ? data.usedQuantity - data.remainingQuantity : data.usedQuantity) : "-"#</div>
            <div class='subTitleTemplate'>Remaining:</div>
            <div>#:data.remainingQuantity ? data.remainingQuantity : "-"#</div>
        </div>
    </script>
    <script type="text/x-kendo-template" id="<%: this.ID %>_tenderMaterialHandover-template">
        <div>
            <div class='subTitleTemplate'>Status:</div>
            <div>#:data.status ? data.status : '-'#</div>
            <div class='subTitleTemplate'>Handover:</div>
            <input type="checkbox" disabled="disabled" #: handover ? "checked" : "" # /> Handover
            <!--input type="checkbox" disabled="disabled" data-role="switch" data-checked="#: handover ? true : false #" data-on-label="Yes" data-off-label="No" data-change="onTenderMaterialHandoverActiveChange" /-->
        </div>
    </script>

    <script id="<%: this.ID %>_editGrdMaterial-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Material<span style="color:red">*</span></td>
                    <td><input id="<%: this.ID %>_TenderMaterialId" name="id" class="k-textbox" style="display:none;" type="text" />
                        <input id="<%: this.ID %>_TenderMaterialMaterialDesc" style="width: 400px" name="materialDesc" class="k-textbox" type="text" disabled="disabled" />
                        <div
                            id="<%: this.ID %>_btnTenderMaterial" 
                            class="k-button">
                            ...
                        </div></td>
                </tr>   
                <tr>
                    <td style="width:100px">MR No</td>
                    <td>
                        <input id="<%: this.ID %>_TenderMaterialMrNo" style="width: 200px" name="mrNo" class="k-textbox" type="text" disabled="disabled" placeholder="Material" required validationMessage="Material is required" />
                   </td>
                </tr>
                <tr>
                    <td style="width:100px">PO No</td>
                    <td>
                        <input id="<%: this.ID %>_TenderMaterialPoNo" style="width: 200px" name="poNo" class="k-textbox" type="text" disabled="disabled" />
                   </td>
                </tr>
                <tr>
                    <td style="width:100px">Reservation No</td>
                    <td>
                        <input id="<%: this.ID %>_TenderMaterialReservation" style="width: 200px" name="reservation" class="k-textbox" type="text" disabled="disabled" />
                   </td>
                </tr>
                <tr>
                    <td style="width:100px">Handover</td>
                    <td>
                        <input type="checkbox" name="handover" />
                   </td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Installation</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportInstallation" 
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        title: 'Task',
                        template: '#:data.task#',
                        width: 110
                    },
                    {
                        title: 'Weight',
                        template: '#: data.weightPercentage ? data.weightPercentage + \' %\' : \'-\'#',
                        width: 60
                    },
                    {
                        title: 'Plan Date',
                        template: kendo.template($('#<%: this.ID %>_tenderPlan-template').html()),
                        width: 80
                    },
                    {
                        title: 'Actual Date',
                        template: kendo.template($('#<%: this.ID %>_tenderActual-template').html()),
                        width: 80
                    },
                    {
                        title: 'Complete',
                        template: kendo.template($('#<%: this.ID %>_completePercentage-template').html()),
                        width: 70
                    },
                    {
                        title: 'Issue',
                        template: '#: data.issue ? data.issue : \'-\'#',
                        width: 130
                    }
                ]"
                data-editable="false"
                data-bind="source: dsListConstructionMonitoringReportInstallation"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdInstallation-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Task<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Task is required" placeholder="Please Input Task" name="task" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Weight (%)</td>
                    <td><input class="k-textbox" type="number" placeholder="Please Input Weight" name="weightPercentage" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planStartDateInstallation" placeholder="Plan Start Date" name="planStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planFinishDateInstallation" placeholder="Plan Finish Date" name="planFinishDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualStartDateInstallation" placeholder="Actual Start Date" name="actualStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualFinishDateInstallation" placeholder="Actual Finish Date" name="actualFinishDate" onfocus/></td>
                </tr>
                <tr>
                    <td style="width:100px">Complete (%)<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="number" placeholder="Please Input Complete" name="completePercentage" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Issue</td>
                    <td>
                        <textarea name="issue" class="k-textbox" style="height: 60px; width: 100%;" maxlength="2000" ></textarea></td>
                </tr>
                <tr>
                    <td style="width:100px">Issue Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
    <br />
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend">Punchlist</legend>
            <div id="<%: this.ID %>_grdListConstructionMonitoringReportPunchlist" 
                data-role="grid"
                data-sortable="true"
                data-columns="[
                    {
                        field: 'punchlist', title: 'Punchlist',
                        width: 110
                    },
                    {
                        title: 'Plan Date',
                        template: kendo.template($('#<%: this.ID %>_tenderPlan-template').html()),
                        width: 80
                    },
                    {
                        title: 'Actual Date',
                        template: kendo.template($('#<%: this.ID %>_tenderActual-template').html()),
                        width: 80
                    },
                    {
                        title: 'Complete',
                        template: kendo.template($('#<%: this.ID %>_completePercentage-template').html()),
                        width: 70
                    },
                    {
                        title: 'Issue',
                        template: '#: data.issue ? data.issue : \'-\'#',
                        width: 140
                    }
                ]"
                data-editable="false"
                data-bind="source: dsListConstructionMonitoringReportPunchlist"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>
    <script id="<%: this.ID %>_editGrdPunchlist-editTemplate" type="text/x-kendo-tmpl">
        <div style="padding:10px">            
            <table style="width:600px">
                <tr>
                    <td style="width:100px">Punchlist<span style="color:red">*</span></td>
                    <td><input class="k-textbox" type="text" style="width: 100%;" required validationMessage="Punchlist is required" placeholder="Please Input Punchlist" name="punchlist" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planStartDatePunchlist" placeholder="Plan Start Date" name="planStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Plan Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planFinishDatePunchlist" placeholder="Plan Finish Date" name="planFinishDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Start Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualStartDatePunchlist" placeholder="Actual Start Date" name="actualStartDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Actual Finish Date</td>
                    <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualFinishDatePunchlist" placeholder="Actual Finish Date" name="actualFinishDate" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Complete (%)</td>
                    <td><input class="k-textbox" type="number" placeholder="Please Input Complete" name="completePercentage" onfocus="this.value = this.value;" /></td>
                </tr>
                <tr>
                    <td style="width:100px">Issue</td>
                    <td>
                        <textarea name="issue" class="k-textbox" style="height: 60px; width: 100%;" maxlength="2000" ></textarea></td>
                </tr>
                <tr>
                    <td style="width:100px">Issue Status</td>
                    <td><input data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource" data-text-field="text" data-value-field="value" name="status" data-value-primitive="true" /></td>
                </tr>
            </table>
        </div>
    </script>
</div>
    <style>
        .km-switch {
            margin-top: .5em !important;
            display: inherit !important;
        }
    </style>
