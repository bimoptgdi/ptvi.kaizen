﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DesignerDrawingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DesignerDrawingControl" %>
<%@ Register Src="~/UserControl/EWPWeightHoursControl.ascx" TagPrefix="uc1" TagName="DWGWeightHoursControl1" %>
<%@ Register Src="~/UserControl/DataInputDsgrProdControl.ascx" TagPrefix="uc1" TagName="DataInputDsgrProdControl" %>
<%@ Register Src="~/UserControl/DataInputFinalResultControl.ascx" TagPrefix="uc1" TagName="DataInputFinalResultControl" %>
<script type="text/x-kendo-template" id="addDDTemplate">
    <div class="projectDocContainer">
        <div class="" style="width: 700px; display: inline-block">
            <div class="labelProjectDoc">EWP/EDC</div>
            <div style="width: 250px; display: inline-block">
                <div style="display: inline-block; width=220px; padding-left: 10px">
                    <select required="required" id="ddEwp" data-role="dropdownlist"
                           data-placeholder="select EWP"
                           data-value-primitive="true"
                           data-text-field="docNo"
                           data-value-field="docNo"
                           data-bind="
                                      source: ProjectDocSource,
                                      value: docNo,
                                    "
                    style="width: 200px"></select> </div>
                <div style="display: inline-block; width=80px"></div>
            </div>
        </div>
        <div class="" >
            <div class="labelProjectDoc" style="height: 15px;margin-top: 10px;margin-bottom: 10px;vertical-align: middle;">Doc. No</div>
            <div class="labelProjectDoc" style="height: 15px;width: 500px; margin-top: 10px;margin-bottom: 10px;vertical-align: middle;"><span id="ddDocNoSpan"></span></div>
            <input name="DocNo." required="required" class=" k-textbox" style="width: 100px; display: none" type="text" id="ddDocNo" data-bind="value: drawingNo" readonly/>
        </div>
        <div class="" >
            <div class="labelProjectDoc" style="height: 15px;margin-top: 10px;margin-bottom: 10px;vertical-align: middle;">Doc. Title</div>
            <div class="labelProjectDoc" style="height: 15px;width: 330px; margin-top: 10px;margin-bottom: 10px;vertical-align: middle;"><span id="ddDocTitleSpan"></span></div>
            <input name="Project Doc." required="required" class=" k-textbox" style="width: 300px; display: none" type="text" id="ddDocTitle" data-bind="value: drawingTitle" readonly/>
        </div>
<%--        <div class="" >
            <div class="labelProjectDoc" style="height: 15px;margin-top: 10px;margin-bottom: 10px;vertical-align: middle;">Discipline</div>
            <div class="labelProjectDoc" style="height: 15px;width: 330px; margin-top: 10px;margin-bottom: 10px;vertical-align: middle;"><span id="disciplineTextSpan"></span></div>
            <input name="Discipline" required="required" class="k-textbox" style="width: 200px; display: none" type="text" id="disciplineText" data-bind="value: discipline" readonly />
        </div>
        <div class="" >
            <div class="labelProjectDoc">Drawing No.</div><div class="labelProjectDoc">
            <input name="Drawing No." class="k-textbox" style="width: 100px" type="text" id="drawingNo" data-bind="value: drawingNo" required="required"/></div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Drawing Title</div><div class="labelProjectDoc">
            <input name="Drawing Title" class="k-textbox" style="width: 250px" type="text" id="drawingTitle" data-bind="value: drawingTitle" required="required"/></div>
        </div>--%>
        <div class="" >
            <div class="labelProjectDoc">Number of Drawing</div><div class="labelProjectDoc">
                <input name="drawingCount" class="k-textbox" required="required" type='number' min=0 style="width: 70px"/>
            </div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Plan Start Date</div><div class="labelProjectDoc"> 
                <input name="Plan Start Date" required="required" data-format="dd MMM yyyy" id="ddDtPlanStartDate" data-role="datepicker"
                     placeholder="Plan Start Date" style="width:180px" data-bind="value: planStartDate"/></div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Plan Finish Date</div><div class="labelProjectDoc"> 
                <input name="Plan Finish Date" data-format="dd MMM yyyy" required="required" id="ddDtPlanFinishDate" data-role="datepicker" 
                    placeholder="Plan Finish Date" style="width:180px" data-bind="value: planFinishDate"/></div>
        </div>
        <div class="" id="ddDtActualStartDateContainer">
            <div class="labelProjectDoc">Actual Start Date</div><div class="labelProjectDoc" > <input id="ddDtActualStartDate" data-role="datepicker"
              data-format="dd MMM yyyy" data-bind="value: actualStartDate"/></div>
        </div>
        <div class="" id="ddDtActualFinishDateContainer">
            <div class="labelProjectDoc">Actual Finish Date</div><div class="labelProjectDoc" > <input id="ddDtActualFinishDate" data-role="datepicker" 
             data-format="dd MMM yyyy" data-bind="value: actualFinishDate"/></div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Status</div><div class="labelProjectDoc" > <input name="Status" required="required" id="ddCbStatus" data-role="combobox"
                   data-placeholder="select Status"
                   data-value-primitive="true"
                   data-text-field="text"
                   data-value-field="value"
                   data-bind="
                              source: StatusSource,
                              value: status,
                            "
            style="width: 250px" /></div>
        </div>
        <div class="" id="ddUcEngineeringContainer" >
            <div class="labelProjectDoc">Designer</div><div class="labelProjectDoc" > 
                <input name="Engineer" id="ddCbEngineer" data-role="dropdownlist"
                   data-value-primitive="true"
                   data-text-field="employeeName"
                   data-value-field="employeeId"
                   data-bind="
                              source: EngineeringSource,
                              value: documentById,
                            "
            style="width: 250px" #= (_roleInProject != 'projectManager' && _roleInProject != 'projectEngineer' && _roleInProject != 'PC' && _roleInProject != 'otherPositionProject' && _currRole != "ADM" && _currRole != "LDES" && _currRole != "LDEI" ) ? 'disabled' : '' #/>
            </div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Progress(%)</div><div class="labelProjectDoc">
            <input name="Progress" class="k-textbox" required="required" type='number' id="ddProgress" min=0
                       data-bind="value: progress" style="width: 70px"/></div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Remark</div><div class="labelProjectDoc"><textarea class=" k-textbox textArea" name="remark"/></textarea></div>
        </div>
        <div class="" >
            <div class="labelProjectDoc">Cut off Date</div><div class="labelProjectDoc"> 
                <input id="ddDtCutOfDate" name="cutOfDate" placeholder="Cut off Date" style="width:180px" data-role="datepicker" data-format="dd MMM yyyy" />

            </div>
        </div>
    <div>
</script>
<style>
    .labelProjectDoc{
        display: inline-block;
        width: 120px;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }

    .inputProjectDoc{
        display: inline-block;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .projectDocContainer{
        width: 700px;
    }

    .textArea{
        width: 300px;
        height: 50px;
    }
</style>


<div id="<%: this.ID %>_FormContent">
    <div>
        <div id="<%: this.ID %>_grdDocuments" data-role="grid" data-bind="source: DesignerDwgSource, events: { edit: editProjectDoc, dataBound: onDataBound, save: saveDoc }" 
            data-columns="[
            { 
                field: 'docNo', title: 'Document', 
                attributes: { style: 'vertical-align: top !important;' },
                template: kendo.template($('#<%: this.ID %>_document-template').html()),
                width: 200
            },
            {   
                field:'disciplineName()', title: 'Discipline & Drawing', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_diciplineDrawing-template').html()),
                width: 150 },
            { 
                field: 'docNo', title: 'Planned', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_planned-template').html()),
                width: 100
            },
            { 
                field: 'docNo', title: 'Actual', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_actual-template').html()),
                width: 100
            },
            { 
                field: 'progress', title: 'Status', 
                attributes: { style: 'vertical-align: top !important;' }, filterable: false,
                template: kendo.template($('#<%: this.ID %>_status-template').html()),
                width: 140
            },                               
            {
                field:'statusName()', title: 'Status', 
                template:'#= statusName() #', hidden:true, filterable: false 
            },
            {
                field:'documentByName', title: 'Designer & Remark', filterable: false,
                attributes: { style: 'vertical-align: top !important;' }, 
                template: kendo.template($('#<%: this.ID %>_designerRemark-template').html())               
            },
            {
                title: 'Hours(s)', width: 65, filterable: false, 
                template: kendo.template($('#<%: this.ID %>_Weight-template').html()),
                attributes: { style: 'text-align: right; vertical-align: top !important;' }
            }
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.DesignerProduction))
            { %>,
            {
                title: 'Parameter', width: 80, filterable: false, 
                template: kendo.template($('#<%: this.ID %>_DsgrProd-template').html()),
                attributes: { style: 'text-align: center; vertical-align: top !important;' }
            }
            <% } %>
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageDesignDoc))
            { %>,
            {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}},{name:'destroy'}] }
            <% } %>
            ]" 
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageDesignDoc))
            { %>
            data-toolbar= "[{name: 'create', text: 'Add New Drawing'}]",
            <% } %>
            data-editable= "{mode: 'popup', confirmation:true, window:{width: 700, title: 'Designer Drawing', modal: true}, template:kendo.template($('#addDDTemplate').html()) }",
            data-filterable="true"
            data-pageable="true", 
            data-sortable="true"
            ></div>
 
    </div>
    <div id="<%: this.ID %>_popWeightPopUp">
        <uc1:DWGWeightHoursControl1 runat="server" ID="DWGWeightHours" />
    </div>
    <div id="<%: this.ID %>_popDsgrProdPopUp">
        <uc1:DataInputDsgrProdControl runat="server" ID="DataInputDsgrProdControl" />
    </div>
    <div id="<%: this.ID %>_popFinalResultPopUp">
        <uc1:DataInputFinalResultControl runat="server" ID="DataInputFinalResultControl" />
    </div>
</div>
<script id="<%: this.ID %>_document-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        No:
    </div>
    <div>
        #= data.docNo #
    </div>
    <div class='subColumnTitle'>
        Title:
    </div>
    <div> 
        #= data.docTitleText # 
    </div>
</script>
<script id="<%: this.ID %>_diciplineDrawing-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Discipline:
    </div>
    <div>
        #= data.disciplineName() ? data.disciplineName() : '-' #
    </div>
    <div class='subColumnTitle'>
        Number of Drawing:
    </div>
    <div> 
        #= data.drawingCount ? data.drawingCount : '-'# 
    </div>
</script>
<script id="<%: this.ID %>_planned-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.planStartDate ? kendo.toString(data.planStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.planFinishDate ? kendo.toString(data.planFinishDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Rev:
    </div>
    <div> 
        #= data.revision ? data.revision : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_actual-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Start Date:
    </div>
    <div> 
        #= data.actualStartDate ? kendo.toString(data.actualStartDate,'dd MMM yyyy') : '-' # 
    </div>
    <div class='subColumnTitle'>
        Finish Date:
    </div>
    <div> 
        #= data.actualFinishDate ? kendo.toString(data.actualFinishDate,'dd MMM yyyy') : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_status-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Status:
    </div>
    <div> 
        #= data.statusName() #  
    </div>
    <div class='subColumnTitle'>
        Progress:
    </div>
    <div> 
        #= data.progress? data.progress : 0 #\%
    </div>
    <div class='subColumnTitle'>
        Cut of Date:
    </div>
    <div> 
        #= data.cutOfDate ? kendo.toString(data.cutOfDate,'dd MMM yyyy') : '-' # 
    </div>
</script>
<script id="<%: this.ID %>_designerRemark-template" type="text/x-kendo-template">
    <div class='subColumnTitle'>
        Designer:
    </div>
    <div> 
        #= data.documentByName ? data.documentByName : '-'#  
    </div>
    <div class='subColumnTitle'>
        Remark:
    </div>
    <div> 
        #= data.remark ? data.remark : '-'#
    </div>
</script>
<script id="<%: this.ID %>_Weight-template" type="text/x-kendo-template">
    <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.EWPWeightHours))
    { %>
        <a href="\#" data-bind="events: { click: openWeightPopUpEdit }">#: data.weightHours ? data.weightHours : "N/A" #</a>
    <% } else { %>
        #: data.weightHours ? data.weightHours : "N/A" #
    <% } %>
</script>

<script id="<%: this.ID %>_DsgrProd-template" type="text/x-kendo-template">
    # if (data.documentGroup == "EWP") { #
        <a href="\#" data-bind="events: { click: openDsgrProd }">#= data.parameterValue ? kendo.toString(data.parameterValue, "n") : "N/A"#</a>
    # } else {#
        <a href="\#" data-bind="events: { click: openFinalResult }">#= data.parameterValue ? kendo.toString(data.parameterValue, "n") : "N/A"#</a>
    # } #
</script>

<script>
    var <%: this.ID %>_popWeightPopUp, <%: this.ID %>_popDsgrProdPopUp, <%: this.ID %>_popFinalResultPopUp;
    var <%: this.ID %>_designerDwgSource = new kendo.data.DataSource({
            transport: {
                create: {
                    type: "POST", dataType: "json",
                    url: _webApiUrl + "ProjectDocuments/postProjectDocumentObjectBadgeNo/" + _currBadgeNo
                    },
                read    : { dataType: "json",
                    url: _webApiUrl + "ProjectDocuments/listPDByProjectIdDocType/" + _projectId + "?docType=DWG"
                    },
                update: {
                    type: "PUT", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "ProjectDocuments/putProjectDocumentObject/" + e.id;
                    }

                },
                destroy: {
                    type: "DELETE", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "ProjectDocuments/" + e.id;
                    }

                },
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        options.cutOfDate = kendo.toString(options.cutOfDate, 'MM/dd/yyyy');
                        options.planStartDate = kendo.toString(options.planStartDate, 'MM/dd/yyyy');
                        options.planFinishDate = kendo.toString(options.planFinishDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.actualStartDate = null;
                        options.actualFinishDate = null;
                        options.projectId = _projectId;
                        options.docNo = $("#ddDocNo").val();
                        options.drawingNo = $("#ddDocNo").val() + "-D#";
                        options.documentById = _currBadgeNo;
                        options.documentByName = common_getEmployeeSName(_currBadgeNo);
                        options.documentByEmail = common_getEmployeeSEmail(_currBadgeNo);
                        options.docType = "DWG";
                        options.createdBy = _currNTUserID;
                        options.updatedBy = _currNTUserID;
                        options.projectDocumentWeightHours = DWGWeightHours_localData ? DWGWeightHours_localData : [];
                        return options;
                    }
                    if (operation == "update") {
                        options.cutOfDate = kendo.toString(options.cutOfDate, 'MM/dd/yyyy');
                        options.planStartDate = kendo.toString(options.planStartDate, 'MM/dd/yyyy');
                        options.planFinishDate = kendo.toString(options.planFinishDate, 'MM/dd/yyyy');
                        options.actualStartDate = kendo.toString(options.actualStartDate, 'MM/dd/yyyy');
                        options.actualFinishDate = kendo.toString(options.actualFinishDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        var ddl = $("#ddCbEngineer").getKendoDropDownList();
                        if (ddl.selectedIndex > -1) {
                            options.documentByName = ddl.dataSource.data()[ddl.selectedIndex].employeeName;
                            options.documentByEmail = ddl.dataSource.data()[ddl.selectedIndex].employeeEmail;
                        }
                        options.project = null;
                        options.projectId = _projectId;
                        options.assignmentType = "DWG";
                        options.updatedBy = _currNTUserID;
                        options.projectDocumentWeightHours = DWGWeightHours_localData ? DWGWeightHours_localData : [];
                        return options;
                    }
                }
            },
            error: function (e) {
                //TODO: handle the errors
                _showDialogMessage("Error", e.xhr.responseText, "");
                //alert(e.xhr.responseText);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        docType: { type: "string" },
                        docDesc: { type: "string" },
                        docNo: { type: "string" },
                        docTitle: { type: "string" },
                        docTitleText: { type: "string" },
                        drawingNo: { type: "string" },
                        drawingTitle: { type: "string" },
                        drawingCount: { type: "number" },
                        cutOfDate: { type: "date", defaultValue: null },
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date" },
                        actualFinishDate: { type: "date" },
                        disciplinedrawingNo: { type: "string" },
                        status: { type: "string" },
                        remark: { type: "string" },
                        discipline: { type: "string" },
                        statusdrawingNo: { type: "string" },
                        remarkdrawingNo: { type: "string" },
                        executordrawingNo: { type: "string" },
                        executorAssignmentdrawingNo: { type: "string" },
                        progress: { type: "number" },
                        documentById: { type: "string" },
                        documentByName: { type: "string" },
                        documentByEmail: { type: "string" },
                        documentTrafic: { type: "string" },
                        revision: { type: "number" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                        parameterValue: { type: "number" }
                    },
                    statusName: function () {
                        if (this.status)
                            return getStatusEngineer(this.status);
                        else return "";
                    },
                    disciplineName: function () {
                        if (this.discipline)
                            return getDiscipline(this.discipline);
                        else return "";
                    }
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_designerDwgSource.read();
                        alert("Process failed: "+d);
                        return 0;
                    } else{

                if (d.length) {
                    $.each(d, function (key, value) {
                        if (value.status)
                            value.statusText = getStatus(value.status);

                        var docTitle = value.docTitle;
                        var t1 = docTitle!=null ? docTitle.split("-"):"";
                        var docTitle1 = t1[0];
                        var docTitle2 = "";
                        var disciplineID = "";
                        var docTitle3 ="";
                        if (t1[1]) {
                            var t2Temp = t1[1];
                            var t2 = t2Temp.split(" ");
                            docTitle2 = value.ewpDocType;
                            disciplineID = value.position;
                            t2.shift();
                            if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                            docTitle3 = t2.join(" ");
                            value.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                            if(docTitle2 == "")
                                value.docTitleText = value.docTitle;
                        } else {
                            value.docTitleText = value.docTitle;
                        }

                    });
                } else {
                        var docTitle = d.docTitle;
                        var t1 = docTitle != null ? docTitle.split("-") : "";
                        var docTitle1 = t1[0];
                        var docTitle2 = "";
                        var disciplineID = "";
                        var docTitle3 ="";
                        if (t1[1]) {
                            var t2Temp = t1[1];
                            var t2 = t2Temp.split(" ");
                            docTitle2 = d.ewpDocType;
                            disciplineID = d.position;
                            t2.shift();
                            if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                            docTitle3 = t2.join(" ");
                            d.docTitleText = docTitle1 + " - " + docTitle2 + " " + disciplineID + " " + docTitle3;
                            if (docTitle2 == "")
                                d.docTitleText = d.docTitle;
                        } else {
                            d.docTitleText = d.docTitle;
                        }

                    statusText = getStatus(d.status);
                }
                        return d;
                     //   <%: this.ID %>_designerDwgSource.read();
                    }

                },
            },
        pageSize: 10,
        change: function (e) {
        }
        });

    var <%: this.ID %>_projectDocSource = new kendo.data.DataSource({
        transport: {
            read: {
                async: true,
                dataType: "json",
                url: _webApiUrl + "ProjectDocuments/listPDByProjectIdEWPDC/" + _projectId 
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    docNo: { type: "string" },
                    docTitle: { type: "string" },
                    planStartDate: { type: "date" },
                    planFinishDate: { type: "date" },
                    actualStartDate: { type: "date" },
                    actualFinishDate: { type: "date" }
                }
            },
            parse: function (response) {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].planFinishDate) {
                        var planFinsihDateEwp = kendo.parseDate(response[i].planFinishDate, "yyyy-MM-ddTHH:mm:ss");
                        planFinsihDateEwp.setDate(planFinsihDateEwp.getDate() - 5);
                        response[i].planFinishDateEwp = planFinsihDateEwp;
                    }
                    var docTitle = response[i].docTitle;
                    var t1 = docTitle != null ? docTitle.split("-") : "";
                    if (t1[1]) {
                        var t2Temp = t1[1];
                        var t2 = t2Temp.split(" ");
                        var t2Text = t2[0] == _useReportCode ? curEngPos + "R" : curEngPos;
                        t2.shift();
                        if (t2[0] ==<%: this.ID %>_viewModel.disciplineID) t2.shift();
                        var dt3 = t2.join(" ");
                        response[i].docTitle = t1[0] + " - " + response[i].ewpDocType + " - " + response[i].position + " - " + dt3;
                    } else {

                    }

                }
                return response;
            }
        }
    });

    var <%: this.ID %>_discipline = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                url: _webApiUrl + "lookup/findbytype/discipline"
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    });

    var <%: this.ID %>_engineeringSource = new kendo.data.DataSource({
        transport: {
            read: {
                dataType: "json",
                cache: false,
                url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1?assignmentType=PD|TSD&PrID=" + _projectId
            },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    employeeId: { type: "string" },
                    employeeName: { type: "string" },
                    employeeEmail: { type: "string" }
                }
            }
        }
    });

    var <%: this.ID %>_viewModel = new kendo.observable({
        planStartDate: null,
        planFinishDate: null,
        documentById: "",
        documentByName: "",
        documentByEmail: "",
        DesignerDwgSource: <%: this.ID %>_designerDwgSource,
        DisciplineSource: <%: this.ID %>_discipline,
        EngineeringSource:<%: this.ID %>_engineeringSource,
        ProjectDocSource:<%: this.ID %>_projectDocSource,
        saveDoc: <%: this.ID %>_saveDoc,
        StatusSource: new kendo.data.DataSource({
            transport: {
                read: {
                    dataType: "json",
                    url: _webApiUrl + "lookup/findbytype/statusEngineer"
                },
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        text: { type: "string" },
                        value: { type: "string" },
                    }
                }
            }
        }),
        editProjectDoc: <%: this.ID %>_EditProjectDocument,
        openDiscipline: OpenDiscipline,
        engineerChange: EngineerChange,
        onDataBound: function (e) {
            var grid = $("#<%: this.ID %>_grdDocuments").getKendoGrid();

            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if ((_roleInProject == "engineer") && (_currBadgeNo != model.documentById)) {
                    $(this).find(".k-grid-edit").hide();
                    $(this).find(".k-grid-delete").hide();
                }
            });
        },
        openWeightPopUpEdit: function (e) {
            DWGWeightHours_localData = [];
            this.openWeightPopUp(e);
        },
        openWeightPopUp: function (e) {
            if (!$("#ddEwp").val())
            {
                alert("Please input EWP/EDC No before input Weight!")
                return;
            }
            DWGWeightHours_viewWeightHoursModel.set("no", e ? e.data.docNo : "-");
            DWGWeightHours_viewWeightHoursModel.set("title", e ? e.data.docTitleText : "-");
            DWGWeightHours_docId = e ? e.data.id : 0;
            DWGWeightHours_viewWeightHoursModel.set("visibleTitle", e ? true : false);

            $.ajax({
                url: "<%:WebApiUrl%>" + "ProjectDocuments/getPDByDocNo/" + (!e ? $("#ddEwp").val() : !e.data ? $("#ddEwp").val() : !e.data.docNo ? $("#ddEwp").val() : e.data.docNo),
                async: false,
                type: "GET",
                success: function (data) {
                    DWGWeightHours_docType = "DWG" + data.groupDocType;
                    kendo.ui.progress($("#DWGWeightHours_grdWeightLIst"), false);
                },
                error: function (jqXHR) {
                    kendo.ui.progress($("#DWGWeightHours_grdWeightLIst"), false);
                }
            });

            //DWGWeightHours_docType = "DWGEWP";
            DWGWeightHours_bind();
            kendo.bind($("#DWGWeightHours_ConstructionMonitoringReportContent"), DWGWeightHours_viewWeightHoursModel);
            DWGWeightHours_viewWeightHoursModel.dsTreeListWeightListSource.read();
            DWGWeightHours_viewWeightHoursModel.set("otherUpdate",
                function (data) {
                    kendo.ui.progress($("#DWGWeightHours_grdWeightLIst"), true);

                    if (DWGWeightHours_docId) {
                        $.ajax({
                            url: "<%:WebApiUrl%>" + "ProjectDocumentWeightHours/totalWeightByDocID/" + DWGWeightHours_docId,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                var dataDWG = <%: this.ID %>_designerDwgSource.get(DWGWeightHours_docId);
                                dataDWG.set("weightHours", data)
                                kendo.ui.progress($("#DWGWeightHours_grdWeightLIst"), false);
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#DWGWeightHours_grdWeightLIst"), false);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#DWGWeightHours_grdWeightLIst"), false);
                    }
                });
            $("#<%: this.ID %>_popWeightPopUp").prevUntil("k-window-titlebar").find(".k-window-title").html(_projectNo + " - " + _projectDesc + " (Weight Hour(s))");
            <%: this.ID %>_popWeightPopUp.center().open();
        },
        openDsgrProd: function (e) {
            DataInputDsgrProdControl_viewDesignerProdModel.set("no", e.data.docNo);
            DataInputDsgrProdControl_viewDesignerProdModel.set("title", e.data.docTitleText);
            DataInputDsgrProdControl_viewDesignerProdModel.set("participationValue", e.data.participationValue)
            DataInputDsgrProdControl_docId = e.data.id;
            DataInputDsgrProdControl_docType = "DWG";
            DataInputDsgrProdControl_docBadgeNo = e.data.documentById;
            DataInputDsgrProdControl_bind();
            kendo.bind($("#DataInputDsgrProdControl_ConstructionMonitoringReportContent"), DataInputDsgrProdControl_viewDesignerProdModel);
            DataInputDsgrProdControl_viewDesignerProdModel.dsDesignerProdSource.read();
            DataInputDsgrProdControl_viewDesignerProdModel.dsComplexitySource.read();
            DataInputDsgrProdControl_viewDesignerProdModel.set("otherUpdate",
                function (data) {
                    kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), true);
                    
                    if (DataInputDsgrProdControl_docId) {
                        $.ajax({
                            url: "<%:WebApiUrl%>" + "ProjectDocuments/getParameterValue/" + DataInputDsgrProdControl_docId,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                var dataDWG = <%: this.ID %>_designerDwgSource.get(DataInputDsgrProdControl_docId);
                                dataDWG.set("parameterValue", data)
                                kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                    }
                });
            $("#<%: this.ID %>_popDsgrProdPopUp").prevUntil("k-window-titlebar").find(".k-window-title").html(_projectNo + " - " + _projectDesc + " (Total of Production))");
            <%: this.ID %>_popDsgrProdPopUp.center().open();
        },
        openFinalResult: function (e) {
            DataInputFinalResultControl_viewFinalResultModel.set("no", e.data.docNo);
            DataInputFinalResultControl_viewFinalResultModel.set("title", e.data.docTitleText);
            DataInputFinalResultControl_viewFinalResultModel.set("participationValue", e.data.participationValue)
            DataInputFinalResultControl_docId = e.data.id;
            DataInputFinalResultControl_docType = "DWG";
            DataInputFinalResultControl_docBadgeNo = e.data.documentById;
            DataInputFinalResultControl_bind();

            $.ajax({
                url: "<%:WebApiUrl%>" + "ProjectDocuments/getCustomProjectDocumentByOid/" + DataInputFinalResultControl_docId,
                async: false,
                type: "GET",
                success: function (data) {
                    DataInputFinalResultControl_viewFinalResultModel.set("remarksActualResults", data.remarksActualResults);
                    DataInputFinalResultControl_viewFinalResultModel.set("remarksActualResultsTmp", data.remarksActualResults);
                    DataInputFinalResultControl_viewFinalResultModel.set("remarksDesignKpi", data.remarksDesignKpi);
                    DataInputFinalResultControl_viewFinalResultModel.set("remarksDesignKpiTmp", data.remarksDesignKpi);
                },
                error: function (jqXHR) {
                }
            });

            kendo.bind($("#DataInputFinalResultControl_ConstructionMonitoringReportContent"), DataInputFinalResultControl_viewFinalResultModel);
            DataInputFinalResultControl_viewFinalResultModel.dsFinalResultSource.read();
            DataInputFinalResultControl_viewFinalResultModel.set("otherUpdate",
                function (data) {
                    kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), true);
                    
                    if (DataInputFinalResultControl_docId) {
                        $.ajax({
                            url: "<%:WebApiUrl%>" + "ProjectDocumentFinalResults/totalFinalResultByDocID/" + DataInputFinalResultControl_docId,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                var dataDWG = <%: this.ID %>_designerDwgSource.get(DataInputFinalResultControl_docId);
                                dataDWG.set("parameterValue", data)
                                kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                            },
                            error: function (jqXHR) {
                                kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                            }
                        });
                    } else {
                        kendo.ui.progress($("#DataInputFinalResultControl_grdTypeOfDrawingLIst"), false);
                    }
                });
            $("#<%: this.ID %>_popFinalResultPopUp").prevUntil("k-window-titlebar").find(".k-window-title").html(_projectNo + " - " + _projectDesc + " (Final Result)");
            <%: this.ID %>_popFinalResultPopUp.center().open();
        }
    });

    function <%: this.ID %>_saveDoc(e) {
        if ((e.model.planFinishDate < e.model.planStartDate) && (e.model.planFinishDate!=null)) {
                e.preventDefault();
                alert("Plan Finish Date cannot start earlier than Plan start date");
            }
        if ((e.model.actualFinishDate < e.model.actualStartDate) && (e.model.actualFinishDate != null)) {
                e.preventDefault();
                alert("Actual Finish Date cannot start earlier than Actual start date");
            }
        }

    function EngineerChange(e) {
        e.data.documentById = e.sender.dataSource.data()[e.sender.selectedIndex].employeeId;
        e.data.documentByName = e.sender.dataSource.data()[e.sender.selectedIndex].employeeName;
        e.data.documentByEmail = e.sender.dataSource.data()[e.sender.selectedIndex].employeeEmail;
        e.sender.dataSource._data[e.sender.selectedIndex].dirty = true;
        <%: this.ID %>_viewModel.set("documentById", e.data.documentById);
        <%: this.ID %>_viewModel.set("documentByName", e.data.documentByName);
        <%: this.ID %>_viewModel.set("documentByEmail", e.data.documentByEmail);
    }

    function <%: this.ID %>_EditProjectDocument(e) {
        <%: this.ID %>_engineeringSource.read().then(function () {
<%--            var ddActive = Enumerable.From(<%: this.ID %>_engineeringSource.data()).Where("$.isActive == true").ToArray();
            if (ddActive.length == 0) {
                _showDialogMessage("Error Message", "You are not listed as Designer<br/>Please add yourself as designer in Assign Engineer and Designer Tab", "");
                $('#<%: this.ID %>_grdDocuments').data("kendoGrid").cancelChanges();
            }--%>
        var ewpNo = "";

            //prepare input weight data
        DWGWeightHours_localData = [];
        if (e.model.isNew()) {
            $("#ddDtActualStartDateContainer").hide();
            $("#ddDtActualFinishDateContainer").hide();
            $("#ddUcEngineeringContainer").hide();
            $("#ddRdExecutorContainer").hide();
            $(".k-window-title").html("Add");

            //prepare input weight
            $(".k-edit-buttons").prepend('<a class="k-button" onClick="<%: this.ID %>_viewModel.openWeightPopUp()" style="float: left"><span class="k-icon k-i-grid-layout"></span> Input Weight</a>');
        } else {
            $("#ddDtActualStartDateContainer").show();
            $("#ddDtActualFinishDateContainer").show();
            $("#ddUcEngineeringContainer").show();
            $("#ddRdExecutorContainer").show();
            $(".k-window-title").html("Edit");
            $("#ddDocTitleSpan").text(e.model.docTitleText);
            $("#ddDocTitle").val(e.model.docTitle);
            $("#ddDocNo").val(e.model.docNo);
            ewpNo = e.model.docNo.substring(0, e.model.docNo.lastIndexOf("-"));
            if (ewpNo == "" || ewpNo.indexOf("-")<0) ewpNo = e.model.docNo;
            $("#ddEwp").getKendoDropDownList().value(ewpNo);
            $("#disciplineText").val(getDiscipline(e.model.discipline));
            $("#disciplineTextSpan").text(getDiscipline(e.model.discipline));
        }

        if (_planStartDate) {
            $("#ddDtPlanStartDate").getKendoDatePicker().min(_planStartDate);
            $("#ddDtPlanFinishDate").getKendoDatePicker().min(_planStartDate);
            $("#ddDtCutOfDate").getKendoDatePicker().min(_planStartDate);
        }

        if (_planFinishDate) {
            $("#ddDtPlanStartDate").getKendoDatePicker().max(_planFinishDate);
            $("#ddDtPlanFinishDate").getKendoDatePicker().max(_planFinishDate);
            $("#ddDtCutOfDate").getKendoDatePicker().max(_planFinishDate);
        }

        $("#ddDtPlanStartDate").getKendoDatePicker().bind("change", function () {
            $("#ddDtPlanFinishDate").getKendoDatePicker().min(this.value());
            $("#ddDtCutOfDate").getKendoDatePicker().min(this.value());
        });

        $("#ddDtPlanFinishDate").getKendoDatePicker().bind("change", function () {
            $("#ddDtPlanStartDate").getKendoDatePicker().max(this.value());
            $("#ddDtCutOfDate").getKendoDatePicker().max(this.value());
        });

        if (e.model.planFinishDate)
            $("#ddDtPlanStartDate").getKendoDatePicker().max(e.model.planFinishDate);
        if (e.model.planStartDate)
            $("#ddDtPlanFinishDate").getKendoDatePicker().min(e.model.planStartDate);

        if (e.model.id) {
            $("#ddDtActualStartDate").getKendoDatePicker().bind("change", function () {
                $("#ddDtActualFinishDate").getKendoDatePicker().min(this.value());
                if($("#ddDtActualStartDate").getKendoDatePicker().value()>$("#ddDtActualFinishDate").getKendoDatePicker().value())
                    $("#ddDtActualFinishDate").getKendoDatePicker().value($("#ddDtActualStartDate").getKendoDatePicker().value())

            });

            $("#ddDtActualFinishDate").getKendoDatePicker().bind("change", function () {
                $("#ddDtActualStartDate").getKendoDatePicker().max(this.value());
            });

            if (e.model.actualFinishDate)
                $("#ddDtActualStartDate").getKendoDatePicker().max(e.model.actualFinishDate);
            if (e.model.actualStartDate)
                $("#ddDtActualFinishDate").getKendoDatePicker().min(e.model.actualStartDate);
        }

      //  if (<%: this.ID %>_projectDocSource.data().length == 0) <%: this.ID %>_projectDocSource.read();

        if (e.model.docNo) {
            $("#ddDocNoSpan").text(e.model.drawingNo ? e.model.drawingNo : e.model.docNo);
            var getPlanDateEwp = Enumerable.From(<%: this.ID %>_projectDocSource.data()).Where("$.docNo == '" + ewpNo + "'").ToArray()[0];
            if (getPlanDateEwp != null && getPlanDateEwp.planStartDate) {
                $("#ddDtPlanStartDate").getKendoDatePicker().min(getPlanDateEwp.planStartDate);
                $("#ddDtPlanFinishDate").getKendoDatePicker().min(getPlanDateEwp.planStartDate);
                $("#ddDtCutOfDate").getKendoDatePicker().min(getPlanDateEwp.planStartDate);
            }
            if (getPlanDateEwp != null && getPlanDateEwp.planFinishDateEwp) {
                
                $("#ddDtPlanStartDate").getKendoDatePicker().max(getPlanDateEwp.planFinishDateEwp);
                $("#ddDtPlanFinishDate").getKendoDatePicker().max(getPlanDateEwp.planFinishDateEwp);
                $("#ddDtCutOfDate").getKendoDatePicker().max(getPlanDateEwp.planFinishDateEwp);
            }

        }
        var dataItm = e;
        $("#ddEwp").getKendoDropDownList().bind("change", function (e) {
            //var dataItem = this.dataItem(e.item.index());
            var idx = $("#ddEwp").getKendoDropDownList().selectedIndex;
            var docDS = <%: this.ID %>_projectDocSource.data().at(idx);
            $("#ddDocNoSpan").text(docDS.docNo + "-D#");
            $("#infoReplace").html("");
            $("#ddDocNoSpan").after("<sup id='infoReplace' style='color: red; padding-left:10px'>\# will be replaced with sequence number</sup>");

            $("#ddDocTitleSpan").text(docDS.docTitle);
            $("#ddDocTitle").val(docDS.docTitle);
            dataItm.model.docTitle = docDS.docTitle;
            dataItm.model.drawingTitle = docDS.docTitle;

            //var discipline = getDiscipline(docDS.discipline);
            //dataItm.model.discipline = docDS.discipline;
            //$("#disciplineText").val(discipline);
            //$("#disciplineTextSpan").text(discipline);
            var diff = (docDS.planFinishDate - docDS.planStartDate) / (1000 * 60 * 60 * 24);
            var m = _designDrawingExDefaultSpan;
            var ddsPlanStart = new Date(docDS.planStartDate);
            var ddsPlanFinish = new Date(docDS.planFinishDate);
            if (diff > _designDrawingDefaultSpan) m = _designDrawingDefaultSpan;
            var sdate = ddsPlanStart.setDate(ddsPlanStart.getDate() + parseInt(m));
            var fdate = ddsPlanFinish.setDate(ddsPlanFinish.getDate() - parseInt(m));

            if (docDS.planFinishDateEwp)
            {
                $("#ddDtPlanStartDate").getKendoDatePicker().min(docDS.planStartDate);
                $("#ddDtPlanStartDate").getKendoDatePicker().max(docDS.planFinishDate);
                $("#ddDtPlanFinishDate").getKendoDatePicker().min(docDS.planStartDate);
                $("#ddDtPlanFinishDate").getKendoDatePicker().max(docDS.planFinishDate);
                $("#ddDtCutOfDate").getKendoDatePicker().max(docDS.planFinishDate);
            } else {
                if (_planFinishDate) {
                    $("#ddDtPlanStartDate").getKendoDatePicker().max(_planFinishDate);
                    $("#ddDtPlanFinishDate").getKendoDatePicker().max(_planFinishDate);
                    $("#ddDtCutOfDate").getKendoDatePicker().max(_planFinishDate);
                }
            }
            $("#ddDtPlanStartDate").getKendoDatePicker().value(new Date(sdate));
            $("#ddDtPlanStartDate").getKendoDatePicker().trigger("change");
            $("#ddDtPlanFinishDate").getKendoDatePicker().value(new Date(fdate));
            $("#ddDtPlanFinishDate").getKendoDatePicker().trigger("change");
            $("#ddDtPlanFinishDate").getKendoDatePicker().min($("#ddDtPlanStartDate").getKendoDatePicker().value());

        })
        $("#ddCbEngineer").getKendoDropDownList().dataSource.read();

        });
    }

    function getStatusEngineer(stat) {
        var status = "";
        $.ajax({
            url: "<%: WebApiUrl %>" + "lookup/FindByTypeAndValue/statusEngineer|" + stat,
            type: "GET",
            async: false,
            success: function (result) {
                if (result.length > 0)
                    status = $.trim(result[0].text);
            },
            error: function (error) {
            }
        });
        return status;
    }


    function OpenDiscipline(e) {
        var listContainer = e.sender.list.closest(".k-list-container");
        listContainer.width(listContainer.width() + kendo.support.scrollbar());
    }

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel); 
        

        <%: this.ID %>_popWeightPopUp = $("#<%: this.ID %>_popWeightPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc + " (Weight Hour(s))",
            modal: true,
            visible: false,
            resizable: false,
            width: 900,
            height: 500
        }).data("kendoWindow");

        <%: this.ID %>_popDsgrProdPopUp = $("#<%: this.ID %>_popDsgrProdPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc + " (Total of Production)",
            modal: true,
            visible: false,
            resizable: false,
            width: 900,
            height: 500
        }).data("kendoWindow");

        <%: this.ID %>_popFinalResultPopUp = $("#<%: this.ID %>_popFinalResultPopUp").kendoWindow({
            title: _projectNo + " - " + _projectDesc + " (Final Result)",
            modal: true,
            visible: false,
            resizable: false,
            width: 900,
            height: 500
        }).data("kendoWindow");

    });

</script>