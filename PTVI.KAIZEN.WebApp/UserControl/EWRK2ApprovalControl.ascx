﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EWRK2ApprovalControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.EWRK2ApprovalControl" %>

<div id="<%: this.ID %>_RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_inputProjectLegend">EWR Detail</legend>
            <div>
                <label class="labelFormWeight">Date Issued</label>
                <input data-role="datepicker" id="<%: this.ID %>_dateIssued" name="<%: this.ID %>_dateIssued" data-format="dd MMM yyyy" data-bind="value: requestData.dateIssued, enabled: isEnabled" placeholder="Date Issued" />
            </div>
            <div>
                <label class="labelFormWeight">Area</label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_area" name="<%: this.ID %>_area" data-bind="value: requestData.area, enabled: isEnabled" placeholder="Area" required />
            </div>
            <div>
                <label class="labelFormWeight">Subject</label>

                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_subject" name="<%: this.ID %>_subject" data-bind="value: requestData.subject, enabled: isEnabled" placeholder="Subject" required />
            </div>
            <div>
                <div id="view">
                    <fieldset class="gdiFieldset">
                        <legend class="gdiLegend">Category of Work</legend>
                        <div class="k-form-field" id="<%: this.ID %>_categoryofwork">
                            <input type="checkbox" id="<%: this.ID %>_eqCat1" data-bind="checked: items, enabled: isEnabled" class="k-checkbox" value="safetyhazard">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqCat1" style="font-size: 10pt; padding-right: 20px;">Safety/Health Hazard</label>

                            <input type="checkbox" id="<%: this.ID %>_eqCat2" data-bind="checked: items, enabled: isEnabled" class="k-checkbox" value="productionloss">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqCat2" style="font-size: 10pt; padding-right: 20px;">Production Loss < 1 Year Payback (Please Justify)</label>

                            <input type="checkbox" id="<%: this.ID %>_eqCat3" data-bind="checked: items, enabled: isEnabled" class="k-checkbox" value="capitalplan">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqCat3" style="font-size: 10pt; padding-right: 20px;">Capital Plan</label>

                            <input type="checkbox" id="<%: this.ID %>_eqCat4" data-bind="checked: items, enabled: isEnabled" class="k-checkbox" value="environmental">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqCat4" style="font-size: 10pt; padding-right: 20px;">Environmental</label>

                            <br />
                            <input type="checkbox" id="<%: this.ID %>_eqCat5" data-bind="checked: items, enabled: isEnabled" class="k-checkbox" value="generalimprovement">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqCat5" style="font-size: 10pt; padding-right: 20px;">General Improvement and Others</label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div>
                <label class="labelFormWeight">Account Code</label>

                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_acccountCode" name="<%: this.ID %>_acccountCode" data-bind="value: requestData.accCode, enabled: isEnabled" placeholder="Account Code" required />
            </div>
            <div>
                <label class="labelFormWeight">Network No</label>

                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_networkNo" name="<%: this.ID %>_networkNo" data-bind="value: requestData.networkNo, enabled: isEnabled" placeholder="Network No" required />
            </div>
            <div>
                <label class="labelFormWeight">Budget Allocated</label>
                <input data-role="numerictextbox"
                    data-format="c"
                    data-min="0"
                    data-bind="value: requestData.budgetAlloc, enabled: isEnabled"
                    style="width: 180px">
            </div>
            <div>
                <div id="viewRequestedWork">
                    <fieldset class="gdiFieldset">
                        <legend class="gdiLegend">Type of Requested Work</legend>
                        <div class="k-form-field" id="<%: this.ID %>_typeofrequestedwork">
                            <input type="checkbox" id="<%: this.ID %>_eqReqWork1" data-bind="checked: itemsRequestedWork, enabled: isEnabled" class="k-checkbox" value="budgetestimate">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqReqWork1" style="font-size: 10pt; padding-right: 20px;">Budget Estimate</label>
                        
                            <input type="checkbox" id="<%: this.ID %>_eqReqWork2" data-bind="checked: itemsRequestedWork, enabled: isEnabled" class="k-checkbox" value="purchaseonly">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqReqWork2" style="font-size: 10pt; padding-right: 20px;">Purchase Only</label>

                            <input type="checkbox" id="<%: this.ID %>_eqReqWork3" data-bind="checked: itemsRequestedWork, enabled: isEnabled" class="k-checkbox" value="projectmanagement">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqReqWork3" style="font-size: 10pt; padding-right: 20px;">Project Management (Capital Only)</label>

                            <br />
                            <input type="checkbox" id="<%: this.ID %>_eqReqWork4" data-bind="checked: itemsRequestedWork, enabled: isEnabled" class="k-checkbox" value="engineeringwork">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqReqWork4" style="font-size: 10pt; padding-right: 20px;">Engineering Work Package (Design and Drafting)</label>

                            <br />
                            <input type="checkbox" id="<%: this.ID %>_eqReqWork5" data-bind="checked: itemsRequestedWork, enabled: isEnabled, events: { change: onOtherChange }" class="k-checkbox" value="other">
                            <label class="k-checkbox-label" for="<%: this.ID %>_eqReqWork5" style="font-size: 10pt; padding-right: 20px;">Other (Please Specify)</label><br />
                            <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_otherRequestWork" name="<%: this.ID %>_otherRequestWork" data-bind="value: otherDescription, enabled: isOtherChecked" placeholder="Other" />
                        </div>
                    </fieldset>
                </div>
            </div>
            <div>
                <label class="labelFormWeight">Date of Completion Required</label>
                <input data-role="datepicker" id="<%: this.ID %>_dateOfCompletion" name="<%: this.ID %>_dateOfCompletion" data-format="dd MMM yyyy" data-bind="value: requestData.dateCompletion, enabled: isEnabled" placeholder="Date Completion" />
            </div>

            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Detail of Work Requirement</legend>
                
                    <div>
                        <label class="labelFormWeight">1. Problem Statement<span style="color: red">*</span></label>
                        <textarea name="<%: this.ID %>_problemStatement" class="k-textbox" style="width: 500px;" rows="4" data-bind="value: requestData.problemStatement, enabled: isEnabled" placeholder="Problem Statement" required data-required-msg="Problem Statement is required"></textarea>
                    </div>
                    <div>
                        <label class="labelFormWeight">2. Objective<span style="color: red">*</span></label>
                        <textarea name="<%: this.ID %>_objective" class="k-textbox" style="width: 500px;" rows="4" data-bind="value: requestData.objective, enabled: isEnabled" placeholder="Objective" required data-required-msg="Objective is required"></textarea>
                    </div>
                    <div>
                        <label class="labelFormWeight" style="width:80%;">3. How this EWR was initiated <i>(Please tick box)</i><span style="color: red">*</span></label>
                        <fieldset class="gdiFieldset">
                            <legend class="gdiLegend">&nbsp;</legend>
                            <div class="k-form-field" id="<%: this.ID %>_howewrinitiated">
                                <input type="checkbox" id="<%: this.ID %>_eqInitiated1" data-bind="checked: itemInitiate, enabled: isEnabled" class="k-checkbox" value="incident">
                                <label class="k-checkbox-label" for="<%: this.ID %>_eqInitiated1" style="font-size: 10pt; padding-right: 20px;">Incident</label>

                                <input type="checkbox" id="<%: this.ID %>_eqInitiated2" data-bind="checked: itemInitiate, enabled: isEnabled" class="k-checkbox" value="improvementidea">
                                <label class="k-checkbox-label" for="<%: this.ID %>_eqInitiated2" style="font-size: 10pt; padding-right: 20px;">Improvement Idea</label>

                                <input type="checkbox" id="<%: this.ID %>_eqInitiated3" data-bind="checked: itemInitiate, enabled: isEnabled" class="k-checkbox" value="hazardstudy">
                                <label class="k-checkbox-label" for="<%: this.ID %>_eqInitiated3" style="font-size: 10pt; padding-right: 20px;">Hazard Study</label>

                                <input type="checkbox" id="<%: this.ID %>_eqInitiated4" data-bind="checked: itemInitiate, enabled: isEnabled" class="k-checkbox" value="riskassessment">
                                <label class="k-checkbox-label" for="<%: this.ID %>_eqInitiated4" style="font-size: 10pt; padding-right: 20px;">Risk Assessment</label>
                            </div>
                        </fieldset>
                    </div>
                    <div>
                        <label class="labelFormWeight">4. Data Supporting Document</label>
                        <div data-role="grid" data-bind="source: supportingDocuments"
                            data-columns="[
                            { 
                                'field': 'FILENAME', 
                                'title': 'File Name', 'width': '50%', 
                                template: <%: this.ID %>_viewModel.supportingDocumentColumnTemplate
                            }
                            ]"
                            >

                        </div>
                    </div>
                    <%--<table>
                        <tr>
                            <td style="width: 30%"><label class="labelFormWeight">Data Supporting Document</label></td>
                            <td style="width: 50%">
                                <div data-role="grid" data-bind="source: supportingDocuments"
                                    data-columns="[
                                    { 
                                        'field': 'FILENAME', 
                                        'title': 'File Name', 'width': '100px', 
                                        template: <%: this.ID %>_viewModel.supportingDocumentColumnTemplate
                                    }
                                    ]"
                                    >

                                </div>

                            </td>
                        </tr>
                    </table>
                   --%>
                </fieldset>
               
            </div>
            <div>
                <label class="labelFormWeight">Project Sponsor</label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_projectSponsor" name="<%: this.ID %>_projectSponsor" data-bind="value: requestData.projectSponsor, enabled: isEnabled" placeholder="Project Sponsor" required />
            </div>
            <div>
                <label class="labelFormWeight">Location</label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_location" name="<%: this.ID %>_location" data-bind="value: requestData.sponsorLocation, enabled: isEnabled" placeholder="Location" required />
            </div>
            <div>
                <label class="labelFormWeight">Phone No</label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_sponsorPhoneNo" name="<%: this.ID %>_sponsorPhoneNo" data-bind="value: requestData.sponsorPhoneNo, enabled: isEnabled" required />
            </div>
            <div>
                <label class="labelFormWeight">Operational Contact Person</label>
                <%--<input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-bind="value: requestData.operationalCP, enabled: isEnabled" placeholder="Operational Contact Person" required />--%>
               <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-bind="value: requestData.operationalCP, enabled: isEnabled" required />
            </div>
            <div>
                <label class="labelFormWeight">Operational Contact Person 2</label>
                <%--<input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-bind="value: requestData.operationalCP, enabled: isEnabled" placeholder="Operational Contact Person" required />--%>
               <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_operationalCP2" name="<%: this.ID %>_operationalCP2" data-bind="value: requestData.operationalCP2, enabled: isEnabled" required />
            </div>
        </fieldset>
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        isReadonly: true,
        items: [],
        itemsRequestedWork: [],
        itemInitiate: [],
        supportingDocuments: [],
        isOtherChecked: false,
        otherDescription: "",
        parentForm: "",
        requestData: new common_ewrRequest(),
        onOtherChange: function (e) {
            this.set("isOtherChecked", !this.get("isOtherChecked"));
            if (!this.get("isOtherChecked")) {
                this.set("otherDescription", "");
            }

        },
        isEnabled: true,
        onValidationEntry: function () {
            var validatable = $("#<%: this.ID %>_RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validatable.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validatable.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }

            return isValid;
        },
        
        dsEmployee: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {

                    var searchParam = options.data.filter.filters[0].value;
                    if (searchParam.length > 0) {
                        var result = [];
                        // if (this.get("selectedUser").length >= 3) {

                        //var filter = "?$filter=((EMPLOYEE_ID contains '*" + searchParam + "*') or lower(FULL_NAME) contains lower('*" + searchParam + "*')) and (LEVEL_INFO not contains '*L2*' and LEVEL_INFO not contains '*L3*' and LEVEL_INFO not contains '*L4*')";
                        var filter = "?$filter=(substringof('" + searchParam + "',EMPLOYEEID) or substringof(tolower('" + searchParam + "'),tolower(FULL_NAME)))";
                        //filter += " and (substringof('L2',LEVEL_INFO) eq true)";

                        var result = [];
                        $.ajax({
                            url: _webOdataUrl + "EmployeeOData" + filter,
                            type: "GET",
                            async: false,
                            success: function (data) {
                                //result = data.value;

                                options.success(data.value);
                            },
                            error: function (data) {
                                //return [];
                                //options.error(data.value);
                                options.error(data.value);
                            }
                        });
                    } else {
                        options.success([]);
                    }
                    // }

                }
            }
        }),
        onOperationalCPSelected: function (e) {
                
            <%: this.ID %>_viewModel.set("requestData.operationalCP", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.operationalCPEmail", e.dataItem.EMAIL);
            
        },
        onOperationalCPClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onProjectSponsorSelected: function(e) {
                
            <%: this.ID %>_viewModel.set("requestData.projectSponsorBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.projectSponsorEmail", e.dataItem.EMAIL);
        },
        onProjectSponsorClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        supportingDocumentColumnTemplate: function(dataItem) {
            var result = "<a href='" + "<%: DocServer %>" + "getdoc.aspx?id=" + dataItem.REFDOCID + "&op=n' target='_blank'>" + dataItem.FILENAME + "</a>";
            return result;
        },
        onClearEvent: function (e) {
            this.requestData.set("ewrNo", null);
            this.requestData.set("subject", null);
            this.requestData.set("area", null);
            this.requestData.set("dateIssued", null);
            this.requestData.set("accCode", null);
            this.requestData.set("networkNo", null);
            this.requestData.set("budgetAlloc", null);
            this.requestData.set("dateCompletion", null);
            this.requestData.set("problemStatement", null);
            this.requestData.set("objective", null);
            this.requestData.set("assignedPM", null);
            this.requestData.set("projectSponsor", null);
            this.requestData.set("sponsorLocation", null);
            this.requestData.set("sponsorPhoneNo", null);
            this.requestData.set("operationalCP", null);
            this.requestData.set("projectTitle", null);
            this.requestData.set("status", null);
            this.set("otherDescription", "");
            this.set("isOtherChecked", false);
            this.set("items", []);
            this.set("itemsRequestedWork", []);
            this.set("itemInitiate", []);
        }
    });

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    // Document Ready
    $(document).ready(function () {
        <%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#<%: this.ID %>_RequestContent"), <%: this.ID %>_viewModel);

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        <%: this.ID %>_viewModel.onClearEvent()
        $("#<%: this.ID %>_RequestContent").kendoTooltip({
            filter: "#<%: this.ID %>_shareFolderDoc",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format Share Folder, e.g.: \\\\netapp8\\folder";
                return content;
            }
        }).data("kendoTooltip");

        <%: this.ID %>_viewModel.requestData.set("dateIssued", new Date());
    }); //doc ready

    function <%: this.ID %>_Init(dataRequest, parentForm, isReadOnly) {
        
        <%: this.ID %>_viewModel.set("isEnabled", !isReadOnly);
        <%: this.ID %>_viewModel.set("isOtherChecked", !isReadOnly);
        
        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        // jika dataRequest ada data nya maka update
        if (dataRequest) {
            var requestData =
            {
                id: dataRequest.OID,
                //ewrNo: 
                subject: dataRequest.SUBJECT,
                area: dataRequest.AREA,
                dateIssued: kendo.toString(dataRequest.DATEISSUED, 'yyyy-MM-dd'),
                accCode: dataRequest.ACCCODE,
                networkNo: dataRequest.NETWORKNO,
                budgetAlloc: dataRequest.BUDGETALLOC,
                dateCompletion: kendo.toString(dataRequest.DATECOMPLETION, 'yyyy-MM-dd'),
                problemStatement: dataRequest.PROBLEMSTATEMENT,
                objective: dataRequest.OBJECTIVE,
                assignedPM: dataRequest.ASSIGNEDPM,
                projectSponsor: dataRequest.PROJECTSPONSOR,
                sponsorLocation: dataRequest.SPONSORLOCATION,
                sponsorPhoneNo: dataRequest.SPONSORPHONENO.replace(/-/g, "").replace(/_/g, ""),
                operationalCP: dataRequest.OPERATIONALCP,
                operationalCP2: dataRequest.OPERATIONALCP2,
                projectTitle: dataRequest.PROJECTTITLE,
                status: dataRequest.STATUS

            };

            <%: this.ID %>_viewModel.set("requestData", requestData);

            var arrReqWork = [];
            $.each(dataRequest.TYPEOFREQUESTEDWORK, function (idx, elem) {
                arrReqWork.push(elem.VALUE);
                if (elem.VALUE == "other") {
                    <%: this.ID %>_viewModel.set("otherDescription", elem.DESCRIPTION);
                    <%: this.ID %>_viewModel.set("isOtherChecked", true);
                }
            });
            <%: this.ID %>_viewModel.set("itemsRequestedWork", arrReqWork);

            var arrCat = [];
            $.each(dataRequest.CATEGORYOFWORK, function (idx, elem) {
                arrCat.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("items", arrCat);

            var arrInitiate = [];
            $.each(dataRequest.DETAILOFWORK, function (idx, elem) {
                arrInitiate.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("itemInitiate", arrInitiate);

            //var arrSupportingDoc = [];
            //$.each(dataRequest.SUPPORTINGDOCUMENTS, function (idx, elem) {
            //    arrSupportingDoc.push({ OID: elem.OID, FILENAME: elem.FILENAME });
            //    console.log(elem.FILENAME);
            //})
            <%: this.ID %>_viewModel.set("supportingDocuments", dataRequest.SUPPORTINGDOCUMENTS);

            $('#<%: this.ID %>_create').hide();
            $('#<%: this.ID %>_update').show();
        } else {
            $('#<%: this.ID %>_create').show();
            $('#<%: this.ID %>_update').hide();
        }

        // set is other to false if readonly = true
        <%: this.ID %>_viewModel.set("isOtherChecked", !isReadOnly);
    }
</script>