﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.iPROM.WebApp.UserControl
{
    public class UserControlBasePage : System.Web.UI.UserControl
    {
        public string DocServer = Properties.Settings.Default.DocServer;

        public string WebAppUrl
        {
            get
            {
                return (this.Page as BasePage).WebAppUrl;
            }
        }

        public string WebApiUrl
        {
            get
            {
                return (this.Page as BasePage).WebApiUrl;
            }
        }

        public string WebApiUrlEllipse
        {
            get
            {
                return (this.Page as BasePage).WebApiUrlEllipse;
            }
        }

        public string WebOdataUrl
        {
            get
            {
                return (this.Page as BasePage).WebOdataUrl;
            }
        }

        public string NtUserID
        {
            get
            {
                return (this.Page as BasePage)._currNTUserID;
            }
        }

        public string BadgeNo
        {
            get
            {
                return (this.Page as BasePage)._currBadgeNo;
            }
        }
        public string ExtKDMonth
        {
            get
            {
                return (this.Page as BasePage).ExtKDMonth;
            }
        }

        public string _currRoleType
        {
            get
            {
                return (this.Page as BasePage)._currRoleType;
            }
        }

        public string _currProjectType
        {
            get
            {
                return (this.Page as BasePage)._projectType;
            }
        }
        public bool IsAllowedTo(AccessRights accessrights)
        {
            return (this.Page as BasePage).IsAllowedTo(accessrights);
        }

        public bool IsReadOnlyTo(AccessRights accessrights)
        {
            return (this.Page as BasePage).IsReadOnlyTo(accessrights);
        }

        public string _planStartDate
        {
            get
            {
                return (this.Page as BasePage)._planStartDate;
            }
        }
        public string _planFinishDate
        {
            get
            {
                return (this.Page as BasePage)._planFinishDate;
            }
        }
    }
}