﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BUDGETMinutesOfMeetingControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.BUDGETMinutesOfMeetingControl" %>

<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<div id="RequestContent" class="k-content wide">
    <div>
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend">Engineering Work Request Information</legend>
                <div>
                    <label class="labelFormWeight">Budget Approval</label>
                    <input data-role="numerictextbox"
                        data-format="c"
                        data-min="0"
                        data-bind="value: requestData.BUDGETAMOUNT"
                        style="width: 180px" disabled>
                </div>
            </fieldset>
            <fieldset class="gdiFieldset">
                <legend class="gdiLegend">Conclusion</legend>
                <div>
                    <label class="labelFormWeight">Project No</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: project.projectNo, enabled: isEnabled" maxlength="50" placeholder="Project No" />
                </div>
                <div>
                    <label class="labelFormWeight">Project Title<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: porject.projectTitle, enabled: isEnabled" maxlength="200" placeholder="Project Title" required />
                </div>
                <div>
                    <label class="labelFormWeight">Project Description<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: project.projectDescription, enabled: isEnabled" maxlength="200" placeholder="Project Description" required />
                </div>
                <div>
                    <label class="labelFormWeight">Project Manager<span style="color: red">*</span></label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.projectManager, enabled: isEnabled" placeholder="Project Manager" required />--%>
                    <input data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-auto-bind="false"
                        data-filter="contains"
                        data-bind="value: project.projectManager,
                                    source: dsProjectManager,
                                    enabled: isEnabled,
                                    events: {
                                        select: onProjectManagerSelect,
                                        filtering: autocompleteFiltering
                                    }"
                        style="width: 400px;"
                        required
                    />
                </div>
                            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Detail of Work Requirement</legend>
                
                    <div>
                        <label class="labelFormWeight">4. File Name</label>
                        <div data-role="grid" data-bind="source: supportingDocuments"
                            data-columns="[
                            { 
                                'field': 'FILENAME', 
                                'title': 'File Name', 'width': '50%', 
                                template: <%: this.ID %>_viewModel.supportingDocumentColumnTemplate
                            }
                            ]"
                            >

                        </div>
                    </div>
            </fieldset>
        </fieldset>
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>

<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        ewrID: 0,
        isReadonly: true,
        parentForm: "",
        itemsProjectType: [],
        itemsRequestedWork: [],
        allParticipants: [],
        allDistributions: [],
        isOtherChecked: false,
        projectTypeOther: "",
        typeOfRequestedOther: "",
        recordedByEmail: "",
        recordedByBN: "",
        projectManagerEmail: "",
        projectManagerBN: "",
        operationalCPFullName: "",
        operationalCP2FullName: "",
        requestData: new common_budgetRequest(),
        project: new common_budgetMom(),
        onOtherChange: function (e) {

            this.set("isOtherChecked", !this.get("isOtherChecked"));
            if (!this.get("isOtherChecked")) {
                this.set("projectTypeOther", "");
            }

        },
        isEnabled: true,
        onValidationEntry: function () {
            var validator = $("#RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validator.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validator.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }

            // cek jika pilih other di project type, maka descriptionnya harus diisi
            var isProjectTypeValid = true;
            $.each(this.itemsProjectType, function (idx, el) {
                if (el == "other") {
                    if (!<%: this.ID %>_viewModel.get("projectTypeOther")) {
                        isProjectTypeValid = false;
                    }
                }
            });
            if (!isProjectTypeValid) {
                _showDialogMessage("Required Message", "Please fill Other's description in Project Type", "");
                return false;
            }

            // cek jika pilih other di requested work, maka descriptionnya harus diisi
            var isRequestedWorkValid = true;
            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    if (!<%: this.ID %>_viewModel.get("typeOfRequestedOther")) {
                        isRequestedWorkValid = false;
                    }
                }
            });
            if (!isRequestedWorkValid) {
                _showDialogMessage("Required Message", "Please fill Other's description in Type of Requested Work", "");
                return false;
            }

            return isValid;
        },
        populateData: function () {
            //console.log("Populate Data");
            var typeOfProjectType = [];
            var otherDesc = this.projectTypeOther;
            $.each(this.itemsProjectType, function (idx, el) {
                
                if (el == "other") {
                    typeOfProjectType.push({ TYPE: "itemsProjectType", VALUE: el, DESCRIPTION: otherDesc });
                } else {
                    typeOfProjectType.push({ TYPE: "itemsProjectType", VALUE: el });
                }
            });
            //console.log(typeOfProjectType);
            var allParticipants = [];
            $.each(this.allParticipants, function (idx, el) {
                allParticipants.push(el.EMPLOYEEID + "-" + el.FULL_NAME + "-" + el.EMAIL);
            });
            //console.log(allParticipants);
            var allDistributions = [];
            $.each(this.allDistributions, function (idx, el) {
                allDistributions.push(el.EMPLOYEEID + "-" + el.FULL_NAME + "-" + el.EMAIL);
            });
            
            var projectData =
                {
                    OID: "-1",
                    PROJECTNO: this.project.projectNo,
                    PROJECTTITLE: this.project.projectTitle,
                    PROJECTDESCRIPTION: this.project.projectDescription,
                    PROJECTMANAGER: this.project.projectManager,
                    BUDGETAMOUNT: this.budgetAmount,
                    FILENAME: this.fileName,
                    PROJECTTYPE: typeOfProjectType
                }
            //console.log(momData);
            //console.log("--Populate Data");
            return projectData;
        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        onChangeParticipants: function() {

        },
        //dsParticipant: new kendo.data.DataSource({
        //    type: "odata",
        //    serverFiltering: true,
        //    transport: {
        //        read: {
        //            url: _webOdataUrl + "EmployeeOData",
        //            dataType: "json"
        //        }
               
        //    },
        //    schema: {ds
        //        data: function (data) {
        //            return data.value;
        //        },
        //        total: function (data) {
        //            return data["odata.count"];
        //        },
        //        model: {
        //            id: "EMPLOYEEID"
        //        }
        //    }
        //}),
        //dsDistribution: new kendo.data.DataSource({
        //    type: "odata",
        //    serverFiltering: true,
        //    transport: {
        //        read: {
        //            url: _webOdataUrl + "EmployeeOData",
        //            dataType: "json"
        //        }

        //    },
        //    schema: {
        //        data: function (data) {
        //            return data.value;
        //        },
        //        total: function (data) {
        //            return data["odata.count"];
        //        },
        //        model: {
        //            id: "EMPLOYEEID"
        //        }
        //    }
        //}),
        dsEmployee: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsParticipant: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsDistribution: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsRecordedBy: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsProjectManager: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsOperationalCP: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsOperationalCP2: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        autocompleteFiltering: function(e) {
            var filter = e.filter;
            
            if (filter) {
                if (!filter.value) {
                    //prevent filtering if the filter does not value
                    e.preventDefault();
                }
            } else {
                e.preventDefault();
            }
            
        },
        onMinutesRecordedBy: function (e) {
               
            <%: this.ID %>_viewModel.set("recordedByBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("recordedByEmail", e.dataItem.EMAIL);
        },
        onProjectManagerSelect: function (e) {
               
            <%: this.ID %>_viewModel.set("projectManagerBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("projectManagerEmail", e.dataItem.EMAIL);
        },
        onClearEvent: function (e) {
            this.requestData.set("projectNo", null);
            this.requestData.set("projectDescription", null);
            this.requestData.set("projectManager", null);
            this.requestData.set("budgetAmount", null);
            this.requestData.set("fileName", null);
            this.requestData.set("projectTitle", null);
        },
        onOperationalCPSelected: function (e) {
            <%: this.ID %>_viewModel.set("operationalCPFullName", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCPBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCPEMAIL", e.dataItem.EMAIL);
            
        },
        onOperationalCPClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onOperationalCP2Selected: function (e) {

            <%: this.ID %>_viewModel.set("operationalCP2FullName", common_toTitleCase(e.dataItem.FULL_NAME));

            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCP2BN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.OPERATIONALCP2EMAIL", e.dataItem.EMAIL);
            
        },
        onOperationalCP2Close: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
    });

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    function <%: this.ID %>_initForm(projectID) {
        <%: this.ID %>_viewModel.set("projectID", projectID);
    }

    function <%: this.ID %>_Init(dataRequest, parentForm, isReadOnly) {
        <%: this.ID %>_viewModel.set("projectID", dataRequest.OID); 
        
        <%: this.ID %>_viewModel.set("isEnabled", !isReadOnly);
        
        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        <%: this.ID %>_viewModel.set("requestData", dataRequest);
        <%: this.ID %>_viewModel.set("operationalCPFullName", dataRequest.OPERATIONALCP);
        <%: this.ID %>_viewModel.set("operationalCP2FullName", dataRequest.OPERATIONALCP2);
        $.ajax({
            url: _webApiUrl + "momewrs/FindMomEwrByRequest/" + dataRequest.OID,
            type: "GET",
            success: function (result) {
                if (result) {
                    var projectData = {
                        OID: result.OID,
                        projectId: result.PROJECTID,
                        projectNo: result.PROJECTNO,
                        projectDescription: result.PROJECTDESCRIPTION,
                        projectManager: result.PROJECTMANAGER,
                        budgetAmount: result.BUDGETAMOUNT,
                        fileName: result.FILENAME,
                        projectTitle: result.PROJECTTITLE,
                        typeOfProjectType: result.PROJECTTYPE
                    }
                    <%: this.ID %>_viewModel.set("projectData", projectData);

                    var itemsProjectType = [];
                    $.each(result.BUDGETAPPROVAL, function (idx, elem) {
                        itemsProjectType.push(elem.VALUE);
                        if (elem.VALUE == "other") <%: this.ID %>_viewModel.set("projectTypeOther", elem.DESCRIPTION);
                    });
                    <%: this.ID %>_viewModel.set("itemsProjectType", itemsProjectType);
                }
                
            },
            error: function (jqXHR) {
            }
        });

        var requestedWork = [];
        $.each(dataRequest.TYPEOFREQUESTEDWORK, function (idx, elem) {
            requestedWork.push(elem.VALUE);
            if (elem.VALUE == "other") <%: this.ID %>_viewModel.set("typeOfRequestedOther", elem.DESCRIPTION);
        });
        <%: this.ID %>_viewModel.set("itemsRequestedWork", requestedWork);

        // User Participants
        var userParticipants = [];
        $.each(dataRequest.USERPARTICIPANTS, function (idx, elem) {
            userParticipants.push({ full_Name: elem.EMPLOYEENAME, employeeId: elem.EMPLOYEEID });
            
        });
        <%: this.ID %>_viewModel.set("allParticipants", userParticipants);

        // User Distributions
        var userDistributions = [];
        $.each(dataRequest.USERDISTRIBUTIONS, function (idx, elem) {
            userDistributions.push({ full_Name: elem.EMPLOYEENAME, employeeId: elem.EMPLOYEEID });
            
        });
        <%: this.ID %>_viewModel.set("allDistributions", userDistributions);

        // set is other to false if readonly = true
        if (isReadOnly) {
            <%: this.ID %>_viewModel.set("isOtherChecked", false);
        }
        
    }
    // Document Ready
    $(document).ready(function () {
        <%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#RequestContent"), <%: this.ID %>_viewModel);

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        //<%: this.ID %>_viewModel.onClearEvent()
        
        <%: this.ID %>_viewModel.requestData.set("dateIssued", new Date());

    }); //doc ready

</script>
