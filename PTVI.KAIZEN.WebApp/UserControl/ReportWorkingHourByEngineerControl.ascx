﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportWorkingHourByEngineerControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportWorkingHourByEngineerControl" %>
<%@ Register Src="~/UserControl/EmployeeOnReportWorkHoursSearchControl.ascx" TagPrefix="uc1" TagName="EmployeeOnReportWorkHoursSearchControl" %>

<script>
    var <%: this.ID %>_chart, <%: this.ID %>_popPickEmployee;
    var <%: this.ID %>_viewReportProjectDistributionModel = kendo.observable({
        dsProjectDistribution: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportTechnicalSupportByEngineer"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportTechnicalSupportByEngineer"), false);
            },
            schema: {
                model: {
                    fields: {
                        estimationCost: { type: "number" },
                        actualCost: { type: "number" }
            }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var year = <%: this.ID %>_viewReportProjectDistributionModel.valueYear ? <%: this.ID %>_viewReportProjectDistributionModel.valueYear : 0;
                        //return _webApiUrl + "dashboard/reportWorkingHoursbyEngineer/" + _currNTUserID + "|" + year + "|" + <%: this.ID %>_viewReportProjectDistributionModel.valueEmployee.join(";");
                        return _webApiUrl + "dashboard/reportWorkingHoursbyEngineer/" + year + "|" + <%: this.ID %>_viewReportProjectDistributionModel.valueEmployee.join(";");
                    },
                    dataType: "json"
                }
            },
            sort: [{ field: "employeeId", dir: "asc" }]
        }),
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportTechnicalSupportByEngineer"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_ReportTechnicalSupportByEngineer"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
        titleChart: "Report Working Hours By Engineer ",
        valueYear: new Date().getFullYear(),
        dataEmployee: [],
        valueEmployee: [],
        onPickEmployeeClick: function (e) {
            var that = this;
            
            EmployeeOnReportWorkHoursSearchControl_pickEmployeeOnProjectReportBudgetModel.set("pickEmployee", function (e) {
                that.set("dataEmployee", EmployeeOnReportWorkHoursSearchControl_pickEmployeeOnProjectReportBudgetModel.onSelect());
                var empId = [];
                $.each(that.dataEmployee, function (index, value) {
                    empId.push(value.employeeId);
                });
                that.set("valueEmployee", empId);
                <%: this.ID %>_popPickEmployee.close();
            });
            // show popup
            <%: this.ID %>_popPickEmployee.center().open();
        },
        onSearchClick: function () {
            this.dsProjectDistribution.read();
            <%: this.ID %>_chart.options.title.text = this.titleChart + this.valueYear;
//            <%: this.ID %>_chart.refresh();
//            <%: this.ID %>_chart.refresh();
        },
        onResetClick: function () {
            this.set("valueYear", new Date().getFullYear());
            this.set("valueEmployee", []);
        }

    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_ReportTechnicalSupportByEngineer"), <%: this.ID %>_viewReportProjectDistributionModel);
        $("#<%: this.ID %>_projectDistribution").kendoChart({
            //theme: "silver",
            title: {
                text: <%: this.ID %>_viewReportProjectDistributionModel.titleChart + <%: this.ID %>_viewReportProjectDistributionModel.valueYear
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_viewReportProjectDistributionModel.dsProjectDistribution,
            transitions: false,
            seriesColors: ["#f7e32c", "#3250e5", "#43db2b", "#d12bce"],
            seriesDefaults: {
                type: "column",
                stack: true
            },
            series: [{ name: "Operating", field: "operating" }, { name: "Capital", field: "capital" }, { name: "Technical Support", field: "ts" }, { name: "OverHead", field: "oh" }],
            valueAxis: {
                labels: {
                    template: "#= kendo.format('{0:N0}', value) #",
                }
            },
            categoryAxis: {
                field: "employeeName",
                labels: {
                    rotation: -45
                }
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= kendo.format('{0:N0}', value) #"
            }
        });

        <%: this.ID %>_popPickEmployee = $("#<%: this.ID %>_popPickEmployee").kendoWindow({
            title: "Operation List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");


        <%: this.ID %>_chart = $("#<%: this.ID %>_projectDistribution").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_ReportTechnicalSupportByEngineer">
    <div id="<%: this.ID %>_popPickEmployee">
       <uc1:EmployeeOnReportWorkHoursSearchControl runat="server" ID="EmployeeOnReportWorkHoursSearchControl" />
    </div>
    <fieldset>
        <legend>Search Criteria</legend>
        <table>
            <tr>
                <td>
                    Report Year
                </td>
                <td>
                    :
                </td>
                <td>
                    <input data-role="combobox"
                            data-placeholder="Select Year"                            
                            data-text-field="year"
                            data-value-field="year"
                            data-bind="value: valueYear, source: dsYearProject"
                            style="width: 250px" />
                </td>
            </tr>
                    <tr>
                <td>
                    Employee
                </td>
                <td>
                    :
                </td>
                <td><div style="width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_multiSelectEmployee" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Clik button to select Employee"
                        data-value-primitive="true"
                        data-text-field="employeeName"
                        data-value-field="employeeId"
                        data-readonly="true"
                        data-bind="value: valueEmployee, source: dataEmployee"></select></div>
                    <div data-role="button"
                        data-bind="events: { click: onPickEmployeeClick }"
                        style="display: inline-block;vertical-align: middle;"
                        class="k-button k-primary">
                        ...
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3"><br />
                    <div class="divCell" style="text-align:right">
                        <div
                            data-role="button"
                            data-bind="events: { click: onSearchClick }"
                            class="k-button k-primary">
                            <span class="k-icon k-i-filter"></span>
                            Search
                        </div>
                        <div
                            data-role="button"
                            data-bind="events: { click: onResetClick }"
                            class="k-button">
                            <span class="k-icon k-i-cancel"></span>
                            Reset
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset><br />
    <fieldset>
        <legend>Search Result</legend>
        <div id="<%: this.ID %>_projectDistribution" style="width: 100%; height: 450px;"></div>
    </fieldset>
</div>

