﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArchiveProjectListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ArchiveProjectListControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>
<%@ Register Src="~/UserControl/RequestProjectControl.ascx" TagPrefix="uc1" TagName="RequestProjectControl" %>

<script>
    // Variable
    var popPickSponsor,
        popPickPM,
        popPickPE,
        popPickEngineer, <%: this.ID %>_popViewProject;

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        selectedTabIndex: 0,
        projectNo: "",
        projectDesc: "",
        sponsorParam: "",
        sponsorParamName: "",
        pmParam: "",
        pmParamName: "",
        peParam: "",
        peParamName: "",
        engineerParam: "",
        engineerParamName: "",
        statusArchived: "",
        isSponsorNameDisabled: true,
        isPMNameDisabled: true,
        isPENameDisabled: true,
        isEngineerNameDisabled: true,
        dsStatusArchived: new kendo.data.DataSource({
            transport: {
                read: "<%: WebApiUrl %>" + "lookup/FindByType/closeOutReason" ,

            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        value: { type: "string" },
                        text: { type: "string" },
                    },
                },
            },
        }),
        dsListProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        totalCost: { type: "number" },
                        actualCost: { type: "number" },
                        commitment: { type: "number" },
                        shareFolderDoc: { type: "number" }
                    }
                }
            },
            transport: {
                read: "<%: WebApiUrl %>" + "Project/archiveProjectList/" + _currRole,
                parameterMap: function (data, type) {
                    if (type == "read") {
                        data["projectNo"] = <%: this.ID %>_viewModel.projectNo;
                        data["projectDesc"] = <%: this.ID %>_viewModel.projectDesc;
                        data["sponsorParam"] = <%: this.ID %>_viewModel.sponsorParam;
                        data["pmParam"] = <%: this.ID %>_viewModel.pmParam;
                        data["peParam"] = <%: this.ID %>_viewModel.peParam;
                        data["engineerParam"] = <%: this.ID %>_viewModel.engineerParam;
                        data["statusArchived"] = <%: this.ID %>_viewModel.statusArchived;
                        return data;
                    }
                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsListMasterProjectCategory: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListProject"), false);
            },
            transport: {
                read: {
                    async: false,
                    url: "<%: WebApiUrl %>" + "MasterProjectCategories",
                    dataType: "json"
                }
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            pageSize: 10,
            sort: [{ field: "description", dir: "asc" }]
        }),
        onSearchClick: function () {
            if (<%: this.ID %>_viewModel.projectNo || <%: this.ID %>_viewModel.projectDesc || <%: this.ID %>_viewModel.sponsorParam ||
                <%: this.ID %>_viewModel.pmParam || <%: this.ID %>_viewModel.peParam || <%: this.ID %>_viewModel.engineerParam || <%: this.ID %>_viewModel.statusArchived) {
                this.dsListProject.read();
            }
            else {
                $('<div/>').kendoAlert({
                    title: 'Search Request',
                    modal: true,
                    content: "<div>Criteria can't be empty</div>",
                    actions: [{ text: "OK" }]
                }).data('kendoAlert').open();
            }
        },
        onResetClick: function () {
            <%: this.ID %>_viewModel.set("projectNo", "");
            <%: this.ID %>_viewModel.set("projectDesc", "");
            <%: this.ID %>_viewModel.set("sponsorParam", "");
            <%: this.ID %>_viewModel.set("sponsorParamName", "");
            <%: this.ID %>_viewModel.set("pmParam", "");
            <%: this.ID %>_viewModel.set("pmParamName", "");
            <%: this.ID %>_viewModel.set("peParam", "");
            <%: this.ID %>_viewModel.set("peParamName", "");
            <%: this.ID %>_viewModel.set("engineerParam", "");
            <%: this.ID %>_viewModel.set("engineerParamName", "");
            <%: this.ID %>_viewModel.set("statusArchived", "");
        },
        onPickSponsorClick: function (e) {
            PickSponsorControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.set("sponsorParam", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.set("sponsorParamName", pickedEmployee.full_Name);

                popPickSponsor.close();
            });
            PickSponsorControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                popPickSponsor.close();
            });
            // show popup
            popPickSponsor.center().open();
            PickSponsorControl_DataBind();
        },
        onPickPMClick: function (e) {
            PickPMControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.set("pmParam", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.set("pmParamName", pickedEmployee.full_Name);

                popPickPM.close();
            });
            PickPMControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                popPickPM.close();
            });
            // show popup
            popPickPM.center().open();
            PickPMControl_DataBind();
        },
        onPickPEClick: function (e) {
            PickPEControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.set("peParam", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.set("peParamName", pickedEmployee.full_Name);

                popPickPE.close();
            });
            PickPEControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                popPickPE.close();
            });
            // show popup
            popPickPE.center().open();
            PickPEControl_DataBind();
        },
        onPickEngineerClick: function (e) {
            PickEngineerControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                console.log(pickedEmployee);
                <%: this.ID %>_viewModel.set("engineerParam", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.set("engineerParamName", pickedEmployee.full_Name);

                popPickEngineer.close();
            });
            PickEngineerControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                popPickEngineer.close();
            });
            // show popup
            popPickEngineer.center().open();
            PickEngineerControl_DataBind();
        },
        openProjectDashboard: function (data) {
            var form = $('<form action="DashboardProjectForm.aspx" method="post">' +
                        '<input type="hidden" name="_projectId" value="' + data.id + '"></input>' +
                        '<input type="hidden" name="_projectNo" value="' + data.projectNo + '"></input>' +
                        '<input type="hidden" name="_projectDesc" value="' + data.projectDescription + '"></input>' +
                        '<input type="hidden" name="_roleInProject" value="' + data.roleInProject + '"></input>' +
                        '<input type="hidden" name="_positionProjectName" value="' + data.positionName + '"></input>' +
                        '</form>');
            $('body').append(form);
            $(form).submit();
        },
        openProjectDetail: function (data) {
            //_redirectPost("HomeProjectForm.aspx", "_projectID", e.data.id)
            //var form = $('<form action="HomeProjectForm.aspx" method="post">' +
            var form = $('<form action="MainProjectForm.aspx" method="post">' +
                        '<input type="hidden" name="_projectId" value="' + data.id + '"></input>' +
                        '<input type="hidden" name="_projectNo" value="' + data.projectNo + '"></input>' +
                        '<input type="hidden" name="_projectDesc" value="' + data.projectDescription + '"></input>' +
                        '<input type="hidden" name="_roleInProject" value="' + data.roleInProject + '"></input>' +
                        '<input type="hidden" name="_iptProjectId" value="' + data.iptProjectId + '"></input>' +
                        '<input type="hidden" name="_planStartDate" value="' + (data.planStartDate ? kendo.toString(data.planStartDate, 'yyyyMMdd') : null) + '"></input>' +
                        '<input type="hidden" name="_planFinishDate" value="' + (data.planFinishDate ? kendo.toString(data.planFinishDate, 'yyyyMMdd') : null) + '"></input>' +
                        '<input type="hidden" name="_positionProjectName" value="' + data.positionName + '"></input>' +
                        '</form>');
            $('body').append(form);
            $(form).submit();
        },
        editViewProject: function (data) {
            $.ajax({
                url: _webApiUrl + "Project/" + data.id,
                type: "GET",
                success: function (data) {
                    RequestProject_viewModel.set("requestData", data);

                    if (!data.iptProjectId) {
                        RequestProject_viewModel.set("isAvailToIntegrated", true);
                        RequestProject_viewModel.set("integratedStatus", "");
                        RequestProject_viewModel.set("btnIntegrate", "Integrate");
                    } else {
                        RequestProject_viewModel.set("isAvailToIntegrated", true);
                        RequestProject_viewModel.set("integratedStatus", "(Integrated)");
                        RequestProject_viewModel.set("btnIntegrate", "Re-Integrate");
                    }
                    RequestProject_viewModel.set("employeeOwnerSelected", { employeeId: data.ownerId, full_Name: data.ownerName, email: data.ownerEmail });
                    RequestProject_viewModel.set("employeeSponsorSelected", { employeeId: data.sponsorId, full_Name: data.sponsorName, email: data.sponsorEmail });
                    RequestProject_viewModel.set("employeePMSelected", { employeeId: data.projectManagerId, full_Name: data.projectManagerName, email: data.projectManagerEmail });
                    RequestProject_viewModel.set("employeePESelected", { employeeId: data.projectEngineerId, full_Name: data.projectEngineerName, email: data.projectEngineerEmail });
                    RequestProject_viewModel.set("employeeMaintenanceSelected", { employeeId: data.maintenanceRepId, full_Name: data.maintenanceRepName, email: data.maintenanceRepEmail });
                    RequestProject_viewModel.set("employeeOperationSelected", { employeeId: data.operationRepId, full_Name: data.operationRepName, email: data.operationRepEmail });

                    RequestProject_viewModel.dsOtherPositionGetData.transport.options.read.url = _webApiUrl + "UserAssignment/otherPostionOnProject/" + data.id;
                    RequestProject_viewModel.dsLookupOtherPositionProject.read();
                    RequestProject_viewModel.dsOtherPositionGetData.read();
                    RequestProject_checkAccessRight();
                },
                error: function (jqXHR) {
                }
            });
            $('#RequestProject_inputProjectLegend').text("Detail Project");
            $('#RequestProject_projectNo').attr('readonly', true);
            $('#RequestProject_create').hide();
            $('#RequestProject_update').show();


            <%: this.ID %>_popViewProject.center().open();
            RequestProject_viewModel.set("onCloseUpdateClick", function (e) {
                <%: this.ID %>_popViewProject.close();
            });

            RequestProject_viewModel.set("onRefreshGridMaster", function (e) {
                <%: this.ID %>_viewModel.dsListProject.read();
            });

            var isCloseOut = false;

            $.ajax({
                url: _webApiUrl + "project/IsCloseOutProjectTo/" + data.id + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                dataType: "json",
                type: "GET",
                success: function (result) {
                    isCloseOut = result;
                },
                error: function (jqXHR) {
                }
            });

            if (isCloseOut) {
                if (_currRole == "ADM")
                    $("#<%: this.ID %>_projectNo").removeAttr('readonly');
            }
        }
    });

    $(document).ready(function () {
        <%: this.ID %>_viewModel.dsListMasterProjectCategory.read();
        kendo.bind($("#<%: this.ID %>_ArchiveProjectListControl"), <%: this.ID %>_viewModel);

        popPickOwner = $("#popPickOwner").kendoWindow({
            title: "Owner List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        popPickSponsor = $("#popPickSponsor").kendoWindow({
            title: "Sponsor List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        popPickPM = $("#popPickPM").kendoWindow({
            title: "PM List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        popPickPE = $("#popPickPE").kendoWindow({
            title: "PE List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        popPickEngineer = $("#popPickEngineer").kendoWindow({
            title: "Engineer List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        $("#<%: this.ID %>_menu").kendoContextMenu({
            orientation: "vertical",
            target: "#<%: this.ID %>_grdListProject",
            filter: ".projectNo-template",
            animation: {
                open: { effects: "fadeIn" },
                duration: 500
            },
            showOn: "click",
            select: function (e) {
                var row = $(e.target).parent().parent();
                var grid = this.target.data("kendoGrid");
                var data = grid.dataItem(row);

                if (e.item.id == "<%: this.ID %>_menuDashboard") {
                    //var accessRight;
                    //$.ajax({
                    //    url: _webApiUrl + "project/getPositionInProject/" + data.id + "?badgeNo=" + _currBadgeNo,
                    //    type: "GET",
                    //    success: function (data) {
                    //        accessRight = data;
                    //    },
                    //    error: function (jqXHR) {
                    //    }
                    //});

                    $.ajax({
                        url: _webApiUrl + "project/IsAllowedProjectTo/" + data.id + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ProjectDashboard%> ",
                        type: "GET",
                        success: function (result) {
                            if (result) {
                                <%: this.ID %>_viewModel.openProjectDashboard(data)
                            } else {
                                _showDialogMessage("Access Denied", "You do not have access privileges", "");
                            }
                        },
                        error: function (jqXHR) {
                        }
                    });

                    <%--                    if (data.roleInProject == _roleOwner || data.roleInProject == _roleSponsor || data.roleInProject == _roleProjectManager || data.roleInProject == _roleProjectEngineer) {
                        <%: this.ID %>_viewModel.openProjectDashboard(data)
                    } else { 
                        _showDialogMessage("Access Denied", "You do not have access privileges to project", "");
                    } --%>
                }
                else if (e.item.id == "<%: this.ID %>_menuSummary") {
                    <%: this.ID %>_viewModel.openProjectDetail(data)
                }
                else if (e.item.id == "<%: this.ID %>_menuViewProject") {
                    $.ajax({
                        url: _webApiUrl + "project/IsAllowedProjectTo/" + data.id + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                        type: "GET",
                        success: function (result) {
                            if (result) {
                                <%: this.ID %>_viewModel.editViewProject(data)
                            } else {
                                _showDialogMessage("Access Denied", "You do not have access privileges", "");
                            }
                        },
                        error: function (jqXHR) {
                        }
                    });
                }

            }
        });

        <%: this.ID %>_popViewProject = $("#<%: this.ID %>_popViewProject").kendoWindow({
            title: "Project Detail",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

    });

</script>

<div id="<%: this.ID %>_ArchiveProjectListControl">
        <div class="k-block">
            <fieldset>
                <legend>Search Criteria</legend>
                    <div class="divRow">
                        <div class="divCell"><span class="textLabel">Project No</span><br />
                            <input class="k-textbox" type="text" data-role="textbox" name="projectNo" data-bind="value: projectNo" style="width: 250px" placeholder="Project No" /></div><br />
                        <div class="divCell"><span class="textLabel">Project Desc.</span><br />
                            <input class="k-textbox" type="text" data-role="textbox" name="projectDesc" data-bind="value: projectDesc" style="width: 90%" placeholder="Project Description" /></div><br />
                        <div class="divCell"><span class="textLabel">Sponsor</span><br />
                            <input id="sponsorName" name="sponsorName" class="k-textbox" type="text" 
                                data-bind="disabled: isSponsorNameDisabled, value: sponsorParamName" placeholder="Project Sponsor" />
                            <div
                                data-role="button"
                                data-bind="events: { click: onPickSponsorClick }"
                                class="k-button">
                                ...
                            </div>
                        </div><br />
                        <div class="divCell"><span class="textLabel">Status</span><br />
                            <input class="k-textbox" type="text" data-role="combobox" name="projectDesc" data-bind="value: statusArchived, source: dsStatusArchived"
                                data-placeholder="Select Status"
                                data-value-primitive="true"
                                data-text-field="text"
                                data-value-field="value"
                                data-readonly="true" />

                        </div><br />                            
                    </div>
                    <div class="divRow">
                        <div class="divCell"><span class="textLabel">Project Manager</span><br />
                            <input id="pmName" name="pmName" class="k-textbox" type="text" 
                                data-bind="disabled: isPMNameDisabled, value: pmParamName" placeholder="Project Manager" />
                            <div
                                data-role="button"
                                data-bind="events: { click: onPickPMClick }"
                                class="k-button">
                                ...
                            </div>
                        </div><br />
                        <div class="divCell"><span class="textLabel">Project Engineer</span><br />
                            <input id="peName" name="peName" class="k-textbox" type="text" 
                                data-bind="disabled: isPENameDisabled, value: peParamName" placeholder="Project Engineer" />
                            <div
                                data-role="button"
                                data-bind="events: { click: onPickPEClick }"
                                class="k-button">
                                ...
                            </div>
                        </div><br />
                        <div class="divCell"><span class="textLabel">Engineer</span><br />
                            <input id="engineerName" name="engineerName" class="k-textbox" type="text" 
                                data-bind="disabled: isEngineerNameDisabled, value: engineerParamName" placeholder="Engineer" />
                            <div
                                data-role="button"
                                data-bind="events: { click: onPickEngineerClick }"
                                class="k-button">
                                ...
                            </div>
                        </div><br />
                        <div class="divCell" style="text-align:right">
                            <div
                                data-role="button"
                                data-bind="events: { click: onSearchClick }"
                                class="k-button k-primary">
                                <span class="k-icon k-i-filter"></span>
                                Search
                            </div>
                            <div
                                data-role="button"
                                data-bind="events: { click: onResetClick }"
                                class="k-button">
                                <span class="k-icon k-i-cancel"></span>
                                Reset
                            </div></div>
                    </div>
            </fieldset>
        </div><br />
    <div>
    <ul id="<%: this.ID %>_menu" style="width: 200px;">
        <li id="<%: this.ID %>_menuDashboard" style="font-size: 15px;">
            <img src="Images/dashboard-icon.png" style="width: 20px; height: 20px;" /> Dashboard
        </li>
        <li id="<%: this.ID %>_menuSummary" style="font-size: 15px;">
            <img src="Images/summary-icon.png" style="width: 20px; height: 20px;" /> Summary
        </li>
        <li class="k-separator"></li>
        <li id="<%: this.ID %>_menuViewProject" style="font-size: 15px;">
            <img src="Images/view-detail-icon.png" style="width: 20px; height: 20px;" /> View Project
        </li>
    </ul>
    <div id="<%: this.ID %>_popViewProject">
        <uc1:RequestProjectControl runat="server" ID="RequestProject" />
    </div>
        <fieldset>
            <legend>Search Result</legend>
            <div id="<%: this.ID %>_grdListProject"
                data-role="grid"
                data-scrollable="true"
                data-sortable="true"
                data-editable="false"
                data-columns="[
                        {
                            field: 'projectNo', title: 'Project No',
                            attributes: { style: 'vertical-align: top !important;' },
                            template: kendo.template($('#<%: this.ID %>_projectNo-template').html()), 
                            width: 110
                        },                                
                        {
                            field: 'projectDescription', title: 'Project Description',
                            attributes: { style: 'vertical-align: top !important;' },
                            template: kendo.template($('#<%: this.ID %>_projectDesc-template').html()), 
                            width: 200
                        },                                
                        {
                            field: 'status', title: 'Status',
                            attributes: { style: 'vertical-align: top !important;' },
                            width: 100
                        },                                
                        { 
                            field: 'projectManagerName', title: 'PM & PE' , 
                            attributes: { style: 'vertical-align: top !important;' },
                            template: kendo.template($('#<%: this.ID %>_pmpe-template').html()), 
                            width: 140
                        },                               
                        { 
                            field: 'userAssignments', title: 'Engineer/Designer', 
                            attributes: { style: 'vertical-align: top !important;' },
                            template: kendo.template($('#<%: this.ID %>_engineer-template').html()),
                            width: 140
                        },                               
                        { 
                            field: 'totalCost', title: 'Budget', 
                            attributes: { style: 'text-align: right; vertical-align: top !important;' },
                            format: '{0:n2}', width: 100
                        },                               
                        { 
                            field: 'actualCost', title: 'Actual Cost', 
                            attributes: { style: 'text-align: right; vertical-align: top !important;' },
                            format: '{0:n2}', width: 100
                        },                               
                        { 
                            field: 'commitment', title: 'Commitment', 
                            attributes: { style: 'text-align: right; vertical-align: top !important;' },
                            format: '{0:n2}', width: 100
                        }
                    ]"
                data-bind="source: dsListProject"
                data-pageable="{ buttonCount: 5 }">
            </div>
        </fieldset>
    </div>

    <div id="popPickSponsor">

        <uc1:PickEmployeesControl runat="server" ID="PickSponsorControl" />
    </div>
    <div id="popPickPM">

        <uc1:PickEmployeesControl runat="server" ID="PickPMControl" />
    </div>
    <div id="popPickPE">

        <uc1:PickEmployeesControl runat="server" ID="PickPEControl" />
    </div>
    <div id="popPickEngineer">

        <uc1:PickEmployeesControl runat="server" ID="PickEngineerControl" />
    </div>

</div>
<script type="text/x-kendo-template" id="<%: this.ID %>_projectNo-template">
        <div class="projectNo-template">
            # var url = ""; #
            #: data.projectNo #
<%--            <a href=#: url # target="_blank">#: data.projectNo #</a>--%>
        </div>
        #if (data.projectCategory) {#
            <div class='subColumnTitle'>
                Project Category
            </div>
            <div>
                #var splitPCategory = data.projectCategory.split('|')#
                #for (var l = 0; l < splitPCategory.length; l++) {#
                    #:(<%: this.ID %>_viewModel.dsListMasterProjectCategory.get(splitPCategory[l]) ? <%: this.ID %>_viewModel.dsListMasterProjectCategory.get(splitPCategory[l]).description + "; " : "-")#
                #}#
            </div>
        #}#
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_projectDesc-template">
        <div>
            #: data.projectDescription #
<%--            <a href="\#" data-bind="events: { click: openProjectDetail }">#: data.projectDescription #</a>--%>
        </div>
</script>
<script type="text/x-kendo-template" id="<%: this.ID %>_pmpe-template">
        <div>
            # if (data.projectManagerId == data.projectEngineerId) {#
                PM & PE: #: data.projectManagerName ? data.projectManagerName : "-"#
            # } else { #
                PM: #: data.projectManagerName ? data.projectManagerName : "-" #</br>
                PE: #: data.projectEngineerName ? data.projectEngineerName : "-" #
            # } #
        </div>
</script>
<script id="<%: this.ID %>_engineer-template" type="text/x-kendo-template">
         <div>
         # var PE = userAssignments.filter(function(val) {
            return val.assignmentType === "PE";
            }); #
         #if(PE.length>0){#
            <b>Engineer:</b><br\>
            # for (var i = 0; i < userAssignments.length; i++) { #
                # if(userAssignments[i].assignmentType=='PE'){ #
                #: userAssignments[i].employeeName #;<br\>
            #} } }#
          # var PD = userAssignments.filter(function(val) {
            return val.assignmentType === "PD";
            }); #
          #if(PD.length>0){#
            <b>Designer:</b><br\>
            # for (var i = 0; i < userAssignments.length; i++) { #
                # if(userAssignments[i].assignmentType=='PD'){#
                #: userAssignments[i].employeeName #;<br\>
            # } } } #
        </div>  
</script>
<style>
    .k-button{
        margin-bottom: 5px;
    }

    .divCell{
        text-align: left;
    }

    .textLabel{
        text-align: left;
        display:flex;
        width: 200px;
        float: left;
        align-items: flex-start;
    }

   .divRow{
        padding-bottom:5px;
        float: left;
    /*    border:1px solid black;*/
        width: 45%;
        /*overflow: hidden;*/
        margin-left:5px;
        margin-right:5px;
        padding-left:5px;
        padding-top:5px;
    }
</style>