﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviseBUDGETControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReviseBUDGETControl" %>

<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton, <%: this.ID %>_uploaderFile;
    var <%: this.ID %>_idProject = "";
    var <%: this.ID %>_dateCompletion;
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        items: [],
        itemsRequestedWork: [],
        itemInitiate: [],
        supportingDocuments: [],
        totalFilesToUpload: 0,
        isOtherChecked: false,
        otherDescription: "",
        parentForm: "",
        projectOID: -1,
        requestData: new common_ewrRequest(),
        onOtherChange: function (e) {
            this.set("isOtherChecked", !this.get("isOtherChecked"));
            if (!this.get("isOtherChecked")) {
                this.set("otherDescription", "");
            }
        },
        isEnabled: true,
        onValidationEntry: function () {
            var validatable = $("#<%: this.ID %>_RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validatable.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validatable.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }

            // cek jika pilih other di requested work, maka descriptionnya harus diisi
            var isRequestedWorkValid = true;
            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    if (!<%: this.ID %>_viewModel.get("otherDescription")) {
                        isRequestedWorkValid = false;
                    }
                }
            });

            if (!isRequestedWorkValid) {
                _showDialogMessage("Required Message", "Please fill Other's description in Type of Requested Work", "");
                return false;
            }

            return isValid;
        },
        onValidateOther: function() {
            var categoryOfWork = [];
            $.each(this.items, function (idx, el) {
                categoryOfWork.push({ TYPE: "categoryofwork", VALUE: el });
            });
            if (categoryOfWork.length == 0) {
                alert("Please select Category of Work");
                return false;
            }
            var typeOfRequestedWork = [];

            var otherDesc = this.otherDescription;

            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    typeOfRequestedWork.push({ TYPE: "requestofwork", VALUE: el, DESCRIPTION: otherDesc });
                } else {
                    typeOfRequestedWork.push({ TYPE: "requestofwork", VALUE: el });
                }

            });
            if (typeOfRequestedWork.length == 0) {
                alert("Please select Type of Requested Work");
                return false;
            }

            var today = new Date();
            today.setHours(0, 0, 0, 0);
            var completionDate = new Date(<%: this.ID %>_dateCompletion.value());

            if (completionDate.getTime() < today.getTime()) {
                alert("Date Completion must be equal or greater than today");
                return false;
            }

            var detailOfWork = [];
            $.each(this.itemInitiate, function (idx, el) {
                detailOfWork.push({ TYPE: "detailofwork", VALUE: el });
            });
            if (detailOfWork.length == 0) {
                alert("Please select Detail of Work Requirement");
                return false;
            }

            return true;
        },
        populateData: function() {
            
            var categoryOfWork = [];
            $.each(this.items, function (idx, el) {
                categoryOfWork.push({ TYPE: "categoryofwork", VALUE: el });
            });
            
            var typeOfRequestedWork = [];
            var otherDesc = this.otherDescription;
            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    typeOfRequestedWork.push({ TYPE: "requestofwork", VALUE: el, DESCRIPTION: otherDesc });
                } else {
                    typeOfRequestedWork.push({ TYPE: "requestofwork", VALUE: el });
                }

            });
            
            var detailOfWork = [];
            $.each(this.itemInitiate, function (idx, el) {
                detailOfWork.push({ TYPE: "detailofwork", VALUE: el });
            });
            
            var requestData =
            {
                OID: this.requestData.id,
                PROJECTNO: this.requestData.projectNo,
                PROJECTDESCRIPTION: this.requestData.projectDescription,
                PROJECTMANAGER: this.requestData.projectManager,
                BUDGETAMOUNT: this.requestData.budgetAmount,
                FILENAME: this.requestData.fileName,
                REQUESTORNAME: _currUserName,
                REQUESTOREMAIL: _currEMail,
                REQUESTORBN: _currBadgeNo,
                CATEGORYOFWORK: categoryOfWork,
                TYPEOFREQUESTEDWORK: typeOfRequestedWork,
                DETAILOFWORK: detailOfWork
            };
            return requestData;
            
        },
        onCloseClick: function (e) {
            this.onClearEvent(e);

            //_showDialogMessage("Close Form", "Close Request Form", <%: this.ID %>_viewModel.get("parentForm"));
            _showDialogMessage("Close Form", "Are you sure you want to close this form", "HomeForm.aspx");

        },
        dsEmployee: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsProjectManager: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsEmployee: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {

                    var searchParam = options.data.filter.filters[0].value;
                    if (searchParam && searchParam.length > 0) {
                        var result = [];
                        // if (this.get("selectedUser").length >= 3) {

                        //var filter = "?$filter=((EMPLOYEE_ID contains '*" + searchParam + "*') or lower(FULL_NAME) contains lower('*" + searchParam + "*')) and (LEVEL_INFO not contains '*L2*' and LEVEL_INFO not contains '*L3*' and LEVEL_INFO not contains '*L4*')";
                        var filter = "?$filter=(substringof('" + searchParam + "',EMPLOYEEID) or substringof(tolower('" + searchParam + "'),tolower(FULL_NAME)))";
                        //filter += " and (substringof('L2',LEVEL_INFO) eq true)";

                        var result = [];
                        $.ajax({
                            url: _webOdataUrl + "EmployeeOData" + filter,
                            type: "GET",
                            async: false,
                            success: function (data) {
                                //result = data.value;

                                options.success(data.value);
                            },
                            error: function (data) {
                                //return [];
                                //options.error(data.value);
                                options.error(data.value);
                            }
                        });
                    } else {
                        options.success([]);
                    }
                    // }

                }
            }
        }),
        dsEmployeeL2: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {

                    var searchParam = options.data.filter.filters[0].value;
                    if (searchParam && searchParam.length > 0) {
                        var result = [];
                        // if (this.get("selectedUser").length >= 3) {

                        //var filter = "?$filter=((EMPLOYEE_ID contains '*" + searchParam + "*') or lower(FULL_NAME) contains lower('*" + searchParam + "*')) and (LEVEL_INFO not contains '*L2*' and LEVEL_INFO not contains '*L3*' and LEVEL_INFO not contains '*L4*')";
                        var filter = "?$filter=(substringof('" + searchParam + "',EMPLOYEEID) or substringof(tolower('" + searchParam + "'),tolower(FULL_NAME)))";
                        filter += " and (substringof('L2',LEVEL_INFO) eq true)";

                        var result = [];
                        $.ajax({
                            url: _webOdataUrl + "EmployeeOData" + filter,
                            type: "GET",
                            async: false,
                            success: function (data) {
                                //result = data.value;

                                options.success(data.value);
                            },
                            error: function (data) {
                                //return [];
                                //options.error(data.value);
                                options.error(data.value);
                            }
                        });
                    } else {
                        options.success([]);
                    }
                    // }

                }
            }
        }),
        autocompleteFiltering: function (e) {
            var filter = e.filter;

            if (!filter.value) {
                //prevent filtering if the filter does not value
                e.preventDefault();
            }
        },
        onOperationalCPSelected: function (e) {
            <%: this.ID %>_viewModel.set("requestData.operationalCP", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.operationalCPBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.operationalCPEmail", e.dataItem.EMAIL);
            
        },
        onOperationalCPClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onOperationalCP2Selected: function (e) {
            <%: this.ID %>_viewModel.set("requestData.operationalCP2", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.operationalCP2BN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.operationalCP2Email", e.dataItem.EMAIL);
            
        },
        onOperationalCP2Close: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onProjectManagerSelect: function (e) {

            <%: this.ID %>_viewModel.set("requestData.projectManager", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.projectManagerBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.projectManagerEmail", e.dataItem.EMAIL);
        },
        onProjectSponsorSelected: function(e) {
            
            <%: this.ID %>_viewModel.set("requestData.projectSponsor", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.projectSponsorBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.projectSponsorEmail", e.dataItem.EMAIL);
        },
        onProjectSponsorClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        supportingDocumentColumnTemplate: function (dataItem) {
            if (dataItem && dataItem.REFDOCID) {
                var result = "<a href='" + "<%: DocServer %>" + "getdoc.aspx?id=" + dataItem.REFDOCID + "&op=n' target='_blank'>" + dataItem.FILENAME + "</a>";
                return result;
            }
            return "";
        },
        onRemove: function (e) {
            if (confirm("Are you sure you want to remove this document?")) {
                var currentDocuments = <%: this.ID %>_viewModel.get("supportingDocuments");

                var tr = $(e.target).closest("tr"); // get the current table row (tr)
                // get the data bound to the current table row
                var data = this.dataItem(tr);


                $.ajax({
                    url: _webApiUrl + "supportingdocument/removeSupportingDocumentById/" + data.OID,
                    type: "DELETE",
                    success: function (data) {
                        var suppDocs = $.grep(currentDocuments, function (elem, idx) {
                            return elem.OID == data.OID;
                        }, true);
                        <%: this.ID %>_viewModel.set("supportingDocuments", suppDocs);
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        _showDialogMessage("Error Message", "There's a problem when deleting the document", "");
                    }
                });
            }
        },
        dataBound: function (e) {
            var grid = e.sender;
            var dataItem;
            grid.tbody.find("tr[role='row']").each(function () {
                dataItem = grid.dataItem(this);
                //console.log(dataItem);
                // Show Print Button
                var isEnabled = <%: this.ID %>_viewModel.get("isEnabled");
                
                if (!isEnabled) {

                    $(this).find(".k-grid-Remove").hide();
                    //$(".k-grid-CancelRequest").find("span").addClass("k-icon k-i-print");
                } else {

                    $(this).find(".k-grid-Remove").hide();
                }

            });
        },
        onClearEvent: function (e) {
            this.requestData.set("projectNo", null);
            this.requestData.set("projectDescription", null);
            this.requestData.set("projectManager", null);
            this.requestData.set("budgetAmount", null);
            this.requestData.set("fileName", null);
            this.set("otherDescription", "");
            this.set("isOtherChecked", false);
            this.set("items", []);
            this.set("itemsRequestedWork", []);
            this.set("itemInitiate", []);
        }
        
    });

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    // Document Ready
    $(document).ready(function () {
        //<%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#<%: this.ID %>_RequestContent"), <%: this.ID %>_viewModel);

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        <%: this.ID %>_viewModel.onClearEvent()
        $("#<%: this.ID %>_RequestContent").kendoTooltip({
            filter: "#<%: this.ID %>_shareFolderDoc",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format Share Folder, e.g.: \\\\netapp8\\folder";
                return content;
            }
        }).data("kendoTooltip");

        <%: this.ID %>_viewModel.requestData.set("dateIssued", new Date());

        if (!<%: this.ID %>_uploaderFile) {
            <%: this.ID %>_uploaderFile = $("#<%: this.ID %>_uploaderFile").kendoUpload({
                async: {
                    saveUrl:  _webApiUrl + "upload/PostUploadBudgetApproval/1",
                    removeUrl: "remove",
                    batch: true,
                    autoUpload: false

                },
                select: projectBudgetApprovalModel.onAttachmentSelect,
                upload: projectBudgetApprovalModel.onUploadAtt,
                success: projectBudgetApprovalModel.onSuccessAtthment,
                eror: projectBudgetApprovalModel.onErrorAttachment,
                remove: <%: this.ID %>_onRemove,
                localization: {
                    select: 'Add Document...'
                },
                upload: function (e) { 
                    
                    e.data = { typeName: "BUDGETAPPROVAL", badgeNo: _currBadgeNo, typeId: projectBudgetApprovalModel.curBudgetApprovalId };
                }
            }).data("kendoUpload");
        }

        <%: this.ID %>_dateCompletion = $("#<%: this.ID %>_dateOfCompletion2").kendoDatePicker({
            value: new Date(),
            format: "dd MMM yyyy",
            min: new Date()
        }).data("kendoDatePicker");

    }); //doc ready

    function <%: this.ID %>_onUploadAtt(e) {
        var files = e.files;
        var filecount = <%: this.ID %>_viewModel.get("totalFilesToUpload");
        filecount += 1;
        <%: this.ID %>_viewModel.set("totalFilesToUpload", filecount);
        
        setTimeout(function () {
            <%: this.ID %>_kendoUploadButton = $(".k-upload-selected");
            <%: this.ID %>_kendoUploadButton.hide();
        }, 1);
    }

    function <%: this.ID %>_onRemove(e) {
        var files = e.files;
        var filecount = <%: this.ID %>_viewModel.get("totalFilesToUpload");
        filecount -= 1;
        <%: this.ID %>_viewModel.set("totalFilesToUpload", filecount);
        
    }
        
    function <%: this.ID %>_onSuccessAtthment(e) {
       
        <%--$.ajax({
            url: _webApiUrl + "supportingdocument/findSuppDocByTypeNameAndID/EWR|" + <%: this.ID %>_viewModel.get("ewrOID"),
            type: "GET",
            success: function (data) {
                
                <%: this.ID %>_viewModel.set("supportingDocuments", data);
            },
            error: function (jqXHR) {
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                        
                _showDialogMessage("Error Message", "There's a problem when deleting the document", "");
            }
        });--%>
        
        <%: this.ID %>_uploaderFile.clearAllFiles();

    }

    function <%: this.ID %>_onErrorAttachment(e) {
        // Array with information about the uploaded files
        var files = e.files;
        var err = e.XMLHttpRequest;

        if (e.operation == "upload") {
            _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
        }
    }
    function <%: this.ID %>_InitViewOnly(dataRequest, parentForm) {
        <%: this.ID %>_Init(dataRequest, parentForm);
        <%: this.ID %>_viewModel.set("isEnabled", false);
        
        <%: this.ID %>_uploaderFile.disable();
    }
    function <%: this.ID %>_Init(dataRequest, parentForm) {

        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        // jika dataRequest ada data nya maka update
        if (dataRequest) {
            var requestData =
            {
                id: dataRequest.OID,
                //ewrNo: 
                projectNo: dataRequest.PROJECTNO,
                projectDescription: dataRequest.PROJECTDESCRIPTION,
                projectManager: dataRequest.PROJECTMANAGER,
                budgetAmount: dataRequest.BUDGETAMOUNT,
                fileName: dataRequest.FILENAME
            };

            <%: this.ID %>_viewModel.set("requestData", requestData);
            <%: this.ID %>_viewModel.set("budgetOID", dataRequest.OID);

            var arrReqWork = [];
            $.each(dataRequest.TYPEOFREQUESTEDWORK, function (idx, elem) {
                arrReqWork.push(elem.VALUE);
                if (elem.VALUE == "other") {
                    <%: this.ID %>_viewModel.set("otherDescription", elem.DESCRIPTION);
                    <%: this.ID %>_viewModel.set("isOtherChecked", true);
                }
            });
            <%: this.ID %>_viewModel.set("itemsRequestedWork", arrReqWork);

            var arrCat = [];
            $.each(dataRequest.CATEGORYOFWORK, function (idx, elem) {
                arrCat.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("items", arrCat);

            var arrInitiate = [];
            $.each(dataRequest.DETAILOFWORK, function (idx, elem) {
                arrInitiate.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("itemInitiate", arrInitiate);
            if (dataRequest.SUPPORTINGDOCUMENTS) {
                <%: this.ID %>_viewModel.set("supportingDocuments", dataRequest.SUPPORTINGDOCUMENTS);
            } else {
                <%: this.ID %>_viewModel.set("supportingDocuments", []);
            }
            

<%--            $('#<%: this.ID %>_create').hide();
            $('#<%: this.ID %>_update').show();--%>
        } else {
           <%-- $('#<%: this.ID %>_create').show();
            $('#<%: this.ID %>_update').hide();--%>
        }

        
    }
</script>
<div id="<%: this.ID %>_RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_inputProjectLegend">Budget Approval</legend>
                <div>
                    <label class="labelFormWeight">Project No</label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.projectNo, enabled: isEnabled" maxlength="50" placeholder="Project No" />
                </div>
                <div>
                    <label class="labelFormWeight">Project Description<span style="color: red">*</span></label>
                    <input class="k-textbox" style="width: 400px;" type="text" data-bind="value: requestData.projectDescription, enabled: isEnabled" maxlength="200" placeholder="Project Description" required />
                </div>
            <div>
                <div>
                    <label class="labelFormWeight">Project Manager<span style="color: red">*</span></label>
                    <%--<input class="k-textbox" style="width: 400px;" type="text" data-bind="value: momData.projectManager, enabled: isEnabled" placeholder="Project Manager" required />--%>
                    <input data-role="autocomplete"
                        data-placeholder="Type Employee Name (min 3 char)"
                        data-text-field="FULL_NAME"
                        data-min-length=3
                        data-auto-bind="false"
                        data-filter="contains"
                        data-bind="value: requestData.projectManager,
                                    source: dsProjectManager,
                                    enabled: isEnabled,
                                    events: {
                                        select: onProjectManagerSelect,
                                        filtering: autocompleteFiltering
                                    }"
                        style="width: 400px;"
                        required
                    />
                </div>
            <div>
                <label class="labelFormWeight">Budget Approval</label>
                <input data-role="numerictextbox"
                    data-format="c"
                    data-min="0"
                    data-bind="value: requestData.budgetAmount, enabled: isEnabled"
                    style="width: 180px">
            </div>
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Detail of Work Requirement</legend>
                
                    <div>
                        <label class="labelFormWeight">4. File Name</label>
                        <div data-role="grid" data-bind="source: supportingDocuments, events: { dataBound: dataBound }"
                            data-columns="[
                            { 
                                'field': 'FILENAME', 
                                'title': 'File Name', 'width': '50%', 
                                template: <%: this.ID %>_viewModel.supportingDocumentColumnTemplate
                            },
                            { 
                                command: [
                                {
                                 name: 'Remove',
                                 click: <%: this.ID %>_viewModel.onRemoveDocument
                                }
                              ]
                            }
                            ]"
                            >

                        </div>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td style="width: 50%"><input type="file" id="<%: this.ID %>_fileNameId" name="<%: this.ID %>_fileName" /></td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </div>
        </fieldset>
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
