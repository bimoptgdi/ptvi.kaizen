﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArchiveProjectListControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ArchiveProjectListControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>
<%@ Register Src="~/UserControl/RequestProjectControl.ascx" TagPrefix="uc1" TagName="RequestProjectControl" %>


<div id="<%: this.ID %>_viewReportsPanel">
    <div>
        <fieldset data-bind="invisible: searchOption">
            <legend>Search</legend>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Project No</div>
                <div style="display: inline-block;padding-left: 20px">
                    <input id="<%: this.ID %>_projectNo" 
                        type= "text" 
                        class= "k-textbox" 
                        data-bind="value: projectNo"/>
                </div>
            </div>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Description</div>
                <div style="display: inline-block;padding-left: 20px">
                    <input id="<%: this.ID %>_description" 
                        type= "text" 
                        class= "k-textbox" 
                        data-bind="value: description"
                        style="width: 250px"/>
                </div>
            </div>
            <div style="padding-bottom: 10px">
                <div style="display: inline-block; width: 150px">Project Engineer:</div> 
                <div style="padding-left: 20px; width: 400px; display: inline-block;vertical-align: middle;">
                    <select id="<%: this.ID %>_msPE" 
                        style="width: 400px;"
                        data-role="multiselect"
                        data-placeholder="Project Engineer"
                        data-value-primitive="true"
                        data-text-field="employeeName"
                        data-value-field="employeeId"
                        data-readonly="true"
                        data-bind="value: valuePE, source: dataPE"></select>
                </div>
            </div>
            <div>
                <div style="display: inline-block;padding-bottom: 20px; width: 150px">Report Overdue</div>
                <div style="display: inline-block;padding-left: 20px">
                           <%--<input data-role="combobox"
                            data-placeholder="Select Year"                            
                            data-text-field="year"
                            data-value-field="year"
                            data-bind="value: valueYear, source: dsYearProject"
                            style="width: 250px" />--%>
                            <input data-role="datepicker"
                            data-placeholder="From"                            
                            data-bind="value: valueYearFrom"
                                data-format="dd MMMM yyyy" 
                            style="width: 250px" />
                            - <input data-role="datepicker"
                            data-placeholder="To"                            
                            data-bind="value: valueYearTo"
                                data-format="dd MMMM yyyy" 
                            style="width: 250px" />
                </div>
            </div>
            <div id="<%: this.ID %>_btnSearch" 
                data-role="button"
                data-bind="events: {click: searchClick}"
                >Search
            </div>
            <div id="<%: this.ID %>_btnReset" 
                data-role="button"
                data-bind="events: {click: resetClick}"
                >Reset
            </div>

        </fieldset>
    </div>
   <fieldset data-bind="visible: searchOption" >
    <div style="text-align: center; font-size: 16px; font-weight: bold;">Task Overdue Report<br /><br /><br /></div>
    <div id="<%: this.ID %>_grdTaskOverdueReport" data-role="grid" style="width: 750px"
        data-bind="source: viewReportsSource, events: { excelExport: excelExport, dataBound: onDataBound }" 
        data-filterable= "true"
        data-height="450"
        <%--data-toolbar="['excel']"--%>
        data-toolbar= "[{ name: 'excelExport', text: 'Export to Excel'}, { name:'generatePDF', text: 'Generate PDF' }]", 
        data-scrollable="false"
        data-excel="{
                allPages: true,
            }"
        data-columns="[ 
        
{ 
                        field: 'pnoName', title: 'Project No. / Name ', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        field: 'tipeName', title: 'Type', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100, template: '#: tipeName #'
                    },
                    { 
                        field: 'desc', title: 'Description', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        title: 'Plan Date', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100,
                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                        columns: [
                                    {
                                        field: 'psd', title: 'Start',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: psd ? kendo.toString(kendo.parseDate(psd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    },
                                    {
                                        field: 'pfd', title: 'Finish',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: pfd ? kendo.toString(kendo.parseDate(pfd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    }]
                    },  
            { 
                        title: 'Actual Date', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100,
                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                        columns: [
                                    {
                                        field: 'asd', title: 'Start',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: asd ? kendo.toString(kendo.parseDate(asd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    },
                                    {
                                        field: 'afd', title: 'Finish',
                                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        attributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                        template: '#: afd ? kendo.toString(kendo.parseDate(afd,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#',
                                        width: 70
                                    }]

                    },
                    { 
                        field: 'pic', title: 'PIC', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
                    { 
                        field: 'statusName', title: 'Status', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100
                    },
        { 
                        title: 'Updated', 
                        attributes: { style: 'vertical-align: top !important;' },
                        width: 100,
                        headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                        columns: [
                                    { 
                                        field: 'updatedByName', title: 'By', 
                                        attributes: { style: 'vertical-align: top !important;' },
                                        width: 80
                                    },
                                    { 
                                        field: 'updatedDate', title: 'Date', 
                                        attributes: { style: 'vertical-align: top !important;' },
                                        width: 70,template: '#: updatedDate ? kendo.toString(kendo.parseDate(updatedDate,\'yyyy-MM-dd\'), \'dd MMM yyyy\') : \'\'#'
                                    },
                                 ]
                    },
        ]"
        data-pageable="true"
        ></div>     
            <div style="text-align: right;" data-bind="visible: printOption">Exported at [<span id="currentDate"></span>]</div>
        </div>                 
        <script id="<%: this.ID %>_projectNo-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                No:
            </div>
            <div> 
                #= data.projectNo # 
            </div>
            <div class='subColumnTitle'>
                Description:
            </div>
            <div> 
                #= data.projectDesc # 
            </div>
        </script>
        <script id="<%: this.ID %>_projectManager-template" type="text/x-kendo-template">
            <div> 
            #: data.projectManager #
            </div>
            <div> 
            <b>Action</b>
            </div>
            <div class='subColumnTitle'>
                Reason:
            </div>
            <div> 
                #: data.reason #
            </div>
            <div class='subColumnTitle'>
                Comment
            </div>
            <div> 
                #= data.comment # 
            </div>
        </script>
        <script id="<%: this.ID %>_action-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                Reason:
            </div>
            <div> 
                #: data.reason #
            </div>
            <div class='subColumnTitle'>
                Comment
            </div>
            <div> 
                #= data.comment # 
            </div>
        </script>
        <script id="<%: this.ID %>_PIC-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                Process:
            </div>
            <div> 
                #= data.picProcessName # 
            </div>
            <div class='subColumnTitle'>
                Name
            </div>
            <div> 
                #= data.picName # 
            </div>
        </script>
        <script id="<%: this.ID %>_date-template" type="text/x-kendo-template">
            <div class='subColumnTitle'>
                Plan:
            </div>
            <div> 
                #: data.planDate ? kendo.toString(data.planDate, 'dd MMM yyyy') : '-' #
            </div>
            <div class='subColumnTitle'>
                Submitted
            </div>
            <div> 
                #= data.submittedDate ? kendo.toString(data.submittedDate, 'dd MMM yyyy') : '-' # 
            </div>
        </script>
</div>           
    </fieldset>
</div>
<style>
.tooltip {
        position: relative;
        display: inline-block;
        text-align: center;
        margin: 0 0 0 4px;
        cursor: pointer;
    }
    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        color: #fff;
        border-radius: 6px;
        width: 200px;
        text-align: left;
        padding-left: 10px;
        padding-right: 10px;
        background-color: #808080;
        padding-bottom: 10px;
             
        /* Position the tooltip text - see examples below! */
        position: absolute;
        top: -5px;
        right: 105%; 
        z-index: 999999;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .tooltip:hover {
        color: #fff;
        padding-right: 1px;
        padding-top: 1px;
        box-sizing: padding-box;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        border-radius: 100%;
        -webkit-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        transition: all 0.3s linear;
        color: #fff;
        border: none;
    }
    .tooltip .tooltiptext::after {
        content: " ";
        position: absolute;
        top: 50%;
        left: 100%; /* To the right of the tooltip */
        margin-top: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: transparent transparent transparent #808080;
    }
    .tooltip_wrapper {
        display: inline-block;
        width: 30px;
    }
    .hrStyle {
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -moz-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -ms-linear-gradient(left, #808080, #ffffff, #808080);
        background-image: -o-linear-gradient(left, #808080, #ffffff, #808080);
    }
</style>

<script type="text/x-kendo-template" id="<%: this.ID %>_approved-template">

# var approvedText = "In Progress"; #    
# if (data.reasonClosure == 'DEL') { approvedText = "Deleted" } else if (data.isApproved) { approvedText = "Approved" } #
<div>
    #: approvedText #
</div>

</script>

<script id="<%: this.ID %>_dateCreated" type="text/x-kendo-template">
    <div> #= createdDate? kendo.toString(kendo.parseDate(createdDate), "dd MMM yyyy"):'-' #</div>
</script>



<script>
    $("#currentDate").text(kendo.toString(new Date(), "dd MMM yyyy HH:mm:ss"));
    var <%: this.ID %>_vmPopViewReports = new kendo.observable({
        printOption: false,
        searchOption: false,
        projectNo: "",
        description: "",
        peParam: "",
        peParamName: "",
        valueYear: new Date().getFullYear(),
        valueYearFrom: new Date(),
        valueYearTo: new Date(),
        onDataBound: function () {
            window.status = 'ready_to_print';
        },
        viewReportsSource: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTaskOverdueReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTaskOverdueReport"), false);
            },
            transport: {
                read: {
                    async: false,
                    url: function (data) {
                        var from = kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearFrom, "dd MMM yyyy");
                        var to = kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearTo, "dd MMM yyyy");
                        var taskOverdueReport = _webApiUrl + "Report/listTaskOverdueReport/?projectNo=" + encodeURIComponent(<%: this.ID %>_vmPopViewReports.projectNo) + "&description=" + encodeURIComponent(<%: this.ID %>_vmPopViewReports.description) + "&peParam=" + encodeURIComponent(<%: this.ID %>_vmPopViewReports.valuePE.join("|")) + "&from=" + kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearFrom, 'MM/dd/yyyy') + "&to=" + kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearTo, 'MM/dd/yyyy')
                        return taskOverdueReport
                    },
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        pNo: { type: "string" },
                        projectNo: { type: "string" },
                        docNo: {type: "string" },
                        tipe: { type: "string" },
                        tipeName: { type: "string" },
                        description: {type: "string" },
                        name: { type: "string" },
                        pic: { type: "string" },
                        afd: { type: "date" },
                        pfd: { type: "date" },
                        asd: { type: "date" },
                        psd: { type: "date" },
                        from: { type: "date" },
                        to: {type: "date"},
                        status: { type: "number" },
                        statusName: { type: "string" },
                        updatedBy: { type: "string" },
                        updatedDate: { type: "date" },
                    }
                },
                parse: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        if (response[i].pic != null)
                            response[i].pic = response[i].pic.replace(/,/g, "\n");
                    }
                    
                    // <%: this.ID %>_vm.ds_Deliverable.data().push.apply(<%: this.ID %>_vm.ds_Deliverable.data(), response[0]);
                    return response;
                }
            },
            sort: [{ field: "psd", dir: "asc" }, { field: "pfd", dir: "asc" }]
        }),
        dataPE: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTaskOverdueReport"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTaskOverdueReport"), false);
            },
            transport: {
                async: false,
                read: {
                    dataType: "json",
                    url: _webApiUrl + "v_projectEmployeeRole/listEmployeeByRoleId/3"
                },
                parameterMap: function (options, operation) {
                }
            },
            schema: {
                model: {
                    id: "employeeId",
                },
            },
            sort: [{ field: "employeeName", dir: "asc" }],
            pageSize: 10,
            batch: false
        }),
        valuePE: [],
        generateExcel: function (e) {
            var currentDate = kendo.toString(new Date, "- dd MMM yyyy - HH_mm_ss");
            var rows = [
                {
                    cells: [
                        {
                            value: "Task Overdue Report",
                            fontSize: 20,
                            textAlign: "center",
                            verticalAlign: "center",
                            background: "#60b5ff",
                            color: "#ffffff"
                        }
                    ],
                    type: "header",
                    height: 70
                },
                {
                    cells: [
                        { value: "Project No / Name", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        //{ value: "Project Desc", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        //{ value: "Project Engineer", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        //{ value: "From", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        //{ value: "To", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Type", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "'Description', ", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Plan Start Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Plan Finish Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Actual Start Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Actual Finish Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "PIC", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Updated By Name", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Updated Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        { value: "Status", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        //{ value: "Submitted Date", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" },
                        //{ value: "Submitted By", background: "#7a7a7a", colSpan: 1, color: "#fff", rowSpan: 1, border: "black", align: "center" }
                    ],
                    type: "header"
                }];
            var dataRow = this.viewReportsSource;

            dataRow.fetch(function () {
                var data = this.data();
                for (var i = 0; i < data.length; i++) {
                    //push single row for every record
                    rows.push({
                        cells: [
                          { value: data[i].pnoName },
                          { value: data[i].tipeName },
                          { value: data[i].description },
                          { value: data[i].planStartDate, format: "dd MMM yyyy" },
                          { value: data[i].planFinishDate, format: "dd MMM yyyy" },
                          { value: data[i].actualStartDate, format: "dd MMM yyyy" },
                          { value: data[i].actualFinishDate, format: "dd MMM yyyy" },
                          { value: data[i].picProcessName },
                          { value: data[i].picName },
                          { value: data[i].status },
                          { value: data[i].statusName},
                          { value: data[i].updatedDate, format: "dd MMM yyyy" },
                          { value: data[i].updatedByName }
                        ]
                    })
                }
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                      {
                          columns: [
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { autoWidth: true },
                            { width: 110, autoWidth: false },
                            { width: 110, autoWidth: false },
                            { autoWidth: true }
                          ],
                          // Title of the sheet
                          title: "Reports",
                          // Rows of the sheet
                          rows: rows,
                          frozenRows: 2,
                          mergedCells: ["A1:K1"]
                      }
                    ]
                });

                //sheet.rows.splice(0, 0, { cells: myHeaders, type: "header", height: 70 });

                kendo.saveAs({
                    dataURI: workbook.toDataURL(),
                    fileName: "Task Overdue Report " + currentDate + ".xlsx",
                    proxyURL: "<%:WebApiUrl%>" + "exportproxy"
                });
            });
        },
        //excelExport: function (e) {
        //    var sheet = e.workbook.sheets[0];
        //    for (var i = 0; i < sheet.rows.length; i++) {
        //        var str = sheet.rows[i].cells[4].value;
        //        var res = str.replace(/<br\/>/g, "\n");
        //        sheet.rows[i].cells[4].value = res;
        //        str = sheet.rows[i].cells[5].value;
        //        res = str.replace(/<br\/>/g, "\n");
        //        sheet.rows[i].cells[5].value = res;
        //        sheet.rows[i].cells[6].format = "#,##0.0_);-#,##0.0;0.0;"
        //        sheet.rows[i].cells[7].format = "#,##0.0_);-#,##0.0;0.0;"
        //        sheet.rows[i].cells[8].format = "#,##0.0_);-#,##0.0;0.0;"
        //        sheet.rows[i].cells[9].format = "#,##0.0_);-#,##0.0;0.0;"
        //        sheet.rows[i].cells[10].format = "#,##0.0_);-#,##0.0;0.0;"
        //    }
        //},
        searchClick: function (e) {
            //this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + encodeURIComponent(this.projectNo) + "&description=" + encodeURIComponent(this.description) + "&projectManager=" + encodeURIComponent(this.valuePM.join("|")) + "&reason=" + encodeURIComponent(this.valueReasons.join("|"));
            //this.viewDetailsSource.read();
            if (<%: this.ID %>_vmPopViewReports.projectNo || <%: this.ID %>_vmPopViewReports.description ||
                <%: this.ID %>_vmPopViewReports.valuePE.length != 0 || <%: this.ID %>_vmPopViewReports.valueYearFrom.length != 0 || <%: this.ID %>_vmPopViewReports.valueYearTo.length != 0) {
                var urlOpenWindow = _webAppUrl + "TaskOverdueReportPrint.aspx?_Search=true&_No=" + encodeURIComponent(<%: this.ID %>_vmPopViewReports.projectNo) + "&_Desc=" + encodeURIComponent(<%: this.ID %>_vmPopViewReports.description) + "&_PE=" + encodeURIComponent(<%: this.ID %>_vmPopViewReports.valuePE.join("|")) + "&_From=" + kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearFrom, 'MM/dd/yyyy') + "&_To=" + kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearTo, 'MM/dd/yyyy');
                window.open(urlOpenWindow)
            }
            else {
                $('<div/>').kendoAlert({
                    title: 'Search Request',
                    modal: true,
                    content: "<div>Search Criteria can't be empty</div>",
                    actions: [{ text: "OK" }]
                }).data('kendoAlert').open();
            }
        },
        createpdf: function (e) {
            //this.viewDetailsSource.options.transport.read.url = _webApiUrl + "closeout/ListCloseOut/?projectNo=" + encodeURIComponent(this.projectNo) + "&description=" + encodeURIComponent(this.description) + "&projectManager=" + encodeURIComponent(this.valuePM.join("|")) + "&reason=" + encodeURIComponent(this.valueReasons.join("|"));
            //this.viewDetailsSource.read();
            var date1 = kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearFrom, 'yyyy-MM-dd').replace(/\//g, "|");
            var date2 = kendo.toString(<%: this.ID %>_vmPopViewReports.valueYearTo, 'yyyy-MM-dd').replace(/\//g, "|");
            var urlPDF = encodeURIComponent(_webAppUrl.replace(/\//g, "|") + "TaskOverdueReportPrint.aspx?_Print=true&_Search=true&_No=" + <%: this.ID %>_vmPopViewReports.projectNo + "&_Desc=" + <%: this.ID %>_vmPopViewReports.description + "&_PE=" + <%: this.ID %>_vmPopViewReports.valuePE.join("|") + "&_From=" + date1 + "&_To=" + date2);
            var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20" + urlPDF + "/";
            //var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--window-status%20ready_to_print%20%20" + urlPDF + "/";

            window.open(pdfUrl);
        },

        resetClick: function (e) {
            this.set("projectNo", "");
            this.set("description", "");
            this.set("valuePE", []);
            this.set("valueYearFrom", new Date().getFullYear());
            this.set("valueYearTo", new Date().getFullYear());
            this.viewReportsSource.options.transport.read.url = _webApiUrl + "Report/listTaskOverdueRepot/?projectNo=" + this.projectNo + "&description=" + this.description + "&peParam=" + this.valuePE + "&from=" + this.valueYearFrom + "&to=" + this.valueYearTo;
            this.viewReportsSource.read();
        },

    });

    var urlParams = new URLSearchParams(window.location.search),
        _Print = "<%:Request["_Print"]%>",
        _Search = "<%:Request["_Search"]%>",
        _No = "<%:Request["_No"]%>",
        _Desc = "<%:Request["_Desc"]%>",
        _PE = "<%:Request["_PE"]%>",
        _From = "<%:Request["_From"]%>",
        _To = "<%:Request["_To"]%>";

     if (_Print) {
            <%: this.ID %>_vmPopViewReports.set("printOption", true);
        }  
    if (_Search) {
        <%: this.ID %>_vmPopViewReports.set("searchOption", true);
    }    
    if (_No) {
        <%: this.ID %>_vmPopViewReports.set("projectNo", _No);
    }    
    if (_Desc) {
        <%: this.ID %>_vmPopViewReports.set("description", _Desc);
    }
    if (_PE) {
        <%: this.ID %>_vmPopViewReports.set("valuePE", _PE.split("|"));
    }
    if (_From) {
            <%: this.ID %>_vmPopViewReports.set("valueYearFrom", _From);
    }
    if (_To) {
            <%: this.ID %>_vmPopViewReports.set("valueYearTo", _To);
     }

    <%: this.ID %>_popPickEngineer = $("#<%: this.ID %>_popPickEngineer").kendoWindow({
        title: "Timesheets Engineer",
        modal: true,
        visible: false,
        resizable: false,
        width: 800,
        height: 450
    }).data("kendoWindow");

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_viewReportsPanel"), <%: this.ID %>_vmPopViewReports);
        if (<%: this.ID %>_vmPopViewReports.printOption) {
            $(".k-grid-toolbar").hide();
            $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-header").css("padding", "0 !important");
           $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-content").css("overflow-y", "visible");
        }
        else {
           $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-generatePDF").find('span').addClass("k-font-icon k-i-pdf");
           $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-generatePDF").find('span').css({ "vertical-align": "text-top", "margin-left": "-.3rem", "margin-right": ".3rem" });
           $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-generatePDF").bind("click", function (e) {
                <%: this.ID %>_vmPopViewReports.createpdf();
            });

           $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-excelExport").find('span').addClass("k-font-icon k-i-xls");
           $("#<%: this.ID %>_grdTaskOverdueReport .k-grid-excelExport").bind("click", function (e) {
                <%: this.ID %>_vmPopViewReports.generateExcel();
            });
        }
    });
</script>