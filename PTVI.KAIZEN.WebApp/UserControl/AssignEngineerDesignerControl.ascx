﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignEngineerDesignerControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.AssignEngineerDesigner" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>

<style>
    .FormTitlePanel {
        border: 1px solid black;
        border-radius: 4px 4px 0 0;
        color: #fff;
        padding: .65em .92em;
        display: inline-block;
        border-bottom-width: 0;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 0px;
        padding-bottom: 7px;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #ccc;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        top: 1px;
    }

    .FormContentPanel {
        top: 10px;
        border-radius: 0px 4px 0 0;
        border: 1px solid black;
        border-top-width: 1;
        margin-top: 0px;
        color: #333;
        padding: .65em .92em;
        display: inline-block;
        box-sizing: content-box;
        text-decoration: none;
        background-color: #fff;
        border-color: #ccc;
        font-size: 100%;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
    }
</style>
<script type="text/x-kendo-template" id="ckBoxTemplate">
    <input type="checkbox" #= isActive ? "checked='checked'" : "" # disabled />
</script>
<div style="display:none" id="popUpEmployee">
                <uc1:PickEmployeesControl runat="server" ID="PickEmployeesControl" />
</div>

    <div id="<%: this.ID %>_FormContent">
      <div class="FormContentPanel">
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignEngineer))
                { %>
            <fieldset>
                <legend>Engineer</legend>
                <div>
                    <div id="<%: this.ID %>_grdEngineer" data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
<%--                        {title: 'Position', template: '#= positionName() #', width: 300 },--%>
                        {field: 'employeePosition', title: 'Position', template: '#= positionName() #', editor: <%: this.ID %>_positionDropDown, width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75 }

                     <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignEngineer))
                        { %>
                        ,
                        {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}}], width: 150 }
                    <% } %>
                        ]" data-editable= "inline", data-role="grid" data-bind="source: EngineerSource, events: { save: saveEngineer, edit: editEngineer }", 
                        data-pageable="true"<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignEngineer))
                        { %>,                     
                        data-toolbar= "[{name: 'create', text: 'Add New Engineer'}]"
                    <% } %>
                        ></div>
                </div>
            </fieldset>
            <% }%>
            <br />
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.AssignDesigner))
                { %>
            <fieldset>
                <legend>Designer</legend>
                <div>
                    <div id="<%: this.ID %>_grdDesigner" data-role="grid" data-bind="source: DesignerSource, events: { save: saveDesigner, edit: editDesigner }" data-columns="[
                        {field:'employeeName', title: 'Name', editor:<%: this.ID %>_getEmployee, width: 250 },
<%--                        {title: 'Position', template: '#= positionName() #', width: 300 },--%>
                        {field: 'employeePosition',name: 'Position',  title: 'Position', template: '#= positionName() #', editor: <%: this.ID %>_positionDropDownDesigner, width: 300 },
                        {field:'joinedDate', title: 'Join Date', template: '#= kendo.toString(joinedDate,\'dd MMM yyyy\') #', width: 100 },
                        {field:'isActive', title: 'Active',template: kendo.template($('#ckBoxTemplate').html()), width: 75}
                     <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignDesigner))
                        { %>
                        ,
                        {command: [{name:'edit', text:{edit: 'Edit', update: 'Save'}}] }
                        <% } %>
                        ]" data-editable= "inline", data-pageable="true"<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.AssignDesigner))
                        { %>, 
                        data-toolbar= "[{name: 'create', text: 'Add New Designer'}]"
                        <% } %>
                        ></div>
                </div>
            </fieldset>
            <% }%>
        </div>
    </div>

    <script>
        $("#popUpEmployee").kendoWindow({
            title: "Pick Employee",
            width: 700,
            activate: function () {
                $("#PickEmployeesControl_employeeSearch").select();
            }
        });

        var <%: this.ID %>_engineerSource = new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdEngineer"), true);
            },
            requestEnd: function (e) {
                if (e.type == "update" || e.type == "create")
                    <%: this.ID %>_viewModel.EngineerSource.read();
                kendo.ui.progress($("#<%: this.ID %>_grdEngineer"), false);
            },
            transport: {
                create: { url: _webApiUrl + "UserAssignment/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                read: { url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1" + "?AssignmentType=PE&PrID=" + _projectId, dataType: "json" },
                update: {
                    type: "PUT", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "UserAssignment/PutUserAssignmentObject/" + e.id;
                    }

                },
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.projectId = _projectId;
                        options.isActive = true;
                        options.assignmentType = "PE";
                        options.employeeId = <%: this.ID %>_viewModel.employeeId
                        options.employeeName = <%: this.ID %>_viewModel.employeeName;
                        options.employeeEmail = <%: this.ID %>_viewModel.employeeEmail;
                        //options.employeePosition = <%: this.ID %>_viewModel.employeePosition;
                        options.createdBy = _currNTUserID;
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                    if (operation == "update") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.projectId = _projectId;
                        options.assignmentType = "PE";
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        technicalSupportId: { type: "number" },
                        assignmentType: { type: "string" },
                        employeeId: { type: "string"},
                        employeeName: { type: "string" },
                        employeeEmail: { type: "string" },
                        employeePosition: {
                            type: "string"
                            },
                        joinedDate: { type: "date", defaultValue: null, validation: {<% if (_planStartDate != "null")
                            { %> min: _planStartDate, <% }
                            else if (_planFinishDate != "null")
                            { %> max: _planFinishDate <% } %> } } ,
                        isActive: { type: "boolean" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                    positionName: function () {
            //change method
            //return common_getPositionByPositionID(this.employeePosition);
            //return common_getPositionName(this.employeePosition);
            if (<%: this.ID %>_dsLookPositionEngineer.data().length == 0) <%: this.ID %>_dsLookPositionEngineer.read();
                        return <%: this.ID %>_dsLookPositionEngineer.get(this.employeePosition) ? <%: this.ID %>_dsLookPositionEngineer.get(this.employeePosition).text : "-"
                    }
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_engineerSource.read();
                        alert("Process failed: " + d);
                        return 0;
                    } else {
                        return d;
                        //                        < %: this.ID %>_engineerSource.read();
                    }
                },
            },
            pageSize: 10,
            change: <%: this.ID %>_change,
        });

        var <%: this.ID %>_designerSource = new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdDesigner"), true);
            },
            requestEnd: function (e) {
                if (e.type == "update" || e.type == "create")
                    <%: this.ID %>_viewModel.DesignerSource.read();
                kendo.ui.progress($("#<%: this.ID %>_grdDesigner"), false);
            },
            transport: {
                create: { url: _webApiUrl + "UserAssignment/postUserAssignmentObject/1", type: "POST", dataType: "json" },
                read: { url: _webApiUrl + "UserAssignment/GetByAssTypePrID/1" + "?AssignmentType=PD&PrID=" + _projectId, dataType: "json" },
                update: {
                    type: "PUT", dataType: "json",
                    url: function (e) {
                        return _webApiUrl + "UserAssignment/PutUserAssignmentObject/" + e.id;
                    }
                },
                //                update: { url: _webApiUrl + "UserAssignment", type: "PUT", dataType: "json" }
                parameterMap: function (options, operation) {
                    if (operation == "create") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.projectId = _projectId;
                        options.isActive = true;
                        options.assignmentType = "PD";
                        options.employeeId = <%: this.ID %>_viewModel.employeeId
                        options.employeeName = <%: this.ID %>_viewModel.employeeName;
                        options.employeeEmail = <%: this.ID %>_viewModel.employeeEmail;
                        //options.employeePosition = <%: this.ID %>_viewModel.employeePosition;

                        options.createdBy = _currNTUserID;
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                    if (operation == "update") {
                        options.joinedDate = kendo.toString(options.joinedDate, 'MM/dd/yyyy');
                        options.createdDate = kendo.toString(options.createdDate, 'MM/dd/yyyy');
                        options.updatedDate = kendo.toString(options.updatedDate, 'MM/dd/yyyy');
                        options.projectId = _projectId;
                        options.assignmentType = "PD";
                        options.updatedBy = _currNTUserID;
                        return options;
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: "number" },
                        projectId: { type: "number" },
                        technicalSupportId: { type: "number" },
                        assignmentType: { type: "string" },
                        employeeId: { type: "string"},
                        employeeName: { type: "string", },
                        employeeEmail: { type: "string" },
                        employeePosition: {
                            type: "string", validation: {
                            }
                        },
                        joinedDate: { type: "date", defaultValue: null, validation: {<% if (_planStartDate != "null")
                            { %> min: _planStartDate, <% }
                            else if (_planFinishDate != "null")
                            { %> max: _planFinishDate <% } %> } } ,
                        isActive: { type: "boolean" },
                        createdDate: { type: "date" },
                        createdBy: { type: "string" },
                        updatedDate: { type: "date" },
                        updatedBy: { type: "string" },
                    },
                    positionName: function () {
                        //change method
                        //return common_getPositionByPositionID(this.employeePosition);
                        //return common_getPositionName(this.employeePosition);
                        if (<%: this.ID %>_dsLookPositionDesigner.data().length == 0) <%: this.ID %>_dsLookPositionDesigner.read();
                        return <%: this.ID %>_dsLookPositionDesigner.get(this.employeePosition) ? <%: this.ID %>_dsLookPositionDesigner.get(this.employeePosition).text : "-"

                    }
                },
                parse: function (d) {
                    if (d == "Employee ID Exist") {
                        <%: this.ID %>_designerSource.read();
                        alert("Process failed: " + d);
                        return 0;
                    } else {
                        return d;
                        //                        < %: this.ID %>_designerSource.read();
                    }
                },
            },
            pageSize: 10,
            change: <%: this.ID %>_change,
        });

        var <%: this.ID %>_viewModel = new kendo.observable({
            EngineerSource: <%: this.ID %>_engineerSource,
            DesignerSource: <%: this.ID %>_designerSource,
            GetEmployee: <%: this.ID %>_getEmployee,
            saveEngineer: <%: this.ID %>_saveEngineer,
            saveDesigner: <%: this.ID %>_saveDesigner,
            editEngineer: <%: this.ID %>_editEngineer,
            editDesigner: <%: this.ID %>_editDesigner,
            employeeId: "",
            employeeName: "",
            employeeEmail: "",
            employeePosition: ""
        });

        function <%: this.ID %>_editEngineer(e) {
            if (e.model.id > 0)
                $(".k-grid-update").html("<span class='k-icon k-i-update'></span>Update");
        }


        function <%: this.ID %>_editDesigner(e) {
            if (e.model.id > 0)
                $(".k-grid-update").html("<span class='k-icon k-i-update'></span>Update");
        }

        function <%: this.ID %>_saveEngineer(e) {
//            console.log("asdfasdf");
            if (e.model.employeeId == "") {
                _showDialogMessage("Required", "Name is empty", "");
                e.preventDefault();
            } else
            if (e.model.employeePosition == "") {
                _showDialogMessage("Required", "Position is empty", "");
                e.preventDefault();
            } else
            if (e.model.joinedDate == null) {
                _showDialogMessage("Required", "Join Date is empty", "");
                e.preventDefault();
            }
        }
        function <%: this.ID %>_saveDesigner(e) {
            if (e.model.employeeId == "") {
                _showDialogMessage("Required", "Name is empty", "");
                e.preventDefault();
            } else
            if (e.model.employeePosition == "") {
                _showDialogMessage("Required", "Position is empty", "");
                e.preventDefault();
            } else
            if (e.model.joinedDate == null) {
                _showDialogMessage("Required", "Join Date is empty", "");
                e.preventDefault();
            }
        }

        function <%: this.ID %>_change(e) {
            switch (e.action) {
                case "add":
                    e.items[0].isActive = true;
                    break;
                case "edit":
                    e.items[0].isActive = true;
                    break;
                default:
            }
        }

        var <%: this.ID %>_dsLookPositionEngineer = new kendo.data.DataSource({
            transport: {
                read: {
                    url:_webApiUrl + "lookup/findbytype/positionEngineer",
                    async: false
                }
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        });


        function <%: this.ID %>_positionDropDown(container, options) {
            console.log(options.field)
            $('<input data-bind="value:' + options.field + '"/>')
                     .appendTo(container)
                     .kendoDropDownList({
                         optionLabel: "Choose Position",
                         valuePrimitive: true,
                         dataSource: <%: this.ID %>_dsLookPositionEngineer,
                         dataTextField: "text",
                         dataValueField: "value"
                     });
        }

        var <%: this.ID %>_dsLookPositionDesigner = new kendo.data.DataSource({
            transport: {
                read: {
                    url: _webApiUrl + "lookup/findbytype/positiondesigner",
                    async: false
                }
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        });

        function <%: this.ID %>_positionDropDownDesigner(container, options) {
            console.log("test")
            $('<input data-text-field="text" data-value-field="value" data-bind="value:' + options.field + '"/>')
                     .appendTo(container)
                     .kendoDropDownList({
                         valuePrimitive: true,
                         dataSource: <%: this.ID %>_dsLookPositionDesigner,
                         dataTextField: "text",
                         dataValueField: "value"

                     });
        }

        function <%: this.ID %>_getEmployee(container, options) {
            var input, btn;
            if (options.model.id == 0) {
                input = $("<input class=' k-textbox' type='text' style='width:150px; display: inline-block' data-bind='value: employeeName' disabled/>");
                input.attr("id", "<%: this.ID %>_txt");
                input.appendTo(container);
                btn = $("<input type='button' class='k-button' style='min-width:30px; display: inline-block' value='...'/>");
                btn.attr("id", "<%: this.ID %>_btn");
                btn.appendTo(container);
                //            var pickedEmp = (this.dataItem($(container.currentTarget).closest("tr")));
            } else {
                input = $("<div type='text' style='width:150px' disabled/>");
                input.attr("id", "<%: this.ID %>_txt");
                input.appendTo(container);
                $("#<%: this.ID %>_txt").html(options.model.employeeName);
            }
            $("#<%: this.ID %>_btn").bind("click", function () {
                PickEmployeesControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                    var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                    options.model.employeeId = pickedEmployee.id;
                    options.model.employeeName = pickedEmployee.full_Name;
                    options.model.employeeEmail = pickedEmployee.email;

                    //options.model.employeePosition = pickedEmployee.positionId;

                    <%: this.ID %>_viewModel.employeeId = pickedEmployee.id;
                    <%: this.ID %>_viewModel.employeeName = pickedEmployee.full_Name;
                    <%: this.ID %>_viewModel.employeeEmail = pickedEmployee.email;
                    //<%: this.ID %>_viewModel.employeePosition = pickedEmployee.positionId;

                    if (PickEmployeesControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                        $.ajax({
                            url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                            type: "GET",
                            async: false,
                            success: function (result) {
                                if (result.length != 0) {
                                    <%: this.ID %>_viewModel.employeeName = result[0].FULL_NAME;
                                    <%: this.ID %>_viewModel.employeeEmail = result[0].EMAIL;
                                }
                            },
                            error: function (data) {
                            }
                        });

                        if (!<%: this.ID %>_viewModel.employeeName || !<%: this.ID %>_viewModel.employeeEmail) {
                            var badgeNoContractor = pickedEmployee.id;
                            $.ajax({
                                url: _webApiUrl + "user/userNonEmployeeByUserName/" + pickedEmployee.id + "",
                                type: "GET",
                                async: false,
                                success: function (result) {
                                    if (result.length != 0) {
                                        badgeNoContractor = result[0].userName;
                                    }
                                },
                                error: function (data) {
                                }
                            });

                            $.ajax({
                                url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_PARAM|@param=" + badgeNoContractor + "",
                                type: "GET",
                                async: false,
                                success: function (result) {
                                    if (result.length != 0) {
                                        <%: this.ID %>_viewModel.employeeName = result[0].FULL_NAME;
                                        <%: this.ID %>_viewModel.employeeEmail = result[0].EMAIL;
                                    }
                                },
                                error: function (data) {
                                }
                            });
                        }
                    }

                    $("#<%: this.ID %>_txt").val(pickedEmployee.full_Name);
                    //container.closest("td").next().html(common_getPositionByPositionID(pickedEmployee.positionId));
                    //                    viewModel.requestData.set("operationRepId", pickedEmployee.employeeId);
                    //                    viewModel.requestData.set("operationRepName", pickedEmployee.full_Name);
                    //                    viewModel.requestData.set("operationRepEmail", pickedEmployee.email);
                    popUpEmployee.close();
                });
                PickEmployeesControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                    popUpEmployee.close();
                });
                // show popup
                PickEmployeesControl_DataBind();

                var popUpEmployee = $("#popUpEmployee").getKendoWindow();
                popUpEmployee.center().open();
                PickEmployeesControl_pickEmployeeViewModel.set("employeeBadgeNo", "");
                $("#PickEmployeesControl_EmployeeGrid").getKendoGrid().dataSource.data([]);
            })


        }

        
        OnDocumentReady = function () {
            kendo.bind($("#<%: this.ID %>_FormContent"), <%: this.ID %>_viewModel);
        }

    </script>
