﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportBudgetPerformance.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ReportBudgetPerformance" %>
<script>
    var <%: this.ID %>_chartO;
    var <%: this.ID %>_chartC;
    var <%: this.ID %>_reportBudgetPerformanceModel = kendo.observable({
        titleChart: "Report Budget Performance by Area",
        valueYear: new Date().getFullYear(),
        valueYearFrom: new Date(),
        valueYearTo: new Date(),
        dsBudgetPerformanceC: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        area: {type: "string"},
                        totalProject: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var year = <%: this.ID %>_reportBudgetPerformanceModel.valueYear ? <%: this.ID %>_reportBudgetPerformanceModel.valueYear : 0;
                        var yearFrom = kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy");
                        var yearTo = kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy");
                        return _webApiUrl + "project/reportBudgetPerformancebyAreaC/1?yearFrom=" + yearFrom + "&yearTo=" + yearTo;
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "area", dir: "asc" }]
        }),
        dsBudgetPerformanceO: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        area: {type: "string"},
                        totalProject: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        var year = <%: this.ID %>_reportBudgetPerformanceModel.valueYear ? <%: this.ID %>_reportBudgetPerformanceModel.valueYear : 0;
                        var yearFrom = kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy");
                        var yearTo = kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy");
                        return _webApiUrl + "project/reportBudgetPerformancebyAreaO/1?yearFrom=" + yearFrom + "&yearTo=" + yearTo;
                    },
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "area", dir: "asc" }]
        }),
        onChange: function () {
<%--            <%: this.ID %>_reportBudgetPerformanceModel.valueYear = this.valueYear;
            <%: this.ID %>_reportBudgetPerformanceModel.dsBudgetPerformance.read();
            $("#<%: this.ID %>_budgetPerformance").data("kendoChart").options.title.text =
                <%: this.ID %>_reportBudgetPerformanceModel.titleChart + " "
                +kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy");
            $("#<%: this.ID %>_budgetPerformance").data("kendoChart").refresh();--%>
        },
        onSearchClick: function () {
            <%: this.ID %>_reportBudgetPerformanceModel.valueYear = this.valueYear;
            <%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom = this.valueYearFrom;
            <%: this.ID %>_reportBudgetPerformanceModel.valueYearTo = this.valueYearTo;
            <%: this.ID %>_reportBudgetPerformanceModel.dsBudgetPerformanceO.read();
            <%: this.ID %>_reportBudgetPerformanceModel.dsBudgetPerformanceC.read();
            $("#<%: this.ID %>_budgetPerformanceO").data("kendoChart").options.title.text =
                <%: this.ID %>_reportBudgetPerformanceModel.titleChart + " & Project Type is Operating" + "\n"
                +kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy");
            $("#<%: this.ID %>_budgetPerformanceO").data("kendoChart").refresh();
            $("#<%: this.ID %>_budgetPerformanceC").data("kendoChart").options.title.text =
                <%: this.ID %>_reportBudgetPerformanceModel.titleChart + " & Project Type is Capital" + "\n" 
                +kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy");
            $("#<%: this.ID %>_budgetPerformanceC").data("kendoChart").refresh();
        },
        dsYearProject: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_reportBudgetPerformance"), false);
            },
            schema: {
                model: {
                    id: "year",
                    fields: {
                        year: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: function (data) {
                        return _webApiUrl + "dashboard/getMinimunYearOnProject/1";
                    },
                    dataType: "json"
                }
            },
            //pageSize: 10,
            sort: [{ field: "year", dir: "desc" }]
        }),
    });


    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_reportBudgetPerformance"), <%: this.ID %>_reportBudgetPerformanceModel);
        $("#<%: this.ID %>_budgetPerformanceO").kendoChart({
            title: {
                text: <%: this.ID %>_reportBudgetPerformanceModel.titleChart + "\n"
                + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy")
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_reportBudgetPerformanceModel.dsBudgetPerformanceO,
            transitions: false,
            seriesColors: ["#943432", "#6D8B2E"],
            series: [{
                name: "Estimated Cost (USD)",
                type: "column",
                field: "estCost",
            }, {
                name: "Total Project Cost (USD)",
                type: "column",
                field: "actCost",
            }
            ],
            valueAxis: {
                labels: {
                    template: "#= kendo.format('{0:N0}', value) #",
                    rotation: -45
                }
            },
            categoryAxis: {
                field: "area"
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= kendo.format('{0:N0}', value) #"
            }
        });
        $("#<%: this.ID %>_budgetPerformanceC").kendoChart({
            title: {
                text: <%: this.ID %>_reportBudgetPerformanceModel.titleChart + "\n"
                + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearFrom, "dd MMM yyyy") + " - " + kendo.toString(<%: this.ID %>_reportBudgetPerformanceModel.valueYearTo, "dd MMM yyyy")
            },
            legend: {
                position: "bottom"
            },
            dataSource: <%: this.ID %>_reportBudgetPerformanceModel.dsBudgetPerformanceC,
            transitions: false,
            seriesColors: ["#943432", "#6D8B2E"],
            series: [{
                name: "Estimated Cost (USD)",
                type: "column",
                field: "estCost",
            }, {
                name: "Total Project Cost (USD)",
                type: "column",
                field: "actCost",
            }
            ],
            valueAxis: {
                labels: {
                    template: "#= kendo.format('{0:N0}', value) #",
                    rotation: -45
                }
            },
            categoryAxis: {
                field: "area"
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= kendo.format('{0:N0}', value) #"
            }
        });
        <%: this.ID %>_chartO = $("#<%: this.ID %>_budgetPerformanceO").data("kendoChart");
        <%: this.ID %>_chartC = $("#<%: this.ID %>_budgetPerformanceC").data("kendoChart");
    });
</script>

<div id="<%: this.ID %>_reportBudgetPerformance">
    Budget Performance Year: 
<%--        <input data-role="combobox"
            data-placeholder="Select Year"                            
            data-text-field="year"
            data-value-field="year"
            data-bind="value: valueYear, source: dsYearProject,
                        events: {
                            change: onChange
                        }"
            style="width: 250px" />--%>
                <input data-role="datepicker"
                data-placeholder="From"                            
                data-bind="value: valueYearFrom"
                    data-format="dd MMMM yyyy" 
                style="width: 250px" />
                - <input data-role="datepicker"
                data-placeholder="To"                            
                data-bind="value: valueYearTo"
                    data-format="dd MMMM yyyy" 
                style="width: 250px" />
            <div
                data-role="button"
                data-bind="events: { click: onSearchClick }"
                class="k-button k-primary">
                <span class="k-icon k-i-filter"></span>
                Search
            </div>


    <div id="<%: this.ID %>_budgetPerformanceO" style="width: 1000px; height: 400px;"></div>
    <div id="<%: this.ID %>_budgetPerformanceC" style="width: 1000px; height: 400px;"></div>
</div>
