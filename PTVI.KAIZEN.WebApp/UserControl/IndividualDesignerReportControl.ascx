﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndividualDesignerReportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.IndividualDesignerReportControl" %>

<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
    var <%: this.ID %>_IndividualReportControlGrid = [], <%: this.ID %>_actualCostFormatData = [], currYear = new Date().getFullYear();
    var <%: this.ID %>_viewIndividualReportModel = kendo.observable({
        paramYear: currYear,
        paramBadge: "",
        isInvisible: true,
        listYear: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_IndividualEngineerGrid"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_IndividualEngineerGrid"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "Report/listYearDocument/1";
                    },
                    async: false
                }
            }
        }),
        listEmployee: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_IndividualEngineerGrid"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_IndividualEngineerGrid"), false);
            },
            transport: {
                read: {
                    url: function() {
                        return _webApiUrl + "MasterYearlyEngineeringHours/getAllEmployeeMasterYearlyEngineeringHours/1";
                    },
                    async: false
                }
            },
            sort: [{ field: "name", dir: "asc" }]
        }),
        onChangeParam: function (e) {
            if (e.sender.element[0].id == "ddlName") {
                var graphEWP = $("#graphEWP").getKendoChart();
                var graphDC = $("#graphDC").getKendoChart();
                var ddlText = e.sender.text();
                graphEWP.options.title = "EWP Compliance - " + ddlText;
                graphDC.options.title = "Design Conformity - " + ddlText;
            }
            this.dsIndividualReport.read();
            this.set("isInvisible", this.paramBadge ? false : true);
        },
        dsIndividualReport: new kendo.data.DataSource({
            requestStart: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), true);
                kendo.ui.progress($("#<%: this.ID %>_IndividualEngineerGrid"), true);
            },
            requestEnd: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), false);
                kendo.ui.progress($("#<%: this.ID %>_IndividualEngineerGrid"), false);
            },
            schema: {
                model: {
                    id: "monthInt",
                    fields: {
                        monthInt: { type: 'number' },
                        countPlanEWP: { type: 'number' },
                        countActualEWP: { type: 'number' },
                        deliverableWeightHour: { type: 'number' },
                        deliverableWeightPercent: { type: 'number' },
                        planScoreEWP: { type: 'number' },
                        planColumnParameterEWP: { type: 'number' },
                        planScoreCummEWP: { type: 'number' },
                        actualAchievementEWP: { type: 'number' },
                        actualScoreCummEWP: { type: 'number' },
                        countPlanConformity: { type: 'number' },
                        countActualConformity: { type: 'number' },
                        planScoreConformity: { type: 'number' },
                        planColumnParameterConformity: { type: 'number' },
                        planScoreCummConformity: { type: 'number' },
                        actualAchievementConformity: { type: 'number' },
                        actualScoreCummConformity: { type: 'number' },
                        summaryPlanScoreCumm: { type: 'number' },
                        summaryPlanActualCumm: { type: 'number' }

                    }
                }
            },
            transport: {
                read: {
                    url: function () {
                        return _webApiUrl + "Report/IndividualDesignerReportForm?paramYearDesigner=" + <%: this.ID %>_viewIndividualReportModel.paramYear + "&badgeNo=" + <%: this.ID %>_viewIndividualReportModel.paramBadge;
                    },
                    async: false
                }
            },
            //group: [{ field: "category" }],
            aggregate: [
                { field: "countPlanEWP", aggregate: "sum" },
                { field: "countActualEWP", aggregate: "sum" },
                { field: "planScoreEWP", aggregate: "sum" },
                { field: "actualScoreCummEWP", aggregate: "max" },
                { field: "countPlanConformity", aggregate: "sum" },
                { field: "countActualConformity", aggregate: "sum" },
                { field: "planScoreConformity", aggregate: "sum" },
                { field: "actualScoreCummConformity", aggregate: "max" },
                { field: "summaryPlanActualCumm", aggregate: "max" },
                { field: "summaryActuaScoreCumm", aggregate: "max" }]
        })
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_IndividualReportControl"), <%: this.ID %>_viewIndividualReportModel);
        //$("#<%: this.ID %>_gridReport").data("kendoGrid").resize();
    });

</script>
<div id="<%: this.ID %>_IndividualReportControl">
     <div style="float:right">
         <span>Year: </span>
         <input data-role="dropdownlist"
            data-placeholder="Select Deliverable"
            data-value-primitive="true"
            data-text-field="text"
            data-value-field="value"
            data-bind="source: listYear, value: paramYear, events: { change: onChangeParam }"
            style="width: 100px" />
         </div>
     <div>
         <span>Name: </span>
         <input id="ddlName" data-role="dropdownlist"
                data-placeholder="Select Deliverable"
                data-value-primitive="true"
                data-text-field="name"
                data-value-field="badge"
                data-bind="source: listEmployee, value: paramBadge, events: { change: onChangeParam } "
                style="width: 350px" />
         </div>
    <br />
    <div id="<%: this.ID %>_IndividualEngineerGrid" class="gridReport" style="height: 400px;" 
        data-role="grid" 
        data-bind="source: dsIndividualReport" <%--, invisible: isInvisible--%>
        <%--data-scrollable="false"--%>
        data-no-records="true"
        data-filterable= "false"
        data-columns="[
            {
                field: 'monthName', title: 'Month', width: 100, locked: true, lockable: false,
                headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, attributes: {class: 'columnReport1'},
                footerTemplate: 'Total'
            },
            { 
                title: 'EWP Compliance', headerAttributes: {style: 'text-align: center'}, columns: [
                { 
                    field: 'countPlanEWP', title: 'Plan', width: 75, 
                    headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'}, 
                    aggregates: ['sum'], footerTemplate: '#: data.countPlanEWP ? countPlanEWP.sum : 0 #'
                },
                { 
                    field: 'countActualEWP', title: 'Completed', width: 75, 
                    headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                    aggregates: ['sum'], footerTemplate: '#: data.countActualEWP ? countActualEWP.sum : 0 #'
                },
                { 
                    title: 'Plan Score', headerAttributes: {style: 'text-align: center'}, columns: [
                    { 
                        field: 'planScoreEWP', title: '% Score', width: 75, 
                        template: '#:kendo.toString(data.planScoreEWP ? data.planScoreEWP : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                        aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.planScoreEWP ? planScoreEWP.sum : 0, \'n2\') #'
                    },
                    { 
                        field: 'planColumnParameterEWP', title: '125', width: 75, 
                        template: '#:kendo.toString(data.planColumnParameterEWP ? data.planColumnParameterEWP : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    },
                    { 
                        field: 'planScoreCummEWP', title: 'Cumm', width: 75, 
                        template: '#:kendo.toString(data.planScoreCummEWP ? data.planScoreCummEWP : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    }
                    ]
                },
                { 
                    title: 'Actual Score', headerAttributes: {style: 'text-align: center'}, columns: [
                    { 
                        field: 'actualAchievementEWP', title: 'Score', width: 75, 
                        template: '#:kendo.toString(data.actualAchievementEWP ? data.actualAchievementEWP : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    },
                    { 
                        field: 'actualScoreCummEWP', title: 'Cumm', width: 75, 
                        template: '#:kendo.toString(data.actualScoreCummEWP ? data.actualScoreCummEWP : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                        aggregates: ['max'], footerTemplate: '#: kendo.toString(data.actualScoreCummEWP ? actualScoreCummEWP.max : 0, \'n2\') #'
                    }
                    ]
                }]
            },
            { 
                title: 'Design Conformity', headerAttributes: {style: 'text-align: center'}, columns: [
                { 
                    field: 'countPlanConformity', title: 'Plan', width: 75, 
                    headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                    aggregates: ['sum'], footerTemplate: '#: data.countPlanConformity ? countPlanConformity.sum : 0 #'
                },
                { 
                    field: 'countActualConformity', title: 'Completed', width: 75, 
                    headerAttributes: {style: 'text-align: center; vertical-align:middle;'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                    aggregates: ['sum'], footerTemplate: '#: data.countActualConformity ? countActualConformity.sum : 0 #'
                } ,
                { 
                    title: 'Plan Score', headerAttributes: {style: 'text-align: center'}, columns: [
                    { 
                        field: 'planScoreConformity', title: '% Score  ', width: 75, 
                        template: '#:kendo.toString(data.planScoreConformity ? data.planScoreConformity : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                        aggregates: ['sum'], footerTemplate: '#: kendo.toString(data.planScoreConformity ? planScoreConformity.sum : 0, \'n2\') #'
                    },
                    { 
                        field: 'planColumnParameterConformity', title: '125', width: 75, 
                        template: '#:kendo.toString(data.planColumnParameterConformity ? data.planColumnParameterConformity : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    },
                    { 
                        field: 'planScoreCummConformity', title: 'Cumm', width: 75, 
                        template: '#:kendo.toString(data.planScoreCummConformity ? data.planScoreCummConformity : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    }
                    ]
                },
                { 
                    title: 'Actual Score', headerAttributes: {style: 'text-align: center'}, columns: [
                    { 
                        field: 'actualAchievementConformity', title: 'Score', width: 75, 
                        template: '#:kendo.toString(data.actualAchievementConformity ? data.actualAchievementConformity : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    },
                    { 
                        field: 'actualScoreCummEWP', title: 'Cumm', width: 75, 
                        template: '#:kendo.toString(data.actualScoreCummConformity ? data.actualScoreCummConformity : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                        aggregates: ['max'], footerTemplate: '#: kendo.toString(data.actualScoreCummEWP ? actualScoreCummEWP.max : 0, \'n2\') #'
                    }
                    ]
                }]
            },
            { 
                title: 'Summary', headerAttributes: {style: 'text-align: center'}, columns: [
                { 
                    title: 'Plan Score', headerAttributes: {style: 'text-align: center'}, columns: [
                    { 
                        field: 'summaryPlanScoreCumm', title: 'Cumm', width: 75, 
                        template: '#:kendo.toString(data.summaryPlanScoreCumm ? data.summaryPlanScoreCumm : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'} 
                    }
                    ]
                },
                { 
                    title: 'Actual Score', headerAttributes: {style: 'text-align: center'}, columns: [
                    { 
                        field: 'summaryActuaScoreCumm', title: 'Cumm', width: 75, 
                        template: '#:kendo.toString(data.summaryPlanActualCumm ? data.planScoreCummEWP : 0, \'n2\')#', 
                        headerAttributes: {style: 'text-align: center'}, footerAttributes: {style: 'text-align: center'}, attributes: {class: 'columnReport2'},
                        aggregates: ['max'], footerTemplate: '#: kendo.toString(data.summaryActuaScoreCumm ? summaryActuaScoreCumm.max : 0, \'n2\') #'
                    }
                    ]
                }]
            }
            ]">
    </div>
    <div id="graphEWP" style="width: 49%; display: inline-block"
        data-role="chart"
        data-title ="EWP Compliance - "
        data-bind="source: dsIndividualReport"
        data-chart-area="{background: 'lightgray'}"
        data-legend="{ position: 'bottom' }"
        data-tooltip= "{ visible: true, template: '#= kendo.toString(data.value, \'n2\') #' }" 
        data-series="[  {name:'Plan', field: 'countPlanEWP', colorField: 'color', labels:{visible: false}}, { name:'Actual', field: 'countActualEWP', colorField: 'color', labels:{visible: false}},
        {type:'line', field: 'planScoreCummEWP', colorField: 'color', labels:{visible: false}}, {type:'line', field: 'actualScoreCummEWP', colorField: 'color', labels:{visible: false}},
                        ]",
        data-category-axis="{field: 'monthName', labels:{rotation: 270 }}"
     >
    </div> 
    <div id="graphDC" style="width: 49%; display: inline-block"
        data-role="chart"
        data-title ="Design Conformity - "
        data-bind="source: dsIndividualReport"
        data-legend="{ position: 'bottom' }"
        data-chart-area="{background: 'lightgray'}"
        data-tooltip= "{ visible: true, template: '#= kendo.toString(data.value, \'n2\') #' }" 
        data-series="[  {name:'Plan', field: 'countPlanConformity', colorField: 'color', labels:{visible: false}}, {name:'Actual', field: 'countActualConformity', colorField: 'color', labels:{visible: false}},
        {type:'line', field: 'planScoreCummConformity', colorField: 'color', labels:{visible: false}}, {type:'line', field: 'actualScoreCummConformity', colorField: 'color', labels:{visible: false}},
                        ]",
        data-category-axis="{field: 'monthName', labels:{rotation: 270 }}"
     >
    </div>
</div>
<style>
    .gridReport{
        display: inline-block;
        font-size: 11px;
        line-height: normal;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        margin: 3px;
    }
    .columnReport1 {
        text-align: right;
        vertical-align:top !important;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        font-size: 11px !important;
    }
    .columnReport2 {
        text-align: center;
        vertical-align:top !important;
        font-family: robotoregular,'Myriad Pro','Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif !important;
        font-size: 11px !important;
    }
</style>
