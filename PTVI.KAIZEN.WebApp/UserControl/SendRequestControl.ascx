﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendRequestControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.SendRequest" %>
    <style type="text/css">
        .auto-style1 {
            width: 707px;
        }
    </style>
    <div id="<%: this.ID %>_ContentForm" style="border: 1px solid #c5c5c5; border-radius: 5px; -moz-border-radius: 5px; -khtml-border-radius: 5px; -webkit-border-radius: 5px; padding: 10px">
    <div>
        <div id="btnSend" class="k-button" data-bind="events: { click: sendMail }">Send</div><div id="btnClose" class="k-button" data-bind="    events: { click: closeMail }" }>Close</div>
    </div><br/>
    <div>
        <div>
            <div style="width: 70px; text-align: right;display:inline-block;">To:</div>
            <div style="width: 700px; text-align: left;display:inline-block; padding-left:10px; padding-top: 5px;">
                <select id= "<%: this.ID %>_MsTo" data-role="multiselect"
                    data-placeholder="Employee Name"
                    data-text-field="full_Name"
                    data-value-field="email"
                    data-bind="source: dsEmployeeOdata"
                ></select>
            </div>
                <%--<div><div style="width: 70px; text-align: right;display:inline-block;">From:</div><div id="mailFromText" style="width: 400px; text-align: left;display:inline-block; padding-left:10px; padding-top: 5px;">  </div></div>--%>
        <div>
            <div>
                <div style="width: 70px; text-align: right;display:inline-block;">Cc:</div>
                <div style="width: 700px; text-align: left;display:inline-block; padding-left:10px; padding-top: 5px;">
                    <select id="<%: this.ID %>_MsCc" data-role="multiselect"
                    data-placeholder="Employee Name"
                    data-text-field="full_Name"
                    data-value-field="email"
                    data-bind="source: dsEmployeeOdata"
                    ></select>
                </div>
                <div><div style="width: 70px; text-align: right;display:inline-block;">Subject:</div>
                    <div style="width: 500px; text-align: left;display:inline-block; padding-left:10px; padding-top: 5px;">
                    <input id="subject" style="width: 100%;" required="required" class="k-textbox"  data-bind="value: Subject"/>
                    </div>
                </div><br/>
            </div>
            <div>
                <div id="dtResponseWrapper" style="width: 400px; display: inline-block;">Response Required By: 
                    <input required="required" id="dtResponse" data-role="datepicker" data-bind="value: respRequired" data-format="dd MMM yyyy"/>
                </div>
                <div id="priorityWrapper" style="width: 400px; display: inline-block;">Priority: 
                    <input id="priority" required="required" id="ddlPriority" data-role="dropdownlist" 
                    data-bind="source: ddlPrioritySource, value: ddlPriority"
                    data-text-field= "text"
                    data-value-field= "value"
                    data-option-label= "Select Priority"
                    data-value-primitive= "true"
                    name="Priority"
                    >
                </div>
            </div><br/>
            <div id="emailEditorWrapper">
                <textarea required="required"
                    id="emailEditor" 
                    data-bind="value: emailEditor" 
                    data-role="editor"
                    data-tools="['bold','italic','underline','strikethrough','justifyLeft','justifyCenter','justifyRight','justifyFull',
                    'insertUnorderedList','insertOrderedList','indent','outdent','createLink','unlink','insertImage','insertFile',
                    'subscript','superscript','createTable','addRowAbove','addRowBelow','addColumnLeft','addColumnRight','deleteRow',
                    'deleteColumn','viewHtml','formatting','cleanFormatting','fontName','fontSize','foreColor','backColor','print']"
                    ></textarea>
                </div>
            </div>
        </div>
    </div>
 </div>
<script>
    function clickCloseEmail(e) {
        if (confirm("Do you want to Cancel this request?")) {
            <%: this.ID %>_dsSendReqToUser = {};
            <%: this.ID %>_viewModel.set("employeeOtherPositionSelected", null);


            <%: this.ID %>_viewModel.set("mailToId", "");
            <%: this.ID %>_viewModel.set("mailToName", "");
            <%: this.ID %>_viewModel.set("mailTo", "");
           
            <%--            <%: this.ID %>_viewModel.set("mailFromId", "");
            <%: this.ID %>_viewModel.set("mailFromName", "");
            <%: this.ID %>_viewModel.set("mailFrom", "");--%>
            <%: this.ID %>_viewModel.set("mailCcId", "");
            <%: this.ID %>_viewModel.set("mailCcName", "");
            <%: this.ID %>_viewModel.set("mailCc", "");
            
            <%: this.ID %>_viewModel.set("Subject", "");
            <%: this.ID %>_viewModel.set("emailEditor", "");
            <%: this.ID %>_viewModel.set("respRequired", "");
            <%: this.ID %>_viewModel.set("ddlPriority", "");
            $("#<%: this.ID %>_employeeOtherPositionTmp").val("");
        }
    }

    function clickSendEmail(e) {
        $("font[name='vText']").remove();
        if ($("#iPROMform")[0].checkValidity()) {
            if (confirm("Do you want to Send this request?")) {
                <%: this.ID %>_dsSendReqToUser.add({
                    id: 0,
                    mailToId: $.map($("#<%: this.ID %>_MsTo").data("kendoMultiSelect").dataItems(), function (val, i) { return val.employeeId; }).join(","),
                    mailToName: $.map($("#<%: this.ID %>_MsTo").data("kendoMultiSelect").dataItems(), function (val, i) { return val.full_Name; }).join(","),
                    mailTo:  $("#<%: this.ID %>_MsTo").data("kendoMultiSelect").value().join(","),
                    mailFromId: <%: this.ID %>_viewModel.mailFromId,
                    mailFromName: <%: this.ID %>_viewModel.mailFromName,
                    mailFrom: <%: this.ID %>_viewModel.mailFrom,
                    mailCcId: $.map($("#<%: this.ID %>_MsCc").data("kendoMultiSelect").dataItems(), function (val, i) { return val.employeeId; }).join(","),
                    mailCcName: $.map($("#<%: this.ID %>_MsCc").data("kendoMultiSelect").dataItems(), function (val, i) { return val.full_Name; }).join(","),
                    mailCc:$("#<%: this.ID %>_MsCc").data("kendoMultiSelect").value(),
                    mailSubject: <%: this.ID %>_viewModel.Subject,
                    mailBody: <%: this.ID %>_viewModel.emailEditor,
                    responseRequiredBy: kendo.toString(<%: this.ID %>_viewModel.respRequired, 'dd MMM yyyy'),
                    priority: <%: this.ID %>_viewModel.ddlPriority,
                    createdBy: _currNTUserID
                });
                <%: this.ID %>_dsSendReqToUser.sync();
                //                $("#iPROMform")[0].submit();
                <%: this.ID %>_dsSendReqToUser.remove(<%: this.ID %>_dsSendReqToUser.at(0));
                //                        < %: this.ID %>_viewModel.set("mailFrom", "");
                <%: this.ID %>_viewModel.set("employeeOtherPositionSelected", null);
                <%: this.ID %>_viewModel.set("Subject", "");
                <%: this.ID %>_viewModel.set("emailEditor", "");
                <%: this.ID %>_viewModel.set("respRequired", "");
                <%: this.ID %>_viewModel.set("ddlPriority", "");
                 var multiSelect = $('#<%: this.ID %>_MsTo').data("kendoMultiSelect");
                multiSelect.value([]);
                <%: this.ID %>_viewModel.set("mailToName", "");
                <%: this.ID %>_viewModel.set("mailTo", "");
                var multiSelect = $('#<%: this.ID %>_MsCc').data("kendoMultiSelect");
                multiSelect.value([]);
                <%: this.ID %>_viewModel.set("mailCcName", "");
                <%: this.ID %>_viewModel.set("mailCc", "");
<%--                <%: this.ID %>_viewModel.set("mailFromId", "");
                <%: this.ID %>_viewModel.set("mailFromName", "");
                <%: this.ID %>_viewModel.set("mailFrom", "");--%>
                $("#<%: this.ID %>_employeeOtherPositionTmp").val("");
            }
        } else {
            if (!$("#mailto")[0].checkValidity()) $("#mailto").after("<font name='vText' style='color: red; padding-left: 10px'>Required...</label>");
            if (!$("#mailCc")[0].checkValidity()) $("#mailCc").after("<font name='vText' style='color: red; padding-left: 10px'>Required...</label>");
            if (!$("#subject")[0].checkValidity()) $("#subject").after("<font name='vText' style='color: red; padding-left: 10px'>Required...</label>");
            if (!$("#dtResponse")[0].checkValidity()) $("#dtResponseWrapper").append("<font name='vText' style='color: red; padding-left: 10px; padding-top: 5px'>Required...</label>");
            if (!$("#priority")[0].checkValidity()) $("#priorityWrapper").append("<font name='vText' style='color: red; padding-left: 10px; padding-top: 5px'>Required...</label>");
            if (!$("#emailEditor")[0].checkValidity()) $("#emailEditorWrapper").before("<font name='vText' style='color: red; padding-left: 10px'>Required...</label>");

            //alert("Please fill requiered fields");
        }
    }



    var <%: this.ID %>_dsSendReqToUser = new kendo.data.DataSource({
        requestStart: function () {
            kendo.ui.progress($("#<%: this.ID %>_ContentForm"), true);
        },
        requestEnd: function () {
            kendo.ui.progress($("#<%: this.ID %>_ContentForm"), false);
            alert("Email Sent!");
        },
        transport: {
            create: { url: _webApiUrl + "RequestToTheUsers/SendRequest/1", dataType: "json", type: "POST" },
            parameterMap: function (options, operation) {
                if (operation == "create") {
                    options.projectId = _projectId;
                    return options;
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    projectId: { type: "number" },
                    requestNo: { type: "string" },
                    requestDate: { type: "date" },
                    mailToId: { type: "string" },
                    mailToName: { type: "string" },
                    mailTo: { type: "string" },
                    mailFromId: { type: "string" },
                    mailFromName: { type: "string" },
                    mailFrom: { type: "string" },
                    mailCcId: { type: "string" },
                    mailCcName: { type: "string" },
                    mailCc: { type: "string" },
                    mailSubject: { type: "string" },
                    mailBody: { type: "string" },
                    responseRequiredBy: { type: "datetime" },
                    priority: { type: "string" },
                    status: { type: "string" },
                    createdBy: { type: "string" },
                    updatedby: { type: "string" }
                }
            }
        }
    })
    var ds = new kendo.data.DataSource({
        transport: {
            read: { url: _webApiUrl + "lookup/findbytype/requestListPriority", dataType: "json", type: "GET" },
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: { type: "number" },
                    text: { type: "string" },
                    value: { type: "string" },
                }
            }
        }
    })
    
    var <%: this.ID %>_viewModel = new kendo.observable({
        ddlPrioritySource: ds,
        ddlPriority: "",
        Subject: "",
        mailToId: "",
        mailToName: "",
        mailTo: "",
        mailCcId: "",
        mailCcName: "",
        mailCc: "",
        mailFromId: _currBadgeNo,
        mailFrom: _parentOthMailFrom,
        mailFromName: _parentOthMailFromName,
        respRequired: "",
        emailEditor: "",
        sendMail: clickSendEmail,
        closeMail: clickCloseEmail,
        employeeOtherPositionSelected: null,
        onChange: function (e) {
            if (this.employeeOtherPositionSelected) {
                if (!this.employeeOtherPositionSelected.employeeId) {
                    this.set("employeeOtherPositionSelected", null);
                } else {
                    this.set("mailToId", this.employeeOtherPositionSelected.employeeId);
                    this.set("mailToName", this.employeeOtherPositionSelected.full_Name);
                    this.set("mailTo", this.employeeOtherPositionSelected.email);
                }
            }
        },
        dsEmployeeOtherPosition: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                    options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        dsEmployeeOdata: new kendo.data.DataSource({
            serverFiltering: false,
            transport: {
                read: function (e) {
                     //on success
                    var result = [];
                    $.ajax({
                        url: _webOdataUrl + "EMPLOYEEs",
                        type: "GET",
                        async: true,
                        success: function (data) {
                            result = data.value;
                           e.success(result);
                        },
                        error: function (data) {
                            //return [];
                        }
                    });
                    
                    //
                    //);
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                    
                }
            },
        }),
        getEmployeeFromServer: function (options) {
            var searchBadgeNo = options.data.filter.filters[0].value;
            var result = [];
            if (searchBadgeNo.length >= 3) {
                //var filter = "?$filter=substringof('" + searchBadgeNo + "',employeeId) or substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var filter = "?$filter=substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name)) and email ne '' and email ne null";
                var result = [];
                $.ajax({
                    url: _webOdataUrl + "EMPLOYEEs" + filter,
                    type: "GET",
                    async: false,
                    success: function (data) {
                        result = data.value;
                    },
                    error: function (data) {
                        //return [];
                    }
                });
            }
            return result;
        }
    });


    $(document).ready(function () {

        $("#multiselect").kendoMultiSelect({
            placeholder: "employee Name",
            dataTextField: "full_Name",
            dataValueField: "email",
            autoBind: true,
            dataSource: {
                transport: {
                    read: {
                        dataType: "json",

                    }
                }
            }
        });

     $("#dtResponse").getKendoDatePicker().min(new Date());

        if (_planStartDate) {
            $("#dtResponse").getKendoDatePicker().min(_planStartDate);
        }
        if (_planFinishDate) {
            $("#dtResponse").getKendoDatePicker().max(_planFinishDate);
        }

        kendo.bind($("#<%: this.ID %>_ContentForm"), <%: this.ID %>_viewModel);
        $("#mailFromText").text(<%: this.ID %>_viewModel.mailFromName);
    });
</script>