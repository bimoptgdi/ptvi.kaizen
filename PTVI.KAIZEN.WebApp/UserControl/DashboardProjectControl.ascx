﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardProjectControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.DashboardProjectControl" %>

<style type="text/css">
    .auto-style1 {
        width: 14%;
        height: 215px;
    }
    .auto-style2 {
        width: 100%;
        height: 262px;
    }
    .auto-style3 {
        width: 40%;
        height: 236px;
    }
    .auto-style4 {
        width: 60%;
        height: 236px;
    }
    .auto-style5 {
        width: 100%;
        height: 358px;
    }
    .auto-style6 {
        width: 30%;
        height: 227px;
    }
    .auto-style9 {
        width: 95%;
    }
    .auto-style7 {
        width: 100%;
        height: 119px;
    }
    .auto-style16 {
        width: 23%;
        height: 215px;
    }
    .auto-style17 {
        height: 23px;
        width: 208px;
    }
    .auto-style19 {
        width: 272px;
    }
    .auto-style20 {
        height: 23px;
        width: 272px;
    }
    .auto-style21 {
        width: 57%;
        height: 82px;
        margin-right: 0px;
    }
    .auto-style24 {
        width: 208px;
    }
    .auto-style28 {
        width: 18px;
    }
    .auto-style29 {
        height: 23px;
        width: 18px;
    }
    </style>

<script>
    common_getAllStatusIprom();
    var aaa;
    
    var <%: this.ID %>_viewDashboardProjectModel = kendo.observable({
        getDsProjectDashboard: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#grdListMainProject"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#grdListMainProject"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        totalCost: { type: "number" },
                        actualCost: { type: "number" },
                        commitment: { type: "number" },
                        projectBudget: { type: "number" },
                        projectManagementModuleChartMaxMinDate: [
                            {
                                minDate: { type: "date" },
                                maxDate: { type: "date" }
                            }
                        ]
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "Dashboard/DasboardAll/" + _projectId,
                    async: false
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dsProjectDashboard: function () {
            this.getDsProjectDashboard.read()
            var getData = this.getDsProjectDashboard.data();

            var datakeyDeliverables = [];
            var dataOtherDepartments = [];
            var data = {};
            if (getData.length > 0) {
                data = getData[0];
                data["assignedCost"] = data.actualCost + data.commitment+data.forecast;
                //data["projectBudget"] = data.totalCost != 0 ? (data.actualCost + data.commitment) / data.totalCost : "-";
                //data["projectBudget"] = data.totalCost != 0 ? (data.actualCost + data.commitment) : "-";
                data["percentageBudget"] = data.totalCost != 0 ? (data.actualCost + data.commitment) / data.totalCost : "-";
                data["percentageBudget"] = data.totalCost != 0 ? (data.actualCost) / data.totalCost : "-";
                data["remainingBudget"] = data.totalCost - (data.actualCost);
                datakeyDeliverables = data.projectManagements.length > 0 ? data.projectManagements[0].keyDeliverables : [];
                dataOtherDepartments = data.otherDepartmentTasks.length > 0 ? data.otherDepartmentTasks : [];

            }

            this.set("dskeyDeliverables",
                    new kendo.data.DataSource({
                        pageSize: 20,
                        data: datakeyDeliverables,
                        autoSync: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    planStartDate: { type: "date" },
                                    planFinishDate: { type: "date" },
                                    actualStartDate: { type: "date" },
                                    actualFinishDate: { type: "date" },
                                    progress: { type: "number" }
                                }
                            }
                        },
                        sort: [{ field: "planStartDate", dir: "asc" }]
                    })
                );

            this.set("dsOtherDepartments",
                    new kendo.data.DataSource({
                        pageSize: 5,
                        data: dataOtherDepartments,
                        autoSync: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    id: {type: "number"},
                                    taskName: { type: "string" },
                                    departmentName: {type: "string"},
                                    planDate: { type: "date" },
                                    picName: { type: "string" }
                                }
                            }
                        }
                    })
                );

            this.set("dsAll", data);
            var datas = getData[0];
            var etcSum = datas.forecast + datas.commitment + datas.actualCost;
            if (etcSum > datas.remainingBudget) {
                $("#<%: this.ID %>_spanBudget").css("color", "red");
            } else {
                $("#<%: this.ID %>_spanBudget").css("color", "green");
            }
            

            $.each(datakeyDeliverables, function (idx, dataGraph) {
                dataGraph["planStartDateRep"] = dataGraph.planStartDate ? kendo.parseDate(dataGraph.planStartDate, "yyyy-MM-dd").getTime() : null;
                dataGraph["planFinishDateRep"] = dataGraph.planFinishDate ? kendo.parseDate(dataGraph.planFinishDate, "yyyy-MM-dd").getTime() : null;
                dataGraph["actualStartDateRep"] = dataGraph.actualStartDate ? kendo.parseDate(dataGraph.actualStartDate, "yyyy-MM-dd").getTime() : null;
                dataGraph["actualFinishDateRep"] = dataGraph.actualFinishDate ? kendo.parseDate(dataGraph.actualFinishDate, "yyyy-MM-dd").getTime() : null;
            });
            var dateNow = new Date();
            var tomorrow = new Date();
            tomorrow.setTime(dateNow.getTime() + (1000 * 60 * 60 * 5));
            var dateTmp;
            
            $("#<%: this.ID %>_projectManagementModuleChart").kendoChart({
                dataSource: datakeyDeliverables,
                title: {
                    text: "Task Completeness\n (Plan " + kendo.toString(dateNow, "MMMM") + ":" + (data.progressCurrentPlanVSActual.percentagePlan ? +data.progressCurrentPlanVSActual.percentagePlan.toFixed(2) : "0") + "% vs Actual to date:" + (data.progressCurrentPlanVSActual.percentageActual ? +data.progressCurrentPlanVSActual.percentageActual.toFixed(2) : "0") + "%)"
                },
                legend: {
                    position: "top"
                },
                seriesDefaults: {
                    type: "rangeBar"
                },
                series: [
                    {
                        name: "Plan Date",
                        fromField: "planStartDateRep",
                        toField: "planFinishDateRep"
                    },
                    {
                        name: "Actual Date",
                        fromField: "actualStartDateRep",
                        toField: "actualFinishDateRep"
                    }],
                categoryAxis: {
                    name: "categoryAxis",
                    field: "nameText",
                    majorGridLines: {
                        visible: false

                    },
                },
                valueAxis: [
                // {
                //    majorUnit: 31 * 24 * 60 * 60 * 1000, // 30 days in milliseconds
                //    majorGridLines: {
                //        visible: true,
                //    },
                //    line: {
                //        visible: true
                //    },
                //    labels: {
                //        rotation: -45,
                //        template: "#= kendo.toString(new Date(value), 'dd MMM yyyy') #",
                //    },
                //    _overlap: true
                //}, 
                {
                    name: "valueAxis1",
    //                majorUnit: 24 * 60 * 60 * 1000, // 30 days in milliseconds
                    //majorGridLines: {
                    //    visible: true,
                    //},
                    line: {
                        visible: true
                    },
                    labels: {
                        rotation: -45,
                        template: "#= kendo.toString(new Date(value), 'dd MMM yyyy') #",
                        //visual: function (e) {
                        //    var group = new kendo.drawing.Group();
                        //    if (e.value != 0 && e.value != 86400000 && dateTmp == null)
                        //        dateTmp = e.value;
                        //    var dateN = e.value - dateTmp;
                        //    var n = new kendo.geometry.Transformation();
                        //    if ((e.value != 0 && e.value != 86400000) && (dateN == 0 || (Math.round(dateN / (24 * 60 * 60 * 1000)) % 31 == 0))) {
                        //        e.text = kendo.toString(new Date(e.value), 'dd MMM yyyy');
                        //        n.rotate(-45, e.rect.origin);
                        //        e.rect.origin.x = e.rect.origin.x - 50;
                        //        e.rect.origin.y = e.rect.origin.y + 10;
                        //        dateTmp = e.value;
                        //    } else {
                        //        e.text = "";
                        //    }
                        //    return new kendo.drawing.Text(e.text, e.rect.origin, { transform: n });
                        //},
                    },

                }, 
                    {
                    name: "valueAxis2",
                    majorUnit: 24 * 60 * 60 * 1000, // 30 days in milliseconds
                    majorGridLines: {
                        visible: false,
                    },
                    line: {
                        visible: false
                    },
                    labels: {
                        visible: false
                    },
                    //plotBands: [{ from: dateNow, to: tomorrow, color: "#F00" }],
                    //_overlap: true
                    },
                ],
                render: function (e) {
                    var valueAxis = e.sender.getAxis("valueAxis2");
                    var valueSlot = valueAxis.slot(dateNow);

                    var categoryAxis = e.sender.getAxis("categoryAxis");
                    var lastCategoryIndex = Math.max(1, categoryAxis.range().max);
                    var minCategorySlot = categoryAxis.slot(0);
                    var maxCategorySlot = categoryAxis.slot(lastCategoryIndex);

                    var line = new kendo.drawing.Path({
                        stroke: {
                            color: "red",
                            width: 1
                        },
                        tooltip: {
                            content: kendo.toString(dateNow, 'dd MMM yyyy') + "(Today)",
                            position: "right"
                        }
                    });
                    line.moveTo([valueSlot.origin.x, minCategorySlot.origin.y]).lineTo([valueSlot.origin.x, maxCategorySlot.origin.y]);

                    var n = new kendo.geometry.Transformation();
                    n.rotate(90, [valueSlot.origin.x + 16, minCategorySlot.origin.y + Math.round((maxCategorySlot.origin.y - minCategorySlot.origin.y) / 2)]);
                    //var labelPos = [valueSlot.origin.x, minCategorySlot.origin.y + Math.round((maxCategorySlot.origin.y - minCategorySlot.origin.y) / 2)];
                    //var label = new kendo.drawing.Text(kendo.toString(dateNow, 'dd MMM yyyy') + "(Today)", labelPos, {
                    //    transform: n,
                    //    fill: {
                    //        color: "red"
                    //    },
                    //    font: "12px sans"
                    //});

                    var group = new kendo.drawing.Group();
                    group.append(line);
                    e.sender.surface.draw(group);
                },
                tooltip: {
                    visible: true,
                    position: "top",
                    template: "#= kendo.toString(new Date(value.from), 'dd MMM yyyy') # - #= kendo.toString(new Date(value.to), 'dd MMM yyyy') #"
                }
            });

            if (data.projectManagementModuleChartMaxMinDate.length > 0 && data.projectManagementModuleChartMaxMinDate[0].minDate && data.projectManagementModuleChartMaxMinDate[0].maxDate) {
                $("#<%: this.ID %>_projectManagementModuleChart").data("kendoChart").setOptions({ valueAxis: { min: kendo.parseDate(data.projectManagementModuleChartMaxMinDate[0].minDate).getTime(), max: kendo.parseDate(data.projectManagementModuleChartMaxMinDate[0].maxDate).getTime() } });
                
            }

        },
        dsAll: {},
        dskeyDeliverables: [],
        dsOtherDepartments: [],
        pdfClick: function (e) {
            var url = "<%: WebAppUrl.Replace("/", "|") %>" + "DashboardProjectReportForm" + ".aspx?ProjectID=" + _projectId;

            url = encodeURIComponent(url);
            var pdfUrl = _webApiUrlEllipse + "pdf3/FromAddress/--page-size%20A4%20--margin-top%202mm%20--margin-left%2010mm%20--margin-right%2010mm%20--margin-bottom%2010mm%20%20" + url + "/";

            window.open(pdfUrl);
        }
    });

    function replaceString(value) {
        return value.replace(/\n/g, "<br />");
    }

    $(document).ready(function () {
        //if (roleInProject == _roleOwner || roleInProject == _roleSponsor || roleInProject == _roleProjectManager || data.roleInProject == _roleProjectEngineer) {
        //} else {
        //    $("#%: this.ID %>_MainProjectContent").remove();
        //    _showDialogMessage("Access Denied", "You do not have access privileges to project", "");
        //}
        
        <%: this.ID %>_viewDashboardProjectModel.dsProjectDashboard();
        kendo.bind($("#<%: this.ID %>_MainProjectContent"), <%: this.ID %>_viewDashboardProjectModel);
    });
</script>

<div id="<%: this.ID %>_MainProjectContent">
    <table class="auto-style5">
        <tr>
            <td>
                <div class="container">
                    <div class="row">
                        <div class="col" style="text-align: right">
                            <div data-role="button" style="width:140px" data-bind="events: { click: pdfClick }">
                                <img src="images/print_36px.png" alt="Export to PDF" title="Print To PDF" /></div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr style="margin-top:10px; vertical-align: top;">
            <td class="auto-style9">
                <table class="auto-style2">
                    <tr>
                        <td style="padding-right:10px;vertical-align:top;" class="auto-style1">
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Scope</legend>
                                <div data-template="templateScope" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateScope" type="text/x-kendo-template">
                                    <div>
                                        #if (data.scope){#
                                            #=replaceString(data.scope) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Change Request</legend>
                                <div data-template="templateChangeRequest" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateChangeRequest" type="text/x-kendo-template">
                                    <div>
                                        #if (data.changeRequest){#
                                            #=replaceString(data.changeRequest) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Benefit</legend>
                                <div data-template="templateBenefit" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateBenefit" type="text/x-kendo-template">
                                    <div>
                                        #if (data.benefit){#
                                            #=replaceString(data.benefit) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Accomplishments</legend>
                                <div data-template="templateAccomplishments" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateAccomplishments" type="text/x-kendo-template">
                                    <div>
                                        #if (data.accomplishment){#
                                            #=replaceString(data.accomplishment) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Details</legend>
                                <div data-template="templateDetails" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateDetails" type="text/x-kendo-template">
                                    <div>
                                        #if (data.details){#
                                            #=replaceString(data.details) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Ehs Records</legend>
                                <div data-template="templateEHSRecords" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateEHSRecords" type="text/x-kendo-template">
                                    <div>
                                        #if (data.ehsRecords){#
                                            #=replaceString(data.ehsRecords) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Safety Perfomance</legend>
                                <div data-template="templateSafetyPerfomance" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateSafetyPerfomance" type="text/x-kendo-template">
                                    <div>
                                        #if (data.safetyPerfomance){#
                                            #=replaceString(data.safetyPerfomance) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset><br />
                            <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Organization Structure - Project Team</legend>
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:50%;text-decoration: underline;font-weight: bold;">
                                            Sponsor : <span data-bind="text: dsAll.sponsorName"></span>
                                        </td>
                                        <td style="width:50%;text-decoration: underline;font-weight: bold;">
                                            Owner: <span data-bind="text: dsAll.ownerName"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="auto-style7">
                                                <tr>
                                                    <td style="width:150px; padding-left: 20px;vertical-align: top">
                                                        Project Manager
                                                    </td>
                                                    <td style="width:5px; vertical-align: top">
                                                        :
                                                    </td>
                                                     <td style="vertical-align: top">
                                                        <span data-bind="text: dsAll.projectManagerName"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px;vertical-align: top">
                                                        Project Engineer
                                                    </td>
                                                    <td style="width:5px; vertical-align: top">
                                                        :
                                                    </td>
                                                     <td style="vertical-align: top">
                                                        <span data-bind="text: dsAll.projectEngineerName"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px; vertical-align: top">
                                                        Engineer
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <div data-template="templateEngineer" data-bind="source: dsAll"></div>
                                                        <script id="templateEngineer" type="text/x-kendo-template">
                                                            <div>
                                                                #var userEngineer = ""#
                                                                #var countEngineer = 0#
                                                                #if (data.userAssignments) {#
                                                                    #for (var i = 0; i < data.userAssignments.length; i++){#
                                                                        #if (data.userAssignments[i].assignmentType == "PE") {#
                                                                            #countEngineer++#
                                                                            #userEngineer += userEngineer ? "; " + data.userAssignments[i].employeeName :  data.userAssignments[i].employeeName#
                                                                        #}#
                                                                    #}#
                                                                #}#
                                                                #:userEngineer ? countEngineer > 1 ? userEngineer + ";" : userEngineer : "-"#
                                                            </div>
                                                        </script>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px; vertical-align: top">
                                                        Designer
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <div data-template="templateDesigner" data-bind="source: dsAll"></div>
                                                        <script id="templateDesigner" type="text/x-kendo-template">
                                                            <div>
                                                                #var userDesigner = ""#
                                                                #var countDesigner = 0#
                                                                #if (data.userAssignments) {#
                                                                    #for (var i = 0; i < data.userAssignments.length; i++){#
                                                                        #if (data.userAssignments[i].assignmentType == "PD") {#
                                                                            #countDesigner++#
                                                                            #userDesigner += userDesigner ? "; " + data.userAssignments[i].employeeName :  data.userAssignments[i].employeeName#
                                                                        #}#
                                                                    #} #
                                                                #} #
                                                                #:userDesigner ? countDesigner > 1 ? userDesigner + ";" : userDesigner : "-"#

                                                            </div>
                                                        </script>
                                                    </td>
                                                </tr>

                                                 <tr>
                                                    <td style="padding-left: 20px;vertical-align: top">
                                                        Project Controller
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <div data-template="templateProjectController" data-bind="source: dsAll"></div>
                                                        <script id="templateProjectController" type="text/x-kendo-template">
                                                            <div>
                                                                #var userProjectController = ""#
                                                                #var countProjectController = 0#
                                                                #if (data.userAssignments) {#
                                                                    #for (var i = 0; i < data.userAssignments.length; i++){#
                                                                        #if (data.userAssignments[i].assignmentType == "PC") {#
                                                                            #countProjectController++#
                                                                            #userProjectController += userProjectController ? "; " + data.userAssignments[i].employeeName :  data.userAssignments[i].employeeName#
                                                                        #}#
                                                                    #}#
                                                                #}#
                                                                #:userProjectController ? countProjectController > 1 ? userProjectController + ";" : userProjectController : "-"#
                                                            </div>
                                                        </script>
                                                    </td>
                                                </tr>

                                                 <tr>
                                                    <td style="padding-left: 20px;vertical-align: top">
                                                        Material Coordinator
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <div data-template="templateMaterialCoordinator" data-bind="source: dsAll"></div>
                                                        <script id="templateMaterialCoordinator" type="text/x-kendo-template">
                                                            <div>
                                                                #var userMaterialCoordinator = ""#
                                                                #var countMaterialCoordinator = 0#
                                                                #if (data.userAssignments) {#
                                                                    #for (var i = 0; i < data.userAssignments.length; i++){#
                                                                        #if (data.userAssignments[i].assignmentType == "MC") {#
                                                                            #countMaterialCoordinator++#
                                                                            #userMaterialCoordinator += userMaterialCoordinator ? "; " + data.userAssignments[i].employeeName :  data.userAssignments[i].employeeName#
                                                                        #}#
                                                                    #}#
                                                                #}#
                                                                #:userMaterialCoordinator ? countMaterialCoordinator > 1 ? userMaterialCoordinator + ";" : userMaterialCoordinator : "-"#
                                                            </div>
                                                        </script>
                                                    </td>
                                                <tr>
                                                    <td style="padding-left: 20px;vertical-align: top">
                                                        Project Officer
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <div data-template="templateProjectOfficer" data-bind="source: dsAll"></div>
                                                        <script id="templateProjectOfficer" type="text/x-kendo-template">
                                                            <div>
                                                                #var userProjectOfficer = ""#
                                                                #var countProjectOfficer = 0#
                                                                #if (data.userAssignments) {#
                                                                    #for (var i = 0; i < data.userAssignments.length; i++){#
                                                                        #if (data.userAssignments[i].assignmentType == "PO") {#
                                                                            #countProjectOfficer++#
                                                                            #userProjectOfficer += userProjectOfficer ? "; " + data.userAssignments[i].employeeName :  data.userAssignments[i].employeeName#
                                                                        #}#
                                                                    #}#
                                                                #}#
                                                                #:userProjectOfficer ? countProjectOfficer > 1 ? userProjectOfficer + ";" : userProjectOfficer : "-"#
                                                            </div>
                                                        </script>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset><br />
                        </td>
                        <td style="padding-right:10px;vertical-align:top;" class="auto-style16">
                          <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Cost</legend>
                                <table style="font-weight: bold;" class="auto-style21">
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style24">
                                            Total Budget
                                        </td>
                                        <td class="auto-style28">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style19">
                                          <span data-format="n2" data-bind="text: dsAll.totalCost"></span>  US$
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style17">
                                            Actual Cost
                                        </td>
                                        <td class="auto-style29">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style20">
                                          <span data-format="n2" data-bind="text: dsAll.actualCost"></span>  US$
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style24">
                                            Commitment
                                        </td>
                                        <td class="auto-style28">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style19">
                                         <span data-format="n2" data-bind="text: dsAll.commitment"></span> US$
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style24">
                                            Service Commitment
                                        </td>
                                        <td class="auto-style28">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style19">
                                         <span data-format="n2" data-bind="text: dsAll.forecast"></span> US$
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style17">
                                            Assigned Cost
                                        </td>
                                        <td class="auto-style29">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style20">
                                            <span data-format="n2" data-bind="text: dsAll.assignedCost"></span> US$
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style24">
                                            Project Budget
                                        </td>
                                        <td class="auto-style28">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style19">
                                          <span data-format="n2" data-bind="text: dsAll.projectBudget"></span>US$
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style24">
                                            % Budget
                                        </td>
                                        <td class="auto-style28">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style19">
                                            <span data-format="n2" data-bind="text: dsAll.percentageBudget"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 20px;" class="auto-style24">
                                            Remaining Budget
                                        </td>
                                        <td class="auto-style28">
                                            :
                                        </td>
                                        <td style="text-align:right; padding-right: 20px;" class="auto-style19">
                                           <span id="<%: this.ID %>_spanBudget" data-format="n2" data-bind="text: dsAll.remainingBudget"></span> US$
                                        </td>
                                    </tr>
                                </table>
                            </fieldset><br />
                                <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Issue/Risks</legend>
                                <div data-template="templateIssue" data-bind="source: dsAll.projectManagements"></div>
                                <script id="templateIssue" type="text/x-kendo-template">
                                    <div>
                                        #if (data.manageKeyIssue){#
                                            #=replaceString(data.manageKeyIssue) #
                                        #} else {#
                                            -
                                        #}#
                                    </div>
                                </script>
                            </fieldset>
                                <fieldset class="gdiFieldset">
                                <table>
                                    <thead>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                                <legend class="gdiLegend">Schedule</legend>
                                <div id="<%: this.ID %>_projectManagementModuleChart"></div>
                            </fieldset>&nbsp;
                                <fieldset class="gdiFieldset">
                                <legend class="gdiLegend">Others Informations</legend>
                                <div id="<%: this.ID %>_grdOthersInformationsProject0" 
                                    data-role="grid"
                                    data-sortable="true"
                                    data-editable="false"
                                    data-pageable="true"
                                    data-scrollable="false"
                                    data-bind="source: dsOtherDepartments"
                                    data-columns="[
                                       {
                                            field: 'taskName', title: 'Task',
                                            headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                            width: 200
                                        },
                                        {
                                            field: 'planDate', title: 'Date',
                                            headerAttributes: { style: 'text-align: center; vertical-align: top !important;' },
                                            attributes: { style: 'text-align: center; vertical-align: top !important;' },
                                            format: '{0:dd MMM yyyy}',
                                            sortable: false, width: 80
                                        },
                                        {
                                            field: 'departmentName', title: 'Department',
                                            headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                            width: 200
                                        },
                                       {
                                            field: 'picName', title: 'PIC',
                                            headerAttributes: { style: 'text-align: center; vertical-align: middle !important;' },
                                            width: 110
                                        },
                                    ]"]"
                                </div>
                            </fieldset>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="margin-top:10px; vertical-align: top;">
            <td class="auto-style9">
                <table class="auto-style6">
                    <tr>
                        <td style="padding-right:10px;vertical-align:top;" class="auto-style3">
                            <td style="padding-right:10px;vertical-align:top;" class="auto-style4">
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="margin-top:10px; vertical-align: top;">
            <td class="auto-style9"></td>
        </tr>
        
    </table>
</div>