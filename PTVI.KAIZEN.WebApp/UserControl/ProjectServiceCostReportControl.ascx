﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectServiceCostReportControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ProjectServiceCostReportControl" %>
<script id="toolbarTemplate" type="text/x-kendo-template">
<div id="exportExcel" class="k-button"><span class="k-icon k-i-file-excel"></span>
    Export to Excel</div>
</script>

<script>
    var <%: this.ID %>_costReportControlGrid = [], <%: this.ID %>_actualCostFormatData = [], currDate = new Date(), currYear = currDate.getFullYear(), currMonth = currDate.getMonth() + 1;
    var <%: this.ID %>_viewCostReportModel = kendo.observable({
        dsCostReport: new kendo.data.DataSource({
            requestStart: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), true);
                kendo.ui.progress($("#ProjectServiceForm"), true);
            },
            requestEnd: function () {
                //kendo.ui.progress($("#<%: this.ID %>_grdCostReportControl"), false);
                kendo.ui.progress($("#ProjectServiceForm"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "CostReport/ProjectServiceCostReport/" + _projectId + "|" + currYear + "|" + currMonth,
                    async: false
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        dsCostReportReformat: function () {
            var dataReformat = [];

            this.dsCostReport.read();

            var dataOri = this.dsCostReport.data()[0].dataCostReport;
            
            //generateColumnGrid
            <%: this.ID %>_costReportControlGrid = [];
            <%: this.ID %>_costReportControlGrid.push({
                field: "wbsLevelDesc", title: "Wbs Level", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 80,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
                locked: true
            });

            <%: this.ID %>_costReportControlGrid.push({
                title: "Wbs Level", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 0,
                attributes: { style: "text-align: center; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "costReportNo", title: "WBS No.", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                template: "<div #=data.wbsLevelDesc == 'Network' ? \"style='float: right;'\" : ''#>#=lpad('', (data.wbsLevelNo * 4), '&nbsp;')# #=data.costReportNo#</div>",
                width: 220,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
                //locked: true
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "description", title: "Description", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 300,
                attributes: { style: "text-align: left; vertical-align: top !important;" },
                filterable: false,
                //locked: true
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "budgetTotal", title: "Budget Total", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                format: "{0:n2}",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "actualCost", title: "Total Actual Cost</br>To Date (US$)", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                template: "#=data.actualCost ? kendo.toString(data.actualCost, 'n2') : '-'#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "allCommitment", title: "All Commitment</br>(US$)", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                template: "#=data.allCommitment ? kendo.toString(data.allCommitment, 'n2') : '-'#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "assignedCost", title: "Service Commitment</br>(US $)", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                template: "#= data.forecast || data.allCommitment ? kendo.toString(((data.forecast || data.forecast != 'null' ? data.forecast : 0) + (data.allCommitment || data.allCommitment != 'null'? data.allCommitment : 0)), 'n2') : '-'#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "budgetPercentage", title: "% Budget", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                template: "#=data.budgetTotal ? kendo.toString(((data.actualCost || data.actualCost != 'null' ? data.actualCost : 0) + (data.allCommitment || data.allCommitment != 'null'? data.allCommitment : 0)) / (data.budgetTotal || data.budgetTotal != 'null'? data.budgetTotal : 0), 'n2') : '-'#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                field: "remainingBudget", title: "Remaining Budget</br>(US$)", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                template: "#=data.allCommitment || data.actualCost || data.budgetTotal ? kendo.toString((data.budgetTotal || data.budgetTotal != 'null'? data.budgetTotal : 0) - ((data.actualCost || data.actualCost != 'null' ? data.actualCost : 0) + (data.allCommitment || data.allCommitment != 'null' ? data.allCommitment : 0)), 'n2') : '-'#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });
            <%: this.ID %>_costReportControlGrid.push({
                field: "forecast", title: "Un-commited Cost</br>(US$)", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                width: 120,
                template: "#=data.forecast ? kendo.toString(data.forecast, 'n2') : '-'#",
                attributes: { style: "text-align: right; vertical-align: top !important;" },
                filterable: false
            });

            <%: this.ID %>_costReportControlGrid.push({
                title: currYear.toString(),
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center; vertical-align: middle !important;"
                },
                columns: [
                    {
                        field: "budgetCurrYear", title: "Budget This Year", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        format: "{0:n2}",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    },
                    {
                        field: "actualCostCurrYear", title: "Actual Cost (US $)", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        template: "#=data.actualCostCurrYear ? kendo.toString(data.actualCostCurrYear, 'n2') : '-'#",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    },
                    {
                        field: "commitmentCurrYear", title: "Commitment (US$)", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        template: "#=data.commitmentCurrYear ? kendo.toString(data.commitmentCurrYear, 'n2') : '-'#",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    },
                    {
                        field: "assignedCostCurrYear", title: "Assigned Cost (US $)", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        template: "#=data.actualCostCurrYear || data.commitmentCurrYear ? kendo.toString(((data.actualCostCurrYear || data.actualCostCurrYear != 'null' ? data.actualCostCurrYear : 0) + (data.commitmentCurrYear || data.commitmentCurrYear != 'null'? data.commitmentCurrYear : 0)), 'n2') : '-'#",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    },
                    {
                        field: "budgetPercentageCurrYear", title: "% Budget", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        template: "#=data.budgetCurrYear ? kendo.toString(((data.actualCostCurrYear || data.actualCostCurrYear != 'null' ? data.actualCostCurrYear : 0) + (data.commitmentCurrYear || data.commitmentCurrYear != 'null'? data.commitmentCurrYear : 0)) / (data.budgetCurrYear || data.budgetCurrYear != 'null'? data.budgetCurrYear : 0), 'n2') : '-'#",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    },
                    {
                        field: "remainingBudgetCurrYear", title: "Remaining Budget (US$)", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        template: "#=data.commitmentCurrYear || data.actualCostCurrYear || data.budgetCurrYear ? kendo.toString((data.budgetCurrYear || data.budgetCurrYear != 'null'? data.budgetCurrYear : 0) - ((data.actualCostCurrYear || data.actualCostCurrYear != 'null' ? data.actualCostCurrYear : 0) + (data.commitmentCurrYear || data.commitmentCurrYear != 'null' ? data.commitmentCurrYear : 0)), 'n2') : '-'#",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    },
                    {
                        field: "totalForecastCurrYear", title: "Total Forecast", editable: false,
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center; vertical-align: middle !important;"
                        },
                        width: 120,
                        format: "{0:n2}",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                        filterable: false
                    }]
            });

            //loop grid column month group by Year
            if (this.dsCostReport.data()[0].min && this.dsCostReport.data()[0].max) {
                var columnMonthByYear = [];

                for (var j = this.dsCostReport.data()[0].min; j <= this.dsCostReport.data()[0].max; j++) {
                    columnMonthByYear.push({
                        field: "amountActual_" + j, title: kendo.toString(new Date(currYear, j - 1, 01), 'MMM'),
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center;"
                        }, width: 120,
                        format: "{0:n2}",
                        template: "#=(data.month_" + j + " < " + currMonth + ") ? data.amountActual_" + j + " ? kendo.toString((data.amountActual_" + j + "), 'n2') : '-' : data.amountCommitment_" + j + " ? kendo.toString((data.amountCommitment_" + j + "), 'n2') : '-'#",
                        attributes: { style: "text-align: right; vertical-align: top !important;" },
                    });
                }

                <%: this.ID %>_costReportControlGrid.push({
                    title: currYear.toString(),
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center; vertical-align: middle !important;"
                    },
                    columns: columnMonthByYear
                });
            }


            this.set("columnProjectKPIProgressMonthlyModule", <%: this.ID %>_costReportControlGrid);

            //Generate Data
            for (var i = 0; i < dataOri.length; i++) {
                // new object for insert DataReformat
                var actualCostData = {};

                actualCostData["wbsLevelNo"] = dataOri[i].wbsLevelNo;
                <%: this.ID %>_actualCostFormatData["wbsLevelNo"] = { type: "number" };

                actualCostData["wbsLevelDesc"] = dataOri[i].wbsLevelDesc;
                <%: this.ID %>_actualCostFormatData["wbsLevelDesc"] = { type: "string" };

                actualCostData["wbsNo"] = dataOri[i].wbsNo;
                <%: this.ID %>_actualCostFormatData["wbsNo"] = { type: "string" };

                actualCostData["costReportNo"] = dataOri[i].costReportNo;
                <%: this.ID %>_actualCostFormatData["costReportNo"] = { type: "string" };

                actualCostData["description"] = dataOri[i].description;
                <%: this.ID %>_actualCostFormatData["description"] = { type: "string" };

                actualCostData["budgetTotal"] = dataOri[i].budgetTotal;
                <%: this.ID %>_actualCostFormatData["budgetTotal"] = { type: "number" };

                actualCostData["actualCost"] = dataOri[i].actualCost;
                <%: this.ID %>_actualCostFormatData["actualCost"] = { type: "number" };

                actualCostData["allCommitment"] = dataOri[i].allCommitment;
                <%: this.ID %>_actualCostFormatData["allCommitment"] = { type: "number" };

                actualCostData["assignedCost"] = dataOri[i].assignedCost;
                <%: this.ID %>_actualCostFormatData["assignedCost"] = { type: "number" };

                actualCostData["budgetPercentage"] = dataOri[i].budgetPercentage;
                <%: this.ID %>_actualCostFormatData["budgetPercentage"] = { type: "number" };

                actualCostData["forecast"] = dataOri[i].forecast;
                <%: this.ID %>_actualCostFormatData["forecast"] = { type: "number" };

                actualCostData["remainingBudget"] = dataOri[i].remainingBudget;
                <%: this.ID %>_actualCostFormatData["remainingBudget"] = { type: "number" };

                actualCostData["budgetCurrYear"] = dataOri[i].budgetCurrYear;
                <%: this.ID %>_actualCostFormatData["budgetCurrYear"] = { type: "number" };

                actualCostData["actualCostCurrYear"] = dataOri[i].actualCostCurrYear;
                <%: this.ID %>_actualCostFormatData["actualCostCurrYear"] = { type: "number" };

                actualCostData["commitmentCurrYear"] = dataOri[i].commitmentCurrYear;
                <%: this.ID %>_actualCostFormatData["commitmentCurrYear"] = { type: "number" };

                actualCostData["assignedCostCurrYear"] = dataOri[i].assignedCostCurrYear;
                <%: this.ID %>_actualCostFormatData["assignedCostCurrYear"] = { type: "number" };

                actualCostData["budgetPercentageCurrYear"] = dataOri[i].budgetPercentageCurrYear;
                <%: this.ID %>_actualCostFormatData["budgetPercentageCurrYear"] = { type: "number" };

                actualCostData["remainingBudgetCurrYear"] = dataOri[i].remainingBudgetCurrYear;
                <%: this.ID %>_actualCostFormatData["remainingBudgetCurrYear"] = { type: "number" };

                actualCostData["totalForecastCurrYear"] = dataOri[i].totalForecastCurrYear;
                <%: this.ID %>_actualCostFormatData[""] = { type: "number" };

                //generate plan & actual progress by month
                for (var j = 0; j < dataOri[i].costReportActualCostMonths.length; j++) {
                    actualCostData["month_" + dataOri[i].costReportActualCostMonths[j].month] = dataOri[i].costReportActualCostMonths[j].month;
                    actualCostData["amountActual_" + dataOri[i].costReportActualCostMonths[j].month] = dataOri[i].costReportActualCostMonths[j].amount;
                }

                for (var j = 0; j < dataOri[i].costReportCommitmentMonths.length; j++) {
                    actualCostData["month_" + dataOri[i].costReportCommitmentMonths[j].month] = dataOri[i].costReportCommitmentMonths[j].month;
                    actualCostData["amountCommitment_" + dataOri[i].costReportCommitmentMonths[j].month] = dataOri[i].costReportCommitmentMonths[j].amount;
                }

                dataReformat[i] = actualCostData;
            }

            return dataReformat;
        },
        generateGriCostReportControl: function () {
            var that = this;
            var dataGrid = that.dsCostReportReformat();
            var idGrid = "#<%: this.ID %>_grdCostReportControl";
            var dataSource = new kendo.data.DataSource({
                data: dataGrid,
                schema: {
                    model: {
                        id: "id",
                        fields: <%: this.ID %>_actualCostFormatData
                    }
                },
                //aggregate: aggregateGridInquery
            });

            if ($(idGrid).getKendoGrid() != null) {
                $(idGrid).getKendoGrid().destroy();
                $(idGrid).empty();
            }

            if ($(idGrid).getKendoGrid() == null) {
                $(idGrid).kendoGrid({
                    toolbar: [{ template: kendo.template($("#toolbarTemplate").html()) }], // ["excel"],
                    dataSource: dataSource,
                    dataBound: <%: this.ID %>_onDataBound,
                    columns: <%: this.ID %>_costReportControlGrid,
                    scrollable: true,
                    excelExport: function (e) {
                        var sheet = e.workbook.sheets[0];
                        var rowIndex = 2, cell = 0;
                        var sheet = e.workbook.sheets[0];
                        $.each(e.data, function (key, value) {
                            value.actualCost || value.allCommitment ? value.assignedCost = ((value.actualCost || value.actualCost != 'null' ? value.actualCost : 0) + (value.allCommitment || value.allCommitment != 'null' ? value.allCommitment : 0)) : '-';
                            value.budgetTotal ? budgetPercentage = ((value.actualCost || value.actualCost != 'null' ? value.actualCost : 0) + 
                                (value.allCommitment || value.allCommitment != 'null'? value.allCommitment : 0)) / (value.budgetTotal || value.budgetTotal != 'null'? value.budgetTotal : 0) : '-'                            
                            value.allCommitment || value.actualCost || value.budgetTotal ?
                                value.remainingBudget = (value.budgetTotal || value.budgetTotal != 'null' ? value.budgetTotal : 0) -
                                ((value.actualCost || value.actualCost != 'null' ? value.actualCost : 0) +
                                    (value.allCommitment || value.allCommitment != 'null' ? value.allCommitment : 0)) : '-';
                            
                            value.actualCostCurrYear ? value.actualCostCurrYear = value.actualCostCurrYear : '-';
                            value.commitmentCurrYear ? value.commitmentCurrYear = value.commitmentCurrYear : '-';
                            value.actualCostCurrYear || value.commitmentCurrYear ? value.assignedCostCurrYear = ((value.actualCostCurrYear || value.actualCostCurrYear != 'null' ? value.actualCostCurrYear : 0) + (value.commitmentCurrYear || value.commitmentCurrYear != 'null'? value.commitmentCurrYear : 0)) : '-';
                            value.budgetCurrYear ? value.budgetPercentageCurrYear = ((value.actualCostCurrYear || value.actualCostCurrYear != 'null' ? value.actualCostCurrYear : 0) + (value.commitmentCurrYear || value.commitmentCurrYear != 'null'? value.commitmentCurrYear : 0)) / (value.budgetCurrYear || value.budgetCurrYear != 'null'? value.budgetCurrYear : 0) : '-';
                            value.commitmentCurrYear || value.actualCostCurrYear || value.budgetCurrYear ? value.remainingBudgetCurrYear = (value.budgetCurrYear || value.budgetCurrYear != 'null'? value.budgetCurrYear : 0) - ((value.actualCostCurrYear || value.actualCostCurrYear != 'null' ? value.actualCostCurrYear : 0) + (value.commitmentCurrYear || value.commitmentCurrYear != 'null' ? value.commitmentCurrYear : 0)) : '-';
                            if (rowIndex > 1) {
                                var row = sheet.rows[rowIndex];
                                row.cells[6].value = value.assignedCost;
                                row.cells[7].value = value.budgetPercentage;
                                row.cells[8].value = value.remainingBudget;

                                row.cells[10].value = value.actualCostCurrYear;
                                row.cells[11].value = value.commitmentCurrYear;
                                row.cells[12].value = value.assignedCostCurrYear;
                                row.cells[13].value = value.budgetPercentageCurrYear;
                                row.cells[14].value = value.remainingBudgetCurrYear;
                                row.cells[15].value = value.totalForecastCurrYear;


                                row.cells[1].value = lpad('', (value.wbsLevelNo * 4), ' ') + row.cells[1].value;
                                if (value.wbsLevelDesc == 'Network') {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].background = "#FFFFFF";
                                    }
                                    row.cells[1].textAlign = "right";
                                } else if (value.wbsLevelNo == 0) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                            row.cells[cellIndex].background = "#F68D36";
                                    }
                                } else if (value.wbsLevelNo == 1) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].background = "#FFC000";
                                    }
                                } else if (value.wbsLevelNo == 2) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].background = "#FFC000";
                                    }
                                } else if (value.wbsLevelNo == 3) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].background = "#92D050";
                                    }
                                } else if (value.wbsLevelNo == 4) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].background = "#808080";
                                    }
                                } else if (value.wbsLevelNo == 5) {
                                    for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                        row.cells[cellIndex].background = "#99CCFF";
                                    }
                                }
                            }
                            rowIndex++;
                        });
                        for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                            var row = sheet.rows[rowIndex];
                            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                                if (!isNaN(row.cells[cellIndex].value)) {
                                    if (cellIndex < 2) {
                                        row.cells[cellIndex].value = row.cells[cellIndex].value * 1;
                                        row.cells[cellIndex].format = "#0_)"
                                    } else
                                    row.cells[cellIndex].format = "[Blue]#,##0.00_);[Red]-#,##0.00;0.00;"
                                }
                                if (rowIndex == 0 || rowIndex == 1) {
                                    sheet.rows[rowIndex].cells[cellIndex].textAlign = "center";
                                }
                            }
                            if (row.type == "header") {
                                row.height = 30;
                            } 
                        }

                    }
                });
            }

        $("#exportExcel").on("click", function () {
            var idGrid = "#<%: this.ID %>_grdCostReportControl";
            var grid = $(idGrid).getKendoGrid();
            var column = grid.columns;
            $.each(column, function (key, value) {
                value.title = value.title.replace("</br>","\r");
            })
            grid.saveAsExcel();
        })

        }
    });

    function <%: this.ID %>_onDataBound(e) {
        var data = $("#<%: this.ID %>_grdCostReportControl").getKendoGrid().dataSource.data()
        $.each(data, function (i, row) {
            if (row.wbsLevelDesc == 'Network') {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#FFFFFF");
            } else if (row.wbsLevelNo == 0) {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#F68D36");
            } else if (row.wbsLevelNo == 1) {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#FFC000");
            } else if (row.wbsLevelNo == 2) {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#FFFF00");
            } else if (row.wbsLevelNo == 3) {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#92D050");
            } else if (row.wbsLevelNo == 4) {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#808080");
            } else if (row.wbsLevelNo == 5) {
                var element = $('tr[data-uid="' + row.uid + '"] ');
                $(element).css("background", "#99CCFF");
            }
        });
    }

    $(document).ready(function () {
        <%--        kendo.bind($("#<%: this.ID %>_CostReportControl"), <%: this.ID %>_viewCostReportModel);
        <%: this.ID %>_viewCostReportModel.generateGriCostReportControl();--%>
        var <%: this.ID %>_lastUpdateCostReport = lastUpdateCostReport(_projectId);
        $("#<%: this.ID %>_lastUpdateText").html("<div style='color: orange'>Last Update: " + (!<%: this.ID %>_lastUpdateCostReport ? "-" : <%: this.ID %>_lastUpdateCostReport != null ? <%: this.ID %>_lastUpdateCostReport : "-") + "</div>");
    });

</script>


<div id="<%: this.ID %>_CostReportControl">
    <div id="<%: this.ID %>_lastUpdateText"></div>
    <div id="<%: this.ID %>_grdCostReportControl" style="height: 450px">
    </div>
</div>
