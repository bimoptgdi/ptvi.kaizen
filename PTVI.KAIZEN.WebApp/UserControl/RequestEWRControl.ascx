﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestEWRControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.RequestEWRControl" %>

<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton, <%: this.ID %>_uploaderFile;
    var <%: this.ID %>_idProject = "";
    var <%: this.ID %>_dateCompletion;
    // Variable

    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        items: [],
        itemsRequestedWork: [],
        itemInitiate: [],
        isOtherChecked: false,
        otherDescription: "",
        parentForm: "",
        ewrOID: -1,
        operationalCPFullName: "",
        operationalCP2FullName: "",
        requestData: new common_ewrRequest(),
        onOtherChange: function (e) {
            this.set("isOtherChecked", !this.get("isOtherChecked"));
            if (!this.get("isOtherChecked")) {
                this.set("otherDescription", "");
            }
        },
        isEnabled: true,
        onValidationEntry: function () {
            var validatable = $("#<%: this.ID %>_RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validatable.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validatable.errors();

                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }
            
            // cek jika pilih other di requested work, maka descriptionnya harus diisi
            var isRequestedWorkValid = true;
            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    if (!<%: this.ID %>_viewModel.get("otherDescription")) {
                        isRequestedWorkValid = false;
                    }
                }
            });
            
            if (!isRequestedWorkValid) {
                _showDialogMessage("Required Message", "Please fill Other's description in Type of Requested Work", "");
                return false;
            }

            if (this.items.length == 0) {
                alert("Please select Category of Work");
                return false;
            }
            if (this.itemsRequestedWork.length == 0) {
                alert("Please select Type of Requested Work");
                return false;
            }
            if (this.itemInitiate.length == 0) {
                alert("Please select Detail of Work Requirement");
                return false;
            }

            return isValid;
        },
        onSubmitClick: function (e) {
            
            if (this.onValidationEntry()) {
                
                if (confirm("Are you sure you submit this request?")) {

                    var requestData = this.populateEWRData();

                    kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);
                    $.ajax({
                        url: _webApiUrl + "ewrrequests/SubmitEWRRequest/1",
                        type: "POST",
                        data: requestData,
                        success: function (data) {
                            
                            <%: this.ID %>_viewModel.set("ewrOID", data.OID);

                            if (<%: this.ID %>_kendoUploadButton) {
                                <%: this.ID %>_kendoUploadButton.click();
                            } else {
                                //_showDialogMessage("Submit Request", "Request has been submitted", "");
                                alert("Request has been submitted");
                                //location.reload();
                                window.location.href = "EwrTaskListForm.aspx?ShowNeedMyResponse=false";
                            }

                            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                            <%: this.ID %>_viewModel.onClearEvent(data);
                        },
                        error: function (jqXHR) {
                            kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                        
                            _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                        }
                    });
                }
                
            } else {
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
            }
        },
        onDraftClick: function (e) {
            
            if (confirm("Are you sure you save this request as Draft?")) {
                //
                var requestData = this.populateEWRData();
                    
                kendo.ui.progress($("#<%: this.ID %>_RequestContent"), true);
                $.ajax({
                    url: _webApiUrl + "ewrrequests/SaveEWRRequestAsDraft/0",
                    type: "POST",
                    data: requestData,
                    success: function (data) {
                            
                        <%: this.ID %>_viewModel.set("ewrOID", data.OID);

                        if (<%: this.ID %>_kendoUploadButton) {
                            <%: this.ID %>_kendoUploadButton.click();
                        } else {
                            //_showDialogMessage("Submit Request", "Request has been submitted", "");
                            alert("Request has been save as Draft");
                            //location.reload();
                            window.location.href = "EwrTaskListForm.aspx?ShowNeedMyResponse=false";
                        }

                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);

                        <%: this.ID %>_viewModel.onClearEvent(data);
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#<%: this.ID %>_RequestContent"), false);
                        
                        _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                    }
                });
            }
           
        },
        onCloseClick: function (e) {
            this.onClearEvent(e);

            //_showDialogMessage("Close Form", "Close Request Form", <%: this.ID %>_viewModel.get("parentForm"));
            _showDialogMessage("Close Form", "Are you sure you want to close this form", "EwrTaskListForm.aspx?ShowNeedMyResponse=false");

        },
        dsEmployee: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {

                    var searchParam = options.data.filter.filters[0].value;
                    if (searchParam && searchParam.length > 0) {
                        var result = [];
                        // if (this.get("selectedUser").length >= 3) {

                        //var filter = "?$filter=((EMPLOYEE_ID contains '*" + searchParam + "*') or lower(FULL_NAME) contains lower('*" + searchParam + "*')) and (LEVEL_INFO not contains '*L2*' and LEVEL_INFO not contains '*L3*' and LEVEL_INFO not contains '*L4*')";
                        var filter = "?$filter=(substringof('" + searchParam + "',EMPLOYEEID) or substringof(tolower('" + searchParam + "'),tolower(FULL_NAME)))";
                        //filter += " and (substringof('L2',LEVEL_INFO) eq true)";

                        var result = [];
                        $.ajax({
                            url: _webOdataUrl + "EmployeeOData" + filter,
                            type: "GET",
                            async: false,
                            success: function (data) {
                                //result = data.value;

                                options.success(data.value);
                            },
                            error: function (data) {
                                //return [];
                                //options.error(data.value);
                                options.error(data.value);
                            }
                        });
                    } else {
                        options.success([]);
                    }
                    // }

                }
            }
        }),
        dsEmployeeL2: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {

                    var searchParam = options.data.filter.filters[0].value;
                    if (searchParam && searchParam.length > 0) {
                        var result = [];
                        // if (this.get("selectedUser").length >= 3) {

                        //var filter = "?$filter=((EMPLOYEE_ID contains '*" + searchParam + "*') or lower(FULL_NAME) contains lower('*" + searchParam + "*')) and (LEVEL_INFO not contains '*L2*' and LEVEL_INFO not contains '*L3*' and LEVEL_INFO not contains '*L4*')";
                        var filter = "?$filter=(substringof('" + searchParam + "',EMPLOYEEID) or substringof(tolower('" + searchParam + "'),tolower(FULL_NAME)))";
                        filter += " and (substringof('L2',LEVEL_INFO) eq true)";

                        var result = [];
                        $.ajax({
                            url: _webOdataUrl + "EmployeeOData" + filter,
                            type: "GET",
                            async: false,
                            success: function (data) {
                                //result = data.value;

                                options.success(data.value);
                            },
                            error: function (data) {
                                //return [];
                                //options.error(data.value);
                                options.error(data.value);
                            }
                        });
                    } else {
                        options.success([]);
                    }
                    // }

                }
            }
        }),
        dsOperationalCP: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        dsOperationalCP2: new kendo.data.DataSource({
            type: "odata",
            serverFiltering: true,
            transport: {
                read: {
                    url: _webOdataUrl + "EmployeeOData",
                    dataType: "json"
                }

            },
            schema: {
                data: function (data) {
                    return data.value;
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: {
                    id: "EMPLOYEEID"
                }
            }
        }),
        autocompleteFiltering: function (e) {
            var filter = e.filter;

            if (!filter.value) {
                //prevent filtering if the filter does not value
                e.preventDefault();
            }
        },
        onOperationalCPSelected: function (e) {
            <%: this.ID %>_viewModel.set("operationalCPFullName", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.operationalCP", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.operationalCPBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.operationalCPEmail", e.dataItem.EMAIL);
            
        },
        onOperationalCPClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onOperationalCP2Selected: function (e) {
            <%: this.ID %>_viewModel.set("operationalCP2FullName", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.operationalCP2", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.operationalCP2BN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.operationalCP2Email", e.dataItem.EMAIL);
            
        },
        onOperationalCP2Close: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onProjectSponsorSelected: function(e) {
            
            <%: this.ID %>_viewModel.set("requestData.projectSponsor", common_toTitleCase(e.dataItem.FULL_NAME));
            <%: this.ID %>_viewModel.set("requestData.projectSponsorBN", e.dataItem.EMPLOYEE_ID);
            <%: this.ID %>_viewModel.set("requestData.projectSponsorEmail", e.dataItem.EMAIL);
        },
        onProjectSponsorClose: function (e) {
           <%-- if (<%: this.ID %>_viewModel.get("requestData.projectSponsorBN")) {
                <%: this.ID %>_viewModel.set("requestForName", <%: this.ID %>_viewModel.get("requestData.projectSponsorBN") + " - " + common_toTitleCase(e.sender.value()));
            }--%>

        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        onClearEvent: function (e) {
            this.requestData.set("ewrNo", null);
            this.requestData.set("subject", null);
            this.requestData.set("area", null);
            this.requestData.set("dateIssued", null);
            this.requestData.set("accCode", null);
            this.requestData.set("networkNo", null);
            this.requestData.set("budgetAlloc", null);
            this.requestData.set("dateCompletion", null);
            this.requestData.set("problemStatement", null);
            this.requestData.set("objective", null);
            this.requestData.set("assignedPM", null);
            this.requestData.set("projectSponsor", null);
            this.requestData.set("sponsorLocation", null);
            this.requestData.set("sponsorPhoneNo", null);
            this.requestData.set("operationalCP", null);
            this.requestData.set("operationalCP2", null);
            this.requestData.set("projectTitle", null);
            this.requestData.set("status", null);

            this.set("otherDescription", "");
            this.set("isOtherChecked", false);
            this.set("items", []);
            this.set("itemsRequestedWork", []);
            this.set("itemInitiate", []);
        },
        populateEWRData: function () {
            var categoryOfWork = [];
            $.each(this.items, function (idx, el) {
                categoryOfWork.push({ TYPE: "categoryofwork", VALUE: el });
            });
            //if (categoryOfWork.length == 0) {
            //    alert("Please select Category of Work");
            //    return;
            //}
            var typeOfRequestedWork = [];

            var otherDesc = this.otherDescription;

            $.each(this.itemsRequestedWork, function (idx, el) {
                if (el == "other") {
                    typeOfRequestedWork.push({ TYPE: "requestofwork", VALUE: el, DESCRIPTION: otherDesc });
                } else {
                    typeOfRequestedWork.push({ TYPE: "requestofwork", VALUE: el });
                }

            });

            //if (typeOfRequestedWork.length == 0) {
            //    alert("Please select Type of Requested Work");
            //    return;
            //}

            var today = new Date();
            today.setHours(0, 0, 0, 0);
            var completionDate = new Date(<%: this.ID %>_dateCompletion.value());

            //if (completionDate.getTime() < today.getTime()) {
            //    alert("Date Completion must be equal or greater than today");
            //    return;
            //}

            var detailOfWork = [];
            $.each(this.itemInitiate, function (idx, el) {
                detailOfWork.push({ TYPE: "detailofwork", VALUE: el });
            });

            //if (detailOfWork.length == 0) {
            //    alert("Please select Detail of Work Requirement");
            //    return;
            //}

            var requestData =
            {
                OID: this.requestData.oid ? this.requestData.oid : -1,
                EWRNO: this.requestData.ewrNo,
                SUBJECT: this.requestData.subject,
                AREA: this.requestData.area,
                DATEISSUED: kendo.toString(this.requestData.dateIssued, 'yyyy-MM-dd'),
                ACCCODE: this.requestData.accCode,
                NETWORKNO: this.requestData.networkNo,
                BUDGETALLOC: this.requestData.budgetAlloc,
                DATECOMPLETION: kendo.toString(<%: this.ID %>_dateCompletion.value(), 'yyyy-MM-dd'),
                PROBLEMSTATEMENT: this.requestData.problemStatement,
                OBJECTIVE: this.requestData.objective,
                ASSIGNEDPM: this.requestData.assignedPM,
                PROJECTSPONSOR: this.requestData.projectSponsor,
                PROJECTSPONSORBN: this.requestData.projectSponsorBN,
                SPONSORLOCATION: this.requestData.sponsorLocation,
                SPONSORPHONENO: this.requestData.sponsorPhoneNo ? this.requestData.sponsorPhoneNo.replace(/-/g, "").replace(/_/g, "") : "",
                OPERATIONALCP: this.get("operationalCPFullName"),
                PROJECTTITLE: this.requestData.projectTitle,
                STATUS: this.requestData.status,
                OPERATIONALCPEMAIL: this.requestData.operationalCPEmail,
                OPERATIONALCPBN: this.requestData.operationalCPBN,
                OPERATIONALCP2: this.get("operationalCP2FullName"),
                OPERATIONALCP2EMAIL: this.requestData.operationalCP2Email,
                OPERATIONALCP2BN: this.requestData.operationalCP2BN,
                SPONSOREMAIL: this.requestData.projectSponsorEmail,
                REQUESTORNAME: _currUserName,
                REQUESTOREMAIL: _currEMail,
                REQUESTORBN: _currBadgeNo,
                CATEGORYOFWORK: categoryOfWork,
                TYPEOFREQUESTEDWORK: typeOfRequestedWork,
                DETAILOFWORK: detailOfWork
            };

            return requestData;
        }
    });

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);

                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    // Document Ready
    $(document).ready(function () {
        //<%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#<%: this.ID %>_RequestContent"), <%: this.ID %>_viewModel);

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        <%: this.ID %>_viewModel.onClearEvent()
        $("#<%: this.ID %>_RequestContent").kendoTooltip({
            filter: "#<%: this.ID %>_shareFolderDoc",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format Share Folder, e.g.: \\\\netapp8\\folder";
                return content;
            }
        }).data("kendoTooltip");

        <%: this.ID %>_viewModel.requestData.set("dateIssued", new Date());

        if (!<%: this.ID %>_uploaderFile) {
            <%: this.ID %>_uploaderFile = $("#<%: this.ID %>_uploaderFile").kendoUpload({
                async: {
                    saveUrl:  _webApiUrl + "upload/PostUploadEWR/1",
                    removeUrl: "remove",
                    batch: true,
                    autoUpload: false

                },
                //showFileList: false,
                select: onUploadSelect,
                upload: onUploadSelect,
                success: onUploadSuccess,
                error: onUploadError,
                localization: {
                    select: 'Browse...'
                },
                upload: function (e) { 
                    
                    e.data = { typeId: <%: this.ID %>_viewModel.get("ewrOID"), typeName: "EWR", badgeNo: _currBadgeNo, typeDetail: "EWR Request" };
                }
            }).data("kendoUpload");
        }
        
        <%: this.ID %>_dateCompletion = $("#<%: this.ID %>_dateOfCompletion2").kendoDatePicker({
            value: new Date(),
            format: "dd MMM yyyy",
            min: new Date()
        }).data("kendoDatePicker");

    }); //doc ready

    function onUploadSelect(e) {
        var files = e.files;

        setTimeout(function () {
            <%: this.ID %>_kendoUploadButton = $(".k-upload-selected");
            <%: this.ID %>_kendoUploadButton.hide();
        }, 1);
    }
        
    function onUploadSuccess (e) {
        //_showDialogMessage("Submit Request", "Request has been submitted", "");
        alert("Request has been submitted");
        <%: this.ID %>_uploaderFile.clearAllFiles();

        window.location.href = "EwrTaskListForm.aspx?ShowNeedMyResponse=false";
        
    }

    function onUploadError(e) {
        // Array with information about the uploaded files
        var files = e.files;
        var err = e.XMLHttpRequest;

        if (e.operation == "upload") {
            _showErrorMessage("Error message", "Failed to upload '" + files[0].name + "' file.", err.status + " " + err.statusText + ".\n" + err.responseText, 3, "");
        }
    }

    function <%: this.ID %>_Init(dataRequest, parentForm) {

        <%: this.ID %>_viewModel.set("parentForm", "HomeForm.aspx");
        if (parentForm) {
            <%: this.ID %>_viewModel.set("parentForm", parentForm);
        }

        // jika dataRequest ada data nya maka update
        if (dataRequest) {
            var requestData =
            {
                id: dataRequest.OID,
                //ewrNo: 
                subject: dataRequest.SUBJECT,
                area: dataRequest.AREA,
                dateIssued: kendo.toString(dataRequest.DATEISSUED, 'yyyy-MM-dd'),
                accCode: dataRequest.ACCCODE,
                networkNo: dataRequest.NETWORKNO,
                budgetAlloc: dataRequest.BUDGETALLOC,
                dateCompletion: kendo.toString(dataRequest.DATECOMPLETION, 'yyyy-MM-dd'),
                problemStatement: dataRequest.PROBLEMSTATEMENT,
                objective: dataRequest.OBJECTIVE,
                assignedPM: dataRequest.ASSIGNEDPM,
                projectSponsor: dataRequest.PROJECTSPONSOR,
                sponsorLocation: dataRequest.SPONSORLOCATION,
                sponsorPhoneNo: dataRequest.SPONSORPHONENO ? dataRequest.SPONSORPHONENO.replace(/-/g, "").replace(/_/g, "") : "",
                operationalCP: dataRequest.OPERATIONALCP,
                operationalCP2: dataRequest.OPERATIONALCP2,
                projectTitle: dataRequest.PROJECTTITLE,
                status: dataRequest.STATUS

            };

            <%: this.ID %>_viewModel.set("requestData", requestData);

            var arrReqWork = [];
            $.each(dataRequest.TYPEOFREQUESTEDWORK, function (idx, elem) {
                arrReqWork.push(elem.VALUE);
                if (elem.VALUE == "other") {
                    <%: this.ID %>_viewModel.set("otherDescription", elem.DESCRIPTION);
                    <%: this.ID %>_viewModel.set("isOtherChecked", true);
                }
            });
            <%: this.ID %>_viewModel.set("itemsRequestedWork", arrReqWork);

            var arrCat = [];
            $.each(dataRequest.CATEGORYOFWORK, function (idx, elem) {
                arrCat.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("items", arrCat);

            var arrInitiate = [];
            $.each(dataRequest.DETAILOFWORK, function (idx, elem) {
                arrInitiate.push(elem.VALUE);
            });
            <%: this.ID %>_viewModel.set("itemInitiate", arrInitiate);

            $('#<%: this.ID %>_create').hide();
            $('#<%: this.ID %>_update').show();
        } else {
            $('#<%: this.ID %>_create').show();
            $('#<%: this.ID %>_update').hide();
        }

        
    }
</script>
<div id="<%: this.ID %>_RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_inputProjectLegend">Input EWR</legend>
            <div>
                <label class="labelFormWeight">Date Issued<span style="color: red">*</span></label>
                <input data-role="datepicker" id="<%: this.ID %>_dateIssued" name="<%: this.ID %>_dateIssued" data-format="dd MMM yyyy" data-bind="value: requestData.dateIssued, enabled: isEnabled" placeholder="Date Issued" required/>
            </div>
            <div>
                <label class="labelFormWeight">Area<span style="color: red">*</span></label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_area" name="<%: this.ID %>_area" data-bind="value: requestData.area, enabled: isEnabled" placeholder="Area" required data-required-msg="Area is required"/>
            </div>
            <div>
                <label class="labelFormWeight">Subject<span style="color: red">*</span></label>

                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_subject" name="<%: this.ID %>_subject" data-bind="value: requestData.subject, enabled: isEnabled" placeholder="Subject" required data-required-msg="Subject is required" />
            </div>
            <div>
                <div id="view">
                    <%--<label class="labelFormWeight" style="width:80%;">Category of Work <i>(Please tick box)</i><span style="color: red">*</span></label>--%>
                    <%--<h3>Category of Work</h3>--%>
                    <fieldset class="gdiFieldset">
                        <legend class="gdiLegend">Category of Work <i>(Please tick box)</i><span style="color: red">*</span></legend>
                        <div class="k-form-field" id="<%: this.ID %>_categoryofwork">
                            <input type="checkbox" id="eqCat1" data-bind="checked: items" class="k-checkbox" value="safetyhazard">
                            <label class="k-checkbox-label" for="eqCat1" style="font-size: 10pt; padding-right: 20px;">Safety/Health Hazard</label>

                            <input type="checkbox" id="eqCat2" data-bind="checked: items" class="k-checkbox" value="productionloss">
                            <label class="k-checkbox-label" for="eqCat2" style="font-size: 10pt; padding-right: 20px;">Production Loss < 1 Year Payback (Please Justify)</label>

                            <input type="checkbox" id="eqCat3" data-bind="checked: items" class="k-checkbox" value="capitalplan">
                            <label class="k-checkbox-label" for="eqCat3" style="font-size: 10pt; padding-right: 20px;">Capital Plan</label>

                            <input type="checkbox" id="eqCat4" data-bind="checked: items" class="k-checkbox" value="environmental">
                            <label class="k-checkbox-label" for="eqCat4" style="font-size: 10pt; padding-right: 20px;">Environmental</label>

                            <input type="checkbox" id="eqCat5" data-bind="checked: items" class="k-checkbox" value="generalimprovement">
                            <label class="k-checkbox-label" for="eqCat5" style="font-size: 10pt; padding-right: 20px;">General Improvement and Others</label>
                         </div>
                    </fieldset>
                        <%--<ul class="fieldlist">
                            <li>
                                <input type="checkbox" id="eqCat1" data-bind="checked: items" class="k-checkbox" value="safetyhazard">
                                <label class="k-checkbox-label labelFormWeight" for="eqCat1" style="width:80%;">Safety/Health Hazard</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqCat2" data-bind="checked: items" class="k-checkbox" value="productionloss">
                                <label class="k-checkbox-label labelFormWeight" for="eqCat2" style="width:80%;">Production Loss < 1 Year Payback (Please Justify)</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqCat3" data-bind="checked: items" class="k-checkbox" value="capitalplan">
                                <label class="k-checkbox-label labelFormWeight" for="eqCat3" style="width:80%;">Capital Plan</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqCat4" data-bind="checked: items" class="k-checkbox" value="environmental">
                                <label class="k-checkbox-label labelFormWeight" for="eqCat4" style="width:80%;">Environmental</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqCat5" data-bind="checked: items" class="k-checkbox" value="generalimprovement">
                                <label class="k-checkbox-label labelFormWeight" for="eqCat5" style="width:80%;">General Improvement and Others</label>
                            </li>
                        </ul>--%>
                   
                </div>
            </div>
            <div>
                <label class="labelFormWeight">Account Code<span style="color: red">*</span></label>

                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_acccountCode" name="<%: this.ID %>_acccountCode" data-bind="value: requestData.accCode, enabled: isEnabled" placeholder="Account Code" required data-required-msg="Account Code is required" />
            </div>
            <div>
                <label class="labelFormWeight">Network No</label>

                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_networkNo" name="<%: this.ID %>_networkNo" data-bind="value: requestData.networkNo, enabled: isEnabled" placeholder="Network No" />
            </div>
            <div>
                <label class="labelFormWeight">Budget Allocated<span style="color: red">*</span></label>
                <input name="<%: this.ID %>_budgetAlloc" data-role="numerictextbox"
                    data-format="c"
                    data-min="0"
                    data-bind="value: requestData.budgetAlloc"
                    style="width: 180px" required data-required-msg="Budget Allocation is required">
            </div>
            <div>
                <div id="viewRequestedWork">
                    <%--<label class="labelFormWeight" style="width:80%;">Type of Requested Work <i>(Please tick box)</i><span style="color: red">*</span></label>--%>
                    <%--<h3>Type of Requested Work</h3>--%>
                    <fieldset class="gdiFieldset">
                        <legend class="gdiLegend">Type of Requested Work <i>(Please tick box)</i><span style="color: red">*</span></legend>
                        <div class="k-form-field" id="<%: this.ID %>_typeofrequestedwork">
                            <input type="checkbox" id="eqReqWork1" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="budgetestimate">
                            <label class="k-checkbox-label" for="eqReqWork1" style="font-size: 10pt; padding-right: 20px;">Budget Estimate</label>
                        
                            <input type="checkbox" id="eqReqWork2" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="purchaseonly">
                            <label class="k-checkbox-label" for="eqReqWork2" style="font-size: 10pt; padding-right: 20px;">Purchase Only</label>

                            <input type="checkbox" id="eqReqWork3" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="projectmanagement">
                            <label class="k-checkbox-label" for="eqReqWork3" style="font-size: 10pt; padding-right: 20px;">Project Management (Capital Only)</label>

                            <br />
                            <input type="checkbox" id="eqReqWork4" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="engineeringwork">
                            <label class="k-checkbox-label" for="eqReqWork4" style="font-size: 10pt; padding-right: 20px;">Engineering Work Package (Design and Drafting)</label>

                            <br />
                            <input type="checkbox" id="eqReqWork5" data-bind="checked: itemsRequestedWork, events: { change: onOtherChange }" class="k-checkbox" value="other">
                            <label class="k-checkbox-label" for="eqReqWork5" style="font-size: 10pt; padding-right: 20px;">Other (Please Specify)</label><br />
                            <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_otherRequestWork" name="<%: this.ID %>_otherRequestWork" data-bind="value: otherDescription, enabled: isOtherChecked" placeholder="Other" />
                        </div>
                    </fieldset>
                    <%--<ul class="fieldlist">
                        <li>
                            <input type="checkbox" id="eqReqWork1" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="budgetestimate">
                            <label class="k-checkbox-label labelForm" for="eqReqWork1" style="width:80%;">Budget Estimate</label>
                        </li>
                        <li>
                            <input type="checkbox" id="eqReqWork2" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="purchaseonly">
                            <label class="k-checkbox-label labelForm" for="eqReqWork2" style="width:80%;">Purchase Only</label>
                        </li>
                        <li>
                            <input type="checkbox" id="eqReqWork3" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="projectmanagement">
                            <label class="k-checkbox-label labelForm" for="eqReqWork3" style="width:80%;">Project Management (Capital Only)</label>
                        </li>
                        <li>
                            <input type="checkbox" id="eqReqWork4" data-bind="checked: itemsRequestedWork" class="k-checkbox" value="engineeringwork">
                            <label class="k-checkbox-label labelForm" for="eqReqWork4" style="width:80%;">Engineering Work Package (Design and Drafting)</label>
                        </li>
                        <li>
                            <input type="checkbox" id="eqReqWork5" data-bind="checked: itemsRequestedWork, events: { change: onOtherChange }" class="k-checkbox" value="other">
                            <label class="k-checkbox-label labelForm" for="eqReqWork5" style="width:80%;">Other (Please Specify)</label><br />
                            <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_otherRequestWork" name="<%: this.ID %>_otherRequestWork" data-bind="value: otherDescription, enabled: isOtherChecked" placeholder="Other" />
                        </li>

                    </ul>--%>

                </div>
            </div>
            <div>
                <label class="labelFormWeight">Date of Completion Required<span style="color: red">*</span></label>
               <%-- <input data-role="datepicker" id="<%: this.ID %>_dateOfCompletion" name="<%: this.ID %>_dateOfCompletion" 
                    data-format="dd MMM yyyy" data-bind="value: requestData.dateCompletion, enabled: isEnabled" 
                    placeholder="Date Completion" required data-required-msg="Date of Completion is required"/>--%>
                <input id="<%: this.ID %>_dateOfCompletion2" title="datepicker" required data-message="Date of Completion is required"/>
            </div>

            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Detail of Work Requirement</legend>
                    <div>
                        <label class="labelFormWeight">1. Problem Statement<span style="color: red">*</span></label>
                        <textarea name="<%: this.ID %>_problemStatement" class="k-textbox" style="width: 500px;" rows="4" data-bind="value: requestData.problemStatement, enabled: isEnabled" placeholder="Problem Statement" required data-required-msg="Problem Statement is required"></textarea>
                    </div>
                    <div>
                        <label class="labelFormWeight">2. Objective<span style="color: red">*</span></label>
                        <textarea name="<%: this.ID %>_objective" class="k-textbox" style="width: 500px;" rows="4" data-bind="value: requestData.objective, enabled: isEnabled" placeholder="Objective" required data-required-msg="Objective is required"></textarea>
                    </div>
                    <div>
                        <label class="labelFormWeight" style="width:80%;">3. How this EWR was initiated <i>(Please tick box)</i><span style="color: red">*</span></label>
                        <%--<label class="labelForm">How this EWR was initiated</label>--%>
                        <fieldset class="gdiFieldset">
                            <legend class="gdiLegend">&nbsp;</legend>
                            <div class="k-form-field" id="<%: this.ID %>_howewrinitiated">
                                <input type="checkbox" id="eqInitiated1" data-bind="checked: itemInitiate" class="k-checkbox" value="incident">
                                <label class="k-checkbox-label" for="eqInitiated1" style="font-size: 10pt; padding-right: 20px;">Incident</label>

                                <input type="checkbox" id="eqInitiated2" data-bind="checked: itemInitiate" class="k-checkbox" value="improvementidea">
                                <label class="k-checkbox-label" for="eqInitiated2" style="font-size: 10pt; padding-right: 20px;">Improvement Idea</label>

                                <input type="checkbox" id="eqInitiated3" data-bind="checked: itemInitiate" class="k-checkbox" value="hazardstudy">
                                <label class="k-checkbox-label" for="eqInitiated3" style="font-size: 10pt; padding-right: 20px;">Hazard Study</label>

                                <input type="checkbox" id="eqInitiated4" data-bind="checked: itemInitiate" class="k-checkbox" value="riskassessment">
                                <label class="k-checkbox-label" for="eqInitiated4" style="font-size: 10pt; padding-right: 20px;">Risk Assessment</label>
                            </div>
                        </fieldset>
                        <%--<ul class="fieldlist">
                            <li>
                                <input type="checkbox" id="eqInitiated1" data-bind="checked: itemInitiate" class="k-checkbox" value="incident">
                                <label class="k-checkbox-label labelForm" for="eqInitiated1" style="width:80%;">Incident</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqInitiated2" data-bind="checked: itemInitiate" class="k-checkbox" value="improvementidea">
                                <label class="k-checkbox-label labelForm" for="eqInitiated2" style="width:80%;">Improvement Idea</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqInitiated3" data-bind="checked: itemInitiate" class="k-checkbox" value="hazardstudy">
                                <label class="k-checkbox-label labelForm" for="eqInitiated3" style="width:80%;">Hazard Study</label>
                            </li>
                            <li>
                                <input type="checkbox" id="eqInitiated4" data-bind="checked: itemInitiate" class="k-checkbox" value="riskassessment">
                                <label class="k-checkbox-label labelForm" for="eqInitiated4" style="width:80%;">Risk Assessment</label>
                            </li>

                        </ul>--%>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td style="width: 10%"><label class="labelFormWeight">4. Data Supporting Document</label></td>
                                <td style="width: 50%"><input type="file" id="<%: this.ID %>_uploaderFile" name="<%: this.ID %>_uploaderFile" /></td>
                            </tr>
                        </table>
                    
                    
                    </div>
                </fieldset>
                
                
            </div>
            <div>
                <label class="labelFormWeight">Project Sponsor<span style="color: red">*</span></label>
                <input id="<%: this.ID %>_projectSponsor" name="<%: this.ID %>_projectSponsor" data-role="autocomplete"
                    data-placeholder="Type B/N or Name (min. 3 char)"
                    data-text-field="FULL_NAME"
                    data-min-length=3
                    data-filter="contains"
                    data-bind="value: requestData.projectSponsor,
                                source: dsEmployeeL2,
                                events: {
                                    select: onProjectSponsorSelected,
                                    close: onProjectSponsorClose,
                                    filtering: autocompleteFiltering
                                }"
                    style="width: 400px;"
                    required data-required-msg="Project Sponsor is required"
                />
                <%--<input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_projectSponsor" name="<%: this.ID %>_projectSponsor" data-bind="value: requestData.projectSponsor, enabled: isEnabled" placeholder="Project Sponsor" required />--%>
            </div>
            <div>
                <label class="labelFormWeight">Location<span style="color: red">*</span></label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_location" name="<%: this.ID %>_location" data-bind="value: requestData.sponsorLocation, enabled: isEnabled" placeholder="Location" required data-required-msg="Location is required"/>
            </div>
            <div>
                <label class="labelFormWeight">Phone No<span style="color: red">*</span></label>
                <input name="<%: this.ID %>_phoneNo" data-role="maskedtextbox"
                    data-mask="00000000000000"
                    data-clear-prompt-char="true"
                    data-bind="value: requestData.sponsorPhoneNo"
                    style="width: 200px;" required data-required-msg="Phone No is required"/>
                
            </div>
            <div>
                <label class="labelFormWeight">Operational Contact Person<span style="color: red">*</span></label>
                <%--<input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-bind="value: requestData.operationalCP, enabled: isEnabled" placeholder="Operational Contact Person" required />--%>
                <input id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-role="autocomplete"
                    data-placeholder="Type B/N or Name (min. 3 char)"
                    data-text-field="FULL_NAME"
                    data-min-length=3
                    data-filter="contains"
                    data-bind="value: requestData.operationalCP,
                                source: dsOperationalCP,
                                events: {
                                    select: onOperationalCPSelected,
                                    close: onOperationalCPClose,
                                    filtering: autocompleteFiltering
                                }"
                    style="width: 400px;"
                    required data-required-msg="Operational Contact Person is required"
                />
            </div>
            <div>
                <label class="labelFormWeight">Operational Contact Person 2</label>
                <%--<input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_operationalCP" name="<%: this.ID %>_operationalCP" data-bind="value: requestData.operationalCP, enabled: isEnabled" placeholder="Operational Contact Person" required />--%>
                <input id="<%: this.ID %>_operationalCP2" name="<%: this.ID %>_operationalCP2" data-role="autocomplete"
                    data-placeholder="Type B/N or Name (min. 3 char)"
                    data-text-field="FULL_NAME"
                    data-min-length=3
                    data-filter="contains"
                    data-bind="value: requestData.operationalCP2,
                                source: dsOperationalCP2,
                                events: {
                                    select: onOperationalCP2Selected,
                                    close: onOperationalCP2Close,
                                    filtering: autocompleteFiltering
                                }"
                    style="width: 400px;"
                />
            </div>
        </fieldset>
    </div>
    <br />
    <div id="<%: this.ID %>_create">
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onCloseClick }"
                class="k-button">
                <span class="k-icon k-i-cancel"></span>Close
            </div>
        </div>
        <div style="float: right;">&nbsp;</div>
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onDraftClick }"
                class="k-button k-primary">
                <span class="k-icon k-i-check"></span>Save as Draft
            </div>
        </div>
        <div style="float: right;">&nbsp;</div>
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onSubmitClick }"
                class="k-button k-primary">
                <span class="k-icon k-i-check"></span>Submit
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    <div id="<%: this.ID %>_update" style="display: none">
        <br />
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onCloseUpdateClick }"
                class="k-button">
                <span class="k-icon k-i-cancel"></span>Cancel
            </div>
        </div>
        <div style="float: right;">&nbsp;</div>
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onSubmitUpdateClick }"
                class="k-button k-primary">
                <span class="k-icon k-i-check"></span>Update
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<style>
    .fieldlist {
        margin: 0 0 -1em;
        padding: 0;
    }

    .fieldlist li {
        list-style: none;
        padding-bottom: 1em;
    }
</style>
