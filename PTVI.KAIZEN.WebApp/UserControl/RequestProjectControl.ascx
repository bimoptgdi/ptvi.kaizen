﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestProjectControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.RequestProjectControl" %>
<%@ Register Src="~/UserControl/PickEmployeesControl.ascx" TagPrefix="uc1" TagName="PickEmployeesControl" %>

<script src="Scripts/common/common_grid_editor_component.js"></script>
<script src="Scripts/common/common_model.js"></script>
<script>
    // Const
    var <%: this.ID %>_kendoUploadButton;
    var <%: this.ID %>_idProject = "";
    // Variable

    var <%: this.ID %>_popPickOwner,
        <%: this.ID %>_popPickSponsor,
        <%: this.ID %>_popPickPM,
        <%: this.ID %>_popPickPE,
        <%: this.ID %>_popPickMaintenance,
        <%: this.ID %>_popPickOperation;
    // View Model
    var <%: this.ID %>_viewModel = kendo.observable({
        requestData: new common_projectModel(),
        isOwnerNameDisabled: true,
        isSponsorNameDisabled: true,
        isPMNameDisabled: true,
        isPENameDisabled: true,
        isMaintenanceNameDisabled: true,
        isOperationNameDisabled: true,
        isEnabled: true,
        isAvailToIntegrated: true,
        integratedStatus: "",
        btnIntegrate: "Integrate",
        getEmployeeFromServer: function (options) {
            var searchBadgeNo = "";
            if (options.data.filter.filters.length>0)
                searchBadgeNo = options.data.filter.filters[0].value;
            var result = [];
            if (searchBadgeNo && searchBadgeNo.length >= 3) {
                //var filter = "?$filter=substringof('" + searchBadgeNo + "',employeeId) or substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var filter = "?$filter=substringof(tolower('" + searchBadgeNo + "'),tolower(full_Name))";
                var result = [];
                $.ajax({
                    url: _webOdataUrl + "EMPLOYEEs" + filter,
                    type: "GET",
                    async: false,
                    success: function (data) {
                        result = data.value;
                    },
                    error: function (data) {
                        //return [];
                    }
                });
            }
            return result;
        },
        projectCategoryList: [],
        momewrid: null,
        employeeOwnerSelected: null,
        dsEmployeeOwner: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangeEWR: function (e) {
            //this.requestData.set("momewrid", this.momewrid ? this.momewrid : null);
        },
        onChangeOwner: function (e) {
            this.requestData.set("ownerId", this.employeeOwnerSelected ? this.employeeOwnerSelected.employeeId : null);
            this.requestData.set("ownerName", this.employeeOwnerSelected ? this.employeeOwnerSelected.full_Name : null);
            this.requestData.set("ownerEmail", this.employeeOwnerSelected ? this.employeeOwnerSelected.email : null);
        },
        employeeSponsorSelected: null,
        dsEmployeeSponsor: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangeSponsor: function (e) {
            this.requestData.set("sponsorId", this.employeeSponsorSelected ? this.employeeSponsorSelected.employeeId : null);
            this.requestData.set("sponsorName", this.employeeSponsorSelected ? this.employeeSponsorSelected.full_Name : null);
            this.requestData.set("sponsorEmail", this.employeeSponsorSelected ? this.employeeSponsorSelected.email : null);
        },
        employeePMSelected: null,
        dsEmployeePM: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangePM: function (e) {
            this.requestData.set("projectManagerId", this.employeePMSelected ? this.employeePMSelected.employeeId : null);
            this.requestData.set("projectManagerName", this.employeePMSelected ? this.employeePMSelected.full_Name : null);
            this.requestData.set("projectManagerEmail", this.employeePMSelected ? this.employeePMSelected.email : null);
        },
        employeePESelected: null,
        dsEmployeePE: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangePE: function (e) {
            this.requestData.set("projectEngineerId", this.employeePESelected ? this.employeePESelected.employeeId : null);
            this.requestData.set("projectEngineerName", this.employeePESelected ? this.employeePESelected.full_Name : null);
            this.requestData.set("projectEngineerEmail", this.employeePESelected ? this.employeePESelected.email : null);
        },
        employeeMaintenanceSelected: null,
        dsEmployeeMaintenance: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangeMaintenance: function (e) {
            this.requestData.set("maintenanceRepId", this.employeeMaintenanceSelected ? this.employeeMaintenanceSelected.employeeId : null);
            this.requestData.set("maintenanceRepName", this.employeeMaintenanceSelected ? this.employeeMaintenanceSelected.full_Name : null);
            this.requestData.set("maintenanceRepEmail", this.employeeMaintenanceSelected ? this.employeeMaintenanceSelected.email : null);
        },
        employeeOperationSelected: null,
        dsEmployeeOperation: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangeOperation: function (e) {
            this.requestData.set("operationRepId", this.employeeOperationSelected ? this.employeeOperationSelected.employeeId : null);
            this.requestData.set("operationRepName", this.employeeOperationSelected ? this.employeeOperationSelected.full_Name : null);
            this.requestData.set("operationRepEmail", this.employeeOperationSelected ? this.employeeOperationSelected.email : null);
        },
        dsAreaInvolved: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "areaInvolveds/areaInvolvedInActive/PROJECT"
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: {},
                        areaName: {}
                    }
                },
                parse: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        if (response[i].areaDesc)
                            response[i].textAll = response[i].areaName + " - " + response[i].areaDesc;
                        else
                            response[i].textAll = response[i].areaName;
                    }
                    return response;
                }
            }
        }),
        dsProjectType: [
            { text: "Capital", value: "CAPITAL" },
            { text: "Operating", value: "OPERATING" }
        ],
        dsProjectCategory: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "MasterProjectCategories"
            },
            schema: {
                model: {
                    id: "id"
                }
            }
        }),
        //dsProjectCategory: new kendo.data.DataSource({
        //    transport: {
        //        read: _webApiUrl + "EWRREQUESTs"
        //    },
        //    schema: {
        //        model: {
        //            id: "id"
        //        }
        //    }
        //}),
        onWindowOpen: function (e) {
            e.sender.center();
        },
        onValidationEntry: function () {
            var validatable = $("#RequestContent").kendoValidator().data("kendoValidator");

            var isValid = true;

            if (validatable.validate() == false) {
                // get the errors and write them out to the "errors" html container
                var errors = validatable.errors();
                //$(errors).each(function () {
                //    $("#errors").html(this);
                //});


                _showDialogMessage("Required Message", "Please fill all the required input", "");
                // get the errors and write them out to the "errors" html container
                isValid = false;
            }
            else if (!this.requestData.get("ownerId")) {
                _showDialogMessage("Owner", "Please select the Owner!", "");
                isValid = false;
            }
            else if (!this.requestData.get("sponsorId")) {
                _showDialogMessage("Sponsor", "Please select the Sponsor!", "");
                isValid = false;
            }
            else if (!this.requestData.get("projectManagerId")) {
                _showDialogMessage("Project Manager", "Please select the Project Manager!", "");
                isValid = false;
            }
            else if (!this.requestData.get("projectEngineerId")) {
                _showDialogMessage("Project Engineer", "Please select the Project Engineer!", "");
                isValid = false;
            }
            //else if (!this.requestData.get("maintenanceRepId")) {
            //    _showDialogMessage("Maintenance Report", "Please select the Maintenance Report!", "");
            //    isValid = false;
            //}
            //else if (!this.requestData.get("operationRepId")) {
            //    _showDialogMessage("Operation", "Please select the Operation!", "");
            //    isValid = false;
            //}
            else if (!this.get("projectCategoryList")) {
                _showDialogMessage("Project Type", "Please select the Project Category!", "");
                isValid = false;
            }
            else if (!this.requestData.get("projectType")) {
                _showDialogMessage("Project Type", "Please select the Project Type!", "");
                isValid = false;
            };
            //else if (this.requestData.get("projectType") == "CAPITAL" && !this.requestData.get("pmoNo")) {
            //    _showDialogMessage("Project Type", "Please input the PMO No!", "");
            //    isValid = false;
            //};
            return isValid;
        },
        onSubmitClick: function (e) {
            if (this.onValidationEntry()) {
                var otherPositions = $("#RequestProject_grdOhtePosition").data("kendoGrid").dataSource.data().toJSON();
                var requestData =
                {
                    id: this.requestData.id,
                    projectNo: this.requestData.projectNo,
                    projectDescription: this.requestData.projectDescription,
                    ownerId: this.requestData.ownerId,
                    ownerName: this.requestData.ownerName,
                    ownerEmail: this.requestData.ownerEmail,
                    sponsorId: this.requestData.sponsorId,
                    sponsorName: this.requestData.sponsorName,
                    sponsorEmail: this.requestData.sponsorEmail,
                    projectManagerId: this.requestData.projectManagerId,
                    projectManagerName: this.requestData.projectManagerName,
                    projectManagerEmail: this.requestData.projectManagerEmail,
                    projectEngineerId: this.requestData.projectEngineerId,
                    projectEngineerName: this.requestData.projectEngineerName,
                    projectEngineerEmail: this.requestData.projectEngineerEmail,
                    maintenanceRepId: this.requestData.maintenanceRepId,
                    maintenanceRepName: this.requestData.maintenanceRepName,
                    maintenanceRepEmail: this.requestData.maintenanceRepEmail,
                    operationRepId: this.requestData.operationRepId,
                    operationRepName: this.requestData.operationRepName,
                    operationRepEmail: this.requestData.operationRepEmail,
                    areaInvolvedId: this.requestData.areaInvolvedId,
                    projectCategory: this.projectCategoryList.join("|"),
                    projectType: this.requestData.projectType,
                    pmoNo: this.requestData.pmoNo,
                    planStartDate: kendo.toString(this.requestData.planStartDate, 'yyyy-MM-dd'),
                    planFinishDate: kendo.toString(this.requestData.planFinishDate, 'yyyy-MM-dd'),
                    actualStartDate: kendo.toString(this.requestData.actualStartDate, 'yyyy-MM-dd'),
                    actualFinishDate: kendo.toString(this.requestData.actualFinishDate, 'yyyy-MM-dd'),
                    totalCost: this.requestData.totalCost,
                    iptProjectId: this.requestData.iptProjectId,
                    shareFolderDoc: this.requestData.shareFolderDoc,
                    otherPositions: otherPositions,
                    momewrid: this.requestData.momewrid
                };

                $.ajax({
                    url: _webApiUrl + "Project/SubmitRequest/1",
                    type: "POST",
                    data: requestData,
                    success: function (data) {


                        if (<%: this.ID %>_kendoUploadButton) {
                            <%: this.ID %>_kendoUploadButton.click();
                        } else {
                            _showDialogMessage("Submit Request", "Request has been submitted", "HomeForm.aspx");
                        }
                        <%: this.ID %>_viewModel.onClearEvent(data);
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status == 400) {

                            aaa = jqXHR;
                            if (jqXHR.responseJSON == "duplicateNo") {
                                _showDialogMessage("Error Message", "Cannot Duplicate Project No!", "");
                            } else
                            {
                                _showDialogMessage("Error Message", "Cannot Duplicate Project Title!", "");
                            }

                        } else {
                            _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                        }
                    }
                });
            }


        },
        onIntegrateClick: function (e) {
            <%: this.ID %>_viewModel.set("btnIntegrate", "Integrating...");                
            if (!this.requestData.get("projectDescription")) {
                _showDialogMessage("Project Title", "Please input the Project Title first!", "");
                isValid = false;
            }
            else {
                var isConfirm = true;
                if (this.requestData.iptProjectId != null && this.requestData.iptProjectId != '') {
                    if (!confirm('Are you sure to re-integrate to Project "' + this.requestData.get("projectDescription") + '" ?')) {
                        isConfirm = false;
                    }
                }

                if (isConfirm == true) {
                    $.ajax({
                        url: _webApiUrl + "IPTProject/SysInfo/" + this.requestData.get("projectDescription"),
                        type: "GET",
                        success: function (result) {
                            if (result) {
                                <%: this.ID %>_viewModel.requestData.set("iptProjectId", result.guid);
                                _showDialogMessage("IPT Integration Succeed", "IPT Integration has been Succeed", "");
                                <%: this.ID %>_viewModel.set("isAvailToIntegrated", true);
                                <%: this.ID %>_viewModel.set("btnIntegrate", "Re-Integrate");

                                <%: this.ID %>_viewModel.set("integratedStatus", "(Integrated)");
                            }
                            else {
                                _showDialogMessage("IPT Integration Failed", "Project Title Not found!, Please Input Correct Project Title", "");
                                <%: this.ID %>_viewModel.set("isAvailToIntegrated", true);

                                <%: this.ID %>_viewModel.set("integratedStatus", "");
                                <%: this.ID %>_viewModel.set("btnIntegrate", "Integrate");
                            }
                        },
                        error: function (jqXHR) {
                        }
                    });
                }
            }
        },
        onCloseClick: function (e) {
            this.onClearEvent(e);

            _showDialogMessage("Close Form", "Close Request Form", "HomeForm.aspx");

        },
        onSubmitUpdateClick: function (e) {
            if (this.onValidationEntry()) {
                kendo.ui.progress($("#RequestContent"), true);

                //data other position
                var otherPositions = $("#RequestProject_grdOhtePosition").data("kendoGrid").dataSource.data().toJSON();
                var requestData =
                {
                    id: this.requestData.id,
                    projectNo: this.requestData.projectNo,
                    projectDescription: this.requestData.projectDescription,
                    ownerId: this.requestData.ownerId,
                    ownerName: this.requestData.ownerName,
                    ownerEmail: this.requestData.ownerEmail,
                    sponsorId: this.requestData.sponsorId,
                    sponsorName: this.requestData.sponsorName,
                    sponsorEmail: this.requestData.sponsorEmail,
                    projectManagerId: this.requestData.projectManagerId,
                    projectManagerName: this.requestData.projectManagerName,
                    projectManagerEmail: this.requestData.projectManagerEmail,
                    projectEngineerId: this.requestData.projectEngineerId,
                    projectEngineerName: this.requestData.projectEngineerName,
                    projectEngineerEmail: this.requestData.projectEngineerEmail,
                    maintenanceRepId: this.requestData.maintenanceRepId,
                    maintenanceRepName: this.requestData.maintenanceRepName,
                    maintenanceRepEmail: this.requestData.maintenanceRepEmail,
                    operationRepId: this.requestData.operationRepId,
                    operationRepName: this.requestData.operationRepName,
                    operationRepEmail: this.requestData.operationRepEmail,
                    areaInvolvedId: this.requestData.areaInvolvedId,
                    projectCategory: this.projectCategoryList.join("|"),
                    projectType: this.requestData.projectType,
                    pmoNo: this.requestData.pmoNo,
                    planStartDate: kendo.toString(this.requestData.planStartDate, 'yyyy-MM-dd'),
                    planFinishDate: kendo.toString(this.requestData.planFinishDate, 'yyyy-MM-dd'),
                    actualStartDate: kendo.toString(this.requestData.actualStartDate, 'yyyy-MM-dd'),
                    actualFinishDate: kendo.toString(this.requestData.actualFinishDate, 'yyyy-MM-dd'),
                    totalCost: this.requestData.totalCost,
                    iptProjectId: this.requestData.iptProjectId,
                    shareFolderDoc: this.requestData.shareFolderDoc,
                    otherPositions: otherPositions,
                    momewrid: this.requestData.momewrid
            };
                $.ajax({
                    url: _webApiUrl + "Project/UpdateRequest/" + this.requestData.id,
                    type: "POST",
                    data: requestData,
                    success: function (data) {
                        kendo.ui.progress($("#RequestContent"), false);

                        _showDialogMessage("Update Completed", "Data Updated Successfully", "");
                        <%: this.ID %>_viewModel.onClearEvent(data);
                    },
                    error: function (jqXHR) {
                        kendo.ui.progress($("#RequestContent"), false);
                        if (jqXHR.responseJSON.InnerException.InnerException.ExceptionMessage.indexOf("Cannot insert duplicate key row in object 'dbo.PROJECT' with unique index 'UNIQUE_NO_PROJECT'")>=0) {
                            _showDialogMessage("Error Message", "Project No. Exist!", "");
                        } else
                        _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input the request again", "");
                    }
                });

                this.onRefreshGridMaster();
                this.onCloseUpdateClick();
            }
        },
        onCloseUpdateClick: function (e) {

        },
        onRefreshGridMaster: function (e) {

        },
        onPickOwnerClick: function (e) {
            PickOwnerControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {

                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.requestData.set("ownerId", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.requestData.set("ownerName", pickedEmployee.full_Name);
                <%: this.ID %>_viewModel.requestData.set("ownerEmail", pickedEmployee.email);

                if (PickOwnerControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                    $.ajax({
                        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                        type: "GET",
                        async: false,
                        success: function (result) {
                            if (result.length != 0) {
                                <%: this.ID %>_viewModel.requestData.set("ownerName", result[0].FULL_NAME);
                                <%: this.ID %>_viewModel.requestData.set("ownerEmail", result[0].EMAIL);

                            }
                        },
                        error: function (data) {
                        }
                    });
                }

                <%: this.ID %>_popPickOwner.close();
            });
            PickOwnerControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popPickOwner.close();
            });
            // show popup
            <%: this.ID %>_popPickOwner.center().open();
            PickOwnerControl_DataBind();
        },
        onPickSponsorClick: function (e) {
            PickSponsorControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.requestData.set("sponsorId", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.requestData.set("sponsorName", pickedEmployee.full_Name);
                <%: this.ID %>_viewModel.requestData.set("sponsorEmail", pickedEmployee.email);

                if (PickSponsorControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                    $.ajax({
                        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                        type: "GET",
                        async: false,
                        success: function (result) {
                            if (result.length != 0) {
                                <%: this.ID %>_viewModel.requestData.set("sponsorName", result[0].FULL_NAME);
                                <%: this.ID %>_viewModel.requestData.set("sponsorEmail", result[0].EMAIL);

                            }
                        },
                        error: function (data) {
                        }
                    });
                }

                <%: this.ID %>_popPickSponsor.close();
            });
            PickSponsorControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popPickSponsor.close();
            });
            // show popup
            <%: this.ID %>_popPickSponsor.center().open();
            PickSponsorControl_DataBind();
        },
        onPickPMClick: function (e) {
            PickPMControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.requestData.set("projectManagerId", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.requestData.set("projectManagerName", pickedEmployee.full_Name);
                <%: this.ID %>_viewModel.requestData.set("projectManagerEmail", pickedEmployee.email);

                if (PickPMControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                    $.ajax({
                        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                        type: "GET",
                        async: false,
                        success: function (result) {
                            if (result.length != 0) {
                                <%: this.ID %>_viewModel.requestData.set("projectManagerName", result[0].FULL_NAME);
                                <%: this.ID %>_viewModel.requestData.set("projectManagerEmail", result[0].EMAIL);

                            }
                        },
                        error: function (data) {
                        }
                    });
                }
                <%: this.ID %>_popPickPM.close();
            });
            PickPMControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popPickPM.close();
            });
            // show popup
            <%: this.ID %>_popPickPM.center().open();
            PickPMControl_DataBind();
        },
        onPickPEClick: function (e) {
            PickPEControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.requestData.set("projectEngineerId", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.requestData.set("projectEngineerName", pickedEmployee.full_Name);
                <%: this.ID %>_viewModel.requestData.set("projectEngineerEmail", pickedEmployee.email);
                               
                if (PickPEControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                    $.ajax({
                        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                        type: "GET",
                        async: false,
                        success: function (result) {
                            if (result.length != 0) {
                                <%: this.ID %>_viewModel.requestData.set("projectEngineerName", result[0].FULL_NAME);
                                <%: this.ID %>_viewModel.requestData.set("projectEngineerEmail", result[0].EMAIL);

                            }
                        },
                        error: function (data) {
                        }
                    });
                }

                <%: this.ID %>_popPickPE.close();
            });
            PickPEControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popPickPE.close();
            });
            // show popup
            <%: this.ID %>_popPickPE.center().open();
            PickPEControl_DataBind();
        },
        onPickMaintenanceClick: function (e) {
            PickMaintenanceControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.requestData.set("maintenanceRepId", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.requestData.set("maintenanceRepName", pickedEmployee.full_Name);
                <%: this.ID %>_viewModel.requestData.set("maintenanceRepEmail", pickedEmployee.email);

                if (PickMaintenanceControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                    $.ajax({
                        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                        type: "GET",
                        async: false,
                        success: function (result) {
                            if (result.length != 0) {
                                <%: this.ID %>_viewModel.requestData.set("maintenanceRepName", result[0].FULL_NAME);
                                <%: this.ID %>_viewModel.requestData.set("maintenanceRepEmail", result[0].EMAIL);

                            }
                        },
                        error: function (data) {
                        }
                    });
                }

                <%: this.ID %>_popPickMaintenance.close();
            });
            PickMaintenanceControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popPickMaintenance.close();
            });
            // show popup
            <%: this.ID %>_popPickMaintenance.center().open();
            PickMaintenanceControl_DataBind();
        },
        onPickOperationClick: function (e) {
            PickOperationControl_pickEmployeeViewModel.set("pickHandlerEmployee", function (e) {
                var pickedEmployee = (this.dataItem($(e.currentTarget).closest("tr")));
                <%: this.ID %>_viewModel.requestData.set("operationRepId", pickedEmployee.employeeId);
                <%: this.ID %>_viewModel.requestData.set("operationRepName", pickedEmployee.full_Name);
                <%: this.ID %>_viewModel.requestData.set("operationRepEmail", pickedEmployee.email);
               
                if (PickOperationControl_pickEmployeeViewModel.employeeTypeSelected == "contractor") {
                    $.ajax({
                        url: _webApiUrlEllipse + "externaldata/executequerybyparam/QRY_ADLIST_EX_IN|@badgenos='" + pickedEmployee.id + "'",
                        type: "GET",
                        async: false,
                        success: function (result) {
                            if (result.length != 0) {
                                <%: this.ID %>_viewModel.requestData.set("operationRepName", result[0].FULL_NAME);
                                <%: this.ID %>_viewModel.requestData.set("operationRepEmail", result[0].EMAIL);

                            }
                        },
                        error: function (data) {
                        }
                    });
                }

                <%: this.ID %>_popPickOperation.close();
            });
            PickOperationControl_pickEmployeeViewModel.set("onCloseSelectEvent", function (data) {
                <%: this.ID %>_popPickOperation.close();
            });
            // show popup
            <%: this.ID %>_popPickOperation.center().open();
            PickOperationControl_DataBind();
        },
        onClearEvent: function (e) {
            this.requestData.set("projectNo", null);
            this.requestData.set("projectDescription", null);
            this.requestData.set("ownerId", null);
            this.requestData.set("ownerName", null);
            this.requestData.set("ownerEmail", null);
            this.requestData.set("sponsorId", null);
            this.requestData.set("sponsorName", null);
            this.requestData.set("sponsorEmail", null);
            this.requestData.set("projectManagerId", null);
            this.requestData.set("projectManagerName", null);
            this.requestData.set("projectManagerEmail", null);
            this.requestData.set("projectEngineerId", null);
            this.requestData.set("projectEngineerName", null);
            this.requestData.set("projectEngineerEmail", null);
            this.requestData.set("maintenanceRepId", null);
            this.requestData.set("maintenanceRepName", null);
            this.requestData.set("maintenanceRepEmail", null);
            this.requestData.set("operationRepId", null);
            this.requestData.set("operationRepName", null);
            this.requestData.set("operationRepEmail", null);
            this.requestData.set("areaInvolvedId", null);
            this.requestData.set("projectCategory", null);
            this.requestData.set("projectType", null);
            this.requestData.set("pmoNo", null);
            this.requestData.set("planStartDate", null);
            this.requestData.set("planFinishDate", null);
            this.requestData.set("actualStartDate", null);
            this.requestData.set("actualFinishDate", null);
            this.requestData.set("totalCost", null);
            this.requestData.set("iptProjectId", null);
            this.requestData.set("shareFolderDoc", null);
            this.dsOtherPositionGetData.read();
        },
        dsLookupOtherPositionProject: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "<%: WebApiUrl %>" + "lookup/FindByTypeAndValue/otherPositionProject" // type = otherPositionProject
                }
            },
            schema: {
                model: {
                    id: "value"
                }
            }
        }),
        dsEWR: new kendo.data.DataSource({
            requestStart: function () {
            },
            requestEnd: function (e) {
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function(e){
                        return _webApiUrl + "MOMEWRs/getMOMEWRList/"+ <%: this.ID %>_viewModel.requestData.id
                        },
                    dataType: "json"
                },
            },
            sort: [{ field: "ewrTitle", dir: "asc" }]
        }),
        dsOtherPositionGetData: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdOhtePosition"), true);
            },
            requestEnd: function (e) {
                <%:this.ID%>_viewModel.dsOtherPosition.data(e.response);

                kendo.ui.progress($("#<%: this.ID %>_grdOhtePosition"), false);
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            transport: {
                read: {
                    url: function () {
                        var projectId = <%:this.ID%>_viewModel.requestData.id || <%:this.ID%>_viewModel.requestData.id == ""  ? 0 : _projectId;
                        return _webApiUrl + "UserAssignment/otherPostionOnProject/" + projectId
                    },
                    dataType: "json"
                },
                create: function (options) {
                    options.success();
                },
                update: function (options) {
                    options.success();

                }
            },
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        dsOtherPosition: new kendo.data.DataSource({
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { type: 'number', defaultValue: 0 }
                    },
                    assignmentName: function () {
                        if (this.assignmentType)
                            return <%: this.ID %>_viewModel.dsLookupOtherPositionProject.get(this.assignmentType).text;
                        else return "";
                    }
                }
            },
            data: [],
            pageSize: 10,
            sort: [{ field: "id", dir: "asc" }]
        }),
        employeeOtherPositionSelected: null,
        dsEmployeeOtherPosition: new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: function (options) {
                        options.success(<%: this.ID %>_viewModel.getEmployeeFromServer(options));
                }
            }
        }),
        onChangeOtherPosition: function (e) {
            return {
                employeeId: this.employeeOperationSelected ? this.employeeOtherPositionSelected.employeeId : null,
                employeeName: this.employeeOperationSelected ? this.employeeOtherPositionSelected.full_Name : null,
                employeeEmail: this.employeeOperationSelected ? this.employeeOtherPositionSelected.email : null,
                employeePosition: this.employeeOperationSelected ? this.employeeOtherPositionSelected.positionId : null
        }
        },
        autocompleteOtherPositionChange: function (e) {
            if (!<%: this.ID %>_viewModel.employeeOtherPositionSelected.employeeId)
            {
                <%: this.ID %>_viewModel.set("employeeOtherPositionSelected", null);
            }
        },
        editOtherPosition: function (e) {
            var that = this;
            //e.model.set("dirty", true);
            if (e.model.isNew() && e.model.employeeId == null) {
                $("#<%: this.ID %>_employeeOtherPositionTmp").closest('.k-widget.k-window').find(".k-window-title").html("Add Employee");
                $(".k-grid-update").html('<span class="k-icon k-i-check"></span>Save');
                $("#<%: this.ID %>_employeeOtherPositionTmp").data("kendoAutoComplete").value("");

                var autocomplete = $("#<%: this.ID %>_employeeOtherPositionTmp").data("kendoAutoComplete");
                autocomplete.bind("change", that.autocompleteOtherPositionChange);

            }
            else
                $("#RequestProject_employeeOtherPositionTmp").closest('.k-widget.k-window').find(".k-window-title").html("Edit Employee");

            <%--            $("#<%: this.ID %>_employeeOtherPositionTmp").getKendoAutoComplete().bind("change", function () {
                that.onChangeOtherPosition();
            });
--%>

            that.set("employeeOtherPositionSelected", {
                employeeId: e.model.employeeId,
                full_Name: e.model.employeeName,
                email: e.model.employeeEmail,
                positionId: e.model.employeePosition
            });

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
        },
        saveOtherPosition: function (e) {
            //ternyata set bikin error
            //e.model.set("employeeId", this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.employeeId : null);
            //e.model.set("employeeName", this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.full_Name : null);
            //e.model.set("employeeEmail", this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.email : null);
            //e.model.set("employeePosition", this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.positionId : null);

            //harusnya begini
            e.model.employeeId = this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.employeeId : null;
            e.model.employeeName = this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.full_Name : null;
            e.model.employeeEmail = this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.email : null;
            e.model.employeePosition = this.employeeOtherPositionSelected ? this.employeeOtherPositionSelected.positionId : null;

        },
        cancelOtherPosition: function (e) {
           <%-- $("#<%: this.ID %>_grdOhtePosition").data("kendoGrid").dataSource.cancelChanges();
            e.preventDefault();--%>
        }
    });

    function <%: this.ID %>_budget() {
        alert("apa saja");
    }

    function <%: this.ID %>_checkAccessRight() {
        var tmpProjectId = <%: this.ID %>_viewModel.requestData.id ? <%: this.ID %>_viewModel.requestData.id : 0;
        if (tmpProjectId != 0) {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);
                        <%: this.ID %>_viewModel.set("isAvailToIntegrated", false);
                        <%: this.ID %>_viewModel.set("integratedStatus", "(Integrated)");
                    } else {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').show();
                        if ($('input:radio[name="projectType"]:checked').val() == 'OPERATING') {
                            // menampilkan note
                            $("#budgetColumn").show();
                        } else {
                            $("#budgetColumn").hide();

                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);                        
                    }
                },
                error: function (jqXHR) {
                }
            });
        } else {
            $.ajax({
                url: _webApiUrl + "project/IsReadonlyProjectTo/" + tmpProjectId + "?badgeNo=" + _currBadgeNo + "&accessrights=<%: (int)PTVI.iPROM.WebApp.AccessRights.InputProject%> ",
                type: "GET",
                success: function (result) {
                    if (result) {
                        $('#<%: this.ID %>_create').hide();
                        $('#<%: this.ID %>_update').hide();
                        <%: this.ID %>_viewModel.set("isEnabled", false);
                        <%: this.ID %>_viewModel.set("isAvailToIntegrated", false);
                        <%: this.ID %>_viewModel.set("integratedStatus", "(Integrated)");
                    } else {
                        if (<%: this.ID %>_viewModel.requestData.id) {
                            $('#<%: this.ID %>_create').hide();
                            $('#<%: this.ID %>_update').show();
                            if ($('input:radio[name="projectType"]:checked').val() == 'OPERATING') {
                                // menampilkan note
                                $("#budgetColumn").show();
                            } else {
                                $("#budgetColumn").hide();

                            }
                        } else {
                            $('#<%: this.ID %>_create').show();
                            $('#<%: this.ID %>_update').hide();
                        }
                        <%: this.ID %>_viewModel.set("isEnabled", true);
                    }
                },
                error: function (jqXHR) {
                }
            });
        }
    }

    // Document Ready
    $(document).ready(function () {

       

        <%: this.ID %>_checkAccessRight();

        // Form Bind
        kendo.bind($("#RequestContent"), <%: this.ID %>_viewModel);
        $('input:radio[name="projectType"]').change(
      function () {
          if ($(this).is(':checked') && $(this).val() == 'OPERATING') {
              // menampilkan note
              $("#budgetColumn").show();
          } else {
              $("#budgetColumn").hide();

          }
      });

        $(document).on('keydown', '#<%: this.ID %>_projectNo', function (e) {
            if (e.keyCode == 32) return false;
        });

        <%: this.ID %>_popPickOwner = $("#<%: this.ID %>_popPickOwner").kendoWindow({
            title: "Owner List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        <%: this.ID %>_popPickSponsor = $("#<%: this.ID %>_popPickSponsor").kendoWindow({
            title: "Sponsor List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        <%: this.ID %>_popPickPM = $("#<%: this.ID %>_popPickPM").kendoWindow({
            title: "PM List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        <%: this.ID %>_popPickPE = $("#<%: this.ID %>_popPickPE").kendoWindow({
            title: "PE List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        <%: this.ID %>_popPickMaintenance = $("#<%: this.ID %>_popPickMaintenance").kendoWindow({
            title: "Maintenance List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        <%: this.ID %>_popPickOperation = $("#<%: this.ID %>_popPickOperation").kendoWindow({
            title: "Operation List",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 450
        }).data("kendoWindow");

        $("input[name='projectType']").on("change", function () {
            if (<%: this.ID %>_viewModel.requestData.projectType == "CAPITAL") {
//                $("#pmoNo").attr('required', 'required');
                $("#pmoNo").removeAttr('required');

            }
            else {
                $("#pmoNo").removeAttr('required');
            }
        });

        //Start Validation Plan Date
        $("#<%: this.ID %>_psd").getKendoDatePicker().bind("change", function () {
            $("#<%: this.ID %>_pfd").getKendoDatePicker().min(this.value());
        });

        $("#<%: this.ID %>_pfd").getKendoDatePicker().bind("change", function () {
            $("#<%: this.ID %>_psd").getKendoDatePicker().max(this.value());
        });

        if (<%: this.ID %>_viewModel.requestData.get("planStartDate") && <%: this.ID %>_viewModel.requestData.get("id"))
            $("#<%: this.ID %>_psd").getKendoDatePicker().max(<%: this.ID %>_viewModel.requestData.planFinishDate);
        if (<%: this.ID %>_viewModel.requestData.get("planFinishDate") && <%: this.ID %>_viewModel.requestData.get("id"))
            $("#<%: this.ID %>_pfd").getKendoDatePicker().min(<%: this.ID %>_viewModel.requestData.planStartDate);
        //End Validation Plan Date

        //Start Validation Actual Date
        $("#<%: this.ID %>_asd").getKendoDatePicker().bind("change", function () {
            $("#<%: this.ID %>_afd").getKendoDatePicker().min(this.value());
        });

        $("#<%: this.ID %>_afd").getKendoDatePicker().bind("change", function () {
            $("#<%: this.ID %>_asd").getKendoDatePicker().max(this.value());
        });

        if (<%: this.ID %>_viewModel.requestData.get("actualStartDate") && <%: this.ID %>_viewModel.requestData.get("id")) {
            console.log(<%: this.ID %>_viewModel.requestData.get("actualStartDate"));
            $("#<%: this.ID %>_asd").getKendoDatePicker().max(<%: this.ID %>_viewModel.requestData.planFinishDate);
        }
        if (<%: this.ID %>_viewModel.requestData.get("actualFinishDate") && <%: this.ID %>_viewModel.requestData.get("id"))
            $("#<%: this.ID %>_afd").getKendoDatePicker().min(<%: this.ID %>_viewModel.requestData.planStartDate);
        //End Validation Actual Date


        <%: this.ID %>_viewModel.onClearEvent()
        $("#RequestContent").kendoTooltip({
            filter: "#<%: this.ID %>_shareFolderDoc",
            position: "top",
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500
                },
                close: {
                    effects: "fade:out",
                    duration: 500
                },
            },
            content: function (e) {
                var content = "Format Share Folder, e.g.: \\\\netapp8\\folder";
                return content;
            }
        }).data("kendoTooltip");
    
    }); //doc ready

</script>
<div id="RequestContent" class="k-content wide">
    <div>
        <fieldset class="gdiFieldset">
            <legend class="gdiLegend" id="<%: this.ID %>_inputProjectLegend">Input Project</legend>
            <div>
                <label class="labelForm">Project No<span style="color:red">*</span></label>
                <input class="k-textbox" type="text" id="<%: this.ID %>_projectNo" name="<%: this.ID %>_projectNo" data-bind="value: requestData.projectNo, enabled: isEnabled" placeholder="Project No" required validationMessage="Project No is required" />
                <label class="labelForm" style="width: 50px">EWR No</label>
                <input class="k-textbox" style="width: 300px" type="text" id="<%: this.ID %>_ewrNo" name="<%: this.ID %>_ewrNo" 
                    data-role="dropdownlist" 
                    data-value-field="id" 
                    data-text-field="ewrTitle" 
                    data-value-primitive=true
                    data-bind="source: dsEWR, value: requestData.momewrid, enabled: isEnabled, events: { change: onChangeEWR }" placeholder="EWR No" />
            </div>
            <div>
                <label class="labelForm">Project Title<span style="color:red">*</span></label>
                <input class="k-textbox" style="width: 400px;" type="text" id="<%: this.ID %>_projectDescription" name="<%: this.ID %>_projectDescription" data-bind="value: requestData.projectDescription, enabled: isEnabled" placeholder="Project Title" required validationMessage="Project Title is required" />
            </div>
            <div>
                <label class="labelForm">Owner<span style="color:red">*</span></label>
                <%--<input id="<%: this.ID %>_ownerName" name="<%: this.ID %>_ownerName" class="k-textbox" type="text" 
                    data-bind="disabled: isOwnerNameDisabled, value: requestData.ownerName" placeholder="Owner" required validationMessage="Owner is required" />
                <div
                    data-role="button"
                    data-bind="events: { click: onPickOwnerClick }, enabled: isEnabled"
                    class="k-button">
                    ...
                </div>--%>
                <input id="<%: this.ID %>_ownerName" name="<%: this.ID %>_ownerName" data-role="autocomplete" 
                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                    placeholder="Owner Name here (min 3 characters)" required validationMessage="Owner is required"
                    data-bind="source: dsEmployeeOwner, value: employeeOwnerSelected, events: { change: onChangeOwner }" />
            </div>
            <div>
                <label class="labelForm">Project Sponsor<span style="color:red">*</span></label>
<%--                <input id="<%: this.ID %>_sponsorName" name="<%: this.ID %>_sponsorName" class="k-textbox" type="text" 
                    data-bind="disabled: isSponsorNameDisabled, value: requestData.sponsorName" placeholder="Project Sponsor" required validationMessage="Project Sponsor is required" />
                <div
                    data-role="button"
                    data-bind="events: { click: onPickSponsorClick }, enabled: isEnabled"
                    class="k-button">
                    ...
                </div>--%>
                <input id="<%: this.ID %>_sponsorName" name="<%: this.ID %>_sponsorName" data-role="autocomplete" 
                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                    placeholder="Project Sponsor Name here (min 3 characters)" required validationMessage="Project Sponsor is required"
                    data-bind="source: dsEmployeeSponsor, value: employeeSponsorSelected, events: { change: onChangeSponsor }" />
            </div>
            <div>
                <label class="labelForm">Project Manager<span style="color:red">*</span></label>
<%--                <input id="<%: this.ID %>_pmName" name="<%: this.ID %>_pmName" class="k-textbox" type="text" 
                    data-bind="disabled: isPMNameDisabled, value: requestData.projectManagerName" placeholder="Project Manager" required validationMessage="Project Manager is required" />
                <div
                    data-role="button"
                    data-bind="events: { click: onPickPMClick }, enabled: isEnabled"
                    class="k-button">
                    ...
                </div>--%>
                <input id="<%: this.ID %>_pmName" name="<%: this.ID %>_pmName" data-role="autocomplete" 
                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                    placeholder="Project Manager Name here (min 3 characters)" required validationMessage="Project Manager is required"
                    data-bind="source: dsEmployeePM, value: employeePMSelected, events: { change: onChangePM }" />
            </div>
            <div>
                <label class="labelForm">Project Engineer<span style="color:red">*</span></label>
<%--                <input id="<%: this.ID %>_peName" name="<%: this.ID %>_peName" class="k-textbox" type="text" 
                    data-bind="disabled: isPENameDisabled, value: requestData.projectEngineerName" placeholder="Project Engineer" required validationMessage="Project Engineer is required" />
                <div
                    data-role="button"
                    data-bind="events: { click: onPickPEClick }, enabled: isEnabled"
                    class="k-button">
                    ...
                </div>--%>
                <input id="<%: this.ID %>_peName" name="<%: this.ID %>_peName" data-role="autocomplete" 
                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                    placeholder="Project Engineer Name here (min 3 characters)" required validationMessage="Project Engineer is required"
                    data-bind="source: dsEmployeePE, value: employeePESelected, events: { change: onChangePE }" />
            </div>
            <div>
                <label class="labelForm">Maintenance Reps.<%--<span style="color:red">*</span>--%></label>
<%--                <input id="<%: this.ID %>_maintenanceName" name="<%: this.ID %>_maintenanceName" class="k-textbox" type="text" 
                    data-bind="disabled: isMaintenanceNameDisabled, value: requestData.maintenanceRepName" placeholder="Maintenance Reps." required validationMessage="Maintenance Report is required" />
                <div
                    data-role="button"
                    data-bind="events: { click: onPickMaintenanceClick }, enabled: isEnabled"
                    class="k-button">
                    ...
                </div>--%>
                <input id="<%: this.ID %>_maintenanceName" name="<%: this.ID %>_maintenanceName" data-role="autocomplete" 
                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                    placeholder="Maintenance Reps. Name here (min 3 characters)" <%--required validationMessage="Maintenance Reps. is required"--%>
                    data-bind="source: dsEmployeeMaintenance, value: employeeMaintenanceSelected, events: { change: onChangeMaintenance }" />
            </div>
            <div>
                <label class="labelForm">Operation<%--<span style="color:red">*</span>--%></label>
<%--                <input id="<%: this.ID %>_operationName" name="<%: this.ID %>_operationName" class="k-textbox" type="text" 
                    data-bind="disabled: isOperationNameDisabled, value: requestData.operationRepName" placeholder="Operation" required validationMessage="Operation is required" />
                <div
                    data-role="button"
                    data-bind="events: { click: onPickOperationClick }, enabled: isEnabled"
                    class="k-button">
                    ...
                </div>--%>
                <input id="<%: this.ID %>_operationName" name="<%: this.ID %>_operationName" data-role="autocomplete" 
                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" style="width: 375px;"
                    placeholder="Operation Name here (min 3 characters)" <%--required validationMessage="Operation is required"--%>
                    data-bind="source: dsEmployeeOperation, value: employeeOperationSelected, events: { change: onChangeOperation }" />
            </div>
<%--            <div>
                <label class="labelForm">Budget Amount<span style="color:red">*</span></label>
                <input class="k-textbox" style="width: 400px;" type="number" id="<%: this.ID %>_budgetAmount" name="<%: this.ID %>_budgetAmount" data-bind="value: requestData.budgetAmount, enabled: isEnabled" placeholder="Budget Amount" required validationMessage="Budget Amount is required" />
            </div>--%>
            <div>
                <label class="labelForm">File Name<%--<span style="color:red">*</span>--%></label>
                <input name="fileName" id="fileNameId" type="file" />            
            </div>
            <div>
                <label class="labelForm" style="vertical-align: top; margin-top: 6px;">Other Position</label>
                <span tabindex="-1" role="presentation" class="k-widget k-autocomplete k-header k-state-default" style="width: 680px;">
                <div id="<%: this.ID %>_grdOhtePosition" data-columns="[
                    { field:'employeeName', title: 'Name', width: 250 },
                    { template:'#: data.assignmentName() #', title: 'Position', width: 230 }
                    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail) || !IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.InputProject))
                    { %>
                    ,
                    { command: [{ name:'edit', text: { edit: 'Edit', update: 'Save' } }, { 'name' : 'destroy', 'text' : 'Delete' } ], width: 175 }
                <% } %>
                    ]" data-editable= '{"mode": "popup" , "template": kendo.template($("#<%: this.ID %>_editGridOtherPosition").html())}', 
                    data-role="grid" data-bind="source: dsOtherPosition, events: { edit: editOtherPosition, save: saveOtherPosition, cancel: cancelOtherPosition }", 
                    data-pageable="true"<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ViewProjectDetail) || !IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.InputProject))
                    { %>,                     
                    data-toolbar= "[ { name: 'create', text: 'Add Employee' } ]"
                <% } %>
                    ></div>
                    <script id="<%: this.ID %>_editGridOtherPosition" type="text/x-kendo-tmpl">
                    <div style="padding:10px">            
                        <table style="width:600px">
                            <tr>
                                <td style="width:100px">Employee Name<span style="color:red">*</span></td>
                                <td><input id="<%: this.ID %>_employeeOtherPositionTmp"
                                    data-role="autocomplete" 
                                    data-text-field="full_Name" data-min-length="3" data-filter: "contains" 
                                    required validationMessage="Employee Name is required" 
                                    data-bind="source: dsEmployeeOtherPosition, value: employeeOtherPositionSelected" 
                                    placeholder="Employee Name here (min 3 characters)" 
                                    name="employeeName" onfocus="this.value = this.value;" style="width:375px" /></td>
                            </tr>
                            <tr>
                                <td>Position<span style="color:red">*</span></td>
                                <td>
                                    <input id="<%: this.ID %>_areaInvolved" data-role="combobox"
                                            name="assignmentType"
                                            data-placeholder="Select Position"                            
                                            data-value-primitive="true"
                                            data-text-field="text"
                                            data-value-field="value"
                                            data-bind="source: dsLookupOtherPositionProject"
                                            style="width: 300px" required validationMessage="Position is required" /></td>
                            </tr>
                        </table>
                    </div>
                </script>
               </span>
            </div>
            <div>
                <label class="labelForm">Area Involved<span style="color:red">*</span></label>
                <input id="<%: this.ID %>_areaInvolved" data-role="combobox"
                        data-placeholder="Select Area Involved"                            
                        data-text-field="textAll"
                        data-value-field="id"
                        data-bind="value: requestData.areaInvolvedId, source: dsAreaInvolved, enabled: isEnabled"
                        style="width: 300px" required validationMessage="Area Involved is required" />
            </div>
            <div>
                <label class="labelForm" style="vertical-align: top; padding-top: 7px;">Project Category<span style="color:red">*</span></label>
                <div style="width: 373px; font-size: 100%; display: inline-block!important" ><select id="<%: this.ID %>_projectCategory" data-role="multiselect"
                   data-placeholder="Project Category"
                   data-value-primitive="true"
                   data-text-field="description"
                   data-value-field="id"
                   data-bind="value: projectCategoryList, source: dsProjectCategory"
                   class="k-widget k-autocomplete k-header k-state-default" required validationMessage=" is required" ></select>
                    </div>
            </div>
            <div>
                <label class="labelForm" style="vertical-align: top; padding-top: 7px;">Project Type<span style="color:red">*</span></label>
                <div data-bind="source: dsProjectType" style="display: inline-block;" data-template="radioProjectType-template" required validationMessage=" is required" ></div>
            </div>
            <%--<div>
                <label class="labelForm">Project Type</label>
                <input type="radio" value="CAPITAL" name="projectType" class="k-radio" id="CAPITAL" data-bind="checked: requestData.projectType" />
                <label class="k-radio-label" for="CAPITAL">Capital</label>
            </div>
            <div>
                <label class="labelForm">&nbsp;</label>
                <input type="radio" value="OPERATING" name="projectType" class="k-radio" id="OPERATING" data-bind="checked: requestData.projectType" />
                <label class="k-radio-label" for="OPERATING">Operating</label>
                <label class="labelForm" style="margin-left: 100px;">PMO No</label><input class="k-textbox" typeof="text" id="pmoNo" data-bind="value: requestData.pmoNo" />
            </div>--%>
          <%--<div id="budgetColumn" style="display:none">
                <label class="labelForm">Total Cost--%><%--<span style="color:red">*</span>--%></label>
<%--                <input class="k-textbox" id="<%: this.ID %>_totalCost" name="<%: this.ID %>_totalCost" type="number" data-bind="value: requestData.totalCost, enabled: isEnabled" placeholder="Total Cost" />
            </div>--%>

            <div>
                <label class="labelForm">Plan Start Date</label>
                <input data-role="datepicker" id="<%: this.ID %>_psd" name="<%: this.ID %>_psd" data-format="dd MMM yyyy" data-bind="value: requestData.planStartDate, enabled: isEnabled" placeholder="Plan Start Date" />
            </div>
            <div>
                <label class="labelForm">Plan Finish Date</label>
                <input data-role="datepicker" id="<%: this.ID %>_pfd" name="<%: this.ID %>_pfd" data-format="dd MMM yyyy" data-bind="value: requestData.planFinishDate, enabled: isEnabled" placeholder="Plan Finish Date" />
            </div>
            <div>
                <label class="labelForm">Actual Start Date</label>
                <input data-role="datepicker" id="<%: this.ID %>_asd" name="<%: this.ID %>_asd" data-format="dd MMM yyyy" data-bind="value: requestData.actualStartDate, enabled: isEnabled" placeholder="Actual Start Date" />
            </div>
            <div>
                <label class="labelForm">Actual Finish Date</label>
                <input data-role="datepicker" id="<%: this.ID %>_afd" name="<%: this.ID %>_afd" data-format="dd MMM yyyy" data-bind="value: requestData.actualFinishDate, enabled: isEnabled" placeholder="Actual Finish Date" />
            </div>
            <div>
                <label class="labelForm">Share Folder URL</label>
                <input class="k-textbox" style="width:450px" type="text" id="<%: this.ID %>_shareFolderDoc" name="<%: this.ID %>_shareFolderDoc"  data-bind="value: requestData.shareFolderDoc, enabled: isEnabled" placeholder="ShareFolderURL: \\NETAPP8\SHARES " />
            </div>
            <div <%: IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.IPTProject) ? "" : "style='display:none'" %>>
                <label class="labelForm">IPT Integration <span style="color:red" data-bind="text: integratedStatus"></span></label>
                <input type="hidden" id="<%: this.ID %>_iptProjectId" data-bind="value: requestData.iptProjectId"/>
                <button type="button" style="width:157px" data-bind="text: btnIntegrate, enabled:isAvailToIntegrated, events: { click: onIntegrateClick }">Integrate</button>
            </div>
        </fieldset>
    </div>
    <div id="<%: this.ID %>_create">
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onCloseClick }"
                class="k-button">
                <span class="k-icon k-i-cancel"></span>Close
            </div>
        </div>
        <div style="float: right;"> &nbsp;</div>
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onSubmitClick }"
                class="k-button k-primary">
                <span class="k-icon k-i-check"></span>Submit
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    <div id="<%: this.ID %>_update" style="display:none">
        <br />
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onCloseUpdateClick }"
                class="k-button">
                <span class="k-icon k-i-cancel"></span>Cancel
            </div>
        </div>
        <div style="float: right;"> &nbsp;</div>
        <div style="float: right;">
            <div
                data-role="button"
                data-bind="events: { click: onSubmitUpdateClick }"
                class="k-button k-primary">
                <span class="k-icon k-i-check"></span>Update
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
</div>
<div id="<%: this.ID %>_popPickOwner">

    <uc1:PickEmployeesControl runat="server" ID="PickOwnerControl" />
</div>
<div id="<%: this.ID %>_popPickSponsor">

    <uc1:PickEmployeesControl runat="server" ID="PickSponsorControl" />
</div>
<div id="<%: this.ID %>_popPickPM">

    <uc1:PickEmployeesControl runat="server" ID="PickPMControl" />
</div>
<div id="<%: this.ID %>_popPickPE">

    <uc1:PickEmployeesControl runat="server" ID="PickPEControl" />
</div>
<div id="<%: this.ID %>_popPickMaintenance">

    <uc1:PickEmployeesControl runat="server" ID="PickMaintenanceControl" />
</div>
<div id="<%: this.ID %>_popPickOperation">

    <uc1:PickEmployeesControl runat="server" ID="PickOperationControl" />
</div>
<script id="radioProjectType-template" type="text/x-kendo-template">
    <div>
        <input type="radio" name="projectType" id="#:value#" class="k-radio" data-bind="attr:{value:value}; checked: requestData.projectType, enabled: isEnabled" />
        <label class="k-radio-label" for="#:value#" style="font-weight: normal; font-size: 10pt;" data-bind="text:text"></label>
        # if (value == "CAPITAL") {#
        <label class="labelForm" style="margin-left: 100px;">PMO No</label>
        <input type="text" class="k-input k-textbox" id="pmoNo" name="pmoNo" data-bind="value: requestData.pmoNo, enabled: isEnabled"  />
        # } #
    </div>
</script>
