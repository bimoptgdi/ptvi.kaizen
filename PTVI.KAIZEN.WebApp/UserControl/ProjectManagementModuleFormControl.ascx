﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectManagementModuleFormControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.ProjectManagementModuleFormControl" %>
<%@ Register Src="~/UserControl/UploaderNewControl.ascx" TagPrefix="uc1" TagName="UploadControl" %>
<%@ Register Src="~/UserControl/PSDFormControl.ascx" TagPrefix="uc1" TagName="PSDFormControl" %>
<%@ Register Src="~/UserControl/ProjectBudgetApprovalControl.ascx" TagPrefix="uc1" TagName="ProjectBudgetApprovalControl" %>

<script>
    common_getAllStatusIprom();
    _extKDMonth = 0;
    var <%: this.ID %>_mainProjectManagementModuleTabStrip;
    var <%: this.ID %>_keyDeliverablegrid = [];
    var <%: this.ID %>_keyDeliverableFormatData = [];
    var <%: this.ID %>_popProjectManagementModuleChart, <%: this.ID %>_popKPIGraphChartModule;
    var <%: this.ID %>_viewProjectManagementModuleModel = kendo.observable({
        isEnabledKeyDeliverable: true,
        projectManagementId: "",
        manageKeyIssue: "",
        manageKeyIssueOri: "",
        scope: "",
        scopeOri: "",
        benefit: "",
        benefitOri: "",
        accomplishment: "",
        accomplishmentOri: "",
        details: "",
        detailsOri: "",
        contractor: "",
        contractorOri: "",
        statusValue: "S6",
        //manageKeyIssueChanged: false,
        //manageKeyIssueChange: function (e) {
        //    if (this.manageKeyIssueOri != this.manageKeyIssue) manageKeyIssueChanged: true;
        //},
        dsProjectManagementModule: function () {
            var that = this;
            $.ajax({
                url: _webApiUrl + "ProjectManagement/PMByProjectId/" + _projectId,
                type: "GET",
                error: function (xhr, textStatus, error) {

                },
                success: function (data) {
                    if (data != null) {
                        that.set("projectManagementId", data.id);
                        that.set("manageKeyIssue", data.manageKeyIssue);
                        that.set("manageKeyIssueOri", data.manageKeyIssue);
                        that.set("safetyPerfomance", data.safetyPerfomance);
                        that.set("safetyPerfomanceOri", data.safetyPerfomance);
                        that.set("scope", data.scope);
                        that.set("scopeOri", data.scope);
                        that.set("changeRequest", data.changeRequest);
                        that.set("changeRequestOri", data.changeRequest);
                        that.set("benefit", data.benefit);
                        that.set("benefitOri", data.benefit);
                        that.set("accomplishment", data.accomplishment);
                        that.set("accomplishmentOri", data.accomplishment);
                        that.set("details", data.details);
                        that.set("detailsOri", data.details);
                        that.set("ehsRecords", data.ehsRecords);
                        that.set("ehsRecordsOri", data.ehsRecords);
                        that.set("contractor", data.contractor);
                        that.set("contractorOri", data.contractor);

                    }
                },
                complete: function (jqXhr, textStatus) {

                }
            });
        },
        dsKeyDList: new kendo.data.DataSource({
            transport: {
                read: {
                    async: false,
                    url: _webApiUrl + "masterKeyDeliverables/FindByKeyDeliverables/" + _projectId
                }
            },
        }),
        dsListKeyDeliverableModule: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListKeyDeliverableModule"), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListKeyDeliverableModule"), false);
                var total = 0;
                var sdata = this.data();
                if (sdata.length > 0) {
                    for (var i = 0; i < sdata.length; i++) {
                        //yang status cancel tidak di hitung
                        if (sdata[i].status != 'S3')
                            total += sdata[i].weightPercentage;
                    }
                }
                $("#weightInfo").remove();
                if (total < 100)
                    $("#<%: this.ID %>_grdListKeyDeliverableModule .k-grid-toolbar").append("<div id='weightInfo' style= 'padding-left:10px;display:inline-block;color:red;width:300px'>Weight is (" + total + "%) less than 100%</div>");
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null },
                        weightPercentage: { type: "number" }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "KeyDeliverables/listKeyDByProjectId/" + _projectId,
                    dataType: "json"
                },
                create: { url: _webApiUrl + "KeyDeliverables/AddKD/" + _projectId, type: "POST" },
                update: {
                    url: function (data) {
                        return _webApiUrl + "KeyDeliverables/UpdateKD/" + data.id;
                    },
                    type: "PUT"
                },
                destroy: {
                    url: function (data) {
                        return _webApiUrl + "KeyDeliverables/" + data.id
                    },
                    type: "DELETE"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        data.projectManagementId = <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId;
                        data.name = $("#keyDL").data("kendoComboBox")=== undefined?$("#keyDL").val() : $("#keyDL").data("kendoComboBox").value();
                        data.planStartDate = kendo.toString(data.planStartDate, 'yyyy-MM-dd');
                        data.planFinishDate = kendo.toString(data.planFinishDate, 'yyyy-MM-dd');
                        data.actualStartDate = kendo.toString(data.actualStartDate, 'yyyy-MM-dd');
                        data.actualFinishDate = kendo.toString(data.actualFinishDate, 'yyyy-MM-dd');
                    }
                    return data;
                }
            },
            pageSize: 10,
            sort: [{ field: "seq", dir: "asc" } , { field: "planStartDate", dir: "asc" }]
        }),
        dsProjectManagementModuleChart: new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListKeyDeliverableModule"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListKeyDeliverableModule"), false);
            },
            schema: {
                model: {
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null }
                    }
                },
                parse: function (responses) {

                    if (responses.length > 0) {
                        $.each(responses, function (idx, response) {
                            response["planStartDateRep"] = response.planStartDate ? kendo.parseDate(response.planStartDate, "yyyy-MM-dd").getTime() : null;
                            response["planFinishDateRep"] = response.planFinishDate ? kendo.parseDate(response.planFinishDate, "yyyy-MM-dd").getTime() : null;
                            response["actualStartDateRep"] = response.actualStartDate ? kendo.parseDate(response.actualStartDate, "yyyy-MM-dd").getTime() : null;
                            response["actualFinishDateRep"] = response.actualFinishDate ? kendo.parseDate(response.actualFinishDate, "yyyy-MM-dd").getTime() : null;
                        });
                    }

                    return responses;
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "KeyDeliverables/projectManagementModuleChart/" + _projectId,
                    dataType: "json"
                }
            },
            sort: [{ field: "planStartDate", dir: "asc" }]
        }),
        dsProjectManagementModuleChart: new kendo.data.DataSource({
            requestStart: function (e) {
                kendo.ui.progress($("#<%: this.ID %>_grdListKeyDeliverableModule"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListKeyDeliverableModule"), false);
            },
            schema: {
                model: {
                    fields: {
                        planStartDate: { type: "date", defaultValue: null },
                        planFinishDate: { type: "date", defaultValue: null },
                        actualStartDate: { type: "date", defaultValue: null },
                        actualFinishDate: { type: "date", defaultValue: null }
                    }
                },
                parse: function (responses) {

                    if (responses.length > 0) {
                        $.each(responses, function (idx, response) {
                            response["planStartDateRep"] = response.planStartDate ? kendo.parseDate(response.planStartDate, "yyyy-MM-dd").getTime() : null;
                            response["planFinishDateRep"] = response.planFinishDate ? kendo.parseDate(response.planFinishDate, "yyyy-MM-dd").getTime() : null;
                            response["actualStartDateRep"] = response.actualStartDate ? kendo.parseDate(response.actualStartDate, "yyyy-MM-dd").getTime() : null;
                            response["actualFinishDateRep"] = response.actualFinishDate ? kendo.parseDate(response.actualFinishDate, "yyyy-MM-dd").getTime() : null;
                        });
                    }

                    return responses;
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "KeyDeliverables/projectManagementModuleChart/" + _projectId,
                    dataType: "json"
                }
            },
            sort: [{ field: "planStartDate", dir: "asc" }]
        }),
        dsprojectKPIProgressMonthlyModule: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), false);
            },
            schema: {
                model: {
                    fields: {
                        minDate: { type: "date", defaultValue: null },
                        maxDate: { type: "date", defaultValue: null }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "KeyDeliverablesProgresses/projectKPIProgressMonthly/" + _projectId + "?ExtKDMonth="+ _extKDMonth,
                    async: false,
                    dataType: "json"
                }
            },
            pageSize: 10
        }),
        dsprojectKPIProgressMonthlyMaxMinDateModule: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), false);
            },
            schema: {
                model: {
                    fields: {
                        minDate: { type: "date", defaultValue: null },
                        maxDate: { type: "date", defaultValue: null }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "KeyDeliverables/projectManagementModuleChartMaxMinDate/" + _projectId,
                    async: false,
                    dataType: "json"
                }
            },
            pageSize: 10,
            sort: [{ field: "seq", dir: "asc" }, { field: "id", dir: "asc" }]
        }),
        dsprojectKPIProgressMonthlyModuleReformat: function () {
            var dataReformat = [];
            this.dsprojectKPIProgressMonthlyModule.options.transport.read.url = _webApiUrl + "KeyDeliverablesProgresses/projectKPIProgressMonthly/" + _projectId + "?ExtKDMonth=" + _extKDMonth;
            this.dsprojectKPIProgressMonthlyModule.read();
            var dataOri = this.dsprojectKPIProgressMonthlyModule.data();

            //get count data group by year
            var groupByYear = []

            if (dataOri.length > 0) {
                groupByYear = Enumerable.From(dataOri[0].keyDeliverablesProgresses).GroupBy("$.year", null, function (key, g) {
                    var result = {
                        year: key,
                        total: g.Count("$.month")
                    }
                    return result;
                }).ToArray();
            }

            //generateColumnGrid
            <%: this.ID %>_keyDeliverablegrid = [];
            <%: this.ID %>_keyDeliverablegrid.push({
                field: "name", title: "Key Deliverables", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center;"
                },
                width: 100,
                attributes: { style: "text-align: left;" },
                footerAttributes: { style: "text-align: left;" },
                footerTemplate: 'Total', 
                filterable: false,
                //locked: true
            });

            <%: this.ID %>_keyDeliverablegrid.push({
                field: "weight", title: "% Weight", editable: false,
                headerAttributes: {
                    "class": "table-header-cell",
                    style: "text-align: center;"
                },
                width: 50,
                attributes: { style: "text-align: left;" },
                filterable: false,
                //locked: true
            });

            //loop grid column month group by Year
            var seqMonth = 0;
            var columnMonthByYear;
            for (var i = 0; i < groupByYear.length; i++) {
                var getMonthByYear = Enumerable.From(dataOri[0].keyDeliverablesProgresses).Where(function (x) { return (x.year == groupByYear[i].year); }).Select("$.month").ToArray();
                columnMonthByYear = [];
                for (var j = 0; j < getMonthByYear.length; j++) {
                    columnMonthByYear.push({
                        title: kendo.toString(new Date(groupByYear[i].year, getMonthByYear[j] - 1, 01), 'MMM'),
                        headerAttributes: {
                            "class": "table-header-cell",
                            style: "text-align: center;"
                        },
                        columns: [
                            {
                                field: "plan_" + seqMonth, title: "Plan",
                                headerAttributes: {
                                    "class": "table-header-cell",
                                    style: "text-align: center;"
                                }, width: 70,
                                attributes: { style: "text-align: right;" },
                                template: '#:data.plan_' + seqMonth + ' == null || data.plan_' + seqMonth + ' == 0 || data.plan_' + seqMonth + ' == \'\' ? "" : data.plan_' + seqMonth + ' + "%"#',
                                footerAttributes: { style: "text-align: right;" },
                                footerTemplate: '#=window.calculateActualProgress("plan_' + seqMonth + '")# %'
                            },
                            {
                                field: "actual_" + seqMonth, title: "Act.",
                                headerAttributes: {
                                    "class": "table-header-cell",
                                    style: "text-align: center;"
                                }, width: 70,
                                attributes: { style: "text-align: right;" },
                                template: '#:data.actual_' + seqMonth + ' == null || data.actual_' + seqMonth + ' == 0 || data.actual_' + seqMonth + ' == \'\' ? "" : data.actual_' + seqMonth + ' + "%"#',
                                footerAttributes: { style: "text-align: right;" },
                                footerTemplate: '#=window.calculateActualProgress("actual_' + seqMonth + '")# %'
                            }]
                    });
                    seqMonth++;
                }

                <%: this.ID %>_keyDeliverablegrid.push({
                    title: groupByYear[i].year + "",
                    headerAttributes: {
                        "class": "table-header-cell",
                        style: "text-align: center;"
                    },
                    columns: columnMonthByYear
                });

            }
            this.set("columnProjectKPIProgressMonthlyModule", <%: this.ID %>_keyDeliverablegrid);

            //Generate Data
            var yearMonthKeyDeliverable = [];
            for (var i = 0; i < dataOri.length; i++) {
                // new object for insert DataReformat
                var keyDeliverableData = {};

                keyDeliverableData["id"] = dataOri[i].id;
                <%: this.ID %>_keyDeliverableFormatData["id"] = { type: "number" };
                keyDeliverableData["name"] = dataOri[i].nameText;
                <%: this.ID %>_keyDeliverableFormatData["name"] = { type: "string" };
                keyDeliverableData["weight"] = dataOri[i].weight;
                <%: this.ID %>_keyDeliverableFormatData["weight"] = { type: "number" };

                //generate plan & actual progress by month
                for (var j = 0; j < dataOri[i].keyDeliverablesProgresses.length; j++) {
                    //keyDeliverable["plan" + dataOri[i].keyDeliverablesProgresses[j].year + lpad(dataOri[i].keyDeliverablesProgresses[j].month, 2, 0)] = dataOri[i].keyDeliverablesProgresses[j].planPercentage;
                    keyDeliverableData["plan_" + j] = dataOri[i].keyDeliverablesProgresses[j].planPercentage;
                    keyDeliverableData["actual_" + j] = dataOri[i].keyDeliverablesProgresses[j].actualPercentage;
                    if (i == 0) {
                        <%: this.ID %>_keyDeliverableFormatData["plan_" + j] = { type: "number", validation: { min: 0, max: 100 } };
                        <%: this.ID %>_keyDeliverableFormatData["actual_" + j] = { type: "number", validation: { min: 0, max: 100 } };
                        yearMonthKeyDeliverable.push({ name: "actual_" + j, year: dataOri[i].keyDeliverablesProgresses[j].year, month: dataOri[i].keyDeliverablesProgresses[j].month });
                        yearMonthKeyDeliverable.push({ name: "plan_" + j, year: dataOri[i].keyDeliverablesProgresses[j].year, month: dataOri[i].keyDeliverablesProgresses[j].month });
                    }
                }

                dataReformat[i] = keyDeliverableData;
            }

            this.set("getYearMonthKPIProgressMonthlyModuleReformat", yearMonthKeyDeliverable);


            return dataReformat;
        },
        getYearMonthKPIProgressMonthlyModuleReformat: [],
        columnProjectKPIProgressMonthlyModule: [],
        columnName: "",
        generateGridProjectKPIProgressModule: function () {
            var that = this;
            var dataGrid = that.dsprojectKPIProgressMonthlyModuleReformat();
            var idGrid = "#<%: this.ID %>_grdListProjectKPIGraph";

            var dataSource = new kendo.data.DataSource({
                data: dataGrid,
                schema: {
                    model: {
                        id: "id",
                        fields: <%: this.ID %>_keyDeliverableFormatData
                    }
                },
                //aggregate: aggregateGridInquery
            });

            if ($(idGrid).getKendoGrid() != null) {
                $(idGrid).getKendoGrid().destroy();
                $(idGrid).empty();
            }

            if ($(idGrid).getKendoGrid() == null) {
                $(idGrid).kendoGrid({
                    toolbar: ["excel"],
                    columns: <%: this.ID %>_keyDeliverablegrid,
                    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ProjectKPIGraph))
                        { %>
                    editable: true,
                    <% } else { %>
                    editable: false,
                    <% } %>
                    edit: function (e) {
                        <% if (IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                        { %>
                        this.closeCell();
                        <% }%>

                        var indexCell = e.container.context.cellIndex;
                        if (indexCell < 2/* || (indexCell % 2) == 0*/) {
                            this.closeCell();
                        }
                        that.set("columnName", indexCell % 2 == 0 ? "plan_" + (Math.floor(indexCell / 2) - 1) : "actual_" + (Math.floor(indexCell / 2) - 1));
                    },
                    save: function (e) {

                        e.model.one("change", function () {
                            aaa = e;
                            var grid = $(idGrid).data("kendoGrid");
                            var ds = grid.dataSource;
                            if (ds == null) return;
                            var columnName = "";
                            var data = ds.data();
                            if (data == null) return;
                            var objName = "";
                            var objVal;
                            for (var name in e.values) {
                                objName = name;
                                objVal = e.values[objName];
                                columnName = name.split('_')[0];
                            }
                            var getIndexColumn = parseInt(objName.replace(columnName + "_", ""));
                            var dataOri = <%: this.ID %>_viewProjectManagementModuleModel.dsprojectKPIProgressMonthlyModule.data();
                            var groupByYear = Enumerable.From(dataOri[0].keyDeliverablesProgresses).GroupBy("$.year", null, function (key, g) {
                                var result = {
                                    year: key,
                                    total: g.Count("$.month")
                                }
                                return result;
                            }).ToArray();
                            var total = 0;
                            var overlimit;
                            //diganti
                            //var totalProgress = 0;
                            //if (data) {
                            //    for (var i = 0; i < data.length; i++) {
                            //        var totalProgress = 0;
                            //        for (var j = 0; j <= groupByYear[0].total; j++) {
                            //            totalProgress += isNaN(parseFloat(data[i][columnName + "_" + j])) ? 0 : parseFloat(data[i][columnName + "_" + j]);
                            //            if (totalProgress > 100) overlimit = true;
                            //        }
                            //        total += totalProgress * (data[i]["weight"] / 100);
                            //    }
                            //}

                            if ((total > 100) || (objVal > 100)||(overlimit==true)) {
                                if (objVal > 100) alert("Values exceed 100%");
                                else if (total > 100) alert("Total exceed 100%");
                                else if (overlimit) alert("Sum Values Exceed 100%");
                                //e.preventDefault();
                                e.sender.dataSource.cancelChanges();
                            } else {
                                if (dataOri[0].keyDeliverablesProgresses.length != 0) {
                                    for (var k = getIndexColumn; k < dataOri[0].keyDeliverablesProgresses.length; k++) {
                                        var updateColumnName = columnName + '_' + k;
                                        if (k != getIndexColumn) {
                                            e.model[updateColumnName] = e.values[objName];
                                        }

                                        $(idGrid).data("kendoGrid").dataSource.sync();
                                        //var columnName = e.container.find(".k-input.k-valid").attr("name");

                                        var getYearMonth = Enumerable.From(that.getYearMonthKPIProgressMonthlyModuleReformat).
                                            Where(function (x) { return (x.name == updateColumnName); }).ToArray();
                                        console.log(updateColumnName);
                                        var data = {};
                                        data.id = e.model.id;
                                        data.year = getYearMonth[0].year;
                                        data.month = getYearMonth[0].month;
                                        var urlUpdate = "";
                                        if (updateColumnName.split('_')[0] == "actual") {
                                            data.actualPercentage = e.values[objName];
                                            //                                    data.planPercentage = e.value[""];
                                            urlUpdate = "KeyDeliverablesProgresses/updateActualProgress/";

                                        } else {
                                            data.planPercentage = e.values[objName];
                                            urlUpdate = "KeyDeliverablesProgresses/updatePlanProgress/";
                                        }
                                        kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), true);

                                        $.ajax({
                                            url: _webApiUrl + urlUpdate + e.model.id,
                                            type: "put",
                                            data: JSON.stringify(data),
                                            contentType: "application/json",
                                            async: true,
                                            success: function () {
                                                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), false);
                                            },
                                            error: function () {
                                                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), false);
                                            }
                                        });
                                    }
                                }
                            }
                        });


                    }
                });
            }
            $(idGrid).getKendoGrid().setDataSource(dataSource);
            $(idGrid).getKendoGrid().dataSource.read();
            $(idGrid).data("kendoGrid");

            $(".k-grid-ViewExtendedMonth").on('click', function (e) {

            });
        },
        dsKPIGraphChartModule: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdListProjectKPIGraph"), false);
            },
            transport: {
                read: {
                    url: _webApiUrl + "KeyDeliverablesProgresses/projectKPIChart/" + _projectId+"?ExtKDMonth="+_extKDMonth,
                    dataType: "json"
                }
            }
        }),
        dsStatusSource: new kendo.data.DataSource({
            transport: {
                read: _webApiUrl + "lookup/findbytype/" + _statusIprom
            },
            schema: {
                model: {
                    id: "value",
                    fields: {
                        value: {},
                        text: {}
                    }
                }
            }
        }),
        editKeyDeliverable: function (e) {
            $.ajax({
                url: _webApiUrl + "project/" + _projectId,
                type: "GET",
                success: function (data) {
                    if (kendo.parseDate(data.createdDate) >= kendo.parseDate(cutOfKey, "MM/dd/yyyy")) {
                        var str =
                        '<input id="keyDL" required validationMessage="Key Deliverables is required" placeholder="Please Input Key Deliverables" name="name" onfocus="this.value = this.value;" value="' + (e.model.name === undefined ? "" : e.model.name) + '" />';
                        //$('#keyDL').data('kendoComboBox').dataSource.read();
                        $("#keyDel").append(str);
                        $("#keyDL").width(300).kendoComboBox({
                            dataTextField: "keyDeliverables",
                            dataValueField: "id",
                            dataSource: {
                                transport: {
                                    read: {
                                        async: false,
                                        url: _webApiUrl + "masterKeyDeliverables/FindByKeyDeliverables/" + _projectId,
                                    }
                                },
                                sort: [{ field: "keyDeliverables", dir: "asc" }]
                            },
                        })
                        $('#keyDL').data('kendoComboBox').value(e.model.name);
                    }
                    else {
                        var str = '<input id="keyDL" required validationMessage="Key Deliverables is required" placeholder="Please Input Key Deliverables" name="name" onfocus="this.value = this.value;" style="width:70%" value="' + (e.model.name === undefined ? "" : e.model.name) + '" />';
                        $("#keyDel").append(str);
                    }
                    if (e.model.isNew()) {
                        var keyDL = $("#keyDL").data("kendoComboBox");
                        $(".k-window-title").html("Add Key Deliverable");
                        $(".k-grid-update").html('<span class="k-icon k-i-check"></span>Save');
                    }
                    else {
                        var keyDL = $("#keyDL").data("kendoComboBox");
                        keyDL.readonly(true);
                        $(".k-window-title").html("Edit Key Deliverable");
                    }
                },
                error: function (data) {

                },
            });
            //e.model.set("dirty", true);


            $("#<%: this.ID %>_planStartDateTmp").getKendoDatePicker().bind("change", function () {
                $("#<%: this.ID %>_planFinishDateTmp").getKendoDatePicker().min(this.value());
            });

            $("#<%: this.ID %>_planFinishDateTmp").getKendoDatePicker().bind("change", function () {
                $("#<%: this.ID %>_planStartDateTmp").getKendoDatePicker().max(this.value());
            });

            if (_planStartDate) {
                $("#<%: this.ID %>_planStartDateTmp").getKendoDatePicker().min(_planStartDate);
                $("#<%: this.ID %>_planFinishDateTmp").getKendoDatePicker().min(_planStartDate);
            }

            if (_planFinishDate) {
                $("#<%: this.ID %>_planStartDateTmp").getKendoDatePicker().max(_planFinishDate);
                $("#<%: this.ID %>_planFinishDateTmp").getKendoDatePicker().max(_planFinishDate);
            }

            if (e.model.planFinishDate)
                $("#<%: this.ID %>_planStartDateTmp").getKendoDatePicker().max(e.model.planFinishDate);
            if (e.model.planStartDate)
                $("#<%: this.ID %>_planFinishDateTmp").getKendoDatePicker().min(e.model.planStartDate);

            if (e.model.id) {
                <%: this.ID %>_viewProjectManagementModuleModel.set("statusValue",e.model.status);
                $("#<%: this.ID %>_actualStartDateTmp").getKendoDatePicker().bind("change", function () {
                    $("#<%: this.ID %>_actualFinishDateTmp").getKendoDatePicker().min(this.value());
                });

                $("#<%: this.ID %>_actualFinishDateTmp").getKendoDatePicker().bind("change", function () {
                    $("#<%: this.ID %>_actualStartDateTmp").getKendoDatePicker().max(this.value());
                });

                if (e.model.actualFinishDate)
                    $("#<%: this.ID %>_actualStartDateTmp").getKendoDatePicker().max(e.model.actualFinishDate);
                if (e.model.actualStartDate)
                    $("#<%: this.ID %>_actualFinishDateTmp").getKendoDatePicker().min(e.model.actualStartDate);
            }
            $(".k-edit-buttons").prepend('<a id="btnCancel" class="k-button" style="float: left">Cancel</a>');
            $(".k-edit-buttons").prepend('<a id="btnHold" class="k-button" style="float: left">Hold</a>');

            window.setTimeout(function () {
                $(".k-edit-form-container").parent().width(620).data("kendoWindow").center();
                $(".k-edit-buttons").width(600);
            }, 100);
            $("#btnCancel").on("click", function () {
                e.model.status = "S3";
                e.model.dirty = true;
                e.model.project = {};
                e.model.tender = {};

                e.sender.dataSource.sync();
            });
            $("#btnHold").on("click", function () {
                e.model.status = "S5";
                e.model.dirty = true;
                e.model.project = {};
                e.model.tender = {};

                e.sender.dataSource.sync();
            });
        },
        saveKeyDeliverable: function (e) {
            //cek total
            <%: this.ID %>_viewProjectManagementModuleModel.set("statusValue", $("#ddStatus").getKendoDropDownList().value());
            if (e.model.status != <%: this.ID %>_viewProjectManagementModuleModel.statusValue) {
                e.model.status = <%: this.ID %>_viewProjectManagementModuleModel.statusValue;
                e.model.dirty = true;
            };
            if (e.model.actualFinishDate != null) e.model.status = "S1";
            var grid = $("#<%: this.ID %>_grdListKeyDeliverableModule").data("kendoGrid");
            var ds = grid.dataSource;
            if (ds == null) return;
            var data = ds.data();
            var total = 0;
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    //yang status cancel tidak di hitung
                    if (data[i].status != 'S3')
                        total += data[i].weightPercentage;
                }
            }

            if ((total > 100)) {
                if (total > 100) alert("Total Weight Percentage exceed 100%");
                e.preventDefault();
                //e.sender.dataSource.cancelChanges();
            }
        },
        excelKeyDeliverable: function (e) {
            var i = 0;
            e.workbook.sheets[0].columns = [{ autoWidth: true }, { autoWidth: true }, { autoWidth: true }, { autoWidth: true }, { autoWidth: true },
            { autoWidth: true }, { autoWidth: false }, { autoWidth: true }, { autoWidth: true }, { autoWidth: true }];

            var rows = e.workbook.sheets[0].rows;

            <%: this.ID %>_viewProjectManagementModuleModel.dsStatusSource.read()

            for (var ri = 0; ri < rows.length; ri++) {
                var row = rows[ri];
                if (row.type == "header") {
                    for (var ci = 0; ci < row.cells.length; ci++) {
                        var cell = row.cells[ci];
                        cell.border = "black";
                        cell.hAlign = "center";
                    }
                }
                if (row.type == "data") {
                    for (var ci = 0; ci < row.cells.length; ci++) {
                        var cell = row.cells[ci];
                        if (cell.value) {
                            // Use jQuery.fn.text to remove the HTML and get only the text
                            if (ci == 7) {
                                cell.value = <%: this.ID %>_viewProjectManagementModuleModel.dsStatusSource.get("S2").text
                            } else {
                                cell.value = cell.value;
                            }
                            // Set the alignment
                            //cell.hAlign = "right";
                        }
                    }
                }
            }
        },
        onViewChartKeyDeliverableClick: function (e) {
            $(".k-window-title").html("View Graph");
            this.dsProjectManagementModuleChart.read();
            this.dsprojectKPIProgressMonthlyMaxMinDateModule.read()
            if (this.dsprojectKPIProgressMonthlyMaxMinDateModule.data().length > 0) {
                $("#<%: this.ID %>_projectManagementModuleChart").data("kendoChart").setOptions({ valueAxis: { min: this.dsprojectKPIProgressMonthlyMaxMinDateModule.data()[0].minDate.getTime(), max: this.dsprojectKPIProgressMonthlyMaxMinDateModule.data()[0].maxDate.getTime() } });
                
            }
            <%: this.ID %>_popProjectManagementModuleChart.center().open();
        },
        onViewChartKeyDeliverableClose: function (e) {
            <%: this.ID %>_popProjectManagementModuleChart.close();
        },
        addAdditionalMonth: function (e) {
            _extKDMonth = _extKDMonth + 1;
            var idGrid = "#<%: this.ID %>_grdListProjectKPIGraph";
            $(idGrid).getKendoGrid().refresh();
            <%: this.ID %>_viewProjectManagementModuleModel.generateGridProjectKPIProgressModule();
            if (_extKDMonth == 3)
                $("#btnAddMonth").hide();
        },
        onViewChartKPIProgressClick: function (e) {
            $(".k-window-title").html("View Graph");
            var that = this;
            this.dsKPIGraphChartModule.read().then(function () {
                var step = Math.round(that.dsKPIGraphChartModule.total() / 10);
                <%: this.ID %>_popKPIGraphChartModule.center().open();
                $("#<%: this.ID %>_KPIGraphChartModule").getKendoChart().setOptions({ categoryAxis: { labels: { step: step } } });
            });
        },
        onViewChartKPIProgressClose: function (e) {
            <%: this.ID %>_popKPIGraphChartModule.close();
        },
        onSaveManageKeyIssueClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                manageKeyIssue: <%: this.ID %>_viewProjectManagementModuleModel.manageKeyIssue
            };
            kendo.ui.progress($("#<%: this.ID %>_MainProjectManagementModuleContent"), true);
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/ManageKeyIssue",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("manageKeyIssueOri", data.manageKeyIssue);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    kendo.ui.progress($("#<%: this.ID %>_MainProjectManagementModuleContent"), false);
                    _showDialogMessage("Manage Key Issue", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    kendo.ui.progress($("#<%: this.ID %>_MainProjectManagementModuleContent"), false);
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
        onCancelManageKeyIssueClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("manageKeyIssue", <%: this.ID %>_viewProjectManagementModuleModel.manageKeyIssueOri);
        },
       onSaveScopeClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                scope: <%: this.ID %>_viewProjectManagementModuleModel.scope,
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/Scope",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("scopeOri", data.scope);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Scope", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
            onCancelScopeClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("scope", <%: this.ID %>_viewProjectManagementModuleModel.scopeOri);
        },
       onSaveSafetyPerfomanceClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                safetyPerfomance: <%: this.ID %>_viewProjectManagementModuleModel.safetyPerfomance,
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/SafetyPerfomance",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("safetyPerfomance", data.safetyPerfomance);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Safety Perfomance", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
            onCancelSafetyPerfomanceClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("safetyPerfomance", <%: this.ID %>_viewProjectManagementModuleModel.safetyPerfomanceOri);
        },
        onSaveChangeRequestClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                changeRequest: <%: this.ID %>_viewProjectManagementModuleModel.changeRequest,
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/ChangeRequest",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("changeRequest", data.changeRequest);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Change Request", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
           onCancelChangeRequestClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("changeRequest", <%: this.ID %>_viewProjectManagementModuleModel.changeRequestOri);
        },
    onSaveBenefitClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                benefit: <%: this.ID %>_viewProjectManagementModuleModel.benefit
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/Benefit",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("benefitOri", data.benefit);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Objective", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
            onCancelBenefitClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("benefit", <%: this.ID %>_viewProjectManagementModuleModel.benefitOri);
        },
      onSaveAccomplishmentClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                accomplishment: <%: this.ID %>_viewProjectManagementModuleModel.accomplishment
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/Accomplishment",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("accomplishmentOri", data.accomplishment);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Accomplishment", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
            onCancelAccomplishmentClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("accomplishment", <%: this.ID %>_viewProjectManagementModuleModel.accomplishmentOri);
        },
      onSaveDetailsClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                details: <%: this.ID %>_viewProjectManagementModuleModel.details
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/Details",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("detailsOri", data.details);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Next Plan", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
            onCancelDetailsClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("details", <%: this.ID %>_viewProjectManagementModuleModel.detailsOri);
        },
     onSaveEHSRecordsClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                EhsRecords: <%: this.ID %>_viewProjectManagementModuleModel.ehsRecords,
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/EhsRecords",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("ehsRecords", data.ehsRecords);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Ehs Records", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
          },
            onCancelEHSRecordsClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("ehsRecords", <%: this.ID %>_viewProjectManagementModuleModel.ehsRecordsOri);
        },
        onSaveContractorClick: function (e) {
            var requestData = {
                id: <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId,
                projectNo: _projectId,
                contractor: <%: this.ID %>_viewProjectManagementModuleModel.contractor
            };
            $.ajax({
                url: _webApiUrl + "ProjectManagement/UpdateManagementModule/Contractor",
                type: "POST",
                data: requestData,
                success: function (data) {
                    <%: this.ID %>_viewProjectManagementModuleModel.set("contractorOri", data.contractor);
                    <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                    _showDialogMessage("Contractor", "Save Successfully!", "");
                },
                error: function (jqXHR) {
                    _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                }
            });
        },
        onCancelContractorClick: function (e) {
            <%: this.ID %>_viewProjectManagementModuleModel.set("contractor", <%: this.ID %>_viewProjectManagementModuleModel.contractorOri);
        },
        onChangePlanStartDate: function (e) {
        },
        saveAll: function (e) {
            var requestData = {};
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul)) { %>
            //save scope
                requestData.id = <%: this.ID %>_viewProjectManagementModuleModel.projectManagementId;
                requestData.projectNo = _projectId;
                if (this.manageKeyIssueOri != this.manageKeyIssue) {
                    requestData.manageKeyIssue = <%: this.ID %>_viewProjectManagementModuleModel.manageKeyIssue;
                    
                } else requestData.manageKeyIssue = <%: this.ID %>_viewProjectManagementModuleModel.manageKeyIssueOri;
                //save scope
                if (this.scopeOri != this.scope) {
                    requestData.scope = <%: this.ID %>_viewProjectManagementModuleModel.scope;
                } else requestData.scope = <%: this.ID %>_viewProjectManagementModuleModel.scopeOri;
                //save safetyPerfomance
                if (this.safetyPerfomance != this.safetyPerfomanceOri) {
                requestData.safetyPerfomance = <%: this.ID %>_viewProjectManagementModuleModel.safetyPerfomance;
                } else requestData.safetyPerfomance = <%: this.ID %>_viewProjectManagementModuleModel.safetyPerfomanceOri;
                //save changeRequest
                if (this.changeRequest != this.changeRequestOri) {
                requestData.changeRequest = <%: this.ID %>_viewProjectManagementModuleModel.changeRequest;
                } else requestData.changeRequest = <%: this.ID %>_viewProjectManagementModuleModel.changeRequestOri;
                //save Objective
                if (this.benefit != this.benefitOri) {
                    requestData.benefit = <%: this.ID %>_viewProjectManagementModuleModel.benefit;
                } else requestData.benefit = <%: this.ID %>_viewProjectManagementModuleModel.benefitOri;
               //save Accomplishment
                if (this.accomplishment != this.accomplishmentOri) {
                    requestData.accomplishment= <%: this.ID %>_viewProjectManagementModuleModel.accomplishment;
                } else requestData.accomplishment= <%: this.ID %>_viewProjectManagementModuleModel.accomplishmentOri;
                //save Next Plan
                if (this.details != this.detailsOri) {
                    requestData.details= <%: this.ID %>_viewProjectManagementModuleModel.details;
                } else requestData.details= <%: this.ID %>_viewProjectManagementModuleModel.detailsOri;
                //save ehsRecords
                if (this.ehsRecords != this.ehsRecordsOri) {
                requestData.ehsRecords = <%: this.ID %>_viewProjectManagementModuleModel.ehsRecords;
                } else requestData.ehsRecords = <%: this.ID %>_viewProjectManagementModuleModel.ehsRecordsOri;
                //save Contractor
                if (this.contractor != this.contractorOri) {
                    requestData.contractor = <%: this.ID %>_viewProjectManagementModuleModel.contractor;
                } else requestData.contractor = <%: this.ID %>_viewProjectManagementModuleModel.contractorOri;
                $.ajax({
                    url: _webApiUrl + "ProjectManagement/UpdateManagementModule/all",
                    type: "POST",
                    data: requestData,
                    success: function (data) {
                        <%: this.ID %>_viewProjectManagementModuleModel.set("manageKeyIssueOri", data.manageKeyIssue);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("safetyPerfomanceOri", data.safetyPerfomance);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("scopeOri", data.scope);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("changeRequestOri", data.changeRequest);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("benefitOri", data.benefit);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("accomplishmentOri", data.accomplishment);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("detailsOri", data.details);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("ehsRecordsOri", data.ehsRecords);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("contractorOri", data.contractor);
                        <%: this.ID %>_viewProjectManagementModuleModel.set("projectManagementId", data.id);
                        _showDialogMessage("Save All", "Saved Successfully!", "");
                    },
                    error: function (jqXHR) {
                        _showDialogMessage("Error Message", "There's a problem when saving the request.<br/>Please try input again", "");
                    }
                });

            <% } %>
        
            
        }
    });

    function onSelectFile(e) {
        var file = e.files[0];
        if (file.extension != ".xls" && file.extension != ".xlsx") {
            alert('Only excel file is accepted.');

            e.preventDefault();
        }
    }

    function onUploadData(e) {

    }

    function onUploadDataSuccess(e) {
        UploadControl_close();

        <%: this.ID %>_viewProjectManagementModuleModel.dsListKeyDeliverableModule.read();
    }

    function onUploadDataError(e) {
        _showDialogMessage("Error Message", "Upload Data Failed.<br/>Please check your input data!", "");
    }

    var btnSaveAllTop;
    var cutOfKey;
    $(document).ready(function () {
        $.ajax({
            url: _webApiUrl + "generalparameter/FindByTitle/cutofkeydeliverable" ,
            type: "GET",
            async: false,
            error: function (data) {

            },
            success: function (data) {
                cutOfKey = data[0].VALUE;
            }
        });
        btnSaveAllTop = $("#btnSaveAll").offset().top;

        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
    { %>
        <%: this.ID %>_viewProjectManagementModuleModel.set("isEnabledKeyDeliverable", true);
    <% } else { %>
        <%: this.ID %>_viewProjectManagementModuleModel.set("isEnabledKeyDeliverable", false);
    <% } %>

        <%: this.ID %>_mainProjectManagementModuleTabStrip = $("#<%: this.ID %>_mainProjectManagementModuleTabStrip").kendoTabStrip().data("kendoTabStrip");
        <%: this.ID %>_mainProjectManagementModuleTabStrip.select(0);
        var <%: this.ID %>_tabstrip = $("#<%: this.ID %>_mainProjectManagementModuleTabStrip").getKendoTabStrip();
        var onActivate = function (e) {
            var id = e.item.id;
            switch (id) {
                case "<%: this.ID %>_mainProjectManagementModuleTab2":
                    <%: this.ID %>_viewProjectManagementModuleModel.generateGridProjectKPIProgressModule();
                    break;
                default:
                    break;
            }
        }

        <%: this.ID %>_tabstrip.bind("activate", onActivate);
        <%: this.ID %>_viewProjectManagementModuleModel.dsStatusSource.read()

        kendo.bind($("#<%: this.ID %>_MainProjectManagementModuleContent"), <%: this.ID %>_viewProjectManagementModuleModel);

<%--        <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
        { %>
        $("#<%: this.ID %>_grdListKeyDeliverableModule").getKendoGrid().bind('remove', function (e) {
            if (!confirm("Are you sure you want to delete this record?")) {
                $('#<%: this.ID %>_grdListKeyDeliverableModule').getKendoGrid().cancelChanges();
            }
        });
        <% } %>--%>

        <%: this.ID %>_viewProjectManagementModuleModel.dsProjectManagementModule();
        <%: this.ID %>_viewProjectManagementModuleModel.dsListKeyDeliverableModule.read();

        $(".k-grid-upload").find('span').addClass("k-font-icon k-i-xls");
        $(".k-grid-upload").bind("click", function (e) {
            UploadControl_show("Upload Data Key Deliverable", "Click button below or drag the excel file. Click <a href='template/uploadKeyDeliverableTemplate.xlsx'>here</a> to get the excel form.",
                false, _webApiUrl + "ProjectManagement/upload/" + _projectId + "|" + _currNTUserID, onSelectFile, onUploadData, onUploadDataSuccess, onUploadDataError);

            e.preventDefault();
        });
        var dateNow = new Date();
        var plotBandDate = new Date();
        var s = <%: this.ID %>_viewProjectManagementModuleModel.dsProjectManagementModuleChart;
        plotBandDate.setTime(dateNow.getTime() + (1000 * 60 * 60 * 5));
        var dateTmp;
        $.ajax({
            url: _webApiUrl + "dashboard/currentprogress/" + _projectId,
            type: "GET",
            error: function (xhr, textStatus, error) {

            },
            success: function (progressResult) {

                $("#<%: this.ID %>_projectManagementModuleChart").kendoChart({
                    dataSource: <%: this.ID %>_viewProjectManagementModuleModel.dsProjectManagementModuleChart,
                    title: {
                        text: "Task Completeness " + _projectNo + " (" + _projectDesc + ")\n (Plan " + kendo.toString(dateNow, "MMMM") + ":" + (progressResult.percentagePlan ? +progressResult.percentagePlan.toFixed(2) : "0") + "% vs Actual to date:" + (progressResult.percentageActual ? +progressResult.percentageActual.toFixed(2) : "0") + "%)"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        type: "rangeBar"
                    },
                    series: [
                        {
                            name: "Plan Date",
                            fromField: "planStartDateRep",
                            toField: "planFinishDateRep"
                        },
                        {
                            name: "Actual Date",
                            fromField: "actualStartDateRep",
                            toField: "actualFinishDateRep"
                        }],
                    categoryAxis: {
                        name: "categoryAxis",
                        field: "name",
                        majorGridLines: {
                            visible: false
                        }
                    },
                    valueAxis: [{
                        name: "valueAxis1",
                
                        line: {
                            visible: true
                        },
                        labels: {
                            rotation: -10,
                            template: "#= kendo.toString(new Date(value), 'dd MMM yyyy') #",
                        }
                    },
                    {
                        name: "valueAxis2",
                        majorUnit: 24 * 60 * 60 * 1000, // 30 days in milliseconds
                        majorGridLines: {
                            visible: false,
                        },
                        line: {
                            visible: false
                        },
                        labels: {
                            visible: false
                        },

                    }],
                    render: function(e) {
                        var valueAxis = e.sender.getAxis("valueAxis2");
                        var valueSlot = valueAxis.slot(dateNow);

                        var categoryAxis = e.sender.getAxis("categoryAxis");
                        var lastCategoryIndex = Math.max(1, categoryAxis.range().max);
                        var minCategorySlot = categoryAxis.slot(0);
                        var maxCategorySlot = categoryAxis.slot(lastCategoryIndex);

                        var line = new kendo.drawing.Path({
                            stroke: {
                                color: "red",
                                width: 1
                            }
                        });
                        line.moveTo([valueSlot.origin.x, minCategorySlot.origin.y]).lineTo([valueSlot.origin.x, maxCategorySlot.origin.y]);

                        var n = new kendo.geometry.Transformation();
                        n.rotate(90, [valueSlot.origin.x + 16, minCategorySlot.origin.y + Math.round((maxCategorySlot.origin.y - minCategorySlot.origin.y) / 2)]);
                        var labelPos = [valueSlot.origin.x, minCategorySlot.origin.y + Math.round((maxCategorySlot.origin.y - minCategorySlot.origin.y) / 2)];
                        var label = new kendo.drawing.Text("(Today)", labelPos, {
                            transform: n,
                            fill: {
                                color: "red"
                            },
                            font: "12px sans"
                        });

                        var group = new kendo.drawing.Group();
                        group.append(line, label);
                        e.sender.surface.draw(group);
                    },
                    tooltip: {
                        visible: true,
                        position: "top",
                        template: "#= kendo.toString(new Date(value.from), 'dd MMM yyyy') # - #= kendo.toString(new Date(value.to), 'dd MMM yyyy') #"
                    }
                });
            }
        });
        

        $("#<%: this.ID %>_KPIGraphChartModule").kendoChart({
            dataSource: <%: this.ID %>_viewProjectManagementModuleModel.dsKPIGraphChartModule,
            title: {
                text: "KPI Graph (%) " + _projectNo + " (" + _projectDesc + ")"
            },
            legend: {
                position: "bottom"
            },
            series: [{
                type: "line",
                field: "planPercentage",
                name: "Plan",
                style: "smooth",
                date: "dateProgress",
                //noteTextField: "extremum",
                //notes: {
                //    label: {
                //        position: "outside"
                //    },
                //    position: "bottom"
                //}
            }, {
                type: "line",
                field: "actualPercentage",
                name: "Actual",
                style: "smooth",
                date: "dateProgress",
            }, {
                type: "rangeColumn",
                name: "Plan vs Actual",
                colorField: "valueColor",
                fromField: "planPercentage",
                toField: "actualPercentage",
                date: "dateProgress",
                gap: 100
            }],
            valueAxis: {
                labels: {
                    format: "{0}%"
                },
                majorGridLines: {
                    visible: true
                }, // 30 days in milliseconds
                line: {
                    visible: false
                }
            },
            categoryAxis: {
                field: "dateProgress",
                majorGridLines: {
                    visible: false
                }, // 30 days in milliseconds
                majorUnit: 24 * 60 * 60 * 1000,
                labels: {
                    rotation: -45, 
                    template: "#= kendo.toString(new Date(value), 'MMM yyyy') #",
                    step: 0
                }
            },
            tooltip: {
                visible: true,
                template: function (e) {
                    //var result "# series.name == 'Plan vs Actual' ? { # #= series.name#: #= value #% # } : {# #=series.name # # } #"
                    return kendo.toString(new Date(e.category), 'MMM yyyy') + "<br/>" + e.series.name + (e.series.name == 'Plan vs Actual' ?
                        ': ' + e.value.from + '% vs ' + e.value.to + '%' : ': ' + e.value + '%');
                }
            }
        });

        <%: this.ID %>_popProjectManagementModuleChart = $("#<%: this.ID %>_popProjectManagementModuleChart").kendoWindow({
            title: "View Graph",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 480
        }).data("kendoWindow");

        <%: this.ID %>_popKPIGraphChartModule = $("#<%: this.ID %>_popKPIGraphChartModule").kendoWindow({
            title: "View Graph",
            modal: true,
            visible: false,
            resizable: false,
            width: 800,
            height: 480
        }).data("kendoWindow");
 
    });

    window.calculateActualProgress = function (field) {
        var grid = $('#<%: this.ID %>_grdListProjectKPIGraph').data("kendoGrid");
        if (grid == null) return;
        
        var columnName = "actual";
        var posIndex = field.indexOf("_");
        if (posIndex != 6) columnName = "plan";
        var getIndexColumn = parseInt(field.replace(columnName + "_", ""));

        var ds = grid.dataSource;
        if (ds == null) return;

        var data = ds.data();
        if (data == null) return;

        var total = 0;
        if (data) {
            for (var i = 0; i < data.length; i++) {
                var totalProgress = 0;
                var lastTotalProgrss = 0;
                for (var j = 0; j <= getIndexColumn; j++)
                {
                    //totalProgress += isNaN(parseFloat(data[i][columnName + "_" + j])) ? 0 : parseFloat(data[i][columnName + "_" + j]);
                    //diganti jadi
                    if (isNaN(parseFloat(data[i][columnName + "_" + j])) && j > 0)
                    {
                        totalProgress = lastTotalProgrss;
                    } else {
                        totalProgress = isNaN(parseFloat(data[i][columnName + "_" + j])) ? 0 : parseFloat(data[i][columnName + "_" + j]);
                        lastTotalProgrss = totalProgress;
                    }
                }
                total += totalProgress * (data[i]["weight"] / 100);
            }
        }

        return total.toFixed(1);
    };
    function scrollFixed() {
        var offsetTop = $(this).scrollTop(),
            offsetLeft = $(this).scrollLeft(),
            tableOffsetTop = btnSaveAllTop,
            tableOffsetBottom = tableOffsetTop + $("#<%: this.ID %>_mainProjectManagementModuleTabStrip").height() - $("#btnSaveAll").height();
        var grdListHeight = $("#<%: this.ID %>_grdListKeyDeliverableModule").height();
        var nvar = $(".content-wrapper").height() - grdListHeight + 50;
        if (offsetTop < (tableOffsetTop - nvar) || offsetTop > (tableOffsetBottom - nvar)) {
            $("#btnSaveAllWrap").removeClass("fixed-header");
            $("#btnSaveAllWrap").removeAttr("style");
            $("#btnSaveAll").removeAttr("style");
        } else if (offsetTop >= (tableOffsetTop - $(".content-wrapper").height()) && offsetTop <= (tableOffsetBottom - $(".content-wrapper").height()) && !$("#btnSaveAll").hasClass("fixed")) {
            $("#btnSaveAll").css("overflow", "hidden");
            $("#btnSaveAll").css("background-color", "red");
            $("#btnSaveAll").css("border-radius", "0px 0px 5px 5px");
            $("#btnSaveAll").css("padding", "30px");
            $("#btnSaveAll").css("padding-right", "5px");
            $("#btnSaveAll").css("padding-top", "30px");
            $("#btnSaveAll").css("padding-bottom", "5px");
            $("#btnSaveAll").css("display", "inline-box");
            $("#btnSaveAll").css("float", "right");
            $("#btnSaveAll").css("box-shadow", "1px 1px 3px");
            $("#btnSaveAllWrap").addClass("fixed-header");
            $("#btnSaveAllWrap").css("top", $(".content-wrapper").height()-14);
            $("#btnSaveAllWrap").css("width", "91%");
        }
    }

    $(window).scroll(scrollFixed);
</script>

<script id="<%: this.ID %>_editGrdListKeyDeliverableModule" type="text/x-kendo-tmpl">
    <div style="padding:10px">            
        <table style="width:600px">
            <tr>
                <td style="width:100px">Key Deliverables<span style="color:red">*</span></td>
                <td><div id="keyDel" name="name"></div>
                    <%--<input name="name" required='required' id="keyDel" data-role="combobox"
                   data-placeholder="Select Deliverable"
                   data-value-primitive="true"
                   data-text-field="keyDeliverables"
                   data-value-field="id"
                   data-bind="source: dsKeyDList"
            style="width: 250px" />--%>
            </td>
            </tr>
            <tr>
                <td>% Weight<span style="color:red">*</span></td>
<%--                <td style="width:250px"><input type="number" class="k-textbox" name="weightPercentage" required="required" min=1 validationMessage="Weight is required" placeholder="Please Input Weight" style="width:131px" /></td>--%>
                <td style="width:250px"><input data-role="numerictextbox" min="0" max="100" name="weightPercentage" required="required" min=1 validationMessage="Weight is required" placeholder="Please Input Weight" style="width:131px" /></td>
            </tr>
            <tr>
                <td>Plan Start Date<span style="color:red">*</span></td>
                <td><input data-role="datepicker" onchange="<%: this.ID %>_viewProjectManagementModuleModel.onChangePlanStartDate(this)" data-format="dd MMM yyyy" id="<%: this.ID %>_planStartDateTmp" name="planStartDate" required validationMessage="Plan Start Date is required" placeholder="Please Input Plan Start" style="width:180px" /></td>
            </tr>
            <tr>
                <td>Plan Finish Date<span style="color:red">*</span></td>
                <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_planFinishDateTmp" name="planFinishDate" required validationMessage="Plan Finish Date is required" placeholder="Please Input Plan Finish" style="width:180px" /></td>
            </tr>
    # if (id) { #
            <tr>
                <td>Actual Start Date</td>
                <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualStartDateTmp" name="actualStartDate" placeholder="Please Input Actual Start" style="width:180px" /></td>
            </tr>
            <tr>
                <td>Actual Finish Date</td>
                <td><input data-role="datepicker" data-format="dd MMM yyyy" id="<%: this.ID %>_actualFinishDateTmp" name="actualFinishDate" placeholder="Please Input Actual Finish" style="width:180px" /></td>
            </tr>
    # } #
            <tr>
                <td>Status</td>
                <td><input id="ddStatus" data-role="dropdownlist" data-option-label="Please Select Status" data-bind="source: dsStatusSource, value: statusValue" data-text-field="text" data-value-field="value" name="status" style="width:100%" data-value-primitive="true" /></td>
            </tr>
            <tr>
                <td>Remark</td>
                <td><textarea name="remark" placeholder="Please Input Remark" style="width:300px" /></td>
            </tr>
        </table>
    </div>
</script>

<div id="<%: this.ID %>_MainProjectManagementModuleContent">
    <uc1:UploadControl runat="server" ID="UploadControl" />
    <div id="<%: this.ID %>_popProjectManagementModuleChart">
        <div id="<%: this.ID %>_projectManagementModuleChart"></div>
        <br />
        <div style="float: right;"
            data-role="button"
            data-bind="events: { click: onViewChartKeyDeliverableClose }"
            class="k-button">
            <span class="k-icon k-i-close"></span>Close Chart
        </div>

    </div>
    <div id="<%: this.ID %>_popKPIGraphChartModule">
        <div id="<%: this.ID %>_KPIGraphChartModule"></div>
         <br />
        <div style="float: right;"
            data-role="button"
            data-bind="events: { click: onViewChartKPIProgressClose }"
            class="k-button">
            <span class="k-icon k-i-close"></span>Close Chart
        </div>
   </div>
    <div id="<%: this.ID %>_mainProjectManagementModuleTabStrip">
        <ul>
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
            { %>
            <li id="<%: this.ID %>_mainProjectManagementModuleTab1">Management Module</li>
            <% }%>
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ProjectKPIGraph))
            { %>
           <li id="<%: this.ID %>_mainProjectManagementModuleTab2">KPI Graph</li>
            <% }%>
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ProjectScopeDefinition))
            { %>
           <li id="<%: this.ID %>_mainProjectScopeDefinition">Project Scope Definition</li>
            <% }%>
            <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ProjectBudgetApproval)&& _currProjectType=="OPERATING")
            { %>
           <li id="<%: this.ID %>_mainProjectManagementModuleTab3">Project Budget Approval</li>
            <% }%>
        </ul>
        <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
        { %>
        <div>
            <div class="headerSubProject">Key Deliverable</div>
            <div style="display: inline-block">
                <div id="<%: this.ID %>_grdListKeyDeliverableModule"
                    data-toolbar='[<% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                    { %>{name:"create", text:"Add Key Deliverable"}, { name:"upload", text:"Upload Data (xlsx)" }, <% } %> {name:"excel"}]'
                    data-excel= '{ fileName: "Key Deliverable.xlsx", allPages: true }',
                    data-role="grid"
                    data-sortable="{ mode: 'multiple' }"
                    data-columns="[
                        {
                            field: 'nameText', title: 'Name',
                            width: 110
                        },
                        {
                            field: 'weightPercentage', title: '% Weight',
                            width: 75
                        },
                        {
                            field: 'planStartDate', title: 'PSD', format: '{0: dd MMM yyyy}',
                            width: 95
                        },
                        {
                            field: 'planFinishDate', title: 'PFD', format: '{0: dd MMM yyyy}',
                            width: 95
                        },
                        {
                            field: 'revision', title: 'Rev',
                            width: 45
                        },
                        {
                            field: 'actualStartDate', title: 'ASD', format: '{0: dd MMM yyyy}',
                            width: 95
                        },
                        {
                            field: 'actualFinishDate', title: 'AFD', format: '{0: dd MMM yyyy}',
                            width: 95
                        },
                        {
                            field: 'status', title: 'Status',
                            template: '#:common_getStatusIpromDesc(status)#',
                            width: 110
                        },
                        {
                            field: 'remark', title: 'Remark', template: kendo.template($('#<%: this.ID %>_remark-template').html()),
                            width: 110
                        }
                        
                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                    { %>
                        ,{
                            command: ['edit', 'destroy']
                        }
                    <% } %>
                    ]"
                    data-bind="source: dsListKeyDeliverableModule, events: { edit: editKeyDeliverable, save: saveKeyDeliverable, excelExport: excelKeyDeliverable }"
                    data-editable='{"mode": "popup", "template": kendo.template($("#<%: this.ID %>_editGrdListKeyDeliverableModule").html())}'
                    data-pageable="{ buttonCount: 5 }">
                </div>
                <br />
                <div style="float: right;"
                    data-role="button"
                    data-bind="events: { click: onViewChartKeyDeliverableClick }"
                    class="k-button">
                    <span class="k-icon k-i-sort-asc"></span>View Chart
                </div>

            </div>
            <br />
            <br />
            <br />
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Manage Key Issue</legend>
            <div id="btnSaveAllWrap">
                <div id="btnSaveAll">
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: saveAll }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save All
                    </div>
                </div> 
            </div> 
            <br />
                    <textarea data-bind="value: manageKeyIssue, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>
                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;" id="btnSaveManageKeyIssue"
                        data-role="button"
                        data-bind="events: { click: onSaveManageKeyIssueClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelManageKeyIssueClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <br />
              <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Safety Perfomance</legend>
                    <textarea data-bind="value: safetyPerfomance, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveSafetyPerfomanceClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelSafetyPerfomanceClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div> 
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Scope</legend>
                    <textarea data-bind="value: scope, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button" id="btnSaveScopeClick"
                        data-bind="events: { click: onSaveScopeClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelScopeClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
              <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Change Request</legend>
                    <textarea data-bind="value: changeRequest, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveChangeRequestClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelChangeRequestClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <br />
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Objective</legend>
                    <textarea data-bind="value: benefit, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveBenefitClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelBenefitClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <br />
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Accomplishment</legend>
                    <textarea data-bind="value: accomplishment, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveAccomplishmentClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelAccomplishmentClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <br />
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Next Plan</legend>
                    <textarea data-bind="value: details, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveDetailsClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelDetailsClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <br />
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">EHS Records</legend>
                    <textarea data-bind="value: ehsRecords, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveEHSRecordsClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelEHSRecordsClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
            <div>
                <fieldset class="gdiFieldset">
                    <legend class="gdiLegend">Contractor</legend>
                    <textarea data-bind="value: contractor, enabled: isEnabledKeyDeliverable" class="k-textbox"
                        style="height: 60px; width: 100%;" maxlength="2000" required></textarea>

                        <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.PMModul))
                            { %>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onSaveContractorClick }"
                        class="k-button">
                        <span class="k-icon k-i-save"></span>Save
                    </div>
                    <div style="float: right;"
                        data-role="button"
                        data-bind="events: { click: onCancelContractorClick }"
                        class="k-button">
                        <span class="k-icon k-i-cancel"></span>Cancel
                    </div>
                    <% } %>
                </fieldset>
            </div>
        </div>
        <% }%>
        <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ProjectKPIGraph))
        { %>
        <div>
            <div class="headerSubProject">KPI Graph Monthly</div>
            <%--            <div style="display: inline-block">--%>
            <div>
                <div id="<%: this.ID %>_grdListProjectKPIGraph" style="width:800px;"></div>
                <br />
                <div id="btnAddMonth" style="float: right;"
                    data-role="button"
                    data-bind="events: { click: addAdditionalMonth }"
                    class="k-button">
                    <span class="k-icon"></span>Add Additional Month
                </div>
                <div style="float: right;"
                    data-role="button"
                    data-bind="events: { click: onViewChartKPIProgressClick }"
                    class="k-button">
                    <span class="k-icon k-i-sort-asc"></span>View Chart
                </div>

            </div>
        </div>
        <% }%>
        <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ProjectScopeDefinition))
        { %>
        <div>
            <uc1:PSDFormControl runat="server" ID="PSDFormControl" />
        </div>
        <% }%>
        <% if (IsAllowedTo(PTVI.iPROM.WebApp.AccessRights.ProjectBudgetApproval) && _currProjectType=="OPERATING")
        { %>
        <div>
           <uc1:ProjectBudgetApprovalControl runat="server" ID="ProjectBudgetApprovalControl" />
        </div>
         <% }%>
    </div>
</div>

<script id="<%: this.ID %>_remark-template" type="text/x-kendo-template">

    #= data.remark?data.remark.replace(/(\r?\n)/gi,"<br>"):"" #

</script>
    <style>
     .fixed-header {
        position:fixed;
        width:auto;
        z-index: 1;
      }
    </style>