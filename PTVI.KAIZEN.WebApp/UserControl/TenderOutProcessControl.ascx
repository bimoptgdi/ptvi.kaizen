﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TenderOutProcessControl.ascx.cs" Inherits="PTVI.iPROM.WebApp.UserControl.TenderOutProcessControl" %>

<script>
    var <%: this.ID %>_popInputTenderNo;
    var <%: this.ID %>_viewTenderOutProcessModel = kendo.observable({
        dsTenderOutProcess: new kendo.data.DataSource({
            requestStart: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTenderOutProcessControl"), true);
            },
            requestEnd: function () {
                kendo.ui.progress($("#<%: this.ID %>_grdTenderOutProcessControl"), false);
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        docNo: { editable: false },
                        docTitle: { editable: false },
                        documentByName: { editable: false },
                        project: { editable: false }
                    }
                }
            },
            transport: {
                read: {
                    url: _webApiUrl + "Tenders/listTenderOutProcessByProjectId/" + _projectId,
                    dataType: "json"
                },
                update: {
                    url: function (data) {
                       // return _webApiUrl + "ProjectDocuments/PDAssignExecutorforEWPUpdate/" + data.id;
                    },
                    type: "PUT"
                },
                parameterMap: function (data, type) {
                    if (type == "create" || type == "update") {
                        data.planStartDate = kendo.toString(data.planStartDate, 'yyyy-MM-dd');
                        data.planFinishDate = kendo.toString(data.planFinishDate, 'yyyy-MM-dd');
                        data.actualStartDate = kendo.toString(data.actualStartDate, 'yyyy-MM-dd');
                        data.actualFinishDate = kendo.toString(data.actualFinishDate, 'yyyy-MM-dd');
                    }
                    return data;
                }
            },
            pageSize: 10,
            sort: [{ field: "docType", dir: "asc" }]
        }),
        tenderNo: "",
        onTenderOutClick: function (e) {
            var data = this.dsTenderOutProcess.data();
            var totalIsCheck = Enumerable.From(data).Where(function (x) { return (x.isActive == true); }).ToArray().length;
            if (totalIsCheck > 0)
                <%: this.ID %>_popInputTenderNo.center().open();
            else
                _showDialogMessage("Alert", "Please check 1 or more Doc No");
        },
        onCheckDetail: function (e) {
            e.data.set("isActive", e.target.checked)
        },
        onSaveClick: function (e) {
            var that = this;

            data = Enumerable.From(this.dsTenderOutProcess.data()).Where(function (x) { return (x.isActive == true); }).
                Select(function (x) {
                    return {
                        'id': x['id']
                    };
                }).ToArray();

            kendo.ui.progress($("#<%: this.ID %>_grdTenderOutProcessControl"), true);

            $.ajax({
                url: _webApiUrl + "Tenders/tederOutSubmit/" + that.tenderNo,
                type: "put",
                data: JSON.stringify(data),
                contentType: "application/json",
                async: true,
                success: function () {
                    that.dsTenderOutProcess.read();
                    that.set("tenderNo", "");
                    kendo.ui.progress($("#<%: this.ID %>_grdTenderOutProcessControl"), false);
                },
                error: function () {
                    kendo.ui.progress($("#<%: this.ID %>_grdTenderOutProcessControl"), false);
                }
            });

            <%: this.ID %>_popInputTenderNo.close();
        },
        onCancelClick: function (e) {
            <%: this.ID %>_popInputTenderNo.close();
        }
    });

    $(document).ready(function () {
        kendo.bind($("#<%: this.ID %>_TenderOutProcessControl"), <%: this.ID %>_viewTenderOutProcessModel);
        <%: this.ID %>_viewTenderOutProcessModel.dsTenderOutProcess.read();

        $("#<%: this.ID %>_checkAll").on("click", function () {
            var state = $("#<%: this.ID %>_checkAll").is(':checked');
            var data = <%: this.ID %>_viewTenderOutProcessModel.dsTenderOutProcess.data();
            $.each(data, function () {
                if (this['isActive'] != state)
                this.set('isActive', state);
            });
        });

        <%: this.ID %>_popInputTenderNo = $("#<%: this.ID %>_popInputTenderNo").kendoWindow({
            title: "Input Tender No",
            modal: true,
            visible: false,
            resizable: false,
            width: 400,
            height: 100
        }).data("kendoWindow");


    });

</script>

<div id="<%: this.ID %>_TenderOutProcessControl">
    <div id="<%: this.ID %>_popInputTenderNo">
        <div>
            <label class="labelForm">Tender No</label>
            <input class="k-textbox" type="text" name="tenderNo"  data-bind="value: tenderNo" placeholder="Tender No" />
        </div>
        <br />
        <div>
            <div style="float: right;">
                &nbsp;&nbsp;
                <div
                    data-role="button"
                    data-bind="events: { click: onCancelClick }"
                    class="k-button k-primary">
                    Cancel
                </div>
            </div>
            <div style="float: right;">
                <div
                    data-role="button"
                    data-bind="events: { click: onSaveClick }"
                    class="k-button k-primary">
                    Save
                </div>
            </div>
        </div>
    </div>
    <div class="headerSubProject">Contract Admin</div>
    <div id="<%: this.ID %>_grdTenderOutProcessControl" 
        data-role="grid"
        data-sortable="true"
        data-editable="false"
        data-columns="[
            <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageTender))
                    { %>{
                title: 'Select', 
                headerAttributes: { style: 'text-align: center;'}, 
                headerTemplate: kendo.template($('#selectTenderOutProcess-headerTemplate').html()),
                attributes: { style: 'text-align: center;'}, 
                template: kendo.template($('#selectTenderOutProcess-template').html()),
                width: 50
            },<% }%>
            {
                field: 'docNo', title: 'Doc No',
                width: 70
            },
            {
                field: 'docTitle', title: 'Doc Title',
                width: 250
            },
            {
                field: 'project', title: 'Project Manager',
                template: '#:project.projectManagerName#',
                width: 110
            },
            {
                field: 'documentByName', title: 'Engineer',
                width: 110
            },
            {
                field: 'executorAssignmentText', title: 'Executor',
                width: 110
            }
        ]"
        data-bind="source: dsTenderOutProcess"
        data-pageable="{ buttonCount: 5 }">
    </div>
    <br />
    <% if (!IsReadOnlyTo(PTVI.iPROM.WebApp.AccessRights.ManageTender))
                    { %><div style="float: right;"
        data-role="button"
        data-bind="events: { click: onTenderOutClick }"
        class="k-button">
    <span class="k-icon k-i-group"></span>Tender Out
    </div><% }%>
    <script type="text/x-kendo-template" id="selectTenderOutProcess-headerTemplate">
        <div>
            <input type=checkbox id='<%: this.ID %>_checkAll' />
        </div>
    </script>
    <script type="text/x-kendo-template" id="selectTenderOutProcess-template">
        <div>
            <input type='checkbox' #: isActive ? "checked" : "" # class='checkbox' data-bind="events: { click: onCheckDetail }" />
        </div>
    </script>
</div>
