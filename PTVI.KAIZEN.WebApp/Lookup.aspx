﻿<%@ Page Title="Lookup" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LookupForm.aspx.cs" Inherits="PTVI.KAIZEN.WebApp.LookupForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <script src="Scripts/kendo/vue/kendo-datasource-vue-wrapper.js"></script>
    <script src="Scripts/kendo/vue/kendo-grid-vue-wrapper.js"></script>
    <script src="Scripts/kendo/vue/kendo-dropdowns-vue-wrapper.js"></script>
    <script src="https://unpkg.com/vue/dist/vue.min.js"></script>
    <script src="https://unpkg.com/@progress/kendo-grid-vue-wrapper/dist/cdn/kendo-grid-vue-wrapper.js"></script>

    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.913/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.913/styles/kendo.default.min.css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div id="vueapp" class="vue-app">
    <Grid ref="grid"
          :style="{height: '440px'}"
          :data-bind="source: dsLookup"
          :edit-field="'inEdit'"
          @edit="onEditLookup"
          @remove="remove"
          @save="onSaveLookup"
          @cancel="cancel"
          @itemchange="itemChange"
          :columns="columns">
        <grid-toolbar>
            <button title="Add new"
                    class="k-button k-primary"
                    @click='insert' >
                Add new
            </button>
            <button v-if="hasItemsInEdit"
                    title="Cancel current changes"
                    class="k-button"
                    @click="cancelChanges">
                    Cancel current changes
            </button>
        </grid-toolbar>
        <grid-norecords>
            There is no data available custom
        </grid-norecords>
    </Grid>
</div>
    <script>
        
        $(document).ready(function () {
            new Vue({
                el: "#grdLookup",
                data: function () {
                    return {
                        dsLookup: new kendo.data.DataSource({
                            transport: {
                                read: _apiInfo.webApiUrl + "lookup",
                                create: {
                                    url: _apiInfo.webApiUrl + "lookup",
                                    type: "POST"
                                },
                                update: {
                                    url: function (data) {
                                        return _apiInfo.webApiUrl + "lookup?type=" + data.TYPE + "&value=" + data.VALUE;
                                    },
                                    type: "PUT"
                                },
                                destroy: {
                                    url: function (data) {
                                        return _apiInfo.webApiUrl + "lookup?type=" + data.TYPE + "&value=" + data.VALUE;
                                    },
                                    type: "DELETE"
                                }
                            },
                            schema: {
                                parse: function (responses) {
                                    for (var i = 0; i < responses.length; i++) {
                                        responses[i]["id"] = responses[i].TYPE + "_" + responses[i].VALUE;
                                    }

                                    return responses;
                                },
                                model: {
                                    id: "id",
                                    fields: {
                                        TYPE: { validation: { required: true } },
                                        VALUE: { validation: { required: true } },
                                        ISACTIVE: { type: 'boolean' }
                                    }
                                }
                            },
                            pageSize: 5
                        }),
                        grdColoumn: [
                    { field: 'TYPE', title: 'Type', filterable: true, editor: lookupTypeEditor },
                    { field: 'VALUE', title: 'Value', filterable: true },
                    { field: 'TEXT', title: 'Text', filterable: true },
                    { field: 'DESCRIPTION', title: 'Description', filterable: true, hidden: true },
                    { field: 'ISACTIVE', title: 'Active', filterable: true, template: '<input type=\'checkbox\' #: ISACTIVE ? \'checked\' : \'\' #  disabled />' },
                    {
                        command: ['edit', 'destroy'], title: '&nbsp;', width: '200px'
                    },
                    ]
                    }
                },
                onEditLookup: function (e) {
                    if (e.model.id == "")
                        e.model.set("ISACTIVE", true);

                    var editWindow = e.sender.editable.element.data("kendoWindow");
                    editWindow.wrapper.css({ width: 700 });

                    var editContainer = e.sender.editable.element.find(".k-edit-form-container");
                    editContainer.css({ width: "100%" });

                    var inputDescription = editContainer.find('input[name="DESCRIPTION"]');
                    inputDescription.css({ width: "100%" });

                    if (e.model.id != "") {
                        $("[name='TYPE']").prop("disabled", true);
                        $("[name='VALUE']").prop("disabled", true);
                    }

                    editWindow.center();
                },
                onSaveLookup: function (e) {
                    Vue.set(e.dataItem, 'inEdit', undefined);
                    let bind = this.dsLookup.find(p => p.Type === e.dataBind.Type);
                    let index = this.dsLookup.findIndex(p => p.Type === item.Type);

                    Vue.set(this.dsLookup, index, this.update(this.dsLookup.slice(), bind));
                    Vue.set(this.dsLookup[index], 'inEdit', undefined);
                    this.updatedData = JSON.parse(JSON.stringify(this.dsLookup));
                },
            })
        });
        function lookupTypeEditor(container, options) {
            var lookupTypes = Enumerable.From(vm.dsLookup.data()).Distinct("$.TYPE").Select("$.TYPE").ToArray();
            console.log(lookupTypes);

            var ms = $('<input style="width: 80%"  required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoAutoComplete({
                    //dataTextField: "FULL_NAME",
                    //dataValueField: "USERNAME",
                    //valuePrimitive: true,
                    filter: "contains",
                    dataSource: lookupTypes,
                    suggest: true,
                    value: options.model.TYPE
                });
        }
    </script>
</asp:Content>
