﻿
var viewModel = kendo.observable({
    showResetPassword: false,
    showLogin: true,
    showSuccessfullRegistration: false,
    resetPasswordErrorMessage: null,
    resetPasswordEmail: null,
    resetPasswordConfirmationMessage: null,
    local: function (key) {
        //return getResource(key);
    },
    login: function (e) {
        e.preventDefault();
        var isValid = val.validate();

        if (isValid) {
            onLogin();
        }
    },
    resetPassword: function (e) {
        e.preventDefault();
        var isValid = val.validate();
        var that = this;

        if (isValid) {

            $.ajax({
                url: _apiInfo.webApiUrl + "login/resetPassword/" + that.resetPasswordEmail + "/",
                dataType: "json",
                contentType: "application/json",
                type: "GET",
                async: true
            }).done(function (response) {
                that.set("resetPasswordConfirmationMessage", "Your new password will be sent to : " + that.resetPasswordEmail);
                $("#dvResetPassword").fadeOut(500, function () {
                    $("#dvConfirmation").fadeIn(500);
                });
            }).fail(function (error) {
                if (error.responseJSON) {                    
                    that.set("resetPasswordErrorMessage", error.responseJSON.Message);
                } else {
                    that.set("resetPasswordErrorMessage", "Error resetting the password. Please contact the system administrator.");
                }
            });
        }
    },
    openResetPassword: function (e) {
        e.preventDefault();
        this.set("showResetPassword", true);
        this.set("showLogin", false);
    },
    cancelReset: function (e) {
        e.preventDefault();
        this.set("showResetPassword", false);
        this.set("showLogin", true);
    },
    register: function () {
        window.location.href = "Registration.aspx";
    },
    onActivate: function () {
        var that = this;

        //var registered = getQueryString("registered");

		//		if (registered)
        //{
        //    that.set("showSuccessfullRegistration", true);
        //}
    }
});

$(document).ready(function () {
    kendo.bind($("#dvContainer"), viewModel);

    $("#txtPassword").keypress(function (e) {
        if (e.which == 13) {
            viewModel.login(e);
        }
    });

    viewModel.onActivate();
});

var errorTemplate = '<div class="k-widget k-tooltip k-tooltip-validation"' +
    'style="margin:0.5em"><span class="k-icon k-i-warning"> </span>' +
    '#=message#<div class="k-callout k-callout-n"></div></div>'

var val = $("#Form1").kendoValidator({
    errorTemplate: errorTemplate,
    messages: {
        userNameRequired: "",//getResource("userNameValidation"),
        passwordRequired: "",//getResource("passwordValidation"),
        emailRequired: "",//getResource("emailValidation"),
        verificationCodeRequired: "",//getResource("verificationCodeValidation"),
    },
    rules: {
        userNameRequired: function (input) {
            if (viewModel.showLogin) {
                if (input.is("[name=txtUsername]")) {

                    return input.val() != '';
                }
            }
            return true;
        },
        passwordRequired: function (input) {
            if (viewModel.showLogin) {
                if (input.is("[name=txtPassword]")) {

                    return input.val() != '';
                }
            }
            return true;
        },
        emailRequired: function (input) {
            if (viewModel.showResetPassword) {
                if (input.is("[name=txtEmail]")) {

                    return input.val() != '';
                }
            }
            return true;
        },
        verificationCodeRequired: function (input) {
            if (viewModel.showLogin) {
                if (input.is("[name=txtVerificationCode]")) {

                    return input.val() != '';
                }
            }
            return true;
        }
    }

}).data("kendoValidator");