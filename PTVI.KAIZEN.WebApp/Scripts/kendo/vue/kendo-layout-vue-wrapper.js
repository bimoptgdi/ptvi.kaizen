(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vue"));
	else if(typeof define === 'function' && define.amd)
		define(["vue"], factory);
	else if(typeof exports === 'object')
		exports["KendoLayoutVueWrapper"] = factory(require("vue"));
	else
		root["KendoLayoutVueWrapper"] = factory(root["Vue"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_3__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KendoSharedMethods = exports.KendoBaseInputComponent = exports.KendoBaseDatasourceComponent = exports.KendoBaseComponent = undefined;

var _kendoBaseComponent = __webpack_require__(14);

var _kendoBaseComponent2 = _interopRequireDefault(_kendoBaseComponent);

var _kendoBaseDatasourceComponent = __webpack_require__(15);

var _kendoBaseDatasourceComponent2 = _interopRequireDefault(_kendoBaseDatasourceComponent);

var _kendoBaseInputComponent = __webpack_require__(16);

var _kendoBaseInputComponent2 = _interopRequireDefault(_kendoBaseInputComponent);

var _kendoSharedMethods = __webpack_require__(17);

var _kendoSharedMethods2 = _interopRequireDefault(_kendoSharedMethods);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.KendoBaseComponent = _kendoBaseComponent2.default;
exports.KendoBaseDatasourceComponent = _kendoBaseDatasourceComponent2.default;
exports.KendoBaseInputComponent = _kendoBaseInputComponent2.default;
exports.KendoSharedMethods = _kendoSharedMethods2.default;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _PaneProps = __webpack_require__(31);

var _PaneProps2 = _interopRequireDefault(_PaneProps);

var _vue = __webpack_require__(3);

var _vue2 = _interopRequireDefault(_vue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KendoSplitterPane = {
    name: 'kendo-splitter-pane',
    render: function render() {
        return _vue2.default.prototype._e;
    },

    mixins: [_PaneProps2.default]
};

exports.default = KendoSplitterPane;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _KendoMenuMixin = __webpack_require__(13);

var _KendoMenuMixin2 = _interopRequireDefault(_KendoMenuMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'kendo-menu',
    mixins: [_KendoMenuMixin2.default]
};

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        animationCloseEffects: {
            type: String,
            kComposite: 'animation.close.effects'
        },
        animationCloseDuration: {
            type: Number,
            kComposite: 'animation.close.duration'
        },
        animationOpenEffects: {
            type: String,
            kComposite: 'animation.open.effects'
        },
        animationOpenDuration: {
            type: Number,
            kComposite: 'animation.open.duration'
        }
    }
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        scrollableDistance: {
            type: Number,
            kComposite: 'scrollable.distance'
        }
    }
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

exports.default = {
    name: 'kendo-menu-item',
    template: '<span><slot></slot></span>',
    mixins: [_kendoBaseComponentsVueWrapper.KendoSharedMethods],
    props: {
        text: String,
        cssClass: String,
        url: String,
        attr: Object,
        encoded: Boolean,
        content: String,
        contentAttr: Object,
        imageAttr: Object,
        imageUrl: String,
        items: Array,
        spriteCssClass: String,
        select: Function
    },
    mounted: function mounted() {
        this._resolveInnerChildren();
    },

    methods: {
        _resolveInnerChildren: function _resolveInnerChildren() {
            if (!this.$options.propsData['items'] && this.$slots.default) {
                var items = this.resolveInnerTags('kendo-menu-item');
                this.subitems = items;
            }
        }
    }
};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _KendoContextMenuMixin = __webpack_require__(20);

var _KendoContextMenuMixin2 = _interopRequireDefault(_KendoContextMenuMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'kendo-contextmenu',
    mixins: [_KendoContextMenuMixin2.default]
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _KendoPanelBarMixin = __webpack_require__(23);

var _KendoPanelBarMixin2 = _interopRequireDefault(_KendoPanelBarMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'kendo-panelbar',
    mixins: [_KendoPanelBarMixin2.default]
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

exports.default = {
    name: 'kendo-panelbar-item',
    template: '<span><slot></slot></span>',
    mixins: [_kendoBaseComponentsVueWrapper.KendoSharedMethods],
    props: {
        text: String,
        cssClass: String,
        url: String,
        encoded: Boolean,
        content: String,
        contentUrl: String,
        imageUrl: String,
        items: Array,
        expanded: Boolean,
        spriteCssClass: String
    },
    mounted: function mounted() {
        this._resolveInnerChildren();
    },

    methods: {
        _resolveInnerChildren: function _resolveInnerChildren() {
            if (!this.$options.propsData['items'] && this.$slots.default) {
                var items = this.resolveInnerTags('kendo-panelbar-item');
                this.subitems = items;
            }
        }
    }
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _KendoTabStripMixin = __webpack_require__(27);

var _KendoTabStripMixin2 = _interopRequireDefault(_KendoTabStripMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'kendo-tabstrip',
    mixins: [_KendoTabStripMixin2.default]
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _KendoSplitterMixin = __webpack_require__(29);

var _KendoSplitterMixin2 = _interopRequireDefault(_KendoSplitterMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'kendo-splitter',
    mixins: [_KendoSplitterMixin2.default]
};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LayoutInstaller = exports.KendoLayoutInstaller = exports.SplitterPane = exports.KendoSplitterPane = exports.Splitter = exports.KendoSplitter = exports.TabStrip = exports.KendoTabStrip = exports.PanelBarItem = exports.KendoPanelBarItem = exports.PanelBar = exports.KendoPanelBar = exports.MenuItem = exports.KendoMenuItem = exports.ContextMenu = exports.KendoContextMenu = exports.Menu = exports.KendoMenu = undefined;

var _KendoMenu = __webpack_require__(2);

var _KendoMenu2 = _interopRequireDefault(_KendoMenu);

var _KendoMenuItem = __webpack_require__(6);

var _KendoMenuItem2 = _interopRequireDefault(_KendoMenuItem);

var _KendoContextMenu = __webpack_require__(7);

var _KendoContextMenu2 = _interopRequireDefault(_KendoContextMenu);

var _KendoPanelBar = __webpack_require__(8);

var _KendoPanelBar2 = _interopRequireDefault(_KendoPanelBar);

var _KendoPanelBarItem = __webpack_require__(9);

var _KendoPanelBarItem2 = _interopRequireDefault(_KendoPanelBarItem);

var _KendoTabStrip = __webpack_require__(10);

var _KendoTabStrip2 = _interopRequireDefault(_KendoTabStrip);

var _KendoSplitter = __webpack_require__(11);

var _KendoSplitter2 = _interopRequireDefault(_KendoSplitter);

var _KendoSplitterPane = __webpack_require__(1);

var _KendoSplitterPane2 = _interopRequireDefault(_KendoSplitterPane);

var _kendoLayoutInstaller = __webpack_require__(32);

var _kendoLayoutInstaller2 = _interopRequireDefault(_kendoLayoutInstaller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.KendoMenu = _KendoMenu2.default;
exports.Menu = _KendoMenu2.default;
exports.KendoContextMenu = _KendoContextMenu2.default;
exports.ContextMenu = _KendoContextMenu2.default;
exports.KendoMenuItem = _KendoMenuItem2.default;
exports.MenuItem = _KendoMenuItem2.default;
exports.KendoPanelBar = _KendoPanelBar2.default;
exports.PanelBar = _KendoPanelBar2.default;
exports.KendoPanelBarItem = _KendoPanelBarItem2.default;
exports.PanelBarItem = _KendoPanelBarItem2.default;
exports.KendoTabStrip = _KendoTabStrip2.default;
exports.TabStrip = _KendoTabStrip2.default;
exports.KendoSplitter = _KendoSplitter2.default;
exports.Splitter = _KendoSplitter2.default;
exports.KendoSplitterPane = _KendoSplitterPane2.default;
exports.SplitterPane = _KendoSplitterPane2.default;
exports.KendoLayoutInstaller = _kendoLayoutInstaller2.default;
exports.LayoutInstaller = _kendoLayoutInstaller2.default;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

var _MenuProps = __webpack_require__(18);

var _MenuProps2 = _interopRequireDefault(_MenuProps);

var _OpenOnClick = __webpack_require__(19);

var _OpenOnClick2 = _interopRequireDefault(_OpenOnClick);

var _Animation = __webpack_require__(4);

var _Animation2 = _interopRequireDefault(_Animation);

var _Scrollable = __webpack_require__(5);

var _Scrollable2 = _interopRequireDefault(_Scrollable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    template: '<ul><slot></slot></ul>',
    props: _MenuProps2.default,
    mixins: [_kendoBaseComponentsVueWrapper.KendoBaseComponent, _kendoBaseComponentsVueWrapper.KendoSharedMethods, _Animation2.default, _OpenOnClick2.default, _Scrollable2.default],
    mounted: function mounted() {
        this._resolveChildren();

        kendo.jQuery(this.$el).kendoMenu(this.widgetOptions);

        this.$emit('kendowidgetready', this.kendoWidget());
    },

    methods: {
        kendoWidget: function kendoWidget() {
            return kendo.jQuery(this.$el).getKendoMenu();
        },
        _resolveChildren: function _resolveChildren() {
            if (this.$options.propsData && !this.$options.propsData['dataSource'] && this.$slots.default) {
                var items = this.resolveInnerTags('kendo-menu-item');

                if (items.length) {
                    this.widgetOptions['dataSource'] = items;
                }
            }
        }
    }
};

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _vue = __webpack_require__(3);

var _vue2 = _interopRequireDefault(_vue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

exports.default = {
    template: '<div></div>',
    props: {
        disabled: Boolean,
        readonly: Boolean
    },
    created: function created() {
        this.resolveWidgetOptions();
        this.$_nativeTemplates = [];
        this.$on('kendowidgetready', this.ready);
    },
    beforeDestroy: function beforeDestroy() {
        if (this.kendoWidget() && this.kendoWidget().destroy) {
            this.kendoWidget().destroy();
            if (this.$_nativeTemplates.length) {
                for (var i = 0; i < this.$_nativeTemplates.length; i++) {
                    this.$_nativeTemplates[i].$destroy();
                }
            }
        }
    },
    mounted: function mounted() {
        if (this.$el.classList.length > 0) {
            this.nativeClasses = [].concat(_toConsumableArray(this.$el.classList));
        } else {
            this.nativeClasses = [];
        }

        for (var key in this.$options.propsData) {
            var that = this;

            if (key.toLowerCase().indexOf('template') !== -1) {
                var isFunction = this.widgetOptions[key] instanceof Function;

                if (isFunction) {
                    this.widgetOptions[key] = this.transformTemplate(key);
                }
            }

            if (key === 'value') {
                that.$watch(key, function (newValue) {
                    that.changeValue(newValue);
                });
            } else if (key === 'disabled') {
                that.$watch(key, function (newValue) {
                    that.makeDisabled(newValue);
                });
            } else if (key === 'readonly') {
                that.$watch(key, function (newValue) {
                    that.makeReadonly(newValue);
                });
            } else {
                that.$watch(key, function (newValue, oldValue) {
                    // Vue always dispatches a change when inline complex objects are bound
                    // https://github.com/telerik/kendo-ui-core/issues/3952
                    if (JSON.stringify(oldValue) !== JSON.stringify(newValue)) {
                        that.updateWidget();
                    }
                });
            }
        }
    },

    watch: {
        $attrs: function $attrs() {
            this.toggleClasses();
        }
    },
    methods: {
        toggleClasses: function toggleClasses() {
            var that = this;
            var $element = kendo.jQuery(that.$el);
            var $wrapper = that.kendoWidget().wrapper;

            if ($wrapper && $wrapper[0] !== $element[0]) {
                that.nativeClasses.forEach(function (item) {
                    $wrapper.removeClass(item);
                });

                that.nativeClasses = [].concat(_toConsumableArray(that.$el.classList));

                that.nativeClasses.forEach(function (item) {
                    $wrapper.addClass(item);
                });
            }

            if (that.kendoClasses) {
                that.kendoClasses.forEach(function (item) {
                    $element.addClass(item);
                });
            }
        },
        updateWidget: function updateWidget() {
            var that = this;

            that.resolveWidgetOptions();

            if (that._resolveChildren) {
                that._resolveChildren();
            }

            if (that.kendoWidget().setOptions) {
                that.kendoWidget().setOptions(that.widgetOptions);
            }
        },

        transformTemplate: function transformTemplate(key, val) {
            var that = this;
            var object;
            var templateDefinition = val || that.$options.propsData[key];
            try {
                object = templateDefinition.call(that, {});
            } catch (e) {
                return templateDefinition;
            }

            if (!object.template) {
                return templateDefinition;
            }

            return function () {
                object = templateDefinition.apply(this, arguments);
                var vueObject = new _vue2.default(object.template);
                vueObject.$data.templateArgs = object.templateArgs;

                var kendoguid = 'kendo' + kendo.guid();

                that.$nextTick(function () {
                    vueObject.$mount('#' + kendoguid);
                    that.$_nativeTemplates.push(vueObject);
                });

                return '<div id="' + kendoguid + '"></div>';
            };
        },
        resolveChildren: function resolveChildren(prop, name) {
            if (!this.widgetOptions[prop] && this.$slots.default) {
                var items = [];
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this.$slots.default[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var childSlot = _step.value;

                        if (childSlot.tag && childSlot.tag.indexOf(name) !== -1) {
                            var itemOptions = this.parseOptions(childSlot.componentInstance);

                            if (itemOptions.dataSourceRef) {
                                this.setInnerDataSource('dataSourceRef', 'dataSource', itemOptions);
                            }

                            items.push(itemOptions);
                            this.handleWatcher(childSlot.componentInstance);
                        }
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                if (items.length) {
                    this.widgetOptions[prop] = items;
                }
            }
        },
        handleWatcher: function handleWatcher(childInstance) {
            var that = this;
            childInstance.stashedPropsData = JSON.stringify(childInstance.$options.propsData);

            if (!childInstance._isWatchAttached) {
                childInstance.$watch('$props', function () {
                    var propsAsJson = JSON.stringify(childInstance.$options.propsData);
                    var needsUpdate = childInstance.stashedPropsData !== propsAsJson;

                    if (that.updateWidget && needsUpdate) {
                        that.updateWidget();
                    } else if (that._resolveInnerChildren) {
                        that._resolveInnerChildren();
                    }

                    childInstance.stashedPropsData = propsAsJson;
                }, { deep: true });

                childInstance._isWatchAttached = true;
            }
        },
        makeDisabled: function makeDisabled(toDisable) {
            if (this.kendoWidget().enable) {
                this.kendoWidget().enable(!toDisable);
            }
        },
        makeReadonly: function makeReadonly(value) {
            if (this.kendoWidget().readonly) {
                this.kendoWidget().readonly(value);
            }
        },
        changeValue: function changeValue(newValue) {
            if (this.kendoWidget().value) {
                this.kendoWidget().value(newValue);
            }
        },
        resolveWidgetOptions: function resolveWidgetOptions() {
            this.widgetOptions = this.parseOptions();
        },
        parseOptions: function parseOptions(component) {
            var that = component || this;
            var options = {};
            for (var key in that.$options.propsData) {
                var propOptions = that.$options.props[key];
                var val = that.$options.propsData[key];

                var compositeProps = propOptions.kComposite;

                if (!compositeProps) {
                    if (key.toLowerCase().indexOf('template') !== -1 && val instanceof Function) {
                        options[key] = this.transformTemplate(key, val);
                    } else {
                        options[key] = val;
                    }
                } else {
                    this.addCompositeProperty(options, compositeProps.split('.'), that.$props[key]);
                }
            }

            Object.keys(that.$props).forEach(function (event) {
                if (that.$listeners && that.$listeners[event.toLowerCase()]) {
                    var kendoIndex = event.lastIndexOf('kendo');
                    var kendoEvent;
                    if (kendoIndex !== -1) {
                        kendoEvent = event.replace('kendo', '').toLowerCase();
                    }

                    options[kendoEvent || event] = that.$listeners[event.toLowerCase()];
                }
            });

            return options;
        },
        addCompositeProperty: function addCompositeProperty(obj, keys, val) {
            var lastKey = keys.pop();
            var lastObj = keys.reduce(function (obj, key) {
                obj[key] = _typeof(obj[key]) === 'object' ? obj[key] || {} : {};
                return obj[key];
            }, obj);

            if (lastKey.toLowerCase().indexOf('template') !== -1 && val instanceof Function) {
                lastObj[lastKey] = this.transformTemplate(lastKey, val);
            } else {
                lastObj[lastKey] = val;
            }
        },
        ready: function ready() {
            var that = this;

            if (this.$el.classList.length > 0) {
                that.kendoClasses = [].concat(_toConsumableArray(that.$el.classList)).filter(function (item) {
                    return that.nativeClasses.indexOf(item) < 0;
                });
            }

            if (that.$options.propsData.disabled) {
                that.makeDisabled(true);
            }

            if (that.$options.propsData.readonly) {
                that.makeReadonly(true);
            }
        }
    }
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    template: '<div></div>',
    props: {
        dataSourceRef: String
    },
    methods: {
        findDataSource: function findDataSource() {
            var dataSource;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.$children[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var a = _step.value;

                    if (this.$children.length && a.kendoDataSource) {
                        dataSource = a.kendoDataSource;
                        break;
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return dataSource;
        },
        setInnerDataSource: function setInnerDataSource() {
            var dataSourceRefProp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'dataSourceRef';
            var rootProp = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'dataSource';
            var subProp = arguments[2];

            var innerDataSource = this.findDataSource();

            if (innerDataSource) {
                if (!subProp) {
                    this.widgetOptions[rootProp] = innerDataSource;
                } else {
                    subProp[rootProp] = innerDataSource;
                }
            } else {
                var refString = subProp ? subProp[dataSourceRefProp] : this[dataSourceRefProp];
                var referredDataSource = this.getParentsRef(refString);

                if (referredDataSource) {
                    this.attachEvents(referredDataSource);
                    if (!subProp) {
                        this.widgetOptions[rootProp] = referredDataSource.kendoDataSource;
                    } else {
                        subProp[rootProp] = referredDataSource.kendoDataSource;
                    }
                }
            }
        },
        getParentsRef: function getParentsRef(anchor) {
            var parent = this.$parent;

            while (parent && !parent.$refs[anchor]) {
                parent = parent.$parent;
            }

            return parent ? parent.$refs[anchor] : undefined;
        },
        setDataSource: function setDataSource(newValue) {
            var newDataSourceRef = this.getParentsRef(newValue);
            var kendoWidget = this.kendoWidget();

            if (kendoWidget.setDataSource && newDataSourceRef) {
                kendoWidget.setDataSource(newDataSourceRef.kendoDataSource);
            } else if (kendoWidget.setDataSource) {
                kendoWidget.setDataSource(newValue);
            }
        },
        attachEvents: function attachEvents(referredDataSource) {
            if (referredDataSource.kendoDataSource) {
                referredDataSource.kendoDataSource.bind('change', function (ev) {
                    referredDataSource.$emit('change', ev);
                });

                referredDataSource.kendoDataSource.bind('error', function (ev) {
                    referredDataSource.$emit('error', ev);
                });

                referredDataSource.kendoDataSource.bind('push', function (ev) {
                    referredDataSource.$emit('push', ev);
                });

                referredDataSource.kendoDataSource.bind('requestEnd', function (ev) {
                    referredDataSource.$emit('requestend', ev);
                });

                referredDataSource.kendoDataSource.bind('requestStart', function (ev) {
                    referredDataSource.$emit('requeststart', ev);
                });

                referredDataSource.kendoDataSource.bind('sync', function (ev) {
                    referredDataSource.$emit('sync', ev);
                });
            }
        }
    },
    mounted: function mounted() {
        var that = this;

        that.$watch('dataSourceRef', function (newValue) {
            that.setDataSource(newValue);
        });
        that.$watch('dataSource', function (newValue) {
            that.setDataSource(newValue);
        });

        that.setInnerDataSource();
    }
};

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    template: '<input ref="valueInput" :value="value" />',
    model: {
        event: 'changemodel'
    }
};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    methods: {
        resolveInnerTags: function resolveInnerTags(tagName) {
            var items = [];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.$slots.default[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var childSlot = _step.value;

                    if (childSlot.tag && childSlot.tag.indexOf(tagName) !== -1) {
                        var childOptions = childSlot.componentOptions;
                        var item = childOptions.propsData;

                        if (!childOptions.propsData.items) {
                            item.items = childSlot.componentInstance.subitems;
                        }

                        items.push(item);
                        this.handleWatcher(childSlot.componentInstance);
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return items;
        },
        handleWatcher: function handleWatcher(childInstance) {
            var that = this;

            if (!childInstance._isWatchAttached) {
                childInstance.$watch('$props', function () {
                    if (that.updateWidget) {
                        that.updateWidget();
                    } else if (that._resolveInnerChildren) {
                        that._resolveInnerChildren();
                    }
                }, { deep: true });

                childInstance._isWatchAttached = true;
            }
        }
    }
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    animation: Boolean | Object,
    closeOnClick: Boolean,
    dataSource: Object | Array,
    direction: String,
    hoverDelay: Number,
    openOnClick: Boolean | Object,
    orientation: String,
    popupCollision: String,
    scrollable: Boolean | Object,

    // Events
    close: Function,
    open: Function,
    activate: Function,
    deactivate: Function,
    select: Function
};

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        openOnClickRootMenuItems: {
            type: Boolean,
            kComposite: 'openOnClick.rootMenuItems'
        },
        openOnClickSubMenuItems: {
            type: Boolean,
            kComposite: 'openOnClick.subMenuItems'
        }
    }
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

var _ContextMenuProps = __webpack_require__(21);

var _ContextMenuProps2 = _interopRequireDefault(_ContextMenuProps);

var _Animation = __webpack_require__(22);

var _Animation2 = _interopRequireDefault(_Animation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    template: '<ul><slot></slot></ul>',
    props: _ContextMenuProps2.default,
    mixins: [_kendoBaseComponentsVueWrapper.KendoBaseComponent, _kendoBaseComponentsVueWrapper.KendoSharedMethods, _Animation2.default],
    mounted: function mounted() {
        this._resolveChildren();

        kendo.jQuery(this.$el).kendoContextMenu(this.widgetOptions);

        this.$emit('kendowidgetready', this.kendoWidget());
    },

    methods: {
        kendoWidget: function kendoWidget() {
            return kendo.jQuery(this.$el).getKendoContextMenu();
        },
        _resolveChildren: function _resolveChildren() {
            if (this.$options.propsData && !this.$options.propsData['dataSource'] && this.$slots.default) {
                var items = this.resolveInnerTags('kendo-menu-item');

                if (items.length) {
                    this.widgetOptions['dataSource'] = items;
                }
            }
        }
    }
};

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    alignToAnchor: Boolean,
    animation: Boolean | Object,
    appendTo: String,
    closeOnClick: Boolean,
    copyAnchorStyles: Boolean,
    dataSource: Object | Array,
    direction: String,
    filter: String,
    hoverDelay: Number,
    orientation: String,
    popupCollision: String,
    showOn: String,
    target: String,

    // Events
    close: Function,
    open: Function,
    activate: Function,
    deactivate: Function,
    select: Function
};

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        animationCloseEffects: {
            type: String,
            kComposite: 'animation.close.effects'
        },
        animationCloseDuration: {
            type: Number,
            kComposite: 'animation.close.duration'
        },
        animationOpenEffects: {
            type: String,
            kComposite: 'animation.open.effects'
        },
        animationOpenDuration: {
            type: Number,
            kComposite: 'animation.open.duration'
        }
    }
};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

var _PanelBarProps = __webpack_require__(24);

var _PanelBarProps2 = _interopRequireDefault(_PanelBarProps);

var _Animation = __webpack_require__(25);

var _Animation2 = _interopRequireDefault(_Animation);

var _Messages = __webpack_require__(26);

var _Messages2 = _interopRequireDefault(_Messages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    template: '<ul><slot></slot></ul>',
    props: _PanelBarProps2.default,
    mixins: [_kendoBaseComponentsVueWrapper.KendoBaseComponent, _kendoBaseComponentsVueWrapper.KendoBaseDatasourceComponent, _kendoBaseComponentsVueWrapper.KendoSharedMethods, _Animation2.default, _Messages2.default],
    mounted: function mounted() {
        this._resolveChildren();

        kendo.jQuery(this.$el).kendoPanelBar(this.widgetOptions);

        this.$emit('kendowidgetready', this.kendoWidget());
    },

    methods: {
        kendoWidget: function kendoWidget() {
            return kendo.jQuery(this.$el).getKendoPanelBar();
        },
        _resolveChildren: function _resolveChildren() {
            if (this.$options.propsData && !this.$options.propsData['dataSource'] && this.$slots.default) {
                var items = this.resolveInnerTags('kendo-panelbar-item');

                if (items.length) {
                    this.widgetOptions['dataSource'] = items;
                }
            }
        }
    }
};

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    animation: Boolean | Object,
    autoBind: Boolean,
    contentUrls: Array,
    dataImageUrlField: String,
    dataSource: Object | Array,
    dataSpriteCssClassField: String,
    dataTextField: String | Array,
    dataUrlField: String,
    expandMode: String,
    loadOnDemand: Boolean,
    messages: Object,
    template: String | Function,

    // Events
    activate: Function,
    collapse: Function,
    contentLoad: Function,
    dataBound: Function,
    error: Function,
    expand: Function,
    select: Function
};

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        animationCollapseDuration: {
            type: Number,
            kComposite: 'animation.collapse.duration'
        },
        animationCollapseEffects: {
            type: String,
            kComposite: 'animation.collapse.effects'
        },
        animationExpandDuration: {
            type: Number,
            kComposite: 'animation.expand.duration'
        },
        animationExpandEffects: {
            type: String,
            kComposite: 'animation.expand.effects'
        }
    }
};

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        messagesLoading: {
            type: String,
            kComposite: 'messages.loading'
        },
        messagesRequestFailed: {
            type: String,
            kComposite: 'messages.requestFailed'
        },
        messagesRetry: {
            type: String,
            kComposite: 'messages.retry'
        }
    }
};

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

var _TabStripProps = __webpack_require__(28);

var _TabStripProps2 = _interopRequireDefault(_TabStripProps);

var _Animation = __webpack_require__(4);

var _Animation2 = _interopRequireDefault(_Animation);

var _Scrollable = __webpack_require__(5);

var _Scrollable2 = _interopRequireDefault(_Scrollable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    template: '<div><slot></slot></div>',
    props: _TabStripProps2.default,
    mixins: [_kendoBaseComponentsVueWrapper.KendoBaseComponent, _kendoBaseComponentsVueWrapper.KendoBaseDatasourceComponent, _Animation2.default, _Scrollable2.default],
    mounted: function mounted() {
        kendo.jQuery(this.$el).kendoTabStrip(this.widgetOptions);

        this.$emit('kendowidgetready', this.kendoWidget());
    },

    methods: {
        kendoWidget: function kendoWidget() {
            return kendo.jQuery(this.$el).getKendoTabStrip();
        }
    }
};

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    animation: Boolean | Object,
    collapsible: Boolean,
    contentUrls: Array,
    dataContentField: String,
    dataContentUrlField: String,
    dataImageUrlField: String,
    dataSource: Object | Array,
    dataSpriteCssClass: String,
    dataTextField: String,
    dataUrlField: String,
    navigatable: Boolean,
    scrollable: Boolean | Object,
    tabPosition: String,
    value: String,

    // Events
    activate: Function,
    contentLoad: Function,
    error: Function,
    select: Function,
    show: Function
};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _kendoBaseComponentsVueWrapper = __webpack_require__(0);

var _SplitterProps = __webpack_require__(30);

var _SplitterProps2 = _interopRequireDefault(_SplitterProps);

var _KendoSplitterPane = __webpack_require__(1);

var _KendoSplitterPane2 = _interopRequireDefault(_KendoSplitterPane);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    template: '<div><slot></slot></div>',
    props: _SplitterProps2.default,
    mixins: [_kendoBaseComponentsVueWrapper.KendoBaseComponent],
    mounted: function mounted() {
        this._resolveChildren();

        kendo.jQuery(this.$el).kendoSplitter(this.widgetOptions);

        this.$emit('kendowidgetready', this.kendoWidget());
    },

    methods: {
        kendoWidget: function kendoWidget() {
            return kendo.jQuery(this.$el).getKendoSplitter();
        },
        _resolveChildren: function _resolveChildren() {
            this.resolveChildren('panes', _KendoSplitterPane2.default.name);
        }
    }
};

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    orientation: String,
    panes: Array,

    // Events
    collapse: Function,
    contentLoad: Function,
    error: Function,
    expand: Function,
    layoutChange: Function,
    resize: Function
};

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: {
        collapsed: Boolean,
        collapsedSize: String,
        collapsible: Boolean,
        contentUrl: String,
        max: String,
        min: String,
        resizable: Boolean,
        scrollable: Boolean,
        size: String
    }
};

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _KendoMenu = __webpack_require__(2);

var _KendoMenu2 = _interopRequireDefault(_KendoMenu);

var _KendoMenuItem = __webpack_require__(6);

var _KendoMenuItem2 = _interopRequireDefault(_KendoMenuItem);

var _KendoContextMenu = __webpack_require__(7);

var _KendoContextMenu2 = _interopRequireDefault(_KendoContextMenu);

var _KendoPanelBar = __webpack_require__(8);

var _KendoPanelBar2 = _interopRequireDefault(_KendoPanelBar);

var _KendoPanelBarItem = __webpack_require__(9);

var _KendoPanelBarItem2 = _interopRequireDefault(_KendoPanelBarItem);

var _KendoTabStrip = __webpack_require__(10);

var _KendoTabStrip2 = _interopRequireDefault(_KendoTabStrip);

var _KendoSplitter = __webpack_require__(11);

var _KendoSplitter2 = _interopRequireDefault(_KendoSplitter);

var _KendoSplitterPane = __webpack_require__(1);

var _KendoSplitterPane2 = _interopRequireDefault(_KendoSplitterPane);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var KendoLayoutInstaller = function () {
    function KendoLayoutInstaller() {
        _classCallCheck(this, KendoLayoutInstaller);
    }

    _createClass(KendoLayoutInstaller, null, [{
        key: 'install',
        value: function install(Vue) {
            Vue.component(_KendoMenu2.default.name, _KendoMenu2.default);
            Vue.component(_KendoContextMenu2.default.name, _KendoContextMenu2.default);
            Vue.component(_KendoMenuItem2.default.name, _KendoMenuItem2.default);
            Vue.component(_KendoPanelBar2.default.name, _KendoPanelBar2.default);
            Vue.component(_KendoPanelBarItem2.default.name, _KendoPanelBarItem2.default);
            Vue.component(_KendoTabStrip2.default.name, _KendoTabStrip2.default);
            Vue.component(_KendoSplitter2.default.name, _KendoSplitter2.default);
            Vue.component(_KendoSplitterPane2.default.name, _KendoSplitterPane2.default);
        }
    }]);

    return KendoLayoutInstaller;
}();

exports.default = KendoLayoutInstaller;

// Automatic installation if Vue has been added to the global scope.

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(KendoLayoutInstaller);
}

/***/ })
/******/ ]);
});
//# sourceMappingURL=kendo-layout-vue-wrapper.js.map