﻿// gatepass_request model
var common_technicalSupportModel = kendo.data.Model.define({
    id: "id",
    fields: {
         tsNo: { type: "string" },
         tsDescription: { type: "string" },
         requestDate : { type: "date" },
         requestById : { type: "string" },
         requestByName : { type: "string" },
         requestByEmail: { type: "string" },
         requestByPosition: { type: "string" },
         areaInvolvedId : [],
         areaInvolvedAreaCode : { type: "string" }
         
    }
});

var common_projectDocumentModel = kendo.data.Model.define({
    id: "id",
    fields: {
        projectId: { type: "number" },
        docType: { type: "string" },
        docDesc: { type: "string" },
        docNo: { type: "string" },
        docTitle: { type: "string" },
        drawingNo: { type: "string" },
        planStartDate: { type: "date" },
        planFinishDate: { type: "date" },
        actualStartDate: { type: "date" },
        actualFinishDate: { type: "date" },
        discipline: { type: "string" },
        status: { type: "string" },
        remark: { type: "string" },
        executor: { type: "string" },
        executorAssignment: { type: "string" },
        progress: { type: "number" },
        documentById: { type: "string" },
        documentByName: { type: "string" },
        documentByEmail: { type: "string" },
        documentTrafic: { type: "number" }
    }
});

var common_projectManagementModel = kendo.data.Model.define({
    id: "id",
    fields: {
        projectId: { type: "number" },
        manageKeyIssue: { type: "string" },
        scope: { type: "string" },
        benefit: { type: "string" },
        accomplishment: { type: "string" },
        details: { type: "string" },
        contractor: { type: "string" }
    }
});

var common_keyDeliverableModel = kendo.data.Model.define({
    id: "id",
    fields: {
        projectManagementId: { type: "number" },
        projectId: { type: "number" },
        name: { type: "string" },
        weightPercentage: { type: "number" },
        planStartDate: { type: "date" },
        planFinishDate: { type: "date" },
        revision: { type: "number" },
        actualStartDate: { type: "date" },
        actualFinishDate: { type: "date" },
        status: { type: "string" },
        remark: { type: "string" }
    }
});
