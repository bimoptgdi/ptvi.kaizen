﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PTVI.KAIZEN.WebApp
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public BasePage ContentBasePage
        {
            get
            {
                return (BasePage)MainContent.Page;
            }
        }

        public Dictionary<object, object> MenuControl
        {
            get
            {
                Dictionary<object, object> menuItems = new Dictionary<object, object>();

                return menuItems;
            }
        }

        protected void ButtonChangeUser_Click(object sender, EventArgs e)
        {
            //if (__activeUserId.Value != "")
            //{
            //    var output = ContentBasePage.GetJsonObjectFromWebApi(ContentBasePage.WebApiUrlEllipse + "employee/" + __activeUserId.Value);

            //    if (output != null)
            //    {
            //        ContentBasePage.SessionUniqueID = ContentBasePage.SessionUniqueID;
            //        ContentBasePage._currNTUserID = output["USERNAME"];
            //        ContentBasePage._currOriginNTUserID = ContentBasePage._currOriginNTUserID;
            //        ContentBasePage.changeUser(ContentBasePage._currOriginNTUserID, ContentBasePage._currNTUserID);
            //        Response.Redirect("HomeForm.aspx", true);
            //    }
            //    else {
            //        Response.End();
            //    }
            //}
        }
    }
}