﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
//using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;

namespace PTVI.KAIZEN.WebApp
{
    public class BasePage : System.Web.UI.Page
    {
        public BasePage()
        {
        }

        private void initializeEventHandler()
        {
            if (this.Master != null)
            {

            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (this.Master != null)
            {
                if (Context != null && Context.Session != null)
                {
                    if (_currNTUserID == string.Empty || _currOriginNTUserID == string.Empty || !string.IsNullOrEmpty(Request.QueryString["uid"]))
                    {
                        InitializeLogon();
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["projectId"]))
                    {
                        // set project
                        SetProjectData(Request.QueryString["projectId"]);
                    }

                    var output = GetJsonDataFromWebApi(WebApiUrl + "menu/IsUserGetAccessRight/" + _currNTUserID + "|" + CurrentPageName);
                    if (output.Count() > 0)
                    {
                        Dictionary<string, dynamic> userAccess = output[0];
                        if (userAccess["auth"] == "0")
                        {
                            //string theMessage = userAccess["message"];
                            Response.Redirect("unauthorizeduserform.html");

                        }
                    }
                    else
                    {
                        Response.Redirect("HomeForm.aspx");
                    }

                    initializeEventHandler();
                }
            }

            base.OnInit(e);
        }

        private void SetProjectData(string p)
        {
            // get project data
            var url = WebApiUrl + "project/ProjectDataByBNandPID?bn=" + _currBadgeNo + "&pID=" + p;
            var projectData = GetJsonDataFromWebApi(url);

            // set to session
            if (projectData.Count > 0)
            {
                var prj = projectData[0];
                _projectId = prj["id"].ToString();
                _projectNo = prj["projectNo"];
                _projectDesc = prj["projectDescription"];
                _projectType = prj["projectType"];
                _roleInProject = prj["roleInProject"];
                _iptProjectId = prj["iptProjectId"];
                _planStartDate = prj["planStartDate"] == null ? null : DateTime.Parse(prj["planStartDate"]).ToString("yyyyMMdd");
                _planFinishDate = prj["planFinishDate"] == null ? null : DateTime.Parse(prj["planFinishDate"]).ToString("yyyyMMdd");
            }
            else
            {
                Response.Redirect("HomeForm.aspx");
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {

            base.OnLoadComplete(e);
        }

        public string _currAPIKey
        {
            get { return (Session["PTVI.KAIZEN.APIKEY" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.APIKEY" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.APIKEY" + SessionUniqueID] = value; }
        }

        public string SessionUniqueID
        {
            get { return (Session["PTVI.KAIZEN.SessionUniqueID"] != null) ? Session["PTVI.KAIZEN.SessionUniqueID"].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.SessionUniqueID"] = value; }
        }

        public string _menuUser
        {
            get { return (Session["PTVI.KAIZEN.MENU" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.MENU" + SessionUniqueID] as string : string.Empty; }
            set { Session["PTVI.KAIZEN.MENU" + SessionUniqueID] = value; }
        }

        public string _designDrawingDefaultSpan
        {
            get { return (Session["PTVI.KAIZEN._designDrawingDefaultSpan" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN._designDrawingDefaultSpan" + SessionUniqueID].ToString() : null; }
            set { Session["PTVI.KAIZEN._designDrawingDefaultSpan" + SessionUniqueID] = value; }
        }
        public string _designDrawingExDefaultSpan
        {
            get { return (Session["PTVI.KAIZEN.designDrawingExDefaultSpan" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.designDrawingExDefaultSpan" + SessionUniqueID].ToString() : null; }
            set { Session["PTVI.KAIZEN.designDrawingExDefaultSpan" + SessionUniqueID] = value; }
        }

        public Dictionary<string, dynamic> _currUser
        {
            get { return (Session["PTVI.KAIZEN.USER" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.USER" + SessionUniqueID] as Dictionary<string, dynamic> : null; }
            set { Session["PTVI.KAIZEN.USER" + SessionUniqueID] = value; }
        }

        public string _currOriginNTUserID
        {
            get { return (Session["PTVI.ETS.ORIGINNTUSERID" + SessionUniqueID] != null) ? Session["PTVI.ETS.ORIGINNTUSERID" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.ETS.ORIGINNTUSERID" + SessionUniqueID] = value; }
        }


        public string _currNTUserID
        {
            get { return (Session["PTVI.KAIZEN.NTUSERID" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.NTUSERID" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.NTUSERID" + SessionUniqueID] = value; }
        }

        public string _currUserName
        {
            get { return (Session["PTVI.KAIZEN.USERNAME" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.USERNAME" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.USERNAME" + SessionUniqueID] = value; }
        }

        public string _currBadgeNo
        {
            get { return (Session["PTVI.KAIZEN.BADGENO" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.BADGENO" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.BADGENO" + SessionUniqueID] = value; }
        }
        public string _currGroupId
        {
            get { return (Session["PTVI.KAIZEN.GROUPID" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.GROUPID" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.GROUPID" + SessionUniqueID] = value; }
        }


        public string _currRoleType
        {
            get
            {
                List<string> results = new List<string>();

                if (_currRoles == null)
                {
                    return "-";
                }
                else
                {
                    foreach (Dictionary<string, dynamic> role in _currRoles)
                    {
                        //if (role["ROLENAME"] != "ADM")
                        results.Add(role["ROLETYPE"]);
                    }
                }

                if (results.Count() > 0)
                {
                    return results.Min();
                }
                else
                {
                    return "-";
                }


            }
        }

        public string _currUserID
        {
            get { return (Session["PTVI.KAIZEN.CURRID" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.CURRID" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.CURRID" + SessionUniqueID] = value; }
        }

        public string _currEmail
        {
            get { return (Session["PTVI.KAIZEN.EMAIL" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.EMAIL" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.EMAIL" + SessionUniqueID] = value; }
        }

        public Dictionary<string, dynamic> _CurrUserBasicInfo
        {
            get { return (Session["PTVI.KAIZEN.USERBASICINFO"] != null) ? Session["PTVI.KAIZEN.USERBASICINFO"] as Dictionary<string, dynamic> : null; }
            set { Session["PTVI.KAIZEN.USERBASICINFO"] = value; }
        }

        public string _currPositionID
        {
            get { return (Session["PTVI.KAIZEN.POSITIONID"] != null) ? Session["PTVI.KAIZEN.POSITIONID"].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.POSITIONID"] = value; }
        }

        public string _currUserLevel
        {
            get { return (Session["PTVI.KAIZEN.USERLEVEL" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.USERLEVEL" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.USERLEVEL" + SessionUniqueID] = value; }
        }

        public string _currSuperiorLevel
        {
            get { return (Session["PTVI.KAIZEN.SUPERIORLEVEL" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.SUPERIORLEVEL" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.SUPERIORLEVEL" + SessionUniqueID] = value; }
        }

        public string _projectId
        {
            get { return (Session["PTVI.KAIZEN.PROJECTID" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.PROJECTID" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.PROJECTID" + SessionUniqueID] = value; }
        }

        public string _projectNo
        {
            get { return (Session["PTVI.KAIZEN.PROJECTNO" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.PROJECTNO" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.PROJECTNO" + SessionUniqueID] = value; }
        }

        public string _projectDesc
        {
            get { return (Session["PTVI.KAIZEN.PROJECTDESC" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.PROJECTDESC" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.PROJECTDESC" + SessionUniqueID] = value; }
        }

        public string _projectType
        {
            get { return (Session["PTVI.KAIZEN.PROJECTTYPE" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.PROJECTTYPE" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.PROJECTTYPE" + SessionUniqueID] = value; }
        }

        public string _roleInProject
        {
            get { return (Session["PTVI.KAIZEN.ROLEINPROJECT" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.ROLEINPROJECT" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.ROLEINPROJECT" + SessionUniqueID] = value; }
        }

        public string _iptProjectId
        {
            get { return (Session["PTVI.KAIZEN.IPTPROJECTID" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.IPTPROJECTID" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.IPTPROJECTID" + SessionUniqueID] = value; }

        }

        public string _planStartDate
        {
            get { return (Session["PTVI.KAIZEN.PLANSTARTDATE" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.PLANSTARTDATE" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.PLANSTARTDATE" + SessionUniqueID] = value; }

        }

        public string _planFinishDate
        {
            get { return (Session["PTVI.KAIZEN.PLANFINISHDATE" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.PLANFINISHDATE" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.PLANFINISHDATE" + SessionUniqueID] = value; }

        }

        public string _positionProjectName
        {
            get { return (Session["PTVI.KAIZEN.POSITIONPROJECTNAME" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.POSITIONPROJECTNAME" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.POSITIONPROJECTNAME" + SessionUniqueID] = value; }

        }

        public List<Dictionary<string, dynamic>> _currRoles
        {
            get
            {
                List<Dictionary<string, dynamic>> roles = new List<Dictionary<string, dynamic>>();
                if (_currUser != null)
                {

                    foreach (Dictionary<string, dynamic> userRole in _currUser["USERROLEs"])
                    {
                        roles.Add(userRole["ROLE"]);
                    }


                }
                return roles;
            }
        }

        public string _currRole
        {
            get
            {
                List<string> results = new List<string>();

                if (_currRoles == null)
                {
                    return "-";
                }
                else
                {
                    foreach (Dictionary<string, dynamic> role in _currRoles)
                    {
                        //if (role["ROLENAME"] != "ADM")
                        results.Add(role["ROLENAME"]);
                    }
                }

                if (results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return "-";
                }
            }
        }

        public string _currRoleName
        {
            get
            {
                List<string> results = new List<string>();

                if (_currRoles == null)
                {
                    return "General User";
                }
                else
                {
                    foreach (Dictionary<string, dynamic> role in _currRoles)
                    {
                        results.Add(role["DESCRIPTION"]);
                    }
                }

                if (results.Count() > 0)
                {
                    return string.Join(", ", results.ToArray());
                }
                else
                {
                    return "General User";
                }


            }
        }

        public string _currDepartmentCode
        {
            get { return (Session["PTVI.KAIZEN.DEPARTMENTCODE" + SessionUniqueID] != null) ? Session["PTVI.KAIZEN.DEPARTMENTCODE" + SessionUniqueID].ToString() : string.Empty; }
            set { Session["PTVI.KAIZEN.DEPARTMENTCODE" + SessionUniqueID] = value; }
        }

        public string WebAppUrl
        {
            get
            {
                return global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.WebAppUrl;

            }
        }
        public string WebApiUrl
        {
            get
            {
                return global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.WebApiUrl;

            }
        }

        public string WebApiUrlEllipse
        {
            get
            {
                return global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.WebApiUrlEllipse;
            }
        }

        public string WebOdataUrl
        {
            get
            {
                return global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.WebOdataUrl;

            }
        }

        public string DocServer
        {
            get
            {
                return global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.DocServer;
            }
        }

        public string ExtKDMonth
        {
            get
            {
                return global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.ExtKDMonth;

            }
        }

        public string CurrentPageName
        {
            get
            {
                return Path.GetFileName(Request.Path);
            }
        }

        private bool FindUserMenu(dynamic menu, string v)
        {
            var fileName = menu["FILENAME"] == null ? "" : (string)menu["FILENAME"];
            var found = fileName.ToLower() == v;

            if (!found && menu["CHILDMENUS"].Count > 0)
            {
                var childs = menu["CHILDMENUS"];
                foreach (var ch in childs)
                {
                    found = FindUserMenu(ch, CurrentPageName.ToLower());

                    if (found)
                        return true;
                }
            }

            return found;
        }
    

    public bool IsAllowToViewCurrentPage
        {
            get
            {


                var menuObj = JsonConvert.DeserializeObject<List<Dictionary<string, dynamic>>>(_menuUser);

                if (menuObj == null)
                    return false;

                var found = false;
                foreach (var menu in menuObj)
                {
                    found = FindUserMenu(menu, CurrentPageName.ToLower());

                    if (found)
                        return true;
                }

                return found;
            }
        }

        private void InitializeLogon()
        {
            try
            {

                Response.Cache.SetNoStore();

                _currNTUserID = getCurrentNtUserID();

                generateDataUserLogin();


            }
            catch (Exception exc)
            {
                string errorMessage = exc.ToString();
            }
        }

        private void generateDataUserLogin()
        {
            try
            {
                // generate menu
                generateMenu(_currNTUserID);
                getCurrentUser();
                getDesignDrawingDefaultSpan();
                // get ADR user profile


                // identification user
                // jika badgeno nya tidak ada
                // maka ambil dari ntuserid nya
                _currUserID = _currBadgeNo;
                if (string.IsNullOrEmpty(_currBadgeNo))
                {
                    _currUserID = _currNTUserID;
                }

                // get user basic info
                _CurrUserBasicInfo = getBasicInfoByBadgeNo(_currBadgeNo);

                _currPositionID = (_CurrUserBasicInfo["EMPLOYEE_POS"]).Trim();
                _currUserLevel = (_CurrUserBasicInfo["EMPLOYEE_LEVEL"]).Trim();

                _currSuperiorLevel = _CurrUserBasicInfo["SUPERIOR_LEVEL"].Trim();

                var other_info = getOtherInfo(_currBadgeNo);
                _currDepartmentCode = ((string)other_info[0]["DEPT_CODE"]).Trim();


            }
            catch (Exception exc)
            {
                string errorMessage = exc.ToString();
            }
        }

        private string getDelegated(string uidParam, string ntUserIdParam)
        {
            string getUidBadge = getBadgeNo(uidParam);
            string getntUser = getBadgeNo(ntUserIdParam);

            var getDataDoa = WebApiUrl + "DoaLogins/isDelegated/" + getUidBadge + "?delegateParam=" + getntUser;

            var isDataDoa = GetJsonStringFromWebApi(getDataDoa);
            if (Convert.ToBoolean(isDataDoa))
            {
                ntUserIdParam = Request.QueryString["uid"];
            }
            return ntUserIdParam;
        }

        private void setActiveUser(string currdate, string delegatedParam, string ntUserIdParam)
        {
            var getsetActiveCheckUserStr = WebApiUrl + "DoaLogins/setActiveCheckUser/" + currdate + "?delegatedParam=" + delegatedParam + "&originParam=" + ntUserIdParam;
            var setActiveCheckUser = GetJsonStringFromWebApi(getsetActiveCheckUserStr);
        }

        public void changeUser(string ntUserIdParam, string newNtUserUdParam) {
            string currdate = DateTime.Now.ToString("yyyy-MM-dd");
            _currNTUserID = newNtUserUdParam;
            _currOriginNTUserID = ntUserIdParam;
            setActiveUser(currdate, ntUserIdParam, newNtUserUdParam);
            generateDataUserLogin();
        }

        private string getCurrentNtUserID()
        {
            string errMsg = String.Empty;
            string ntUserId = String.Empty;

            System.Security.Principal.IPrincipal principal = HttpContext.Current.User;
            string userName = principal.Identity.Name;
            if (string.IsNullOrEmpty(userName))
                userName = Request.ServerVariables["LOGON_USER"];
            string[] identityName = userName.Split('\\');
            if (identityName.Length > 1)
            {
                ntUserId = identityName[1];
            }
            else
            {
                ntUserId = userName;
            }
            string currdate = DateTime.Now.ToString("yyyy-MM-dd");
#if DEBUG
            ntUserId = "herus"; // admin
#endif
            var tempOriginNtUser = ntUserId; //untuk handle change SessionUniqueID

            if (!string.IsNullOrEmpty(Request.QueryString["uid"]))
            {
                var userWithUid = GetJsonObjectFromWebApi(WebApiUrlEllipse + "employee/" + Request.QueryString["uid"])["USERNAME"];
                var  test = GetJsonObjectFromWebApi(WebApiUrlEllipse + "employee/" + Request.QueryString["uid"])["USERNAME"];
                //Set NtUser Jika Mempunyai DOA
                try
                {
                    if (!string.IsNullOrEmpty(userWithUid) && !string.IsNullOrEmpty(ntUserId))
                    {
                        //ntUserId = getDelegated(userWithUid, ntUserId);
                        ntUserId = userWithUid;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());

                }

#if DEBUG
                ntUserId = Request.QueryString["uid"];
#else
                // get debug user setting
                var debugUserUrl = WebApiUrl +  "user/IsDebugUser/" + ntUserId;
                var isDebugUserStr = GetJsonStringFromWebApi(debugUserUrl);

                if (Convert.ToBoolean(isDebugUserStr))
                {
                    ntUserId = Request.QueryString["uid"];
                }

#endif
            }
            SessionUniqueID = ntUserId;

            //set current activeUser untuk handle DOA
            _currOriginNTUserID = tempOriginNtUser;
            if (!string.IsNullOrEmpty(ntUserId))
            {
                //setActiveUser(currdate, _currOriginNTUserID, ntUserId);
            }

            return ntUserId;
        }


        public string CurrentApplicationUrl
        {
            get
            {
                string uri = HttpContext.Current.Request.Url.AbsoluteUri;

                return uri.Substring(0, uri.LastIndexOf("/"));
            }
        }

        public List<Dictionary<string, dynamic>> GetJsonDataFromWebApi(string url)
        {
            try
            {
                List<Dictionary<string, dynamic>> output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }
                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public List<Dictionary<string, dynamic>> PostDataFromWebApi(string url, string postParam)
        {
            try
            {
                List<Dictionary<string, dynamic>> output;

                Uri uri = new Uri(url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;
                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                Stream reqStream = webRequest.GetRequestStream();
                string postData = postParam;// "username=YourUser&password=YourPassword";
                byte[] postArray = Encoding.ASCII.GetBytes(postData);
                reqStream.Write(postArray, 0, postArray.Length);
                reqStream.Close();

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();
                }
                throw new WebException(errorText);
            }
        }

        public void getDesignDrawingDefaultSpan()
        {
            var url = WebApiUrl + "projectDocuments/DesignDrawingExDefaultSpan/1";
                var output = GetJsonDataFromWebApi(url);
                if (output.Count > 0)
                {
                var dd = output[0];
                    _designDrawingDefaultSpan = dd["ddds"];
                    _designDrawingExDefaultSpan = dd["ddeds"];

                }
                else
                {

                }
        }

        public string getBadgeNo(string param)
        {

            var output = GetJsonDataFromWebApi(WebApiUrl + "User/FindLoginInfoByNtUserID/" + param);

            if (output.Count() > 0)
            {
                return output[0]["BADGENO"];
            }
            else
            {
                output = GetJsonDataFromWebApi(WebApiUrl + "account/EmployeeData/" + param);
                if (output.Count() > 0)
                {
                    return output[0]["employeeId"];

                    if (param.StartsWith("X"))
                    {
                        var userNonEmployeeByUserName = GetJsonDataFromWebApi(WebApiUrl + "user/userNonEmployeeByUserName/" + param);
                        if (userNonEmployeeByUserName.Count() > 0)
                        {
                            return userNonEmployeeByUserName[0]["badgeNo"];
                        }

                    }
                }

            }
            return param;
        }


        public void getCurrentUser()
        {
            try
            {

                var output = GetJsonDataFromWebApi(WebApiUrl + "User/FindLoginInfoByNtUserID/" + _currNTUserID);

                if (output.Count > 0)
                {
                    _currUser = output[0];
                    _currUserName = _currUser["USERNAME"];
                    _currBadgeNo = _currUser["BADGENO"];
                    _currEmail = _currUser["EMAIL"];
                    _currGroupId = _currUser["GROUPID"];
                    //_currExt = _currUser["OFFICE_PHONE"];

                }
                else
                {

                    _currUser = null;
                    _currUserName = null;
                    _currBadgeNo = null;
                    _currEmail = null;

                    output = GetJsonDataFromWebApi(WebApiUrl + "account/EmployeeData/" + _currNTUserID);

                    if (output.Count > 0)
                    {
                        _currUserName = output[0]["full_Name"];
                        _currBadgeNo = output[0]["employeeId"];
                        _currEmail = output[0]["email"];

                        if (_currBadgeNo.StartsWith("X"))
                        {
                            var userNonEmployeeByUserName = GetJsonDataFromWebApi(WebApiUrl + "user/userNonEmployeeByUserName/" + _currNTUserID);
                            if (userNonEmployeeByUserName.Count > 0)
                            {
                                _currBadgeNo = userNonEmployeeByUserName[0]["badgeNo"];
                            }

                        }
                    }

                }

                if (String.IsNullOrEmpty(_currBadgeNo) || _currBadgeNo == "-")
                {
                    Response.Redirect("unauthorizeduserform.html");
                }

            }
            catch (Exception)
            {
                //Response.Redirect("UnAuthorizedForm.aspx?msg=" + "Only PT. Vale Indonesia employee can access ADR application");

            }


        }

        public string GetJsonStringFromWebApi(string url)
        {
            try
            {
                string output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    output = json;
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }

                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public Dictionary<string, dynamic> GetJsonObjectFromWebApi(string url)
        {
            try
            {
                Dictionary<string, dynamic> output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    output = serializer.Deserialize<Dictionary<string, dynamic>>(json);
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }

                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public void generateMenu(string ntUserID)
        {
            _menuUser = GetJsonStringFromWebApi(WebApiUrl + "menu/GetMenuByNTUserIDNew/" + ntUserID);
        }

        public bool IsAllowedTo(AccessRights accessrights)
        {


            var output = GetJsonDataFromWebApi(WebApiUrl + "project/IsAllowedProjectToOnList/" + (!string.IsNullOrEmpty(_projectId) ? _projectId : "0") + "?badgeNo=" + _currBadgeNo + "&accessrights=" + (int)accessrights);

            if (output.Count > 0)
            {
                return Convert.ToBoolean(output[0]["result"]);
            }

            return false;
        }



        //untuk membaca apakah dalam menu tersebut user boleh full access atau hanya readonly
        // dalam penggunaanya pada table ROLEACCESSRIGHT di tambahkan column ISREADONLY typenya boolean
        public bool IsReadOnlyTo(AccessRights accessrights)
        {
#if DEBUG
            return false;
#endif

            var output = GetJsonDataFromWebApi(WebApiUrl + "project/IsReadonlyProjectToOnList/" + (!string.IsNullOrEmpty(_projectId) ? _projectId : "0") + "?badgeNo=" + _currBadgeNo + "&accessrights=" + (int)accessrights);
            if (output.Count() > 0)
            {
                return Convert.ToBoolean(output[0]["result"]);
            }
            return false;
        }

        public bool IsUserOnProject()
        {
#if DEBUG
            return true;
#endif

            var output = GetJsonDataFromWebApi(WebApiUrl + "project/isUserOnProject/" + _currBadgeNo);
            if (output.Count() > 0)
            {
                return Convert.ToBoolean(output[0]["result"]);
            }
            else
            {
                return false;
            }
        }

        public Dictionary<string, dynamic> getBasicInfoByBadgeNo(string badgeNo)
        {
            var output = GetJsonDataFromWebApi(WebApiUrlEllipse + "externaldata/executequerybyparam/QRY_ALL_L1_L2_L3|@badgeno=" + badgeNo);
            if (output.Count > 0)
            {
                Dictionary<string, dynamic> basicInfo = output[0];
                Dictionary<string, dynamic> newBasicInfo = new Dictionary<string, dynamic>();
                newBasicInfo.Add("EMPLOYEE_POS", basicInfo["EMPLOYEE_POS"]);
                newBasicInfo.Add("EMPLOYEE_LEVEL", basicInfo["EMPLOYEE_LEVEL"]);
                String superiorName = string.Empty;
                String superiorLevel = "X";
                if (!String.IsNullOrEmpty(basicInfo["EMPLOYEE_L1_ID"]))
                {
                    superiorName = basicInfo["EMPLOYEE_L1_NAME"];
                    superiorLevel = "L1";
                }
                else if (!String.IsNullOrEmpty(basicInfo["EMPLOYEE_L2_ID"]))
                {
                    superiorName = basicInfo["EMPLOYEE_L2_NAME"];
                    superiorLevel = "L2";
                }
                else if (!String.IsNullOrEmpty(basicInfo["EMPLOYEE_L3_ID"]))
                {
                    superiorName = basicInfo["EMPLOYEE_L3_NAME"];
                    superiorLevel = "L3";
                }
                else if (!String.IsNullOrEmpty(basicInfo["EMPLOYEE_L4_ID"]))
                {
                    superiorName = basicInfo["EMPLOYEE_L4_NAME"];
                    superiorLevel = "L4";
                }

                newBasicInfo.Add("SUPERIOR_NAME", superiorName);
                newBasicInfo.Add("SUPERIOR_LEVEL", superiorLevel);

                return newBasicInfo;
            }

            return null;
        }

        //public List<Dictionary<string, dynamic>> getSuperiorInfoByBadgeNo(string badgeNo)
        //{
        //    var output = GetJsonDataFromWebApi(WebApiUrlEllipse + "externaldata/executequerybyparam/QRY_EMP_SUPERIOR|@badgenos='" + badgeNo + "'");
        //    if (output.Count > 0)
        //    {
        //        return output;
        //    }

        //    return null;
        //}

        public List<Dictionary<string, dynamic>> getOtherInfo(string badgeNo)
        {
            var output = GetJsonDataFromWebApi(WebApiUrlEllipse + "externaldata/executequerybyparam?param=QRY_ADLIST_EX&$filter=employeeId eq '" + badgeNo + "'");
            if (output.Count > 0)
            {
                return output;
            }

            return null;
        }

        public void WriteLogs(string p)
        {
            string logPath = global::PTVI.KAIZEN.WebApp.Properties.Settings.Default.LogPath;
            using (StreamWriter writer = new StreamWriter(new FileStream(Path.Combine(logPath, "Log.txt"), FileMode.Append)))
            {
                writer.WriteLine(DateTime.Now + ": " + p);
            }
        }
    }

}