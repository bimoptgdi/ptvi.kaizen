﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PTVI.KAIZEN.WebApp
{
    public enum AccessRights
    {
        iPromMenu,
        InputiProm,
        InputProject,
        InputTechnicalSupport,
        Report,
        ReportArchiveProjectList,
        ReportProjectDistribution,
        ReportCapitalOperatingProject,
        ReportWorkingHours,
        ReportTechnicalSupport,
        ReportBudgetPerformancebyArea,
        Admin,
        AdminManageLookup,
        AdminManageUserProfile,
        AdminManageSystemConfiguration,
        AdminManageMenu,
        AdminFileIntegration,
        ProjectDashboard,
        ViewProjectDetail,
        ViewTechnicalSupport,
        PMModul,
        ProjectKPIGraph,
        AssignEngineer,
        AssignDesigner,
        AssignProjectController,
        AssignMaterialCoordinator,
        ManageDocEWP,
        ManageDesignDoc,
        MaterialStatusReport,
        CostReport,
        ManageEWPConstruction,
        ManageTender,
        ConstructionMonitoring,
        Timesheet,
        ProjectCloseOut,
        OtherDeptTask,
        EngineerMaterialReport,
        BuyerMaterial,
        IPTProject,
        FileIntegrationCostController,
        FileIntegrationMaterial,
        FileIntegrationConstructionServices,
        EmailRedaction,
        ReportTimesheetDetail,
        AssignProjectOfficer,
        ForecastSpending,
        DesignConformity,
        SecretaryEngineering,
        EWPWeightHours,
        ConstructionListEWPControl,
        DesignerProduction,
        MaterialExcessFileIntegrationMenu,
        MaterialExcessInquiryMenu,
        InputEWRMenu,
        MyEWRMenu,
        FileIntegrationMenu,
        ProjectBudgetApproval,
        DCWeightHours,
        ProjectScopeDefinition,
        ReportEngineeringServices,
        ReportEngineerDesigner
    }

    public static class constantRoleAbb
    {
        public const string ADMINISTRATOR = "ADM";
        public const string PROJECT_MANAGER = "PPM";
        public const string LEAD_DESIGNER_C_M_ES = "LCES";
        public const string LEAD_DESIGNER_E_I_ES = "LEES";
        public const string MATERIAL_COORDINATOR = "PMC";
        public const string DOCUMENT_CONTROLLER = "DC";
        public const string PROJECT_CONTROLLER = "PPC";        
    }
    public static class constantRoleDefi
    {
        public const string PROJECT_MANAGER = "Project Manager";
        public const string LEAD_DESIGNER_C_M_ES = "Lead Designer Mechanical Civil";
        public const string LEAD_DESIGNER_E_I_ES = "Lead Designer Electrical Instrument";
        public const string MATERIAL_COORDINATOR = "Material Coordinator";
        public const string DOCUMENT_CONTROLLER = "Document Controller";
        public const string PROJECT_CONTROLLER = "Project Controller";
        public const string PM_FINALIZE = "PM Finalize";
    }
}