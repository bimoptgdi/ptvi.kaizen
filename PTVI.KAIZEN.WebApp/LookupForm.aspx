﻿<%@ Page Title="Lookup" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LookupForm.aspx.cs" Inherits="PTVI.KAIZEN.WebApp.LookupForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <script src="Scripts/kendo/vue/kendo-datasource-vue-wrapper.js"></script>
    <script src="Scripts/kendo/vue/kendo-grid-vue-wrapper.js"></script>
    <script src="Scripts/kendo/vue/kendo-dropdowns-vue-wrapper.js"></script>

    <link rel="stylesheet" href="Content/kendo/kendo.common.min.css"/>
<link rel="stylesheet" href="Content/kendo/kendo.default.min.css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <div id="fixed" class="row border-bottom white-bg dashboard-header" style="padding-top: 20px !important;padding-bottom:20px !important" >
   <div class="col-lg-12">
    <div id="grdLookup">
        <div class="container">
            <kendo-grid style="width:1050px" :data-source="dsLookup" :columns="grdColoumn" event="onEditLookup"
                :editable="'popup'"
                :toolbar="[{name:'create'}]"
                :pageable="{buttonCount: 5}" >
            </kendo-grid>
        </div>
    </div>
    </div>
    </div>
    <script>
        
        $(document).ready(function () {
            new Vue({
                el: "#grdLookup",
                data: function () {
                    return {
                        dsLookup: new kendo.data.DataSource({
                            transport: {
                                read: _apiInfo.webApiUrl + "Lookups",
                                create: {
                                    url: _apiInfo.webApiUrl + "Lookups/addLookup/1",
                                    type: "POST"
                                },
                                update: {
                                    url: function (data) {
                                        return _apiInfo.webApiUrl + "Lookups/updateLookup/" + data.OID;
                                    },
                                    type: "PUT"
                                },
                                destroy: {
                                    url: function (data) {
                                        return _apiInfo.webApiUrl + "Lookups/" + data.OID
                                    },
                                    type: "DELETE"
                                },
                                methods: {
                                    parameterMap: function (options, operation) {
                                        if (operation !== 'read' && options.models) {
                                            return { models: kendo.stringify(options.models) }
                                        }
                                    }
                                },
                            },
                            schema: {
                                model: {
                                    id: "OID",
                                    fields: {
                                        TYPE: { type: "string", validation: { required: true } },
                                        TEXT: { type: "string" },
                                        VALUE: { type: "string", validation: { required: true } },
                                        DESCRIPTION: { type: "string" },
                                        PARENTID: { type: "int" },
                                        ISACTIVE: { type: "boolean" },
                                        PARENT: { type: "json" }
                                    }
                                }
                            },
                            pageSize: 10
                        }),
                        grdColoumn: [
                    { field: 'TYPE', title: 'Type' },
                    { field: 'VALUE', title: 'Value' },
                    { field: 'TEXT', title: 'Text' },
                    { field: 'DESCRIPTION', title: 'Description' },
                    { field: 'ISACTIVE', title: 'Active', filterable: true, template: '<input type=\'checkbox\' #: ISACTIVE ? \'checked\' : \'\' #  disabled />' },
                    { field: "PARENT", title: "Parent", template: this.parentTemplate, editor: this.parentEditor },
                    { command: ['edit', 'destroy'], title: "&nbsp;" }
                    ]
                    }
                },
               
                
                parentEditor: function (container, options) {
                    var lookupData = this.dsLookup.data();
                    var newLookupData = $.grep(lookupData, function (elem) {
                        return elem.OID != "";
                    });
                    newLookupData = $.each(newLookupData, function (idx, elem) {
                        elem.DISPLAY = elem.TYPE + " - " + elem.VALUE;
                    });
                    var ms = $('<input style="width: 80%" name="' + options.field + '"/>')
                        .appendTo(container)
                        .kendoComboBox({
                            filter: "contains",
                            dataSource: newLookupData,
                            dataTextField: "DISPLAY",
                            dataValueField: "OID"
                        });
                },
                onEditLookup: function (e) {
                    if (e.model.id == "")
                        e.model.set("ISACTIVE", true);

                    var editWindow = e.sender.editable.element.data("kendoWindow");
                    editWindow.wrapper.css({ width: 700 });

                    var editContainer = e.sender.editable.element.find(".k-edit-form-container");
                    editContainer.css({ width: "100%" });

                    var inputDescription = editContainer.find('input[name="DESCRIPTION"]');
                    inputDescription.css({ width: "100%" });

                    if (e.model.id != "") {
                        $("[name='TYPE']").prop("disabled", true);
                        $("[name='VALUE']").prop("disabled", true);
                    }

                    editWindow.center();
                },
            })
        });
        function lookupTypeEditor(container, options) {
            var lookupTypes = Enumerable.From(vm.dsLookup.data()).Distinct("$.TYPE").Select("$.TYPE").ToArray();
            console.log(lookupTypes);

            var ms = $('<input style="width: 80%"  required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoAutoComplete({
                    //dataTextField: "FULL_NAME",
                    //dataValueField: "USERNAME",
                    //valuePrimitive: true,
                    filter: "contains",
                    dataSource: lookupTypes,
                    suggest: true,
                    value: options.model.TYPE
                });
        }
    </script>
</asp:Content>
