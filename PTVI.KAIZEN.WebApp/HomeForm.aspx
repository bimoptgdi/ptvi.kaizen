﻿<%@ PagePage Title="Lookup" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeForm.aspx.cs" Inherits="PTVI.KAIZEN.WebApp.HomeForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/kendo/vue/kendo-grid-vue-wrapper.js"></script>
    <script src="Scripts/kendo/vue/kendo-datasource-vue-wrapper.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="app">
        <home v-if="showHome"></home>
        <analyst-home v-if="showAnalystHome"></analyst-home>
    </div>

    <script>

        Vue.component('home', httpVueLoader('scripts/components/home/HomeTemplate.vue'));
        Vue.component('analyst-home', httpVueLoader('scripts/components/home/analysttemplate.vue'));

        $(document).ready(function () {
            
            new Vue({
                el: "#app",
                
                data: function() {
                    return {
                        showHome: false,
                        showAnalystHome: true,
                        dataSource: new kendo.data.DataSource({
                                transport: {
                                read: projectBaseData.webApiUrl + "sample/getsamplesforanalyst"
                            },
                            schema: {
                                model: {
                                    id: "OID",
                                    fields: {
                                        WAYBILLNO: { type: "string", validation: { required: true } },
                                        SAMPLENO: { type: "string" },
                                        SAMPLESEQ: { type: "int" },
                                        SAMPLEDATE: { type: "date" },
                                        FRACTION: { type: "string" },
                                        URGENCY: { type: "string" },
                                        VARIAN: { type: "string" },
                                        SEMIDRY: { type: "string" },
                                        TYPE: { type: "string" },
                                        SAMPLER: { type: "string" }
                                    }
                                }
                            },
                            pageSize: 15
                        }),
                        gridColumns: [
                            { field: "SAMPLENO", title: "Sample No" },
                            { field: "TYPE", title: "Type" },
                            { field: "VARIAN", title: "Varian" },
                            { field: "URGENCY", title: "Urgency" },
                            { field: "SAMPLER", title: "Sampler" },
                            { command: [{ name: 'View', click: this.onClickView }], title: "&nbsp;" }
                        ]
                    }
                },
                methods: {
                    onClickView: function (e) {
                        alert("Klik");
                    }
                }
            });


        });

    </script>
</asp:Content>